package com.gmail.alaerof.model;

import java.io.File;

public class FileGDP {
    private String shortName;
    private String fullName;
    private String remoteName;
    private int sentQ;
    // 0 - to send, 1 - successfully sent, -1 - something wrong happened while send
    private int status;
    
	public FileGDP(String fullName) {
        this.fullName = fullName;
        shortName = new File(this.fullName).getName();
	}
	
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getRemoteName() {
		return remoteName;
	}

	public void setRemoteName(String remoteName) {
		this.remoteName = remoteName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getServerReply() {
		if(status == 0){
			return "Файл '" + shortName + "' еще не отсылался";
			//return "File '"+fullName+"' is not sent";
		}
		if(status == 1){
			return "Файл '" + shortName
					+ "' успешно отослан, название файла в удаленной системе '"
					+ remoteName + "'";
			//return "File '" + fullName
			//		+ "' is successfully sent, file name on remote is '"
			//		+ remoteName + "'";
		}
		return "Не удалось отослать файл '" + shortName
				+ "', название файла в удаленной системе " + remoteName
				+ "', подробности см. в логе";
		//return "Something wrong happened while send file '" + fullName
		//		+ "', remote name is " + remoteName
		//		+ "', and it wasn't delivered";
		
	}

	public int getSentQ() {
		return sentQ;
	}
	public void setSentQ(int sentQ) {
		this.sentQ = sentQ;
	}

    public boolean isSuccessfullySent() {
		return status == 1;
    }
    
    private String getEName() {
        if (shortName != null) {
            return shortName.toLowerCase();
        }
        return null;
    }
    
    @Override
    public String toString() {
        return shortName;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        String ename = getEName();
        result = prime * result + ((ename == null) ? 0 : ename.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileGDP other = (FileGDP) obj;
        String ename = getEName();
        String otherename = other.getEName();
        if (ename == null) {
            if (otherename != null)
                return false;
        } else if (!ename.equals(otherename))
            return false;
        return true;
    }
    
}
