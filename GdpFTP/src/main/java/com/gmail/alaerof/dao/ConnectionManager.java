package com.gmail.alaerof.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.utils.CommonUtils;

public class ConnectionManager {
	private static Logger logger = LogManager.getLogger(ConnectionManager.class);
	private Connection connection;
	private PoolInfo poolInfo;
	public static final String FILE_NAME = "config" + File.separator
			+ "database.properties";

	private class PoolInfo {
		String driver;
		String url;
	}

	public ConnectionManager() throws ClassNotFoundException {
		poolInfo = loadPoolInfo();
		if (poolInfo != null) {
			String driver = poolInfo.driver;
			Class.forName(driver);
		}
	}

	private PoolInfo loadPoolInfo() {
		PoolInfo pi = new PoolInfo();
		try {
			Properties prop = CommonUtils.getProperties(FILE_NAME);
			pi.driver = prop.getProperty("jdbc.driverClassName");
			pi.url = prop.getProperty("jdbc.url");
			System.out.println(pi.driver);
			System.out.println(pi.url);
			logger.info(pi.driver);
			logger.info(pi.url);
		} catch (Exception e) {
			logger.error("init param fail", e);
		}
		return pi;
	}

	public Connection getConnection() throws SQLException {
		String url = poolInfo.url;
		connection = DriverManager.getConnection(url);
		return connection;
	}

}
