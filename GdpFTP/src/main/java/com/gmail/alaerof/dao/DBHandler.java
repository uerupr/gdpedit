package com.gmail.alaerof.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.model.FileGDP;
import com.gmail.alaerof.utils.CommonUtils;

public class DBHandler {
	private static Logger logger = LogManager.getLogger(DBHandler.class);
    private static DBHandler dbhandler = new DBHandler();
	private ConnectionManager cm;
	private final static String SQL_UPDATE="UPDATE GDParm SET dateKTC = ?, responceKTC = ? WHERE GDParmID = ?";

    private DBHandler() {
		try {
			cm = new ConnectionManager();
		} catch (ClassNotFoundException e) {
			logger.error(e.toString(), e);
			e.printStackTrace();
		}
    }
    
    public static DBHandler getInstance() {
        return dbhandler;
    }
    
	public void saveServerResponse(FileGDP fileGDP, int gdparmID){
		if (!fileGDP.isSuccessfullySent()) {
			try (Connection cn = cm.getConnection(); PreparedStatement pst = cn.prepareStatement(SQL_UPDATE)) {
                String dd = CommonUtils.getFormattedDate();
                pst.setString(1, dd);
                String sr = fileGDP.getServerReply();
                pst.setString(2, sr);
                pst.setInt(3, gdparmID);
                pst.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                e.printStackTrace();
            } 
		} else {
			logger.error("file " + fileGDP.getFullName()
					+ "not uploaded - something wrong happened while send ");
		}
	}
}
