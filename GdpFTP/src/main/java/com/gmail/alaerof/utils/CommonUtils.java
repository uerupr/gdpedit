package com.gmail.alaerof.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.model.FileGDP;

public class CommonUtils {
	private static Logger logger = LogManager.getLogger(CommonUtils.class);
	private static final String TEMP_PATH = "log" + File.separator + "fileInfo.txt"; 
	
    public static String getFormattedDate() {
        Date d = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateF = formatter.format(d) + ":00";
        return dateF;
    }
    
    public static String getShortFormattedDate() {
        Date d = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHH");
        String dateF = formatter.format(d);
        return dateF;
    }    
    
    public static Properties getProperties(String fileName) {
        Properties prop = new Properties();
        try {
			try (InputStream in = new FileInputStream(fileName)) {
				prop.load(in);
			}
        } catch (FileNotFoundException e) {
            logger.error(e.toString(), e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            e.printStackTrace();
        }
        return prop;
    }
    
    public static String saveTempFile(FileGDP fileGDP){
		String info = fileGDP.getRemoteName() + ".xml" + "\n"
				+ fileGDP.getFullName() + "\n" + fileGDP.getShortName();
    	boolean isErr = false;
        try {
			try (BufferedWriter out = new BufferedWriter(new FileWriter(TEMP_PATH))) {
				out.write(info);
				out.flush();
			}
        } catch (FileNotFoundException e) {
            logger.error(e.toString(), e);
            e.printStackTrace();
        	isErr = true;
        } catch (IOException e) {
            logger.error(e.toString(), e);
            e.printStackTrace();
        	isErr = true;
        }    	
		if (!isErr) {
			return TEMP_PATH;
		}
		return null;
    }
    
    public static String extractTail(String formattedName){
    	// 14 Минск - Молодечно 140114_140114.xml
    	String [] s = formattedName.split(" ");
    	String res = s[s.length-1];
    	String []ss = res.split("\\.");
    	res = ss[0];
    	return res;
    }
}
