package com.gmail.alaerof.app;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.gmail.alaerof.model.FileGDP;

public class LocalFilesPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static File currentDir = null;
    private DefaultListModel<FileGDP> listModel = new DefaultListModel<>();
    private JList<?> list = new JList<>(listModel);

    public LocalFilesPanel() {
        this.setLayout(new BorderLayout());
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setPreferredSize(new Dimension(400, 300));

        JPanel left = new JPanel();
        left.setPreferredSize(new Dimension(300, 300));
        left.setLayout(new BorderLayout());
        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        list.setVisibleRowCount(-1);

        list.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                int cllc = event.getClickCount();
                if (cllc == 2) {
                    int index = list.locationToIndex(event.getPoint());
                    if (index > -1) {
                        listModel.remove(index);
                        list.repaint();
                    }
                }
            }
        });

        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(400, 80));
        left.add(listScroller, BorderLayout.CENTER);

        JPanel right = new JPanel();
        right.setPreferredSize(new Dimension(100, 300));
        FlowLayout ly = new FlowLayout();
        ly.setAlignment(FlowLayout.LEFT);
        right.setLayout(ly);
        {
            JButton btnNewButton = new JButton("Открыть");
            right.add(btnNewButton);
            btnNewButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    String fileName = "";
                    FileNameExtensionFilter filter = new FileNameExtensionFilter("ГДП файлы", "xml", "txt");
                    final JFileChooser fc = new JFileChooser(currentDir);
                    fc.setFileFilter(filter);
                    fc.setMultiSelectionEnabled(true);
                    int returnVal = fc.showOpenDialog(FTPClientFrame.frame);

                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File[] files = fc.getSelectedFiles();
                        for (File file : files) {
                            currentDir = file;
                            fileName = file.getAbsolutePath();
                            FileGDP fg = new FileGDP(fileName);
                            fg.setRemoteName(fg.getShortName());
                            if (!listModel.contains(fg)) {
                                listModel.addElement(fg);
                            }
                        }
                    }
                }
            });
        }
        {
            JButton btnNewButton = new JButton("Отослать");
            right.add(btnNewButton);

            btnNewButton.addActionListener(new ActionListener() {
                @SuppressWarnings("unchecked")
                @Override
                public void actionPerformed(ActionEvent evn) {
                	FTPClientFrame.frame.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    try {
                        @SuppressWarnings("rawtypes")
                        List ll = Arrays.asList(listModel.toArray());
                        FTPClientFrame.frame.sendAllFiles(ll);
                        listModel.clear();
                        list.repaint();
                    } finally {
                    	FTPClientFrame.frame.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    }
                }
            });
        }
        {
            JButton btnNewButton = new JButton("Обновить настройки FTP");
            right.add(btnNewButton);

            btnNewButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evn) {
                	FTPClientFrame.frame.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    try {
                        FTPClientFrame.frame.connector.updateConnectionProperty();
                    } finally {
                    	FTPClientFrame.frame.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    }
                }
            });
        }        

        JSplitPane pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, left, right);
        pane.setOneTouchExpandable(true);
        pane.getLeftComponent().setMinimumSize(new Dimension(0, 0));
        pane.getLeftComponent().setPreferredSize(new Dimension(400, 300));
        this.add(pane, BorderLayout.CENTER);
    }

}