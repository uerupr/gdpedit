package com.gmail.alaerof.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.gmail.alaerof.dao.DBHandler;
import com.gmail.alaerof.ftp.Connector;
import com.gmail.alaerof.model.FileGDP;
import com.gmail.alaerof.utils.CommonUtils;

public class FTPClientFrame extends JFrame{
	private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(FTPClientFrame.class);
    public static final String ICON_PATH = "res" + File.separator + "gdpsrv.gif";
    private JPanel contentPane;
    private SentPanel sentPanel = new SentPanel();
    public static FTPClientFrame frame;
    private static int topPanelHeight = 200;
    private int sentQ = 1;
    public Connector connector = new Connector();
    
    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }    
    
    private static void setLookAndFeel() {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    UIManager.put("swing.boldMetal", Boolean.FALSE);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
    	final String[] arg = args;
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    setLookAndFeel();
                    
                    if (arg.length > 0) {
                    	topPanelHeight = 0;
                    	frame = new FTPClientFrame();
                        frame.sendFileFromCMD(arg);
                        frame.setVisible(true);
                    } else{
                    	frame = new FTPClientFrame();
                        frame.setVisible(true);
                    }
                    
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.toString(), e);
                }
            }
        });
    }
    
	/**
     * Create the frame.
     */
    private FTPClientFrame() {
        setTitle("Отправка ГДП в КТЦ");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 800, 500);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        setLocationRelativeTo(null);

        try {
            BufferedImage buffImage = ImageIO.read(new File(ICON_PATH));
            setIconImage(buffImage);
        } catch (IOException e) {
            logger.error("Icon create error ", e);
        }

        JPanel panelTop = new LocalFilesPanel();

        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, panelTop, sentPanel);
        split.setOneTouchExpandable(true);
        split.getLeftComponent().setMinimumSize(new Dimension(0, 0));
        split.getLeftComponent().setPreferredSize(new Dimension(0, 300));
        split.setDividerLocation(topPanelHeight);
        contentPane.add(split, BorderLayout.CENTER);

    }
    
    protected void sendFileFromCMD(String[] arg) {
        String filePath = "";
        String fileName = "";
        int gdparmID = 0;
        for (String s : arg) {
            logger.info(s);
            String[] ss = s.split("=");
            if ("filePath".equals(ss[0])) {
                filePath = ss[1];
            }
            if ("fileName".equals(ss[0])) {
                fileName = ss[1];
            }
            if ("GDParmID".equals(ss[0])) {
                try {
                    gdparmID = Integer.parseInt(ss[1]);
                } catch (RuntimeException e) {
                    logger.error(e.toString(), e);
                    throw e;
                }
            }
        }
        FileGDP file = new FileGDP(filePath);
        file.setShortName(fileName);
        String remoteName = "ID" + gdparmID + "_" + CommonUtils.extractTail(fileName);
        file.setRemoteName(remoteName);
        List<FileGDP> list = new ArrayList<>();
        list.add(file);
        frame.sendAllFiles(list);
        DBHandler.getInstance().saveServerResponse(file, gdparmID);
	}	
    
	public void sendAllFiles(List<FileGDP> list) {
        sentQ++;
        for (FileGDP f : list) {
        	String infoName = CommonUtils.saveTempFile(f);
            f.setSentQ(sentQ);
            // send file GDP
            boolean completed = connector.sendFile(f.getFullName(), f.getRemoteName() + ".xml");
            // send info file
            connector.sendFile(infoName, f.getRemoteName() + "_info.txt");
			if (completed) {
				f.setStatus(1);
			} else {
				f.setStatus(-1);
			}
        }
        sentPanel.addSentFiles(list);
	}

    
}
