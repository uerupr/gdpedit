package com.gmail.alaerof.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.utils.CommonUtils;

public class Connector {
    private static Logger logger = LogManager.getLogger(Connector.class);
    private ConnectionProperty ftpConnectionProperty;
    private static final String CONF_PATH = "config" + File.separator + "connection.properties";
    
    public Connector(){
    	updateConnectionProperty();
    }
    
    public void updateConnectionProperty(){
		ftpConnectionProperty = new ConnectionProperty(CommonUtils.getProperties(CONF_PATH));
    }
    
	public boolean sendFile(String fullFileName, String remoteFileName){
        String server = ftpConnectionProperty.getServer();
        int port = ftpConnectionProperty.getPort();
        FTPClient ftpClient = new FTPClient();
        boolean completed = false;
        try {
        	 
            ftpClient.connect(server, port);
            String user = ftpConnectionProperty.getUser();
            String pwd = ftpConnectionProperty.getPwd();
			if (ftpClient.login(user, pwd)) {
				ftpClient.enterLocalPassiveMode();

				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
				
		        // APPROACH #2: uploads second file using an OutputStream
		        File localFile = new File(fullFileName);
		        String shortFileName = FilenameUtils.getName(fullFileName) + " = " + remoteFileName;
		        InputStream inputStream = new FileInputStream(localFile);

				System.out.println("Start uploading " + shortFileName + " file");
				logger.info("Start uploading " + shortFileName + " file");
		        
		        OutputStream outputStream = ftpClient.storeFileStream(remoteFileName);
		        byte[] bytesIn = new byte[4096];
		        int read = 0;

		        while ((read = inputStream.read(bytesIn)) != -1) {
		            outputStream.write(bytesIn, 0, read);
		        }
		        inputStream.close();
		        outputStream.close();

		        completed = ftpClient.completePendingCommand();
		        if (completed) {
		            System.out.println("File " + shortFileName + " is uploaded successfully.");
		            logger.info("File " + shortFileName + " is uploaded successfully.");
		        }
			} else {
				logger.error("Error: user login or password failed");
			}

        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
            logger.error(ex.toString(), ex);
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                logger.error(ex.toString(), ex);
            }
        }      
        return completed;
	}
	
}
