package com.gmail.alaerof.ftp;

import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ConnectionProperty {
    private static Logger logger = LogManager.getLogger(ConnectionProperty.class);
    private static final int DEFAULT_PORT = 21;
	private String server;
	private int	port = DEFAULT_PORT;
	private String user;
	private String pwd;
	
	public ConnectionProperty(Properties properties) {
		try {
			port = Integer.parseInt(properties.getProperty("port"));
		} catch (NumberFormatException e) {
			logger.error(e.toString(), e);
			e.printStackTrace();
		}		
		server = properties.getProperty("server");
		user = properties.getProperty("user");
		pwd = properties.getProperty("pwd");
		logger.info("server = " + server);
		logger.info("port = " + port);
		logger.info("user = " + user);
		System.out.println("server = " + server);
		System.out.println("port = " + port);
		System.out.println("user = " + user);
	}

	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}	
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
