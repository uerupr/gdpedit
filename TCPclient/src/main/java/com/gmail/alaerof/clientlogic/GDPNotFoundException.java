package com.gmail.alaerof.clientlogic;

public class GDPNotFoundException extends Exception{
    private static final long serialVersionUID = 1L;

    public GDPNotFoundException() {
        super();
    }

    public GDPNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public GDPNotFoundException(String message) {
        super(message);
    }

    
}
