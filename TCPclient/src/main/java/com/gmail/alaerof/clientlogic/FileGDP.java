package com.gmail.alaerof.clientlogic;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FileGDP {
    private static Logger logger = LogManager.getLogger(FileGDP.class);
    private String shortName;
    private String fullName;
    private String log;
    /** в формате UTF-8 !!!*/
    private String serverReply;
    private int sentQ;
    private Throwable exception;

    public boolean sendFile(InitParam initParam) {
        String fileName = getFullName();
        String log = "";
        String serverReply = "";
        Throwable err = null;
        ClientSocketGDP clientGDP = new ClientSocketGDP();
        try {
            serverReply = "ответ сервера: "
                    + clientGDP.connectAndSend(initParam.getServerName(), initParam.getPort(), fileName, getShortName());

            log = initParam.getServerName() + ":" + initParam.getPort() + " отправлен '" + fileName + "'";
        } catch (IOException e) {
            log = "при передаче файла произошла ошибка '" + fileName + "'";
            err = e;
        } catch (ServerConnectionException e) {
            log = "ошибка связи с сервером '" + fileName + "'";
            err = e;
        } catch (GDPNotFoundException e) {
            log = "не найден файл " + e.getMessage();
            err = e;
        }
        logger.info(log);
        if (err != null) {
            logger.error(err.toString(), err);
            err.printStackTrace();
            setException(err);
        }
        logger.info(serverReply);
        setLog(log);
        setServerReply(serverReply);
        return err == null;
    }

    public FileGDP(String fullName) {
        this.fullName = fullName;
        shortName = new File(this.fullName).getName();
        /*
         * String fileNameASCII = ""; try { fileNameASCII = new
         * String(fileNameUTF.getBytes("UTF-8"), "windows-1251"); } catch
         * (UnsupportedEncodingException e) { e.printStackTrace(); }
         */
    }

    private String getEName() {
        if (shortName != null) {
            return shortName.toLowerCase();
        }
        return null;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        String ename = getEName();
        result = prime * result + ((ename == null) ? 0 : ename.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileGDP other = (FileGDP) obj;
        String ename = getEName();
        String otherename = other.getEName();
        if (ename == null) {
            if (otherename != null)
                return false;
        } else if (!ename.equals(otherename))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return shortName;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getServerReply() {
        return serverReply;
    }

    public void setServerReply(String serverReply) {
        this.serverReply = serverReply;
    }

    public String getServerReplyWin(){
        String strWin = "";
        try {
//            strWin = new String(serverReply.getBytes("UTF-8"), "windows-1251");
            strWin = new String(serverReply.getBytes("UTF-8"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
        return strWin;
    }
    
    public int getSentQ() {
        return sentQ;
    }

    public void setSentQ(int sentQ) {
        this.sentQ = sentQ;
    }
    
    /** указывает что был ексепшен при отправке на сервер */
    public boolean isException() {
        return exception != null;
    }

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    
}
