package com.gmail.alaerof.clientlogic;

import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.util.FileProperties;

public class InitParam {
    private static Logger logger = LogManager.getLogger(InitParam.class);
    private int port = 4040;
    private String serverName = "localhost";

    public int getPort() {
        return port;
    }

    public String getServerName() {
        return serverName;
    }
    
    public void loadInitParam(String conf) {
        try {
            Properties prop = FileProperties.getInstance().getProperties(conf);
            port = new Integer(prop.getProperty("port"));
            serverName = prop.getProperty("serverName");
            logger.info(this.toString());
        } catch (Exception e) {
            logger.error("init param fail", e);
        }
    }

    @Override
    public String toString() {
        return "InitParam [port=" + port + ", serverName=" + serverName + "]";
    }

}
