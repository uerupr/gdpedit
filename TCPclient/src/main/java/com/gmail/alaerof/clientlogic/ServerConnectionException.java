package com.gmail.alaerof.clientlogic;

public class ServerConnectionException extends Exception{
    private static final long serialVersionUID = 1L;

    public ServerConnectionException() {
        super();
    }

    public ServerConnectionException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ServerConnectionException(String arg0) {
        super(arg0);
    }

    public ServerConnectionException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }
}
