package com.gmail.alaerof.main;

import java.io.File;

public class FileGDP {
    private String shortName;
    private String fullName;
    private String log;
    private String serverReply;
    private int sentQ;
    private Throwable exception;

    public FileGDP(String fullName) {
        this.fullName = fullName;
        shortName = new File(this.fullName).getName();
        /*
         * String fileNameASCII = ""; try { fileNameASCII = new
         * String(fileNameUTF.getBytes("UTF-8"), "windows-1251"); } catch
         * (UnsupportedEncodingException e) { e.printStackTrace(); }
         */
    }

    private String getEName() {
        if (shortName != null) {
            return shortName.toLowerCase();
        }
        return null;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        String ename = getEName();
        result = prime * result + ((ename == null) ? 0 : ename.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileGDP other = (FileGDP) obj;
        String ename = getEName();
        String otherename = other.getEName();
        if (ename == null) {
            if (otherename != null)
                return false;
        } else if (!ename.equals(otherename))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return shortName;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getServerReply() {
        return serverReply;
    }

    public void setServerReply(String serverReply) {
        this.serverReply = serverReply;
    }

    public int getSentQ() {
        return sentQ;
    }

    public void setSentQ(int sentQ) {
        this.sentQ = sentQ;
    }

    public boolean isException() {
        return exception != null;
    }

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

}
