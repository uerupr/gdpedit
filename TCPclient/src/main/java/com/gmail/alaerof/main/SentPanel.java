package com.gmail.alaerof.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.clientlogic.FileGDP;

class MyCellRenderer extends JLabel implements ListCellRenderer<Object> {
    private static final long serialVersionUID = 1L;

    public Component getListCellRendererComponent(JList<?> list, // the list
            Object value, // value to display
            int index, // cell index
            boolean isSelected, // is the cell selected
            boolean cellHasFocus) // does the cell have focus
    {
        String s = value.toString();
        setText(s);
        FileGDP f = (FileGDP) value;
        if (f.isException()) {
            setForeground(Color.RED);
        }else{
            setForeground(Color.BLACK);
        }
        return this;
    }
}

public class SentPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private DefaultListModel<FileGDP> listModel = new DefaultListModel<>();
    private JList<?> list = new JList<>(listModel);
    private JTextArea description = new JTextArea();
    private JTextArea log = new JTextArea();

    public SentPanel() {
        this.setLayout(new BorderLayout());
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setPreferredSize(new Dimension(400, 300));

        JPanel left = new JPanel();
        left.setPreferredSize(new Dimension(200, 300));
        left.setLayout(new BorderLayout());

        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        list.setVisibleRowCount(-1);
        list.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                int index = list.locationToIndex(event.getPoint());
                if (index > -1) {
                    FileGDP f = listModel.get(index);
                    description.setText(f.getLog() + "\n" + f.getServerReply());
                }
            }
        });
        list.setCellRenderer(new MyCellRenderer());
        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(300, 80));
        left.add(listScroller, BorderLayout.CENTER);
        {
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            panel.setPreferredSize(new Dimension(200, 80));
            JScrollPane scroll = new JScrollPane(description);
            panel.add(scroll, BorderLayout.CENTER);
            left.add(panel, BorderLayout.SOUTH);
        }

        JPanel right = new JPanel();
        right.setPreferredSize(new Dimension(200, 300));
        right.setLayout(new BorderLayout());
        {
            JScrollPane scroll = new JScrollPane(log);
            right.add(scroll, BorderLayout.CENTER);
        }

        JSplitPane pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, left, right);
        pane.setOneTouchExpandable(true);
        pane.getLeftComponent().setMinimumSize(new Dimension(0, 0));
        pane.getLeftComponent().setPreferredSize(new Dimension(300, 300));
        this.add(pane, BorderLayout.CENTER);
    }

    public void addSentFiles(List<FileGDP> files) {
        for (FileGDP f : files) {
            listModel.addElement(f);
            appendText(f);
        }
        list.repaint();
        log.repaint();
    }

    private void appendText(FileGDP f) {
        StringBuilder ss = new StringBuilder(log.getText());
        ss.append(f.getLog() + "\n" + f.getServerReply() + "\n");
        log.setText(ss.toString());
    }

}
