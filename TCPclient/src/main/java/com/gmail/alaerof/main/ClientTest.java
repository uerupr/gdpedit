package com.gmail.alaerof.main;

import java.io.UnsupportedEncodingException;

import com.gmail.alaerof.clientlogic.ClientSocketGDP;

public class ClientTest {
    public static void main(String[] args) {
        ClientSocketGDP clientGDP = new ClientSocketGDP();
        String fileNameUTF = "C:\\Test.xml";
        //System.out.println(args[0]);
        String fileNameASCII="";
        try {
            fileNameASCII = new String(fileNameUTF.getBytes("UTF-8"),"windows-1251");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //clientGDP.connect("192.167.0.100", 4040, fileNameUTF);
        System.out.println(fileNameASCII);
        System.out.println(fileNameUTF);
        //clientGDP.connectAndSend("localhost", 4040, "", fileNameUTF);
    }
}
