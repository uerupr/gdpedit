package com.gmail.alaerof.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.gmail.alaerof.clientlogic.FileGDP;
import com.gmail.alaerof.clientlogic.InitParam;
import com.gmail.alaerof.dao.DBHandler;

import java.awt.image.BufferedImage;

public class TCPclientFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(TCPclientFrame.class);
    private JPanel contentPane;
    private SentPanel sentPanel = new SentPanel();
    private InitParam initParam = new InitParam();
    public static TCPclientFrame frame;
    private int sentQ = 1;
    public static final String CONF_PATH = "res" + File.separator + "TCPclientConf.properties";
    public static final String ICON_PATH = "res" + File.separator + "gdpsrv.gif";

    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }

    private static void setLookAndFeel() {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    UIManager.put("swing.boldMetal", Boolean.FALSE);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        final String[] arg = args;
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    setLookAndFeel();
                    frame = new TCPclientFrame();
                    if (arg.length > 0) {
                        frame.sendFileFromCMD(arg);
                    }
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.toString(), e);
                }
            }
        });
    }

    private void sendFileFromCMD(String[] arg) {
        String filePath = "";
        String fileName = "";
        int gdparmID = 0;
        for (String s : arg) {
            logger.info(s);
            String[] ss = s.split("=");
            if ("filePath".equals(ss[0])) {
                filePath = ss[1];
            }
            if ("fileName".equals(ss[0])) {
                fileName = ss[1];
            }
            if ("GDParmID".equals(ss[0])) {
                try {
                    gdparmID = Integer.parseInt(ss[1]);
                } catch (RuntimeException e) {
                    logger.error(e.toString(), e);
                    throw e;
                }
            }
        }
        FileGDP file = new FileGDP(filePath);
        file.setShortName(fileName);
        List<FileGDP> list = new ArrayList<>();
        list.add(file);
        frame.sendAllFiles(list);
        DBHandler.getInstance().saveServerResponse(file, gdparmID);
    }

    /**
     * Create the frame.
     */
    private TCPclientFrame() {
        setTitle("Отправка ГДП в КТЦ");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 800, 500);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        setLocationRelativeTo(null);

        try {
            BufferedImage buffImage = ImageIO.read(new File(ICON_PATH));
            setIconImage(buffImage);
        } catch (IOException e) {
            logger.error("создание иконки", e);
        }

        JPanel panelTop = new LoadedPanel();

        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, panelTop, sentPanel);
        split.setOneTouchExpandable(true);
        split.getLeftComponent().setMinimumSize(new Dimension(0, 0));
        split.getLeftComponent().setPreferredSize(new Dimension(0, 300));
        split.setDividerLocation(200);
        contentPane.add(split, BorderLayout.CENTER);

        initParam.loadInitParam(CONF_PATH);
    }

    public void sendAllFiles(List<FileGDP> list) {
        sentQ++;
        for (FileGDP f : list) {
            f.setSentQ(sentQ);
            f.sendFile(initParam);
        }
        sentPanel.addSentFiles(list);
    }

}
