package com.gmail.alaerof.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.clientlogic.FileGDP;

public class DBHandler {
    private static DBHandler dbhandler = new DBHandler();
    private static Logger logger = LogManager.getLogger(DBHandler.class);
    private ConnectionManager cm;

    private DBHandler() {
        try {
            cm = new ConnectionManager();
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
            e.printStackTrace();
        }
    }

    public static DBHandler getInstance() {
        return dbhandler;
    }

    public static String getFormattedDate() {
        Date d = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateF = formatter.format(d) + ":00";
        return dateF;
    }

    public void saveServerResponse(FileGDP fileGDP, int gdparmID) {
        String sql = "UPDATE GDParm SET dateKTC = ?, responceKTC = ? WHERE GDParmID = ?";
        if (!fileGDP.isException()) {
            try (Connection cn = cm.getConnection(); PreparedStatement pst = cn.prepareStatement(sql)) {
                String dd = getFormattedDate();
                pst.setString(1, dd);
                String sr = fileGDP.getServerReply();
                pst.setString(2, sr);
                pst.setInt(3, gdparmID);
                pst.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                e.printStackTrace();
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
                e.printStackTrace();
            }
        }
    }
}
