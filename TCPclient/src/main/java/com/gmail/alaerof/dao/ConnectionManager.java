package com.gmail.alaerof.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.util.FileProperties;

public class ConnectionManager {
    private static Logger logger = LogManager.getLogger(ConnectionManager.class);
    private Connection connection;
    private PoolInfo poolInfo;
    public static final String FILE_NAME = "res" + File.separator + "dbserver.properties";

    private class PoolInfo {
        String driver;
        String url;
    }

    public ConnectionManager() throws PoolManagerException {
        poolInfo = loadPoolInfo();
        if (poolInfo != null) {
            String driver = poolInfo.driver;
            try {
                Class.forName(driver);
            } catch (ClassNotFoundException e) {
                throw new PoolManagerException("не найден драйвер к БД", e);
            }
        } else {
            throw new PoolManagerException("ошибка чтения настроек подключения");
        }
    }

    public Connection getConnection() throws PoolManagerException {
        String url = poolInfo.url;
        try {
            connection = DriverManager.getConnection(url);
        } catch (SQLException e) {
            throw new PoolManagerException("нет подключения к БД", e);
        }
        return connection;
    }

    private PoolInfo loadPoolInfo() {
        PoolInfo pi = new PoolInfo();
        try {
            Properties prop = FileProperties.getInstance().getProperties(FILE_NAME);
            pi.driver = prop.getProperty("driver");
            pi.url = prop.getProperty("url");
            System.out.println(pi.driver);
            System.out.println(pi.url);
            logger.info(pi.driver);
            logger.info(pi.url);
        } catch (Exception e) {
            logger.error("init param fail", e);
        }
        return pi;
    }
}
