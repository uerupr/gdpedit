package com.gmail.alaerof.dao;

public class PoolManagerException extends Exception {
    private static final long serialVersionUID = 1L;

    public PoolManagerException(String mess) {
        super(mess);
    }

    public PoolManagerException(String mess, Throwable t) {
        super(mess, t);
    }
}
