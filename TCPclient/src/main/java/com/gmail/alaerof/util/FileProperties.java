package com.gmail.alaerof.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FileProperties {
    private static Logger logger = LogManager.getLogger(FileProperties.class);
    private static FileProperties instance = new FileProperties();

    private FileProperties() { }

    public Properties getProperties(String fileName) {
        Properties prop = new Properties();
        InputStream in;
        try {
            in = new FileInputStream(fileName);
            try {
                prop.load(in);
            } finally {
                in.close();
            }
        } catch (FileNotFoundException e) {
            logger.error(e.toString(), e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            e.printStackTrace();
        }
        return prop;
    }

    public static FileProperties getInstance() {
        return instance;
    }

}

