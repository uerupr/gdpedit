package com.gmail.alaerof.loc.util;

import java.awt.Color;

public class ColorConverter {
    public static Color convert(int value){
        Color clr = new Color(value, false);
        int r = clr.getBlue();
        int g = clr.getGreen();
        int b = clr.getRed();
        clr = new Color(r,g,b);
        return clr;
    }
    
    public static int convert(Color clr){
        int value = 0;
        int r = clr.getBlue();
        int g = clr.getGreen();
        int b = clr.getRed();
        clr = new Color(r,g,b,0);
        value = clr.getRGB();
        return value;
    }
    
    public static int convertInt(int value){
        Color clr = new Color(value, false);
        int r = clr.getBlue();
        int g = clr.getGreen();
        int b = clr.getRed();
        clr = new Color(r,g,b);
        return clr.getRGB();
    }
}

