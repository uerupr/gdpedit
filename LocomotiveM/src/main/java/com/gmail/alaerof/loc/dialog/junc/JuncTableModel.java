package com.gmail.alaerof.loc.dialog.junc;

import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.Junc.JuncType;

public class JuncTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "Код ЕСР", "Название", "Тип", "НОД", "Линий" };
    private List<Junc> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (list != null) {
            Junc el = list.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return el.codeESR;
            case 1:
                return el.name;
            case 2:
                if (el.getJuncType() != null) {
                    return el.getJuncType().getString();
                } else {
                    return null;
                }
            case 3:
                if (el.getDir() != null) {
                    return el.getDir().name;
                } else {
                    return null;
                }
            case 4:
                return el.lineCount;
            }
        }
        return null;
    }

    @Override
    public java.lang.Class<?> getColumnClass(int col) {
        switch (col) {
        case 2:
            return JuncType.class;
        case 3:
            return Direction.class;
        case 4:
            return Integer.class;
        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null) {
            Junc el = list.get(row);
            if (el != null) {
                switch (col) {
                case 0:
                    el.codeESR = (String) aValue;
                    break;
                case 1:
                    Junc test = new Junc();
                    test.name = ((String) aValue).trim();
                    if ("".equals(test.name)) {
                        JOptionPane.showMessageDialog(null, "Название должно быть!", "Предупреждение",
                                JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                    if (!list.contains(test))
                        el.name = test.name;
                    else
                        JOptionPane.showMessageDialog(null, "Названия узлов должны быть разными!", "Предупреждение",
                                JOptionPane.WARNING_MESSAGE);
                    break;
                case 2:
                    JuncType jt = (JuncType) aValue;
                    el.setJuncType(jt);
                    break;
                case 3:
                    Direction d = (Direction) aValue;
                    el.setDir(d);
                    break;
                case 4:
                    Integer lc = (Integer) aValue;
                    el.lineCount = lc;
                    break;
                default:
                    break;
                }
            }
        }
    }

    public List<Junc> getList() {
        return list;
    }

    public void setList(List<Junc> list) {
        this.list = list;
    }

}
