package com.gmail.alaerof.loc.dao.dto;

public class DistrTurn implements Comparable<DistrTurn>{
    public int distrTurnID = -1;
    public int dirID;
    public int distrID;
    public String name;
    public int stBID;
    public int stEID;
    public int weightforw;
    public int weightback;
    public double gradientforw;
    public double gradientback;
    public double lenght;
    public int num;

    // add
    public String dirName = "-";
    public String distrName = "-";
    public Station stB;
    public Station stE;

    public boolean isFieldsFull() {
        return dirID > 0 && distrID > 0 && stBID > 0 && stEID > 0;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + distrTurnID;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DistrTurn other = (DistrTurn) obj;
        if (distrTurnID != other.distrTurnID)
            return false;
        return true;
    }

    @Override
    public int compareTo(DistrTurn o) {
        return this.num - o.num;
    }
}
