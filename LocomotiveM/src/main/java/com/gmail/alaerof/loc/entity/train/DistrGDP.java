package com.gmail.alaerof.loc.entity.train;

import java.util.ArrayList;
import java.util.List;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.DistrTrainDAO;

public class DistrGDP {
    private Distr distr;
    // это только "свои". "чужих" в этом приложении по ка не запланировано
    private List<TrainThread> listTrain;

    public DistrGDP(Distr distr) {
        this.distr = distr;
    }

    public Distr getDistr() {
        return distr;
    }

    public List<TrainThread> getListTrain() {
        return listTrain;
    }

    public void loadGDPFromDB() {
        if (distr != null) {
            DistrTrainDAO dtDAO = LocDAOFactory.getInstance().getDistrTrainDAO();
            // тут загружаютс§ чисто данные из љ?
            listTrain = dtDAO.loadGDP(distr.distrID, new TrainThread());
        }
    }

    private boolean arriveToJunc(Junc junc, TrainThread tr) {
        List<Integer> list = junc.listStID();
        Integer arrID = tr.getLastStID();
        return list.contains(arrID);
    }

    private boolean departFromJunc(Junc junc, TrainThread tr) {
        List<Integer> list = junc.listStID();
        Integer depID = tr.getFirstStID();
        return list.contains(depID);
    }

    public List<TrainThread> getArrived(Junc junc) {
        List<TrainThread> list = new ArrayList<>();
        for (int i = 0; i < listTrain.size(); i++) {
            TrainThread tr = listTrain.get(i);
            if (arriveToJunc(junc, tr)) {
                list.add(tr);
            }
        }
        return list;
    }

    public List<TrainThread> getDepatured(Junc junc) {
        List<TrainThread> list = new ArrayList<>();
        for (int i = 0; i < listTrain.size(); i++) {
            TrainThread tr = listTrain.get(i);
            if (departFromJunc(junc, tr)) {
                list.add(tr);
            }
        }
        return list;
    }

    public TrainThread getTrain(int code, String name) {
        for (TrainThread tr : listTrain) {
            if (code == tr.getCodeINT()&&name.equals(tr.distrTrain.nameTR)) {
                return tr;
            }
        }
        return null;
    }
}
