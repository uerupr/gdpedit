package com.gmail.alaerof.loc.entity;

import com.gmail.alaerof.loc.dao.dto.Scale;
import com.gmail.alaerof.loc.dao.dto.Span;
/**
 * класс бизнесс-объекта (сущности) "ж.д. перегон".
 * содержит:
 * - информацию по перегону (Span, Scale);
 * - начальную станцию (DistrStation);
 * - конечную станцию (DistrStation).
 * @author Helen Yrofeeva
 *
 */
public class DistrSpan implements Comparable<DistrSpan> {
    private Span span;
    private Scale scale;
    private DistrStation stB;
    private DistrStation stE;

    public DistrSpan(Span span, Scale scale) {
        this.span = span;
        this.scale = scale;
    }

    public Span getSpan() {
        return span;
    }

    public Scale getScale() {
        return scale;
    }

    public DistrStation getStB() {
        return stB;
    }

    protected void setStB(DistrStation stB) {
        this.stB = stB;
    }

    public DistrStation getStE() {
        return stE;
    }

    protected void setStE(DistrStation stE) {
        this.stE = stE;
    }

    @Override
    public String toString() {
        return scale.num + " " + span.name;
    }

    @Override
    public int compareTo(DistrSpan o) {
        if (this.scale != null && o.scale != null) {
            return this.scale.num - o.scale.num;
        }
        return 0;
    }

}
