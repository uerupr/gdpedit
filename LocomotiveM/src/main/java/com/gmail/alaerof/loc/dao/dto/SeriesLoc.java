package com.gmail.alaerof.loc.dao.dto;

public class SeriesLoc implements Comparable<SeriesLoc>{
    public int seriesLocID;
    public String series;
    public boolean electr;
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((series == null) ? 0 : series.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SeriesLoc other = (SeriesLoc) obj;
        if (series == null) {
            if (other.series != null)
                return false;
        } else if (!series.equals(other.series))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return series;
    }

    @Override
    public int compareTo(SeriesLoc o) {
        return series.compareTo(o.series);
    }
    
}
