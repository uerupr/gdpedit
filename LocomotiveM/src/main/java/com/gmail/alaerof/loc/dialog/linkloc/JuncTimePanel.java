package com.gmail.alaerof.loc.dialog.linkloc;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;

import com.gmail.alaerof.loc.dao.dto.JuncTime;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;

public class JuncTimePanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private JLabel eq;
    private JLabel dep;
    private JLabel arr;

    public JuncTimePanel() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
        gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);

        JLabel label = new JLabel("по прибытию:");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.anchor = GridBagConstraints.WEST;
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        add(label, gbc_label);

        arr = new JLabel("мин");
        arr.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
        gbc_lblNewLabel.gridx = 1;
        gbc_lblNewLabel.gridy = 0;
        add(arr, gbc_lblNewLabel);

        JLabel label_1 = new JLabel("по отправлению:");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.anchor = GridBagConstraints.WEST;
        gbc_label_1.insets = new Insets(0, 0, 5, 5);
        gbc_label_1.gridx = 0;
        gbc_label_1.gridy = 1;
        add(label_1, gbc_label_1);

        dep = new JLabel("мин");
        dep.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_label_4 = new GridBagConstraints();
        gbc_label_4.insets = new Insets(0, 0, 5, 0);
        gbc_label_4.gridx = 1;
        gbc_label_4.gridy = 1;
        add(dep, gbc_label_4);

        JLabel label_2 = new JLabel("перецепка без захода в депо:");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.anchor = GridBagConstraints.WEST;
        gbc_label_2.insets = new Insets(0, 0, 5, 5);
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 2;
        add(label_2, gbc_label_2);

        eq = new JLabel("мин");
        eq.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_label_5 = new GridBagConstraints();
        gbc_label_5.insets = new Insets(0, 0, 5, 0);
        gbc_label_5.gridx = 1;
        gbc_label_5.gridy = 2;
        add(eq, gbc_label_5);

    }

    public void setTime(JuncTime juncTime) {
        if (juncTime != null) {
            arr.setText("" + juncTime.arrive);
            dep.setText("" + juncTime.depart);
            eq.setText("" + juncTime.equipment);
        } else {
            arr.setText("");
            dep.setText("");
            eq.setText("");
        }
    }

}
