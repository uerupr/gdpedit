package com.gmail.alaerof.loc.mainframe.sheettree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.entity.sheet.SheetEntity;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.application.LinkingLocMain;

public class TreePopupMenuSheet extends JPopupMenu {
    private static final long serialVersionUID = 1L;
    private SheetEntity sheetEntity;

    public TreePopupMenuSheet() {
        initContent();
    }

    private void initContent() {
        this.removeAll();
        {
            JMenuItem item = new JMenuItem("Добавить лист");
            this.add(item);
            item.addActionListener(new ActionListenerAddSheet());
        }
        {
            JMenuItem item = new JMenuItem("Изменить лист");
            this.add(item);
            item.addActionListener(new ActionListenerEditSheet());
        }
        {
            JMenuItem item = new JMenuItem("Удалить лист");
            this.add(item);
            item.addActionListener(new ActionListenerDeleteSheet());
        }
        {
            JMenuItem item = new JMenuItem("Расписание");
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evn) {
                    List<TrainThreadSheet> listTr = sheetEntity.getListTrain();
                    List<SheetDistrSt> listSt = sheetEntity.getListSheetDistrSt();
                    LinkingLocMain.mainFrame.timetableDialog.setListTrain(listTr);
                    LinkingLocMain.mainFrame.timetableDialog.setListSt(listSt);
                    LinkingLocMain.mainFrame.timetableDialog.setVisible(true);
                }
            });
        }

    }

    public SheetEntity getSheetEntity() {
        return sheetEntity;
    }

    public void setSheetEntity(SheetEntity sheetEntity) {
        this.sheetEntity = sheetEntity;
    }
}
