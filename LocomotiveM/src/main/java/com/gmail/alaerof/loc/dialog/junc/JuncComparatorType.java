package com.gmail.alaerof.loc.dialog.junc;

import java.util.Comparator;

import com.gmail.alaerof.loc.dao.dto.Junc;

public class JuncComparatorType implements Comparator<Junc> {

    @Override
    public int compare(Junc j1, Junc j2) {
        int t1 = j1.getJuncType().getInt();
        int t2 = j2.getJuncType().getInt();
        if (t1 == t2) {
            return j1.name.compareTo(j2.name);
        }
        return t1 - t2;
    }

}
