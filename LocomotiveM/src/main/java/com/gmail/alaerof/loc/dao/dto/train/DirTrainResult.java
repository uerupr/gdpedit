package com.gmail.alaerof.loc.dao.dto.train;

/**
 * для получения общего результата поезда нужно суммировать по поезду (trainID)
 * 
 * @author alerof
 * 
 */
public class DirTrainResult {
    public long trainID;
    public int dirID;
    /** расстояние в метрах */
    public int L;
    /** время без остановой на отделении */
    public int Tt;
    /** время с остановками на отделении */
    public int Tu;

    public double getVt() {
        if (Tt > 0) {
            return L * 0.06 / Tt;
        }
        return -1;
    }

    public double getVu() {
        if (Tu > 0) {
            return L * 0.06 / Tu;
        }
        return -1;
    }
}
