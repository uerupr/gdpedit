package com.gmail.alaerof.loc.dao.dto;

import java.util.ArrayList;
import java.util.List;

public class Junc implements Comparable<Junc> {
    public enum JuncType {
        depot, turn, point;

        public String getString() {
            switch (this) {
            case depot:
                return "депо";
            case turn:
                return "оборотное депо";
            case point:
                return "пункт";
            default:
                return null;
            }
        }

        public int getInt() {
            switch (this) {
            case depot:
                return 0;
            case turn:
                return 1;
            case point:
                return 2;
            default:
                return 2;
            }
        }

        public static JuncType getType(String typeValue) {
            switch (typeValue) {
            case "депо":
                return depot;
            case "оборотное депо":
                return turn;
            case "пункт":
                return point;
            default:
                return null;
            }
        }

        public static JuncType getType(int type) {
            switch (type) {
            case 0:
                return depot;
            case 1:
                return turn;
            case 2:
                return point;
            default:
                return null;
            }
        }
    }

    public int juncID;
    public String codeESR;
    public String name;
    public int dirID;
    public int lineCount;
    private JuncType juncType = JuncType.depot;
    // -------
    private List<JuncSt> listSt = new ArrayList<>();
    private JuncTime juncTime = new JuncTime();
    private Direction dir;

    public List<JuncSt> getListSt() {
        return listSt;
    }

    public void setListSt(List<JuncSt> listSt) {
        this.listSt = listSt;
    }

    public List<Integer> listStID() {
        List<Integer> list = new ArrayList<>();
        for (JuncSt st : listSt) {
            list.add(st.stationID);
        }
        return list;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Junc other = (Junc) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public int compareTo(Junc o) {
        return name.compareTo(o.name);
    }

    public JuncType getJuncType() {
        return juncType;
    }

    public void setJuncType(JuncType juncType) {
        this.juncType = juncType;
    }

    public void setJuncType(int type) {
        this.juncType = JuncType.getType(type);
    }

    public Direction getDir() {
        return dir;
    }

    public void setDir(Direction dir) {
        this.dir = dir;
        if (dir != null) {
            dirID = dir.dirID;
        } else {
            dirID = 0;
        }
    }

    public JuncTime getJuncTime() {
        return juncTime;
    }

    public void setJuncTime(JuncTime juncTime) {
        this.juncTime = juncTime;
    }

}
