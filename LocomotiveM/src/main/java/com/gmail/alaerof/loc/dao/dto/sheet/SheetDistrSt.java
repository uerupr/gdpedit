package com.gmail.alaerof.loc.dao.dto.sheet;

public class SheetDistrSt implements Comparable<SheetDistrSt> {
    // DB
    public int sheetID;
    public int distrID; //* 
    public int stationID; //*
    public int num; 
    public int scale;
    public boolean hidden;
    public String visibleName;
    public boolean showVisibleName;
    // add
    public String stationName; //*
    private int y0;
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + num;
        result = prime * result + stationID;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SheetDistrSt other = (SheetDistrSt) obj;
        if (num != other.num)
            return false;
        if (stationID != other.stationID)
            return false;
        return true;
    }

    @Override
    public String toString(){
        return stationName;
    }

    @Override
    public int compareTo(SheetDistrSt o) {
        return num - o.num;
    }

    public int getY0() {
        return y0;
    }

    public void setY0(int y0) {
        this.y0 = y0;
    }
    
    public int getY1(){
        return y0;
    }
    
    public int getYE1(){
        return y0;
    }
    
    
}
