package com.gmail.alaerof.loc.dialog.distrturn;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.entity.DistrSpan;

public class SpanTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "Перегоны" };
    private List<DistrSpan> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            DistrSpan el = list.get(row);
            return el.toString();
        }
        return null;
    }

    public List<DistrSpan> getList() {
        return list;
    }

    public void setList(List<DistrSpan> list) {
        this.list = list;
    }

}
