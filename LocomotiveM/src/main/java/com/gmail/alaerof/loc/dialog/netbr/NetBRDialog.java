package com.gmail.alaerof.loc.dialog.netbr;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dialog.netbr.JuncButton.JuncButtonType;
import com.gmail.alaerof.loc.util.PointCounter;

import java.awt.Font;

public class NetBRDialog extends JFrame {
    private static Logger logger;
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JLabel imageLabel;

    public NetBRDialog() {
        logger = LogManager.getLogger(this.getClass());
        try {
            //setIconImage(Toolkit.getDefaultToolkit().getImage(NetBRDialog.class.getResource("/pic/LL1.GIF")));
            BufferedImage buffImage = ImageIO.read(new File("pic//LL1.GIF"));
            setIconImage(buffImage);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        initContent();
    }

    private void initContent() {
        setBounds(30, 45, 1200, 650);
        int w = 1100;
        int h = 653;

        {
            imageLabel = new JLabel("");
            try {
                BufferedImage buffImage = ImageIO.read(new File("pic//netBR3.jpg"));
                imageLabel.setIcon(new ImageIcon(buffImage));
            } catch (IOException e) {
                logger.error(e.toString(), e);
            }
            imageLabel.setLayout(null);
            imageLabel.setPreferredSize(new Dimension(w, h));
            imageLabel.setBounds(0, 0, w, h);
            contentPanel.setLayout(null);
            contentPanel.add(imageLabel);
            contentPanel.setBounds(0, 0, w, h);
            contentPanel.setPreferredSize(new Dimension(w, h));

            JScrollPane scrollPane = new JScrollPane(contentPanel);
            getContentPane().add(scrollPane, BorderLayout.CENTER);
            
            addAllPoint();
            addAllDistr();

        }

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }

    }

    private void addAllPoint(){
        {
            String name = "Крулевщизна";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.recycling);
            b.setNameSt(name);
            b.setJuncID(24);
            b.setBounds(424, 149, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.PLAIN, 13));
            la.setBounds(452, 171, 100, 30);

        }
        {
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            String name = "Полоцк";
            b.setNameSt(name);
            b.setJuncID(15);
            b.setBounds(548, 82, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(552, 112, 100, 30);
        }
        {
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            String name = "Новополоцк";
            b.setNameSt(name);
            b.setJuncID(47);
            b.setBounds(476, 88, b.getDim(), b.getDim());
        }
        {
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            String name = "Даугавпилс";
            b.setNameSt(name);
            b.setJuncID(50);
            b.setBounds(421, 48, b.getDim(), b.getDim());
        }
        {
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            String name = "Витебск";
            b.setNameSt(name);
            b.setJuncID(13);
            b.setBounds(798, 155, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(745, 125, 100, 30);
        }
        {
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            String name = "Орша";
            b.setNameSt(name);
            b.setJuncID(14);
            b.setBounds(785, 248, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(745, 268, 60, 30);
        }
        {
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            String name = "Кричев";
            b.setNameSt(name);
            b.setJuncID(16);
            b.setBounds(918, 323, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(935, 299, 100, 30);
        }

        {
            String name = "Могилев";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(20);
            b.setBounds(771, 334, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(800, 354, 100, 30);
        }
        {
            String name = "Минск";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(5);
            b.setBounds(457, 302, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(470, 275, 100, 30);
        }
        {
            String name = "Молодечно";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(12);
            b.setBounds(313, 208, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(347, 208, 100, 30);
        }
        {
            String name = "Осиповичи";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(11);
            b.setBounds(576, 384, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(496, 378, 100, 30);
        }
        {
            String name = "Жлобин";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(17);
            b.setBounds(760, 420, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(795, 415, 100, 30);
        }
        {
            String name = "Гомель";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(6);
            b.setBounds(886, 525, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(920, 525, 100, 30);
        }
        {
            String name = "Калинковичи";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(18);
            b.setBounds(668, 517, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(703, 498, 100, 30);
        }
        {
            String name = "Лунинец";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(19);
            b.setBounds(363, 523, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(395, 505, 100, 30);
        }

        {
            String name = "Барановичи";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(8);
            b.setBounds(293, 419, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(335, 405, 100, 30);
        }
        {
            String name = "Лида";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(10);
            b.setBounds(188, 289, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(225, 285, 100, 30);
        }
        {
            String name = "Волковыск";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(21);
            b.setBounds(114, 383, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(122, 408, 100, 30);
        }
        {
            String name = "Брест";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.depot);
            b.setNameSt(name);
            b.setJuncID(9);
            b.setBounds(69, 512, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.BOLD, 13));
            la.setBounds(90, 490, 100, 30);
        }
        {
            String name = "Бобруйск";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.recycling);
            b.setNameSt(name);
            b.setJuncID(23);
            b.setBounds(674, 405, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.PLAIN, 13));
            la.setBounds(688, 380, 100, 30);
        }

        {
            String name = "Слуцк";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.recycling);
            b.setNameSt(name);
            b.setJuncID(25);
            b.setBounds(445, 423, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.PLAIN, 13));
            la.setBounds(450, 395, 100, 30);
        }

        {
            String name = "Гродно";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.recycling);
            b.setNameSt(name);
            b.setJuncID(22);
            b.setBounds(62, 291, b.getDim(), b.getDim());

            JLabel la = new JLabel(name);
            imageLabel.add(la);
            la.setFont(new Font("Tahoma", Font.PLAIN, 13));
            la.setBounds(99, 285, 100, 30);
        }

        {
            String name = "Лынтупы";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(39);
            b.setBounds(266, 108, b.getDim(), b.getDim());
        }

        {
            String name = "Воропаево";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(30);
            b.setBounds(319, 122, b.getDim(), b.getDim());
        }

        {
            String name = "Друя";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(34);
            b.setBounds(332, 64, b.getDim(), b.getDim());
        }

        {
            String name = "Лепель";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(38);
            b.setBounds(634, 192, b.getDim(), b.getDim());
        }

        {
            String name = "Придвинская";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(48);
            b.setBounds(858, 145, b.getDim(), b.getDim());
        }

        {
            String name = "Заднепровская";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(35);
            b.setBounds(847, 260, b.getDim(), b.getDim());
        }

        {
            String name = "Колодищи";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(36);
            b.setBounds(510, 298, b.getDim(), b.getDim());
        }

        {
            String name = "Михановичи";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(40);
            b.setBounds(490, 330, b.getDim(), b.getDim());
        }

        {
            String name = "Помыслище";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(41);
            b.setBounds(433, 326, b.getDim(), b.getDim());
        }
        {
            String name = "Крыжовка";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(37);
            b.setBounds(412, 276, b.getDim(), b.getDim());
        }
        {
            String name = "Гродзянка";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(32);
            b.setBounds(574, 342, b.getDim(), b.getDim());
        }
        {
            String name = "Рабкор";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(45);
            b.setBounds(633, 457, b.getDim(), b.getDim());
        }

        {
            String name = "Барбаров";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(26);
            b.setBounds(696, 560, b.getDim(), b.getDim());
        }

        {
            String name = "Солигорск";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(46);
            b.setBounds(454, 466, b.getDim(), b.getDim());
        }

        {
            String name = "Домашевичи";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(33);
            b.setBounds(272, 383, b.getDim(), b.getDim());
        }

        {
            String name = "Боровцы";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(28);
            b.setBounds(259, 414, b.getDim(), b.getDim());
        }

        {
            String name = "Мосты";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(42);
            b.setBounds(122, 336, b.getDim(), b.getDim());
        }

        {
            String name = "Узбережь";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(44);
            b.setBounds(112, 250, b.getDim(), b.getDim());
        }

        {
            String name = "Берестовица";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(27);
            b.setBounds(56, 397, b.getDim(), b.getDim());
        }
        {
            String name = "Свислочь";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(43);
            b.setBounds(94, 436, b.getDim(), b.getDim());
        }

        {
            String name = "Высоко-Литовск";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(31);
            b.setBounds(70, 463, b.getDim(), b.getDim());
        }
        {
            String name = "Влодава";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.point);
            b.setNameSt(name);
            b.setJuncID(29);
            b.setBounds(77, 575, b.getDim(), b.getDim());
        }

        {
            String name = "Тересполь";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(59);
            b.setBounds(21, 520, b.getDim(), b.getDim());
        }

        {
            String name = "Кузница";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(53);
            b.setBounds(29, 335, b.getDim(), b.getDim());
        }
        {
            String name = "Вильнюс";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(49);
            b.setBounds(197, 169, b.getDim(), b.getDim());
        }
        {
            String name = "Невель";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(54);
            b.setBounds(635, 44, b.getDim(), b.getDim());
        }
        {
            String name = "Новосокольники";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(55);
            b.setBounds(818, 58, b.getDim(), b.getDim());
        }
        {
            String name = "Смоленск";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(58);
            b.setBounds(978, 215, b.getDim(), b.getDim());
        }

        {
            String name = "Рославль";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(56);
            b.setBounds(1027, 320, b.getDim(), b.getDim());
        }

        {
            String name = "Унеча";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(60);
            b.setBounds(1014, 445, b.getDim(), b.getDim());
        }

        {
            String name = "Щорс";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(62);
            b.setBounds(985, 602, b.getDim(), b.getDim());
        }

        {
            String name = "Чернигов";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(61);
            b.setBounds(882, 605, b.getDim(), b.getDim());
        }

        {
            String name = "Коростень";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(52);
            b.setBounds(646, 588, b.getDim(), b.getDim());
        }

        {
            String name = "Сарны";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(57);
            b.setBounds(377, 588, b.getDim(), b.getDim());
        }

        {
            String name = "Ковель";
            JuncButton b = new JuncButton();
            imageLabel.add(b);
            b.setType(JuncButtonType.noRB);
            b.setNameSt(name);
            b.setJuncID(51);
            b.setBounds(158, 588, b.getDim(), b.getDim());
        }
        
    }
    
    private void addAllDistr(){
        { // Полоцк Крулевщизна
            PointCounter.PointRect pr = PointCounter.getRectPoints(562, 96, 438, 163);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        { // Полоцк Новополоцк
            PointCounter.PointRect pr = PointCounter.getRectPoints(562, 96, 486, 98);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        { // Полоцк Даугавпилс
            PointCounter.PointRect pr = PointCounter.getRectPoints(562, 96, 431, 58);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        { // Полоцк Невель
            PointCounter.PointRect pr = PointCounter.getRectPoints(562, 96, 645, 54);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        { // Полоцк Витебск
            PointCounter.PointRect pr = PointCounter.getRectPoints(562, 96, 812, 169);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {  // новосокольники Витебск
            PointCounter.PointRect pr = PointCounter.getRectPoints(828, 68, 812, 169);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // придвинская Витебск
            PointCounter.PointRect pr = PointCounter.getRectPoints(868, 155, 812, 169);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // смоленск Витебск
            PointCounter.PointRect pr = PointCounter.getRectPoints(988, 225, 812, 169);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // орша Витебск
            PointCounter.PointRect pr = PointCounter.getRectPoints(799, 262, 812, 169);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // орша смоленск
            PointCounter.PointRect pr = PointCounter.getRectPoints(799, 262, 988, 225);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // орша лепель
            PointCounter.PointRect pr = PointCounter.getRectPoints(799, 262, 644, 202);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // орша заднепровская
            PointCounter.PointRect pr = PointCounter.getRectPoints(799, 262, 857, 270);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // орша кричев 
            PointCounter.PointRect pr = PointCounter.getRectPoints(799, 262, 932, 337);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // орша могилев 
            PointCounter.PointRect pr = PointCounter.getRectPoints(799, 262, 785, 348);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // орша минск 
            PointCounter.PointRect pr = PointCounter.getRectPoints(799, 262, 471, 316);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // рославль кричев 
            PointCounter.PointRect pr = PointCounter.getRectPoints(1037, 330, 932, 337);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // унеча кричев 
            PointCounter.PointRect pr = PointCounter.getRectPoints(1024, 455, 932, 337);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // могилев кричев 
            PointCounter.PointRect pr = PointCounter.getRectPoints(785, 348, 932, 337);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // могилев жлобин 
            PointCounter.PointRect pr = PointCounter.getRectPoints(785, 348, 774, 434);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // могилев осиповичи 
            PointCounter.PointRect pr = PointCounter.getRectPoints(785, 348, 590, 398);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // Бобруйск осиповичи 
            PointCounter.PointRect pr = PointCounter.getRectPoints(688, 419, 590, 398);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // слуцк осиповичи 
            PointCounter.PointRect pr = PointCounter.getRectPoints(459, 437, 590, 398);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // гродзянка осиповичи 
            PointCounter.PointRect pr = PointCounter.getRectPoints(584, 352, 590, 398);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // минск осиповичи 
            PointCounter.PointRect pr = PointCounter.getRectPoints(471, 316, 590, 398);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // помыслище крыжовка ("05 Парк №3,4 - Колядичи")
            PointCounter.PointRect pr = PointCounter.getRectPoints(443, 336, 422, 286);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // колодищи михановка ("13 Шабаны - Помыслище")
            PointCounter.PointRect pr = PointCounter.getRectPoints(520, 308, 500, 340);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // помыслище михановка ("13 Шабаны - Помыслище")
            PointCounter.PointRect pr = PointCounter.getRectPoints(443, 336, 500, 340);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // минск барановичи
            PointCounter.PointRect pr = PointCounter.getRectPoints(471, 316, 307, 433);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // минск молодечно
            PointCounter.PointRect pr = PointCounter.getRectPoints(471, 316, 327, 222);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // крулевщизна молодечно
            PointCounter.PointRect pr = PointCounter.getRectPoints(438, 163, 327, 222);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // крулевщизна лынтупы
            PointCounter.PointRect pr = PointCounter.getRectPoints(438, 163, 276, 118);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // воропаево друя
            PointCounter.PointRect pr = PointCounter.getRectPoints( 329, 132, 342, 74);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // молодечно вильнюс
            PointCounter.PointRect pr = PointCounter.getRectPoints(327, 222, 207, 179);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // молодечно лида
            PointCounter.PointRect pr = PointCounter.getRectPoints(327, 222, 202, 303);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // вильнюс лида
            PointCounter.PointRect pr = PointCounter.getRectPoints(207, 179, 202, 303);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // лида мосты ("38 Лида - Мосты - Гродно")
            PointCounter.PointRect pr = PointCounter.getRectPoints(202, 303, 132, 346);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // мосты гродно ("38 Лида - Мосты - Гродно")
            PointCounter.PointRect pr = PointCounter.getRectPoints(132, 346, 76, 305);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // узбережь гродно  ("22 Узбереж - Гродно")
            PointCounter.PointRect pr = PointCounter.getRectPoints(122, 260, 76, 305);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // гродно кузница  ("21 Гродно - Кузница")
            PointCounter.PointRect pr = PointCounter.getRectPoints(76, 305, 39, 345);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // волковыск берестовица  ("25 Волковыск - Берестовица")
            PointCounter.PointRect pr = PointCounter.getRectPoints(128, 397, 66, 407);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // волковыск свислочь  ("18 Волковыск-Цент - Свислочь")
            PointCounter.PointRect pr = PointCounter.getRectPoints(128, 397, 104, 446);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // мосты волковыск ("26 Мосты - Волковыск")
            PointCounter.PointRect pr = PointCounter.getRectPoints(132, 346, 128, 397);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // боровцы домашевичи ("36 Барановичи-П - Домашевичи")
            PointCounter.PointRect pr = PointCounter.getRectPoints(269, 424, 282, 393);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // барановичи волковыск ("19 Барановичи-Пол - Волковыск-Центр")
            PointCounter.PointRect pr = PointCounter.getRectPoints(307, 433, 128, 397);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // барановичи лида ("37 Барановичи-Полесс - Лида")
            PointCounter.PointRect pr = PointCounter.getRectPoints(307, 433, 202, 303);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // барановичи слуцк ("35 Слуцк - Барановичи")
            PointCounter.PointRect pr = PointCounter.getRectPoints(307, 433, 459, 437);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // слуцк солигорск ("71 Слуцк - Солигорск")
            PointCounter.PointRect pr = PointCounter.getRectPoints(459, 437, 464, 476);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // барановичи лунинец ("28 Барановичи - Лунинец")
            PointCounter.PointRect pr = PointCounter.getRectPoints(307, 433, 377, 537);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // барановичи брест ("81 Барановичи - Брест-Центр")
            PointCounter.PointRect pr = PointCounter.getRectPoints(307, 433, 83, 526);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // высоко-литовск брест ("86 Брест-Вост - В-Литовск-Черемха")
            PointCounter.PointRect pr = PointCounter.getRectPoints(80, 473, 83, 526);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // тересполь брест ("82 Брест-Вост - Тересполь")
            PointCounter.PointRect pr = PointCounter.getRectPoints(31, 530, 83, 526);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // влодава брест ("96 Влодава - Брест-Центр")
            PointCounter.PointRect pr = PointCounter.getRectPoints(87, 585, 83, 526);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // ковель брест ("84 Ковель - Брест-Центр")
            PointCounter.PointRect pr = PointCounter.getRectPoints(168, 598, 83, 526);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // лунинец брест ("80 Лунинец - Брест-Центр")
            PointCounter.PointRect pr = PointCounter.getRectPoints(377, 537, 83, 526);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // лунинец сарны ("30 Лунинец - Сарны")
            PointCounter.PointRect pr = PointCounter.getRectPoints(377, 537, 387, 598);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // лунинец калинковичи ("52 Калинковичи - Лунинец")
            PointCounter.PointRect pr = PointCounter.getRectPoints(377, 537, 682, 531);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // коростень калинковичи ("46 Калинковичи - Коростень")
            PointCounter.PointRect pr = PointCounter.getRectPoints(656, 598, 682, 531);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // барбаров калинковичи ("46 Калинковичи - Коростень" ?)
            PointCounter.PointRect pr = PointCounter.getRectPoints(706, 570, 682, 531);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // гомель калинковичи ("52 Калинковичи - Лунинец")
            PointCounter.PointRect pr = PointCounter.getRectPoints(900, 539, 682, 531);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // гомель чернигов ("17 Чернигов - Гомель")
            PointCounter.PointRect pr = PointCounter.getRectPoints(900, 539, 892, 615);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // гомель щорс ("49_2 Щорс - Гомель")
            PointCounter.PointRect pr = PointCounter.getRectPoints(900, 539, 995, 612);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // гомель щорс ("49_2 Щорс - Гомель")
            PointCounter.PointRect pr = PointCounter.getRectPoints(900, 539, 995, 612);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // гомель унеча ("44 Унеча - Гомель")
            PointCounter.PointRect pr = PointCounter.getRectPoints(900, 539, 1024, 455);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // гомель жлобин ("47 Гомель - Жлобин")
            PointCounter.PointRect pr = PointCounter.getRectPoints(900, 539, 774, 434);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // калинковичи жлобин ("51 Жлобин - Калинковичи")
            PointCounter.PointRect pr = PointCounter.getRectPoints(682, 531, 774, 434);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // бобруйск жлобин ("63 Жлобин - Осиповичи")
            PointCounter.PointRect pr = PointCounter.getRectPoints(688, 419, 774, 434);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        {   // бобруйск рабкор ("74 Бобруйск - Рабкор")
            PointCounter.PointRect pr = PointCounter.getRectPoints(688, 419, 643, 467);
            DistrLine dl = pr.dl;
            imageLabel.add(dl);
            dl.setBounds(pr.x, pr.y, pr.w, pr.h);
        }
        
    }
}
