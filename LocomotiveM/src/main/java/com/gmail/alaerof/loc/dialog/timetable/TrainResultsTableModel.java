package com.gmail.alaerof.loc.dialog.timetable;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.train.DirTrainResult;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.util.TableUtils;

public class TrainResultsTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "НОД", "L, м", "Tt, мин", "Tu, мин", "Vt, км/ч", "Vu, км/ч" };
    private List<DirTrainResult> list;
    private List<Direction> listDir;

    public TrainResultsTableModel() {
        listDir = LocDAOFactory.getInstance().getDirectionDAO().loadAllDirection();
    }

    private String getDirName(int dirID) {
        if (dirID == 0) {
            return "Всего:";
        }
        for (Direction dir : listDir) {
            if (dir.dirID == dirID)
                return dir.name.trim();
        }
        return "";
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            DirTrainResult dtr = list.get(row);
            switch (col) {
            case 0:
                return getDirName(dtr.dirID);
            case 1:
                return dtr.L;
            case 2:
                return dtr.Tt;
            case 3:
                return dtr.Tu;
            case 4:
                return TableUtils.dfNumber(dtr.getVt(),1);
            case 5:
                return TableUtils.dfNumber(dtr.getVu(),1);
            }
        }
        return null;
    }

    public void setList(List<DirTrainResult> list) {
        this.list = new ArrayList<>(list);
        calculateSum();
    }

    private void calculateSum() {
        DirTrainResult dtr = new DirTrainResult();
        for (DirTrainResult d : list) {
            dtr.L += d.L;
            dtr.Tt += d.Tt;
            dtr.Tu += d.Tu;
        }
        list.add(dtr);
    }

}
