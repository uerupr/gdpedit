package com.gmail.alaerof.loc.dialog.distr;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Junc;

public class DistrTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "участок", "узел нач.", "узел кон.", "источник ГДП (файл xml)" };
    private List<Distr> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            Distr el = list.get(row);
            switch (col) {
            case 0:
                return el.name;
            case 1:
                if (el.getJuncBFull() != null)
                    return el.getJuncBFull().name;
                else
                    return null;
            case 2:
                if (el.getJuncEFull() != null)
                    return el.getJuncEFull().name;
                else
                    return null;
            case 3:
                return el.fileName;
            }
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null) {
            Distr d = list.get(row);
            switch (col) {
            case 1:
                Junc juncB = (Junc) aValue;
                if (juncB != null) {
                    d.juncB = juncB.juncID;
                    d.setJuncBFull(juncB);
                }
                break;
            case 2:
                Junc juncE = (Junc) aValue;
                if (juncE != null) {
                    d.juncE = juncE.juncID;
                    d.setJuncEFull(juncE);
                }
                break;
            default:
                break;
            }
        }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 1 || col == 2) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 1:
        case 2:
            return Junc.class;
        default:
            return Object.class;
        }
    }

    public List<Distr> getList() {
        return list;
    }

    public void setList(List<Distr> list) {
        this.list = list;
    }

}
