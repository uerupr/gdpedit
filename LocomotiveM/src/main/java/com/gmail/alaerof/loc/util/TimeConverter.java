package com.gmail.alaerof.loc.util;

public class TimeConverter {
    /** tm - "xx:xx" */
    public static int timeToMinutes(String tm) {
        int res = 0;
        if ("".equals(tm.trim())) {
            return -1;
        }
        if (tm != null) {
            String[] ss = tm.split(":");
            int h = Integer.parseInt(ss[0]);
            int m = Integer.parseInt(ss[1]);
            res = h * 60 + m;
        }
        return res;
    }

    /** @return "xx:xx" */
    public static String minutesToTime(int m) {
        int hh = m / 60;
        int mm = m - hh * 60;
        String sh = "" + hh;
        if (hh >= 24)
            hh = 0;
        if (hh < 10) {
            sh = "0" + hh;
        }
        String sm = "" + mm;
        if (mm < 10) {
            sm = "0" + mm;
        }
        return sh + ":" + sm;
    }

    /** с учетом перехода через сутки и переводом в минуты */
    public static int minutesBetween(String tOn, String tOff) {
        int mOn = timeToMinutes(tOn);
        int mOff = timeToMinutes(tOff);
        return minutesBetween(mOn, mOff);
    }

    /** с учетом перехода через сутки */
    public static int minutesBetween(int mOn, int mOff) {
        int res = 0;
        if (mOn <= mOff) {
            res = mOff - mOn;
        } else {
            res = 24 * 60 - (mOn - mOff);
        }
        return res;
    }

    /**
     * добавление минут к заданному моменту времени с учетом перехода через сутки
     * @param time исходное время в минутах
     * @param min добавочые минуты (величина может быть отрицательной)
     * @return итоговое время в минутах
     */
    public static int addMinutes(int time, int min) {
        int res = time + min;
        if (res >= 24 * 60) {
            res = res - 24 * 60;
        }
        if (res < 0) {
            res = 24 * 60 + res;
        }
        return res;
    }

    /**
     * проверяет попадает ли значение в сутки
     * @param time время в минутах
     */
    public static void checkTimeArgument(Integer time) {
        if (time != null) {
            if (time > 1439 || time < 0)
                throw new IllegalArgumentException();
        }
    }

}
