package com.gmail.alaerof.loc.mainframe.sheettree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.sheet.DistrSheetGDP;
import com.gmail.alaerof.loc.entity.sheet.SheetDistrEntity;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.application.LinkingLocMain;
import com.gmail.alaerof.loc.util.FileHandler;
import com.gmail.alaerof.loc.xml.GDPDataXML;

public class TreePopupMenuDistr extends JPopupMenu {
    private static final long serialVersionUID = 1L;
    private SheetDistrEntity sheetDistrEntity;

    public TreePopupMenuDistr() {
        initContent();
    }

    private void initContent() {
        this.removeAll();
        {
            JMenuItem item = new JMenuItem("Загрузить ГДП из xml в БД");
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    String fileName = "";
                    File file = FileHandler.chooseFile(true, new FileNameExtensionFilter("xml files",
                            "xml"), fileName, LinkingLocMain.mainFrame);

                    if (file != null) {
                        boolean done = false;
                        LinkingLocMain.mainFrame.setCursorWait();
                        try {
                            fileName = file.getAbsolutePath();
                            int distrID = sheetDistrEntity.getSheetDistr().distrID; // !!!
                            DistrSheetGDP distrGDP = sheetDistrEntity.getDistrGDP(); // !!!
                            GDPDataXML xmlReader = GDPDataXML.getInstance();
                            List<TrainThreadSheet> list = xmlReader.loadFromFile(fileName,
                                    distrGDP.getDistrEntity());
                            if (list.size() > 0) {
                                distrGDP.setListTrain(list);
                                LocDAOFactory.getInstance().getDistrTrainDAO().saveGDP(distrID, list);
                                sheetDistrEntity.setGDPSource(fileName); // !!!
                                done = true;
                            }
                        } finally {
                            LinkingLocMain.mainFrame.setCursorDefault();
                        }
                        String mess = "";
                        if (done) {
                            mess = "ГДП загружен успешно";
                        } else {
                            mess = "ГДП не загружен";
                        }
                        JOptionPane.showMessageDialog(LinkingLocMain.mainFrame, mess, "",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            });
        }
        {
            JMenuItem item = new JMenuItem("Обновить ГДП из БД");
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    LinkingLocMain.mainFrame.setCursorWait();
                    try {
                        // тут нужно загрузить ГДП из БД
                        sheetDistrEntity.loadGDPFromDB();
                    } finally {
                        LinkingLocMain.mainFrame.setCursorDefault();
                    }
                }
            });
        }
    }

    public SheetDistrEntity getSheetDistrEntity() {
        return sheetDistrEntity;
    }

    public void setSheetDistrEntity(SheetDistrEntity sheetDistrEntity) {
        this.sheetDistrEntity = sheetDistrEntity;
    }
}
