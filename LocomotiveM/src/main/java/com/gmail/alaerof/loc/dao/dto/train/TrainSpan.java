package com.gmail.alaerof.loc.dao.dto.train;

import com.gmail.alaerof.loc.util.TimeConverter;

public class TrainSpan {
    public long trainID;
    public int spanID;
    /** занятие перегона */
    private int timeOn;
    /** освобождение перегона */
    private int timeOff;

    /** занятие перегона */
    public int getTimeOn() {
        return timeOn;
    }

    /** занятие перегона */
    public void setTimeOn(int timeOn) {
        TimeConverter.checkTimeArgument(timeOn);
        this.timeOn = timeOn;
    }

    /** освобождение перегона */
    public int getTimeOff() {
        return timeOff;
    }

    /** освобождение перегона */
    public void setTimeOff(int timeOff) {
        TimeConverter.checkTimeArgument(timeOff);
        this.timeOff = timeOff;
    }

    @Override
    public String toString() {
        return "TrainSpan [timeOn=" + TimeConverter.minutesToTime(timeOn) + ", timeOff=" + TimeConverter.minutesToTime(timeOff) + "]";
    }

}
