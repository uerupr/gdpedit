package com.gmail.alaerof.loc.dialog.linkloc;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.SeriesLoc;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.linktrain.LinkTrainEntity;
import com.gmail.alaerof.loc.entity.train.DistrGDP;
import com.gmail.alaerof.loc.entity.train.TrainThread;

public class LinkLocUtil {
    protected static Logger logger = LogManager.getLogger(LinkLocUtil.class);
    public static List<Distr> listDistr;
    public static List<SeriesLoc> listLoc;

    public static String getNameDistr(int distrID) {
        if (listDistr != null) {
            for (Distr distr : listDistr) {
                if (distr.distrID == distrID) {
                    return distr.name;
                }
            }
        }
        return "";
    }

    public static String getNameLoc(int locID) {
        if (listLoc != null) {
            for (SeriesLoc loc : listLoc) {
                if (loc.seriesLocID == locID) {
                    return loc.series;
                }
            }
        }
        return "";
    }

    /**
     * заполняет список увязок поездами из ГДП,
     * если поезда нет в списке ГДП пытается добыть его из БД
     * @param listLink список увязок
     * @param listGDP список выбранных ГДП
     * @return заполненный список увязок
     */
    public static List<LinkTrainEntity> getListEntity(List<LinkTrain> listLink, List<DistrGDP> listGDP) {
        List<LinkTrainEntity> list = new ArrayList<>();
        for (LinkTrain lk : listLink) {
            TrainThread trOn = null;
            TrainThread trOff = null;
            for (DistrGDP gdp : listGDP) {
                if (lk.distrIDoff == gdp.getDistr().distrID) {
                    trOff = gdp.getTrain(lk.codeTRoff, lk.nameTRoff);
                }
                if (lk.distrIDon == gdp.getDistr().distrID) {
                    trOn = gdp.getTrain(lk.codeTRon, lk.nameTRon);
                }
            }
            if (trOn == null) {
                trOn = LocDAOFactory.getInstance().getDistrTrainDAO().loadTrain(lk, new TrainThread(), true);
            }
            if (trOff == null) {
                trOff = LocDAOFactory.getInstance().getDistrTrainDAO()
                        .loadTrain(lk, new TrainThread(), false);
            }
            // в список добавляем даже если нет ни одного поезда
            // т.е. возможно когда trOn = null и trOff = null !!!
            // null означает что поезда нет в БД
            LinkTrainEntity lte = new LinkTrainEntity();
            lte.setLinkTrain(lk);
            lte.setTrainOn(trOn);
            lte.setTrainOff(trOff);
            list.add(lte);
        }
        return list;
    }

    /** поиск в списке увязок по поезду отправления со станции */
    public static LinkTrainEntity getLinkTrainEntityOff(List<LinkTrainEntity> list, TrainThread trDep) {
        for (LinkTrainEntity lt : list) {
            TrainThread trOff = lt.getTrainOff();
            if (trOff != null && trOff.equals(trDep)) {
                return lt;
            }
        }
        return null;
    }

    /** поиск в списке увязок по поезду прибытия на станцию */
    public static LinkTrainEntity getLinkTrainEntityOn(List<LinkTrainEntity> list, TrainThread trArr) {
        for (LinkTrainEntity lt : list) {
            TrainThread trOn = lt.getTrainOn();
            if (trOn != null && trOn.equals(trArr)) {
                return lt;
            }
        }
        return null;
    }

    /**
     * Обновляет имеющуюся или добавляет новую увязку в список.
     * Только один из поездов может быть null (это означает депо)
     * @param list список увязок
     * @param trArr поезд прибытия
     * @param trDep поезд отправления
     * @param junc узел
     * @param loc локомотив
     */
    public static void setLinkTrainEntity(List<LinkTrainEntity> list, TrainThread trArr, TrainThread trDep,
            Junc junc, SeriesLoc loc) {
        try {
            if ((trArr != null) || (trDep != null)) {
                // сначала ищем по поезду прибытия
                LinkTrainEntity lte = getLinkTrainEntityOn(list, trArr);
                if (lte == null) {
                    // потом по поезду отправления
                    lte = getLinkTrainEntityOff(list, trDep);
                }
                // создание новой увязки при необходимости
                if (lte == null) {
                    lte = new LinkTrainEntity();
                    lte.setLinkTrain(new LinkTrain());
                    list.add(lte);
                }
                LinkTrain lt = lte.getLinkTrain();
                lt.seriesLocID = loc.seriesLocID;
                lt.juncID = junc.juncID;
                lte.setTrainOn(trArr);
                lte.setTrainOff(trDep);
                // отправление из депо
                if (trArr == null) {
                    lte.setDepotOn();
                }
                // прибытие в депо
                if (trDep == null) {
                    lte.setDepotOff();
                }
                lt.distrNameOn = getNameDistr(lt.distrIDon);
                lt.distrNameOff = getNameDistr(lt.distrIDoff);
                lt.locName = getNameLoc(lt.seriesLocID);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
