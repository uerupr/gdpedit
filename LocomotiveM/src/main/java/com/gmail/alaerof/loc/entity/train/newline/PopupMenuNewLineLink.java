package com.gmail.alaerof.loc.entity.train.newline;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import com.gmail.alaerof.loc.entity.linktrain.LinkTrainSheet;

public class PopupMenuNewLineLink extends PopupMenuNewLine {
    private static final long serialVersionUID = 1L;

    @Override
    protected void initContent() {
        {
            JMenuItem item = new JMenuItem("переместить выше");
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    moveLink(-1);
                }
            });
        }
        {
            JMenuItem item = new JMenuItem("переместить ниже");
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    moveLink(1);
                }
            });
        }
        {
            JMenuItem item = new JMenuItem("удалить");
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteLink();
                }
            });
        }
    }

    protected void moveLink(int step) {
        LinkTrainSheet linkTS = (LinkTrainSheet) line.getTrainThreadElement();
        linkTS.moveLink(step);
    }

    private void deleteLink() {

        LinkTrainSheet linkTS = (LinkTrainSheet) line.getTrainThreadElement();
        int res = JOptionPane.showConfirmDialog(null, "Удалить " + linkTS.getToolTipText() + " ?", "?",
                JOptionPane.OK_CANCEL_OPTION);
        System.out.println(res);
        if (res == 0) {
            linkTS.markLinkForDelete();
        }
    }
}
