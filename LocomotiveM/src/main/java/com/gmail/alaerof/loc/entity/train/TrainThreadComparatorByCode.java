package com.gmail.alaerof.loc.entity.train;

import java.util.Comparator;

public class TrainThreadComparatorByCode implements Comparator<TrainThread>{

    @Override
    public int compare(TrainThread tr1, TrainThread tr2) {
        return tr1.getCodeINT() - tr2.getCodeINT();
    }

}
