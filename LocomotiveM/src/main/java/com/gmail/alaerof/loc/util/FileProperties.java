package com.gmail.alaerof.loc.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FileProperties {
    private static Logger logger = LogManager.getLogger(FileProperties.class);
    private static FileProperties instance = new FileProperties();

    private FileProperties() {
    }

    public Properties getProperties(String fileName) {
        Properties prop = new Properties();
        try {
            try(InputStream in = new FileInputStream(fileName)) {
                prop.load(in);
            }
        } catch (FileNotFoundException e) {
            logger.error(e.toString(), e);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        return prop;
    }

    public void saveProperties(Properties prop, String fileName) {
        try {
            OutputStream out = new FileOutputStream(fileName);
            try {
                //prop.storeToXML(out, "");
                prop.store(out, "");
            } catch (IOException e) {
                logger.error(e.toString(), e);
            } finally {
                out.close();
            }
        } catch (FileNotFoundException e) {
            logger.error(e.toString(), e);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
    }

    public static FileProperties getInstance() {
        return instance;
    }

}
