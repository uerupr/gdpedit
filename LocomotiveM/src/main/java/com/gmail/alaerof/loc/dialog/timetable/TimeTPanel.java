package com.gmail.alaerof.loc.dialog.timetable;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;

public class TimeTPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    protected TimeTTableModel tableModel;
    protected JTable table;
    protected TrainThreadSheet selectedTrain;
    protected List<SheetDistrSt> listSt; 
    protected List<TimeTStation> listTimeSt = new ArrayList<>();
    protected JLabel laTrain;;

    public TimeTPanel() {
        this.setLayout(new BorderLayout(0, 0));
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        JScrollPane listScroll = new JScrollPane();
        this.add(listScroll, BorderLayout.CENTER);
        tableModel = new TimeTTableModel();
        table = new JTable(tableModel);
        listScroll.setViewportView(table);
        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSize[] = { 120, 40, 40, 30, 30 };
        com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(table, colSize);
        JPanel top = new JPanel();
        this.add(top, BorderLayout.NORTH);
        laTrain = new JLabel("-");
        top.add(laTrain);
    }

    protected void fillListTimeSt() {
        listTimeSt.clear();
        if (listSt != null) {
            for (SheetDistrSt st : listSt) {
                TimeTStation tst = new TimeTStation();
                tst.setStationName(st.stationName);
                tst.setStationID(st.stationID);
                tst.setDistrID(st.distrID);
                listTimeSt.add(tst);
            }
        }
    }

    protected void clearTrainTime() {
        laTrain.setText("-");
        if (listTimeSt != null) {
            for (int i = 0; i < listTimeSt.size(); i++) {
                TimeTStation tst = listTimeSt.get(i);
                tst.setTimeOn("");
                tst.setTimeOff("");
                tst.settStop("");
                tst.setTexStop("");
            }
        }
    }

    protected void fillTrainTime() {
        if (listTimeSt != null && selectedTrain != null) {
            laTrain.setText("" + selectedTrain.getDistrTrain().codeTR + "(" + selectedTrain.getDistrTrain().nameTR
                    + ")");
            for (int i = 0; i < listTimeSt.size(); i++) {
                TimeTStation tst = listTimeSt.get(i);
                TrainStation trSt = selectedTrain.getTrainStation(tst.getStationID());
                if (trSt != null && (tst.getDistrID() == selectedTrain.getDistrID())) {
                    String tmOn = trSt.getTimeOnStr();
                    String tmOff = trSt.getTimeOffStr();
                    if (tmOn.equals(tmOff)) {
                        tmOn = "";
                    }
                    String tstop = "";
                    int min = trSt.getTStop();
                    if (min != 0) {
                        tstop = "" + min;
                    }
                    String texstop = "";
                    if (trSt.texStop > 0) {
                        texstop = "" + trSt.texStop;
                    }
                    tst.setTimeOn(tmOn);
                    tst.setTimeOff(tmOff);
                    tst.settStop(tstop);
                    tst.setTexStop(texstop);
                }
            }
        }
    }

    public TrainThreadSheet getSelectedTrain() {
        return selectedTrain;
    }

    public void setSelectedTrain(TrainThreadSheet selectedTrain) {
        this.selectedTrain = selectedTrain;
        clearTrainTime();
        fillTrainTime();
        table.repaint();
    }

    public List<SheetDistrSt> getListSt() {
        return listSt;
    }

    public void setListSt(List<SheetDistrSt> listSt) {
        this.listSt = listSt;
        fillListTimeSt();
        tableModel.setList(listTimeSt);
        table.repaint();
    }

}
