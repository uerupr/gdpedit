package com.gmail.alaerof.loc.dialog.timetable;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.util.Collections;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.entity.train.TrainThreadComparatorByCodeDistr;
import com.gmail.alaerof.loc.entity.train.TrainThreadComparatorByDistrCode;
import com.gmail.alaerof.loc.entity.train.TrainThreadListUtil;
import org.apache.log4j.LogManager;

public class ListTrainPanel extends JPanel {
  private static final long serialVersionUID = 1L;
  private ListTrainTableModel tableModel;
  private JTable table;
  private TrainThreadSheet selTrain;
  private TimeTPanel timePanel;
  private TimeTFullPanel timeFullPanel;
  private TrainResultsPanel trainResultsPanel;
  private JRadioButton sortNameCode = new JRadioButton("Назв., номер");
  private JRadioButton sortCodeName = new JRadioButton("Номер, Назв.");

  {
    sortNameCode.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (sortNameCode.isSelected()) {
          List<TrainThreadSheet> list = tableModel.getList();
          if (list != null) {
            // сортировка по участкам
            Collections.sort(list, new TrainThreadComparatorByDistrCode());
            table.repaint();
          }
        }
      }
    });
    sortCodeName.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        if (sortCodeName.isSelected()) {
          List<TrainThreadSheet> list = tableModel.getList();
          if (list != null) {
            // сортировка по коду
            Collections.sort(list, new TrainThreadComparatorByCodeDistr());
            table.repaint();
          }
        }
      }
    });
  }

  public ListTrainPanel(TimeTPanel timePanel, TimeTFullPanel timeFullPanel, TrainResultsPanel trainResultsPanel) {
    this.timePanel = timePanel;
    this.timeFullPanel = timeFullPanel;
    this.trainResultsPanel = trainResultsPanel;

    this.setLayout(new BorderLayout(0, 0));
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    JScrollPane listScroll = new JScrollPane();
    this.add(listScroll, BorderLayout.CENTER);
    tableModel = new ListTrainTableModel();
    table = new JTable(tableModel);
    listScroll.setViewportView(table);
    table.setFillsViewportHeight(true);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    int colSize[] = {40, 40, 30, 120};
    com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(table, colSize);

    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent ev) {
        selectRow();
      }
    });

    table.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent ev) {
        selectRow();
      }
    });

    JPanel buttonPanel = new JPanel();
    this.add(buttonPanel, BorderLayout.SOUTH);

    final JTextField searchF = new JTextField();
    searchF.setColumns(10);
    buttonPanel.add(searchF);
    JButton searchB = new JButton("Найти");
    buttonPanel.add(searchB);
    searchB.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ev) {
        String test = searchF.getText();
        int code = 0;
        try {
          code = Integer.parseInt(test);
          int start = table.getSelectedRow() + 1;
          if (start < 0) {
            start = 0;
          }
          List<TrainThreadSheet> list = tableModel.getList();
          TrainThreadSheet sTR = TrainThreadListUtil.getTrainByCode(list, start, code);
          if (sTR != null) {
            int index = list.indexOf(sTR);
            table.changeSelection(index, 0, false, false);
          }
        } catch (NumberFormatException e) {
          LogManager.getLogger(ListTrainPanel.class).error(e.toString(), e);
        }
      }
    });

    JPanel sortPanel = new JPanel();
    this.add(sortPanel, BorderLayout.NORTH);
    sortPanel.add(sortNameCode);
    sortPanel.add(sortCodeName);
    // Group the radio buttons.
    ButtonGroup group = new ButtonGroup();
    group.add(sortNameCode);
    group.add(sortCodeName);
    sortNameCode.setSelected(true);
  }

  private void selectRow() {
    int row = table.getSelectedRow();
    if (row > -1) {
      List<TrainThreadSheet> list = tableModel.getList();
      if (list.size() > 0) {
        // это на случай когда список поездов стал короче, чем ранее
        // выбранная строка в таблице
        if (row > list.size() - 1) {
          row = 0;
        }
        TrainThreadSheet tr = list.get(row);
        if (!tr.equals(selTrain)) {
          selTrain = tr;
          // другие действия при выборе поезда в списке !!!!
          timePanel.setSelectedTrain(selTrain);
          timeFullPanel.setSelectedTrain(selTrain);
          trainResultsPanel.setSelectedTrain(selTrain);
        }
      }
    }
  }

  public List<TrainThreadSheet> getListTrain() {
    return tableModel.getList();
  }

  public void setListTrain(List<TrainThreadSheet> listTrain) {
    // проверяем признак сортировки и сортируем как нужно
    if (sortCodeName.isSelected()) {
      // сортировка по коду
      Collections.sort(listTrain, new TrainThreadComparatorByCodeDistr());
    } else {
      // сортировка по участкам
      Collections.sort(listTrain, new TrainThreadComparatorByDistrCode());
    }
    // отправляем список в модель
    tableModel.setList(listTrain);
    table.repaint();
    selectRow();
  }

}
