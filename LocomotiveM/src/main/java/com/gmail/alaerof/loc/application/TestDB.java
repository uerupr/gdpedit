package com.gmail.alaerof.loc.application;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

//import org.apache.log4j.LogManager;
//import org.apache.log4j.xml.DOMConfigurator;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.factory.SynchronizerDB;
import com.gmail.alaerof.loc.entity.DistrEntity;
import com.gmail.alaerof.loc.entity.sheet.DistrSheetGDP;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.xml.GDPDataXML;

public class TestDB {
    //static {
    //    new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    //}
    
    public static void main(String[] args) {
        //firstLoad();
        loadXML();
        //syncDB();
    }
    
    public static void printListTrain(List<TrainThreadSheet> listTr){
        for(TrainThreadSheet tr: listTr){
            tr.sortSt();
            System.out.println("--- " + tr.getCode() + " ---");
            List<TrainStation> listSt = tr.getTrainSt();
            for(int i=0; i<listSt.size();i++){
                TrainStation st = listSt.get(i);
                System.out.println(st.stationID + "\t" + st.getTimeOnStr() + "\t" + st.getTimeOffStr());
            }
        }
    }
    
    public static void printList(Collection<?> list) {
        Iterator<?> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public static void loadXML(){
        LocDAOFactory f = LocDAOFactory.getInstance();
        List<Distr> list2 = f.getDistrDAO().loadAllDistr();
        DistrEntity distr = new DistrEntity(list2.get(0));
        System.out.println(distr.toString());
        //printList(distr.getListSpan());
        
        GDPDataXML gdpx = GDPDataXML.getInstance();
        DistrSheetGDP distrGDP = new DistrSheetGDP(distr);
        distrGDP.setListTrain(gdpx.loadFromFile("06 Орша - Минск_ГДП с 26.05.2013.xml", distrGDP.getDistrEntity()));
        
        printList(distrGDP.getListTrain().get(0).getTrainSt());
        printList(distrGDP.getListTrain().get(0).getTrainSp());
    }
    
    public static void firstLoad() {
        SynchronizerDB.firstLoad();
        LocDAOFactory f = LocDAOFactory.getInstance();
        List<Direction> list = f.getDirectionDAO().loadAllDirection();
        printList(list);
        //List<distr> list2 = f.getDistrDAO().loadAllDistr();
        //printList(list2);
        //List<Station> list3 = f.getStationDAO().loadAllStation();
        //printList(list3);
        printList(LocDAOFactory.getInstance().getDirectionDAO().loadAllCategory());
    }

    public static void testSync() {
        Set<String> listDst = new HashSet<>();
        Set<String> listSrc = new HashSet<>();
        Set<String> listIns = new HashSet<>();
        Set<String> listDel = new HashSet<>();
        Set<String> listUpd = new HashSet<>();

        // 1 получить список для удаления т.е. те элементы dst которых нет в src
        // 2 получить список для добавл. т.е. те элементы src которых нет в dst
        // 3 получить список для обновления

        listDst.add("str1");
        listDst.add("str2");

        listSrc.add("str2");
        listSrc.add("str3");
        listSrc.add("str4");

        listUpd.addAll(listDst);
        listUpd.retainAll(listSrc);

        listIns.addAll(listSrc);
        listIns.removeAll(listUpd);

        listDel.addAll(listDst);
        listDel.removeAll(listUpd);
        System.out.println("--update--");
        printList(listUpd);
        System.out.println("--ins--");
        printList(listIns);
        System.out.println("--del--");
        printList(listDel);
    }
    
    public static void syncDB(){
        String ss = new SynchronizerDB().updateServerTable();
        //System.out.println(ss);
        JOptionPane.showMessageDialog(null, ss);        
    }

}
