package com.gmail.alaerof.loc.entity.train;

import java.util.Comparator;

public class TrainThreadComparatorByDistrCode implements Comparator<TrainThreadSheet> {
    @Override
    public int compare(TrainThreadSheet tr1, TrainThreadSheet tr2) {
        int x = tr1.getSheetDistrNum() - tr2.getSheetDistrNum();
        if (x == 0) {
            x = tr1.getCodeINT() - tr2.getCodeINT();
        }
        return x;
    }
}
