package com.gmail.alaerof.loc.entity.train.newline;

import javax.swing.JPopupMenu;

public abstract class PopupMenuNewLine extends JPopupMenu {
    private static final long serialVersionUID = 1L;
    protected NewLine line;

    public PopupMenuNewLine() {
        initContent();
    }

    protected abstract void initContent();
    
    public NewLine getLine() {
        return line;
    }

    public void setLine(NewLine line) {
        this.line = line;
    }
}
