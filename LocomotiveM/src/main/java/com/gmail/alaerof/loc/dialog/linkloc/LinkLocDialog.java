package com.gmail.alaerof.loc.dialog.linkloc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.JuncTime;
import com.gmail.alaerof.loc.dao.dto.SeriesLoc;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.linktrain.LinkTrainEntity;
import com.gmail.alaerof.loc.entity.train.ComparatorByFirstDepatureTime;
import com.gmail.alaerof.loc.entity.train.ComparatorByLastArriveTime;
import com.gmail.alaerof.loc.entity.train.DistrGDP;
import com.gmail.alaerof.loc.entity.train.TrainThread;

import javax.swing.BoxLayout;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LinkLocDialog extends JDialog {
    private static Logger logger = LogManager.getLogger(LinkLocDialog.class);

    private class CheckBoxDistr {
        JCheckBox ch;
        Distr distr;
    }

    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JComboBox<Junc> juncSelect;
    private Junc selectedJunc;
    private JComboBox<SeriesLoc> locSelect;
    private SeriesLoc selectedLoc;
    private JTable arrTable;
    private JTable depTable;
    private ListArrTrainTableModel arrTableModel = new ListArrTrainTableModel();
    private ListDepTrainTableModel depTableModel = new ListDepTrainTableModel();
    private JPanel panelDistr;
    private List<CheckBoxDistr> checkDistr;
    private JPanel topPanel;
    private List<DistrGDP> listGDP = new ArrayList<>();
    private List<LinkTrainEntity> listLinkTrain = new ArrayList<>();
    private JuncTimePanel panelJuncTime;
    private JLabel locCount = new JLabel();
    private LinkPopupMenu linkPopupMenu = new LinkPopupMenu();

    public LinkLocDialog() {
        initContent();
    }

    public LinkLocDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private void initContent() {
        setBounds(100, 100, 1200, 700);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        setLocationRelativeTo(null);

        {
            topPanel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) topPanel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEFT);
            contentPanel.add(topPanel, BorderLayout.NORTH);

            JPanel panelJunc = new JPanel();
            topPanel.add(panelJunc);
            JLabel label = new JLabel("Узел:");
            panelJunc.add(label);
            juncSelect = new JComboBox<Junc>();
            panelJunc.add(juncSelect);

            juncSelect.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent aev) {
                    try {
                        int juncID = 0;
                        JuncTime jt = null;
                        selectedJunc = (Junc) juncSelect.getSelectedItem();
                        if (selectedJunc != null) {
                            juncID = selectedJunc.juncID;
                            jt = selectedJunc.getJuncTime();
                        }
                        fillListDistr(juncID);
                        panelJuncTime.setTime(jt);
                        clearTrainTables();
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });

            panelDistr = new JPanel();
            topPanel.add(panelDistr);
            panelDistr.setLayout(new BoxLayout(panelDistr, BoxLayout.Y_AXIS));

            JButton button = new JButton("Выбор");
            topPanel.add(button);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evn) {
                    try {
                        buildListDistrGDP();
                        fillTrainTables();
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });

            JPanel panelLoc = new JPanel();
            topPanel.add(panelLoc);
            JLabel labelL = new JLabel("Локомотив:");
            panelLoc.add(labelL);
            locSelect = new JComboBox<SeriesLoc>();
            panelLoc.add(locSelect);
            locSelect.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent aev) {
                    try {
                        selectedLoc = (SeriesLoc) locSelect.getSelectedItem();
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });

            panelJuncTime = new JuncTimePanel();
            topPanel.add(panelJuncTime);
        }
        {
            LinkLocDialog dialog = this;
            JPanel tablePanel = new JPanel();
            tablePanel.setLayout(new BorderLayout());
            CellDataTransferHelper helper = new CellDataTransferHelper(dialog);

            arrTable = new JTable(arrTableModel);
            arrTable.setFillsViewportHeight(true);
            arrTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSizeO[] = { 40, 90, 40, 40, 40, 90, 40 };
            com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(arrTable, colSizeO);
            arrTable.setDefaultRenderer(Integer.class, new JuncTimeCellRenderer(this));

            arrTable.setCellSelectionEnabled(true);
            arrTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            arrTable.setRowSelectionAllowed(false);
            arrTable.setColumnSelectionAllowed(false);

            arrTable.setDragEnabled(true);
            arrTable.setDropMode(DropMode.USE_SELECTION);
            arrTable.setTransferHandler(helper);

            arrTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent ev) {
                    maybeShowPopup(ev);
                }

                @Override
                public void mouseReleased(MouseEvent ev) {
                    maybeShowPopup(ev);
                }

                private void maybeShowPopup(MouseEvent ev) {
                    linkPopupMenu.row = arrTable.rowAtPoint(ev.getPoint());
                    if (linkPopupMenu.row > -1 && ev.isPopupTrigger()) {
                        linkPopupMenu.listTrain = arrTableModel;
                        linkPopupMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    }
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    try {
                        // удаление увязки
                        if (e.getClickCount() == 2) {
                            int row = arrTable.getSelectedRow();
                            if (row > -1) {
                                arrTableModel.deleteLinkTrain(row);
                                depTable.repaint();
                                arrTable.repaint();
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.toString(), ex);
                    }
                }
            });

            depTable = new JTable(depTableModel);
            depTable.setFillsViewportHeight(true);
            depTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSizeE[] = { 40, 90, 40, 40, 40, 90, 40 };
            com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(depTable, colSizeE);
            depTable.setDefaultRenderer(Integer.class, new JuncTimeCellRenderer(this));

            depTable.setCellSelectionEnabled(true);
            depTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            depTable.setRowSelectionAllowed(false);
            depTable.setColumnSelectionAllowed(false);

            depTable.setDragEnabled(true);
            depTable.setDropMode(DropMode.USE_SELECTION);
            depTable.setTransferHandler(helper);

            depTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent ev) {
                    maybeShowPopup(ev);
                }

                @Override
                public void mouseReleased(MouseEvent ev) {
                    maybeShowPopup(ev);
                }

                private void maybeShowPopup(MouseEvent ev) {
                    linkPopupMenu.row = depTable.rowAtPoint(ev.getPoint());
                    if (linkPopupMenu.row > -1 && ev.isPopupTrigger()) {
                        linkPopupMenu.listTrain = depTableModel;
                        linkPopupMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    }
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    try {
                        // удаление увязки
                        if (e.getClickCount() == 2) {
                            int row = depTable.getSelectedRow();
                            if (row > -1) {
                                depTableModel.deleteLinkTrain(row);
                                depTable.repaint();
                                arrTable.repaint();
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.toString(), ex);
                    }
                }
            });

            JScrollPane arrScroll = new JScrollPane(arrTable);
            arrScroll.setPreferredSize(new Dimension(580, 0));
            tablePanel.add(arrScroll, BorderLayout.WEST);

            JScrollPane depScroll = new JScrollPane(depTable);
            depScroll.setPreferredSize(new Dimension(580, 0));
            tablePanel.add(depScroll, BorderLayout.EAST);

            contentPanel.add(tablePanel, BorderLayout.CENTER);
        }

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog d = this;
            {
                buttonPane.add(locCount);
            }
            {
                JButton button = new JButton("Расчет");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent aev) {
                        try {
                            calcLinkTime();
                            depTable.repaint();
                            arrTable.repaint();
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
            {
                JButton button = new JButton("Сохранить");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent aev) {
                        try {
                            LocDAOFactory.getInstance().getLinkTrainDAO().saveListLinkTrainEntity(selectedJunc.juncID, listLinkTrain);
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
            {
                JButton button = new JButton("Отмена");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        d.setVisible(false);
                    }
                });

            }
        }
        fillContent();
    }

    /** расчет времени для увязок */
    protected void calcLinkTime() {
        int from = 0;
        int to = 0;
        for (LinkTrainEntity lte : listLinkTrain) {
            lte.calcTime(selectedJunc);
            if (lte.fromDepot()) {
                from++;
            }
            if (lte.toDepot()) {
                to++;
            }
        }
        locCount.setText("в депо = " + to + ", из депо = " + from);
    }

    protected void fillListDistr(int juncID) {
        panelDistr.removeAll();
        checkDistr = null;
        panelDistr.revalidate();

        List<Distr> list = LocDAOFactory.getInstance().getDistrDAO().loadListDistr(juncID);
        checkDistr = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            Distr distr = list.get(i);
            JCheckBox ch = new JCheckBox(distr.name);
            panelDistr.add(ch);
            CheckBoxDistr cbd = new CheckBoxDistr();
            cbd.ch = ch;
            cbd.distr = distr;
            checkDistr.add(cbd);
        }
        LinkLocUtil.listDistr = list;
        panelDistr.repaint();
    }

    protected void fillContent() {
        List<Junc> listJunc = LocDAOFactory.getInstance().getJuncDAO().loadAllJunc();
        Collections.sort(listJunc);
        juncSelect.removeAllItems();
        for (int i = 0; i < listJunc.size(); i++) {
            juncSelect.addItem(listJunc.get(i));
        }

        List<SeriesLoc> listLoc = LocDAOFactory.getInstance().getSeriesLocDAO().loadAllSLoc();
        Collections.sort(listLoc);
        locSelect.removeAllItems();
        for (int i = 0; i < listLoc.size(); i++) {
            locSelect.addItem(listLoc.get(i));
        }
        LinkLocUtil.listLoc = listLoc;
        linkPopupMenu.initContext();
    }

    /**
     * формирование списка ГДП выбранных участков
     */
    private void buildListDistrGDP() {
        // DistrGDP -> (DistrEntity + List<TrainThread>) ;
        // DistrEntity -> (distr + List<DistrSpan> + List<DistrStation>)
        // удаляем все ГДП из списка
        listGDP.clear();
        if (checkDistr != null) {
            int count = checkDistr.size();
            for (int i = 0; i < count; i++) {
                CheckBoxDistr cdistr = checkDistr.get(i);
                if (cdistr.ch.isSelected()) {
                    // создаем ГДП участка
                    DistrGDP gdp = new DistrGDP(cdistr.distr);
                    // загружаем ГДП из БД ;
                    gdp.loadGDPFromDB();
                    // добавляем ГДП в список
                    listGDP.add(gdp);
                }
            }
        }
        // все увязки по узлу
        listLinkTrain.clear();
        List<LinkTrain> listLink = null;
        if (selectedJunc != null) {
            listLink = LocDAOFactory.getInstance().getLinkTrainDAO().loadJuncLinkTrain(selectedJunc.juncID);
        }
        listLinkTrain = LinkLocUtil.getListEntity(listLink, listGDP);
    }

    /** удаляет все из таблиц прибытия и отправления и обновляет отображение */
    private void clearTrainTables() {
        List<TrainThread> listArr = new ArrayList<>();
        List<TrainThread> listDep = new ArrayList<>();
        arrTableModel.setList(listArr);
        depTableModel.setList(listDep);
        // обновить отображение
        arrTable.revalidate();
        arrTable.repaint();
        depTable.revalidate();
        depTable.repaint();
        locCount.setText("");
    }

    /** заполняет списки поездов в таблицах прибытия и отправления */
    private void fillTrainTables() {
        clearTrainTables();

        List<TrainThread> listArr = arrTableModel.getList();
        List<TrainThread> listDep = depTableModel.getList();

        if (selectedJunc != null) {
            for (int i = 0; i < listGDP.size(); i++) {
                DistrGDP gdp = listGDP.get(i);
                listArr.addAll(gdp.getArrived(selectedJunc));
                listDep.addAll(gdp.getDepatured(selectedJunc));
            }
        }
        Collections.sort(listArr, new ComparatorByLastArriveTime());
        Collections.sort(listDep, new ComparatorByFirstDepatureTime());
        arrTableModel.setList(listArr);
        depTableModel.setList(listDep);
        arrTableModel.setListLinkTrain(listLinkTrain);
        depTableModel.setListLinkTrain(listLinkTrain);

        // обновить отображение
        arrTable.revalidate();
        arrTable.repaint();
        depTable.revalidate();
        depTable.repaint();
    }

    public Junc getSelectedJunc() {
        return selectedJunc;
    }

    public SeriesLoc getSelectedLoc() {
        return selectedLoc;
    }

    public List<LinkTrainEntity> getListLinkTrain() {
        return listLinkTrain;
    }

    public void updateData() {
        buildListDistrGDP();
        fillTrainTables();
    }

    private class LinkPopupMenu extends JPopupMenu {
        private static final long serialVersionUID = 1L;
        public ListTrain listTrain;
        public int row;

        public void initContext() {
            this.removeAll();
            {
                JMenuItem item = new JMenuItem("Удалить");
                Font font = item.getComponent().getFont();
                Font newFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
                item.getComponent().setFont(newFont);
                this.add(item);

                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        listTrain.deleteLinkTrain(row);
                        depTable.repaint();
                        arrTable.repaint();
                    }
                });
            }
            {
                JMenuItem item = new JMenuItem("В/Из депо");
                Font font = item.getComponent().getFont();
                Font newFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
                item.getComponent().setFont(newFont);
                this.add(item);
                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        Junc junc = getSelectedJunc();
                        SeriesLoc loc = getSelectedLoc();
                        List<LinkTrainEntity> list = getListLinkTrain();
                        TrainThread tr = listTrain.getList().get(row);
                        if (listTrain.isArrive()) {
                            LinkLocUtil.setLinkTrainEntity(list, tr, null, junc, loc);
                        } else {
                            LinkLocUtil.setLinkTrainEntity(list, null, tr, junc, loc);
                        }
                        depTable.repaint();
                        arrTable.repaint();
                    }
                });
            }
            for (final SeriesLoc series : LinkLocUtil.listLoc) {
                {
                    JMenuItem item = new JMenuItem(series.series);
                    this.add(item);
                    item.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            LinkTrainEntity link = listTrain.getLinkTrainEntity(row);
                            if (link != null) {
                                link.getLinkTrain().seriesLocID = series.seriesLocID;
                                link.getLinkTrain().locName = series.series;
                                depTable.repaint();
                                arrTable.repaint();
                            }
                        }
                    });
                }
            }
        }
    }
}
