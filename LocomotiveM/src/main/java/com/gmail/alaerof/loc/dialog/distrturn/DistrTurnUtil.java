package com.gmail.alaerof.loc.dialog.distrturn;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.gmail.alaerof.loc.dao.dto.DistrTurn;
import com.gmail.alaerof.loc.dao.dto.DistrTurnResult;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.JuncSt;
import com.gmail.alaerof.loc.dao.dto.RangeTR;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.DistrSpan;
import com.gmail.alaerof.loc.entity.train.TrainThread;
import com.gmail.alaerof.loc.application.LinkingLocMain;
import com.gmail.alaerof.loc.util.FileHandler;

public class DistrTurnUtil {
    protected static Logger logger = LogManager.getLogger(DistrTurnUtil.class);

    public void calculate(List<DistrTurn> list) {
        StringBuilder warn = new StringBuilder();
        for (DistrTurn dt : list) {
            //System.out.println("---" + dt.name + "---");
            List<DistrTurnResult> listRes = new ArrayList<>();
            // список поездов родительского ж.д. участка
            List<TrainThread> listTrain = LocDAOFactory.getInstance().getDistrTrainDAO()
                    .loadGDP(dt.distrID, new TrainThread());
            // список перегонов для участка обращения
            List<DistrSpan> listSpan = LocDAOFactory.getInstance().getSpanDAO()
                    .loadDistrTurnSpan(dt.distrTurnID);
            // список диапазонов поездов для участка обращения
            List<RangeTR> listRange = LocDAOFactory.getInstance().getDistrTurnDAO()
                    .loadRangeDistrTurn(dt.distrTurnID);
            // узлы соотв. ж.д.участка
            Junc[] juncs = new Junc[2];
            setDistrJunc(dt.distrID, juncs);
            Junc juncB = getJunc(dt.stBID, juncs);
            Junc juncE = getJunc(dt.stEID, juncs);
            //System.out.println("jB=" + juncB);
            //System.out.println("jE=" + juncE);
            int idSt = 0; // ид спорной станции (но только 1 станция должна быть
                          // не узловая, иначе что-нить посчитается дважды полюбак)
            if (juncB == null) {
                idSt = dt.stBID;
            }
            if (juncE == null) {
                idSt = dt.stEID;
            }

            List<LinkTrainCouple> listCouple = new ArrayList<>();
            getLinkedTrains(dt.distrID, listTrain, listCouple);
            for (RangeTR range : listRange) {
                List<TrainThread> listTR = filterTrainsByRange(range, listTrain);
                int[] countAll = calcCountTrain(listTR);

                // список пар (поезд, увязка) для заданного диапазона номеров
                List<LinkTrainCouple> listCPR = filterCouplesByRange(range, listCouple);
                //System.out.println("- linked - " + range.toString());

                // теперь нужно получить список локомотивов из этого списка пар
                List<Integer> listLocID = getSeriesLocID(listCPR);
                // а вот теперь для каждого локомотива считаем показатели
                // т.к. получили нужное сужение: поезда -> диапазоны -> увязки
                // -> серии локомотивов
                for (Integer locID : listLocID) {
                    DistrTurnResult res = new DistrTurnResult();
                    res.distrTurnID = dt.distrTurnID;
                    res.rangeTRID = range.rangeTRID;
                    res.seriesLocID = locID;
                    // количество четных, нечетных поездов всего на участке
                    // оборота[=ж.д.участке] для заданного диапазона
                    res.countTrain_o = countAll[0];
                    res.countTrain_e = countAll[1];
                    // все остальные показатели
                    calcRes(res, listCPR, listSpan, idSt, (idSt == dt.stEID), juncB, juncE, warn);
                    listRes.add(res);
                }
            }

            LocDAOFactory.getInstance().getDistrTurnDAO().saveDistrTurnResult(listRes);
        }
    }

    /**
     * расчет показателей
     * @param res объект результата расчета
     * @param listCPR список пар (поезд, увязка)
     * @param listSpan список перегонов участка обророта (для расчета поездо-км и поездо-час)
     * @param idSt ид спорной станции (не узловая станция на ж.д. участке)
     * @param includeSt включать ли время по спорной станции в расчет участковых поездо-минут
     * @param juncB начальный узел участка обращения (может быть Null)
     * @param juncE конечный узел участка обращения (может быть Null)
     * @param warn предупреждения при расчете
     */
    private void calcRes(DistrTurnResult res, List<LinkTrainCouple> listCPR, List<DistrSpan> listSpan,
            int idSt, boolean includeSt, Junc juncB, Junc juncE, StringBuilder warn) {
        System.out.println("locID = " + res.seriesLocID);
        for (LinkTrainCouple cp : listCPR) {
            // если это увязка нужным локомотивом
            if (cp.isLoc(res.seriesLocID)) {
                // если у поезда нет перегонов, значит нужно произвести расчет
                // показателей на листе, а лучше не всех
                if (cp.getTrain().getTrainSp().size() == 0) {
                    String w = "не предварительных расчетов на поезду " + cp.getTrain().getCode() + "("
                            + cp.getTrain().getDistrTrain().nameTR + ")";
                    logger.error(w);
                    warn.append(w);
                }

                // технические и участковые поездо-минуты и поездо-метры
                int trainMin[] = cp.getTrain().calculateTrainMinutes(listSpan, idSt, includeSt);
                // время в узлах
                int tmLink[] = cp.getLinkTm(juncB, juncE);
                int tmDep = tmLink[0]; // cp.getTmArr(juncE);
                int tmArr = tmLink[1]; // cp.getTmDep(juncB);
                int tmEq = tmLink[2]; // cp.getTmEq(juncB, juncE);

                System.out.println("" + cp.getTrain().getCode() + " tt=" + trainMin[0] + " tu=" + trainMin[1]
                        + " arr="
                        + tmArr + " dep=" + tmDep + " eq=" + tmEq);

                if (cp.getTrain().isOdd()) {
                    res.countLoc_o++; // количество локомотивов (увязанных
                                      // поездов)
                    res.hourTT_o += ((double) trainMin[0]) / 60;
                    res.hourTU_o += ((double) trainMin[1]) / 60;
                    res.hourArr_o += ((double) tmArr) / 60;
                    res.hourDep_o += ((double) tmDep) / 60;
                    res.hourEq_o += ((double) tmEq) / 60;
                    res.trkm_o += ((double) trainMin[2]) / 1000;
                } else {
                    res.countLoc_e++; // количество локомотивов (увязанных
                                      // поездов)
                    res.hourTT_e += ((double) trainMin[0]) / 60;
                    res.hourTU_e += ((double) trainMin[1]) / 60;
                    res.hourArr_e += ((double) tmArr) / 60;
                    res.hourDep_e += ((double) tmDep) / 60;
                    res.hourEq_e += ((double) tmEq) / 60;
                    res.trkm_e += ((double) trainMin[2]) / 1000;
                }
            }
        }
    }

    /**
     * количество четных, нечетных поездов всего
     * @param list список поездов
     * @return массив [к-во нечет, к-во чет]
     */
    private int[] calcCountTrain(List<TrainThread> list) {
        int o = 0;
        int e = 0;
        for (TrainThread tr : list) {
            if (tr.isEven())
                e++;
            else
                o++;
        }
        int count[] = { o, e };
        return count;
    }

    /**
     * возвращает (distinct) список ид локомотивов из заданного списка увязок
     * @param listCPR список пар(поезд, увязка)
     * @return список ид локомотивов
     */
    private List<Integer> getSeriesLocID(List<LinkTrainCouple> listCPR) {
        List<Integer> list = new ArrayList<>();
        for (LinkTrainCouple cp : listCPR) {
            cp.fillLocID(list);
        }
        return list;
    }

    /**
     * выбирает из списка поездов участка увязанные поезда и заполняет список
     * пар (увязки, поезд)
     * 
     * @param distrID
     *            ид ж.д. участка
     * @param listAll
     *            список всех поездов ж.д.участка
     * @param listCouple
     *            список пар (увязка, поезд) для заполнения
     * @return cписок увязанных поездов
     */
    private void getLinkedTrains(int distrID, List<TrainThread> listAll, List<LinkTrainCouple> listCouple) {
        List<LinkTrain> listLink = LocDAOFactory.getInstance().getLinkTrainDAO().loadDistrLinkTrain(distrID);

        for (TrainThread tr : listAll) {
            List<LinkTrain> links = new ArrayList<>();
            for (LinkTrain link : listLink) {
                if ((tr.getCodeINT() == link.codeTRon) && (tr.getDistrID() == link.distrIDon)) {
                    links.add(link);
                }
                if ((tr.getCodeINT() == link.codeTRoff) && (tr.getDistrID() == link.distrIDoff)) {
                    links.add(link);
                }
            }
            if (links.size() > 0) {
                LinkTrainCouple cp = new LinkTrainCouple();
                cp.setLink(links);
                cp.setTrain(tr);
                listCouple.add(cp);
            }
        }
    }

    /**
     * возвращает начальный и конечный узел ж.д. участка, но начало и конец
     * может не совпадать с началом и концом участка оборота, впрочем как и
     * направление
     * 
     * @param distrID
     *            ид ж.д.участка
     * @param juncs
     *            массив на 2 узла
     */
    private void setDistrJunc(int distrID, Junc[] juncs) {
        Junc jB = LocDAOFactory.getInstance().getJuncDAO().getJuncB(distrID);
        Junc jE = LocDAOFactory.getInstance().getJuncDAO().getJuncE(distrID);
        juncs[0] = jB;
        juncs[1] = jE;
    }

    /**
     * выбирает из двух (в лучшем случае) узлов ж.д.участка тот, который
     * соответствует ид начальной или конечной станции участка оборота
     * 
     * @param stID
     *            ид начальной или конечной станции участка оборота
     * @param juncs
     *            массив из 2-х узлов ж.д.участка
     * @return начальный узел участка оборота или null
     */
    private Junc getJunc(int stID, Junc[] juncs) {
        for (int i = 0; i < juncs.length; i++) {
            Junc junc = juncs[i];
            if (junc != null) {
                for (JuncSt jst : junc.getListSt()) {
                    if (jst.stationID == stID) {
                        return junc;
                    }
                }
            }
        }
        return null;
    }

    /**
     * возвращает список поездов из заданного списка в соответствии с заданным
     * диапазоном номеров
     * 
     * @param range
     *            диапазон поездов
     * @param listTrain
     *            список всех поездов родительского ж.д.участка
     * @return список поездов для диапазона
     */
    private List<TrainThread> filterTrainsByRange(RangeTR range, List<TrainThread> listTrain) {
        List<TrainThread> listTR = new ArrayList<>();
        List<RangeTR.Range> listRR = range.getListRange();
        for (TrainThread tr : listTrain) {
            for (RangeTR.Range rr : listRR) {
                int codeTR = tr.getCodeINT();
                if (codeTR >= rr.trMin && codeTR <= rr.trMax) {
                    listTR.add(tr);
                }
            }
        }
        return listTR;
    }

    /**
     * список пар (поезд, увязка) для заданного диапазона
     * 
     * @param range диапазон номеров поездов
     * @param listCoupleAll общий список пар (поезд, увязка)
     * @return отфильтрованный список
     */
    private List<LinkTrainCouple> filterCouplesByRange(RangeTR range, List<LinkTrainCouple> listCoupleAll) {
        List<LinkTrainCouple> listCouple = new ArrayList<>();
        List<RangeTR.Range> listRR = range.getListRange();
        for (LinkTrainCouple cp : listCoupleAll) {
            for (RangeTR.Range rr : listRR) {
                int codeTR = cp.getTrain().getCodeINT();
                if (codeTR >= rr.trMin && codeTR <= rr.trMax) {
                    listCouple.add(cp);
                }
            }
        }
        return listCouple;
    }

    // protected void printTrains(List<TrainThread> list) {
    // int o = 0;
    // int e = 0;
    // for (TrainThread tr : list) {
    // if (tr.isEven())
    // e++;
    // else
    // o++;
    // System.out.println(tr.getCode());
    // }
    // System.out.println("нечет = " + o + ", чет = " + e);
    // }

    // protected void printCouples(List<LinkTrainCouple> list) {
    // int o = 0;
    // int e = 0;
    // for (LinkTrainCouple cp : list) {
    // if (cp.getTrain().isEven())
    // e++;
    // else
    // o++;
    // System.out.println(cp.getTrain().getCode());
    // }
    // System.out.println("нечет = " + o + ", чет = " + e);
    // }

    public void exportDistrTurnToExcel(int dirID, String fileName) {
        String sampleFileName = "samples//distrturnres.xls";
        fileName = FilenameUtils.removeExtension(fileName) + ".xls";
        File file = FileHandler.chooseFile(false, new FileNameExtensionFilter("Excel",
                "xls"), fileName, LinkingLocMain.mainFrame);

        if (file != null) {
            try {
                String filePath = file.getAbsolutePath();
                String ff = FilenameUtils.removeExtension(filePath) + ".xls";
                file = new File(ff);

                FileInputStream input = new FileInputStream(new File(sampleFileName));
                Workbook wb = WorkbookFactory.create(input);
                Sheet sheet = wb.getSheetAt(0);

                int rowIndx = 5;
                List<DistrTurn> listDT = new ArrayList<>();
                List<DistrTurnResult> listRes = LocDAOFactory.getInstance().getDistrTurnDAO()
                        .getDistrTurnResult(dirID, listDT);
                for (int i = 0; i < listRes.size(); i++) {
                    DistrTurnResult dtr = listRes.get(i);
                    DistrTurn dt = listDT.get(i);

                    Row row = sheet.getRow(rowIndx);
                    row.getCell(0).setCellValue(dtr.nameDT);
                    row.getCell(2).setCellValue(dtr.nameR);
                    row.getCell(3).setCellValue(dtr.series);

                    row.getCell(5).setCellValue(dt.gradientforw);
                    row.getCell(6).setCellValue(dt.gradientback);
                    row.getCell(7).setCellValue(dt.weightforw);
                    row.getCell(8).setCellValue(dt.weightback);
                    row.getCell(9).setCellValue(dt.lenght);

                    row.getCell(10).setCellValue(dtr.countLoc_o);
                    row.getCell(11).setCellValue(dtr.countLoc_e);
                    row.getCell(12).setCellValue(dtr.countTrain_o);
                    row.getCell(13).setCellValue(dtr.countTrain_e);
                    row.getCell(14).setCellValue(dtr.hourTT_o);
                    row.getCell(15).setCellValue(dtr.hourTT_e);
                    row.getCell(16).setCellValue(dtr.hourTU_o);
                    row.getCell(17).setCellValue(dtr.hourTU_e);
                    row.getCell(18).setCellValue(dtr.trkm_o);
                    row.getCell(19).setCellValue(dtr.trkm_e);

                    if (dtr.hourTT_o > 0)
                        row.getCell(20).setCellValue(dtr.trkm_o / dtr.hourTT_o);
                    if (dtr.hourTT_e > 0)
                        row.getCell(21).setCellValue(dtr.trkm_e / dtr.hourTT_e);
                    if (dtr.hourTU_o > 0)
                        row.getCell(22).setCellValue(dtr.trkm_o / dtr.hourTU_o);
                    if (dtr.hourTU_e > 0)
                        row.getCell(23).setCellValue(dtr.trkm_e / dtr.hourTU_e);

                    row.getCell(27).setCellValue(dtr.hourDep_o);
                    row.getCell(28).setCellValue(dtr.hourDep_e);
                    row.getCell(29).setCellValue(dtr.hourArr_o);
                    row.getCell(30).setCellValue(dtr.hourArr_e);
                    row.getCell(31).setCellValue(dtr.hourEq_o);
                    row.getCell(32).setCellValue(dtr.hourEq_e);
                    rowIndx++;
                }

                FileOutputStream fileOut = new FileOutputStream(file);
                wb.write(fileOut);
                fileOut.close();
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }
}
