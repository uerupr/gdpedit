package com.gmail.alaerof.loc.dao.dto;

public class Distr extends AbstractDTO implements Comparable<Distr>{
    public int distrID;
    public int dirID;
    public String name;
    public boolean electric;
    public String fileName;
    // --- from DistrJunc ---
    public int juncB;
    public int juncE;
    // --- add ---
    /** может быть NULL !!! */
    private Junc juncBFull;
    /** может быть NULL !!! */
    private Junc juncEFull;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int getID() {
        return distrID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + dirID;
        result = prime * result + distrID;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Distr other = (Distr) obj;
        if (dirID != other.dirID)
            return false;
        if (distrID != other.distrID)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public Junc getJuncBFull() {
        return juncBFull;
    }

    public void setJuncBFull(Junc juncBFull) {
        if (juncBFull != null) {
            if (juncBFull.juncID == 0)
                juncBFull = null;
        }
        this.juncBFull = juncBFull;
    }

    public Junc getJuncEFull() {
        return juncEFull;
    }

    public void setJuncEFull(Junc juncEFull) {
        if (juncEFull != null) {
            if (juncEFull.juncID == 0)
                juncEFull = null;
        }
        this.juncEFull = juncEFull;
    }

    @Override
    public int compareTo(Distr o) {
        return this.name.compareToIgnoreCase(o.name);
    }

}
