package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.DistrTurn;
import com.gmail.alaerof.loc.dao.dto.Scale;
import com.gmail.alaerof.loc.dao.dto.Span;
import com.gmail.alaerof.loc.entity.DistrSpan;

public class SpanDAO {
    private static Logger logger = LogManager.getLogger(SpanDAO.class);
    private Connection cnLocal;

    public SpanDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    public int insertListSpan(List<Span> list) {
        String sql = "INSERT INTO Span (spanID, dirID, name, SCB, line, stB, stE, br)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Span item = list.get(i);
                st.setInt(1, item.spanID);
                if (item.dirID > 0) {
                    st.setInt(2, item.dirID);
                } else {
                    st.setNull(2, java.sql.Types.NULL);
                }
                st.setString(3, item.name);
                st.setString(4, item.SCB);
                st.setInt(5, item.line);
                st.setInt(6, item.stB);
                st.setInt(7, item.stE);
                st.setBoolean(8, item.br);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    public int updateListSpan(List<Span> list) {
        String sql = "UPDATE Span SET dirID=?, name=?, SCB=?, line=?, stB=?, stE=?, br=? "
                + " WHERE spanID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Span item = list.get(i);
                if (item.dirID > 0) {
                    st.setInt(1, item.dirID);
                } else {
                    st.setNull(1, java.sql.Types.NULL);
                }
                st.setString(2, item.name);
                st.setString(3, item.SCB);
                st.setInt(4, item.line);
                st.setInt(5, item.stB);
                st.setInt(6, item.stE);
                st.setBoolean(7, item.br);
                st.setInt(8, item.spanID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    public int insertListScale(List<Scale> list) {
        String sql = "INSERT INTO Scale (scaleID, distrID, spanID, num, L, absKM, absKM_e)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Scale item = list.get(i);
                st.setInt(1, item.scaleID);
                st.setInt(2, item.distrID);
                st.setInt(3, item.spanID);
                st.setInt(4, item.num);
                st.setInt(5, item.L);
                st.setDouble(6, item.absKM);
                st.setDouble(7, item.absKM_e);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    public int updateListScale(List<Scale> list) {
        String sql = "UPDATE Scale SET distrID = ?, spanID=?, num=?, L=?, absKM=?, absKM_e=? "
                + " WHERE scaleID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Scale item = list.get(i);
                st.setInt(1, item.distrID);
                st.setInt(2, item.spanID);
                st.setInt(3, item.num);
                st.setInt(4, item.L);
                st.setDouble(5, item.absKM);
                st.setDouble(6, item.absKM_e);
                st.setInt(7, item.scaleID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    private void fillSpanItem(ResultSet rs, Span item) throws SQLException {
        item.spanID = rs.getInt("spanID");
        item.dirID = rs.getInt("dirID");
        item.name = rs.getString("name").trim();
        String scb = rs.getString("SCB");
        if (scb != null)
            scb = scb.trim();
        item.SCB = scb;
        item.line = rs.getInt("line");
        item.stB = rs.getInt("stB");
        item.stE = rs.getInt("stE");
        item.br = rs.getBoolean("br");
    }

    public List<Span> loadAllSpan() {
        List<Span> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT spanID, dirID, name, SCB, line, stB, stE, br  FROM Span";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Span item = new Span();
                    fillSpanItem(rs, item);
                    list.add(item);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    private void fillScaleItem(ResultSet rs, Scale item, boolean readSpanID) throws SQLException {
        item.scaleID = rs.getInt("scaleID");
        item.distrID = rs.getInt("distrID");
        if (readSpanID) {
            item.spanID = rs.getInt("spanID");
        }
        item.num = rs.getInt("num");
        item.L = rs.getInt("L");
        item.absKM = rs.getDouble("absKM");
        item.absKM_e = rs.getDouble("absKM_e");
    }

    public List<Scale> loadAllScale() {
        List<Scale> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT scaleID, distrID, spanID, num, L, absKM, absKM_e  FROM Scale";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Scale item = new Scale();
                    fillScaleItem(rs, item, true);
                    list.add(item);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    public List<DistrSpan> loadDistrSpan(int distrID) {
        List<DistrSpan> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT Span.spanID, dirID, name, SCB, line, stB, stE, br,"
                    + " scaleID, distrID, num, L, absKM, absKM_e"
                    + " FROM Span INNER JOIN Scale ON Span.spanID = Scale.spanID WHERE distrID = " + distrID
                    + " ORDER BY num ";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Span sp = new Span();
                    Scale sc = new Scale();
                    fillSpanItem(rs, sp);
                    fillScaleItem(rs, sc, false);
                    sc.spanID = sp.spanID;
                    DistrSpan ds = new DistrSpan(sp, sc);
                    list.add(ds);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    public List<DistrSpan> loadDistrTurnSpan(int distrTurnID) {
        List<DistrSpan> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT Span.spanID, dirID, name, SCB, line, stB, stE, br,"
                    + " scaleID, distrID, num, L, absKM, absKM_e"
                    + " FROM Span INNER JOIN Scale ON Span.spanID = Scale.spanID "
                    + " WHERE scaleID IN (SELECT scaleID FROM DistrTurnScale WHERE distrTurnID = "
                    + distrTurnID + ") "
                    + " ORDER BY num ";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Span sp = new Span();
                    Scale sc = new Scale();
                    fillSpanItem(rs, sp);
                    fillScaleItem(rs, sc, false);
                    sc.spanID = sp.spanID;
                    DistrSpan ds = new DistrSpan(sp, sc);
                    list.add(ds);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**
     * удаляет все перегоны участка обращения
     * @param distrTurn Участок обращения
     * @return
     */
    private int deleteDistrTurnSpan(DistrTurn distrTurn) {
        String sql = "DELETE FROM DistrTurnScale WHERE distrTurnID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, distrTurn.distrTurnID);
            count = st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    /**
     * сохраняет список перегонов для участка обращения,
     * предварительно удаляя все перегоны участка обращения
     * @param distrTurn участок обращения
     * @param listDistrSpan список перегонов
     */
    public int saveDistrTurnSpan(DistrTurn distrTurn, List<DistrSpan> listDistrSpan) {
        deleteDistrTurnSpan(distrTurn);
        String sql = "INSERT INTO DistrTurnScale (distrTurnID, scaleID) VALUES(?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < listDistrSpan.size(); i++) {
                DistrSpan ds = listDistrSpan.get(i);
                st.setInt(1, distrTurn.distrTurnID);
                st.setInt(2, ds.getScale().scaleID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

}
