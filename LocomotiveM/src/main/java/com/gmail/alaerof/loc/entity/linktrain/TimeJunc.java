package com.gmail.alaerof.loc.entity.linktrain;

import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.train.TrainThread;
import com.gmail.alaerof.loc.util.TimeConverter;

public class TimeJunc {
    /** расписание по станции поезда прибыти¤ */
    private TrainStation trainStOn;
    /** расписание по станции поезда отправлени¤ */
    private TrainStation trainStOff;
    /** вместо прибывающего депо (т.е. это выезд из депо) */
    private boolean depotOn;
    /** вместо отправл¤ющегос¤ депо (т.е. это возвращение в депо) */
    private boolean depotOff;
    /** врем¤ в узле */
    private int juncTm;

    /** прибытие в минутах */
    int mOn;
    /** отправление в минутах */
    int mOff;
    /** коорд по х прибыти¤ на станцию */
    int xOn;
    /** коорд по х отправлени¤ со станции */
    int xOff;

    public TimeJunc(TrainThread trainOn, TrainThread trainOff, LinkTrain linkTrain) {
        if (trainOn != null) {
            trainStOn = trainOn.getLastSt();
        }
        if (trainOff != null) {
            trainStOff = trainOff.getFirstSt();
        }
        // null может быть только 1 из них !!!
        if (trainStOn == null) {
            trainStOn = trainStOff;
        }
        if (trainStOff == null) {
            trainStOff = trainStOn;
        }

        juncTm = linkTrain.tm;
        // выезд из депо
        depotOn = linkTrain.distrIDon == -1 && trainOn == null;
        // возвращение в депо
        depotOff = linkTrain.distrIDoff == -1 && trainOff == null;
        setTimes();
    }
    
    private void setTimes(){
        mOn = trainStOn.getTimeOnChecked();
        mOff = trainStOff.getTimeOffChecked();
        // выезд из депо
        if (depotOn) {
            mOn = TimeConverter.addMinutes(mOff, -juncTm);
        }
        // возвращение в депо
        if (depotOff) {
            mOff = TimeConverter.addMinutes(mOn, juncTm);
        }        
    }

    /** расчет горизонтальных координат прибыти¤ и отправлени¤ */
    public void calcX(SheetConfig sConf) {
        int x0 = sConf.ltGDP.x;
        setTimes();
        xOn = x0 + mOn * sConf.picTimeHor;
        xOff = x0 + mOff * sConf.picTimeHor;
    }

}
