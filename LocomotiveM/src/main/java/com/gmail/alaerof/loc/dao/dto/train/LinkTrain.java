package com.gmail.alaerof.loc.dao.dto.train;

public class LinkTrain {
    public int linkTrainID = -1;
    public int juncID;
    public int seriesLocID;
    /**
     * если distrIDon == -1 значит, что это выезд ИЗ ДЕПО
     * и время увязки берется из JuncTime.depart
     * при этом нелья, чтобы distrIDoff было = -1
     */
    public int distrIDon;
    public int codeTRon;
    /**
     * если distrIDoff == -1 значит, что это возвращение В ДЕПО
     * и время увязки берется из JuncTime.arrive
     * при этом нелья, чтобы distrIDon было = -1
     */
    public int distrIDoff;
    public int codeTRoff;
    public String nameTRon;
    public String nameTRoff;
    public int lineNumber;
    public int tm;
    // ------ add ------
    public String distrNameOn = "";
    public String distrNameOff = "";
    public String locName = "";

    public void setNameTRon(String name) {
        if (name != null) {
            name = name.trim();
        } else {
            name = "";
        }
        nameTRon = name;
    }

    public void setNameTRoff(String name) {
        if (name != null) {
            name = name.trim();
        } else {
            name = "";
        }
        nameTRoff = name;
    }

    public String getCaptionValue() {
        String caption = "" + codeTRon;
        if (codeTRon != codeTRoff) {
            caption = caption + " - " + codeTRoff;
        }
        return caption;
    }

    public String getToolTipText() {
        String text = "" + codeTRon;
        if (codeTRon != codeTRoff) {
            text = text + " - " + codeTRoff;
        }

        text = text + " [" + distrNameOn;
        if (distrIDon != distrIDoff) {
            text = text + ", " + distrNameOff;
        }
        text = text + "]";
        return text;
    }

    @Override
    public String toString() {
        return "linktrain [juncID=" + juncID + ", seriesLocID=" + seriesLocID + ", distrIDon=" + distrIDon + ", codeTRon=" + codeTRon
                + ", distrIDoff=" + distrIDoff + ", codeTRoff=" + codeTRoff + ", nameTRon=" + nameTRon + ", nameTRoff=" + nameTRoff
                + ", lineNumber=" + lineNumber + "]";
    }
    
    
}
