package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.SeriesLoc;

public class SeriesLocDAO {
    private static Logger logger = LogManager.getLogger(SeriesLocDAO.class);
    private Connection cnLocal;

    public SeriesLocDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    public List<SeriesLoc> loadAllSLoc() {
        List<SeriesLoc> listE = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT seriesLocID, series, electr FROM SeriesLoc ORDER BY series";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    SeriesLoc ent = new SeriesLoc();
                    ent.seriesLocID = rs.getInt("seriesLocID");
                    ent.series = rs.getString("series").trim();
                    ent.electr = rs.getBoolean("electr");
                    listE.add(ent);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return listE;
    }

    public void addSeriesLoc(SeriesLoc seriesLoc) {
        if (seriesLoc != null) {
            String sql = "INSERT INTO SeriesLoc(series, electr) VALUES (?, ?)";
            try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
                st.setString(1, seriesLoc.series);
                st.setBoolean(2, seriesLoc.electr);
                st.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }
        }
    }
    
    public void updateSeriesLoc(SeriesLoc seriesLoc){
        if (seriesLoc != null) {
            String sql = "UPDATE SeriesLoc SET series = ?, electr = ?) WHERE seriesLocID = " + seriesLoc.seriesLocID;
            try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
                st.setString(1, seriesLoc.series);
                st.setBoolean(2, seriesLoc.electr);
                st.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }
        }
    }
    
    public void deleteSeriesLoc(int seriesLocID){
        String sql = "DELETE FROM SeriesLoc WHERE seriesLocID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, seriesLocID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void deleteSeriesLoc(List<SeriesLoc> list){
        for(SeriesLoc sl: list){
            deleteSeriesLoc(sl.seriesLocID);
        }
    }

    public String getNameLoc(List<SeriesLoc> listL, int seriesLocID) {
        for(SeriesLoc loc:listL){
            if(loc.seriesLocID == seriesLocID){
                return loc.series.trim();
            }
        }
        return "";
    }    
    
    public SeriesLoc getLocByID(List<SeriesLoc> list, int id) {
        for(SeriesLoc loc:list){
            if(loc.seriesLocID == id){
                return loc;
            }
        }
        return null;
    }    
    
    public SeriesLoc getLocByName(List<SeriesLoc> list, String name) {
        for(SeriesLoc loc:list){
            if(loc.series.trim().equals(name.trim())){
                return loc;
            }
        }
        return null;
    }
}
