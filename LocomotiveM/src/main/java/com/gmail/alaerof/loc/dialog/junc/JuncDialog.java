package com.gmail.alaerof.loc.dialog.junc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.JTextComponent;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.Junc.JuncType;
import com.gmail.alaerof.loc.dao.dto.JuncSt;
import com.gmail.alaerof.loc.dao.dto.JuncTime;
import com.gmail.alaerof.loc.dao.dto.Station;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.JuncDAO;
import com.gmail.alaerof.loc.application.LinkingLocMain;
import com.gmail.alaerof.loc.util.FileHandler;
import com.gmail.alaerof.loc.xml.LinkTrainXML;

public class JuncDialog extends JDialog {
    private static Logger logger = LogManager.getLogger(JuncDialog.class);
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTable juncTable;
    private JTable juncStTable;
    private JuncTableModel juncTableModel;
    private JuncStTableModel juncStTableModel;
    private List<Junc> juncList = new ArrayList<>();
    private JComboBox<Station> stSelect;
    private Junc selectedJunc;
    private List<Junc> juncListDel = new ArrayList<>();
    private int juncOrder = 0; // 0 - name, 1 - ESR, 2 - type, 3 - dir
    private JTextField jtArrive = new JTextField(15);
    private JTextField jtDepart = new JTextField(15);
    private JTextField jtEquipment = new JTextField(15);

    public JuncDialog() {
        initContent();
    }

    public JuncDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    public void loadData() {
        LocDAOFactory f = LocDAOFactory.getInstance();
        juncList = f.getJuncDAO().loadAllJunc();
        sortListJunc();
        juncTableModel.setList(juncList);
        juncTable.repaint();
        fillStSelect();
    }

    private void sortListJunc() {
        // 0 - name, 1 - ESR, 2 - type, 3 - dir
        switch (juncOrder) {
        case 0:
            Collections.sort(juncList);
            break;
        case 1:
            Collections.sort(juncList, new JuncComparatorESR());
            break;
        case 2:
            Collections.sort(juncList, new JuncComparatorType());
            break;
        case 3:
            Collections.sort(juncList, new JuncComparatorDir());
            break;
        default:
            break;
        }
    }

    private void initContent() {
        setBounds(0, 0, 920, 400);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);

        JPanel juncPanel = new JPanel();
        juncPanel.setLayout(new BorderLayout());
        contentPanel.add(juncPanel, BorderLayout.CENTER);
        {
            juncTableModel = new JuncTableModel();
            juncTable = new JTable(juncTableModel);
            JScrollPane juncScroll = new JScrollPane(juncTable);
            juncPanel.add(juncScroll, BorderLayout.CENTER);
            juncTable.setFillsViewportHeight(true);
            juncTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 45, 80, 45, 45, 45 };
            com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(juncTable, colSize);

            juncTable.setDefaultEditor(JuncType.class, new JuncTypeCellEditor());
            juncTable.setDefaultEditor(Direction.class, new JuncDirCellEditor());

            juncTable.getTableHeader().addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent ev) {
                    try {
                        int col = juncTable.columnAtPoint(ev.getPoint());
                        String name = juncTable.getColumnName(col);
                        // 0 - name, 1 - ESR, 2 - type, 3 - dir
                        if ("Название".equals(name))
                            juncOrder = 0;
                        if ("Код ЕСР".equals(name))
                            juncOrder = 1;
                        if ("Тип".equals(name))
                            juncOrder = 2;
                        if ("НОД".equals(name))
                            juncOrder = 3;
                        sortListJunc();
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });

            juncScroll.setPreferredSize(new Dimension(360, 0));
        }
        {
            JPanel juncActPanel = new JPanel();
            juncActPanel.setPreferredSize(new Dimension(0, 40));
            FlowLayout flowLayout = (FlowLayout) juncActPanel.getLayout();
            flowLayout.setAlignment(FlowLayout.RIGHT);
            juncPanel.add(juncActPanel, BorderLayout.NORTH);
            {
                JButton b = new JButton("+");
                juncActPanel.add(b);
                b.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Junc jn = new Junc();
                        jn.name = "-";
                        jn.codeESR = "0";
                        jn.setListSt(new ArrayList<JuncSt>());
                        sortListJunc();
                        if (!juncList.contains(jn))
                            juncList.add(jn);
                        juncTable.repaint();
                    }
                });
            }
            {
                JButton b = new JButton("-");
                juncActPanel.add(b);
                b.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        int index = juncTable.getSelectedRow();
                        if (index > -1) {
                            Junc jn = juncList.remove(index);
                            juncTable.repaint();
                            juncListDel.add(jn);
                        }
                    }
                });
            }
        }

        JPanel juncStPanel = getStPanel();
        JPanel juncTimePanel = getTimePanel();
        JTabbedPane tabPane = new JTabbedPane();
        tabPane.addTab("Станции", juncStPanel);
        tabPane.addTab("Нормы времени, мин", juncTimePanel);
        tabPane.setPreferredSize(new Dimension(380, 0));
        contentPanel.add(tabPane, BorderLayout.EAST);

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog d = this;
            {
                JButton button = new JButton("Сохранить увязки по узлу в файл");
                buttonPane.add(button);
                getRootPane().setDefaultButton(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            if (selectedJunc != null) {
                                String fileName = selectedJunc.name + "_увязки.xml";
                                File file = FileHandler.chooseFile(false, new FileNameExtensionFilter("xml files", "xml"), fileName, LinkingLocMain.mainFrame);

                                if (file != null) {
                                    boolean done = false;
                                    LinkingLocMain.mainFrame.setCursorWait();
                                    try {
                                        done = new LinkTrainXML().saveLinkTrain(file, selectedJunc.juncID);
                                    } finally {
                                        LinkingLocMain.mainFrame.setCursorDefault();
                                    }
                                    String mess = "";
                                    if (done) {
                                        mess = "Увязки сохранены успешно";
                                    } else {
                                        mess = "При сохранении обнаружены проблемы. Лучше проверить логи";
                                    }
                                    JOptionPane.showMessageDialog(LinkingLocMain.mainFrame, mess, "", JOptionPane.INFORMATION_MESSAGE);
                                }
                            }
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
            {
                JButton button = new JButton("Загрузить увязки по узлу из файла");
                buttonPane.add(button);
                getRootPane().setDefaultButton(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            if (selectedJunc != null) {
                                String fileName = "";
                                File file = FileHandler.chooseFile(true, new FileNameExtensionFilter("xml files", "xml"), fileName, LinkingLocMain.mainFrame);

                                if (file != null) {
                                    boolean err = false;
                                    boolean done = false;
                                    LinkingLocMain.mainFrame.setCursorWait();
                                    try {
                                        List<LinkTrain> listLinkTrain = new ArrayList<>();
                                        done = new LinkTrainXML().loadLinkTrain(file, selectedJunc.juncID, listLinkTrain);
                                        if(done){
                                            // сохранить в БД !!!
                                            done = LocDAOFactory.getInstance().getLinkTrainDAO().saveListLinkTrain(selectedJunc.juncID, listLinkTrain);
                                        }
                                    } finally {
                                        LinkingLocMain.mainFrame.setCursorDefault();
                                    }
                                    if(done && (!err)){
                                        JOptionPane.showMessageDialog(LinkingLocMain.mainFrame, "ok", "", JOptionPane.INFORMATION_MESSAGE);
                                    }else{
                                        JOptionPane.showMessageDialog(LinkingLocMain.mainFrame, "при загрузке произошли ошибки, см. лог", "", JOptionPane.INFORMATION_MESSAGE);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
            {
                JButton button = new JButton("Сохранить");
                buttonPane.add(button);
                getRootPane().setDefaultButton(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        JuncDAO juncDAO = LocDAOFactory.getInstance().getJuncDAO();
                        juncDAO.deleteJunc(juncListDel);
                        for (int i = 0; i < juncList.size(); i++) {
                            Junc junc = juncList.get(i);
                            if (junc.juncID > 0) {
                                juncDAO.updateJunc(junc);
                            } else {
                                juncDAO.addJunc(junc);
                            }
                        }
                        juncStTableModel.setList(null);
                        juncStTable.repaint();

                    }
                });
            }
            {
                JButton button = new JButton("Отмена");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        juncStTableModel.setList(null);
                        juncStTable.repaint();
                        d.setVisible(false);
                    }
                });
            }
        }

        juncTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent ev) {
                int row = juncTable.getSelectedRow();
                if (row > -1) {
                    Junc di = juncList.get(row);
                    if (!di.equals(selectedJunc)) {
                        selectedJunc = di;
                        juncStTableModel.setList(selectedJunc.getListSt());
                        juncStTable.repaint();
                        JuncTime jt = selectedJunc.getJuncTime();
                        if (jt != null) {
                            jtArrive.setText("" + jt.arrive);
                            jtDepart.setText("" + jt.depart);
                            jtEquipment.setText("" + jt.equipment);
                            // jtAcceptance.setText("" + jt.acceptance);
                        } else {
                            jtArrive.setText("0");
                            jtDepart.setText("0");
                            jtEquipment.setText("0");
                            // jtAcceptance.setText("0");
                        }
                    }
                }
            }
        });
    }

    protected JPanel getTimePanel() {
        JPanel juncTimePanel = new JPanel();
        juncTimePanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        juncTimePanel.setLayout(new BorderLayout());
        {
            JPanel time = new JPanel();
            time.setLayout(new BoxLayout(time, BoxLayout.Y_AXIS));
            juncTimePanel.add(time, BorderLayout.CENTER);
            {
                JPanel panel = new JPanel();
                panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
                time.add(panel);
                panel.add(new JLabel("По прибытию: "));
                panel.add(jtArrive);
            }
            {
                JPanel panel = new JPanel();
                panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
                time.add(panel);
                panel.add(new JLabel("По отправлению: "));
                panel.add(jtDepart);
            }
            {
                JPanel panel = new JPanel();
                panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
                time.add(panel);
                panel.add(new JLabel("На перецепку: "));
                panel.add(jtEquipment);
            }

        }
        {
            JPanel bottom = new JPanel();
            bottom.setLayout(new FlowLayout(FlowLayout.RIGHT));
            juncTimePanel.add(bottom, BorderLayout.SOUTH);
            {
                JButton actionPane = new JButton("Применить");
                bottom.add(actionPane);
                getRootPane().setDefaultButton(actionPane);
                actionPane.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        if (selectedJunc != null) {
                            JuncTime jt = selectedJunc.getJuncTime();
                            if (jt == null) {
                                jt = new JuncTime();
                                selectedJunc.setJuncTime(jt);
                            }
                            jt.juncID = selectedJunc.juncID;
                            jt.acceptance = 0;
                            try {
                                jt.arrive = Integer.parseInt(jtArrive.getText());
                                jt.depart = Integer.parseInt(jtDepart.getText());
                                jt.equipment = Integer.parseInt(jtEquipment.getText());
                            } catch (NumberFormatException e) {
                                // JOptionPane.showConfirmDialog(null,"Вводить следует числа!!!",
                                // "Предупреждение", JOptionPane.OK_OPTION);
                            }
                        }
                    }
                });
            }

        }
        return juncTimePanel;
    }

    protected JPanel getStPanel() {
        JPanel juncStPanel = new JPanel();
        juncStPanel.setLayout(new BorderLayout());
        juncStPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        // juncStPanel.setPreferredSize(new
        // Dimension(380,0));BorderFactory.createLineBorder(Color.black)
        {
            JPanel stPanel = new JPanel();
            stPanel.setLayout(new BorderLayout(0, 0));
            juncStPanel.add(stPanel, BorderLayout.CENTER);
            {
                juncStTableModel = new JuncStTableModel();
                juncStTable = new JTable(juncStTableModel);
                JScrollPane stScroll = new JScrollPane(juncStTable);
                juncStTable.setFillsViewportHeight(true);
                juncStTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                int colSize[] = { 20, 45, 140 };
                com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(juncStTable, colSize);
                // stScroll.setPreferredSize(new Dimension(340, 0));
                stPanel.add(stScroll, BorderLayout.CENTER);
            }
            {
                JPanel stActPanel = new JPanel();
                FlowLayout flowLayout = (FlowLayout) stActPanel.getLayout();
                flowLayout.setAlignment(FlowLayout.RIGHT);
                // stActPanel.setPreferredSize(new Dimension(150, 0));
                juncStPanel.add(stActPanel, BorderLayout.NORTH);
                {
                    stSelect = new JComboBox<Station>();

                    stSelect.setEditable(true);
                    final JTextComponent editor = (JTextComponent) stSelect.getEditor().getEditorComponent();
                    editor.setDocument(new StationPlainDocument(stSelect.getModel()));

                    stActPanel.add(stSelect);

                }
                {
                    JButton b = new JButton("+");
                    stActPanel.add(b);
                    b.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Station st = (Station) stSelect.getSelectedItem();
                            if (selectedJunc != null && st != null) {
                                List<JuncSt> listSt = selectedJunc.getListSt();
                                Collections.sort(listSt);
                                int num = 0;
                                if (listSt.size() > 0)
                                    num = listSt.get(listSt.size() - 1).num + 1;
                                JuncSt di = new JuncSt();
                                di.juncID = selectedJunc.juncID;
                                di.stationID = st.stationID;
                                di.codeESR = st.codeESR;
                                di.name = st.name;
                                di.num = num;
                                if (!listSt.contains(di))
                                    listSt.add(di);
                                juncStTable.repaint();
                            }
                        }
                    });
                }
                {
                    JButton b = new JButton("-");
                    stActPanel.add(b);
                    b.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            int index = juncStTable.getSelectedRow();
                            if (index > -1) {
                                juncStTableModel.getList().remove(index);
                                juncStTable.repaint();
                            }
                        }
                    });
                }
            }
        }
        return juncStPanel;
    }

    private void fillStSelect() {
        LocDAOFactory f = LocDAOFactory.getInstance();
        List<Station> list = f.getStationDAO().loadAllStation();
        stSelect.removeAllItems();
        for (int i = 0; i < list.size(); i++) {
            stSelect.addItem(list.get(i));
        }
    }
}
