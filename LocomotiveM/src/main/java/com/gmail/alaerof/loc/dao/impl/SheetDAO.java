package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistr;
import com.gmail.alaerof.loc.dao.dto.sheet.Sheet;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.sheet.SheetDistrEntity;
import com.gmail.alaerof.loc.entity.sheet.SheetEntity;
import com.gmail.alaerof.loc.entity.sheet.SheetJuncEntity;

public class SheetDAO {
    private static Logger logger = LogManager.getLogger(SheetDAO.class);
    private Connection cnLocal;

    public SheetDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    public List<SheetEntity> loadAllSheet() {
        List<SheetEntity> listE = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT sheetID, name FROM sheet ORDER BY name";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    SheetEntity ent = new SheetEntity();
                    Sheet ll = new Sheet();
                    ll.sheetID = rs.getInt("sheetID");
                    ll.name = rs.getString("name").trim();
                    ent.setList(ll);
                    List<SheetDistrEntity> listDE = loadSheetDistrEntity(ll.sheetID);
                    ent.setListDistr(listDE);
                    List<SheetJuncEntity> listJE = loadSheetJuncEntity(listDE, ent.getSheetConfig());
                    ent.setListJunc(listJE);
                    listE.add(ent);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return listE;
    }

    private List<SheetJuncEntity> loadSheetJuncEntity(List<SheetDistrEntity> listDE, SheetConfig sConf) {
        List<SheetJuncEntity> listJE = new ArrayList<>();
        JuncDAO juncDAO = LocDAOFactory.getInstance().getJuncDAO();
        SheetJuncEntity juncEEPrev = null;
        SheetJuncEntity juncBE = null;
        SheetJuncEntity juncEE = null;
        int distrIDPrev = 0;
        int juncIDprev = 0;
        for (int i = 0; i < listDE.size(); i++) {
            SheetDistrEntity sde = listDE.get(i);
            int distrID = sde.getSheetDistr().distrID;
            Junc juncB = null;
            Junc juncE = null;
            boolean isJB = false;
            boolean isJE = false;
            if (!sde.getSheetDistr().evenDown) {
                isJB = !sde.getSheetDistr().hideJuncB;
                isJE = !sde.getSheetDistr().hideJuncE;
                juncB = juncDAO.getJuncB(distrID);
                juncE = juncDAO.getJuncE(distrID);
            } else {
                isJB = !sde.getSheetDistr().hideJuncE;
                isJE = !sde.getSheetDistr().hideJuncB;
                juncB = juncDAO.getJuncE(distrID);
                juncE = juncDAO.getJuncB(distrID);
            }

            
            if (juncEEPrev != null) {
                juncEEPrev.setDistrIDAfter(distrID);
                juncEEPrev.setJuncIDAfter(juncB.juncID);
            }
            
            if (isJB) {
                juncBE = new SheetJuncEntity(juncB, sConf);
                juncBE.setSheetJuncType(SheetJuncEntity.TOP);
                juncBE.setDistrIDBefore(distrIDPrev);
                juncBE.setDistrIDAfter(distrID);

                juncBE.setJuncIDBefore(juncIDprev);
                juncBE.setJuncIDAfter(juncB.juncID);
            } else {
                juncBE = null;
            }
            if (isJE) {
                juncEE = new SheetJuncEntity(juncE, sConf);
                juncEE.setSheetJuncType(SheetJuncEntity.BOTTOM);
                juncEE.setDistrIDBefore(distrID);
                juncEE.setDistrIDAfter(0);

                juncEE.setJuncIDBefore(juncE.juncID);
                juncEE.setJuncIDAfter(0);
            } else {
                juncEE = null;
            }
            if (juncBE != null) {
                if (!juncBE.equals(juncEEPrev)) {
                    listJE.add(juncBE);
                } else {
                    juncEEPrev.setSheetJuncType(SheetJuncEntity.BOTH);
                }

            }
            
            if (juncEE != null) {
                listJE.add(juncEE);
            }
            
            juncEEPrev = juncEE;
            distrIDPrev = distrID;
            juncIDprev = juncE.juncID;
        }
        return listJE;
    }

    public List<SheetDistrEntity> loadSheetDistrEntity(int sheetID) {
        List<SheetDistrEntity> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT sheetDistrID, sheetID, distrID, num, evenDown, hideJuncB, hideJuncE, name "
                    + "FROM SheetDistr INNER JOIN distr ON SheetDistr.distrID = distr.distrID " + " WHERE sheetID = "
                    + sheetID + " ORDER BY num ";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    SheetDistrEntity ent = new SheetDistrEntity();
                    SheetDistr ld = new SheetDistr();
                    ld.sheetDistrID = rs.getInt("sheetDistrID");
                    ld.sheetID = rs.getInt("sheetID");
                    ld.distrID = rs.getInt("distrID");
                    ld.num = rs.getInt("num");
                    ld.evenDown = rs.getBoolean("evenDown");
                    ld.hideJuncB = rs.getBoolean("hideJuncB");
                    ld.hideJuncE = rs.getBoolean("hideJuncE");
                    ld.distrName = rs.getString("name").trim();
                    ent.setSheetDistr(ld);
                    List<SheetDistrSt> ll = loadSheetDistrSt(ld.sheetID, ld.distrID);
                    ent.setListSt(ll);
                    list.add(ent);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    public List<SheetDistrSt> loadSheetDistrSt(int sheetID, int distrID) {
        List<SheetDistrSt> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT sheetID, distrID, stationID, num, scale, hidden, visibleName, showVisibleName ,name "
                    + "FROM SheetDistrSt INNER JOIN Station ON SheetDistrSt.stationID = Station.stationID "
                    + " WHERE sheetID = " + sheetID + " AND distrID = " + distrID + " ORDER BY num ";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    SheetDistrSt ld = new SheetDistrSt();
                    ld.sheetID = rs.getInt("sheetID");
                    ld.distrID = rs.getInt("distrID");
                    ld.stationID = rs.getInt("stationID");
                    ld.num = rs.getInt("num");
                    ld.scale = rs.getInt("scale");
                    ld.hidden = rs.getBoolean("hidden");
                    String vn = rs.getString("visibleName");
                    if (vn != null)
                        vn = vn.trim();
                    ld.visibleName = vn;
                    ld.showVisibleName = rs.getBoolean("showVisibleName");
                    ld.stationName = rs.getString("name").trim();
                    list.add(ld);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    public Sheet getSheetByName(String name) {
        Sheet sh = null;
        String sql = "SELECT sheetID, name FROM sheet WHERE name = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setString(1, name);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    sh = new Sheet();
                    sh.sheetID = rs.getInt("sheetID");
                    sh.name = rs.getString("name").trim();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return sh;
    }

    private void insertSheet(String name) {
        String sql = "INSERT INTO sheet(name) VALUES (?)";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setString(1, name);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void deleteSheet(String name) {
        String sql = "DELETE FROM sheet WHERE name = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setString(1, name);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    private void insertSheetDistrSt(int sheetID, int distrID, List<SheetDistrSt> list) {
        String sql = "INSERT INTO SheetDistrSt (sheetID, distrID, stationID, num, hidden, scale, visibleName, showVisibleName)"
                + " VALUES(?,?,?,?,?,?,?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                SheetDistrSt de = list.get(i);
                st.setInt(1, sheetID);
                st.setInt(2, distrID);
                st.setInt(3, de.stationID);
                st.setInt(4, de.num);
                st.setBoolean(5, de.hidden);
                st.setInt(6, de.scale);
                st.setString(7, de.visibleName == null ? "" : de.visibleName.trim());
                st.setBoolean(8, de.showVisibleName);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    private void deleteSheetDistr(int sheetID) {
        String sql = "DELETE FROM SheetDistr WHERE sheetID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, sheetID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        sql = "DELETE FROM SheetDistrSt WHERE sheetID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, sheetID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    private void insertSheetDistr(int sheetID, List<SheetDistrEntity> listDistr) {
        String sql = "INSERT INTO SheetDistr(sheetID, distrID, num, evenDown, hideJuncB, hideJuncE) VALUES(?,?,?,?,?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < listDistr.size(); i++) {
                SheetDistr de = listDistr.get(i).getSheetDistr();
                st.setInt(1, sheetID);
                st.setInt(2, de.distrID);
                st.setInt(3, de.num);
                st.setBoolean(4, de.evenDown);
                st.setBoolean(5, de.hideJuncB);
                st.setBoolean(6, de.hideJuncE);
                st.addBatch();
                count++;
                insertSheetDistrSt(sheetID, de.distrID, listDistr.get(i).getListSt());
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void addSheet(String name, List<SheetDistrEntity> listDistr) {
        deleteSheet(name);
        insertSheet(name);
        Sheet sh = getSheetByName(name);
        if (sh != null) {
            insertSheetDistr(sh.sheetID, listDistr);
        }
    }

    private void updateSheetName(int sheetID, String name) {
        String sql = "UPDATE sheet SET name = ? WHERE sheetID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setString(1, name);
            st.setInt(2, sheetID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void updateSheet(SheetEntity sheetEntity) {
        int sheetID = sheetEntity.getSheet().sheetID;
        updateSheetName(sheetID, sheetEntity.getSheet().name);
        deleteSheetDistr(sheetID);
        insertSheetDistr(sheetID, sheetEntity.getListDistr());
    }
}
