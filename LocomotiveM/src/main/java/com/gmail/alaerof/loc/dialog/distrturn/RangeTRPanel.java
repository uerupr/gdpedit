package com.gmail.alaerof.loc.dialog.distrturn;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import com.gmail.alaerof.loc.dao.dto.DistrTurn;
import com.gmail.alaerof.loc.dao.dto.RangeTR;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.application.LinkingLocMain;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RangeTRPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private RangeTableModel rangeTableModel = new RangeTableModel();
    private JTable rangeTable;
    private RangeAllTableModel rangeAllTableModel = new RangeAllTableModel();
    private JTable rangeAllTable;
    private DistrTurn distrTurn;
    private List<RangeTR> listDel = new ArrayList<>();

    public RangeTRPanel() {
        setLayout(new BorderLayout(0, 0));

        JPanel allRangePanel = new JPanel();
        this.add(allRangePanel, BorderLayout.CENTER);
        allRangePanel.setLayout(new BorderLayout(0, 0));
        // allRangePanel.setPreferredSize(new Dimension(0, 250));

        JPanel allRangePanelTop = new JPanel();
        allRangePanelTop.add(new JLabel("Все диапазоны"));
        allRangePanel.add(allRangePanelTop, BorderLayout.NORTH);
        JButton allCat = new JButton("Категории");
        allRangePanelTop.add(allCat);
        allCat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LinkingLocMain.mainFrame.categoryDialog.loadData();
                LinkingLocMain.mainFrame.categoryDialog.setVisible(true);
            }
        });

        rangeAllTable = new JTable(rangeAllTableModel);
        rangeAllTable.setFillsViewportHeight(true);
        rangeAllTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSizeO[] = { 80, 80 };
        com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(rangeAllTable, colSizeO);
        rangeAllTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(rangeAllTable);
        allRangePanel.add(scroll, BorderLayout.CENTER);

        JPanel allRangeButtonPanel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) allRangeButtonPanel.getLayout();
        flowLayout.setAlignment(FlowLayout.LEADING);
        JButton newRange = new JButton("+");
        newRange.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                RangeTR r = new RangeTR();
                r.name = "--";
                rangeAllTableModel.getList().add(r);
                rangeAllTable.repaint();
            }
        });
        JButton delRange = new JButton("-");
        delRange.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                int row = rangeAllTable.getSelectedRow();
                RangeTR r = rangeAllTableModel.getList().get(row);
                listDel.add(r);
                rangeAllTableModel.getList().remove(row);
                rangeAllTable.repaint();
            }
        });
        JButton saveRange = new JButton("Сохранить");
        saveRange.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                LocDAOFactory.getInstance().getDistrTurnDAO().delListRange(listDel);
                LocDAOFactory.getInstance().getDistrTurnDAO().saveListRange(rangeAllTableModel.getList());
                loadRangeAll();
            }
        });

        allRangeButtonPanel.add(newRange);
        allRangeButtonPanel.add(delRange);
        allRangeButtonPanel.add(saveRange);
        allRangePanel.add(allRangeButtonPanel, BorderLayout.SOUTH);
        allRangeButtonPanel.add(new JLabel("пример: 3501-3599, 4501-5499"));

        JPanel turnRangePanel = new JPanel();
        turnRangePanel.setPreferredSize(new Dimension(0, 250));
        this.add(turnRangePanel, BorderLayout.NORTH);
        turnRangePanel.setLayout(new BorderLayout());

        JPanel turnTopPanel = new JPanel();
        turnRangePanel.add(turnTopPanel, BorderLayout.NORTH);
        turnTopPanel.add(new JLabel("Диапазоны для участка обращения"));

        rangeTable = new JTable(rangeTableModel);
        rangeTable.setFillsViewportHeight(true);
        rangeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(rangeTable, colSizeO);
        rangeTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scroll = new JScrollPane(rangeTable);
        turnRangePanel.add(scroll, BorderLayout.CENTER);

        JPanel turnButtom = new JPanel();
        FlowLayout flowLayout_1 = (FlowLayout) turnButtom.getLayout();
        flowLayout_1.setAlignment(FlowLayout.LEADING);
        turnRangePanel.add(turnButtom, BorderLayout.SOUTH);
        JButton addTurn = new JButton("+ участку");
        addTurn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (distrTurn != null) {
                    int row = rangeAllTable.getSelectedRow();
                    if (row > -1) {
                        RangeTR range = rangeAllTableModel.getList().get(row);
                        if (range.rangeTRID > 0) {
                            List<RangeTR> list = rangeTableModel.getList();
                            if (!list.contains(range)) {
                                list.add(range);
                                Collections.sort(list);
                                rangeTable.repaint();
                                LocDAOFactory.getInstance().getDistrTurnDAO()
                                        .saveListDistrTurnRangeTR(distrTurn, list);
                            }
                        } else {
                            JOptionPane
                                    .showMessageDialog(null,
                                            "Перед добавлением диапазона участку, необходимо сохранить диапазоны в БД");
                        }
                    }
                }
            }
        });
        JButton remTurn = new JButton("- участку");
        remTurn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row = rangeTable.getSelectedRow();
                if (row > -1) {
                    List<RangeTR> list = rangeTableModel.getList();
                    list.remove(row);
                    rangeTable.repaint();
                    LocDAOFactory.getInstance().getDistrTurnDAO().saveListDistrTurnRangeTR(distrTurn, list);
                }
            }
        });
        turnButtom.add(addTurn);
        turnButtom.add(remTurn);
    }

    public DistrTurn getDistrTurn() {
        return distrTurn;
    }

    public void setDistrTurn(DistrTurn distrTurn) {
        this.distrTurn = distrTurn;
    }

    public void loadRangeAll() {
        List<RangeTR> list = LocDAOFactory.getInstance().getDistrTurnDAO().loadRangeAll();
        rangeAllTableModel.setList(list);
        rangeAllTable.repaint();
    }

    public void setSelectedDistrTurn(DistrTurn dt) {
        this.distrTurn = dt;
        fillDistrTurnRange();
    }

    private void fillDistrTurnRange() {
        if (distrTurn != null) {
            List<RangeTR> list = LocDAOFactory.getInstance().getDistrTurnDAO()
                    .loadRangeDistrTurn(distrTurn.distrTurnID);
            rangeTableModel.setList(list);
        }else{
            rangeTableModel.getList().clear();
        }
        rangeTable.repaint();
    }
}
