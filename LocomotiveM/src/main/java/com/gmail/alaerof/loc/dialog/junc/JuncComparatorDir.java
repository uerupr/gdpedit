package com.gmail.alaerof.loc.dialog.junc;

import java.util.Comparator;

import com.gmail.alaerof.loc.dao.dto.Junc;

public class JuncComparatorDir implements Comparator<Junc> {

    @Override
    public int compare(Junc j1, Junc j2) {
        String d1 = "-";
        String d2 = "-";
        if (j1.getDir() != null)
            d1 = j1.getDir().name;
        if (j2.getDir() != null)
            d2 = j2.getDir().name;
        if (d1.equals(d2)) {
            int t1 = j1.getJuncType().getInt();
            int t2 = j2.getJuncType().getInt();
            if (t1 == t2) {
                return j1.name.compareTo(j2.name);
            }
            return t1 - t2;
        }
        return d1.compareTo(d2);
    }

}
