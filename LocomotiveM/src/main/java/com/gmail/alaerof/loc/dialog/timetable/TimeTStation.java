package com.gmail.alaerof.loc.dialog.timetable;

//import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;

public class TimeTStation {
    //private SheetDistrSt st;
    private String stationName = "";
    private int stationID;
    private int distrID;
    private String timeOn = "";
    private String timeOff = "";
    private String tStop = "";
    private String texStop = "";

   // public SheetDistrSt getSt() {
     //   return st;
    //}

   // public void setSt(SheetDistrSt st) {
   //     this.st = st;
   // }

    public String getTimeOn() {
        return timeOn;
    }

    public void setTimeOn(String timeOn) {
        if (timeOn == null) {
            timeOn = "";
        }
        this.timeOn = timeOn;
    }

    public String getTimeOff() {
        return timeOff;
    }

    public void setTimeOff(String timeOff) {
        if (timeOff == null) {
            timeOff = "";
        }
        this.timeOff = timeOff;
    }

    public String getTexStop() {
        return texStop;
    }

    public void setTexStop(String texStop) {
        if (texStop == null) {
            texStop = "";
        }
        this.texStop = texStop;
    }

    @Override
    public String toString() {
        return stationName + ": " + timeOn + " - " + timeOff + ", " + tStop + ", "
                + texStop;
    }

    public String gettStop() {
        return tStop;
    }

    public void settStop(String tStop) {
        this.tStop = tStop;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public int getStationID() {
        return stationID;
    }

    public void setStationID(int stationID) {
        this.stationID = stationID;
    }

    public int getDistrID() {
        return distrID;
    }

    public void setDistrID(int distrID) {
        this.distrID = distrID;
    }

}
