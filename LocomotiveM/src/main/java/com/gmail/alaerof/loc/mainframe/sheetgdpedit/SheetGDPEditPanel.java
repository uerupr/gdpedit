package com.gmail.alaerof.loc.mainframe.sheetgdpedit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.entity.linktrain.LinkTrainSheet;
import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.sheet.SheetEntity;
import com.gmail.alaerof.loc.entity.train.TrainThreadGeneralParams;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;

public class SheetGDPEditPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(SheetGDPEditPanel.class);
    public static Color backgroundColor = Color.WHITE;

    private String description = "";
    private SheetEntity sheetEntity;
    private JPanel contentLeft;
    private JPanel contentGDP;
    private JPanel contentHour;
    private JPanel contentHead;
    private JScrollPane scrollLeft;
    private JScrollPane scrollGDP;
    private JScrollPane scrollHour;
    private JLabel imageLabelLeft;
    private JLabel imageLabelGDP;
    private JLabel imageLabelHour;
    private JScrollBar vertScrollBar;
    private JScrollBar horScrollBar;
    private TrainThreadGeneralParams generalParams = new TrainThreadGeneralParams();
    private TrainThreadSheet selectedTrain;
    private LinkTrainSheet selectedLink;

    private ActionListener actionOnClose = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

        }
    };

    /**
     * отрисовка сетки на 3-х панелях
     */
    private void fillGDPNet() {
        sheetEntity.calcImageSize();
        sheetEntity.calcXY(0, 0);
        SheetConfig cf = sheetEntity.getSheetConfig();

        int hMain = cf.hGDP;
        int hHour = cf.hHour;
        int wLeft = cf.wLeft;
        int wGDP = cf.wGDP;

        imageLabelLeft = null;
        imageLabelGDP = null;
        imageLabelHour = null;

        BufferedImage buffImageLeft = new BufferedImage(wLeft, hMain, BufferedImage.TYPE_USHORT_555_RGB);
        BufferedImage buffImageGDP = new BufferedImage(wGDP, hMain, BufferedImage.TYPE_USHORT_555_RGB);
        BufferedImage buffImageHour = new BufferedImage(wGDP, hHour, BufferedImage.TYPE_USHORT_555_RGB);

        Graphics gLeft = buffImageLeft.createGraphics();
        Graphics gGDP = buffImageGDP.createGraphics();
        Graphics gHour = buffImageHour.createGraphics();

        // тут надо нарисовать все на Graphics
        sheetEntity.drawGDPGrid(gGDP, 0, 0, backgroundColor);
        sheetEntity.drawLeft(gLeft, 0, 0, backgroundColor);
        sheetEntity.drawHour(gHour, 0, 0, backgroundColor);

        // ----------- left -----------
        ImageIcon imageIconLeft = new ImageIcon(buffImageLeft);
        imageLabelLeft = new JLabel(imageIconLeft);
        imageLabelLeft.setPreferredSize(new Dimension(wLeft, hMain));

        contentLeft.setBounds(0, 0, wLeft, hMain);
        contentLeft.setLayout(null);
        contentLeft.setPreferredSize(new Dimension(wLeft, hMain));
        // contentLeft.setBackground(backgroundColor);

        contentLeft.add(imageLabelLeft);
        imageLabelLeft.setBounds(0, 0, wLeft, hMain);
        imageLabelLeft.setLayout(null);
        // ----------- GDP -----------
        ImageIcon imageIconGDP = new ImageIcon(buffImageGDP);
        imageLabelGDP = new JLabel(imageIconGDP);
        imageLabelGDP.setPreferredSize(new Dimension(wGDP, hMain));

        contentGDP.setBounds(0, 0, wGDP, hMain);
        contentGDP.setLayout(null);
        contentGDP.setPreferredSize(new Dimension(wGDP, hMain));
        // contentGDP.setBackground(backgroundColor);

        contentGDP.add(imageLabelGDP);
        imageLabelGDP.setBounds(0, 0, wGDP, hMain);
        imageLabelGDP.setLayout(null);
        // ----------- Hour -----------
        int wHour = wGDP + 20;
        ImageIcon imageIconGDPHour = new ImageIcon(buffImageHour);
        imageLabelHour = new JLabel(imageIconGDPHour);
        imageLabelHour.setPreferredSize(new Dimension(wGDP, hHour));

        contentHour.setBounds(0, 0, wHour, hHour);
        contentHour.setLayout(null);
        contentHour.setPreferredSize(new Dimension(wHour, hHour));
        contentHour.setBackground(backgroundColor);

        contentHour.add(imageLabelHour);
        imageLabelHour.setBounds(0, 0, wGDP, hHour);
        imageLabelHour.setLayout(null);

        // ----------- Head -----------
        contentHead.setBounds(0, 0, wLeft, hHour + 5);
        contentHead.setLayout(null);
        contentHead.setPreferredSize(new Dimension(wLeft, hHour + 5));
    }

    public SheetGDPEditPanel(SheetEntity se) {
        super(new BorderLayout());
        this.sheetEntity = se;
        description = se.getSheet().name;
        contentLeft = new JPanel(null);
        contentGDP = new JPanel(null);
        contentHour = new JPanel(null);
        contentHead = new JPanel(null);

        scrollLeft = new JScrollPane(contentLeft);
        scrollGDP = new JScrollPane(contentGDP);
        scrollHour = new JScrollPane(contentHour);

        JPanel left = new JPanel(new BorderLayout());
        JPanel right = new JPanel(new BorderLayout());

        left.add(scrollLeft, BorderLayout.CENTER);
        left.add(contentHead, BorderLayout.NORTH);

        right.add(scrollGDP, BorderLayout.CENTER);
        right.add(scrollHour, BorderLayout.NORTH);

        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, left, right);
        add(split, BorderLayout.CENTER);
        split.setDividerLocation(100);

        horScrollBar = scrollHour.getHorizontalScrollBar();
        scrollGDP.setHorizontalScrollBar(horScrollBar);

        vertScrollBar = scrollLeft.getVerticalScrollBar();
        scrollGDP.setVerticalScrollBar(vertScrollBar);

        fillGDPNet();
    }

    public String getDescription() {
        return description;
    }

    public int getSheetID() {
        return sheetEntity.getSheet().sheetID;
    }

    public ActionListener getActionOnClose() {
        return actionOnClose;
    }

    /** обновляет отображения ГДП листа */
    public void fillGDP() {
        sheetEntity.drawGDP();
    }

    public SheetEntity getSheetEntity() {
        return sheetEntity;
    }

    public JComponent getGDPOwner() {
        return imageLabelGDP;
    }

    /**
     * установить новое выделение, предварительно убрав каскадно старое
     * только один из них (поезд или увязка) должен быть не Null
     * в противном случае не Null будет только поезд 
     * @param train выбранный поезд
     * @param link выбранная увязка
     */
    public void setSelected(TrainThreadSheet train, LinkTrainSheet link) {
        if (selectedTrain != null) {
            selectedTrain.setSelected(null, false);
            selectedTrain = null;
        }
        if (selectedLink != null) {
            selectedLink.setSelected(null, false);
            selectedLink = null;
        }
        // в худшем случае оба будут null, зато не null будет только 1 из них
        if (train != null) {
            selectedTrain = train;
        } else {
            selectedLink = link;
        }
    }

    /** обнулить выделение на листе, ничего с ними не делая предварительно */
    public void setSelectedToNull() {
        this.selectedTrain = null;
        this.selectedLink = null;
    }

    /**
     * возвращает список выбранных поездов
     * @return список выбранных поездов
     */
    public List<TrainThreadSheet> getSelectedTrains() {
        if (selectedTrain != null) {
            List<TrainThreadSheet> list = new ArrayList<>();
            list.add(selectedTrain);
            return list;
        }
        return null;
    }

    public TrainThreadSheet getTrainThreadSheet(int distrID, int codeTR, String nameTR) {
        return sheetEntity.getTrainThreadSheet(distrID, codeTR, nameTR);
    }

    public TrainThreadGeneralParams getGeneralParams() {
        return generalParams;
    }

    public void setGeneralParams(TrainThreadGeneralParams generalParams) {
        this.generalParams = generalParams;
    }

    /**
     * сохранение ГДП (позиция увязки по вертикали, и только) в БД
     */
    public void saveGDP() {
        sheetEntity.saveGDP();
    }

    /**
     * расчет показателей поездов
     * @return
     */
    public int calculateTrainResults() {
        return sheetEntity.calculateTrainResults();
    }

    /**
     * печать ГДП в файл
     * @param file файл
     */
    public void printGDP(File file) {
        // sheetEntity.calcImageSize(); посчитано при выводе ГДП на панель
        SheetConfig cf = sheetEntity.getSheetConfig();

        int hGDP = cf.hGDP;
        int hHour = cf.hHour;
        int wLeft = cf.wLeft;
        int wGDP = cf.wGDP;
        // а вот XY нужно пересчитать в соответствии с отступами
        sheetEntity.calcXY(hHour, wLeft);
        try {
            BufferedImage buffImageGDP = new BufferedImage(wLeft + wGDP, hHour + hGDP,
                    BufferedImage.TYPE_USHORT_555_RGB);
            Graphics gGDP = buffImageGDP.createGraphics();

            // отрисовка сетки, названий станций и часов
            sheetEntity.drawGDPGrid(gGDP, hHour, wLeft, backgroundColor);
            sheetEntity.drawLeft(gGDP, hHour, 0, backgroundColor);
            sheetEntity.drawHour(gGDP, 0, wLeft, backgroundColor);
            sheetEntity.drawLeftHead(gGDP, 0, 0, backgroundColor);
            // отрисовка поездов
            sheetEntity.drawGDPImage(gGDP, wLeft, hHour);

            try {
                String ff = file.getAbsolutePath();
                if (!ff.endsWith(".png")) {
                    ff = ff + ".png";
                }
                File outputfile = new File(ff);

                ImageIO.write((RenderedImage) buffImageGDP, "png", outputfile);
            } catch (IOException e) {
                logger.error(e.toString(), e);
            }
        } finally {
            // и вернуть их обратно !!!
            sheetEntity.calcXY(0, 0);
        }
    }

    /**
     * печать ГДП на принтер
     */
    public void printGDP() {
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setJobName(sheetEntity.getSheet().name);

        boolean doPrint = job.printDialog();
        if (doPrint) {
            try {
                job.setPrintable(new SheetGDPPrintable(sheetEntity));
                job.print();
            } catch (PrinterException e) {
                logger.error(e.toString(), e);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

}
