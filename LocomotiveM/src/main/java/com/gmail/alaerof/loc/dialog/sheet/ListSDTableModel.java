package com.gmail.alaerof.loc.dialog.sheet;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.entity.sheet.SheetDistrEntity;

public class ListSDTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "Номер", "Назв.", "Перев.", "Скрыть нач.", "Скрыть кон." };
    private List<SheetDistrEntity> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (list != null) {
            SheetDistrEntity el = list.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return el.getSheetDistr().num;
            case 1:
                return el.getSheetDistr().distrName;
            case 2:
                return el.getSheetDistr().evenDown ? "*" : "";
            case 3:
                return el.getSheetDistr().hideJuncB ? "*" : "";
            case 4:
                return el.getSheetDistr().hideJuncE ? "*" : "";
            }
        }
        return null;
    }

    @Override
    public java.lang.Class<?> getColumnClass(int col) {
        switch (col) {
        case 0:
            return Integer.class;
        case 1:
        case 2:
        case 3:
        case 4:
            return String.class;
        }
        return Object.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 0 ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null) {
            SheetDistrEntity el = list.get(row);
            if (el != null) {
                switch (col) {
                case 0:
                    el.getSheetDistr().num = (Integer) aValue;
                    break;
                default:
                    break;
                }
            }
        }
    }

    public List<SheetDistrEntity> getList() {
        return list;
    }

    public void setList(List<SheetDistrEntity> list) {
        this.list = list;
    }

}
