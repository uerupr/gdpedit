package com.gmail.alaerof.loc.mainframe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.loc.dao.dto.train.TrainType;
import com.gmail.alaerof.loc.util.FileProperties;

public class MainConfigDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    public static final String CONF_PATH = "res" + File.separator + "mainconf.properties";
    private final JPanel contentPanel = new JPanel();
    private TypeTrPanel typeTrPanel;

    public MainConfigDialog() {
        initContent();
    }

    public MainConfigDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private void initContent() {
        setBounds(100, 100, 450, 300);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new FlowLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        {
            typeTrPanel = new TypeTrPanel();
            contentPanel.add(typeTrPanel);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog d = this;
            {
                JButton okButton = new JButton("Применить");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        saveMainConfig();
                        d.setVisible(false);
                    }
                });
            }
            {
                JButton cancelButton = new JButton("Отмена");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        d.setVisible(false);
                    }
                });

            }
        }

        loadMainConfig();
    }

    private void saveMainConfig() {
        Properties prop = new Properties();
        typeTrPanel.saveStateToProp(prop);
        FileProperties.getInstance().saveProperties(prop, CONF_PATH);
    }

    private void loadMainConfig() {
        Properties prop = FileProperties.getInstance().getProperties(CONF_PATH);
        typeTrPanel.loadStateFromProp(prop);
    }

    public boolean isVisibleTypeTrain(TrainType type) {
        return typeTrPanel.isVisibleTypeTrain(type);
    }
}

class TypeTrPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    JCheckBox[] visibleTypes;

    TypeTrPanel() {
        TrainType[] allTypes = TrainType.values();
        visibleTypes = new JCheckBox[allTypes.length];
        for (int i = 0; i < allTypes.length; i++) {
            JCheckBox ch = new JCheckBox(allTypes[i].getString());
            visibleTypes[i] = ch;
            this.add(ch);
        }
    }

    boolean isVisibleTypeTrain(TrainType type) {
        if (type != null) {
            for (int i = 0; i < visibleTypes.length; i++) {
                JCheckBox ch = visibleTypes[i];
                String typeStr = type.getString();
                String name = ch.getText().trim();
                if (name.equals(typeStr)) {
                    return ch.isSelected();
                }
            }
        }
        return true;
    }

    void saveStateToProp(Properties prop) {
        for (int i = 0; i < visibleTypes.length; i++) {
            JCheckBox ch = visibleTypes[i];
            TrainType type = TrainType.getType(ch.getText());
            prop.put(type.toString(), Boolean.toString(ch.isSelected()));
        }
    }

    void loadStateFromProp(Properties prop) {
        for (int i = 0; i < visibleTypes.length; i++) {
            JCheckBox ch = visibleTypes[i];
            TrainType type = TrainType.getType(ch.getText());
            String key = type.toString();
            String value = prop.get(key).toString();
            // System.out.println(value);
            if (value != null) {
                ch.setSelected(Boolean.parseBoolean(value));
            }
        }
    }
}
