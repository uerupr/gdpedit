package com.gmail.alaerof.loc.dao.dto;

public class JuncSt implements Comparable<JuncSt>{
    public int juncID;
    public int stationID;
    public int num;
    //----
    public String codeESR;
    public String name;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(JuncSt o) {
        return num - o.num;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + juncID;
        result = prime * result + stationID;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JuncSt other = (JuncSt) obj;
        if (juncID != other.juncID)
            return false;
        if (stationID != other.stationID)
            return false;
        return true;
    }
    
    
}
