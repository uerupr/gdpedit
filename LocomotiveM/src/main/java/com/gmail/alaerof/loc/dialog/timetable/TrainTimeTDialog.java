package com.gmail.alaerof.loc.dialog.timetable;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;

public class TrainTimeTDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private ListTrainPanel trainPanel;
    private TimeTPanel timePanel;
    private TimeTFullPanel timeFullPanel;
    private TrainResultsPanel trainResultsPanel;


    public TrainTimeTDialog() {
        initContent();
    }

    public TrainTimeTDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }
    
    private void initContent(){
        setBounds(100, 100, 840, 500);
        getContentPane().setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

        timePanel = new TimeTPanel();
        timeFullPanel = new TimeTFullPanel();
        trainResultsPanel = new TrainResultsPanel();
        
        JTabbedPane timeTabbedPane = new JTabbedPane();
        timeTabbedPane.addTab("На участке", timePanel);
        timeTabbedPane.addTab("Полное расписание", timeFullPanel);
        timeTabbedPane.addTab("Показатели", trainResultsPanel);
        
        trainPanel = new ListTrainPanel(timePanel, timeFullPanel, trainResultsPanel);
        trainPanel.setPreferredSize(new Dimension(380, 0));

        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, trainPanel, timeTabbedPane);
        split.setDividerLocation(300);
        contentPanel.add(split, BorderLayout.CENTER);
        
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        TrainTimeTDialog.this.setVisible(false);
                    }
                });
                
            }
            {
               // JButton cancelButton = new JButton("Cancel");
               // cancelButton.setActionCommand("Cancel");
               // buttonPane.add(cancelButton);
            }
        }
    }
    
    public void setListTrain(List<TrainThreadSheet> listTrain){
        trainPanel.setListTrain(listTrain);
    }
    
    public void setListSt(List<SheetDistrSt> listSt){
        timePanel.setListSt(listSt);
    }
}
