package com.gmail.alaerof.loc.entity.train;

import java.util.Comparator;

import com.gmail.alaerof.loc.dao.dto.train.TrainStation;

public class ComparatorByLastArriveTime implements Comparator<TrainThread>{

    @Override
    public int compare(TrainThread tr1, TrainThread tr2) {
        TrainStation st1 = tr1.getTrainStation(tr1.getLastStID());
        TrainStation st2 = tr2.getTrainStation(tr2.getLastStID());
        return st1.getTimeOnChecked() - st2.getTimeOnChecked();
    }

}
