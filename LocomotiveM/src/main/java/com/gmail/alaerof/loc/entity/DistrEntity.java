package com.gmail.alaerof.loc.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Station;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
/**
 * Класс бизнесс-объекта (сущности) "ж.д. участок".
 * Содержит:
 * - данные по участку (distr);
 * - список перегонов  (DistrSpan) отсортированный по порядку нумерации;
 * - карту станций (DistrStation);
 * - список неповторяющихся станций в порядке нумерации перегонов.
 * @author Helen Yrofeeva
 *
 */
public class DistrEntity {
    private Distr distr;
    private List<DistrSpan> listSpan;
    private Map<Integer, DistrStation> mapSt = new HashMap<>();
    private List<DistrStation> listSt = new ArrayList<>();

    /**
     * в конструкторе происходит загрузка перегонов и станций из БД
     * @param distr объект distr
     */
    public DistrEntity(Distr distr) {
        this.distr = distr;
        loadListSpan();
    }

    /**
     * возвращает станцию по ID
     * @param idSt ID станции
     * @return станцию DistrStation или null
     */
    public DistrStation getStationByID(Integer idSt) {
        return mapSt.get(idSt);
    }
    
    /**
     * возвращает станцию по расширенному коду ЕСР 
     * @param ESR код ЕСР
     * @param ESR2 расширение кода
     * @return станцию DistrStation или null
     */
    public DistrStation getStationByESR(String ESR, String ESR2) {
        Iterator<DistrStation> iSt = mapSt.values().iterator();
        while (iSt.hasNext()) {
            DistrStation dst = iSt.next();
            Station st = dst.getStation();
            if (st.codeESR.equals(ESR) && st.codeESR2.equals(ESR2)) {
                return dst;
            }
        }
        return null;
    }

    /**
     * возвращает станцию по коду Экспресс
     * @param express код Экспресс
     * @return станцию DistrStation или null
     */
    public DistrStation getStationByExpress(String express) {
        Iterator<DistrStation> iSt = mapSt.values().iterator();
        while (iSt.hasNext()) {
            DistrStation dst = iSt.next();
            Station st = dst.getStation();
            if (st.codeExpress.equals(express)) {
                return dst;
            }
        }
        return null;
    }

    /**
     * Возвращает станцию DistrStation из карты участка по ID.
     * Если в карте такой нет, то загружает ее из БД
     * (метод используется только при загрузке перегонов !!!)
     * @param idSt ID станции
     * @return станцию DistrStation
     */
    private DistrStation getStation(Integer idSt) {
        DistrStation st = mapSt.get(idSt);
        if (st == null) {
            st = new DistrStation(idSt);
            mapSt.put(idSt, st);
        }
        return st;
    }

    /**
     * загружает перегоны DistrSpan из БД
     */
    private void loadListSpan() {
        listSpan = LocDAOFactory.getInstance().getSpanDAO().loadDistrSpan(distr.distrID);
        for (int i = 0; i < listSpan.size(); i++) {
            DistrSpan ds = listSpan.get(i);
            int idSt = ds.getSpan().stB;
            DistrStation dst = getStation(idSt);
            ds.setStB(dst);
            if (!listSt.contains(dst)) {
                listSt.add(dst);
            }
            idSt = ds.getSpan().stE;
            dst = getStation(idSt);
            ds.setStE(dst);
            if (!listSt.contains(dst)) {
                listSt.add(dst);
            }
        }
    }

    /**
     * возвращает перегон по названию
     * @param name название перегона 
     * @return перегон DistrSpan или null
     */
    public DistrSpan getSpan(String name) {
        for (int i = 0; i < listSpan.size(); i++) {
            DistrSpan ds = listSpan.get(i);
            if (name.equals(ds.getSpan().name)) {
                return ds;
            }
        }
        return null;
    }

    /**
     * возвращает перегон по ID начальной и конечной станции
     * @param stB ID начальной станции
     * @param stE ID конечной станции
     * @return перегон DistrSpan или null
     */
    public DistrSpan getSpan(int stB, int stE) {
        for (int i = 0; i < listSpan.size(); i++) {
            DistrSpan ds = listSpan.get(i);
            int sB = ds.getStB().getStation().stationID;
            int sE = ds.getStE().getStation().stationID;
            if (stB == sB && stE == sE) {
                return ds;
            }
        }
        return null;
    }

    public List<DistrSpan> getListSpan() {
        return listSpan;
    }

    public int getDistrID() {
        return distr.distrID;
    }

    @Override
    public String toString() {
        return distr.name;
    }

    public String getDistrName() {
        return distr.name.trim();
    }

    public Distr getDistr() {
        return distr;
    }

    /**
     * возвращает перегон по ID
     * @param spanID ID перегона
     * @return перегон DistrSpan или null
     */
    public DistrSpan getSpan(int spanID) {
        for (int i = 0; i < listSpan.size(); i++) {
            DistrSpan ds = listSpan.get(i);
            if (ds.getSpan().spanID == spanID) {
                return ds;
            }
        }
        return null;        
    }
}
