package com.gmail.alaerof.loc.dialog.linkloc;

import javax.swing.JTable;

public class CellData {
    private JTable table;

    public CellData(JTable table) {
        this.table = table;
    }

    public int getColumn() {
        return table.getSelectedColumn();
    }

    public int getRow() {
        return table.getSelectedRow();
    }

    public Object getValue() {
        int row = table.getSelectedRow();
        int col = table.getSelectedColumn();
        return table.getValueAt(row, col);
    }

    public void setValue(Object aValue) {
        int row = table.getSelectedRow();
        int col = table.getSelectedColumn();
        table.setValueAt(aValue, row, col);
    }

    public JTable getTable() {
        return table;
    }

}
