package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Category;
import com.gmail.alaerof.loc.dao.dto.Direction;

public class DirectionDAO {
    private static Logger logger = LogManager.getLogger(DirectionDAO.class);
    private Connection cnLocal;

    public DirectionDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    public int insertListDirection(List<Direction> listDir) {
        String sql = "INSERT INTO Direction (dirID, name, orderNum) VALUES (?, ?, ?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < listDir.size(); i++) {
                Direction d = listDir.get(i);
                st.setInt(1, d.dirID);
                st.setString(2, d.name);
                st.setInt(3, d.orderNum);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }
    
    public int insertListCategory(List<Category> list) {
        String sql = "INSERT INTO category (categoryID, name, prior, trMin, trMax, type) VALUES (?, ?, ?, ?, ?, ?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Category cat = list.get(i);
                st.setInt(1, cat.categoryID);
                st.setString(2, cat.name);
                st.setInt(3, cat.prior);
                st.setInt(4, cat.trMin);
                st.setInt(5, cat.trMax);
                st.setString(6, cat.type);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    public int updateListDirection(List<Direction> listDir) {
        String sql = "UPDATE Direction SET name = ?, orderNum = ?  WHERE dirID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < listDir.size(); i++) {
                Direction d = listDir.get(i);
                st.setString(1, d.name);
                st.setInt(2, d.orderNum);
                st.setInt(3, d.dirID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }
    
    public int updateListCategory(List<Category> list) {
        String sql = "UPDATE category SET name=?, prior=?, trMin=?, trMax=?, type = ? WHERE categoryID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Category cat = list.get(i);
                st.setString(1, cat.name);
                st.setInt(2, cat.prior);
                st.setInt(3, cat.trMin);
                st.setInt(4, cat.trMax);
                st.setString(5, cat.type);
                st.setInt(6, cat.categoryID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    public List<Direction> loadAllDirection() {
        List<Direction> listDir = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT dirID, name, orderNum FROM Direction ORDER BY orderNum, name";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Direction dir = new Direction();
                    dir.dirID = rs.getInt("dirID");
                    dir.name = rs.getString("name").trim();
                    dir.orderNum = rs.getInt("orderNum");
                    listDir.add(dir);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return listDir;
    }
    
    public List<Category> loadAllCategory() {
        List<Category> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT categoryID, name, prior, trMin, trMax, type FROM category";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Category cat = new Category();
                    cat.categoryID = rs.getInt("categoryID");
                    cat.name = rs.getString("name").trim();
                    cat.prior = rs.getInt("prior");
                    cat.trMin = rs.getInt("trMin");
                    cat.trMax = rs.getInt("trMax");
                    cat.type = rs.getString("type").trim();
                    list.add(cat);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

}
