package com.gmail.alaerof.loc.entity.sheet;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistr;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.DistrEntity;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditPanel;
import com.gmail.alaerof.loc.util.CalcString;

public class SheetDistrEntity implements Comparable<SheetDistrEntity> {
    /** привязка к листу участка */
    private SheetDistr sheetDistr;
    /** привязка к листу станций */
    private List<SheetDistrSt> listSt;
    // далее тут должно все касаться ГДП и его отображения
    private DistrSheetGDP distrGDP;
    /** начало участка по y (сверху) */
    private int yTop;
    /** конец участка по y (снизу) */
    private int yBottom;

    /**
     * возвращает список видимых станций (!hidden)
     * 
     * @return список видимых станций
     */
    public List<SheetDistrSt> getListStVisible() {
        List<SheetDistrSt> list = new ArrayList<>();
        for (int i = 0; i < listSt.size(); i++) {
            SheetDistrSt st = listSt.get(i);
            if (!st.hidden) {
                list.add(st);
            }
        }
        return list;
    }

    /**
     * расчет длины участка (только видимых станций (!hidden))
     * @return длина участка
     */
    public int getDistrLen() {
        int len = 0;
        List<SheetDistrSt> list = getListStVisible();
        for (int i = 0; i < list.size() - 1; i++) {
            SheetDistrSt sd = list.get(i);
            // скрытие станций на участке означает - не показывать станцию
            len += sd.scale;
        }
        return len;
    }

    /**
     * расчет вертикальных координат станций участка
     * @param y верхняя координата участка
     */
    public void calcStY(int y) {
        yTop = y;
        Collections.sort(listSt);
        if (sheetDistr.evenDown) {
            Collections.reverse(listSt);
        }
        int lastScale = 0;
        for (int i = 0; i < listSt.size(); i++) {
            SheetDistrSt sd = listSt.get(i);
            if (!sd.hidden) {
                y += lastScale;
                lastScale = sd.scale;
            }
            sd.setY0(y);
        }
        yBottom = y;
    }

    public void drawGDPGrid(Graphics grGDP, Color bgColor, SheetConfig sConf) {
        Graphics2D gGDP = (Graphics2D) grGDP;
        gGDP.setColor(sConf.gdpGridColor);
        int w = sConf.rbGDP.x - sConf.ltGDP.x;
        int h = yBottom - yTop;
        gGDP.drawRect(sConf.ltGDP.x, yTop, w, h);

        // --- линии сетки ---
        int hCount = 24;
        gGDP.setColor(sConf.gdpGridColor);
        int k = 0;
        Stroke dash = new BasicStroke(1.0f, // Width
                BasicStroke.CAP_SQUARE, // End cap
                BasicStroke.JOIN_MITER, // Join style
                10.0f, // Miter limit
                new float[] { 5.0f, 5.0f }, // Dash pattern
                0.0f); // Dash phase
        Stroke solid = new BasicStroke(1.0f);
        Stroke bold = new BasicStroke(2.0f);

        while (k <= hCount * 60) {
            int x0 = (int) (sConf.ltGDP.x + k * sConf.picTimeHor);

            gGDP.setStroke(solid);
            if (k % 30 == 0) {
                gGDP.setStroke(dash);
            }
            if (k % 60 == 0) {
                gGDP.setStroke(bold);
            }
            gGDP.drawLine(x0, yTop, x0, yBottom);
            k += 10;
        }
        gGDP.setStroke(solid);
        // --- линии станций ---
        for (int i = 0; i < listSt.size(); i++) {
            SheetDistrSt sd = listSt.get(i);
            gGDP.drawLine(sConf.ltGDP.x, sd.getY0(), sConf.rbGDP.x, sd.getY0());
        }
    }

    public void drawLeft(Graphics grLeft, SheetConfig sConf) {
        Graphics2D gLeft = (Graphics2D) grLeft;
        gLeft.setColor(sConf.gdpGridColor);
        gLeft.setFont(sConf.stationFont);
        int hSt = CalcString.getStringH(sConf.stationFont);

        // --- названи¤ станций ---
        for (int i = 0; i < listSt.size(); i++) {
            SheetDistrSt sd = listSt.get(i);
            if (!sd.hidden) {
                if (sd.showVisibleName) {
                    gLeft.drawString(sd.visibleName, 5, sd.getY0() + hSt / 2);
                } else {
                    gLeft.drawString(sd.stationName, 5, sd.getY0() + hSt / 2);
                }
            }
        }
    }

    public SheetDistr getSheetDistr() {
        return sheetDistr;
    }

    /**
     * при установке участка создаетс новый объект distrGDP, но ГДП не
     * загружаетс¤
     * 
     * @param sdistr
     */
    public void setSheetDistr(SheetDistr sdistr) {
        this.sheetDistr = sdistr;
        Distr distr = LocDAOFactory.getInstance().getDistrDAO().loadDistr(sdistr.distrID);
        DistrEntity distrEntity = new DistrEntity(distr);
        distrGDP = new DistrSheetGDP(distrEntity);
    }

    public List<SheetDistrSt> getListSt() {
        return listSt;
    }

    public void setListSt(List<SheetDistrSt> listSt) {
        this.listSt = listSt;
    }

    public SheetDistrSt getSheetDistrSt(int stID) {
        SheetDistrSt st = null;
        for (int i = 0; i < listSt.size(); i++) {
            st = listSt.get(i);
            if (st.stationID == stID) {
                return st;
            }
        }
        return null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sheetDistr == null) ? 0 : sheetDistr.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SheetDistrEntity other = (SheetDistrEntity) obj;
        if (sheetDistr == null) {
            if (other.sheetDistr != null)
                return false;
        } else if (!sheetDistr.equals(other.sheetDistr))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return sheetDistr.distrName;
    }

    @Override
    public int compareTo(SheetDistrEntity o) {
        return sheetDistr.num - o.getSheetDistr().num;
    }

    public String getSourceGDP() {
        if (distrGDP == null)
            return null;
        else
            return distrGDP.getDistrEntity().getDistr().fileName;
    }

    public DistrSheetGDP getDistrGDP() {
        return distrGDP;
    }

    public void loadGDPFromDB() {
        if (distrGDP != null) {
            distrGDP.loadGDPFromDB(sheetDistr.num);
        }
    }

    public void setGDPSource(String fileName) {
        if (distrGDP != null) {
            Distr d = distrGDP.getDistrEntity().getDistr();
            d.fileName = fileName;
            LocDAOFactory.getInstance().getDistrDAO().updateFileName(d);
        }
    }

    /**
     * отрисовка ГДП на панели (включает в себя удаление имеющихся объектов ГДП и создание новых)
     * @param sheetGDPEditPanel контейнер для рисования
     */
    public void drawGDP(SheetGDPEditPanel sheetGDPEditPanel) {
        distrGDP.drawGDP(sheetGDPEditPanel, sheetDistr.num);
    }

    public List<TrainThreadSheet> getListTr() {
        if (distrGDP != null) {
            return distrGDP.getListTrain();
        }
        return null;
    }
//    /**
//     * устанавливает на участке selected поезд в переданное значение
//     * (train или null)
//     * @param trainThread значение для установки
//     */
//    public void setSelected(TrainThreadSheet trainThread) {
//        distrGDP.setSelected(trainThread);
//    }
//
//    public TrainThreadSheet getSelectedTrain() {
//        return distrGDP.getSelectedTrain();
//    }

    public TrainThreadSheet getTrainThreadSheet(int codeTR, String nameTR) {
        if (distrGDP != null) {
            return distrGDP.getTrainThreadSheet(codeTR, nameTR);
        }
        return null;
    }

    public int calculateTrainResults() {
        DistrEntity distr = distrGDP.getDistrEntity();
        List<TrainThreadSheet> list = distrGDP.getListTrain();
        int err = 0;
        for (TrainThreadSheet tr : list) {
            err += tr.calculateResults(distr);
        }
        return err;
    }

    /**
     * отрисовка ГДП на канве
     * @param grGDP канва
     */
    public void drawGDPImage(Graphics grGDP, int gapLeft, int gapTop) {
        distrGDP.drawGDPImage(grGDP, gapLeft, gapTop);
    }
}
