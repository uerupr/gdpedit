package com.gmail.alaerof.loc.dao.dto;

public class Station extends AbstractDTO implements Comparable<Station>{
    public int stationID;
    public int dirID;
    public String codeExpress;
    public String codeESR;
    public String codeESR2;
    public String name;
    public boolean br = true;
    public boolean joint;

    @Override
    public String toString() {
        return name;
    }


    @Override
    public int getID() {
        return stationID;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (br ? 1231 : 1237);
        result = prime * result + ((codeESR == null) ? 0 : codeESR.hashCode());
        result = prime * result + ((codeESR2 == null) ? 0 : codeESR2.hashCode());
        result = prime * result + ((codeExpress == null) ? 0 : codeExpress.hashCode());
        result = prime * result + dirID;
        result = prime * result + (joint ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + stationID;
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Station other = (Station) obj;
        if (br != other.br)
            return false;
        if (codeESR == null) {
            if (other.codeESR != null)
                return false;
        } else if (!codeESR.equals(other.codeESR))
            return false;
        if (codeESR2 == null) {
            if (other.codeESR2 != null)
                return false;
        } else if (!codeESR2.equals(other.codeESR2))
            return false;
        if (codeExpress == null) {
            if (other.codeExpress != null)
                return false;
        } else if (!codeExpress.equals(other.codeExpress))
            return false;
        if (dirID != other.dirID)
            return false;
        if (joint != other.joint)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (stationID != other.stationID)
            return false;
        return true;
    }


    @Override
    public int compareTo(Station o) {
        return this.name.compareToIgnoreCase(o.name);
    }
       
}
