package com.gmail.alaerof.loc.dialog.distrturn;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import com.gmail.alaerof.loc.dao.dto.DistrTurn;

public class DistrTurnTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "№", "Участок обращения", "Ж.д. участок", "Нач. ст.", "Кон. ст.",
            "расч. подьем туда", "расч. подьем обратно", "расч. вес туда", "расч. вес обратно", "длина, км" };
    private List<DistrTurn> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            DistrTurn el = list.get(row);
            // "Участок оборота", "Ж.д. участок", "Нач. ст.", "Кон. ст.",
            // "расч. подьем туда", "расч. подьем обратно",
            // "расч. вес туда", "расч. вес обратно", "длина, км"

            switch (col) {
            case 0:
                return el.num;
            case 1:
                return el.name;
            case 2:
                return el.distrName;
            case 3:
                return el.stB.name;
            case 4:
                return el.stE.name;
            case 5:
                return el.gradientforw;
            case 6:
                return el.gradientback;
            case 7:
                return el.weightforw;
            case 8:
                return el.weightback;
            case 9:
                return el.lenght;
            }
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null) {
            DistrTurn el = list.get(row);
            if (el != null) {
                switch (col) {
                case 0:
                    el.num = (Integer) aValue;
                    break;
                case 5:
                    el.gradientforw = (Double) aValue;
                    break;
                case 6:
                    el.gradientback = (Double) aValue;
                    break;
                case 7:
                    el.weightforw = (Integer) aValue;
                    break;
                case 8:
                    el.weightback = (Integer) aValue;
                    break;
                case 9:
                    el.lenght = (Double) aValue;
                    break;
                default:
                    break;
                }
            }
        }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col > 4 || col==0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return Integer.class;
        case 1:
        case 2:
        case 3:
        case 4:
            return String.class;
        case 5:
        case 6:
            return Double.class;
        case 7:
        case 8:
            return Integer.class;
        case 9:
            return Double.class;
        default:
            return Object.class;
        }
    }

    public List<DistrTurn> getList() {
        return list;
    }

    public void setList(List<DistrTurn> list) {
        this.list = list;
    }

}
