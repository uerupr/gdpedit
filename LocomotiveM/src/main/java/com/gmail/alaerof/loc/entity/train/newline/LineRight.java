package com.gmail.alaerof.loc.entity.train.newline;

import java.awt.Dimension;
import java.awt.Polygon;

import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.train.TrainThreadElement;
import com.gmail.alaerof.loc.entity.train.TrainThreadGeneralParams;

public class LineRight extends NewLine {
    private static final long serialVersionUID = 1L;

    public LineRight(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams,
            TrainThreadElement trainThreadElement, SheetConfig sheetConfig) {
        super(generalParams, selfParams, trainThreadElement, sheetConfig);
        direction = Direction.right;
    }

    @Override
    protected Polygon getPolygon() {
        int npoints = 10;
        int x[] = new int[npoints];
        int y[] = new int[npoints];
        int maxW = getSize().width; // ширина с отступами
        int maxH = getSize().height; // высота с отступами
        int w = getOriginalSize().width; // ширина без отступов
        int h = getOriginalSize().height; // высота без отступов
        double gg = Math.max(1, Math.sqrt(w * w + h * h));// гипотенуза
        int indentX = this.getIndent().width;
        x[0] = indentX; // начало оси
        y[0] = maxH; // начало оси

        x[1] = x[0] - indentX;
        y[1] = y[0];

        int step = sheetConfig.stepCodeE;
        if (step + getCaptionSize().width > gg) {
            step = 15;
        }
        double stepX = step * w / h;
        x[2] = (int) (x[1] + stepX);
        y[2] = y[1] - step;

        double chx = 0;
        double chy = 0;
        if (isCaption()) {
            chx = getCaptionSize().getHeight() * h / gg;
            chy = getCaptionSize().getHeight() * w / gg;
        }
        x[3] = (int) (x[2] - chx);
        y[3] = (int) (y[2] - chy);

        double cwx = getCaptionSize().getWidth() * w / gg;
        double cwy = getCaptionSize().getWidth() * h / gg;
        x[4] = (int) (x[3] + cwx);
        y[4] = (int) (y[3] - cwy);

        x[5] = (int) (x[4] + chx);
        y[5] = (int) (y[4] + chy);

        x[6] = maxW - indentX * 2;
        y[6] = 0;

        x[7] = x[6] + indentX; // конец оси
        y[7] = y[6]; // конец оси

        x[8] = x[7] + indentX;
        y[8] = y[6];

        x[9] = x[0] + indentX;
        y[9] = y[0];

        alphaCaption = -Math.asin(h / gg);
         captionPoint.x = (int) (x[0] + stepX);
         captionPoint.y = y[0] - step - 1;
        //captionPoint.x = x[2];
        //captionPoint.y = y[2];
        begin.x = x[0];
        begin.y = y[0];
        end.x = x[7];
        end.y = y[7];

        pointTimeL.x = getTimeSize().width;
        pointTimeL.y = y[1] - 3 - (int) (getTimeSize().height / 2);
        pointTimeA.x = x[8] - getTimeSize().width;
        pointTimeA.y = (int) (getTimeSize().height * 3 / 4);

        captionPointGDP.x = (int) (x[0] + stepX) - indentX;
        captionPointGDP.y = y[0] - step - 1 - maxH + h;
        beginGDP.x = x[0] - indentX;
        beginGDP.y = y[0] - maxH + h;
        endGDP.x = x[7] - indentX;
        endGDP.y = y[7] - maxH + h;

        pointTimeLGDP.x = getTimeSize().width - indentX;
        pointTimeLGDP.y = y[1] - 3 - (int) (getTimeSize().height / 2) - maxH + h;
        pointTimeAGDP.x = x[8] - getTimeSize().width - indentX;
        pointTimeAGDP.y = (int) (getTimeSize().height * 3 / 4) - maxH + h;

        return new Polygon(x, y, npoints);
    }

    @Override
    public Dimension getIndent() {
        int w = getOriginalSize().width; // ширина без отступов
        int h = getOriginalSize().height; // высота без отступов
        double gg = Math.max(1, Math.sqrt(h * h + w * w));
        int pw = (int) (ADD_WIDTH + selfParams.lineWidth);
        int indx = (int) (pw * gg / h);
        return new Dimension(indx + getTimeSize().width, 0);
    }

}
