package com.gmail.alaerof.loc.dialog.distrturn;

import java.text.ParseException;

import javax.swing.JOptionPane;

import com.gmail.alaerof.loc.dao.dto.RangeTR;

public class RangeAllTableModel extends RangeTableModel {
    private static final long serialVersionUID = 1L;

    @Override
    public boolean isCellEditable(int row, int col) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null) {
            RangeTR el = list.get(row);
            if (el != null) {
                switch (col) {
                case 0:
                    el.name = aValue.toString();
                    break;
                default:
                    String range = aValue.toString();
                    try {
                        el.setRangeValue(range);
                    } catch (ParseException e) {
                        JOptionPane.showMessageDialog(null, "Неправильный формат ввода!!!");
                    }
                    break;
                }
            }
        }
    }
}
