package com.gmail.alaerof.loc.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gmail.alaerof.loc.dao.dto.train.DistrTrain;
import com.gmail.alaerof.loc.dao.dto.train.TrainSpan;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.DistrEntity;
import com.gmail.alaerof.loc.entity.DistrSpan;
import com.gmail.alaerof.loc.entity.DistrStation;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.util.ColorConverter;
import com.gmail.alaerof.loc.util.TimeConverter;

public class GDPDataXML {
    private static Logger logger = LogManager.getLogger(GDPDataXML.class);
    public static final int ESR = 0;
    public static final int EXPRESS = 1;
    public static int StationIdentification = EXPRESS;
    private static GDPDataXML instance = new GDPDataXML();

    private GDPDataXML() {
    }

    private Document getDocGDP(String fileName) {
        Document document = null;
        try {
            ConstXML.copyTempFile(fileName);
            document = DataXMLUtil.loadDocument(ConstXML.tempFile);
            try {
            } finally {
                ConstXML.delTempFile();
            }
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        return document;
    }

    /**
     * читает данные по ГДП из файла (не сохрняет в БД !!!)
     * @param fileName имя файла xml с ГДП от графистов
     * @param distrEntity участок со станциями и перегонами
     * @return список TrainThread - поездов с расписанием по станциям и перегонам
     */
    public List<TrainThreadSheet> loadFromFile(String fileName, DistrEntity distrEntity) {
        List<TrainThreadSheet> listTrain = new ArrayList<>();

        Document doc = getDocGDP(fileName);
        boolean canLoade = false;
        if (doc != null) {
            // проверка на корректность по формату
            NodeList nodeList = doc.getElementsByTagName("DistrGDP");
            if (nodeList.getLength() == 1) {
                // проверка на корректность по названию участка
                String nameDistr = "";
                nodeList = doc.getElementsByTagName("distr");
                if (nodeList.getLength() == 1) {
                    Element item = (Element) nodeList.item(0);
                    nameDistr = item.getAttribute("name").trim();
                }
                if (!nameDistr.equals(distrEntity.getDistrName())) {
                    int n = JOptionPane.showConfirmDialog(null, "Вы действительно хотите загрузить ГДП для участка '" + distrEntity.getDistrName()
                            + "' из файла, где участок называется '" + nameDistr + "' ?", "Предупреждение", JOptionPane.YES_NO_OPTION);
                    canLoade = n == 0;
                } else {
                    canLoade = true;
                }

            } else {
                JOptionPane.showMessageDialog(null, "Ошибка чтения файла " + fileName + ", ГДП не загружен");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ошибка чтения файла " + fileName + ", подробности см. в logs");
        }

        if (canLoade) {
            // проверка наличия всех требуемых элементов участка
            Map<Integer, Integer> allSt = new HashMap<>();
            Map<Integer, Integer> allSp = new HashMap<>();

            List<String> listMess = checkAllDistrStruct(distrEntity, doc, allSt, allSp);

            if (listMess.size() > 0) {
                ListMessageDialog ds = new ListMessageDialog(null, "Предупреждение", true);
                ds.setText(listMess);
                ds.setVisible(true);
                int res = ds.getResult();
                canLoade = res == 1;
            }

            if (canLoade) {
                listTrain = loadDistrGDP(distrEntity, doc, allSt, allSp);
            }
        }

        return listTrain;
    }

    /**
     * проверка структуры участка
     * @param distr
     * @param doc
     * @param allSt
     * @param allSp
     * @return
     */
    private List<String> checkAllDistrStruct(DistrEntity distr, Document doc, Map<Integer, Integer> allSt, Map<Integer, Integer> allSp) {
        List<String> listMess = new ArrayList<>();
        checkAllSt(distr, doc, allSt, listMess);
        // checkAllSp(distr, doc, allSp, allSt, listMess);
        // не используется, поскольку фактически нужны только проходы по станциям.
        // нахождение на перегонах вычисляется только при расчете поездных показателей на листе !!!
        // (на основе данных проходов по станциям)
        return listMess;
    }

    /**
     * проверка наличия и соответствий станций (либо по ЕСР, либо по Экспресс)
     * @param distr
     * @param doc
     * @param allSt
     * @param listMess
     */
    private void checkAllSt(DistrEntity distr, Document doc, Map<Integer, Integer> allSt, List<String> listMess) {
        NodeList nodeList = doc.getElementsByTagName("Station");
        for (int i = 0; i < nodeList.getLength(); i++) {
            try {
                Element item = (Element) nodeList.item(i);
                String xmlName = item.getAttribute("name").trim();
                String esr = item.getAttribute("CodeESR").trim();
                String esr2 = item.getAttribute("CodeESR2").trim();
                String expr = item.getAttribute("CodeExpress").trim();
                int xmlID = Integer.parseInt(item.getAttribute("IDst"));
                int dbID = 0;
                allSt.put(xmlID, dbID);
                if (StationIdentification == ESR && "".equals(esr) || StationIdentification == EXPRESS && "".equals(expr)) {
                    listMess.add("В Файле станция " + xmlName + " не имеет кода");
                } else {
                    String mess = "";
                    if (StationIdentification == ESR) {
                        mess = "c кодом ЕСР '" + esr + "-" + esr2 + "'";
                    } else {
                        mess = "с кодом Экспресс '" + expr + "'";
                    }

                    DistrStation dst = null;
                    if (StationIdentification == ESR) {
                        dst = distr.getStationByESR(esr, esr2);
                    } else {
                        dst = distr.getStationByExpress(expr);
                    }

                    if (dst != null) {
                        dbID = dst.getStation().stationID;
                        allSt.put(xmlID, dbID);
                        if (!dst.getStation().name.equals(xmlName)) {
                            listMess.add("Название станции " + mess + " из файла " + xmlName + " не совпадает с названием в БД " + dst.getStation().name);
                        }
                    } else {
                        listMess.add("Станция " + xmlName + " " + mess + " не найдена на участке в БД ");
                    }

                }
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }

        }
    }

    /**
     * проверка соответствий перегонов (пока нигде не используется)
     * @param distr
     * @param doc
     * @param allSp
     * @param allSt
     * @param listMess
     */
    protected void checkAllSp(DistrEntity distr, Document doc, Map<Integer, Integer> allSp, Map<Integer, Integer> allSt, List<String> listMess) {
        NodeList nodeList = doc.getElementsByTagName("Span");
        for (int i = 0; i < nodeList.getLength(); i++) {
            try {
                Element item = (Element) nodeList.item(i);
                String xmlName = item.getAttribute("Name").trim();
                int xmlID = Integer.parseInt(item.getAttribute("IDspan"));
                int dbID = 0;

                DistrSpan ds = distr.getSpan(xmlName);
                if (ds != null) {
                    dbID = ds.getSpan().spanID;
                } else {
                    listMess.add("Перегон с названием " + xmlName + " не найден на участке в БД ");
                }

                allSp.put(xmlID, dbID);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /**
     * 
     * @param distrEntity
     * @param doc
     * @param allSt
     * @param allSp
     * @return
     */
    private List<TrainThreadSheet> loadDistrGDP(DistrEntity distrEntity, Document doc, Map<Integer, Integer> allSt, Map<Integer, Integer> allSp) {
        Map<Long, TrainThreadSheet> mapTrain = new HashMap<>();
        // список поездов
        NodeList nodeList = doc.getElementsByTagName("Distr_Train");
        for (int i = 0; i < nodeList.getLength(); i++) {
            try {
                Element item = (Element) nodeList.item(i);
                DistrTrain distrTrain = new DistrTrain();
                TrainThreadSheet train = new TrainThreadSheet(distrTrain);
                distrTrain.trainID = Long.parseLong(item.getAttribute("IDtrain"));
                distrTrain.distrID = distrEntity.getDistrID();
                distrTrain.categoryID = Integer.parseInt(item.getAttribute("IDcat")); // НО !!! категории с таким ID может уже не быть в БД !!!
                distrTrain.codeTR = Integer.parseInt(item.getAttribute("code"));
                distrTrain.nameTR = item.getAttribute("Name");
                int xmlColor = Integer.parseInt(item.getAttribute("colorTR"));
                distrTrain.colorTR = ColorConverter.convertInt(xmlColor);
                distrTrain.hidden = Boolean.parseBoolean(item.getAttribute("hidden").toLowerCase());
                distrTrain.ainscr = item.getAttribute("AInscr");
                distrTrain.showAI = !Boolean.parseBoolean(item.getAttribute("ShowCodeI").toLowerCase());
                distrTrain.lineStyle = Integer.parseInt(item.getAttribute("LineStyle"));
                distrTrain.widthTR = Integer.parseInt(item.getAttribute("widthTR"));
                mapTrain.put(distrTrain.trainID, train);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
        // необходимо обновить категории поездов во избежание потерь категорий !!!
        LocDAOFactory.getInstance().getDistrTrainDAO().updateTrainCat();

        // расписание поездов по станциям
        nodeList = doc.getElementsByTagName("LineSt_Tr");
        for (int i = 0; i < nodeList.getLength(); i++) {
            try {
                Element item = (Element) nodeList.item(i);
                TrainStation trainSt = new TrainStation();
                trainSt.trainID = Long.parseLong(item.getAttribute("IDtrain"));
                int xmlStID = Integer.parseInt(item.getAttribute("IDst"));
                trainSt.setTimeOn(item.getAttribute("occupyOn"));
                trainSt.setTimeOff(item.getAttribute("occupyOff"));
                trainSt.texStop = Integer.parseInt(item.getAttribute("TtexStop"));
                TrainThreadSheet train = mapTrain.get(trainSt.trainID);
                if (train != null) {
                    Integer stID = allSt.get(xmlStID);
                    if (stID != null) {
                        trainSt.stationID = stID;
                        train.getTrainSt().add(trainSt);
                    }
                }
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }

        // занятость перегонов
        nodeList = doc.getElementsByTagName("SpTOcc");
        for (int i = 0; i < nodeList.getLength(); i++) {
            try {
                Element item = (Element) nodeList.item(i);
                TrainSpan trainSp = new TrainSpan();
                trainSp.trainID = Long.parseLong(item.getAttribute("IDtrain"));
                int xmlSpID = Integer.parseInt(item.getAttribute("IDspan"));
                trainSp.setTimeOn(TimeConverter.timeToMinutes(item.getAttribute("occupyOn")));
                trainSp.setTimeOff(TimeConverter.timeToMinutes(item.getAttribute("occupyOff")));
                TrainThreadSheet train = mapTrain.get(trainSp.trainID);

                if (train != null) {
                    Integer spID = allSp.get(xmlSpID);
                    if (spID != null) {
                        trainSp.spanID = spID;
                        train.getTrainSp().add(trainSp);
                    }
                }
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }

        return new ArrayList<>(mapTrain.values());
    }

    public static GDPDataXML getInstance() {
        return instance;
    }

}
