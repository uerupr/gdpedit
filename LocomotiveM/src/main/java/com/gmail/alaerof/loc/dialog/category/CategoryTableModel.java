package com.gmail.alaerof.loc.dialog.category;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.dao.dto.Category;

public class CategoryTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "Наименование", "с №", "по №", "Сообщение" };
    private List<Category> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            Category el = list.get(row);
            switch (col) {
            case 0:
                return el.name;
            case 1:
                return el.trMin;
            case 2:
                return el.trMax;
            case 3:
                return el.type;
            default:
                return null;
            }
        }
        return null;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return true;
    }

    public List<Category> getList() {
        return list;
    }

    public void setList(List<Category> list) {
        this.list = list;
    }

}
