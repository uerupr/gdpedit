package com.gmail.alaerof.loc.mainframe.sheettree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.SheetDAO;
import com.gmail.alaerof.loc.entity.sheet.SheetEntity;
import com.gmail.alaerof.loc.application.LinkingLoc;
import com.gmail.alaerof.loc.application.LinkingLocMain;

public class ActionListenerDeleteSheet implements ActionListener {
    private static Logger logger = LogManager.getLogger(ActionListenerDeleteSheet.class);

    @Override
    public void actionPerformed(ActionEvent ev) {
        try {
            LinkingLoc f = LinkingLocMain.mainFrame;
            Object userObject = f.mainPaneBuilder.getSheetTree().getSelectedUserObject();
            if (userObject != null && userObject instanceof SheetEntity) {
                SheetEntity se = (SheetEntity) userObject;
                int x = JOptionPane.showConfirmDialog(LinkingLocMain.mainFrame,
                        "Вы действительно хотите удалить лист '" + se
                                + "'?");
                if (x == JOptionPane.YES_OPTION) {
                    LocDAOFactory fc = LocDAOFactory.getInstance();
                    SheetDAO dao = fc.getSheetDAO();
                    dao.deleteSheet(se.getSheet().name.trim());
                }
                f.mainPaneBuilder.updateTree();
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

}
