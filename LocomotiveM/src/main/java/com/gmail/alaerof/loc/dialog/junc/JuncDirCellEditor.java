package com.gmail.alaerof.loc.dialog.junc;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;

public class JuncDirCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
    private static final long serialVersionUID = 1L;
    private Direction dir;
    private List<Direction> list;

    public JuncDirCellEditor() {
        list = LocDAOFactory.getInstance().getDirectionDAO().loadAllDirection();
    }

    @Override
    public Object getCellEditorValue() {
        return dir;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof Direction) {
            this.dir = (Direction)value;
        }

        JComboBox<Direction> combo = new JComboBox<>();
        for (Direction d : list) {
            combo.addItem(d);
        }

        combo.setSelectedItem(this.dir);
        combo.addActionListener(this);

        if (isSelected) {
            combo.setBackground(table.getSelectionBackground());
        } else {
            combo.setBackground(table.getSelectionForeground());
        }

        return combo;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        @SuppressWarnings("unchecked")
        JComboBox<Direction> combo = (JComboBox<Direction>) event.getSource();
        this.dir = (Direction) combo.getSelectedItem();
    }
}
