package com.gmail.alaerof.loc.dao.dto;

public class Direction extends AbstractDTO implements Comparable<Direction>{
    public int dirID;
    public String name;
    public int orderNum;

    @Override
    public String toString() {
        return name;
    }

 
    @Override
    public int getID() {
        return dirID;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + dirID;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + orderNum;
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Direction other = (Direction) obj;
        if (dirID != other.dirID)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (orderNum != other.orderNum)
            return false;
        return true;
    }


    @Override
    public int compareTo(Direction o) {
        if(this.orderNum == o.orderNum){
            return this.name.compareToIgnoreCase(o.name);
        }
        return this.orderNum - o.orderNum;
    }
    
    

}
