package com.gmail.alaerof.loc.dao.factory;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.JOptionPane;

import com.gmail.alaerof.loc.dao.impl.DirectionDAO;
import com.gmail.alaerof.loc.dao.impl.DistrDAO;
import com.gmail.alaerof.loc.dao.impl.DistrTrainDAO;
import com.gmail.alaerof.loc.dao.impl.DistrTurnDAO;
import com.gmail.alaerof.loc.dao.impl.JuncDAO;
import com.gmail.alaerof.loc.dao.impl.LinkTrainDAO;
import com.gmail.alaerof.loc.dao.impl.LocalDAO;
import com.gmail.alaerof.loc.dao.impl.SeriesLocDAO;
import com.gmail.alaerof.loc.dao.impl.ServerDAO;
import com.gmail.alaerof.loc.dao.impl.SheetDAO;
import com.gmail.alaerof.loc.dao.impl.SpanDAO;
import com.gmail.alaerof.loc.dao.impl.StationDAO;
import org.apache.log4j.LogManager;


public class LocDAOFactory {
    private static final String FILE_SERVER = "res" + File.separator + "dbserver.properties";
    private static final String FILE_LOCAL = "res" + File.separator + "dblocal.properties";
    private Connection cnServer;
    private Connection cnLocal;
    private ServerDAO serverDAO;
    private LocalDAO localDAO;
    private DirectionDAO directionDAO;
    private DistrDAO distrDAO;
    private StationDAO stationDAO;
    private JuncDAO juncDAO;
    private SpanDAO spanDAO;
    private DistrTrainDAO distrTrainDAO;
    private SheetDAO sheetDAO;
    private SeriesLocDAO seriesLocDAO;
    private LinkTrainDAO linkTrainDAO;
    private DistrTurnDAO distrTurnDAO;

    private static class FactoryHolder {
        static final LocDAOFactory factoryInstance = new LocDAOFactory();
    }

    public static LocDAOFactory getInstance() {
        return FactoryHolder.factoryInstance;
    }

    private LocDAOFactory() {
        if (ConnectionBuilder.isServerGDP(FILE_SERVER)) {
            try {
                cnServer = ConnectionBuilder.getConnectionServer(FILE_SERVER);
            } catch (SQLException e) {
                String mess = "Ошибка открытия соединения с БД на сервере:\n " + e.toString();
                LogManager.getLogger(LocDAOFactory.class).error(mess, e);
                // JOptionPane.showMessageDialog(null,
                // WrapText.getWrapText(mess), "Предупреждение",
                // JOptionPane.OK_OPTION);
            }
        }
        try {
            if (ConnectionBuilder.inprocess(FILE_LOCAL)) {
                ConnectionBuilder.startServer();
            }
            cnLocal = ConnectionBuilder.getConnection(FILE_LOCAL);
        } catch (SQLException e) {
            String mess = "Ошибка открытия соединения с локальной БД " + e.toString();
            LogManager.getLogger(LocDAOFactory.class).error(mess, e);
            JOptionPane.showMessageDialog(null, mess, "Предупреждение", JOptionPane.OK_OPTION);
        }
    }

    public void closeConnections() {
        if (cnServer != null) {
            try {
                cnServer.close();
            } catch (SQLException e) {
                LogManager.getLogger(this.getClass()).error("Ошибка закрытия соединения с БД: " + e.toString(), e);
            }
        }

        if (cnLocal != null) {
            try {
                cnLocal.close();
            } catch (SQLException e) {
                LogManager.getLogger(this.getClass()).error("Ошибка закрытия соединения с БД: " + e.toString(), e);
            }
        }
    }

    public ServerDAO getServerDAO() {
        if (serverDAO == null) {
            serverDAO = new ServerDAO(cnServer);
        }
        return serverDAO;
    }

    public DirectionDAO getDirectionDAO() {
        if (directionDAO == null) {
            directionDAO = new DirectionDAO(cnLocal);
        }
        return directionDAO;
    }

    public StationDAO getStationDAO() {
        if (stationDAO == null) {
            stationDAO = new StationDAO(cnLocal);
        }
        return stationDAO;
    }

    public LocalDAO getLocalDAO() {
        if (localDAO == null) {
            localDAO = new LocalDAO(cnLocal);
        }
        return localDAO;
    }

    public DistrDAO getDistrDAO() {
        if (distrDAO == null) {
            distrDAO = new DistrDAO(cnLocal);
        }
        return distrDAO;
    }

    public SpanDAO getSpanDAO() {
        if (spanDAO == null) {
            spanDAO = new SpanDAO(cnLocal);
        }
        return spanDAO;
    }

    public DistrTrainDAO getDistrTrainDAO() {
        if (distrTrainDAO == null) {
            distrTrainDAO = new DistrTrainDAO(cnLocal);
        }
        return distrTrainDAO;
    }

    public SheetDAO getSheetDAO() {
        if (sheetDAO == null) {
            sheetDAO = new SheetDAO(cnLocal);
        }
        return sheetDAO;
    }

    public SeriesLocDAO getSeriesLocDAO() {
        if (seriesLocDAO == null) {
            seriesLocDAO = new SeriesLocDAO(cnLocal);
        }
        return seriesLocDAO;
    }

    public LinkTrainDAO getLinkTrainDAO() {
        if (linkTrainDAO == null) {
            linkTrainDAO = new LinkTrainDAO(cnLocal);
        }
        return linkTrainDAO;
    }

    public JuncDAO getJuncDAO() {
        if (juncDAO == null) {
            juncDAO = new JuncDAO(cnLocal);
        }
        return juncDAO;
    }

    public DistrTurnDAO getDistrTurnDAO() {
        if(distrTurnDAO == null){
            distrTurnDAO = new DistrTurnDAO(cnLocal);
        }
        return distrTurnDAO;
    }
}
