package com.gmail.alaerof.loc.tab;

import java.awt.event.ActionListener;

import javax.swing.JComponent;

public class TitledTab {
    private String title = "";
    private JComponent tab;
    private ActionListener actionListener;
    
    public TitledTab() {
        super();
    }
    public TitledTab(JComponent tab) {
        super();
        this.tab = tab;
    }
    
    public TitledTab(String title, JComponent tab) {
        super();
        this.title = title;
        this.tab = tab;
    }
    
    public TitledTab(String title, JComponent tab, ActionListener actionListener) {
        super();
        this.title = title;
        this.tab = tab;
        this.setActionListener(actionListener);
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public JComponent getTab() {
        return tab;
    }
    
    public void setTab(JComponent tab) {
        this.tab = tab;
    }
    
    public ActionListener getActionListener() {
        return actionListener;
    }
    
    public void setActionListener(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

}
