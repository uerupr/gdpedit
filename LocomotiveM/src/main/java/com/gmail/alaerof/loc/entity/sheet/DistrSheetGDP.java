package com.gmail.alaerof.loc.entity.sheet;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.DistrTrainDAO;
import com.gmail.alaerof.loc.entity.DistrEntity;
import com.gmail.alaerof.loc.entity.train.TrainThread;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditPanel;

public class DistrSheetGDP {
    private DistrEntity distrEntity;
    private List<TrainThreadSheet> listTrain = new ArrayList<>();
    

    public DistrSheetGDP(DistrEntity distrEntity) {
        this.setDistrEntity(distrEntity);
    }

    public DistrEntity getDistrEntity() {
        return distrEntity;
    }

    public void setDistrEntity(DistrEntity distrEntity) {
        this.distrEntity = distrEntity;
    }

    public List<TrainThreadSheet> getListTrain() {
        return listTrain;
    }

    public void setListTrain(List<TrainThreadSheet> listTrain) {
        if (listTrain != null)
            this.listTrain = listTrain;
    }

    public String getDistrName() {
        return distrEntity.getDistrName();
    }

    /**
     * загружает ГДП из БД 
     * каждый поезд должент знать порядковый номер своего участка на листе (для сортировки!!!)
     * 
     * @param sheetDistrNum порядковый номер участка на листе
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void loadGDPFromDB(int sheetDistrNum) {
        if (distrEntity != null) {
            DistrTrainDAO dtDAO = LocDAOFactory.getInstance().getDistrTrainDAO();
            // тут загружаются чисто данные из БД
            List<TrainThread> list = dtDAO.loadGDP(distrEntity.getDistrID(), new TrainThreadSheet());
            listTrain = new ArrayList(list);
            // каждый поезд должент знать порядковый номер своего участка на листе (для сортировки!!!)
            for (int i = 0; i < listTrain.size(); i++) {
                TrainThreadSheet tr = listTrain.get(i);
                tr.setSheetDistrNum(sheetDistrNum);
            }
        }
    }

    /** построение и отображение списка поездов */
    public void drawGDP(SheetGDPEditPanel sheetGDPEditPanel, int sheetDistrNum) {
        if (listTrain.size() != 0) {
            for (int i = 0; i < listTrain.size(); i++) {
                TrainThreadSheet tr = listTrain.get(i);
                tr.removeAllFromOwner();
            }
            sheetGDPEditPanel.repaint();
        }
        loadGDPFromDB(sheetDistrNum);
        //selected = null;
        for (int i = 0; i < listTrain.size(); i++) {
            TrainThreadSheet tr = listTrain.get(i);
            tr.buildThread(sheetGDPEditPanel);
        }
    }
    
//    /**
//     * устанавливает на участке selected поезд в переданное значение
//     * (train или null), предварительно снимая выделение, если оно было
//     * @param trainThread значение для установки
//     */
//
//    public void setSelected(TrainThreadSheet trainThread) {
//        if (selected != null && !selected.equals(trainThread)) {
//            selected.setSelected(false);
//        }
//        selected = trainThread;
//    }
//
//    public TrainThreadSheet getSelectedTrain() {
//        return selected;
//    }

    public TrainThreadSheet getTrainThreadSheet(int codeTR, String nameTR) {
        for (TrainThreadSheet tr : listTrain) {
            String name = tr.getDistrTrain().nameTR.trim();
            nameTR = nameTR.trim();
            if (tr.getDistrTrain().codeTR == codeTR && nameTR.equals(name)) {
                return tr;
            }
        }
        return null;
    }

    /**
     * отрисовка ГДП на канве
     * @param grGDP канва
     */
    public void drawGDPImage(Graphics grGDP, int gapLeft, int gapTop) {
        for (TrainThreadSheet tr : listTrain) {
            tr.drawInImage(grGDP, gapLeft, gapTop);
        }
    }
}
