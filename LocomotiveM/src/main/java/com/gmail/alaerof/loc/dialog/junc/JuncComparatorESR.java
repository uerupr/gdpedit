package com.gmail.alaerof.loc.dialog.junc;

import java.util.Comparator;

import com.gmail.alaerof.loc.dao.dto.Junc;

public class JuncComparatorESR implements Comparator<Junc>{

    @Override
    public int compare(Junc j1, Junc j2) {
        return j1.codeESR.compareTo(j2.codeESR);
    }

}
