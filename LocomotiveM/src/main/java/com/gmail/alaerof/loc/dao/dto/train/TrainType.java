package com.gmail.alaerof.loc.dao.dto.train;

public enum TrainType {
    gr, pass, town;

    public String getString() {
        switch (this) {
        case gr:
            return "грузовой";
        case pass:
            return "пассажирский";
        case town:
            return "пригородный";
        default:
            return null;
        }
    }

    public static TrainType getType(String typeValue) {
        switch (typeValue) {
        case "грузовой":
            return gr;
        case "пассажирский":
            return pass;
        case "пригородный":
            return town;
        case "региональный":
            return town;
        default:
            return null;
        }
    }
}
