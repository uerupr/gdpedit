package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;

public class DistrDAO {
    private static Logger logger = LogManager.getLogger(DistrDAO.class);
    private Connection cnLocal;

    public DistrDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    public int insertListDistr(List<Distr> list) {
        String sql = "INSERT INTO distr (distrID, dirID, name) VALUES (?, ?, ?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Distr d = list.get(i);
                st.setInt(1, d.distrID);
                st.setInt(2, d.dirID);
                st.setString(3, d.name);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    public int updateListDistr(List<Distr> list) {
        String sql = "UPDATE distr SET dirID = ?, name = ? WHERE distrID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Distr d = list.get(i);
                st.setInt(1, d.dirID);
                st.setString(2, d.name);
                st.setInt(3, d.distrID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    private List<Distr> loadListDistr(String sql) {
        List<Distr> list = new ArrayList<>();
        sql = "SELECT distrID, dirID, name, electric, fileName, juncB, juncE " + sql;
        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Distr d = new Distr();
                    d.distrID = rs.getInt("distrID");
                    d.dirID = rs.getInt("dirID");
                    d.name = rs.getString("name").trim();
                    d.electric = rs.getBoolean("electric");
                    d.fileName = rs.getString("fileName");
                    if (d.fileName != null) {
                        d.fileName = d.fileName.trim();
                    }
                    d.juncB = rs.getInt("juncB");
                    d.juncE = rs.getInt("juncE");
                    list.add(d);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        fillJunc(list);
        return list;
    }

    public List<Distr> loadListDistr(int juncID) {
        String sql = " FROM distr " + "WHERE distrID IN (SELECT distrID FROM Scale WHERE spanID IN ("
                + "SELECT spanID FROM Span WHERE stB IN(SELECT stationID FROM JuncSt WHERE juncID = " + juncID + ") "
                + "OR stE IN (SELECT stationID FROM JuncSt WHERE juncID = " + juncID + ")))  ORDER BY name";

        return loadListDistr(sql);
    }

    public List<Distr> loadAllDistr() {
        String sql = " FROM distr ORDER BY name";
        return loadListDistr(sql);
    }

    public List<Distr> getListDistr(int dirID) {
        String sql = " FROM distr WHERE dirID=" + dirID
                + " ORDER BY name";
        return loadListDistr(sql);
    }

    public Distr loadDistr(int distrID) {
        Distr d = null;
        String sql = " FROM distr WHERE distrID = " + distrID;
        List<Distr> list = loadListDistr(sql);
        if (list.size() > 0) {
            d = list.get(0);
            if (list.size() > 1) {
                logger.error("more than 1 distr for distrID=" + distrID);
            }
        }
        return d;
    }

    public int updateFileName(Distr distr) {
        String sql = "UPDATE distr SET fileName = ? WHERE distrID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            if (distr.fileName == null)
                st.setNull(1, java.sql.Types.NULL);
            else
                st.setString(1, distr.fileName);
            st.setInt(2, distr.distrID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }
    

    public int updateDistrJunc(Distr distr) {
        String sql = "UPDATE distr SET juncB = ?, juncE = ? WHERE distrID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, distr.juncB);
            st.setInt(2, distr.juncE);
            st.setInt(3, distr.distrID);
            count = st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }
    
    public void fillJunc(List<Junc> listJ, Distr distr){
        for(Junc junc: listJ){
            if(junc.juncID == distr.juncB){
                distr.setJuncBFull(junc);
            }
            if(junc.juncID == distr.juncE){
                distr.setJuncEFull(junc);
            }
        }
    }
    
    public void fillJunc(List<Distr> list){
        List<Junc> listJ = LocDAOFactory.getInstance().getJuncDAO().loadAllJunc();
        for(Distr distr: list){
            fillJunc(listJ, distr);
        }
    }
    
    public String getNameDistr(List<Distr> listDistr, int distrID) {
        if (listDistr != null) {
            for (Distr distr : listDistr) {
                if (distr.distrID == distrID) {
                    return distr.name;
                }
            }
        }
        return "";
    }

    public Map<Integer, Distr> getMapAllDistr() {
        Map<Integer, Distr> map = new HashMap<>();
        List<Distr> list = loadAllDistr();
        for(Distr distr: list){
            map.put(distr.distrID, distr);
        }
        return map;
    }
    
    public Distr getDistrByName(List<Distr> listDistr, String name) {
        if (listDistr != null) {
            for (Distr distr : listDistr) {
                if (distr.name.equals(name)) {
                    return distr;
                }
            }
        }
        return null;
    }
    
    public Distr getDistrByName(Map<Integer, Distr> map, String name) {
        List<Distr> listDistr = new ArrayList<>(map.values());
        return getDistrByName(listDistr, name);
    }
}
