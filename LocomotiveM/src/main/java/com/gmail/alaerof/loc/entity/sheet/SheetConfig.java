package com.gmail.alaerof.loc.entity.sheet;

import java.awt.Color;
import java.awt.Font;
import java.awt.Point;

public class SheetConfig {
    /** пикселей в мин, гор */
    public int picTimeHor = 3;
    
    // --- отступы ---
    /** от краев по высоте, отступ в пикселях */
    public int indentVert = 15;
    /** от краев по ширине, отступ в пикселях */
    public int indentHor = 10;
    /** минимальное расст. между станциями */
    public int minGapSt = 20;
    /** расстояние между линиями увязок */
    public int gapLine = 15;    
    /** номер поезда (нечет) от линии станции, отступ в пикселях */
    public int stepCodeO = 15;
    /** номер поезда (чет) от линии станции, отступ в пикселях */
    public int stepCodeE = 15;
    /** минуты прибытия от линии станции, отступ в пикселях */
    public int stepTimeOn = 10;
    /** минуты отправления от линии станции, отступ в пикселях */
    public int stepTimeOff = 10;
    
    // --- цвет, шрифт ---
    /** цвет сетки графика */
    public Color gdpGridColor = new Color(75, 75, 0);
    /** часы, шрифт */
    public Font gdpHourFont = new Font("Sans Serif", Font.BOLD, 18);
    /** название р.п., шрифт */
    public Font stationFont = new Font("Sans Serif", 0, 14);
    /** надпись поезда (перегон), шрифт */
    public Font trainCaptionSpFont = new Font("Times New Roman", Font.BOLD, 14);
    /** надпись поезда (станция), шрифт */
    public Font trainCaptionStFont = new Font("Times New Roman", 0, 10);
    /** шрифт минут прибытия/отправления */
    public Font trainArrDepTimeFont = new Font("Times New Roman", 0, 10);;
    
    // --- расчет ---
    /** высота панели ГДП в пикселях */
    public int hGDP;
    /** высота панели часов в пикселях */
    public int hHour = 30;
    /** ширина панели левой части в пикселях */
    public int wLeft = 150;
    /** ширина панели ГДП в пикселях */
    public int wGDP = 24 * 60 * picTimeHor + indentHor*2;
    /** левая верхняя точка поля ГДП */
    public Point ltGDP = new Point(0,0);
    /** правая нижняя точка поля ГДП */
    public Point rbGDP = new Point(0,0);
    
    public int getTopGap(){
        return (int) ltGDP.getY();
    }
    
    public int getLeftGap(){
        return (int) ltGDP.getX();
    }
}
