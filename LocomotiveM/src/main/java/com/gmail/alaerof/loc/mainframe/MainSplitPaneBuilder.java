package com.gmail.alaerof.loc.mainframe;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditTabPane;
import com.gmail.alaerof.loc.mainframe.sheettree.SheetTree;

public class MainSplitPaneBuilder {
    private JPanel left = new JPanel();
    private JPanel right = new JPanel();
    private SheetGDPEditTabPane sheetGDPEditTabPane; 
    private SheetTree sheetTree; 
    private JSplitPane pane; 
    
    public JSplitPane buildPane(){
        sheetGDPEditTabPane = new SheetGDPEditTabPane();
        sheetTree = new SheetTree(sheetGDPEditTabPane);
        
        right.setLayout(new BorderLayout());
        right.add(sheetTree.getTreePanel(), BorderLayout.CENTER);
        left.setLayout(new BorderLayout());
        left.add(sheetGDPEditTabPane, BorderLayout.CENTER);
        pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, right ,left);
        pane.setOneTouchExpandable(true);
        pane.getLeftComponent().setMinimumSize(new Dimension(0, 0));
        pane.getLeftComponent().setPreferredSize(new Dimension(300, 0));
        return pane;
    }
    
    public SheetTree getSheetTree(){
        return sheetTree;
    }
    
    public void updateTree(){
        right.removeAll();
        right.add(sheetTree.updateTree(), BorderLayout.CENTER);
        int d = pane.getDividerLocation();
        pane.setDividerLocation(d+1);
        pane.setDividerLocation(d);
    }

    public SheetGDPEditTabPane getSheetGDPEditTabPane() {
        return sheetGDPEditTabPane;
    }

}
