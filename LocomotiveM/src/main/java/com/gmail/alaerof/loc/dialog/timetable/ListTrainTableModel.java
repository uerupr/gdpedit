package com.gmail.alaerof.loc.dialog.timetable;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;

public class ListTrainTableModel  extends AbstractTableModel{
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "Номер", "Назв.", "Скр.", "Надпись" };
    private List<TrainThreadSheet> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (list != null) {
            TrainThreadSheet el = list.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return el.getDistrTrain().codeTR;
            case 1:
                return el.getDistrTrain().nameTR;
            case 2:
                return el.getDistrTrain().hidden ? "*" : "";
            case 3:
                return el.getDistrTrain().ainscr.trim();
            }
        }
        return null;
    }

    public List<TrainThreadSheet> getList() {
        return list;
    }

    public void setList(List<TrainThreadSheet> list) {
        this.list = list;
    }
   
}
