package com.gmail.alaerof.loc.entity.sheet;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gmail.alaerof.loc.dao.dto.sheet.*;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.entity.train.TrainThreadComparatorByCode;
import com.gmail.alaerof.loc.util.CalcString;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditPanel;

public class SheetEntity {
    /** ID и название */
    private Sheet sheet;
    /** список участков (в каждом свой ГДП) */
    private List<SheetDistrEntity> listDistr;
    /** список узлов (только видимые!!!), 
     * формируется одновременно с инициацией участков */
    private List<SheetJuncEntity> listJunc;
    /** общие настройки отображения */
    private SheetConfig sConf = new SheetConfig();
    /** контейнер для листа */
    private SheetGDPEditPanel sheetGDPEditPanel;

    /** ID и название */
    public Sheet getSheet() {
        return sheet;
    }
    /** ID и название */
    public void setList(Sheet sheet) {
        this.sheet = sheet;
    }
    /** список участков (в каждом свой ГДП) */
    public List<SheetDistrEntity> getListDistr() {
        return listDistr;
    }
    /** список участков (в каждом свой ГДП) */
    public void setListDistr(List<SheetDistrEntity> listDistr) {
        this.listDistr = listDistr;
    }
    /** список узлов (только видимые!!!) */
    public List<SheetJuncEntity> getListJunc() {
        return listJunc;
    }
    /** список узлов (только видимые!!!) */
    public void setListJunc(List<SheetJuncEntity> listJunc) {
        this.listJunc = listJunc;
    }
    /**
     * возвращает {@link SheetDistrEntity} по ид участка
     * @param distrID ид участка
     * @return {@link SheetDistrEntity} объект или null
     */
    public SheetDistrEntity getSheetDistrEntity(int distrID) {
        SheetDistrEntity sde = null;
        for (int i = 0; i < listDistr.size(); i++) {
            sde = listDistr.get(i);
            if (sde.getSheetDistr().distrID == distrID) {
                return sde;
            }
        }
        return null;
    }
    /** возвращает общие настройки отображения листа*/
    public SheetConfig getSheetConfig() {
        return sConf;
    }
    /** узлы над участком на листе */
    protected List<SheetJuncEntity> getJuncsBeforeDistr(int distrID) {
        List<SheetJuncEntity> list = new ArrayList<>();
        for (SheetJuncEntity sje : listJunc) {
            if (sje.getDistrIDAfter() == distrID) {
                list.add(sje);
            }
        }
        return list;
    }
    /** узлы под участком на листе */
    protected List<SheetJuncEntity> getJuncsAfterDistr(int distrID) {
        List<SheetJuncEntity> list = new ArrayList<>();
        for (SheetJuncEntity sje : listJunc) {
            if (sje.getDistrIDBefore() == distrID) {
                list.add(sje);
            }
        }
        return list;
    }

    /** расчет высоты основного поля  */
    public void calcImageSize() {
        sConf.hGDP = sConf.indentVert * 2;
        for (int i = 0; i < listDistr.size(); i++) {
            SheetDistrEntity de = listDistr.get(i);
            int gapTop = 0, gapBot = 0;
            if (i == 0) {
                List<SheetJuncEntity> list = getJuncsBeforeDistr(de.getSheetDistr().distrID);
                for (SheetJuncEntity sje : list) {
                    gapTop += (sje.getJunc().lineCount + 1) * sConf.gapLine;
                }
            }
            List<SheetJuncEntity> list = getJuncsAfterDistr(de.getSheetDistr().distrID);
            for (SheetJuncEntity sje : list) {
                gapBot += (sje.getJunc().lineCount + 1) * sConf.gapLine;
            }
            if (gapBot == 0) {
                gapBot = sConf.gapLine;
            }
            sConf.hGDP += gapTop + de.getDistrLen() + gapBot;
        }
    }
    
    /**
     * расчет вертикальных и горизонтальных координат основного поля ГДП 
     * в соответствии с отступами
     * @param topGap отступ сверху
     * @param leftGap отступ слева
     */
    public void calcXY(int topGap, int leftGap){
        calcGDPGridX(topGap, leftGap);
        calcDistrY(topGap);
    }
    /**
     * расчет левой верхней и правой нижней точек поля ГДП
     * @param topGap отступ сверху (часы)
     * @param leftGap отступ слева (названия станций)
     */
    private void calcGDPGridX(int topGap, int leftGap) {
        sConf.ltGDP = new Point(leftGap + sConf.indentHor, topGap - sConf.indentVert);
        sConf.rbGDP = new Point(leftGap + sConf.wGDP - sConf.indentHor, topGap + sConf.hGDP - sConf.indentVert);
    }
    /** 
     * расчет верхней границы по Y для каждого участка
     * @param topGap отступ сверху
     */
    private void calcDistrY(int topGap) {
        int y0 = topGap + sConf.indentVert;
        for (int i = 0; i < listDistr.size(); i++) {
            SheetDistrEntity de = listDistr.get(i);
            int gapTop = 0, gapBot = 0;
            if (i == 0) {
                List<SheetJuncEntity> list = getJuncsBeforeDistr(de.getSheetDistr().distrID);
                for (SheetJuncEntity sje : list) {
                    sje.setY(y0 + gapTop + sConf.gapLine);
                    gapTop += (sje.getJunc().lineCount + 1) * sConf.gapLine;
                }
            }
            y0 += gapTop;
            de.calcStY(y0);
            List<SheetJuncEntity> list = getJuncsAfterDistr(de.getSheetDistr().distrID);
            y0 += de.getDistrLen();
            for (SheetJuncEntity sje : list) {
                sje.setY(y0 + gapBot + sConf.gapLine);
                gapBot += (sje.getJunc().lineCount + 1) * sConf.gapLine;
            }
            if (gapBot == 0) {
                gapBot = sConf.gapLine;
            }
            y0 += gapBot;
        }
    }

    /**
     * отрисовка сетки графика
     * расчет
     * @param grGDP поверхность рисования
     * @param topGap отступ сверху
     * @param leftGap отступ слева
     * @param bgColor цвет фона
     */
    public void drawGDPGrid(Graphics grGDP, int topGap, int leftGap, Color bgColor) {
        Graphics2D gGDP = (Graphics2D) grGDP;
        // цвет фона
        gGDP.setColor(bgColor);
        gGDP.fillRect(leftGap, topGap, sConf.wGDP, sConf.hGDP);
        for (int i = 0; i < listDistr.size(); i++) {
            SheetDistrEntity de = listDistr.get(i);
            de.drawGDPGrid(grGDP, bgColor, sConf);
        }
    }

    /***
     * отрисовка левой части (названия станций)
     * @param grLeft поверхность рисования
     * @param topGap отступ сверху
     * @param leftGap отступ слева
     * @param bgColor цвет фона
     */
    public void drawLeft(Graphics grLeft, int topGap, int leftGap, Color bgColor) {
        Graphics2D gLeft = (Graphics2D) grLeft;
        // цвет фона
        gLeft.setColor(bgColor);
        gLeft.fillRect(leftGap, topGap, sConf.wLeft, sConf.hGDP);
        for (int i = 0; i < listDistr.size(); i++) {
            SheetDistrEntity de = listDistr.get(i);
            de.drawLeft(grLeft, sConf);
        }
    }
    /**
     * отрисовка верхней части (часы)
     * @param grHour поверхность рисования
     * @param topGap отступ сверху
     * @param leftGap отступ слева
     * @param bgColor цвет фона
     */
    public void drawHour(Graphics grHour, int topGap, int leftGap, Color bgColor) {
        Graphics2D gHour = (Graphics2D) grHour;
        // цвет фона
        gHour.setColor(bgColor);
        gHour.fillRect(leftGap, topGap, sConf.wGDP, sConf.hHour);

        // --- линии сетки ---
        // цвет сетки
        int hCount = 24;
        int hb = 0;
        gHour.setColor(sConf.gdpGridColor);
        int k = 0;
        gHour.setFont(sConf.gdpHourFont);
        int heightHour = CalcString.getStringH(sConf.gdpHourFont);

        while (k <= hCount * 60) {
            int x0 = (int) (sConf.ltGDP.x + k * sConf.picTimeHor);

            if (k % 60 == 0) {
                // подписи часа
                int h = hb + k / 60;
                if (h > 24)
                    h -= 24;
                String tx = new Integer(h).toString();
                int hw = CalcString.getStringW(sConf.gdpHourFont, tx);
                gHour.drawString(tx, x0 - hw / 2, heightHour);
            }
            k += 60;
        }

    }
    /**
     * отрисовка верхнего левого угла (шапка таблицы в левой части)
     * @param gGDP поверхность рисования
     * @param topGap отступ сверху
     * @param leftGap отступ слева
     * @param bgColor цвет фона
     */
    public void drawLeftHead(Graphics gGDP, int topGap, int leftGap, Color bgColor) {
        Graphics2D gHour = (Graphics2D) gGDP;
        // цвет фона
        gHour.setColor(bgColor);
        gHour.fillRect(leftGap, topGap, sConf.wLeft, sConf.hHour);
    }
    
    @Override
    public String toString() {
        return sheet.name;
    }

    /**
     * обновляет отображения ГДП всех своих участков и узлов
     * @param topGap отступ сверху /не используется/
     * @param leftGap отступ слева /не используется/
     * @param backgroundColor цвет фона /не используется/
     */
    public void drawGDP() {
        for (int i = 0; i < listDistr.size(); i++) {
            SheetDistrEntity de = listDistr.get(i);
            de.drawGDP(sheetGDPEditPanel);
        }
        for (int i = 0; i < listJunc.size(); i++) {
            SheetJuncEntity je = listJunc.get(i);
            je.drawGDP(sheetGDPEditPanel);
        }
    }
    
    /**
     * отрисовка поездов и увязок на некой общей канве
     * @param grGDP канва
     */
    public void drawGDPImage(Graphics grGDP, int gapLeft, int gapTop){
        for (int i = 0; i < listDistr.size(); i++) {
            SheetDistrEntity de = listDistr.get(i);
            de.drawGDPImage(grGDP, gapLeft, gapTop);
        }
        for (int i = 0; i < listJunc.size(); i++) {
            SheetJuncEntity je = listJunc.get(i);
            je.drawGDPImage(grGDP, gapLeft, gapTop);
        }
    }

    public SheetGDPEditPanel getSheetGDPEditPanel() {
        return sheetGDPEditPanel;
    }

    public void setSheetGDPEditPanel(SheetGDPEditPanel sheetGDPEditPanel) {
        this.sheetGDPEditPanel = sheetGDPEditPanel;
    }

    /** возвращает список всех поездов листа */
    public List<TrainThreadSheet> getListTrain() {
        List<TrainThreadSheet> listAll = new ArrayList<>();
        for (SheetDistrEntity shd : listDistr) {
            List<TrainThreadSheet> list = new ArrayList<>(shd.getListTr());
            // начальная сортировка
            Collections.sort(list, new TrainThreadComparatorByCode());
            if (list != null) {
                listAll.addAll(list);
            }
        }
        return listAll;
    }

    /** возвращает список всех станций листа */
    public List<SheetDistrSt> getListSheetDistrSt() {
        List<SheetDistrSt> listAll = new ArrayList<>();
        for (SheetDistrEntity shd : listDistr) {
            List<SheetDistrSt> list = new ArrayList<>(shd.getListSt());
            listAll.addAll(list);
        }
        return listAll;
    }
//    /**
//     * устанавливает на указанном участке selected поезд в переданное значение
//     * (train или null)
//     * @param trainThread значение для установки
//     * @param distrID код участка
//     */
//    public void setSelected(TrainThreadSheet trainThread, int distrID) {
//        for (SheetDistrEntity shd : listDistr) {
//            if (shd.getSheetDistr().distrID == distrID) {
//                shd.setSelected(trainThread);
//            }
//        }
//    }
//
//    public List<TrainThreadSheet> getSelectedTrains() {
//        List<TrainThreadSheet> list = new ArrayList<>();
//        for (SheetDistrEntity shd : listDistr) {
//            TrainThreadSheet tr = shd.getSelectedTrain();
//            if (tr != null) {
//                list.add(tr);
//            }
//        }
//        return list;
//    }

    public TrainThreadSheet getTrainThreadSheet(int distrID, int codeTR, String nameTR) {
        SheetDistrEntity sde = getSheetDistrEntity(distrID);
        if (sde != null) {
            return sde.getTrainThreadSheet(codeTR, nameTR);
        }
        return null;
    }

    /** сохранение в БД вертикальных позиций увязок */
    public void saveGDP() {
        for (int i = 0; i < listJunc.size(); i++) {
            SheetJuncEntity je = listJunc.get(i);
            je.saveGDP();
        }
    }
    /**
     * расчет показателей поездов
     * @return количество поездов, для которых есть вероятность ошибки в расчетах
     */
    public int calculateTrainResults() {
        int err = 0;
        for (SheetDistrEntity shd : listDistr) {
            err += shd.calculateTrainResults();
        }
        return err;
    }

}
