package com.gmail.alaerof.loc.util;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FileHandler {
    public static File currentDir = null;

    /**
     * диалоговое окно открытия/сохранения файла с историей локаций
     * @param open диалог для открытия(true) или сохранения (false)
     * @param fileNameExtFilter фильтр расширений
     * @param defaultFileName название файла по умолчанию
     * @return файл File
     */
    public static File chooseFile(boolean open, FileNameExtensionFilter fileNameExtFilter, String defaultFileName, JFrame frame) {
        File file = null;
        JFileChooser chooser = null;
        if (currentDir != null) {
            chooser = new JFileChooser(currentDir);
        } else {
            chooser = new JFileChooser();
        }
        // FileNameExtensionFilter filter = new FileNameExtensionFilter("xml files", "xml");
        chooser.setFileFilter(fileNameExtFilter);
        chooser.setSelectedFile(new File(defaultFileName));
        int returnVal = 0;
        if (open) {
            returnVal = chooser.showOpenDialog(frame);
        } else {
            returnVal = chooser.showSaveDialog(frame);
        }
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
        }
        if (!open && file != null && file.exists()) {
            int x = JOptionPane.showConfirmDialog(frame, "файл " + file.getName() + " уже существует, перезаписать его?", "Предупреждение",
                    JOptionPane.OK_CANCEL_OPTION);
            if (x != 0) {
                file = null;
            }
        }
        return file;
    }
}
