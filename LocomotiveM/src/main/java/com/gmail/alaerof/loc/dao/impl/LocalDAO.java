package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LocalDAO {
  private static Logger logger = LogManager.getLogger(LocalDAO.class);
  private Connection cnLocal;
  private final String sqlCat = "DELETE FROM category WHERE categoryID = ?";
  private final String sqlDir = "DELETE FROM Direction WHERE dirID = ?";
  private final String sqlDistr = "DELETE FROM distr WHERE distrID = ?";
  private final String sqlStation = "DELETE FROM Station WHERE stationID = ?";
  private final String sqlSpan = "DELETE FROM Span WHERE spanID = ?";
  private final String sqlScale = "DELETE FROM Scale WHERE scaleID = ?";

  public LocalDAO(Connection cnLocal) {
    this.cnLocal = cnLocal;
  }

  public void clearServerTables() {
    String sql;
    try (Statement st = cnLocal.createStatement()) {
      sql = "DELETE FROM Scale";
      st.addBatch(sql);
      sql = "DELETE FROM Span";
      st.addBatch(sql);
      sql = "DELETE FROM distr";
      st.addBatch(sql);
      sql = "DELETE FROM Station";
      st.addBatch(sql);
      sql = "DELETE FROM Direction";
      st.addBatch(sql);
      sql = "DELETE FROM category";
      st.addBatch(sql);
      st.executeBatch();
    } catch (SQLException e) {
      logger.error(e.toString(), e);
    }
  }

  /**
   * type: 0 - category, 1 - Direction, 2 - distr, 3 - Station, 4 - Span, 5 - Scale
   */
  public int deleteListEntity(List<Integer> listID, int type) {
    int countDel = 0;
    String sql = null;
    switch (type) {
      case 0:
        sql = sqlCat;
        break;
      case 1:
        sql = sqlDir;
        break;
      case 2:
        sql = sqlDistr;
        break;
      case 3:
        sql = sqlStation;
        break;
      case 4:
        sql = sqlSpan;
        break;
      case 5:
        sql = sqlScale;
        break;
    }
    if (sql != null) {
      int count = 0;
      try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
        for (int i = 0; i < listID.size(); i++) {
          int id = listID.get(i);
          st.setInt(1, id);
          st.addBatch();
          count++;
        }
        if (count > 0) {
          int[] res = st.executeBatch();
          for (int c : res)
            countDel += c;
        }
      } catch (SQLException e) {
        logger.error(e.toString(), e);
      }
    }
    return countDel;
  }
}
