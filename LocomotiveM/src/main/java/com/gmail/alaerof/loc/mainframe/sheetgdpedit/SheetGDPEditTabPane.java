package com.gmail.alaerof.loc.mainframe.sheetgdpedit;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.gmail.alaerof.loc.entity.sheet.SheetEntity;
import com.gmail.alaerof.loc.application.LinkingLoc;
import com.gmail.alaerof.loc.tab.ButtonTabPane;
import com.gmail.alaerof.loc.tab.TitledTab;

public class SheetGDPEditTabPane extends ButtonTabPane {
    private static final long serialVersionUID = 1L;

    public SheetGDPEditTabPane() {
        List<TitledTab> listC = new ArrayList<>();
        super.initTabs(listC);
        final SheetGDPEditTabPane etp = this;
        
        this.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent arg0) {
                int index = etp.getSelectedIndex();
                if (index > -1) {
                    SheetGDPEditPanel ep = getEditPanelByTabIndex(index);
                    if (ep != null)
                        LinkingLoc.statusBar.setMessage(ep.getDescription());
                }
            }

        });
    }

    public void addEditTab(SheetEntity se) {
        boolean ins = true;
        
        for (int i = 0; i < this.getTabCount(); i++) {
            SheetGDPEditPanel ep = getEditPanelByTabIndex(i);
            if (ep.getSheetID() == se.getSheet().sheetID) {
                ins = false;
                setSelectedIndex(i);
            }
        }
        if (ins) {
            SheetGDPEditPanel newEditPanel = new SheetGDPEditPanel(se);
            se.setSheetGDPEditPanel(newEditPanel);
            add(se.toString(), newEditPanel);
            initTabComponent(this.getTabCount() - 1, newEditPanel.getActionOnClose());
            setSelectedIndex(this.getTabCount() - 1);
        }
    }
    
    public SheetGDPEditPanel getEditPanelByTabIndex(int index) {
        SheetGDPEditPanel ep = null;
        Component item = this.getComponentAt(index);
        ep = (SheetGDPEditPanel)item;
        return ep;
    }
}
