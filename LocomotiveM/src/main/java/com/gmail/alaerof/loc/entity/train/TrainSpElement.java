package com.gmail.alaerof.loc.entity.train;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JComponent;

import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.train.newline.LineLeft;
import com.gmail.alaerof.loc.entity.train.newline.LineRight;
import com.gmail.alaerof.loc.entity.train.newline.NewLine;
import com.gmail.alaerof.loc.entity.train.newline.NewLine.Direction;
import com.gmail.alaerof.loc.entity.train.newline.NewLine.DrawCaption;

/** отображение перегона для нитки поезда */
public class TrainSpElement extends TrainThreadElement {
    private NewLine.Direction direction;
    // все элементы могут состоять из хз частей (но как минимум из 1)
    /** визуальные объекты */
    private ArrayList<NewLine> lines = new ArrayList<>();
    /** список станций */
    private ArrayList<SheetDistrSt> subListSt;
    /** начальная станция перегона */
    private TimeStation timeStBegin;
    /** конечная станция перегона */
    private TimeStation timeStEnd;
    /** признак последнего перегона нитки */
    private boolean lastSp;

    public TrainSpElement(JComponent owner, TrainThreadSheet train, SheetConfig sheetConf, TimeStation timeStB,
            TimeStation timeStE) {
        super(owner, train, sheetConf);
        this.timeStBegin = timeStB;
        this.timeStEnd = timeStE;
        try {
            setLineBounds();
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    public TimeStation getTimeStB() {
        return timeStBegin;
    }

    public TimeStation getTimeStE() {
        return timeStEnd;
    }

    public boolean isLastSp() {
        return lastSp;
    }

    public void setLastSp(boolean lastSp) {
        this.lastSp = lastSp;
    }

    private void hideAllLines() {
        for (NewLine line : lines) {
            line.setVisible(false);
        }
    }

    @Override
    public void setLineBounds() {
        if (train.isHidden()) {
            hideAllLines();
        } else {
            buildLines();
            // boolean viewTimeA = false;
            // if ((lastSp || (timeStEnd.mOn != timeStEnd.mOff)) && !("0".equals(selfParams.timeA))) {
            // viewTimeA = true;
            // }
            int w = 0;
            if (timeStBegin.xOff <= timeStEnd.xOn) {
                // нет перехода на след сутки
                w = timeStEnd.xOn - timeStBegin.xOff;
            } else {
                // есть переход на след сутки
                int w1 = sheetConf.rbGDP.x - timeStBegin.xOff;
                int w2 = timeStEnd.xOn - sheetConf.ltGDP.x;
                w = w1 + w2;
            }

            float xL = timeStBegin.xOff;
            int yTop = 0;
            // st1 - верхняя; st2 - нижняя
            SheetDistrSt st1 = timeStBegin.getDistrSt();
            SheetDistrSt st2 = timeStEnd.getDistrSt();
            if (direction == Direction.right) {
                st2 = timeStBegin.getDistrSt();
                st1 = timeStEnd.getDistrSt();
            }
            // находим общую высоту
            int h = st2.getY1() - st1.getYE1();
            yTop = st1.getY1();
            int hl = st2.getY1() - st1.getYE1();
            float wl = w * hl / h;
            int xNext = (int) (xL + wl);
            // --- sheetConf.rbGDP.x = generalParams.xRight
            // --- sheetConf.ltGDP.x = generalParams.xLeft
            if (xNext > sheetConf.rbGDP.x) {
                // тут у нас переход на след сутки
                float w1 = sheetConf.rbGDP.x - xL;
                float w2 = xNext - sheetConf.rbGDP.x;
                int h1 = (int) (w1 * h / w);
                int h2 = (int) (w2 * h / w);

                NewLine line = lines.get(0);
                // line.setTimeA(false);
                // line.setTimeL(false);
                Dimension dim = line.getOriginalSize();
                if (direction == Direction.left) {
                    dim.width = (int) w1;
                    dim.height = Math.max(1, h1);
                    line.setPosition((int) xL, yTop);
                } else {
                    dim.width = (int) w1;
                    dim.height = Math.max(1, h1);
                    line.setPosition((int) xL, (int) (yTop + h2));
                }
                line.setVisible(true);
                line.repaint();

                line = lines.get(1);
                // line.setTimeA(false);
                // line.setTimeL(false);
                dim = line.getOriginalSize();
                if (direction == Direction.left) {
                    dim.width = (int) w2;
                    dim.height = Math.max(1, h2);
                    line.setPosition(sheetConf.ltGDP.x, (int) (yTop + h1));
                } else {
                    dim.width = (int) w2;
                    dim.height = Math.max(1, h2);
                    line.setPosition(sheetConf.ltGDP.x, yTop);
                }
                line.setVisible(true);
                line.repaint();

                xL = sheetConf.ltGDP.x + w2;
            } else {
                // а тут нет
                NewLine line = lines.get(0);
                Dimension dim = line.getOriginalSize();
                dim.width = (int) wl;
                dim.height = Math.max(1, hl);
                // line.setTimeA(false);
                // line.setTimeL(false);
                line.setPosition((int) xL, yTop);
                line.setVisible(true);
                line.repaint();

                xL += wl;
            }

            if (lines.size() > 0) {
                NewLine line = lines.get(0);
                line.setTimeL(true);
                //line.setTimeL(!("0".equals(selfParams.timeL)));
                line = lines.get(lines.size() - 1);
                //line.setTimeA(viewTimeA);
                line.setTimeA(true);
            }
            
           

        }

    }

    private void buildLines() {

        String str = train.getCode();
        /*
         * if (timeStBegin.showCode == 1) { str = trainThread.getAInscr(); } if
         * (timeStBegin.showCode == 2) { str = timeStBegin.ainscr; } if (str ==
         * null) str = "";
         */
        selfParams.setCaptionValue(str);
        selfParams.color = train.getColorTR();

        selfParams.timeL = "" + timeStBegin.mOff % 10;
        selfParams.timeA = "" + timeStEnd.mOn % 10;

        Direction newDirection = null;
        // направление движения
        if (timeStBegin.getY0() <= timeStEnd.getY0()) {
            newDirection = NewLine.Direction.left;
        } else {
            newDirection = NewLine.Direction.right;
        }

        if (newDirection != direction) {
            lines.clear();
            direction = newDirection;
        }

        // стиль линии
        selfParams.lineStyle = train.getLineStyle();
        selfParams.lineWidth = train.getLineWidth();

        // определяем количество линий
        int lineCount = calcLineCount();

        TrainThreadGeneralParams params = train.getGeneralParams();

        int minCount = Math.min(lineCount, lines.size());
        for (int i = 0; i < minCount; i++) {
            lines.get(i).setSelfParams(selfParams);
            lines.get(i).setGeneralParams(params);
        }

        if (lineCount < lines.size()) {
            // убираем лишние
            int lc = lines.size() - 1;
            for (int i = lc; i >= lineCount; i--) {
                NewLine ln = lines.get(i);
                owner.remove(ln);
                lines.remove(i);
            }
        } else {
            // добавляем недостающие
            int lc = lineCount - lines.size();
            for (int i = 0; i < lc; i++) {
                NewLine ln = null;
                if (direction == Direction.left) {
                    ln = new LineLeft(params, selfParams, this, sheetConf);
                } else {
                    ln = new LineRight(params, selfParams, this, sheetConf);
                }
                lines.add(ln);
                owner.add(ln, 0);
            }
        }

        if (lines.size() > 0) {
            // признак отображения надписи на линии
            if (timeStBegin.showCode != -1) {
                lines.get(0).setCaption(DrawCaption.Center);
            } else {
                lines.get(0).setCaption(DrawCaption.NoCaption);
            }
        }
    }

    private int calcLineCount() {
        int res = 1;
        subListSt = new ArrayList<>();
        // должен содержать как минимум 2 станции
        subListSt.add(timeStBegin.getDistrSt());
        subListSt.add(timeStEnd.getDistrSt());
        if (timeStBegin.xOff > timeStEnd.xOn) {
            // при переходе на след сутки добавляется еще 1 линия
            res += 1;
        }
        return res;
    }

    
    /* private int calcLineCount2() {
     * int res = 1; // List<SheetDistrSt> listSt =
     * train.getListSheetDistrSt(false);
     * List<SheetDistrSt> listSt = train.getListSheetDistrSt(true);
     * SheetDistrSt st1 = timeStBegin.getDistrSt();
     * SheetDistrSt st2 = timeStEnd.getDistrSt();
     * int indx1 = listSt.indexOf(st1);
     * int indx2 = listSt.indexOf(st2);
     * if (indx1 > -1 && indx2 > -1) {
     * // должен содержать как минимум 2 станции
     * subListSt = new ArrayList<>(listSt.subList(Math.min(indx1, indx2), Math.max(indx1, indx2)));
     * subListSt.add(listSt.get(Math.max(indx1, indx2)));
     * // кол-во линий меньше кол-ва станций на 1
     * res = subListSt.size() - 1;
     * if (timeStBegin.xOff > timeStEnd.xOn) {
     * // при переходе на след сутки добавляется еще 1 линия
     * res += 1;
     * }
     * } else {
     * // означает, что хотя бы 1 станции поезда нет среди видимых на участке, т.е. и рисовать не стоит
     * res = 0;
     * subListSt = new ArrayList<>();
     * }
     * return res;
     * } */   

    @Override
    public void removeFromOwner() {
        for (NewLine line : lines) {
            owner.remove(line);
        }
    }

    @Override
    public void addToOwner() {
        for (NewLine line : lines) {
            owner.add(line, 0);
        }
    }

    @Override
    public String toString() {
        return "TrainSpElement [timeStBegin=" + timeStBegin.getDistrSt().stationName + ", timeStEnd="
                + timeStEnd.getDistrSt().stationName + "]";
    }

    @Override
    public void repaint() {
        for (NewLine line : lines) {
            line.repaint();
        }
    }

    @Override
    public void drawInImage(Graphics grGDP, int gapLeft, int gapTop) {
        for (int i = 0; i < lines.size(); i++) {
            NewLine line = lines.get(i);
            if (line != null && line.isVisible())
                line.drawInPicture(grGDP, gapLeft, gapTop);
        }
        
    }

}
