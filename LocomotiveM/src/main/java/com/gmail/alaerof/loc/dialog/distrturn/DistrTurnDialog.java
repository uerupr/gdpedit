package com.gmail.alaerof.loc.dialog.distrturn;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;

public class DistrTurnDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(DistrTurnDialog.class);
    private final JPanel contentPanel = new JPanel();
    private DefaultListModel<Direction> dirListModel = new DefaultListModel<>();
    private JList<Direction> dirList = new JList<Direction>(dirListModel);
    private Direction selectedDir;
    private DistrTurnPanel distrTurnPanel;
    
    public DistrTurnDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    public DistrTurnDialog() {
        initContent();
    }

    private void initContent() {
        setBounds(0, 0, 1180, 500);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        
        {
            dirList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            dirList.setLayoutOrientation(JList.VERTICAL);
            dirList.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent evt) {
                    int index = dirList.getSelectedIndex();
                    if (index > -1) {
                        Direction d = dirListModel.get(index);
                        if (!d.equals(selectedDir)) {
                            selectedDir = d;
                            distrTurnPanel.setDirID(selectedDir.dirID);
                        }
                    }
                }
            });

            JPanel nodPanel = new JPanel();
            nodPanel.setLayout(new BorderLayout());
            nodPanel.setPreferredSize(new Dimension(80, 100));
            JScrollPane scroll = new JScrollPane(dirList);
            nodPanel.add(scroll, BorderLayout.CENTER);

            contentPanel.add(nodPanel, BorderLayout.WEST);
        }
        {
            distrTurnPanel = new DistrTurnPanel();
            contentPanel.add(distrTurnPanel, BorderLayout.CENTER);
        }

    }

    public void loadData() {
        dirListModel.removeAllElements();
        List<Direction> list = LocDAOFactory.getInstance().getDirectionDAO().loadAllDirection();
        for (Direction d : list) {
            dirListModel.addElement(d);
        }
        int selDir = dirList.getSelectedIndex();
        if(selDir<0){
            dirList.setSelectedIndex(0);
        }
        distrTurnPanel.loadRangeAll();
    }

}
