package com.gmail.alaerof.loc.entity.train;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.train.newline.LineHor;
import com.gmail.alaerof.loc.entity.train.newline.NewLine;
import com.gmail.alaerof.loc.entity.train.newline.NewLine.DrawCaption;
import com.gmail.alaerof.loc.entity.train.newline.NewLineSelfParams;

/** отображение станции для нитки поезда */
public class TrainStElement extends TrainThreadElement {
    /** станция поезда прибытия */
    private TimeStation timeSt;
    /** гор линия стоянки прибытия */
    private NewLine horLine1;
    /** вторая половина, если есть переход на след.сутки) */
    private NewLine horLine2;

    public TrainStElement(JComponent owner, TrainThreadSheet train,
            SheetConfig sheetConf, TimeStation timeSt) {
        super(owner, train, sheetConf);
        this.timeSt = timeSt;
        try {
            // GeneralParams у всех ниток одинаковые
            TrainThreadGeneralParams params = train.getGeneralParams();
            selfParams = new NewLineSelfParams();
            selfParams.setCaptionValue(train.getCode());
            
            horLine1 = new LineHor(params, selfParams, this, sheetConf);
            horLine2 = new LineHor(params, selfParams, this, sheetConf);

            // стиль линии
            selfParams.lineStyle = train.getLineStyle();
            selfParams.lineWidth = train.getLineWidth();
            
            addToOwner();

            setLineBounds();
            
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    public TimeStation getTimeSt() {
        return timeSt;
    }

    @Override
    public void setLineBounds() {
        selfParams.color = train.getColorTR();
        horLine1.setCaption(DrawCaption.NoCaption);
        horLine2.setCaption(DrawCaption.NoCaption);

        boolean line1 = false;
        boolean line2 = false;

        if (timeSt.xOn != timeSt.xOff) {
            line1 = true; // есть стоянка
        }
        if (timeSt.xOn > timeSt.xOff) {
            line2 = true; // есть переход на новые сутки
        }

        if (train.isHidden()) {
            line1 = false;
            line2 = false;
        }

        if (line1) {
            // расчет вертикальных координат
            int yMiddle = timeSt.getY1();

            int w1 = 0;
            int w2 = 0;
            selfParams.lineWidth = Math.max(1, train.getLineWidth());
            // если нет перехода через сутки
            if (!line2) {
                w1 = Math.max(1, timeSt.xOff - timeSt.xOn);
                Dimension dim = horLine1.getOriginalSize();
                dim.width = w1;
                dim.height = LineHor.NO_CAPTION_HEIGHT;
                horLine1.setPosition(timeSt.xOn, yMiddle);
            } else {
                w1 = sheetConf.rbGDP.x - timeSt.xOn;
                w2 = timeSt.xOff - sheetConf.ltGDP.x;
                Dimension dim = horLine1.getOriginalSize();
                dim.width = Math.max(1, w1);
                dim.height = LineHor.NO_CAPTION_HEIGHT;

                dim = horLine2.getOriginalSize();
                dim.width = Math.max(1, w2);
                dim.height = LineHor.NO_CAPTION_HEIGHT;

                horLine1.setPosition(timeSt.xOn, yMiddle);
                horLine2.setPosition(sheetConf.ltGDP.x, yMiddle);
            }
        }

        horLine1.setVisible(line1);
        horLine2.setVisible(line2);
        horLine1.repaint();
        horLine2.repaint();
        
    }

    @Override
    public void removeFromOwner() {
        if (horLine1 != null)
            owner.remove(horLine1);
        if (horLine2 != null)
            owner.remove(horLine2);
    }

    @Override
    public void addToOwner() {
        if (horLine1 != null)
            owner.add(horLine1, 0);
        if (horLine2 != null)
            owner.add(horLine2, 0);
    }

    @Override
    public void repaint() {
        if (horLine1 != null)
            horLine1.repaint();
        if (horLine2 != null)
            horLine2.repaint();
    }

    @Override
    public void drawInImage(Graphics grGDP, int gapLeft, int gapTop) {
        if (horLine1 != null)
            horLine1.drawInPicture(grGDP, gapLeft, gapTop);
        if (horLine2 != null)
            horLine2.drawInPicture(grGDP, gapLeft, gapTop);
    }

}
