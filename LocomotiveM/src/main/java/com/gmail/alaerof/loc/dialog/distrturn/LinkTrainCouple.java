package com.gmail.alaerof.loc.dialog.distrturn;

import java.util.ArrayList;
import java.util.List;

import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.entity.train.TrainThread;

public class LinkTrainCouple {
    /** вообще по-идее больше 2-х увязок у поезда быть не должно !!! */
    private List<LinkTrain> link = new ArrayList<>();
    private TrainThread train;

    /**
     * проверяет есть ли у поезда увязка с нужным локомотивом
     * 
     * @param seriesLocID
     * @return
     */
    public boolean isLoc(int seriesLocID) {
        for (LinkTrain l : getLink()) {
            if (l.seriesLocID == seriesLocID)
                return true;
        }
        return false;
    }

    /**
     * дополняет список неповторяющихся ид локомотивов из своего списка увязок
     * [и да, я знаю, что тут можно было использовать Set, но не хочу]
     * 
     * @param list
     *            список неповторяющихся ид-шников
     */
    public void fillLocID(List<Integer> list) {
        for (LinkTrain l : getLink()) {
            Integer locID = l.seriesLocID;
            if (!list.contains(locID)) {
                list.add(locID);
            }
        }
    }

    /**
     * возвращает массив времен (по отправлению, по прибытию, по обороту)
     * @return
     */
    public int[] getLinkTm(Junc juncB, Junc juncE) {
        int[] tm = new int[3];
        for (LinkTrain l : link) {
            // у поезда есть увязка по узлу
            if (juncB != null && juncB.juncID == l.juncID || juncE != null && juncE.juncID == l.juncID) {
                // это отправляющийся поезд
                if ((l.distrIDoff == train.getDistrID()) && (l.codeTRoff == train.getCodeINT())
                        && (l.distrIDon == -1)) {
                    tm[0]+= l.tm;
                }
                // это прибывающий поезд
                if ((l.distrIDon == train.getDistrID()) && (l.codeTRon == train.getCodeINT())
                        && (l.distrIDoff == -1)) {
                    tm[1]+= l.tm;
                }
                // это прибывающий и увязанный с другим поезд (оборот)
                if ((l.distrIDon == train.getDistrID()) && (l.codeTRon == train.getCodeINT())
                        && (l.distrIDoff > 0)) {
                    tm[2]+= l.tm;
                }
            }
        }
        return tm;
    }

    /**
     * время на технологические операции в узле с локомотивом по прибытии поезда
     * 
     * @param juncE
     *            конечный узел
     * @return
     */
    public int getTmArr(Junc juncE) {
        if (juncE != null) {
            for (LinkTrain l : link) {
                // у поезда есть увязка по узлу
                if (l.juncID == juncE.juncID) {
                    // это прибывающий поезд
                    if (l.distrIDon == train.getDistrID() && (l.codeTRon == train.getCodeINT())) {
                        if (l.distrIDoff == -1) {
                            // по идее не может быть больше 1 такой ситуации на
                            // отдельно взятый поезд !!!
                            return l.tm; // это время должно быть рассчитано при
                                         // увязке в окне увязки[таблица]!!!
                        }
                    }
                }
            }
        }
        return 0;
    }

    /**
     * время на технологические операции в узле с локомотивом по отправлении
     * поезда
     * @param juncB начальный узел
     * @return
     */
    public int getTmDep(Junc juncB) {
        if (juncB != null) {
            for (LinkTrain l : link) {
                // у поезда есть увязка по узлу
                if (l.juncID == juncB.juncID) {
                    // это отправляющийся поезд
                    if ((l.distrIDoff == train.getDistrID()) && (l.codeTRoff == train.getCodeINT())) {
                        if (l.distrIDon == -1) {
                            // по идее не может быть больше 1 такой ситуации на
                            // отдельно взятый поезд !!!
                            return l.tm; // это время должно быть рассчитано при
                                         // увязке в окне увязки[таблица]!!!
                        }
                    }
                }
            }
        }
        return 0;
    }

    /**
     * время по обороту, считается только для прибывающих поездов и только для
     * конечного узла участка (хз почему так)
     * 
     * @param juncB
     * @param juncE
     * @return
     */
    public int getTmEq(Junc juncB, Junc juncE) {

        for (LinkTrain l : link) {
            if (juncE != null) {
                // у поезда есть увязка по узлу
                if (l.juncID == juncE.juncID) {
                    // это прибывающий поезд
                    if ((l.distrIDon == train.getDistrID()) && (l.codeTRon == train.getCodeINT())) {
                        // нужно ли делать проверку на наличие связанного поездв
                        // а БД ???
                        // пока что делать этого не будем !
                        if (l.distrIDoff > 0) {
                            // по идее не может быть больше 1 такой ситуации на
                            // отдельно взятый поезд !!!
                            return l.tm; // это время должно быть рассчитано при
                                         // увязке в окне увязки[таблица]!!!
                        }
                    }
                }
            }
            // if (juncB != null) {
            // // у поезда есть увязка по узлу
            // if (l.juncID == juncB.juncID) {
            // // это прибывающий поезд
            // if (l.distrIDon == train.getDistrID()) {
            // // нужно ли делать проверку на наличие связанного поездв
            // // а БД ???
            // // пока что делать этого не будем !
            // if (l.distrIDoff > 0) {
            // // по идее не может быть больше 1 такой ситуации на
            // // отдельно взятый поезд !!!
            // return l.tm; // это время должно быть рассчитано при
            // // увязке в окне увязки[таблица]!!!
            // }
            // }
            // }
            // }
        }
        return 0;
    }

    public List<LinkTrain> getLink() {
        return link;
    }

    public void setLink(List<LinkTrain> link) {
        this.link = link;
    }

    public TrainThread getTrain() {
        return train;
    }

    public void setTrain(TrainThread train) {
        this.train = train;
    }
}