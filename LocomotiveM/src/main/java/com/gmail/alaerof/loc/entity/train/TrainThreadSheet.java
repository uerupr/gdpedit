package com.gmail.alaerof.loc.entity.train;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.dao.dto.train.DistrTrain;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.entity.linktrain.LinkTrainSheet;
import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.sheet.SheetDistrEntity;
import com.gmail.alaerof.loc.entity.sheet.SheetEntity;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditPanel;

public class TrainThreadSheet extends TrainThread {
    /** список видимых станций на листе */
    private List<TimeStation> timeSt = new ArrayList<>();
    /**
     * отображаемые элементы нитки
     * (на перегоне TrainSpanEntity и на станции TrainStationEntity )
     */
    private ArrayList<TrainThreadElement> listElements = new ArrayList<>();
    private boolean selected;
    private TrainThreadGeneralParams generalParams = new TrainThreadGeneralParams();
    /** порядковый номер участка на листе */
    private int sheetDistrNum;
    private SheetGDPEditPanel sheetGDPEditPanel;
    /** увязка в начале */
    private LinkTrainSheet linkBegin;
    /** увязка в конце */
    private LinkTrainSheet linkEnd;

    public TrainThreadSheet() {

    }

    public TrainThreadSheet(DistrTrain distrTrain) {
        super(distrTrain);
    }

    /**
     * возвращает список станций участка для нитки поезда (по которым идет поезд)
     * @param onlyVisible признак возвращать только отображаемые станции
     * @return список
     */
    public List<SheetDistrSt> getListSheetDistrSt(boolean onlyVisible) {
        List<SheetDistrSt> list = null;
        if (sheetGDPEditPanel != null) {
            SheetEntity sheetEntity = sheetGDPEditPanel.getSheetEntity();
            SheetDistrEntity sheetDistrEntity = sheetEntity.getSheetDistrEntity(distrTrain.distrID);
            if (onlyVisible) {
                list = sheetDistrEntity.getListStVisible();
            } else {
                list = sheetDistrEntity.getListSt();
            }
        }
        return list;
    }

    /**
     * построение нитки поезда
     * @param sheetGDPEditPanel контейнер
     */
    // новая версия
    @Deprecated
    public void buildThread1(SheetGDPEditPanel sheetGDPEditPanel) {
        this.sheetGDPEditPanel = sheetGDPEditPanel;
        // создание нового списка элементов (старые будут потеряны...
        // надо удалить их с панели отображения)
        removeAllFromOwner();
        listElements = new ArrayList<>();
        // if (distrTrain.codeTR == 2607) {
        // System.out.println("!!!");
        // }
        timeSt = new ArrayList<>();

        SheetEntity sheetEntity = sheetGDPEditPanel.getSheetEntity();
        SheetConfig sConf = sheetEntity.getSheetConfig();
        SheetDistrEntity sheetDistrEntity = sheetEntity.getSheetDistrEntity(distrTrain.distrID);

        // сортируем список станций расписания (по ходу движения)
        sortSt();

        if (sheetDistrEntity != null) {
            // формируем список пересечения расписания и участка (часть станций
            // видны, а часть нет!)
            int order = 0;
            for (int i = 0; i < trainSt.size(); i++) {
                TrainStation trSt = trainSt.get(i);
                SheetDistrSt dSt = sheetDistrEntity.getSheetDistrSt(trSt.stationID);
                if (dSt != null) {
                    TimeStation tmSt = new TimeStation(trSt, dSt, order);
                    tmSt.calcX(sConf);
                    order++;
                    timeSt.add(tmSt);
                }
            }
            // указываем последней станции, что она последняя
            TimeStation tmSt = null;
            if (timeSt.size() > 0) {
                tmSt = timeSt.get(timeSt.size() - 1);
                tmSt.setLast(true);
            }

            // теперь как бы нужно сформировать список элементов ...
            // будем ли Мы делать элемент станции для безостановочных? ... да
            // нет... пока вообще не будем делать станционные элементы!
            List<TimeStation> list = new ArrayList<>();
            TimeStation stB = null;
            for (int i = 0; i < timeSt.size(); i++) {
                // элемент станции
                TimeStation st1 = timeSt.get(i);
                st1.showCode = -1;
                if (!st1.getDistrSt().hidden) {
                    if (stB == null) {
                        st1.showCode = 0;
                        stB = st1;
                    }
                    list.add(st1);
                }
            }
            // хоть 1 станция есть в списке
            int count = list.size();
            if (count > 0) {
                // строим все элементы
                TimeStation stE = null;
                for (int i = 0; i < count - 1; i++) {
                    stB = list.get(i);
                    stE = list.get(i + 1);
                    TrainSpElement spE = new TrainSpElement(sheetGDPEditPanel.getGDPOwner(), this, sConf,
                            stB, stE);
                    listElements.add(spE);
                    if (i == count - 2) {
                        spE.setLastSp(true);
                    }
                }
            }
        }
    }

    /**
     * построение нитки поезда
     * @param sheetGDPEditPanel контейнер
     */
    // старая версия
    public void buildThread(SheetGDPEditPanel sheetGDPEditPanel) {
        this.sheetGDPEditPanel = sheetGDPEditPanel;
        // создание нового списка элементов (старые будут потеряны...
        // надо удалить их с панели отображения)
        removeAllFromOwner();
        listElements = new ArrayList<>();
        // if (distrTrain.codeTR == 2607) {
        // System.out.println("!!!");
        // }
        timeSt = new ArrayList<>();

        SheetEntity sheetEntity = sheetGDPEditPanel.getSheetEntity();
        SheetConfig sConf = sheetEntity.getSheetConfig();
        SheetDistrEntity sheetDistrEntity = sheetEntity.getSheetDistrEntity(distrTrain.distrID);

        // сортируем список станций расписания (по ходу движения)
        sortSt();

        if (sheetDistrEntity != null) {
            // формируем список пересечения расписания и участка (часть станций
            // видны, а часть нет!)
            int order = 0;
            for (int i = 0; i < trainSt.size(); i++) {
                TrainStation trSt = trainSt.get(i);
                SheetDistrSt dSt = sheetDistrEntity.getSheetDistrSt(trSt.stationID);
                if (dSt != null) {
                    TimeStation tmSt = new TimeStation(trSt, dSt, order);
                    tmSt.calcX(sConf);
                    order++;
                    timeSt.add(tmSt);
                }
            }
            // указываем последней станции, что она последняя
            TimeStation tmSt = null;
            if (timeSt.size() > 0) {
                tmSt = timeSt.get(timeSt.size() - 1);
                tmSt.setLast(true);
            }

            // теперь как бы нужно сформировать список элементов ...
            // будем ли Мы делать элемент станции для безостановочных? ... да
            // нет... пока вообще не будем делать станционные элементы!
            List<TimeStation> list = new ArrayList<>();
            TimeStation stB = null;
            TimeStation stE = null;
            boolean flag = false;
            for (int i = 0; i < timeSt.size(); i++) {
                // элемент станции
                TimeStation st1 = timeSt.get(i);
                st1.showCode = -1;
                if (stB == null) {
                    st1.showCode = 0;
                    stB = st1;
                } else {
                    if (st1.getY0() != stB.getY0()) {
                        if (flag) {
                            // это все вертикальные промежутки после 1
                            // делаем наклонные элементы,
                            // только в случае НЕ cовпадения станций по Y
                            stE = st1;
                            list.add(stE);
                            // TrainSpElement spE = new
                            // TrainSpElement(sheetGDPEditPanel.getGDPOwner(), this, sConf, stB, stE);
                            // listElements.add(spE);
                            stB = stE;
                        } else {
                            // это первый настоящий вертикальный промежуток
                            flag = true;
                            list.add(stB);
                            // stB = st1;
                        }

                    }
                }
            }
            // хоть 1 станция есть в списке
            int count = list.size();
            if (count > 0) {
                // последний вертикальный промежуток должен быть до последней
                // станции
                // поезда!
                stE = tmSt; // это наша последняя станция в списке
                // если длина списке =1 то просто добавляем, иначе заменяем
                // последнюю

                if (count > 1) {
                    list.set(count - 1, tmSt);
                } else {
                    list.add(tmSt);
                    count++;
                }
                // строим все элементы
                for (int i = 0; i < count - 1; i++) {
                    stB = list.get(i);
                    stE = list.get(i + 1);
                    TrainSpElement spE = new TrainSpElement(sheetGDPEditPanel.getGDPOwner(), this, sConf,
                            stB, stE);
                    listElements.add(spE);
                    if (i == count - 2) {
                        spE.setLastSp(true);
                    }
                }
            }
        }
    }

    /** удаление визуальных элементов из контейнера */
    public void removeAllFromOwner() {
        for (TrainThreadElement tre : listElements) {
            tre.removeFromOwner();
        }
    }

    /**
     * отрисовка нитки поезда на канве
     * @param grGDP канва
     */
    public void drawInImage(Graphics grGDP, int gapLeft, int gapTop) {
        for (int i = 0; i < listElements.size(); i++) {
            TrainThreadElement element = listElements.get(i);
            element.drawInImage(grGDP, gapLeft, gapTop);
        }
    }

    /**
     * возвращает признак избранности нитки поезда
     * @return признак избранности нитки поезда
     */
    public boolean isSelected() {
        return selected;
    }

    private void repaint() {
        for (TrainThreadElement tre : listElements) {
            tre.repaint();
        }
    }

    public TrainThreadGeneralParams getGeneralParams() {
        return generalParams;
    }

    /**
     * устанавливает признак избранности для нитки поезда и вызывает перерисовку
     * (каскадно !!!)
     * @param link увязка, на которой был выбор, может быть Null
     * @param selected значение признака избранности
     */
    public void setSelected(LinkTrainSheet link, boolean selected) {
        this.selected = selected;
        // каскадное обновление избранности увязки и соотв. связанных поездов
        if (linkBegin != null && !linkBegin.equals(link)) {
            linkBegin.setSelected(this, isSelected());
        }
        if (linkEnd != null && !linkEnd.equals(link)) {
            linkEnd.setSelected(this, isSelected());
        }
        repaint();
    }
    
    /**
     * меняет избранность поезда на противоположную
     */
    public void mouseClicked() {
        boolean sel = isSelected();
        if (sel) {
            // если поезд/увязка selected то просто убираем выделение (каскадно!!!) 
            // и выставляем в SheetGDPEditPanel оба selectedTrain и selectedLink в нулл, 
            // ничего с ними не делая, если они были не null 
            // т.к. если выбранное поезд/увязка было selected, 
            // то это (selectedTrain и selectedLink) могут быть только его частями
            sel = false;
            sheetGDPEditPanel.setSelected(null, null);
        } else {
            // если поезд/увязка не selected, 
            // то сначала нужно убрать имеющееся выделение (каскадно!!!)
            // либо selectedTrain либо selectedLink (если кто-то из них не null)
            // а затем установить (каскадно!!!) выделение для выбранного поезда/увязки
            sel = true;
            sheetGDPEditPanel.setSelected(this, null);
        }
        // тут собственно вызывается каскадное обновление текущего выбора
        setSelected(null, sel);
    }

    /**
     * возвращает порядковый номер участка на листе
     * @return порядковый номер участка на листе
     */
    public int getSheetDistrNum() {
        return sheetDistrNum;
    }

    /**
     * устанавливает порядковый номер участка на листе
     * @param sheetDistrNum порядковый номер участка на листе
     */
    public void setSheetDistrNum(int sheetDistrNum) {
        this.sheetDistrNum = sheetDistrNum;
    }

    /** возвращает Y начала нитки поезда */
    public int getFirstY() {
        if (timeSt.size() > 0) {
            TimeStation st = timeSt.get(0);
            return st.getY1();
        }
        return -1;
    }

    /** возвращает Y конца нитки поезда */
    public int getLastY() {
        if (timeSt.size() > 0) {
            TimeStation st = timeSt.get(timeSt.size() - 1);
            return st.getY1();
        }
        return -1;
    }

    /**
     * возвращает увязку начала поезда или null
     * @return
     */
    public LinkTrainSheet getLinkBegin() {
        return linkBegin;
    }

    /**
     * устанавливает увязку начала поезда
     * @param linkBegin увязка
     */
    public void setLinkBegin(LinkTrainSheet linkBegin) {
        this.linkBegin = linkBegin;
    }

    /**
     * возвращает увязку конца проезда или null
     * @return
     */
    public LinkTrainSheet getLinkEnd() {
        return linkEnd;
    }

    /**
     * устанавливает увязку конца поезда
     * @param linkEnd увязка
     */
    public void setLinkEnd(LinkTrainSheet linkEnd) {
        this.linkEnd = linkEnd;
    }

}
