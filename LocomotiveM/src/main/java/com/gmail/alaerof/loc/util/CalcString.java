package com.gmail.alaerof.loc.util;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Formatter;
import java.util.regex.Pattern;

public class CalcString {
    private static BufferedImage canvas = new BufferedImage(30, 10, BufferedImage.TYPE_INT_ARGB);
    private static Graphics g = canvas.createGraphics();

    /**
     * возвращает ширину строки в пикселах для заданного шрифта
     * 
     * @param f
     *            шрифт
     * @param str
     *            строка
     * @return ширину строки в пикселах
     */
    public static int getStringW(Font f, String str) {
        int w = 0;
        g.setFont(f);
        FontMetrics fm = g.getFontMetrics();
        if (str != null) {
            w = fm.stringWidth(str);
        }
        return w;
    }

    /**
     * возвращает высоту строки в пикселах для заданного шрифта
     * 
     * @param f
     *            шрифт
     * @return высоту строки в пикселах
     */
    public static int getStringH(Font f) {
        int h = 0;
        g.setFont(f);
        FontMetrics fm = g.getFontMetrics();
        h = fm.getHeight();
        return h;
    }

    /**
     * форматирует дробное в строку
     * 
     * @param value
     *            число
     * @param intP
     *            знаков на целое
     * @param decP
     *            знаков после запятой
     * @return отформатированное число в виде строки
     */
    public static String formatFloat(float value, int intP, int decP) {
        String str = "%" + intP + '.' + decP + 'f';
        Formatter f = new Formatter();
        f.format(str, value);
        str = f.toString();
        String p = ".*\\,0.*";
        if (Pattern.matches(p, str)) {
            int x = (int) value;
            if (x > 0) {
                str = Integer.toString(x);
            } else {
                str = "";
            }
        }
        return str;
    }

}
