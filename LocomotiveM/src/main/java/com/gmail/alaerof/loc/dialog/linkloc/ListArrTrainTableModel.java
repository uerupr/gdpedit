package com.gmail.alaerof.loc.dialog.linkloc;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.entity.linktrain.LinkTrainEntity;
import com.gmail.alaerof.loc.entity.train.TrainThread;

public class ListArrTrainTableModel extends AbstractTableModel implements ListTrain {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "уч.приб.", "станция", "назв.", "№ приб", "приб.", "увязка", "мин." };
    private List<TrainThread> list;
    private List<LinkTrainEntity> listLinkTrain; // увязки по узлу

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (list != null) {
            TrainThread el = list.get(rowIndex);
            TrainStation st = el.getTrainStation(el.getLastStID());
            String nameSt = "";
            String timeSt = "";
            if (st != null) {
                nameSt = st.stationName.trim();
                timeSt = st.getTimeOnStr().trim();
            }
            String value = "";
            LinkTrainEntity lt = LinkLocUtil.getLinkTrainEntityOn(listLinkTrain, el);
            if (lt != null) {
                value = lt.getDescriptionOff();
            }

            switch (columnIndex) {
            case 0:
                return LinkLocUtil.getNameDistr(el.getDistrTrain().distrID);
            case 1:
                return nameSt;
            case 2:
                return el.getDistrTrain().nameTR;
            case 3:
                return el.getDistrTrain().codeTR;
            case 4:
                return timeSt;
            case 5:
                return value;
            case 6:
                if (lt != null) {
                    LinkTrain llt = lt.getLinkTrain();
                    if (llt != null) {
                        return llt.tm;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 6:
            return Integer.class;
        default:
            return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
    }

    @Override
    public List<TrainThread> getList() {
        return list;
    }

    @Override
    public void setList(List<TrainThread> list) {
        this.list = list;
    }

    @Override
    public List<LinkTrainEntity> getListLinkTrain() {
        return listLinkTrain;
    }

    @Override
    public void setListLinkTrain(List<LinkTrainEntity> listLinkTrain) {
        this.listLinkTrain = listLinkTrain;
    }

    @Override
    public void deleteLinkTrain(int row) {
        if (list != null) {
            TrainThread el = list.get(row);
            LinkTrainEntity lt = LinkLocUtil.getLinkTrainEntityOn(listLinkTrain, el);
            if (lt != null) {
                listLinkTrain.remove(lt);
            }
        }
    }

    @Override
    public LinkTrainEntity getLinkTrainEntity(int row) {
        TrainThread el = list.get(row);
        return LinkLocUtil.getLinkTrainEntityOn(listLinkTrain, el);
    }

    @Override
    public boolean isArrive() {
        return true;
    }

}
