package com.gmail.alaerof.loc.dialog.linkloc;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.entity.linktrain.LinkTrainEntity;

public class JuncTimeCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    private static final long serialVersionUID = 1L;
    protected LinkLocDialog dialog;

    public JuncTimeCellRenderer(LinkLocDialog dialog) {
        this.dialog = dialog;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int col) {

        if (value != null){
            setText(value.toString());
        }else{
            setText("");
        }

        Color background;
        if (row % 2 == 0) {
            background = Color.WHITE;
        } else {
            background = new Color(242, 242, 242);
        }
        if (isSelected) {
            background = Color.LIGHT_GRAY;
        }
        ListTrain ltm = (ListTrain) table.getModel();
        LinkTrainEntity lte = ltm.getLinkTrainEntity(row);
        int diff = 0;
        if (lte != null) {
            Junc junc = dialog.getSelectedJunc();
            //diff = lte.getLinkTrain().tm - junc.getJuncTime().equipment;
            diff = lte.getDiffTime(junc);
        }

        if (diff < 0) {
            setForeground(Color.red);
        }else{
            setForeground(Color.black);
        }
        setBackground(background);

        return this;
    }

}
