package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Station;

/** работает только с локальной БД */
public class StationDAO {
    private static Logger logger = LogManager.getLogger(StationDAO.class);
    private Connection cnLocal;

    public StationDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    public int insertListStation(List<Station> list) {
        String sql = "INSERT INTO Station (stationID, dirID, codeESR, codeESR2, codeExpress, name, br, joint)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Station s = list.get(i);
                st.setInt(1, s.stationID);
                st.setInt(2, s.dirID);
                st.setString(3, s.codeESR);
                st.setString(4, s.codeESR2);
                st.setString(5, s.codeExpress);
                st.setString(6, s.name);
                st.setBoolean(7, s.br);
                st.setBoolean(8, s.joint);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    public int updateListStation(List<Station> list) {
        String sql = "UPDATE Station SET dirID=?, codeESR=?, codeESR2=?, codeExpress=?, name=?, br=?, joint=?"
                + " WHERE stationID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                Station s = list.get(i);
                st.setInt(1, s.dirID);
                st.setString(2, s.codeESR);
                st.setString(3, s.codeESR2);
                st.setString(4, s.codeExpress);
                st.setString(5, s.name);
                st.setBoolean(6, s.br);
                st.setBoolean(7, s.joint);
                st.setInt(8, s.stationID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    private void fillStationItem(ResultSet rs, Station item) throws SQLException {
        item.stationID = rs.getInt("stationID");
        item.dirID = rs.getInt("dirID");
        item.codeESR = rs.getString("CodeESR").trim();
        item.codeESR2 = rs.getString("CodeESR2").trim();
        item.codeExpress = rs.getString("CodeExpress").trim();
        item.name = rs.getString("name").trim();
        item.br = rs.getBoolean("br");
        item.joint = rs.getBoolean("joint");
    }

    public List<Station> loadAllStation() {
        List<Station> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT stationID, dirID, codeESR, codeESR2, codeExpress, name, br, joint FROM Station ORDER BY name";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Station item = new Station();
                    fillStationItem(rs, item);
                    list.add(item);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    public Station getStationByID(int idSt) {
        Station item = null;
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT stationID, dirID, codeESR, codeESR2, codeExpress, name, br, joint FROM Station "
                    + "WHERE stationID = " + idSt;
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    item = new Station();
                    fillStationItem(rs, item);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return item;
    }

    public List<Station> getDistrStation(int distrID) {
        List<Station> list = new ArrayList<>();
        Station item = null;
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT DISTINCT stationID, dirID, codeESR, codeESR2, codeExpress, name, br, joint FROM Station "
                    + "WHERE (stationID IN (SELECT stB FROM Span WHERE spanID IN (SELECT spanID FROM Scale WHERE distrID = "
                    + distrID
                    + "))) "
                    + "OR (stationID IN (SELECT stE FROM Span WHERE spanID IN (SELECT spanID FROM Scale WHERE distrID = "
                    + distrID + ")))";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    item = new Station();
                    fillStationItem(rs, item);
                    if (!list.contains(item)) {
                        list.add(item);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

}
