package com.gmail.alaerof.loc.dialog.junc;

import javax.swing.ComboBoxModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.gmail.alaerof.loc.dao.dto.Station;

public class StationPlainDocument extends PlainDocument {
    private static final long serialVersionUID = 1L;
    private ComboBoxModel<Station> model;
    private boolean selecting = false;

    public StationPlainDocument(ComboBoxModel<Station> model) {
        this.model = model;
    }

    public void remove(int offs, int len) throws BadLocationException {
        if (selecting)
            return;
        super.remove(offs, len);
    }

    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        // return immediately when selecting an item
        if (selecting)
            return;
        // System.out.println("insert " + str + " at " + offs);
        // insert the string into the document
        super.insertString(offs, str, a);
        // get the resulting string
        String content = getText(0, getLength()).toLowerCase();
        // lookup a matching item
        Object item = lookupItem(content);
        // select the item (or deselect if null)
        if (item != model.getSelectedItem()) {
            // System.out.println("Selecting '" + item + "'");
            selecting = true;
            model.setSelectedItem(item);
            selecting = false;
        }
    }

    private Object lookupItem(String pattern) {
        // iterate over all items
        for (int i = 0, n = model.getSize(); i < n; i++) {
            Object currentItem = model.getElementAt(i);
            // current item starts with the pattern?
            if (currentItem.toString().toLowerCase().startsWith(pattern)) {
                return currentItem;
            }
        }
        // no item starts with the pattern => return null
        return null;
    }
}
