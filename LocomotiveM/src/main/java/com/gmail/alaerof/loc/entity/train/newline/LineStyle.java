package com.gmail.alaerof.loc.entity.train.newline;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public enum LineStyle {
    Solid, Dash, DashDot, Wave, Double;

    public static LineStyle getLineStyle(int style) {
        switch (style) {
        case 0:
            return Solid;
        case 1:
            return Dash;
        case 2:
            return DashDot;
        case 3:
            return Wave;
        case 4:
            return Double;

        default:
            return Solid;
        }
    }

    public int getLineStyleCode() {
        switch (this) {
        case Solid:
            return 0;
        case Dash:
            return 1;
        case DashDot:
            return 2;
        case Wave:
            return 3;
        case Double:
            return 4;
        default:
            return 0;
        }
    }

    public static BasicStroke getStroke(LineStyle style, float lineWidth) {
        // по умолчанию Solid
        BasicStroke stroke = new BasicStroke(lineWidth, // Width
                BasicStroke.CAP_ROUND, // End cap
                BasicStroke.JOIN_MITER);// Join style
        switch (style) {
        case Dash:
            stroke = new BasicStroke(1.0f, // Width
                    BasicStroke.CAP_BUTT, // End cap
                    BasicStroke.JOIN_MITER, // Join style
                    1.0f, // Miter limit
                    new float[] { 5.0f, 5.0f }, // Dash pattern
                    100.0f); // Dash phase
            break;
        case DashDot:
            stroke = new BasicStroke(1.0f, // Width
                    BasicStroke.CAP_ROUND, // End cap
                    BasicStroke.JOIN_ROUND, // Join style
                    0.0f, // Miter limit
                    new float[] { 10.0f, 5.0f, 3.0f, 5.0f }, // Dash pattern
                    0.0f); // Dash phase
            break;
        default:
            break;
        }
        return stroke;
    }

    /** рисует пунктир, штрих-пунктир, волну и двойную линию */
    public static void drawLine(Graphics2D g2, int x1, int y1, int x2, int y2, LineStyle style, float lineWidth,
            Color background, Color pen) {
        switch (style) {
        case Dash:
            drawDash(g2, x1, y1, x2, y2, lineWidth, background, pen);
            break;
        case DashDot:
            drawDashDot(g2, x1, y1, x2, y2, lineWidth, background, pen);
            break;
        case Wave:
            drawWave(g2, x1, y1, x2, y2, lineWidth, background, pen);
            break;
        case Double:
            drawDouble(g2, x1, y1, x2, y2, lineWidth, background, pen);
            break;
        default:
            break;
        }
    }

    private static void drawDouble(Graphics2D g2, int x1, int y1, int x2, int y2, float lineWidth, Color background,
            Color pen) {
        BasicStroke main = new BasicStroke(Math.min(2, lineWidth), // Width
                BasicStroke.CAP_BUTT, // End cap
                BasicStroke.JOIN_MITER);// Join style
        BasicStroke add = new BasicStroke(1, // Width
                BasicStroke.CAP_BUTT, // End cap
                BasicStroke.JOIN_MITER);// Join style
        g2.setStroke(main);
        // главна¤ лини¤
        g2.drawLine(x1, y1, x2, y2);
        // боковые линии только дл¤ наклонных
        if (x1 != x2 && y1 != y2) {
            g2.setStroke(add);
            int w = Math.abs(x2 - x1);
            int h = Math.abs(y2 - y1);
            // диагональ всего пр¤моугольника
            double gg = Math.sqrt(w * w + h * h);
            int picL = 3; // перпенд. отступ от главной линии
            double picLx = h * picL / gg; // проекци¤ отступа на х
            double picLy = w * picL / gg; // проекци¤ отступа на y
            // проекци¤ боковой линии на х
            double dxL = w / 3;
            // проекци¤ боковой линии на y
            double dyL = h / 3;
            int xx1 = x1;
            int yy1 = y1;
            int xx2 = x2;
            int yy2 = y2;
            // верхн¤¤ бокова¤ лини¤
            if (y1 < y2) {
                xx1 = (int) (x1 + dxL + picLx);
                yy1 = (int) (y1 + dyL - picLy);
                xx2 = (int) (x1 + dxL * 2 + picLx);
                yy2 = (int) (y1 + dyL * 2 - picLy);
            } else {
                xx1 = (int) (x1 + dxL - picLx);
                yy1 = (int) (y1 - dyL - picLy);
                xx2 = (int) (x1 + dxL * 2 - picLx);
                yy2 = (int) (y1 - dyL * 2 - picLy);
            }
            g2.drawLine(xx1, yy1, xx2, yy2);

            // нижн¤¤ бокова¤ лини¤
            if (y1 < y2) {
                xx1 = (int) (x1 + dxL - picLx);
                yy1 = (int) (y1 + dyL + picLy);
                xx2 = (int) (x1 + dxL * 2 - picLx);
                yy2 = (int) (y1 + dyL * 2 + picLy);
            } else {
                xx1 = (int) (x1 + dxL + picLx);
                yy1 = (int) (y1 - dyL + picLy);
                xx2 = (int) (x1 + dxL * 2 + picLx);
                yy2 = (int) (y1 - dyL * 2 + picLy);
            }
            g2.drawLine(xx1, yy1, xx2, yy2);
        }
    }

    private static void drawWave(Graphics2D g2, int x1, int y1, int x2, int y2, float lineWidth, Color background,
            Color pen) {
        BasicStroke stroke = new BasicStroke(Math.min(2, lineWidth), // Width
                BasicStroke.CAP_BUTT, // End cap
                BasicStroke.JOIN_MITER);// Join style
        g2.setStroke(stroke);
        int picS = 3;
        double sx = 0;
        double sy = 0;
        int Lx = x2 - x1;
        int Ly = y2 - y1;
        double gg = Math.sqrt(Lx * Lx + Ly * Ly);
        if (gg > 0) {
            double dxS = Lx * picS / gg;
            double dyS = Ly * picS / gg;
            double ddx = 0;
            double ddy = 0;
            int mode = 1;
            int i = 0;
            while ((((int) Math.abs(sx) < Math.abs(Lx)) && ((int) Math.abs(sy) < Math.abs(Ly)) && (Ly != 0) && (Lx != 0))
                    || (((int) Math.abs(sx) < Math.abs(Lx)) && (Ly == 0))
                    || (((int) Math.abs(sy) < Math.abs(Ly)) && (Lx == 0))) {

                int xx1 = (int) (x1 + sx);
                int yy1 = (int) (y1 + sy);
                if ((int) Math.abs(sx + dxS) > Math.abs(Lx)) {
                    dxS = dxS - (sx + dxS - Lx);
                }
                if ((int) Math.abs(sy + dyS) > Math.abs(Ly)) {
                    dyS = dyS - (sy + dyS - Ly);
                }
                int xx2 = (int) (x1 + sx + dxS);
                int yy2 = (int) (y1 + sy + dyS);

                int xx0 = (int) (x1 + sx + dxS / 2 - mode * Math.abs(dyS / 2));
                int yy0;
                if (dyS != 0) {
                    yy0 = (int) (y1 + sy + dyS / 2 + mode * Math.signum(dyS) * Math.abs(dxS / 2));
                } else {
                    yy0 = (int) (y1 + sy + dyS / 2 + mode * Math.abs(dxS / 2));
                }

                g2.drawLine(xx1, yy1, xx0, yy0);
                g2.drawLine(xx0, yy0, xx2, yy2);

                sx += dxS + ddx;
                sy += dyS + ddy;
                if (mode == 1) {
                    mode = -1;
                } else {
                    mode = 1;
                }
                i++;
                if (i > 50)
                    break;
            }
        }
    }

    private static void drawDashDot(Graphics2D g2, int x1, int y1, int x2, int y2, float lineWidth, Color background,
            Color pen) {
        BasicStroke stroke = new BasicStroke(lineWidth, // Width
                BasicStroke.CAP_BUTT, // End cap
                BasicStroke.JOIN_MITER);// Join style
        g2.setStroke(stroke);
        int picL = 10; // лини¤
        int picD = 2; // точка
        int picS = 5; // пробел
        int w = Math.abs(x1 - x2);
        int h = Math.abs(y1 - y2);
        // диагональ всего пр¤моугольника
        double gg = Math.sqrt(w * w + h * h);
        int ggInt = (int) gg;
        if (gg > 0) {
            int xB = x1;
            int yB = y1;
            int xE = x2;
            int yE = y2;
            int gL = 0;
            double dxL = picL * (xE - xB) / gg;
            double dyL = picL * (yE - yB) / gg;
            x2 = (int) (x1 + dxL);
            y2 = (int) (y1 + dyL);
            g2.setColor(pen);
            int i = 0;
            int xLen1 = 0;
            int yLen1 = 0;
            int xLen2 = 0;
            int yLen2 = 0;
            boolean draw = true;
            while (draw) {
                g2.drawLine(x1, y1, x2, y2);
                x1 = (int) (xB + xLen1);
                y1 = (int) (yB + yLen1);
                if (i % 2 == 0) {
                    gL += picL;
                    xLen2 = (int)(gL * (xE - xB) / gg);
                    yLen2 = (int)(gL * (yE - yB) / gg);
                    gL +=  picS;
                } else {
                    gL += picD;
                    xLen2 = (int)(gL * (xE - xB) / gg);
                    yLen2 = (int)(gL * (yE - yB) / gg);
                    gL +=  picS;
                }
                x2 = (int) (xB + xLen2);
                y2 = (int) (yB + yLen2);
                xLen1 = (int)(gL * (xE - xB) / gg);
                yLen1 = (int)(gL * (yE - yB) / gg);

                i++;
                if (gL > ggInt) {
                    draw = false;
                }
            }
        }

    }

    private static void drawDash(Graphics2D g2, int x1, int y1, int x2, int y2, float lineWidth, Color background,
            Color pen) {
        BasicStroke stroke = new BasicStroke(lineWidth, // Width
                BasicStroke.CAP_BUTT, // End cap
                BasicStroke.JOIN_MITER);// Join style
        g2.setStroke(stroke);
        int picL = 8;
        int picS = 4;
        int w = Math.abs(x1 - x2);
        int h = Math.abs(y1 - y2);
        // диагональ всего пр¤моугольника
        double gg = Math.sqrt(w * w + h * h);
        int ggInt = (int) gg; // вс¤ диагональ
        int gL = 0; // растуща¤ диагональ
        if (gg > 0) {
            int xB = x1;
            int yB = y1;
            int xE = x2;
            int yE = y2;

            gL = picL;
            // приращение по х линии
            double dxL = gL * (xE - xB) / gg;
            // приращение по y линии
            double dyL = gL * (yE - yB) / gg;
            x2 = (int) (x1 + dxL);
            y2 = (int) (y1 + dyL);
            g2.setColor(pen);
            int i = 1;
            boolean draw = true;
            while (draw) {
                g2.drawLine(x1, y1, x2, y2);
                gL = i * (picL + picS);
                // приращение по х линии
                dxL = gL * (xE - xB) / gg;
                // приращение по y линии
                dyL = gL * (yE - yB) / gg;
                x1 = (int) (xB + dxL);
                y1 = (int) (yB + dyL);

                gL = i * (picL + picS) + picL;
                // приращение по х линии
                dxL = gL * (xE - xB) / gg;
                // приращение по y линии
                dyL = gL * (yE - yB) / gg;
                x2 = (int) (xB + dxL);
                y2 = (int) (yB + dyL);

                i++;
                if (gL > ggInt) {
                    draw = false;
                }
            }
        }
    }
}
