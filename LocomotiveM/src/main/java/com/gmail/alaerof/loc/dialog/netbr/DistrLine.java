package com.gmail.alaerof.loc.dialog.netbr;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;

import javax.swing.JButton;

public abstract class DistrLine extends JButton {
    private static final long serialVersionUID = 1L;
    private static Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
    private Color color = new Color(0x6495ed);
    protected Polygon polygon;
    protected Direction direction;
    protected int xleft;
    protected int ytop;
    protected Point begin = new Point();
    protected Point end = new Point();
    protected static final int ADD_WIDTH = 1;
    protected int lineWidth = 4;

    protected enum Direction {
        right, left, vert, hor
    }

    public DistrLine() {
        this.setCursor(handCursor);
        setContentAreaFilled(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Color pen = color;
        if (getModel().isArmed()) {
            pen = Color.lightGray;
        }
        if (polygon == null || !polygon.getBounds().equals(getBounds())) {
            polygon = getPolygon();
        }
        g2.setColor(pen);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.fillPolygon(polygon);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    }

    @Override
    public boolean contains(int x, int y) {
        if (polygon == null || !polygon.getBounds().equals(getBounds())) {
            polygon = getPolygon();
        }
        return polygon.contains(x, y);
    }

    protected abstract Polygon getPolygon();

    public void setPosition(int x, int y, int w, int h) {
        setBounds(x, y, w, h);
    }

}
