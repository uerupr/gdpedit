package com.gmail.alaerof.loc.dialog.linkloc;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import org.apache.log4j.LogManager;

public class CellDataTransferable implements Transferable{
    public static final DataFlavor CELL_DATA_FLAVOR = createConstant(CellData.class, "application/x-java-celldata");
    private CellData cellData;
    
    static protected DataFlavor createConstant(Class<?> clazz, String name) {
        try {
            return new DataFlavor(clazz, name);
        } catch (Exception e) {
            LogManager.getLogger(CellDataTransferable.class).error(e.toString(), e);
            return null;
        }
    }
    
    public CellDataTransferable(CellData cellData) {
        this.cellData = cellData;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { CELL_DATA_FLAVOR };
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        boolean supported = false;
        for (DataFlavor available : getTransferDataFlavors()) {
            if (available.equals(flavor)) {
                supported = true;
            }
        }
        return supported;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return cellData;
    }

}
