package com.gmail.alaerof.loc.dialog.timetable;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;

public class TrainResultsPanel extends JPanel{
    private static final long serialVersionUID = 1L;
    protected TrainResultsTableModel tableModel;
    protected JTable table;
    protected TrainThreadSheet selectedTrain;

    public TrainResultsPanel(){
        this.setLayout(new BorderLayout(0, 0));
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        JScrollPane listScroll = new JScrollPane();
        this.add(listScroll, BorderLayout.CENTER);
        tableModel = new TrainResultsTableModel();
        table = new JTable(tableModel);
        listScroll.setViewportView(table);
        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSize[] = { 120, 40, 40, 30, 30 };
        com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(table, colSize);
    }
    
    public TrainThreadSheet getSelectedTrain() {
        return selectedTrain;
    }
    
    public void setSelectedTrain(TrainThreadSheet selectedTrain) {
        this.selectedTrain = selectedTrain;
        tableModel.setList(this.selectedTrain.getDistrTrain().getListRes());
        table.repaint();
    }

}
