package com.gmail.alaerof.loc.dao.dto.sheet;

public class SheetDistr {
    // DB
    public int sheetDistrID;
    public int sheetID;
    public int distrID;
    public int num;
    public boolean evenDown;
    // скрывать начальный узел участка
    public boolean hideJuncB;
    // скрывать конечный узел участка
    public boolean hideJuncE;
    // add
    public String distrName;
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + distrID;
        result = prime * result + num;
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SheetDistr other = (SheetDistr) obj;
        if (distrID != other.distrID)
            return false;
        if (num != other.num)
            return false;
        return true;
    }
    
    @Override
    public String toString() {
        return num + ".." + distrName;
    }
    
}
