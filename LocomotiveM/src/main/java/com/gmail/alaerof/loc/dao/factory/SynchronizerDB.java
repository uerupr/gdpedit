package com.gmail.alaerof.loc.dao.factory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gmail.alaerof.loc.dao.dto.AbstractDTO;
import com.gmail.alaerof.loc.dao.dto.Category;
import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Scale;
import com.gmail.alaerof.loc.dao.dto.Span;
import com.gmail.alaerof.loc.dao.dto.Station;
import com.gmail.alaerof.loc.dao.impl.*;

public class SynchronizerDB {

    /** первичный перенос инфы с сервера в локальную БД */
    public static void firstLoad() {
        LocDAOFactory f = LocDAOFactory.getInstance();
        LocalDAO lDAO = f.getLocalDAO();
        lDAO.clearServerTables();
        ServerDAO sDAO = f.getServerDAO();

        DirectionDAO dirDAO = f.getDirectionDAO();
        dirDAO.insertListCategory(sDAO.loadAllCategory());
        dirDAO.insertListDirection(sDAO.loadAllDirection());

        DistrDAO distrDAO = f.getDistrDAO();
        distrDAO.insertListDistr(sDAO.loadAllDistr());

        StationDAO stDAO = f.getStationDAO();
        stDAO.insertListStation(sDAO.loadAllStation());

        SpanDAO spDAO = f.getSpanDAO();
        spDAO.insertListSpan(sDAO.loadAllSpan());
        spDAO.insertListScale(sDAO.loadAllScale());
    }

    private Set<Integer> upd;
    private Set<Integer> ins;
    private Set<Integer> del;

    private <T extends AbstractDTO> void buildSet(List<T> src, List<T> dst) {
        upd = new HashSet<>();
        ins = new HashSet<>();
        del = new HashSet<>();

        for (int i = 0; i < dst.size(); i++) {
            upd.add(dst.get(i).getID());
        }

        for (int i = 0; i < src.size(); i++) {
            ins.add(src.get(i).getID());
        }

        del.addAll(upd);

        upd.retainAll(ins);
        ins.removeAll(upd);
        del.removeAll(upd);
    }

    private <T extends AbstractDTO> List<T> getListByID(List<T> src, Set<Integer> id) {
        List<T> list = new ArrayList<>();
        for (int i = 0; i < src.size(); i++) {
            T item = src.get(i);
            if (id.contains(item.getID())) {
                list.add(item);
            }
        }
        return list;
    }

    private <T extends AbstractDTO> List<T> checkListUpd(List<T> upd, List<T> dst) {
        List<T> list = new ArrayList<>();
        for (int i = 0; i < upd.size(); i++) {
            T item = upd.get(i);
            if (!dst.contains(item)) {
                list.add(item);
            }
        }
        return list;
    }

    public String updateServerTable() {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        LocDAOFactory f = LocDAOFactory.getInstance();
        ServerDAO sDAO = f.getServerDAO();
        LocalDAO lDAO = f.getLocalDAO();
        if (sDAO.isConnectionNull()) {
            sb.append("Обновление невозможно, проверьте настройки соединения с сервером и лог");
        } else {
            DirectionDAO dirDAO = f.getDirectionDAO();
            // ------ category ----------------------
            // Получить список обновляемых сущностей с сервера (источник)
            List<Category> srcCat = sDAO.loadAllCategory();
            // Получить список обновляемых сущностей из лок бд (назначение)
            List<Category> dstCat = dirDAO.loadAllCategory();
            // получить списки добавляемых, обновляемых и ID удаляемых сущностей
            buildSet(srcCat, dstCat);
            List<Category> insCat = getListByID(srcCat, ins);
            List<Category> updCat = getListByID(srcCat, upd);
            updCat = checkListUpd(updCat, dstCat);
            // удалить из назначения сущности, которых нет в источнике
            List<Integer> dellC = new ArrayList<>(del);
            count = lDAO.deleteListEntity(dellC, 0);
            sb.append("Категории: удалено = " + count);
            // Добавить в назначение сущности из источника
            count = dirDAO.insertListCategory(insCat);
            sb.append(" добавлено = " + count);
            // Обновить содержимое всех сущностей назначения в соотв с
            // источником
            count = dirDAO.updateListCategory(updCat);
            sb.append(" обновлено = " + count);

            // ------ Direction ----------------------
            // Получить список обновляемых сущностей с сервера (источник)
            List<Direction> srcDir = sDAO.loadAllDirection();
            // Получить список обновляемых сущностей из лок бд (назначение)
            List<Direction> dstDir = dirDAO.loadAllDirection();
            // получить списки добавляемых, обновляемых и ID удаляемых сущностей
            buildSet(srcDir, dstDir);
            List<Direction> insDir = getListByID(srcDir, ins);
            List<Direction> updDir = getListByID(srcDir, upd);
            updDir = checkListUpd(updDir, dstDir);
            // удалить из назначения сущности, которых нет в источнике
            List<Integer> dell = new ArrayList<>(del);
            count = lDAO.deleteListEntity(dell, 1);
            sb.append("\nНаправления: удалено = " + count);
            // Добавить в назначение сущности из источника
            count = dirDAO.insertListDirection(insDir);
            sb.append(" добавлено = " + count);
            // Обновить содержимое всех сущностей назначения в соотв с
            // источником
            count = dirDAO.updateListDirection(updDir);
            sb.append(" обновлено = " + count);

            // ------- distr ------------------------------
            DistrDAO distrDAO = f.getDistrDAO();
            // Получить список обновляемых сущностей с сервера (источник)
            List<Distr> srcD = sDAO.loadAllDistr();
            // Получить список обновляемых сущностей из лок бд (назначение)
            List<Distr> dstD = distrDAO.loadAllDistr();
            // получить списки добавляемых, обновляемых и ID удаляемых сущностей
            buildSet(srcD, dstD);
            List<Distr> insD = getListByID(srcD, ins);
            List<Distr> updD = getListByID(srcD, upd);
            updD = checkListUpd(updD, dstD);
            // удалить из назначения сущности, которых нет в источнике
            List<Integer> dellD = new ArrayList<>(del);
            count = lDAO.deleteListEntity(dellD, 2);
            sb.append("\nУчастки: удалено = " + count);
            // Добавить в назначение сущности из источника
            count = distrDAO.insertListDistr(insD);
            sb.append(" добавлено = " + count);
            // Обновить содержимое всех сущностей назначения в соотв с
            // источником
            count = distrDAO.updateListDistr(updD);
            sb.append(" обновлено = " + count);

            // ------- Station ------------------------------
            StationDAO stDAO = f.getStationDAO();
            // Получить список обновляемых сущностей с сервера (источник)
            List<Station> srcSt = sDAO.loadAllStation();
            // Получить список обновляемых сущностей из лок бд (назначение)
            List<Station> dstSt = stDAO.loadAllStation();
            // получить списки добавляемых, обновляемых и ID удаляемых сущностей
            buildSet(srcSt, dstSt);
            List<Station> insSt = getListByID(srcSt, ins);
            List<Station> updSt = getListByID(srcSt, upd);
            updSt = checkListUpd(updSt, dstSt);
            // удалить из назначения сущности, которых нет в источнике
            List<Integer> dellSt = new ArrayList<>(del);
            count = lDAO.deleteListEntity(dellSt, 3);
            sb.append("\nСтанции: удалено = " + count);
            // Добавить в назначение сущности из источника
            count = stDAO.insertListStation(insSt);
            sb.append(" добавлено = " + count);
            // Обновить содержимое всех сущностей назначения в соотв с
            // источником
            count = stDAO.updateListStation(updSt);
            sb.append(" обновлено = " + count);

            // -------- Span -----------------------------
            SpanDAO spDAO = f.getSpanDAO();
            // Получить список обновляемых сущностей с сервера (источник)
            List<Span> srcSp = sDAO.loadAllSpan();
            // Получить список обновляемых сущностей из лок бд (назначение)
            List<Span> dstSp = spDAO.loadAllSpan();
            // получить списки добавляемых, обновляемых и ID удаляемых сущностей
            buildSet(srcSp, dstSp);
            List<Span> insSp = getListByID(srcSp, ins);
            List<Span> updSp = getListByID(srcSp, upd);
            updSp = checkListUpd(updSp, dstSp);
            // удалить из назначения сущности, которых нет в источнике
            List<Integer> dellSp = new ArrayList<>(del);
            count = lDAO.deleteListEntity(dellSp, 4);
            sb.append("\nПерегоны: удалено = " + count);
            // Добавить в назначение сущности из источника
            count = spDAO.insertListSpan(insSp);
            sb.append(" добавлено = " + count);
            // Обновить содержимое всех сущностей назначения в соотв с
            // источником
            count = spDAO.updateListSpan(updSp);
            sb.append(" обновлено = " + count);

            // --------- Scale ----------------------------
            // Получить список обновляемых сущностей с сервера (источник)
            List<Scale> srcSc = sDAO.loadAllScale();
            // Получить список обновляемых сущностей из лок бд (назначение)
            List<Scale> dstSc = spDAO.loadAllScale();
            // получить списки добавляемых, обновляемых и ID удаляемых сущностей
            buildSet(srcSc, dstSc);
            List<Scale> insSc = getListByID(srcSc, ins);
            List<Scale> updSc = getListByID(srcSc, upd);
            updSc = checkListUpd(updSc, dstSc);
            // удалить их назначения сущности, которых нет в источнике
            List<Integer> dellSc = new ArrayList<>(del);
            count = lDAO.deleteListEntity(dellSc, 5);
            sb.append("\nПерегоны-Участков: удалено = " + count);
            // Добавить в назначение сущности из источника
            count = spDAO.insertListScale(insSc);
            sb.append(" добавлено = " + count);
            // Обновить содержимое всех сущностей назначения в соотв с
            // источником
            count = spDAO.updateListScale(updSc);
            sb.append(" обновлено = " + count);
        }
        return sb.toString();
    }

}
