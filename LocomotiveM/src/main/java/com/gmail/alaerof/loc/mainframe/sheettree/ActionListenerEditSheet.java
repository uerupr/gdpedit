package com.gmail.alaerof.loc.mainframe.sheettree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.gmail.alaerof.loc.entity.sheet.SheetEntity;
import com.gmail.alaerof.loc.application.LinkingLoc;
import com.gmail.alaerof.loc.application.LinkingLocMain;

public class ActionListenerEditSheet implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        LinkingLoc f = LinkingLocMain.mainFrame;
        Object userObject = f.mainPaneBuilder.getSheetTree().getSelectedUserObject();
        if (userObject != null && userObject instanceof SheetEntity) {
            SheetEntity se = (SheetEntity) userObject;
            f.sheetDialog.setSheetEntity(se);
            f.sheetDialog.fillContent();
            f.sheetDialog.setVisible(true);
            f.mainPaneBuilder.updateTree();
        }
    }

}
