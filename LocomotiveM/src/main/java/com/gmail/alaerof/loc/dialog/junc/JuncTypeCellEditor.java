package com.gmail.alaerof.loc.dialog.junc;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.gmail.alaerof.loc.dao.dto.Junc.JuncType;

public class JuncTypeCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener{
    private static final long serialVersionUID = 1L;
    private String jtype;

    public JuncTypeCellEditor(){
        
    }
    
    @Override
    public Object getCellEditorValue() {
        return JuncType.getType(this.jtype);
    }
    
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
            int column) {
        String jname = value.toString();
        this.jtype = jname;
        
        JComboBox<String> combo = new JComboBox<>();

        JuncType[] listT = JuncType.values();
        for (JuncType aJunc : listT) {
            combo.addItem(aJunc.getString());
        }

        combo.setSelectedItem(this.jtype);
        combo.addActionListener(this);

        if (isSelected) {
            combo.setBackground(table.getSelectionBackground());
        } else {
            combo.setBackground(table.getSelectionForeground());
        }

        return combo;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        @SuppressWarnings("unchecked")
        JComboBox<String> combo = (JComboBox<String>) event.getSource();
        this.jtype = (String) combo.getSelectedItem();
    }

}
