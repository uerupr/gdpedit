package com.gmail.alaerof.loc.tab;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JTabbedPane;

public class ButtonTabPane extends JTabbedPane {
    private static final long serialVersionUID = 1L;

    public void initTabs(List<TitledTab> listComp) {
        removeAll();
        TitledTab item;
        String title = "";
        for (int i = 0; i < listComp.size(); i++) {
            item = listComp.get(i);
            title = item.getTitle();
            add(title, item.getTab());
            initTabComponent(i, item.getActionListener());
        }
        setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        setSize(new Dimension(400, 200));
    }

    protected void initTabComponent(int i, ActionListener actionListener) {
        setTabComponentAt(i, new ButtonTabComponent(this, actionListener));
    }
}
