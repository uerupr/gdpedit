package com.gmail.alaerof.loc.util;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class WrapText {
    public static JScrollPane getWrapText(String text){
        JTextArea tarea = new JTextArea();
        tarea.setWrapStyleWord(true);
        tarea.setText(text);
        JScrollPane pane = new JScrollPane(tarea);
        return pane;
    }
}
