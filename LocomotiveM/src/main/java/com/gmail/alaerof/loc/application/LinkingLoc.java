package com.gmail.alaerof.loc.application;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import java.io.InputStream;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.dao.factory.ConnectionBuilder;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.factory.SynchronizerDB;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.dialog.category.CategoryDialog;
import com.gmail.alaerof.loc.dialog.distr.DistrDialog;
import com.gmail.alaerof.loc.dialog.distrturn.DistrTurnAEDialog;
import com.gmail.alaerof.loc.dialog.distrturn.DistrTurnDialog;
import com.gmail.alaerof.loc.dialog.junc.JuncDialog;
import com.gmail.alaerof.loc.dialog.linkloc.LinkLocDialog;
import com.gmail.alaerof.loc.dialog.loc.LocDialog;
import com.gmail.alaerof.loc.dialog.netbr.NetBRDialog;
import com.gmail.alaerof.loc.dialog.sheet.SheetDialog;
import com.gmail.alaerof.loc.dialog.timetable.TrainTimeTDialog;
import com.gmail.alaerof.loc.mainframe.MainConfigDialog;
import com.gmail.alaerof.loc.mainframe.MainSplitPaneBuilder;
import com.gmail.alaerof.loc.mainframe.StatusBar;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditPanel;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditTabPane;
import com.gmail.alaerof.loc.mainframe.sheettree.ActionListenerAddSheet;
import com.gmail.alaerof.loc.mainframe.sheettree.ActionListenerDeleteSheet;
import com.gmail.alaerof.loc.mainframe.sheettree.ActionListenerEditSheet;
import com.gmail.alaerof.loc.util.FileHandler;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JMenuBar;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LinkingLoc extends JFrame {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(LinkingLoc.class);
    //private static final String VERSION_FILE = "res" + File.separator + "versionInfo.properties";
    private LinkingLoc frame;
    private NetBRDialog netBRDialog;
    public SheetDialog sheetDialog;
    public JuncDialog juncDialog;
    public LocDialog locDialog;
    public DistrDialog distrDialog;
    public CategoryDialog categoryDialog;
    public DistrTurnDialog distrTurnDialog;
    public TrainTimeTDialog timetableDialog;
    public LinkLocDialog linkLocDialog;
    public MainConfigDialog mainConfigDialog;
    public MainSplitPaneBuilder mainPaneBuilder;
    public DistrTurnAEDialog distrTurnAEDialog;
    // public SheetGDPEditTabPane sheetGDPEditTabPane;
    public static StatusBar statusBar = new StatusBar();

    public void setCursorWait() {
        setCursor(LinkingLocMain.waitCursor);
    }

    public void setCursorDefault() {
        setCursor(LinkingLocMain.defCursor);
    }

    /**
     * Create the frame.
     */
    public LinkingLoc() {
        super();
        setExtendedState(Frame.MAXIMIZED_BOTH);
        frame = this;
        String title = "АРМ Увязка локомотивов " + getVersion();
        this.setTitle(title);
        initContent();
    }

    private void initContent() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                LocDAOFactory.getInstance().closeConnections();
                ConnectionBuilder.stopServer();
            }
        });

        try {
            BufferedImage buffImage = ImageIO.read(new File("pic//LL1.GIF"));
            setIconImage(buffImage);
        } catch (IOException e) {
            logger.error("создание иконки", e);
        }

        setBounds(100, 100, 450, 300);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        netBRDialog = new NetBRDialog();
        sheetDialog = new SheetDialog(frame, "", true);
        juncDialog = new JuncDialog(frame, "Ж.д. узлы", true);
        locDialog = new LocDialog(frame, "Серии локомотивов", true);
        timetableDialog = new TrainTimeTDialog(frame, "Расписание", false);
        linkLocDialog = new LinkLocDialog(frame, "Увязка", false);
        mainConfigDialog = new MainConfigDialog(frame, "Настройки", true);
        distrDialog = new DistrDialog(frame, "Ж.д. участки", false);
        categoryDialog = new CategoryDialog(frame, "Категории поездов", false);
        distrTurnDialog = new DistrTurnDialog(frame, "Участки обращения", false);
        distrTurnAEDialog = new DistrTurnAEDialog(frame, "", false);

        JToolBar toolBar = new JToolBar();
        // Insets(int top, int left, int bottom, int right)
        // System.out.println(toolBar.getMargin());
        contentPane.add(toolBar, BorderLayout.NORTH);

        {
            JButton toolButton = new JButton("Открыть ГДП");
            toolBar.add(toolButton);
            toolBar.add(Box.createHorizontalStrut(10));

            toolButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ev) {
                    frame.setCursorWait();
                    try {
                        SheetGDPEditPanel edit = getActiveSheetGDPEditPanel();
                        if (edit != null) {
                            edit.fillGDP();
                        }
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    } finally {
                        frame.setCursorDefault();
                    }
                }
            });
        }

        {
            JButton toolButton = new JButton("Сохранить изменения");
            toolBar.add(toolButton);
            toolBar.add(Box.createHorizontalStrut(10));

            toolButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ev) {
                    frame.setCursorWait();
                    try {
                        SheetGDPEditPanel edit = getActiveSheetGDPEditPanel();
                        if (edit != null) {
                            edit.saveGDP();
                        }
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    } finally {
                        frame.setCursorDefault();
                    }
                }
            });
        }

        {
            JButton toolButton = new JButton("Расписание");
            toolBar.add(toolButton);
            toolBar.add(Box.createHorizontalStrut(10));

            toolButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        SheetGDPEditPanel edit = getActiveSheetGDPEditPanel();
                        if (edit != null) {
                            List<TrainThreadSheet> listTr = edit.getSheetEntity().getListTrain();
                            List<SheetDistrSt> listSt = edit.getSheetEntity().getListSheetDistrSt();
                            List<TrainThreadSheet> selected = edit.getSelectedTrains();
                            timetableDialog.setListSt(listSt);
                            if (selected.size() > 0) {
                                timetableDialog.setListTrain(selected);
                            } else {
                                timetableDialog.setListTrain(listTr);
                            }
                            timetableDialog.setVisible(true);
                        }
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }

        {
            JButton toolButton = new JButton("Расчет показателей поездов");
            toolBar.add(toolButton);
            toolBar.add(Box.createHorizontalStrut(10));

            toolButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ev) {
                    frame.setCursorWait();
                    try {
                        SheetGDPEditPanel edit = getActiveSheetGDPEditPanel();
                        if (edit != null) {
                            int err = edit.calculateTrainResults();
                            if (err > 0) {
                                JOptionPane.showMessageDialog(frame,
                                        "Расчет может быть неверным, подробности см. в логе");
                            }
                        }
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    } finally {
                        frame.setCursorDefault();
                    }
                }
            });
        }

        {
            JButton toolButton = new JButton("Печать (файл)");
            toolBar.add(toolButton);
            toolBar.add(Box.createHorizontalStrut(10));

            toolButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        SheetGDPEditPanel edit = getActiveSheetGDPEditPanel();
                        if (edit != null) {
                            String fileName = edit.getSheetEntity().getSheet().name + ".png";
                            File file = FileHandler.chooseFile(false, new FileNameExtensionFilter(
                                    "Image file", "png"), fileName, LinkingLocMain.mainFrame);
                            if (file != null) {
                                frame.setCursorWait();
                                try {
                                    edit.printGDP(file);
                                } finally {
                                    frame.setCursorDefault();
                                }
                            }
                        }
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JButton toolButton = new JButton("Печать (принтер)");
            toolBar.add(toolButton);
            toolBar.add(Box.createHorizontalStrut(10));

            toolButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        SheetGDPEditPanel edit = getActiveSheetGDPEditPanel();
                        frame.setCursorWait();
                        try {
                            edit.printGDP();
                        } finally {
                            frame.setCursorDefault();
                        }
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }

        statusBar.setFont(new Font("SansSerif", Font.PLAIN, 12));
        contentPane.add(statusBar, BorderLayout.SOUTH);

        mainPaneBuilder = new MainSplitPaneBuilder();
        JSplitPane mainPane = mainPaneBuilder.buildPane();
        contentPane.add(mainPane, BorderLayout.CENTER);

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        JMenu mnFile = new JMenu("Приложение");
        menuBar.add(mnFile);
        {
            JMenuItem menuItem = new JMenuItem("Настройки");
            mnFile.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        mainConfigDialog.setVisible(true);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JMenuItem menuItem = new JMenuItem("Обновить нормативные данные с сервера ГДП");
            mnFile.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        String ss = new SynchronizerDB().updateServerTable();
                        JOptionPane.showMessageDialog(frame, ss);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JMenuItem menuItem = new JMenuItem("Исправить категории для всех поездов в БД");
            menuItem.setToolTipText("в соответствии с номерами поездов и диапазонами категорий");
            mnFile.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        boolean result = LocDAOFactory.getInstance().getDistrTrainDAO().updateTrainCat();
                        if (result) {
                            JOptionPane.showMessageDialog(frame, "Категории всех поездов проверены",
                                    "Сообщение",
                                    JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(frame,
                                    "В процессе проверки произошла ошибка см. лог", "Сообщение",
                                    JOptionPane.WARNING_MESSAGE);
                        }
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JMenuItem menuItem = new JMenuItem("Список категорий");
            mnFile.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        categoryDialog.loadData();
                        categoryDialog.setVisible(true);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JMenuItem menuItem = new JMenuItem("Закрыть");
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    frame.setVisible(false);
                    System.exit(0);
                }
            });
            mnFile.add(menuItem);
        }

        JMenu mnNSI = new JMenu("Сеть БЧ");
        menuBar.add(mnNSI);
        {
            JMenuItem menuItem = new JMenuItem("Карта");
            mnNSI.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        netBRDialog.setVisible(true);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JMenuItem menuItem = new JMenuItem("Ж.д. узлы");
            mnNSI.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        juncDialog.loadData();
                        juncDialog.setVisible(true);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JMenuItem menuItem = new JMenuItem("Ж.д. участки");
            mnNSI.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        distrDialog.loadData();
                        distrDialog.setVisible(true);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JMenuItem menuItem = new JMenuItem("Участки обращения");
            mnNSI.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        distrTurnDialog.loadData();
                        distrTurnDialog.setVisible(true);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }

        JMenu mnSheet = new JMenu("Листы увязки");
        menuBar.add(mnSheet);
        {
            JMenuItem menuItem = new JMenuItem("Добавить лист");
            mnSheet.add(menuItem);
            menuItem.addActionListener(new ActionListenerAddSheet());
        }
        {
            JMenuItem menuItem = new JMenuItem("Изменить лист");
            mnSheet.add(menuItem);
            menuItem.addActionListener(new ActionListenerEditSheet());
        }
        {
            JMenuItem menuItem = new JMenuItem("Удалить лист");
            mnSheet.add(menuItem);
            menuItem.addActionListener(new ActionListenerDeleteSheet());
        }

        JMenu mnLink = new JMenu("Увязка");
        menuBar.add(mnLink);
        {
            JMenuItem menuItem = new JMenuItem("Таблица");
            mnLink.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        linkLocDialog.updateData();
                        linkLocDialog.setVisible(true);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }
        {
            JMenuItem menuItem = new JMenuItem("Локомотивы");
            mnLink.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        locDialog.loadData();
                        locDialog.setVisible(true);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }

    }

    protected SheetGDPEditPanel getActiveSheetGDPEditPanel() {
        SheetGDPEditTabPane pane = mainPaneBuilder.getSheetGDPEditTabPane();
        if (pane != null) {
            int index = pane.getSelectedIndex();
            if (index > -1) {
                SheetGDPEditPanel edit = pane.getEditPanelByTabIndex(index);
                return edit;
            }
        }
        return null;
    }

    public void showMesssage(String message) {
        JOptionPane.showMessageDialog(this, message, "Предупреждение", JOptionPane.OK_OPTION);
    }

    /**
     * Get version project from jar
     *
     * <artifactId>Locomotive</artifactId>
     * <version>1.0</version>
     */
    public String getVersion() {
        String version = "";
        try (InputStream is = getClass()
                .getResourceAsStream("/META-INF/maven/" + "by.belsut.uer" + "/"
                        + "Locomotive" + "/pom.properties")) {
            if (is != null) {
                Properties p = new Properties();
                p.load(is);
                version = p.getProperty("version", "").trim();
                if (!version.isEmpty()) {
                    return version;
                }
            }
        } catch (Exception e) {
            // Ignore
        }
        return "";
    }
}
