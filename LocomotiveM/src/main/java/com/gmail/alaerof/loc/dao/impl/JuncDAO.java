package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.JuncSt;
import com.gmail.alaerof.loc.dao.dto.JuncTime;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;

public class JuncDAO {
    private static Logger logger = LogManager.getLogger(JuncDAO.class);
    private Connection cnLocal;

    public JuncDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    public void fillJuncDir(List<Direction> listD, Junc junc) {
        for (Direction d : listD) {
            if (d.dirID == junc.dirID) {
                junc.setDir(d);
            }
        }
    }

    public void fillJuncDir(List<Junc> list) {
        List<Direction> listD = LocDAOFactory.getInstance().getDirectionDAO().loadAllDirection();
        for (Junc junc : list) {
            fillJuncDir(listD, junc);
        }
    }

    /**
     * возвращает список узлов
     * @param sql источник и условия выборки
     * @return список узлов
     */
    private List<Junc> getListJunc(String sql) {
        List<Junc> list = new ArrayList<>();
        sql = "SELECT juncID, codeESR, name, jType, dirID, lineCount " + sql;
        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Junc item = new Junc();
                    item.juncID = rs.getInt("juncID");
                    item.codeESR = rs.getString("codeESR");
                    item.name = rs.getString("name").trim();
                    item.setJuncType(rs.getInt("jType"));
                    item.dirID = rs.getInt("dirID");
                    item.lineCount = rs.getInt("lineCount");
                    item.setListSt(getJuncSt(item.juncID));
                    list.add(item);
                    fillJuncTime(item);
                }
            }
            fillJuncDir(list);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    private void fillJuncTime(Junc junc) {
        String sql = "SELECT juncID, arrive, depart, equipment, acceptance" + " FROM JuncTime WHERE juncID = " + junc.juncID;
        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                if (rs.next()) {
                    JuncTime jt = new JuncTime();
                    jt.juncID = rs.getInt("juncID");
                    jt.arrive = rs.getInt("arrive");
                    jt.depart = rs.getInt("depart");
                    jt.equipment = rs.getInt("equipment");
                    jt.acceptance = rs.getInt("acceptance");
                    junc.setJuncTime(jt);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void deleteJuncTime(JuncTime juncTime) {
        String sql = "DELETE FROM JuncTime WHERE juncID = " + juncTime.juncID;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void saveJuncTime(JuncTime juncTime) {
        if (juncTime != null) {
            deleteJuncTime(juncTime);
            String sql = "INSERT INTO JuncTime (juncID, arrive, depart, equipment, acceptance) VALUES (?,?,?,?,?)";
            try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
                st.setInt(1, juncTime.juncID);
                st.setInt(2, juncTime.arrive);
                st.setInt(3, juncTime.depart);
                st.setInt(4, juncTime.equipment);
                st.setInt(5, juncTime.acceptance);
                st.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    public List<Junc> loadAllJunc() {
        String sql = " FROM junc";
        return getListJunc(sql);
    }

    /** начальный узел заданного участка */
    public Junc getJuncB(int distrID) {
        String sql = " FROM junc WHERE juncID IN (SELECT juncB FROM distr WHERE distrID = " + distrID + ")";
        List<Junc> list = getListJunc(sql);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    /** конечный узел заданного участка */
    public Junc getJuncE(int distrID) {
        String sql = " FROM junc WHERE juncID IN (SELECT juncE FROM distr WHERE distrID = " + distrID + ")";
        List<Junc> list = getListJunc(sql);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    /**
     * возвращает списое станций узла из БД
     * 
     * @param juncID код узла
     * @return список станций
     */
    private List<JuncSt> getJuncSt(int juncID) {
        List<JuncSt> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement();) {
            String sql = "SELECT juncID, JuncSt.stationID, num, codeESR, name"
                    + " FROM JuncSt INNER JOIN Station ON JuncSt.stationID = Station.stationID " + " WHERE juncID = " + juncID;
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    JuncSt item = new JuncSt();
                    item.juncID = rs.getInt("juncID");
                    item.stationID = rs.getInt("stationID");
                    item.num = rs.getInt("num");
                    String s = rs.getString("codeESR");
                    if (s != null) {
                        s = s.trim();
                    }
                    item.codeESR = s;
                    item.name = rs.getString("name").trim();
                    list.add(item);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**
     * возвращает узел из БД по имени, Null если такого узла нет
     * 
     * @param name
     * @return узел
     */
    public Junc getJuncByName(String name) {
        Junc junc = null;
        String sql = " FROM junc WHERE name = '" + name + "'";
        List<Junc> list = getListJunc(sql);
        if (list.size() > 0) {
            junc = list.get(0);
        }
        return junc;
    }

    /**
     * 
     * @param junc
     */
    private void insertJunc(Junc junc) {
        String sql = "INSERT INTO junc (codeESR, name, jType, dirID, lineCount) VALUES (?,?,?,?,?)";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setString(1, junc.codeESR.trim());
            st.setString(2, junc.name.trim());
            st.setInt(3, junc.getJuncType().getInt());
            st.setInt(4, junc.dirID);
            st.setInt(5, junc.lineCount);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * добавляет узел в БД (проверка по имени)
     * 
     * @param junc
     *            - узел
     */
    public void addJunc(Junc junc) {
        insertJunc(junc);
        Junc newJ = getJuncByName(junc.name);
        if (newJ != null && newJ.juncID > 0) {
            junc.juncID = newJ.juncID;
            junc.getJuncTime().juncID = newJ.juncID;
            insertJuncSt(junc);
            saveJuncTime(junc.getJuncTime());
        }
    }

    /**
     * удяляет узел из БД (по имени)
     * 
     * @param junc
     */
    public void deleteJunc(Junc junc) {
        String sql = "DELETE FROM junc WHERE name = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setString(1, junc.name);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * удяляет список узлов из БД (по имени)
     * 
     * @param list
     */
    public void deleteJunc(List<Junc> list) {
        for (int i = 0; i < list.size(); i++) {
            Junc junc = list.get(i);
            deleteJunc(junc);
        }
    }

    /**
     * обновляет все свойства узла в БД
     * 
     * @param junc
     *            - узел
     */
    public void updateJunc(Junc junc) {
        deleteJuncSt(junc);
        insertJuncSt(junc);
        String sql = "UPDATE junc SET codeESR = ?, name = ?, jType = ?, dirID = ?, lineCount = ? WHERE juncID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setString(1, junc.codeESR);
            st.setString(2, junc.name);
            st.setInt(3, junc.getJuncType().getInt());
            st.setInt(4, junc.dirID);
            st.setInt(5, junc.lineCount);
            st.setInt(6, junc.juncID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        saveJuncTime(junc.getJuncTime());
    }

    /**
     * удаляет все станции узла из БД (связь)
     * 
     * @param junc
     *            - узел
     */
    private void deleteJuncSt(Junc junc) {
        String sql = "DELETE FROM JuncSt WHERE juncID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, junc.juncID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * добавляет все станции узла в БД (связь)
     * @param junc - узел
     */
    private void insertJuncSt(Junc junc) {
        String sql = "INSERT INTO JuncSt(juncID, stationID, num) VALUES(?,?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            List<JuncSt> list = junc.getListSt();
            for (int i = 0; i < list.size(); i++) {
                JuncSt js = list.get(i);
                st.setInt(1, junc.juncID);
                st.setInt(2, js.stationID);
                st.setInt(3, js.num);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }

    }

    /**
     * возвращает список ID участков, у которых есть станции заданного узла
     * 
     * @param juncID
     * @return
     */
    public List<Integer> getDistrIDByJunc(int juncID) {
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT DISTINCT distrID FROM SheetDistrSt " + "WHERE stationID IN (SELECT stationID FROM JuncSt WHERE JuncID = ?)";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, juncID);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    Integer distrID = rs.getInt("distrID");
                    list.add(distrID);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**
     * возвращает узел по ИД
     * @param juncID ИД узла
     * @return узел
     */
    public Junc getJunc(int juncID) {
        String sql = " FROM junc WHERE juncID = " + juncID;
        List<Junc> list = getListJunc(sql);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

}
