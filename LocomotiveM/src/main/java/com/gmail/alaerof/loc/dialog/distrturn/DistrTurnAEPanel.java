package com.gmail.alaerof.loc.dialog.distrturn;

import java.util.Collections;
import java.util.List;

import javax.swing.JPanel;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.DistrTurn;
import com.gmail.alaerof.loc.dao.dto.Station;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.DistrSpan;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.border.EtchedBorder;

public class DistrTurnAEPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(DistrTurnAEPanel.class);
    private JComboBox<Direction> cbDir;
    private JComboBox<Distr> cbDistr;
    private JComboBox<Station> cbStation;
    private JTextField distrTurnName;
    private JLabel nameDir;
    private JLabel nameDistr;
    private JLabel nameStB;
    private JLabel nameStE;
    private DistrTurn distrTurn;
    private Direction selectedDir;
    private Distr selectedDistr;
    private Station selectedSt;
    /** перегоны ж.д. участка */
    private SpanTableModel tableModelDS = new SpanTableModel();
    /** перегоны участка обращения */
    private SpanTableModel tableModelTS = new SpanTableModel();
    private JTable tableDS = new JTable(tableModelDS);
    private JTable tableTS = new JTable(tableModelTS);

    public DistrTurnAEPanel() {
        setLayout(new BorderLayout(0, 0));

        // ================= choosePanel ====================

        JPanel choosePanel = new JPanel();
        choosePanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        choosePanel.setPreferredSize(new Dimension(420, 0));
        add(choosePanel, BorderLayout.EAST);
        choosePanel.setLayout(new BorderLayout(0, 0));

        JPanel chooseTopPanel = new JPanel();
        chooseTopPanel.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
        chooseTopPanel.setPreferredSize(new Dimension(0, 140));
        choosePanel.add(chooseTopPanel, BorderLayout.NORTH);
        GridBagLayout gbl_panel_2 = new GridBagLayout();
        gbl_panel_2.columnWidths = new int[] { 0, 0, 0, 0 };
        gbl_panel_2.rowHeights = new int[] { 0, 0, 0, 0, 0 };
        gbl_panel_2.columnWeights = new double[] { 1.0, 0.0, 0.0, Double.MIN_VALUE };
        gbl_panel_2.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        chooseTopPanel.setLayout(gbl_panel_2);

        cbDir = new JComboBox<>();
        GridBagConstraints gbc_dirCB = new GridBagConstraints();
        gbc_dirCB.insets = new Insets(0, 0, 5, 5);
        gbc_dirCB.fill = GridBagConstraints.HORIZONTAL;
        gbc_dirCB.gridx = 0;
        gbc_dirCB.gridy = 0;
        chooseTopPanel.add(cbDir, gbc_dirCB);
        cbDir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                Direction dir = (Direction) cbDir.getSelectedItem();
                if (dir != null && !dir.equals(selectedDir)) {
                    selectedDir = dir;
                    fillDistr();
                }
            }
        });

        JLabel dirSelLabel = new JLabel("направление");
        GridBagConstraints gbc_dirSelLabel = new GridBagConstraints();
        gbc_dirSelLabel.anchor = GridBagConstraints.WEST;
        gbc_dirSelLabel.insets = new Insets(0, 0, 5, 5);
        gbc_dirSelLabel.gridx = 1;
        gbc_dirSelLabel.gridy = 0;
        chooseTopPanel.add(dirSelLabel, gbc_dirSelLabel);

        JButton btnDirSelect = new JButton("Выбрать");
        GridBagConstraints gbc_btnDirSelect = new GridBagConstraints();
        gbc_btnDirSelect.anchor = GridBagConstraints.WEST;
        gbc_btnDirSelect.insets = new Insets(0, 0, 5, 0);
        gbc_btnDirSelect.gridx = 2;
        gbc_btnDirSelect.gridy = 0;
        chooseTopPanel.add(btnDirSelect, gbc_btnDirSelect);
        btnDirSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                Direction dir = (Direction) cbDir.getSelectedItem();
                if (dir != null) {
                    distrTurn.dirID = dir.dirID;
                    distrTurn.dirName = dir.name.trim();
                    fillDistrTurnValue();
                }
            }
        });

        cbDistr = new JComboBox<>();
        GridBagConstraints gbc_distrCB = new GridBagConstraints();
        gbc_distrCB.insets = new Insets(0, 0, 5, 5);
        gbc_distrCB.fill = GridBagConstraints.HORIZONTAL;
        gbc_distrCB.gridx = 0;
        gbc_distrCB.gridy = 1;
        chooseTopPanel.add(cbDistr, gbc_distrCB);
        cbDistr.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                Distr distr = (Distr) cbDistr.getSelectedItem();
                if (distr != null && !distr.equals(selectedDistr)) {
                    selectedDistr = distr;
                    fillSt();
                    fillSpan();
                }
            }
        });

        JLabel distrSelLabel = new JLabel("ж.д. участок");
        GridBagConstraints gbc_distrSelLabel = new GridBagConstraints();
        gbc_distrSelLabel.anchor = GridBagConstraints.WEST;
        gbc_distrSelLabel.insets = new Insets(0, 0, 5, 5);
        gbc_distrSelLabel.gridx = 1;
        gbc_distrSelLabel.gridy = 1;
        chooseTopPanel.add(distrSelLabel, gbc_distrSelLabel);

        JButton btnDistrSelect = new JButton("Выбрать");
        GridBagConstraints gbc_btnDistrSelect = new GridBagConstraints();
        gbc_btnDistrSelect.anchor = GridBagConstraints.WEST;
        gbc_btnDistrSelect.insets = new Insets(0, 0, 5, 0);
        gbc_btnDistrSelect.gridx = 2;
        gbc_btnDistrSelect.gridy = 1;
        chooseTopPanel.add(btnDistrSelect, gbc_btnDistrSelect);
        btnDistrSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Distr distr = (Distr) cbDistr.getSelectedItem();
                if (distr != null) {
                    distrTurn.distrID = distr.distrID;
                    distrTurn.distrName = distr.name.trim();
                    fillDistrTurnValue();
                }
            }
        });

        cbStation = new JComboBox<>();
        GridBagConstraints gbc_stCB = new GridBagConstraints();
        gbc_stCB.insets = new Insets(0, 0, 5, 5);
        gbc_stCB.fill = GridBagConstraints.HORIZONTAL;
        gbc_stCB.gridx = 0;
        gbc_stCB.gridy = 2;
        chooseTopPanel.add(cbStation, gbc_stCB);
        cbStation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                Station st = (Station) cbStation.getSelectedItem();
                if (st != null && !st.equals(selectedSt)) {
                    selectedSt = st;
                }
            }
        });

        JLabel stSelLabel = new JLabel("станция");
        GridBagConstraints gbc_stSelLabel = new GridBagConstraints();
        gbc_stSelLabel.anchor = GridBagConstraints.WEST;
        gbc_stSelLabel.insets = new Insets(0, 0, 5, 5);
        gbc_stSelLabel.gridx = 1;
        gbc_stSelLabel.gridy = 2;
        chooseTopPanel.add(stSelLabel, gbc_stSelLabel);

        JButton btnStBSelect = new JButton("Выбрать Нач.");
        GridBagConstraints gbc_btnStBSelect = new GridBagConstraints();
        gbc_btnStBSelect.anchor = GridBagConstraints.WEST;
        gbc_btnStBSelect.insets = new Insets(0, 0, 5, 0);
        gbc_btnStBSelect.gridx = 2;
        gbc_btnStBSelect.gridy = 2;
        chooseTopPanel.add(btnStBSelect, gbc_btnStBSelect);
        btnStBSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Station st = (Station) cbStation.getSelectedItem();
                if (st != null) {
                    distrTurn.stBID = st.stationID;
                    distrTurn.stB = st;
                    fillDistrTurnValue();
                }
            }
        });

        JButton btnStESelect = new JButton("Выбрать Кон.");
        GridBagConstraints gbc_btnStESelect = new GridBagConstraints();
        gbc_btnStESelect.anchor = GridBagConstraints.WEST;
        gbc_btnStESelect.gridx = 2;
        gbc_btnStESelect.gridy = 3;
        chooseTopPanel.add(btnStESelect, gbc_btnStESelect);
        btnStESelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Station st = (Station) cbStation.getSelectedItem();
                if (st != null) {
                    distrTurn.stEID = st.stationID;
                    distrTurn.stE = st;
                    fillDistrTurnValue();
                }
            }
        });

        int colSize[] = { 200 };
        tableDS.setFillsViewportHeight(true);
        tableDS.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(tableDS, colSize);
        tableDS.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(tableDS);
        choosePanel.add(scroll, BorderLayout.CENTER);
        tableDS.setToolTipText("Двойной клик мыши ДОБАВЛЯЕТ перегон к участку обращения");
        tableDS.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent ev) {
                int row = tableDS.getSelectedRow();
                if ((row > -1)&&(ev.getClickCount() == 2)) {
                    DistrSpan ds = tableModelDS.getList().get(row);
                    List<DistrSpan> list = tableModelTS.getList();
                    list.add(ds);
                    Collections.sort(list);
                    tableTS.repaint();
                }
            }
        });

        // ================= distrTurnPanel ====================

        JPanel distrTurnPanel = new JPanel();
        distrTurnPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        add(distrTurnPanel, BorderLayout.CENTER);
        distrTurnPanel.setLayout(new BorderLayout(0, 0));

        JPanel distrTurnTopPanel = new JPanel();
        distrTurnTopPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        distrTurnTopPanel.setPreferredSize(new Dimension(0, 140));
        distrTurnPanel.add(distrTurnTopPanel, BorderLayout.NORTH);
        GridBagLayout gbl_distrTurnTopPanel = new GridBagLayout();
        gbl_distrTurnTopPanel.columnWidths = new int[] { 0, 0, 0 };
        gbl_distrTurnTopPanel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        gbl_distrTurnTopPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
        gbl_distrTurnTopPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        distrTurnTopPanel.setLayout(gbl_distrTurnTopPanel);

        JLabel distrTurnNameLabel = new JLabel("Название участка обращения: ");
        GridBagConstraints gbc_distrTurnNameLabel = new GridBagConstraints();
        gbc_distrTurnNameLabel.insets = new Insets(0, 0, 5, 5);
        gbc_distrTurnNameLabel.anchor = GridBagConstraints.WEST;
        gbc_distrTurnNameLabel.gridx = 0;
        gbc_distrTurnNameLabel.gridy = 0;
        distrTurnTopPanel.add(distrTurnNameLabel, gbc_distrTurnNameLabel);

        distrTurnName = new JTextField();
        GridBagConstraints gbc_distrTurnName = new GridBagConstraints();
        gbc_distrTurnName.insets = new Insets(0, 0, 5, 0);
        gbc_distrTurnName.fill = GridBagConstraints.HORIZONTAL;
        gbc_distrTurnName.gridx = 1;
        gbc_distrTurnName.gridy = 0;
        distrTurnTopPanel.add(distrTurnName, gbc_distrTurnName);
        distrTurnName.setColumns(10);
        distrTurnName.addFocusListener(new FocusListener() {
            @Override
            public void focusLost(FocusEvent ev) {
                distrTurn.name = distrTurnName.getText().trim();
            }
            @Override
            public void focusGained(FocusEvent ev) {
            }
        });
        
        JLabel dirLabel = new JLabel("Направление: ");
        GridBagConstraints gbc_dirLabel = new GridBagConstraints();
        gbc_dirLabel.anchor = GridBagConstraints.WEST;
        gbc_dirLabel.insets = new Insets(0, 0, 5, 5);
        gbc_dirLabel.gridx = 0;
        gbc_dirLabel.gridy = 1;
        distrTurnTopPanel.add(dirLabel, gbc_dirLabel);

        nameDir = new JLabel("--");
        nameDir.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_dirNameLabel = new GridBagConstraints();
        gbc_dirNameLabel.anchor = GridBagConstraints.WEST;
        gbc_dirNameLabel.insets = new Insets(0, 0, 5, 0);
        gbc_dirNameLabel.gridx = 1;
        gbc_dirNameLabel.gridy = 1;
        distrTurnTopPanel.add(nameDir, gbc_dirNameLabel);

        JLabel distrLabel = new JLabel("Ж.д. участок: ");
        GridBagConstraints gbc_distrLabel = new GridBagConstraints();
        gbc_distrLabel.anchor = GridBagConstraints.WEST;
        gbc_distrLabel.insets = new Insets(0, 0, 5, 5);
        gbc_distrLabel.gridx = 0;
        gbc_distrLabel.gridy = 2;
        distrTurnTopPanel.add(distrLabel, gbc_distrLabel);

        nameDistr = new JLabel("--");
        nameDistr.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_distrNameLabel = new GridBagConstraints();
        gbc_distrNameLabel.anchor = GridBagConstraints.WEST;
        gbc_distrNameLabel.insets = new Insets(0, 0, 5, 0);
        gbc_distrNameLabel.gridx = 1;
        gbc_distrNameLabel.gridy = 2;
        distrTurnTopPanel.add(nameDistr, gbc_distrNameLabel);

        JLabel stBLabel = new JLabel("Начальная станция уч.об.: ");
        GridBagConstraints gbc_stBLabel = new GridBagConstraints();
        gbc_stBLabel.anchor = GridBagConstraints.WEST;
        gbc_stBLabel.insets = new Insets(0, 0, 5, 5);
        gbc_stBLabel.gridx = 0;
        gbc_stBLabel.gridy = 3;
        distrTurnTopPanel.add(stBLabel, gbc_stBLabel);

        nameStB = new JLabel("--");
        nameStB.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_stBNameLabel = new GridBagConstraints();
        gbc_stBNameLabel.anchor = GridBagConstraints.WEST;
        gbc_stBNameLabel.insets = new Insets(0, 0, 5, 0);
        gbc_stBNameLabel.gridx = 1;
        gbc_stBNameLabel.gridy = 3;
        distrTurnTopPanel.add(nameStB, gbc_stBNameLabel);

        JLabel stELabel = new JLabel("Конечная станция уч.об.: ");
        GridBagConstraints gbc_stELabel = new GridBagConstraints();
        gbc_stELabel.anchor = GridBagConstraints.NORTHWEST;
        gbc_stELabel.insets = new Insets(0, 0, 0, 5);
        gbc_stELabel.gridx = 0;
        gbc_stELabel.gridy = 4;
        distrTurnTopPanel.add(stELabel, gbc_stELabel);

        nameStE = new JLabel("--");
        nameStE.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_stENameLabel = new GridBagConstraints();
        gbc_stENameLabel.anchor = GridBagConstraints.WEST;
        gbc_stENameLabel.gridx = 1;
        gbc_stENameLabel.gridy = 4;
        distrTurnTopPanel.add(nameStE, gbc_stENameLabel);

        tableTS.setFillsViewportHeight(true);
        tableTS.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(tableTS, colSize);
        tableTS.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scroll = new JScrollPane(tableTS);
        distrTurnPanel.add(scroll, BorderLayout.CENTER);
        tableTS.setToolTipText("Двойной клик мыши УДАЛЯЕТ перегон из участка обращения");
        tableTS.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent ev) {
                int row = tableTS.getSelectedRow();
                if ((row > -1)&&(ev.getClickCount() == 2)) {
                    DistrSpan ds = tableModelTS.getList().get(row);
                    List<DistrSpan> list = tableModelTS.getList();
                    list.remove(ds);
                    Collections.sort(list);
                    tableTS.repaint();
                }
            }
        });
        
    }

    /**
     * заполняет форму в соответствии с объектом участка обращения,
     * загружает из БД список НОД
     * @param distrTurn участок обращения
     */
    public void loadData(DistrTurn distrTurn) {
        this.distrTurn = distrTurn;
        fillDistrTurnValue();
        
        List<Direction> list = LocDAOFactory.getInstance().getDirectionDAO().loadAllDirection();
        Collections.sort(list);
        cbDir.removeAllItems();
        for (int i = 0; i < list.size(); i++) {
            cbDir.addItem(list.get(i));
        }
    }

    /**
     * заполняет форму в соответствии с имеющимся объектом участка обращения,
     * загружает из БД список перегонов участка обращения
     */
    private void fillDistrTurnValue() {
        if (distrTurn != null) {
            distrTurnName.setText(distrTurn.name);
            nameDir.setText(distrTurn.dirName);
            nameDistr.setText(distrTurn.distrName);
            if (distrTurn.stB != null) {
                nameStB.setText(distrTurn.stB.name);
            } else {
                nameStB.setText("-");

            }
            if (distrTurn.stE != null) {
                nameStE.setText(distrTurn.stE.name);
            } else {
                nameStE.setText("-");
            }
            
            // список перегонов участка обращения из БД
            List<DistrSpan> list = LocDAOFactory.getInstance().getSpanDAO().loadDistrTurnSpan(distrTurn.distrTurnID);
            tableModelTS.setList(list);
        }
    }

    /**
     * заполняет combobox участков, если есть выбранный НОД 
     */
    private void fillDistr() {
        if (selectedDir != null) {
            List<Distr> list = LocDAOFactory.getInstance().getDistrDAO().getListDistr(selectedDir.dirID);
            Collections.sort(list);
            cbDistr.removeAllItems();
            for (int i = 0; i < list.size(); i++) {
                cbDistr.addItem(list.get(i));
            }
        }
    }

    /**
     * заполняет combobox станций, если есть выбранный участок
     */
    private void fillSt() {
        if (selectedDistr != null) {
            List<Station> list = LocDAOFactory.getInstance().getStationDAO().getDistrStation(selectedDistr.distrID);
            Collections.sort(list);
            cbStation.removeAllItems();
            for (int i = 0; i < list.size(); i++) {
                cbStation.addItem(list.get(i));
            }
        }
    }

    /**
     * заполняет таблицу перегонов, если есть выбранный участок
     */
    private void fillSpan() {
        if (selectedDistr != null) {
            List<DistrSpan> list = LocDAOFactory.getInstance().getSpanDAO().loadDistrSpan(selectedDistr.distrID);
            Collections.sort(list);
            tableModelDS.setList(list);
            tableDS.repaint();
        }
    }
    
    /**
     * возвращает список перегонов для участка обращения
     * @return список DistrSpan для участка обращения
     */
    public List<DistrSpan> getTurnDistrSpan(){
        return tableModelTS.getList();
    }
}
