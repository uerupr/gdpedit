package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.SeriesLoc;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.linktrain.LinkTrainEntity;

public class LinkTrainDAO {
    private static Logger logger = LogManager.getLogger(LinkTrainDAO.class);
    private Connection cnLocal;

    public LinkTrainDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    /** все увязки участка */
    public List<LinkTrain> loadDistrLinkTrain(int distrID) {
        String sql = " FROM linktrain WHERE distrIDon = " + distrID + " OR distrIDoff = " + distrID;
        return getListLinkTrain(sql);
    }

    /** все увязки одного узла */
    public List<LinkTrain> loadJuncLinkTrain(int juncID) {
        String sql = " FROM linktrain WHERE juncID = " + juncID;
        return getListLinkTrain(sql);
    }

    /** загрузка списка увязок */
    private List<LinkTrain> getListLinkTrain(String sql) {
        List<LinkTrain> list = new ArrayList<>();
        sql = "SELECT linkTrainID, juncID, seriesLocID, distrIDon, codeTRon, distrIDoff, codeTRoff, nameTRon, nameTRoff, lineNumber, tm "
                + sql;
        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    LinkTrain item = new LinkTrain();
                    item.linkTrainID = rs.getInt("linkTrainID");
                    item.juncID = rs.getInt("juncID");
                    item.seriesLocID = rs.getInt("seriesLocID");
                    item.distrIDon = rs.getInt("distrIDon");
                    item.codeTRon = rs.getInt("codeTRon");
                    item.distrIDoff = rs.getInt("distrIDoff");
                    item.codeTRoff = rs.getInt("codeTRoff");
                    item.setNameTRon(rs.getString("nameTRon"));
                    item.setNameTRoff(rs.getString("nameTRoff"));
                    item.lineNumber = rs.getInt("lineNumber");
                    item.tm = rs.getInt("tm");
                    list.add(item);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        fillDistrName(list);
        fillLocName(list);
        return list;
    }

    private void fillLocName(List<LinkTrain> list) {
        SeriesLocDAO locDAO = LocDAOFactory.getInstance().getSeriesLocDAO();
        List<SeriesLoc> listL = locDAO.loadAllSLoc();
        for (LinkTrain lt : list) {
            lt.locName = locDAO.getNameLoc(listL, lt.seriesLocID);
        }
    }

    private void fillDistrName(List<LinkTrain> list) {
        DistrDAO distrDAO = LocDAOFactory.getInstance().getDistrDAO();
        List<Distr> listD = distrDAO.loadAllDistr();
        for (LinkTrain lt : list) {
            lt.distrNameOn = distrDAO.getNameDistr(listD, lt.distrIDon);
            lt.distrNameOff = distrDAO.getNameDistr(listD, lt.distrIDoff);
        }
    }

    /** сохранить в БД список увязок узла (с предварительным удалением) */
    public boolean saveListLinkTrain(int juncID, List<LinkTrain> listLinkTrain) {
        boolean res = true;
        // удаление увязок узла
        deleteJuncLinkTrain(juncID);
        // создание новых
        String sql = "INSERT INTO linktrain (juncID, seriesLocID, distrIDon, codeTRon, distrIDoff, codeTRoff, nameTRon, nameTRoff, lineNumber, tm) VALUES(?,?,?,?,?,?,?,?,?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (LinkTrain link : listLinkTrain) {
                // linktrain link = linkEnt.getLinkTrain();
                st.setInt(1, link.juncID);
                st.setInt(2, link.seriesLocID);
                st.setInt(3, link.distrIDon);
                st.setInt(4, link.codeTRon);
                st.setInt(5, link.distrIDoff);
                st.setInt(6, link.codeTRoff);
                st.setString(7, link.nameTRon);
                st.setString(8, link.nameTRoff);
                st.setInt(9, link.lineNumber);
                st.setInt(10, link.tm);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            res = false;
        }
        return res;
    }

    /** сохранить в БД список увязок узла (с предварительным удалением) */
    public void saveListLinkTrainEntity(int juncID, List<LinkTrainEntity> listLinkTrain) {
        List<LinkTrain> list = new ArrayList<>();
        for (LinkTrainEntity linkEnt : listLinkTrain) {
            LinkTrain link = linkEnt.getLinkTrain();
            list.add(link);
        }
        saveListLinkTrain(juncID, list);
    }

    /** удаление из БД всех увязок узла */
    public void deleteJuncLinkTrain(int juncID) {
        try (Statement st = cnLocal.createStatement();) {
            String sql = "DELETE FROM linktrain WHERE juncID = " + juncID;
            st.executeUpdate(sql);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /** сохранение в БД одной увязки (с предварительным удалением) */
    public void saveLink(LinkTrain link) {
        // удаление старой
        deleteLink(link);
        // создание новой
        String sql = "INSERT INTO linktrain (juncID, seriesLocID, distrIDon, codeTRon, distrIDoff, codeTRoff, nameTRon, nameTRoff, lineNumber, tm) VALUES(?,?,?,?,?,?,?,?,?,?)";
        try (PreparedStatement pstIns = cnLocal.prepareStatement(sql)) {
            pstIns.setInt(1, link.juncID);
            pstIns.setInt(2, link.seriesLocID);
            pstIns.setInt(3, link.distrIDon);
            pstIns.setInt(4, link.codeTRon);
            pstIns.setInt(5, link.distrIDoff);
            pstIns.setInt(6, link.codeTRoff);
            pstIns.setString(7, link.nameTRon);
            pstIns.setString(8, link.nameTRoff);
            pstIns.setInt(9, link.lineNumber);
            pstIns.setInt(10, link.tm);
            pstIns.addBatch();
            pstIns.executeBatch();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /** удаление увязки из БД */
    public void deleteLink(LinkTrain link) {
        String sql = "DELETE FROM linktrain WHERE juncID = ? AND distrIDon = ? AND codeTRon = ? AND distrIDoff = ? AND codeTRoff = ? AND nameTRon = ? AND nameTRoff = ?";
        try (PreparedStatement pstDel = cnLocal.prepareStatement(sql)) {
            pstDel.setInt(1, link.juncID);
            pstDel.setInt(2, link.distrIDon);
            pstDel.setInt(3, link.codeTRon);
            pstDel.setInt(4, link.distrIDoff);
            pstDel.setInt(5, link.codeTRoff);
            pstDel.setString(6, link.nameTRon);
            pstDel.setString(7, link.nameTRoff);
            pstDel.addBatch();
            pstDel.executeBatch();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

}
