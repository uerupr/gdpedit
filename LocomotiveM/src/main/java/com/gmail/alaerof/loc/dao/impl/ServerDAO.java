package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Category;
import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Scale;
import com.gmail.alaerof.loc.dao.dto.Span;
import com.gmail.alaerof.loc.dao.dto.Station;

/** работает с сервером */
public class ServerDAO {
    private static Logger logger = LogManager.getLogger(ServerDAO.class);
    private Connection cnServer;
    
    public boolean isConnectionNull(){
        return cnServer == null;
    }

    public ServerDAO(Connection cnServer) {
        this.cnServer = cnServer;
    }
    
    public List<Category> loadAllCategory() {
        List<Category> list = new ArrayList<>();
        try (Statement st = cnServer.createStatement();) {
            String sql = "SELECT IDcat, name, prioritet_, trMin, trMax, type FROM category";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Category cat = new Category();
                    cat.categoryID = rs.getInt("IDcat");
                    cat.name = rs.getString("name").trim();
                    cat.prior = rs.getInt("prioritet_");
                    cat.trMin = rs.getInt("trMin");
                    cat.trMax = rs.getInt("trMax");
                    cat.type = rs.getString("type").trim();
                    list.add(cat);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }
    
    public List<Direction> loadAllDirection() {
        List<Direction> list = new ArrayList<>();
        try (Statement st = cnServer.createStatement();) {
            String sql = "SELECT IDdir, Name, ord FROM Direction";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Direction dir = new Direction();
                    dir.dirID = rs.getInt("IDdir");
                    dir.name = rs.getString("Name").trim();
                    dir.orderNum = rs.getInt("ord");
                    list.add(dir);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    public List<Distr> loadAllDistr() {
        List<Distr> list = new ArrayList<>();
        try (Statement st = cnServer.createStatement();) {
            String sql = "SELECT IDdistr, IDdir, name FROM distr";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Distr d = new Distr();
                    d.distrID = rs.getInt("IDdistr");
                    d.dirID = rs.getInt("IDdir");
                    d.name = rs.getString("name").trim();
                    list.add(d);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    public List<Station> loadAllStation() {
        List<Station> list = new ArrayList<>();
        List<String> listFail = new ArrayList<>();
        try (Statement st = cnServer.createStatement();) {
            String sql = "SELECT IDst, IDdir, name, CodeESR, CodeESR2, CodeExpress, BR, joint FROM Station";
            boolean failCode = false;
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    failCode = false;
                    Station s = new Station();
                    s.stationID = rs.getInt("IDst");
                    s.dirID = rs.getInt("IDdir");
                    s.name = rs.getString("name").trim();
                    String codeESR = rs.getString("CodeESR");
                    if (codeESR == null) {
                        codeESR = "0";
                        failCode = true;
                        listFail.add("У станции " + s.name + " нет кода ЕСР\n");

                    }
                    String codeESR2 = rs.getString("CodeESR2");
                    if (codeESR2 == null) {
                        codeESR2 = "00";
                    }
                    String codeE = rs.getString("CodeExpress");
                    if (codeE == null) {
                        codeE = "0";
                        // failCode = true;
                        // listFail.add("У станции " + s.name +
                        // " нет кода Экспресс ");
                    }
                    s.codeESR = codeESR.trim();
                    s.codeESR2 = codeESR2.trim();
                    s.codeExpress = codeE.trim();
                    s.br = rs.getBoolean("BR");
                    s.joint = rs.getBoolean("joint");
                    if (!failCode) {
                        list.add(s);
                    }
                }
            }
            if (listFail.size() > 0) {
                logger.error(listFail.toString());
                JOptionPane.showMessageDialog(null, listFail.toString());
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }
    
    public List<Span> loadAllSpan() {
        List<Span> list = new ArrayList<>();
        try (Statement st = cnServer.createStatement();) {
            String sql = "SELECT IDspan, IDdir, name, SCB, line, IDst, IDst_e, BR  FROM Span";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Span item = new Span();
                    item.spanID = rs.getInt("IDspan");
                    item.dirID = rs.getInt("IDdir");
                    item.name = rs.getString("name").trim();
                    String scb = rs.getString("SCB");
                    if (scb != null)
                        scb = scb.trim();
                    item.SCB = scb;
                    item.line = rs.getInt("line");
                    item.stB = rs.getInt("IDst");
                    item.stE = rs.getInt("IDst_e");
                    item.br = rs.getBoolean("br");
                    list.add(item);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }
    
    public List<Scale> loadAllScale() {
        List<Scale> list = new ArrayList<>();
        try (Statement st = cnServer.createStatement();) {
            String sql = "SELECT IDscale, IDdistr, IDspan, num, L, absKM, absKM_e  FROM Scale";
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Scale item = new Scale();
                    item.scaleID = rs.getInt("IDscale");
                    item.distrID = rs.getInt("IDdistr");
                    item.spanID = rs.getInt("IDspan");
                    item.num = rs.getInt("num");
                    item.L = rs.getInt("L");
                    item.absKM = rs.getDouble("absKM");
                    item.absKM_e = rs.getDouble("absKM_e");
                    list.add(item);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }
}
