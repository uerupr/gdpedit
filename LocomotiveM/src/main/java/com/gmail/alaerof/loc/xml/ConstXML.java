package com.gmail.alaerof.loc.xml;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class ConstXML {
    public static final String tempDir = "dataTemp";
    public static final String tempFile = tempDir + File.separator + "temp.xml";

    public static void copyTempFile(String fileName) throws IOException {
        File dir = new File(ConstXML.tempDir);
        if (!dir.exists()) {
            dir.mkdir();
        }
        File oldFile = new File(fileName);
        File newFile = new File(ConstXML.tempFile);

        if (oldFile.exists()) {
            Files.copy(oldFile.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }

    }

    public static void delTempFile() {
        File file = new File(ConstXML.tempFile);
        if (file.exists()) {
            file.delete();
        }
    }
 
}
