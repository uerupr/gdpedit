package com.gmail.alaerof.loc.dao.dto;

public class DistrTurnResult {
    public int distrTurnID;
    public int rangeTRID;
    public int seriesLocID;
    public int countTrain_o;
    public int countTrain_e;
    public int countLoc_o;
    public int countLoc_e;
    public double hourTT_o;
    public double hourTT_e;
    public double hourTU_o;
    public double hourTU_e;
    public double hourArr_o;
    public double hourArr_e;
    public double hourDep_o;
    public double hourDep_e;
    public double hourEq_o; // по обороту
    public double hourEq_e; // по обороту
    public double trkm_o;
    public double trkm_e;
    
    // -- add ----
    public String nameDT = "";
    public String nameR = "";
    public String series = "";
}
