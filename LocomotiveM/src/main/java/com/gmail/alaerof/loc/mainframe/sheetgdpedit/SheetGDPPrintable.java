package com.gmail.alaerof.loc.mainframe.sheetgdpedit;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.sheet.SheetEntity;

/**
 * реализация вывода странички с картинкой ГДП на принтере
 * @author Helen Yrofeeva
 * 
 */
public class SheetGDPPrintable implements Printable {
    public static final int PAPER_FIELDS = 50;
    private SheetEntity sheetEntity;

    public SheetGDPPrintable(SheetEntity sheetEntity) {
        this.sheetEntity = sheetEntity;
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex > 0) {
            return NO_SUCH_PAGE;
        }
        Graphics2D gGDP = (Graphics2D) graphics;
        gGDP.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        int headGap = 100;
        //int bottomGap = 100;

        double wPage = pageFormat.getWidth();
        double hPage = pageFormat.getHeight();

        SheetConfig cf = sheetEntity.getSheetConfig();

        int hGDP = cf.hGDP;
        int hHour = cf.hHour;
        int wLeft = cf.wLeft;
        int wGDP = cf.wGDP;
        // а вот XY нужно пересчитать в соответствии с отступами
        sheetEntity.calcXY(hHour, wLeft);
        try {

            double wIm = wLeft + wGDP + PAPER_FIELDS*2;
            double hIm = hHour + hGDP + headGap;
            
            double coeff = Math.min(wPage/wIm, hPage/hIm);
            
            gGDP.scale(coeff, coeff);

            // отрисовка сетки, названий станций и часов
            sheetEntity.drawGDPGrid(gGDP, hHour, wLeft, Color.white);
            sheetEntity.drawLeft(gGDP, hHour, 0, Color.white);
            sheetEntity.drawHour(gGDP, 0, wLeft, Color.white);
            // sheetEntity.drawLeftHead(gGDP, 0, 0, Color.white);
            // отрисовка поездов
            sheetEntity.drawGDPImage(gGDP, wLeft, hHour);
            
        } finally {
            // и вернуть их обратно !!!
            sheetEntity.calcXY(0, 0);
        }

        return 0;
    }

}
