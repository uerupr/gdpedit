package com.gmail.alaerof.loc.entity.train;

/**
 * параметры отображения, общие для всех ниток (отступы)
 *
 */
public class TrainThreadGeneralParams {
    /** конец левой части по х в совмещенном ГДП */
    public int xLRightJoint;
    /** отступ сверху в совмещенном ГДП */
    public int yIndTop;
    
 }
