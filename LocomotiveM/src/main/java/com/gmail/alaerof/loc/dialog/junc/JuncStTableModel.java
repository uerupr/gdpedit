package com.gmail.alaerof.loc.dialog.junc;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.dao.dto.JuncSt;

public class JuncStTableModel  extends AbstractTableModel{
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "№", "Код ЕСР", "Название" };
    private List<JuncSt> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (list != null) {
            JuncSt el = list.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return el.num;
            case 1:
                return el.codeESR;
            case 2:
                return el.name;
            }
        }
        return null;
    }

    @Override
    public java.lang.Class<?> getColumnClass(int col) {
        switch (col) {
        case 0:
            return Integer.class;
        case 1:
        case 2:
            return String.class;
        }
        return Object.class;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null) {
            JuncSt el = list.get(row);
            if (el != null) {
                switch (col) {
                case 0:
                    el.num = (Integer)aValue;
                    break;
                default:
                    break;
                }
            }
        }
    }

    public List<JuncSt> getList() {
        return list;
    }

    public void setList(List<JuncSt> list) {
        this.list = list;
    }

}
