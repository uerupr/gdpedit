package com.gmail.alaerof.loc.dialog.distr;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.gmail.alaerof.loc.dao.dto.Junc;

public class DistrJuncCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener{
    private static final long serialVersionUID = 1L;
    private Junc junc;
    private List<Junc> listJunc;

    public DistrJuncCellEditor(List<Junc> listJunc) {
        this.listJunc = listJunc;
    }

    @Override
    public Object getCellEditorValue() {
        return this.junc;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
            int column) {
        if (value instanceof Junc) {
            this.junc = (Junc) value;
        }

        JComboBox<Junc> comboJunc = new JComboBox<>();

        for (Junc aJunc : listJunc) {
            comboJunc.addItem(aJunc);
        }

        comboJunc.setSelectedItem(junc);
        comboJunc.addActionListener(this);

        if (isSelected) {
            comboJunc.setBackground(table.getSelectionBackground());
        } else {
            comboJunc.setBackground(table.getSelectionForeground());
        }

        return comboJunc;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        @SuppressWarnings("unchecked")
        JComboBox<Junc> comboJunc = (JComboBox<Junc>) event.getSource();
        this.junc = (Junc) comboJunc.getSelectedItem();
    }

    public void setListJunc(List<Junc> listJunc) {
        this.listJunc = listJunc;
    }
}
