package com.gmail.alaerof.loc.dao.dto.train;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.util.TimeConverter;

public class TrainStation {
    private static Logger logger;

    public TrainStation() {
        logger = LogManager.getLogger(this.getClass());
    }

    // DB
    public long trainID;
    public int stationID;
    private Integer timeOn; // врем¤ прибыти¤ в минутах (может быть null!!!)
    private Integer timeOff; // врем¤ отправлени¤ в минутах (может быть null!!!)
    public int texStop;
    // add
    public String stationName = "";

    public Integer getTimeOn() {
        return timeOn;
    }

    public String getTimeOnStr() {
        if (timeOn == null) {
            return "";
        } else {
            String tm = TimeConverter.minutesToTime(timeOn);
            return tm;
        }
    }

    public void setTimeOn(Integer timeOn) {
        TimeConverter.checkTimeArgument(timeOn);
        this.timeOn = timeOn;
    }

    public void setTimeOn(String timeOn) {
        int x = TimeConverter.timeToMinutes(timeOn);
        if (x == -1) {
            this.timeOn = null;
        } else {
            setTimeOn(x);
        }
    }

    public Integer getTimeOff() {
        return timeOff;
    }

    public String getTimeOffStr() {
        if (timeOff == null) {
            return "";
        } else {
            String tm = TimeConverter.minutesToTime(timeOff);
            return tm;
        }
    }

    public void setTimeOff(Integer timeOff) {
        TimeConverter.checkTimeArgument(timeOff);
        this.timeOff = timeOff;
    }

    public void setTimeOff(String timeOff) {
        int x = TimeConverter.timeToMinutes(timeOff);
        if (x == -1) {
            this.timeOff = null;
        } else {
            setTimeOff(x);
        }
    }

    public int getTStop() {
        if (timeOn != null && timeOff != null) {
            return TimeConverter.minutesBetween(timeOn, timeOff);
        }
        return 0;
    }

    /**
     * возвращает время прибытия. если времени прибытия нет, то возвращает время
     * отправления (обычно, для первой станции).
     */
    public int getTimeOnChecked() {
        if (timeOn != null) {
            return timeOn;
        }
        if (timeOff != null) {
            return timeOff;
        }
        logger.error("одновременно отсуствуют времена прибытия и отправления: trainID="
                + trainID + ", stationID=" + stationID);
        return -1;
    }

    /**
     * возвращает время отправления. если времени отправления нет, то возвращает
     * время прибытия (обычно, для последней станции).
     */
    public int getTimeOffChecked() {
        if (timeOff != null) {
            return timeOff;
        }
        if (timeOn != null) {
            return timeOn;
        }
        logger.error("одновременно отсуствуют времена прибытия и отправления: trainID="
                + trainID + ", stationID=" + stationID);
        return -1;
    }

    @Override
    public String toString() {
        return "TrainStation [timeOn=" + timeOn + ", timeOff=" + timeOff + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + stationID;
        result = prime * result + (int) (trainID ^ (trainID >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TrainStation other = (TrainStation) obj;
        if (stationID != other.stationID)
            return false;
        if (trainID != other.trainID)
            return false;
        return true;
    }

}
