package com.gmail.alaerof.loc.entity;

import java.util.ArrayList;
import java.util.List;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.DistrTrainDAO;
import com.gmail.alaerof.loc.entity.train.TrainThread;
/**
 * Класс бизнесс-объекта (сущности) "ГДП ж.д. участка".
 * Содержит:
 * - данные по участку (distr);
 * - список поездов (TrainThread).
 * @author Helen Yrofeeva
 *
 */
public class DistrGDP {
    private Distr distr;
    private List<TrainThread> listTrain = new ArrayList<>();

    /**
     * тут загрузки ГДП из БД нету
     * @param distr
     */
    public DistrGDP(Distr distr) {
        this.distr = distr;
    }

    public Distr getDistr() {
        return distr;
    }

    public void setDistr(Distr distr) {
        this.distr = distr;
    }

    public List<TrainThread> getListTrain() {
        return listTrain;
    }

    public void setListTrain(List<TrainThread> listTrain) {
        if (listTrain != null)
            this.listTrain = listTrain;
    }

    /**
     * метод производит загрузку ГДП из БД
     * @param sheetDistrNum
     */
    public void loadGDPFromDB(int sheetDistrNum) {
        if (distr != null) {
            DistrTrainDAO dtDAO = LocDAOFactory.getInstance().getDistrTrainDAO();
            // тут загружаются чисто данные из БД
            listTrain = dtDAO.loadGDP(distr.distrID, new TrainThread());
        }
    }

}
