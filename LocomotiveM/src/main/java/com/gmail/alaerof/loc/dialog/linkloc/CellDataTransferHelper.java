package com.gmail.alaerof.loc.dialog.linkloc;

import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.TransferHandler;
import javax.swing.table.TableModel;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.SeriesLoc;
import com.gmail.alaerof.loc.entity.linktrain.LinkTrainEntity;
import com.gmail.alaerof.loc.entity.train.TrainThread;

public class CellDataTransferHelper extends TransferHandler {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(CellDataTransferHelper.class);
    private LinkLocDialog dialog;

    public CellDataTransferHelper(LinkLocDialog dialog) {
        this.dialog = dialog;
    }

    @Override
    public int getSourceActions(JComponent c) {
        return COPY_OR_MOVE;
    }

    @Override
    protected Transferable createTransferable(JComponent source) {
        // Create the transferable
        // Because I'm hacking a little, I've included the source table...
        JTable table = (JTable) source;
        return new CellDataTransferable(new CellData(table));
    }

    @Override
    protected void exportDone(JComponent source, Transferable data, int action) {
    }

    @Override
    public boolean canImport(TransferSupport support) {
        // Reject the import by default...
        boolean canImport = false;
        // Can only import into another JTable
        Component comp = support.getComponent();
        if (comp instanceof JTable) {
            JTable table = (JTable) comp;
            try {
                // Get the Transferable, we need to check the constraints
                Transferable t = support.getTransferable();
                CellData cd = (CellData) t.getTransferData(CellDataTransferable.CELL_DATA_FLAVOR);
                // Make sure we're not dropping onto ourselves...
                if (cd.getTable() != table) {
                    canImport = true;
                }
            } catch (UnsupportedFlavorException | IOException ex) {
                ex.printStackTrace();
                logger.error(ex.toString(), ex);
            }
        }
        return canImport;
    }

    private void setLinkTrain(TrainThread trArr, TrainThread trDep) {
        Junc junc = dialog.getSelectedJunc();
        SeriesLoc loc = dialog.getSelectedLoc();
        List<LinkTrainEntity> list = dialog.getListLinkTrain();
        LinkLocUtil.setLinkTrainEntity(list, trArr, trDep, junc, loc);
    }

    @Override
    public boolean importData(TransferSupport support) {
        // Import failed for some reason...
        boolean imported = false;
        // Only import into JTables...
        Component comp = support.getComponent();
        if (comp instanceof JTable) {
            JTable target = (JTable) comp;
            // Need to know where we are importing to...
            DropLocation dl = support.getDropLocation();
            Point dp = dl.getDropPoint();
            int targetRow = target.rowAtPoint(dp);
            try {
                // Get the Transferable at the heart of it all
                Transferable t = support.getTransferable();
                CellData cd = (CellData) t.getTransferData(CellDataTransferable.CELL_DATA_FLAVOR);
                if (cd.getTable() != target) {
                    JTable source = cd.getTable();
                    int sourceRow = source.getSelectedRow();

                    TrainThread trArr = null;
                    TrainThread trDep = null;
                    TableModel targetModel = target.getModel();
                    TableModel sourceModel = source.getModel();
                    if (targetModel instanceof ListArrTrainTableModel) {
                        List<TrainThread> listArr = ((ListArrTrainTableModel)targetModel).getList();
                        List<TrainThread> listDep = ((ListDepTrainTableModel)sourceModel).getList();
                        trArr = listArr.get(targetRow);
                        trDep = listDep.get(sourceRow);
                    } else {
                        List<TrainThread> listArr = ((ListArrTrainTableModel)sourceModel).getList();
                        List<TrainThread> listDep = ((ListDepTrainTableModel)targetModel).getList();
                        trArr = listArr.get(sourceRow);
                        trDep = listDep.get(targetRow);
                    }
                    setLinkTrain(trArr, trDep);
                    imported = true;
                    // обновить отображение 
                    target.revalidate();
                    target.repaint();
                    source.revalidate();
                    source.repaint();
                }
            } catch (UnsupportedFlavorException | IOException ex) {
                ex.printStackTrace();
            }

        }
        return imported;
    }

}
