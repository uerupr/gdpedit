package com.gmail.alaerof.loc.application;

import java.awt.Cursor;
import java.awt.EventQueue;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

public class LinkingLocMain {
    public static LinkingLoc mainFrame;
    public static Cursor waitCursor = new Cursor(Cursor.WAIT_CURSOR);
    public static Cursor defCursor = new Cursor(Cursor.DEFAULT_CURSOR);

    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }

    private static void setLookAndFeel() {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    UIManager.put("swing.boldMetal", Boolean.FALSE);
                    break;
                }
            }
        } catch (Exception e) {
            LogManager.getLogger(LinkingLocMain.class).error(e.toString(), e);
        }
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    setLookAndFeel();
                    mainFrame = new LinkingLoc();
                    mainFrame.setLocationRelativeTo(null);
                    mainFrame.setVisible(true);
                } catch (Exception e) {
                    LogManager.getLogger(LinkingLocMain.class).error(e.toString(), e);
                    JOptionPane.showMessageDialog(null, "Во время выполнения программы произошла фатальная ошибка: \n"
                            + e.toString());
                }
            }
        });
    }

}
