package com.gmail.alaerof.loc.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DataXMLUtil {
    private static Logger logger = LogManager.getLogger(DataXMLUtil.class);

    public static Document getNewDocument() {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            return doc;
        } catch (ParserConfigurationException e) {
            logger.error(e.toString(), e);
        }
        return null;
    }

    public static boolean saveDocument(File file, Document doc) {
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "windows-1251");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file);
            transformer.transform(source, result);
            return true;
        } catch (TransformerConfigurationException e) {
            logger.error(e.toString(), e);
        } catch (TransformerException e) {
            logger.error(e.toString(), e);
        }
        return false;
    }
    /**
     * загружает xml файл в объект Document
     * @param file файл для чтения
     * @return объект Document или null
     */
    public static Document loadDocument(File file) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document document = null;
        try {
            db = dbf.newDocumentBuilder();
            if (file.exists()) {
                document = db.parse(file);
            }
        } catch (ParserConfigurationException e) {
            logger.error(e.toString(), e);
        } catch (SAXException e) {
            logger.error(e.toString(), e);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        return document;
    }

    public static Document loadDocument(String fileName) {
        return loadDocument(new File(fileName));
    }

}
