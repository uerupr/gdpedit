package com.gmail.alaerof.loc.mainframe;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

public class StatusBar extends JLabel {
    private static final long serialVersionUID = 1L;

    /** Creates a new instance of StatusBar */
    public StatusBar() {
        super();
        super.setPreferredSize(new Dimension(100, 19));
        setMessage("Ready");
        // this.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        // this.setBorder(BorderFactory.createEtchedBorder());
        this.setBorder(BorderFactory.createLineBorder(Color.GRAY));

    }

    public void setMessage(String message) {
        setText(" " + message);
        this.repaint();
    }

}
