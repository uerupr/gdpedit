package com.gmail.alaerof.loc.dialog.distrturn;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.DistrTurn;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;

public class DistrTurnAEDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(DistrTurnAEDialog.class);
    private DistrTurnAEPanel panel;
    private DistrTurnPanel invoker;
    private DistrTurn distrTurn;

    public DistrTurnAEDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    public DistrTurnAEDialog() {
        initContent();
    }

    private void initContent() {
        setBounds(0, 0, 900, 400);
        setLocationRelativeTo(null);
        try {
            BufferedImage buffImage = ImageIO.read(new File("pic//LL1.GIF"));
            setIconImage(buffImage);
        } catch (IOException e) {
            logger.error("создание иконки", e);
        }
        getContentPane().setLayout(new BorderLayout());
        panel = new DistrTurnAEPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog dialog = this;
            {
                JButton button = new JButton("Сохранить");
                buttonPane.add(button);
                getRootPane().setDefaultButton(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // сохраняем значение участка в БД
                        if (distrTurn.isFieldsFull()) {
                            if (distrTurn.distrTurnID > 0) {
                                LocDAOFactory.getInstance().getDistrTurnDAO().saveDistrTurnHead(distrTurn);
                            } else {
                                LocDAOFactory.getInstance().getDistrTurnDAO().addDistrTurn(distrTurn);
                            }
                            LocDAOFactory.getInstance().getSpanDAO()
                                    .saveDistrTurnSpan(distrTurn, panel.getTurnDistrSpan());
                            // обновляем значение списка
                            invoker.fillListDistrTurn();
                        } else {
                            JOptionPane.showMessageDialog(null, "Должны быть выбраны: НОД, Ж.д. участок, начальная и конечная станции!!!");
                        }
                        // dialog.setVisible(false);
                    }
                });
            }
            {
                JButton button = new JButton("Отмена");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // обновляем значение списка
                        invoker.fillListDistrTurn();
                        dialog.setVisible(false);
                    }
                });
            }
        }
    }

    public void loadData(DistrTurnPanel invoker, DistrTurn distrTurn) {
        this.invoker = invoker;
        this.distrTurn = distrTurn;
        panel.loadData(distrTurn);
    }
}
