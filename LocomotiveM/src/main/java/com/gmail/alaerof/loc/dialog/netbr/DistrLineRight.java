package com.gmail.alaerof.loc.dialog.netbr;

import java.awt.Polygon;

public class DistrLineRight extends DistrLine{
    private static final long serialVersionUID = 1L;

    @Override
    protected Polygon getPolygon() {
        int npoints = 6;
        int x[] = new int[npoints];
        int y[] = new int[npoints];
        int w = getSize().width;  // ширина 
        int h = getSize().height; // высота 
        double gg = Math.max(1, Math.sqrt(w * w + h * h));// гипотенуза
        int pw = lineWidth/2 + ADD_WIDTH;
        int pwx = (int)(pw*h/gg);
        int pwy = (int)(pw*w/gg);
        x[0] = 0; // начало оси
        y[0] = h; // начало оси

        x[1] = x[0] - pwx;
        y[1] = y[0] - pwy;

        x[5] = x[0] + pwx;
        y[5] = y[0] + pwy;

        x[3] = x[0] + w;
        y[3] = 0;
        
        x[2] = x[3] - pwx;
        y[2] = y[3] - pwy;

        x[4] = x[3] + pwx;
        y[4] = y[3] + pwy;

        return new Polygon(x, y, npoints);

    }


}
