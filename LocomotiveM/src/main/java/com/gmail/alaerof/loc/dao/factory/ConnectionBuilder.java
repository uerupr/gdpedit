package com.gmail.alaerof.loc.dao.factory;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.gmail.alaerof.loc.util.FileProperties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hsqldb.Server;


public class ConnectionBuilder {
    private static Logger logger = LogManager.getLogger(ConnectionBuilder.class);
    private static Server server;

    public static void startServer() {
        server = new Server();
        server.setTrace(true);
        try {
            server.setLogWriter(new PrintWriter(new FileWriter("database/db.log")));
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        server.setAddress("localhost");
        server.setDatabaseName(0, "testdb");
        server.setDatabasePath(0, "file:database/hsqldb/hemrajdb");
        //server.setPort(1234);
        server.start();
    }

    public static void stopServer() {
        if (server != null) {
            if (server.getState() == 1)
                server.shutdown();
        }
    }

    private static void registerDriver(String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            logger.error(e.toString(), e);
        }
    }

    public static Connection getConnection(String fileName) throws SQLException {
        Properties prop = FileProperties.getInstance().getProperties(fileName);
        String driver = prop.getProperty("driver");
        String url = prop.getProperty("url");
        String user = prop.getProperty("user");
        String pass = prop.getProperty("pass");
        String charSet = prop.getProperty("charSet");
        Properties connInfo = new Properties();
        connInfo.put("user", user);
        connInfo.put("pass", pass);
        if (charSet != null) {
            connInfo.put("charSet", charSet);
        }
        // необязательно, как оказалось
        registerDriver(driver);
        return DriverManager.getConnection(url, connInfo);
    }

    public static Connection getConnectionServer(String fileName) throws SQLException {
        Properties prop = FileProperties.getInstance().getProperties(fileName);
        String driver = prop.getProperty("driver");
        String url = prop.getProperty("url");

        logger.debug(driver);
        logger.debug(url);
        return DriverManager.getConnection(url);
    }

    public static boolean inprocess(String fileName) {
        Properties prop = FileProperties.getInstance().getProperties(fileName);
        String ins = prop.getProperty("in-process", "0");
        int in = Integer.parseInt(ins);
        return in == 1;
    }

    /**
     * необходимость установки соединения с сервером
     *
     * @param fileName
     * @return
     */
    public static boolean isServerGDP(String fileName) {
        Properties prop = FileProperties.getInstance().getProperties(fileName);
        String ins = prop.getProperty("serverGDP", "0");
        int in = Integer.parseInt(ins);
        return in == 1;
    }

}
