package com.gmail.alaerof.loc.dialog.loc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.loc.dao.dto.SeriesLoc;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.SeriesLocDAO;

public class LocDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTable table;
    private LocTableModel tableModel;
    private List<SeriesLoc> locList = new ArrayList<>();
    private List<SeriesLoc> locListDel = new ArrayList<>();

    public LocDialog() {
        initContent();
    }

    public LocDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    public void loadData() {
        LocDAOFactory f = LocDAOFactory.getInstance();
        locList = f.getSeriesLocDAO().loadAllSLoc();
        tableModel.setList(locList);
        table.repaint();
    }

    private void initContent() {
        setBounds(100, 100, 450, 300);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);

        {
            tableModel = new LocTableModel();
            table = new JTable(tableModel);
            JScrollPane pane = new JScrollPane(table);
            table.setFillsViewportHeight(true);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 100, 50 };
            com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(table, colSize);
            pane.setPreferredSize(new Dimension(305, 0));
            contentPanel.add(pane, BorderLayout.CENTER);

            table.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    int row = table.getSelectedRow();
                    if (row > -1) {
                        SeriesLoc el = locList.get(row);
                        if (e.getClickCount() == 2) {
                            el.electr = !el.electr;
                            table.repaint();
                        }
                    }
                }
            });

        }

        {
            JPanel actionPanel = new JPanel();
            actionPanel.setLayout(new FlowLayout());
            contentPanel.add(actionPanel, BorderLayout.EAST);

            {
                JButton b = new JButton("+");
                actionPanel.add(b);
                b.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        SeriesLoc jn = new SeriesLoc();
                        jn.series = "-";
                        jn.electr = false;
                        if (!locList.contains(jn))
                            locList.add(jn);
                        table.repaint();
                    }
                });
            }
            {
                JButton b = new JButton("-");
                actionPanel.add(b);
                b.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        int index = table.getSelectedRow();
                        if (index > -1) {
                            SeriesLoc jn = locList.remove(index);
                            table.repaint();
                            locListDel.add(jn);
                        }
                    }
                });
            }
        }

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog d = this;
            {
                JButton button = new JButton("Применить");
                buttonPane.add(button);
                getRootPane().setDefaultButton(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        SeriesLocDAO slDAO = LocDAOFactory.getInstance().getSeriesLocDAO();
                        slDAO.deleteSeriesLoc(locListDel);
                        for (int i = 0; i < locList.size(); i++) {
                            SeriesLoc loc = locList.get(i);
                            if (loc.seriesLocID > 0) {
                                slDAO.updateSeriesLoc(loc);
                            } else {
                                slDAO.addSeriesLoc(loc);
                            }
                        }
                    }
                });
            }
            {
                JButton button = new JButton("Отмена");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        d.setVisible(false);
                    }
                });
                
            }
        }
    }
}
