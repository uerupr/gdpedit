package com.gmail.alaerof.loc.dialog.category;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.loc.dao.dto.Category;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;

public class CategoryDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private CategoryTableModel tableModel = new CategoryTableModel();
    private JTable table = new JTable(tableModel);

    public CategoryDialog() {
        initContent();
    }

    public CategoryDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private void initContent() {
        setBounds(0, 0, 920, 400);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        {
            JScrollPane pane = new JScrollPane(table);
            table.setFillsViewportHeight(true);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 300, 50, 50, 100 };
            com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(table, colSize);
            contentPanel.add(pane, BorderLayout.CENTER);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }
    }

    public void loadData() {
        List<Category> list = LocDAOFactory.getInstance().getDirectionDAO().loadAllCategory();
        Collections.sort(list);
        tableModel.setList(list);
    }

}
