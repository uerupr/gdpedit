package com.gmail.alaerof.loc.dialog.loc;

import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.dao.dto.SeriesLoc;

public class LocTableModel  extends AbstractTableModel{
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "Серия", "Электр." };
    private List<SeriesLoc> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (list != null) {
            SeriesLoc el = list.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return el.series;
            case 1:
                return el.electr?"*":"";
            }
        }
        return null;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return col==0;
    }
    
    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null) {
            SeriesLoc el = list.get(row);
            if (el != null) {
                switch (col) {
                case 0:
                    SeriesLoc test = new SeriesLoc();
                    test.series = ((String) aValue).trim();
                    if ("".equals(test.series)) {
                        JOptionPane.showMessageDialog(null, "Серия должна быть!", "Предупреждение",
                                JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                    if (!list.contains(test))
                        el.series = test.series;
                    else
                        JOptionPane.showMessageDialog(null, "Серии должны быть разными!", "Предупреждение",
                                JOptionPane.WARNING_MESSAGE);
                    break;
                default:
                    break;
                }
            }
        }
    }

    public List<SeriesLoc> getList() {
        return list;
    }

    public void setList(List<SeriesLoc> list) {
        this.list = list;
    }

}
