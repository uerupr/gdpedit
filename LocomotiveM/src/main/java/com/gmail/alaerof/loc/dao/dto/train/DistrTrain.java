package com.gmail.alaerof.loc.dao.dto.train;

import java.util.List;

public class DistrTrain {
    public long trainID;
    public int distrID;
    public int categoryID;
    public int codeTR;
    public String nameTR;
    public int colorTR;
    public boolean hidden;
    public String ainscr;
    public boolean showAI;
    public int lineStyle;
    public int widthTR;
    // ---------
    private TrainType typeTR; // type
    // показатели 
    private List<DirTrainResult> listRes;
    
    public TrainType getTypeTR() {
        return typeTR;
    }

    public void setTypeTR(TrainType typeTR) {
        if (typeTR != null) {
            this.typeTR = typeTR;
        }
    }

    @Override
    public String toString() {
        return "DistrTrain [trainID=" + trainID + ", distrID=" + distrID + ", codeTR=" + codeTR + ", nameTR=" + nameTR
                + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + codeTR;
        result = prime * result + distrID;
        result = prime * result + ((nameTR == null) ? 0 : nameTR.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DistrTrain other = (DistrTrain) obj;
        if (codeTR != other.codeTR)
            return false;
        if (distrID != other.distrID)
            return false;
        if (nameTR == null) {
            if (other.nameTR != null)
                return false;
        } else if (!nameTR.equals(other.nameTR))
            return false;
        return true;
    }

    public List<DirTrainResult> getListRes() {
        return listRes;
    }

    public void setListRes(List<DirTrainResult> listRes) {
        this.listRes = listRes;
    }

}
