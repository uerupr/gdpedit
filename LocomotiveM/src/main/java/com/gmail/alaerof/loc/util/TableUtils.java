package com.gmail.alaerof.loc.util;

import java.text.DecimalFormat;

import javax.swing.JTable;

public class TableUtils {
    public static void setTableColumnSize(JTable table, int[] colSize) {
        int colCount = table.getColumnModel().getColumnCount();
        for (int i = 0; i < colSize.length; i++) {
            if (i < colCount) {
                table.getColumnModel().getColumn(i).setPreferredWidth((int) (colSize[i] * 1.5));
                table.getColumnModel().getColumn(i).setMinWidth(colSize[i]);
            }
        }
    }

    /**
     * переводит число в строку с ограничением после запятой
     * @param value число
     * @param dec количество цифр после запятой
     * @return отформатированное число в виде строки
     */
    public static String dfNumber(Double value, int dec) {
        StringBuilder pattern = new StringBuilder("###.");
        for (int i = 0; i < dec; i++) {
            pattern.append("#");
        }
        DecimalFormat myFormatter = new DecimalFormat(pattern.toString());
        return myFormatter.format(value);
    }

}
