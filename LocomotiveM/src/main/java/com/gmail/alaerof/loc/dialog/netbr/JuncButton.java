package com.gmail.alaerof.loc.dialog.netbr;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

//import javax.swing.border.LineBorder;

public class JuncButton extends JButton {
    private static final long serialVersionUID = 1L;
    private static Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
    private String nameSt = "";
    private int juncID = 0;
    private Color colorBorder = Color.DARK_GRAY;
    private Color color1 = Color.WHITE;
    private Color color2 = Color.DARK_GRAY;
    private int dim = 20;

    public enum JuncButtonType {
        depot, recycling, point, noRB
    }

    public JuncButton() {
        this.setCursor(handCursor);
        setContentAreaFilled(false);
        addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                int x = getLocation().x + getWidth() / 2;
                int y = getLocation().y + getHeight() / 2;
                setToolTipText(x + ", " + y);
                System.out.println(nameSt + "\t" + x + ", " + y);

            }
        });

    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int h = this.getHeight();
        int w = this.getWidth();
        Color bord = colorBorder;
        Color grad = color2;
        if (getModel().isArmed()) {
            bord = Color.lightGray;
            grad = bord;
        }
        g2.setColor(bord);
        g2.fillOval(0, 0, w, h);

        GradientPaint gradient = new GradientPaint(0, 0, color1, w, w, grad);
        g2.setPaint(gradient);
        g2.fillOval(3, 3, w - 6, h - 6);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        super.paintComponent(g);
    }

    public String getNameSt() {
        return nameSt;
    }

    public void setNameSt(String nameSt) {
        this.nameSt = nameSt;
        this.setToolTipText(nameSt);
    }

    public int getJuncID() {
        return juncID;
    }

    public void setJuncID(int juncID) {
        this.juncID = juncID;
    }

    public void setType(JuncButtonType juncButtonType) {
        switch (juncButtonType) {
        case depot:
            colorBorder = new Color(0x483d86);
            color1 = new Color(0x6495ed);
            color2 = colorBorder;
            dim = 28;
            break;
        case recycling:
            colorBorder = new Color(0x9acd32);
            color1 = new Color(0x6495ed);
            color2 = new Color(0x483d86);
            dim = 28;
            break;
        case point:
            colorBorder = new Color(0x556b2f);
            color1 = new Color(0x9acd32);
            color2 = colorBorder;
            break;
        case noRB:
            colorBorder = new Color(0xa52a2a);//new Color(0xd2691e);//Color.gray;// new Color(0xa9a9a9);
            color1 = Color.white;
            color2 = colorBorder;
            break;
        }
    }

    public int getDim() {
        return dim;
    }

}
