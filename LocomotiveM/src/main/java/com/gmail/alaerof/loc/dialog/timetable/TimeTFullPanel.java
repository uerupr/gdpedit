package com.gmail.alaerof.loc.dialog.timetable;

import java.util.List;

import com.gmail.alaerof.loc.dao.dto.train.TrainStation;

public class TimeTFullPanel extends TimeTPanel {
    private static final long serialVersionUID = 1L;

    @Override
    protected void clearTrainTime() {
        // System.out.println("TimeTFullPanel");
    }

    @Override
    protected void fillTrainTime() {
        // System.out.println("TimeTFullPanel");
        laTrain.setText("-");
        listTimeSt.clear();

        List<?> ll = tableModel.getList();
        if (ll == null) {
            tableModel.setList(listTimeSt);
        }
        if (selectedTrain != null) {
            laTrain.setText("" + selectedTrain.getDistrTrain().codeTR + "(" + selectedTrain.getDistrTrain().nameTR
                    + ")");
            List<TrainStation> listTrSt = selectedTrain.getTrainSt();
            for (int i = 0; i < listTrSt.size(); i++) {
                int index = i;
                if (selectedTrain.isEven()) {
                    index = listTrSt.size() - i - 1;
                }
                TrainStation trSt = listTrSt.get(index);

                TimeTStation tst = new TimeTStation();
                tst.setStationName(trSt.stationName.trim());
                tst.setStationID(trSt.stationID);
                tst.setDistrID(selectedTrain.getDistrID());

                String tmOn = trSt.getTimeOnStr();
                String tmOff = trSt.getTimeOffStr();
                if (tmOn.equals(tmOff)) {
                    tmOn = "";
                }
                String tstop = "";
                int min = trSt.getTStop();
                if (min != 0) {
                    tstop = "" + min;
                }
                String texstop = "";
                if (trSt.texStop > 0) {
                    texstop = "" + trSt.texStop;
                }
                tst.setTimeOn(tmOn);
                tst.setTimeOff(tmOff);
                tst.settStop(tstop);
                tst.setTexStop(texstop);
                listTimeSt.add(tst);
            }
        }
    }
}
