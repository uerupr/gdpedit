package com.gmail.alaerof.loc.dao.dto;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class RangeTR implements Comparable<RangeTR>{
    public static final String regex = "([\\d]{1,10})-([\\d]{1,10})\\s*(,\\s*([\\d]{1,10})-([\\d]{1,10})\\s*)*,?";
    protected static Logger logger = LogManager.getLogger(RangeTR.class);

    public static class Range {
        public int trMin;
        public int trMax;
    }
    
    public int rangeTRID;
    public String name;
    private String rangeValue = "";

    public boolean isCorrect(String rangeValue) {
        if (rangeValue != null) {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(rangeValue.trim());
            return m.matches();
        }
        return false;
    }

    public String getRangeValue() {
        return rangeValue;
    }

    public void setRangeValue(String rangeValue) throws ParseException {
        if (isCorrect(rangeValue)){
            this.rangeValue = rangeValue;
        }else{
            throw new ParseException(rangeValue, 0);
        }
    }

    public List<Range> getListRange() {
        List<Range> list = new ArrayList<>();
        String[] listR = rangeValue.split(",");
        for (String rn : listR) {
            String[] r = rn.split("-");
            Range range = new Range();
            list.add(range);
            try {
                range.trMin = Integer.parseInt(r[0].trim());
                range.trMax = Integer.parseInt(r[1].trim());
            } catch (NumberFormatException e) {
                logger.error(e.toString(), e);
            }
        }
        return list;
    }

    @Override
    public String toString() {
        return rangeValue;
    }

    @Override
    public int compareTo(RangeTR o) {
        return this.name.compareToIgnoreCase(o.name);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + rangeTRID;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RangeTR other = (RangeTR) obj;
        if (rangeTRID != other.rangeTRID)
            return false;
        return true;
    }
}
