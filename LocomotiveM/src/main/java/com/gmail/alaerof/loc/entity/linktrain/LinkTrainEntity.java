package com.gmail.alaerof.loc.entity.linktrain;

import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.entity.train.TrainThread;
import com.gmail.alaerof.loc.util.TimeConverter;

public class LinkTrainEntity {
    private LinkTrain linkTrain;
    private TrainThread trainOn;
    private TrainThread trainOff;

    public LinkTrain getLinkTrain() {
        return linkTrain;
    }

    public void setLinkTrain(LinkTrain linkTrain) {
        this.linkTrain = linkTrain;
    }

    public TrainThread getTrainOn() {
        return trainOn;
    }

    public void setTrainOn(TrainThread trainOn) {
        this.trainOn = trainOn;
        if (trainOn != null) {
            linkTrain.distrIDon = trainOn.getDistrID();
            linkTrain.codeTRon = trainOn.getCodeINT();
            linkTrain.setNameTRon(trainOn.getDistrTrain().nameTR);
        }
    }

    /**
     * из депо
     */
    public void setDepotOn() {
        this.trainOn = null;
        linkTrain.distrIDon = -1;
        linkTrain.codeTRon = 0;
        linkTrain.nameTRon = "";
    }

    public TrainThread getTrainOff() {
        return trainOff;
    }

    public void setTrainOff(TrainThread trainOff) {
        this.trainOff = trainOff;
        if (trainOff != null) {
            linkTrain.distrIDoff = trainOff.getDistrID();
            linkTrain.codeTRoff = trainOff.getCodeINT();
            linkTrain.setNameTRoff(trainOff.getDistrTrain().nameTR);
        }
    }

    /**
     * в депо
     */
    public void setDepotOff() {
        this.trainOff = null;
        linkTrain.distrIDoff = -1;
        linkTrain.codeTRoff = 0;
        linkTrain.nameTRoff = "";
    }

    @Override
    public String toString() {
        return "LinkTrainEntity [linkTrain=" + linkTrain + ", trainOn=" + trainOn + ", trainOff=" + trainOff
                + "]";
    }

    /**
     * 
     * @param junc
     */
    public void calcTime(Junc junc) {
        // одновременно не должно быть  distrIDon == -1 и distrIDoff == -1
        if (linkTrain.distrIDon == -1) {
            linkTrain.tm = junc.getJuncTime().depart;
            return;
        }
        if (linkTrain.distrIDoff == -1) {
            linkTrain.tm = junc.getJuncTime().arrive;
            return;
        }
        if (trainOn != null && trainOff != null) {
            int tOn = trainOn.getLastSt().getTimeOnChecked();
            int tOff = trainOff.getFirstSt().getTimeOffChecked();
            linkTrain.tm = TimeConverter.minutesBetween(tOn, tOff);
            return;
        }
        linkTrain.tm = 0;
    }

    /**
     * конструирует строковое представление увязки по поезду отправления
     * @return
     */
    public String getDescriptionOff() {
        String value = "";
        TrainThread trainLink = null;
        value = getLinkTrain().locName;
        trainLink = getTrainOff();
        if (trainLink != null) {
            value = trainLink.getCode() + " (" + trainLink.getDistrTrain().nameTR + ") " + value;
        } else {
            if (getLinkTrain().distrIDoff > 0) {
                String nameD = getLinkTrain().distrNameOff;
                value = getLinkTrain().codeTRoff + " " + value + " " + nameD;
            } else {
                value = "В депо " + value;
            }
        }
        return value;
    }

    /**
     * конструирует строковое представление увязки по поезду прибытия
     * @return
     */
    public String getDescriptionOn() {
        String value = "";
        TrainThread trainLink = null;
        value = getLinkTrain().locName;
        trainLink = getTrainOn();
        if (trainLink != null) {
            value = trainLink.getCode() + " (" + trainLink.getDistrTrain().nameTR + ") " + value;
        } else {
            if (getLinkTrain().distrIDon > 0) {
                String nameD = getLinkTrain().distrNameOn;
                value = getLinkTrain().codeTRon + " " + value + " " + nameD;
            } else {
                value = "ИЗ депо " + value;
            }
        }
        return value;
    }

    public int getDiffTime(Junc junc) {
        int diff = 0;
        if (trainOn != null && trainOff != null) {
            diff = getLinkTrain().tm - junc.getJuncTime().equipment;
        } else {
            if (linkTrain.distrIDoff == -1) {
                diff = getLinkTrain().tm - junc.getJuncTime().arrive;
            }
            if (linkTrain.distrIDon == -1) {
                diff = getLinkTrain().tm - junc.getJuncTime().depart;
            }
        }
        return diff;
    }

    public boolean fromDepot() {
        return linkTrain.distrIDon == -1;
    }

    public boolean toDepot() {
        return linkTrain.distrIDoff == -1;
    }

}
