package com.gmail.alaerof.loc.dialog.sheet;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;

public class ListSDSTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "Номер", "Название", "Расст.", "Скр.", "Псевдоним", "Показ. псевдоним" };
    private List<SheetDistrSt> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (list != null) {
            SheetDistrSt el = list.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return el.num;
            case 1:
                return el.stationName;
            case 2:
                return el.scale;
            case 3:
                return el.hidden ? "*" : "";
            case 4:
                return el.visibleName;
            case 5:
                return el.showVisibleName ? "*" : "";
            }
        }
        return null;
    }

    @Override
    public java.lang.Class<?> getColumnClass(int col) {
        switch (col) {
        case 0:
        case 2:
            return Integer.class;
        case 1:
        case 3:
        case 4:
        case 5:
            return String.class;
        }
        return Object.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 0 || col == 2 || col == 4) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null) {
            SheetDistrSt el = list.get(row);
            if (el != null) {
                switch (col) {
                case 0:
                    el.num = (Integer) aValue;
                    break;
                case 2:
                    el.scale = (Integer) aValue;
                    break;
                case 4:
                    el.visibleName = ((String) aValue).trim();
                    break;
                default:
                    break;
                }
            }
        }
    }

    public List<SheetDistrSt> getList() {
        return list;
    }

    public void setList(List<SheetDistrSt> list) {
        this.list = list;
    }

}
