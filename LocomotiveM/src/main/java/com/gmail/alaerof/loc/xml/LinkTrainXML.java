package com.gmail.alaerof.loc.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.DistrTurn;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.JuncSt;
import com.gmail.alaerof.loc.dao.dto.JuncTime;
import com.gmail.alaerof.loc.dao.dto.SeriesLoc;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.DistrDAO;
import com.gmail.alaerof.loc.dao.impl.SeriesLocDAO;

/**
 * Класс для экспорта/импорта данных приложения увязки:
 * - узлы[junc] (станции[JuncSt+Station], узл.время[JuncTime])
 * - серии локомотивов[SeriesLoc]
 * - учаски обращения [distrturn]
 * - увязки [linktrain]
 * 
 * сохранять все в 1 файл
 * загружать выборочно по аспектам
 * 
 * @author Helen Yrofeeva
 */
public class LinkTrainXML {
    protected static Logger logger = LogManager.getLogger(LinkTrainXML.class);

    /**
     * загружает увязки для узла из файла
     * @param file файл чтения
     * @param juncID ID узла
     * @return список увязок
     */
    /**
     * загружает увязки для узла из файла
     * @param file файл чтения
     * @param juncID ID узла
     * @param listLinkTrain пустой список увязок для заполнения
     * @return признак, что попытка загрузить была
     */
    public boolean loadLinkTrain(File file, int juncID, List<LinkTrain> listLinkTrain) {
        Document doc = DataXMLUtil.loadDocument(file);
        Junc junc = LocDAOFactory.getInstance().getJuncDAO().getJunc(juncID);
        boolean canLoade = false;
        if (doc != null) {
            // проверка на корректность по формату
            NodeList nodeList = doc.getElementsByTagName("linkingloc");
            if (nodeList.getLength() == 1) {
                // проверка на корректность по названию участка
                String name = "";
                nodeList = doc.getElementsByTagName("junc");
                if (nodeList.getLength() == 1) {
                    Element item = (Element) nodeList.item(0);
                    name = item.getAttribute("name").trim();
                }
                if (!name.equals(junc.name.trim())) {
                    int n = JOptionPane.showConfirmDialog(null, "Вы действительно хотите загрузить увязки для узла '" + junc.name
                            + "' из файла, где узел называется '" + name + "' ?", "Предупреждение", JOptionPane.YES_NO_OPTION);
                    canLoade = n == 0;
                } else {
                    canLoade = true;
                }

            } else {
                JOptionPane.showMessageDialog(null, "Ошибка чтения файла " + file.getName() + ", увязки не загружены");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ошибка чтения файла " + file.getName() + ", подробности см. в logs");
        }

        if (canLoade) {
            // проверка наличия всех требуемых элементов
            Map<Integer, Integer> allDistr = new HashMap<>();
            Map<Integer, Integer> allLoc = new HashMap<>();

            List<String> listMess = checkStruct(doc, junc, allDistr, allLoc);

            if (listMess.size() > 0) {
                ListMessageDialog ds = new ListMessageDialog(null, "Предупреждение", true);
                ds.setText(listMess);
                ds.setVisible(true);
                int res = ds.getResult();
                canLoade = res == 1;
            }

            if (canLoade) {
                listLinkTrain.addAll(loadJuncLinkTrain(doc, junc, allDistr, allLoc));
            }
        }
        return canLoade;
    }

    private List<String> checkStruct(Document doc, Junc junc, Map<Integer, Integer> allDistr, Map<Integer, Integer> allLoc) {
        List<String> mess = new ArrayList<>();
        checkAllDistr(doc, allDistr, mess);
        checkAllLoc(doc, allLoc, mess);
        return mess;
    }

    private void checkAllDistr(Document doc, Map<Integer, Integer> allDistr, List<String> mess) {
        NodeList nodeList = doc.getElementsByTagName("linktrain");
        allDistr.put(-1, -1);
        for (int i = 0; i < nodeList.getLength(); i++) {
            try {
                Element item = (Element) nodeList.item(i);
                String xmlNameOn = item.getAttribute("distrNameOn").trim();
                int xmlIDon = Integer.parseInt(item.getAttribute("distrIDon"));
                String xmlNameOff = item.getAttribute("distrNameOff").trim();
                int xmlIDoff = Integer.parseInt(item.getAttribute("distrIDoff"));
                int dbIDon = 0;
                int dbIDoff = 0;

                if (xmlIDon != -1) {
                    dbIDon = searchDistr(xmlIDon, xmlNameOn, mess);
                    allDistr.put(xmlIDon, dbIDon);
                }
                if (xmlIDoff != -1) {
                    dbIDoff = searchDistr(xmlIDoff, xmlNameOff, mess);
                    allDistr.put(xmlIDoff, dbIDoff);
                }
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    private int searchDistr(int xmlID, String xmlName, List<String> mess) {
        int dbID = 0;
        DistrDAO distrDAO = LocDAOFactory.getInstance().getDistrDAO();
        Map<Integer, Distr> mapDistr = distrDAO.getMapAllDistr();
        Distr distr = mapDistr.get(xmlID);
        if (distr == null) {
            mess.add("Участок с ID '" + xmlID + "' не найден в БД ");
            distr = distrDAO.getDistrByName(mapDistr, xmlName);
            if (distr == null) {
                mess.add("Участок с названием '" + xmlName + "' не найден в БД ");
            } else {
                mess.add("но зато найден с таким же названием '" + xmlName + "', это норм " + distr.distrID);
                dbID = distr.distrID;
            }
        } else {
            if (!distr.name.equals(xmlName)) {
                mess.add("Название участка в файле '" + xmlName + "' не совпадает с названием в БД '" + distr.name + "'");
                Distr distr2 = distrDAO.getDistrByName(mapDistr, xmlName);
                if (distr2 == null) {
                    dbID = distr.distrID;
                    mess.add("в БД вообще нет участка с таким названием как в файле, поэтому будет использован участок " + distr.name);
                } else {
                    dbID = distr2.distrID;
                    mess.add("Но в БД найден другой участок с таким же названием, и он будет использован");
                }
            } else {
                dbID = distr.distrID;
            }
        }
        return dbID;
    }

    private void checkAllLoc(Document doc, Map<Integer, Integer> allLoc, List<String> mess) {
        SeriesLocDAO locDAO = LocDAOFactory.getInstance().getSeriesLocDAO();
        List<SeriesLoc> listloc = locDAO.loadAllSLoc();
        NodeList nodeList = doc.getElementsByTagName("seriesloc");
        for (int i = 0; i < nodeList.getLength(); i++) {
            try {
                Element item = (Element) nodeList.item(i);
                String xmlName = item.getAttribute("series").trim();
                int xmlID = Integer.parseInt(item.getAttribute("seriesLocID"));
                int dbID = 0;

                SeriesLoc loc = locDAO.getLocByName(listloc, xmlName);
                if (loc != null) {
                    dbID = loc.seriesLocID;
                } else {
                    mess.add("Серия локомотива '" + xmlName + "' не найдена в БД ");
                }
                allLoc.put(xmlID, dbID);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    private List<LinkTrain> loadJuncLinkTrain(Document doc, Junc junc, Map<Integer, Integer> allDistr, Map<Integer, Integer> allLoc) {
        List<LinkTrain> list = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("linktrain");
        for (int i = 0; i < nodeList.getLength(); i++) {

            Element item = (Element) nodeList.item(i);
            // int juncID = getIntAttr("juncID", item.getAttribute("juncID"));
            int codeTRon = getIntAttr("codeTRon", item.getAttribute("codeTRon"));
            int codeTRoff = getIntAttr("codeTRoff", item.getAttribute("codeTRoff"));
            String nameTRon = item.getAttribute("nameTRon");
            String nameTRoff = item.getAttribute("nameTRoff");
            int lineNumber = getIntAttr("lineNumber", item.getAttribute("lineNumber"));

            int seriesLocIDX = getIntAttr("seriesLocID", item.getAttribute("seriesLocID"));
            int distrIDonX = getIntAttr("distrIDon", item.getAttribute("distrIDon"));
            int distrIDoffX = getIntAttr("distrIDoff", item.getAttribute("distrIDoff"));

            Integer seriesLocIDDB = allLoc.get(seriesLocIDX);
            if (seriesLocIDDB == null) {
                seriesLocIDDB = 0;
                logger.error("серия локомотива не идентифицированна в БД, в файле ID=" + seriesLocIDX);
            }
            Integer distrIDonDB = allDistr.get(distrIDonX);
            Integer distrIDoffDB = allDistr.get(distrIDoffX);
            if (distrIDonDB != null && distrIDoffDB != null) {

                LinkTrain link = new LinkTrain();
                link.juncID = junc.juncID;
                link.codeTRon = codeTRon;
                link.codeTRoff = codeTRoff;
                link.nameTRon = nameTRon;
                link.nameTRoff = nameTRoff;
                link.distrIDon = distrIDonDB;
                link.distrIDoff = distrIDoffDB;
                link.lineNumber = lineNumber;
                link.seriesLocID = seriesLocIDDB;

                list.add(link);
            } else {
                logger.error("НЕ загружена увязка для " + codeTRon + "(" + nameTRon + ") - " + codeTRoff + "(" + nameTRoff + ")");
                if (distrIDonDB == null) {
                    logger.error("участок прибытия не идентифицированн в БД, в файле ID=" + distrIDonX + " название='"
                            + item.getAttribute("distrNameOn") + "'");
                } else {
                    logger.error("участок отправления не идентифицированн в БД, в файле ID=" + distrIDoffX + " название='"
                            + item.getAttribute("distrNameOff") + "'");
                }
            }

        }
        return list;
    }

    private int getIntAttr(String name, String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            logger.error(name + " " + e.toString(), e);
        }
        return 0;
    }

    /**
     * сохраняет в файл:
     * серии локомотивов[SeriesLoc];
     * учаски обращения [distrturn];
     * узел[junc] (станции[JuncSt+Station], узл.время[JuncTime]);
     * увязки узла [linktrain].
     * 
     * @param file файл записи
     * @param juncID код узла
     * @return признак успешной загрузки
     */
    public boolean saveLinkTrain(File file, int juncID) {
        Document doc = DataXMLUtil.getNewDocument();
        Element el = doc.createElement("linkingloc");
        doc.appendChild(el);
        saveJunc(doc, juncID);
        saveLinkTrain(doc, juncID);
        saveSeriesLoc(doc);
        saveDistrTurn(doc);
        return DataXMLUtil.saveDocument(file, doc);
    }

    private void saveLinkTrain(Document doc, int juncID) {
        Element root = doc.getDocumentElement();
        Element all = doc.createElement("listlinktrain");
        root.appendChild(all);
        List<LinkTrain> list = LocDAOFactory.getInstance().getLinkTrainDAO().loadJuncLinkTrain(juncID);
        for (LinkTrain itm : list) {
            Element el = doc.createElement("linktrain");
            all.appendChild(el);
            el.setAttribute("linkTrainID", "" + itm.linkTrainID);
            el.setAttribute("juncID", "" + itm.juncID);
            el.setAttribute("seriesLocID", "" + itm.seriesLocID);
            el.setAttribute("distrIDon", "" + itm.distrIDon);
            el.setAttribute("codeTRon", "" + itm.codeTRon);
            el.setAttribute("distrIDoff", "" + itm.distrIDoff);
            el.setAttribute("codeTRoff", "" + itm.codeTRoff);
            el.setAttribute("nameTRon", "" + itm.nameTRon.trim());
            el.setAttribute("nameTRoff", "" + itm.nameTRoff.trim());
            el.setAttribute("lineNumber", "" + itm.lineNumber);
            el.setAttribute("distrNameOn", "" + itm.distrNameOn.trim());
            el.setAttribute("distrNameOff", "" + itm.distrNameOff.trim());
        }
    }

    private void saveJunc(Document doc, int juncID) {
        Element root = doc.getDocumentElement();
        Element jel = doc.createElement("junc");
        root.appendChild(jel);
        Junc junc = LocDAOFactory.getInstance().getJuncDAO().getJunc(juncID);
        jel.setAttribute("juncID", "" + junc.juncID);
        jel.setAttribute("codeESR", "" + junc.codeESR.trim());
        jel.setAttribute("name", "" + junc.name.trim());
        jel.setAttribute("jType", "" + junc.getJuncType().getInt());
        jel.setAttribute("dirID", "" + junc.dirID);
        jel.setAttribute("lineCount", "" + junc.lineCount);

        Element jtel = doc.createElement("junctime");
        jel.appendChild(jtel);

        JuncTime jt = junc.getJuncTime();
        if (jt != null) {
            jtel.setAttribute("juncID", "" + jt.juncID);
            jtel.setAttribute("arrive", "" + jt.arrive);
            jtel.setAttribute("depart", "" + jt.depart);
            jtel.setAttribute("equipment", "" + jt.equipment);
        }

        Element jlstel = doc.createElement("listjuncst");
        jel.appendChild(jlstel);
        List<JuncSt> list = junc.getListSt();
        if (list != null && list.size() > 0) {
            for (JuncSt st : list) {
                Element jstel = doc.createElement("juncst");
                jlstel.appendChild(jstel);
                jstel.setAttribute("juncID", "" + st.juncID);
                jstel.setAttribute("stationID", "" + st.stationID);
                jstel.setAttribute("num", "" + st.num);
                jstel.setAttribute("codeESR", "" + st.codeESR.trim());
                jstel.setAttribute("name", "" + st.name.trim());
            }
        }
    }

    private void saveDistrTurn(Document doc) {
        Element root = doc.getDocumentElement();
        Element all = doc.createElement("listdistrturn");
        root.appendChild(all);
        List<DistrTurn> list = LocDAOFactory.getInstance().getDistrTurnDAO().loadAllDistrTurn();
        for (DistrTurn itm : list) {
            Element el = doc.createElement("distrturn");
            all.appendChild(el);
            el.setAttribute("distrTurnID", "" + itm.distrTurnID);
            el.setAttribute("dirID", "" + itm.dirID);
            el.setAttribute("distrID", "" + itm.distrID);
            el.setAttribute("name", "" + itm.name.trim());
            el.setAttribute("stBID", "" + itm.stBID);
            el.setAttribute("stEID", "" + itm.stEID);
            el.setAttribute("weightforw", "" + itm.weightforw);
            el.setAttribute("weightback", "" + itm.weightback);
            el.setAttribute("gradientforw", "" + itm.gradientforw);
            el.setAttribute("gradientback", "" + itm.gradientback);
            el.setAttribute("lenght", "" + itm.lenght);
            el.setAttribute("num", "" + itm.num);

            try {
                el.setAttribute("dirName", "" + itm.dirName.trim());
                el.setAttribute("distrName", "" + itm.distrName.trim());
                el.setAttribute("stBcodeESR", "" + itm.stB.codeESR.trim());
                el.setAttribute("stEcodeESR", "" + itm.stE.codeESR.trim());
                el.setAttribute("stBcodeExpress", "" + itm.stB.codeExpress.trim());
                el.setAttribute("stEcodeExpress", "" + itm.stE.codeExpress.trim());
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }

        }
    }

    private void saveSeriesLoc(Document doc) {
        Element root = doc.getDocumentElement();
        Element all = doc.createElement("listseriesloc");
        root.appendChild(all);
        List<SeriesLoc> list = LocDAOFactory.getInstance().getSeriesLocDAO().loadAllSLoc();
        for (SeriesLoc itm : list) {
            Element el = doc.createElement("seriesloc");
            all.appendChild(el);
            el.setAttribute("seriesLocID", "" + itm.seriesLocID);
            el.setAttribute("series", "" + itm.series);
            el.setAttribute("electr", "" + itm.electr);
        }
    }
}
