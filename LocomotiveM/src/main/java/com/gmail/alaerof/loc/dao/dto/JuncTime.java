package com.gmail.alaerof.loc.dao.dto;
/**
 * представляет собой запись из таблицы JuncTime
 * временные параметры для узла по обслуживанию локомотивов
 */
public class JuncTime {
    public int juncID;
    /** по прибытию, мин */
    public int arrive;
    /** по отправлению, мин */
    public int depart;
    /** перецепка без захода в депо, мин */
    public int equipment;
    /** не используется !!! */
    public int acceptance;
    
}
