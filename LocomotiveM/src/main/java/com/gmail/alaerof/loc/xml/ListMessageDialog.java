package com.gmail.alaerof.loc.xml;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class ListMessageDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private int result;
    private JTextArea textArea = new JTextArea();

    /**
     * Create the dialog.
     */
    public ListMessageDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private void initContent() {
        setBounds(100, 100, 650, 300);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        {
            JPanel topPane = new JPanel();
            topPane.setLayout(new FlowLayout(FlowLayout.LEADING));
            getContentPane().add(topPane, BorderLayout.NORTH);
            topPane.add(new JLabel("Есть нарушения структуры участка. Продолжить?"));
        }
        {
            JPanel contentPanel = new JPanel();
            contentPanel.setLayout(new BorderLayout());
            contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
            getContentPane().add(contentPanel, BorderLayout.CENTER);
            JScrollPane scrollPane = new JScrollPane(textArea);
            contentPanel.add(scrollPane, BorderLayout.CENTER);
        }

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog d = this;
            {
                JButton okButton = new JButton("Да");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        result = 1;
                        d.setVisible(false);
                    }
                });
            }
            {
                JButton cancelButton = new JButton("Нет");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        result = 0;
                        d.setVisible(false);

                    }
                });
            }
        }
    }

    public int getResult() {
        return result;
    }

    public void setText(List<String> text) {
        textArea.setText("");
        for (int i = 0; i < text.size(); i++) {
            String str = text.get(i);
            textArea.append(str);
            textArea.append("\n");
        }
        textArea.setEditable(false);
    }
}
