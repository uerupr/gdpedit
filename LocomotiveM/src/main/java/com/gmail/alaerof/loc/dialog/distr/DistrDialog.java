package com.gmail.alaerof.loc.dialog.distr;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.Direction;
import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.DistrDAO;
import com.gmail.alaerof.loc.entity.DistrEntity;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.application.LinkingLocMain;
import com.gmail.alaerof.loc.util.FileHandler;
import com.gmail.alaerof.loc.xml.GDPDataXML;

public class DistrDialog extends JDialog {
    private static Logger logger = LogManager.getLogger(DistrDialog.class);
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private DefaultListModel<Direction> dirListModel = new DefaultListModel<>();
    private JList<Direction> dirList = new JList<Direction>(dirListModel);
    private Direction selectedDir;
    private DistrTableModel distrTableModel = new DistrTableModel();
    private JTable distrTable;
    private DistrJuncCellEditor juncSellEditor = new DistrJuncCellEditor(null);

    public DistrDialog() {
        initContent();
    }

    public DistrDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private void initContent() {
        setBounds(100, 100, 800, 400);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);

        {
            dirList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            dirList.setLayoutOrientation(JList.VERTICAL);
            dirList.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent evt) {
                    int index = dirList.getSelectedIndex();
                    if (index > -1) {
                        Direction d = dirListModel.get(index);
                        if (!d.equals(selectedDir)) {
                            selectedDir = d;
                            fillDistr();
                        }
                    }
                }
            });

            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            panel.setPreferredSize(new Dimension(100, 100));
            JScrollPane scroll = new JScrollPane(dirList);
            panel.add(scroll, BorderLayout.CENTER);

            contentPanel.add(panel, BorderLayout.WEST);
        }
        {
            distrTable = new JTable(distrTableModel) {
                private static final long serialVersionUID = 1L;

                // Implement table cell tool tips.
                public String getToolTipText(MouseEvent e) {
                    String tip = null;
                    java.awt.Point p = e.getPoint();
                    int rowIndex = rowAtPoint(p);
                    int colIndex = columnAtPoint(p);
                    int realColumnIndex = convertColumnIndexToModel(colIndex);
                    int realRowIndex = convertRowIndexToModel(rowIndex);
                    if (realRowIndex > -1 && realColumnIndex > -1) {
                        Object o = distrTableModel.getValueAt(realRowIndex, realColumnIndex);
                        if (o != null) {
                            tip = o.toString();
                        }
                    }
                    return tip;
                }

            };
            distrTable.setFillsViewportHeight(true);
            distrTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSizeO[] = { 160, 80, 80, 300 };
            com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(distrTable, colSizeO);
            distrTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            // distrTable.setDefaultRenderer(Object.class, new
            // DistrCellRenderer());
            distrTable.setDefaultEditor(Junc.class, juncSellEditor);

            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            JScrollPane scroll = new JScrollPane(distrTable);
            panel.add(scroll, BorderLayout.CENTER);

            contentPanel.add(panel, BorderLayout.CENTER);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog d = this;
            {
                JButton okButton = new JButton("Загрузить ГДП из xml в БД");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            int row = distrTable.getSelectedRow();
                            Distr distr = null;
                            if (row > -1) {
                                distr = distrTableModel.getList().get(row);
                            }
                            String fileName = "";
                            File file = FileHandler.chooseFile(true, new FileNameExtensionFilter("xml files",
                                    "xml"), fileName, LinkingLocMain.mainFrame);

                            if (file != null) {
                                boolean done = false;
                                LinkingLocMain.mainFrame.setCursorWait();
                                try {
                                    fileName = file.getAbsolutePath();
                                    int distrID = distr.distrID; // !!!
                                    DistrEntity distrEnt = new DistrEntity(distr);
                                    GDPDataXML xmlReader = GDPDataXML.getInstance();
                                    List<TrainThreadSheet> list = xmlReader.loadFromFile(fileName, distrEnt);
                                    if (list.size() > 0) {
                                        LocDAOFactory.getInstance().getDistrTrainDAO().saveGDP(distrID, list);
                                        distr.fileName = fileName;
                                        LocDAOFactory.getInstance().getDistrDAO().updateFileName(distr);
                                        done = true;
                                    }
                                } finally {
                                    LinkingLocMain.mainFrame.setCursorDefault();
                                }
                                String mess = "";
                                if (done) {
                                    mess = "ГДП загружен успешно";
                                } else {
                                    mess = "ГДП не загружен";
                                }
                                JOptionPane.showMessageDialog(LinkingLocMain.mainFrame, mess, "",
                                        JOptionPane.INFORMATION_MESSAGE);
                                distrTable.repaint();
                            }
                        } catch (Exception ex) {
                            logger.error(ex.toString(), ex);
                            ex.printStackTrace();
                        }
                    }

                });
            }
            {
                JButton button = new JButton("Сохранить");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            List<Distr> list = distrTableModel.getList();
                            DistrDAO distrDAO = LocDAOFactory.getInstance().getDistrDAO();
                            for (Distr distr : list) {
                                distrDAO.updateDistrJunc(distr);
                            }
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
            {
                JButton cancelButton = new JButton("Закрыть");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        d.setVisible(false);
                    }
                });
            }
        }
    }

    private void fillDistr() {
        List<Distr> list = LocDAOFactory.getInstance().getDistrDAO().getListDistr(selectedDir.dirID);
        distrTableModel.setList(list);
        distrTable.revalidate();
        distrTable.repaint();
    }

    public void loadData() {
        dirListModel.removeAllElements();
        List<Direction> list = LocDAOFactory.getInstance().getDirectionDAO().loadAllDirection();
        for (Direction d : list) {
            dirListModel.addElement(d);
        }
        List<Junc> listJ = LocDAOFactory.getInstance().getJuncDAO().loadAllJunc();
        Junc jj = new Junc();
        jj.name = "-";
        listJ.add(jj);
        Collections.sort(listJ);
        juncSellEditor.setListJunc(listJ);
    }

}
