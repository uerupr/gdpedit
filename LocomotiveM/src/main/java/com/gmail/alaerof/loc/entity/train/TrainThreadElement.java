package com.gmail.alaerof.loc.entity.train;

import java.awt.Graphics;

import javax.swing.JComponent;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.train.newline.NewLineSelfParams;

public abstract class TrainThreadElement {
    protected static Logger logger = LogManager.getLogger(TrainThreadSheet.class);
    /** визуальный контейнер элемента */
    protected JComponent owner;
    /** нитка поезда, которой принадлежит элемент */
    protected TrainThreadSheet train;
    /** параметры отображения, общие для ниток на листе */
    protected SheetConfig sheetConf;
    /** индивидуальные параметры отображения элемента */
    protected NewLineSelfParams selfParams = new NewLineSelfParams();

    public TrainThreadElement(JComponent owner, TrainThreadSheet train, SheetConfig sheetConf) {
        super();
        this.owner = owner;
        this.train = train;
        this.sheetConf = sheetConf;
    }

    public TrainThreadSheet getTrainThread() {
        return train;
    }

    /** установка размеров и положения линий */
    public abstract void setLineBounds();

    /** удаление элемента из компонента-владельца */
    public abstract void removeFromOwner();

    /** добавление элемента в компонент-владелец */
    public abstract void addToOwner();

    public void mouseClicked() {
        if (train != null) {
            train.mouseClicked();
        }
    }
    
    /**
     * перерисовка элемента в контейнере
     */
    public abstract void repaint();

    /**
     * отрисовка элемента на некой канве
     * @param grGDP канва для рисования
     * @param gapLeft отступ слева
     * @param gapTop отступ справа
     */
    public abstract void drawInImage(Graphics grGDP, int gapLeft, int gapTop);

}
