package com.gmail.alaerof.loc.entity.train;


public interface ThrainThreadHandler {
    public TrainThread getTrainThread();
    public void setTrainThread(TrainThread train);
}
