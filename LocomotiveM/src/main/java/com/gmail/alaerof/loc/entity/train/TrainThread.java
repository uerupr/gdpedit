package com.gmail.alaerof.loc.entity.train;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.train.DistrTrain;
import com.gmail.alaerof.loc.dao.dto.train.DirTrainResult;
import com.gmail.alaerof.loc.dao.dto.train.TrainSpan;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.DistrEntity;
import com.gmail.alaerof.loc.entity.DistrSpan;
import com.gmail.alaerof.loc.entity.DistrStation;
import com.gmail.alaerof.loc.entity.train.newline.LineStyle;
import com.gmail.alaerof.loc.util.ColorConverter;
import com.gmail.alaerof.loc.util.TimeConverter;

public class TrainThread {
    private static Logger logger = LogManager.getLogger(TrainThread.class);
    /** данные о нитке */
    protected DistrTrain distrTrain;
    /** полное расписание по станциям */
    protected List<TrainStation> trainSt = new ArrayList<>();
    /**
     * полное расписание по перегонам
     * [используется только для расчета показателей !!!]
     */
    protected List<TrainSpan> trainSp = new ArrayList<>();

    public TrainThread(DistrTrain distrTrain) {
        this.distrTrain = distrTrain;
    }

    public TrainThread() {
    }

    public DistrTrain getDistrTrain() {
        return distrTrain;
    }

    public void setDistrTrain(DistrTrain distrTrain) {
        this.distrTrain = distrTrain;
    }

    /** используется только при загрузке расписания из БД */
    public void setTrainSt(List<TrainStation> trainSt) {
        this.trainSt = trainSt;
    }

    public List<TrainStation> getTrainSt() {
        return trainSt;
    }

    /** используется только при загрузке расписания из БД */
    public void setTrainSp(List<TrainSpan> trainSp) {
        this.trainSp = trainSp;
    }

    public List<TrainSpan> getTrainSp() {
        return trainSp;
    }

    /** сортировка станций полного расписания по ходу движения */
    public void sortSt() {
        Collections.sort(trainSt, new TrainStComparator());
    }

    /** возвращает ID станции отправления при наличии, иначе -1 */
    public Integer getFirstStID() {
        TrainStation trSt = getFirstSt();
        if (trSt != null) {
            return trSt.stationID;
        }
        return -1;
    }

    /** возвращает станцию отправления при наличии, иначе null */
    public TrainStation getFirstSt() {
        if (trainSt.size() == 0)
            return null;
        return trainSt.get(0);
    }

    /** возвращает ID станции прибытия при наличии, иначе -1 */
    public Integer getLastStID() {
        TrainStation trSt = getLastSt();
        if (trSt != null) {
            return trSt.stationID;
        }
        return -1;
    }

    /** возвращает ID станции прибытия при наличии, иначе -1 */
    public TrainStation getLastSt() {
        if (trainSt.size() == 0)
            return null;
        return trainSt.get(trainSt.size() - 1);
    }

    /** возвращает номер поезда как строку */
    public String getCode() {
        return "" + distrTrain.codeTR;
    }

    /** возвращает номер поезда как число */
    public int getCodeINT() {
        return distrTrain.codeTR;
    }

    public LineStyle getLineStyle() {
        return LineStyle.getLineStyle(distrTrain.lineStyle);
        // return LineStyle.Solid;
    }

    public int getLineWidth() {
        return distrTrain.widthTR;
    }

    public Color getColorTR() {
        Color clr = ColorConverter.convert(distrTrain.colorTR);
        if (clr.equals(Color.WHITE)) {
            clr = Color.black;
        }
        return clr;
    }

    public boolean isHidden() {
        return false;
    }

    /** возвращает элемент расписания по ID станции, или null */
    public TrainStation getTrainStation(int stationID) {
        for (int i = 0; i < trainSt.size(); i++) {
            TrainStation st = trainSt.get(i);
            if (st.stationID == stationID) {
                return st;
            }
        }
        return null;
    }

    public int getDistrID() {
        return distrTrain.distrID;
    }

    /** true если поезд нечетный */
    public boolean isOdd() {
        return (distrTrain.codeTR % 2) == 1;
    }

    /** true если поезд четный */
    public boolean isEven() {
        return (distrTrain.codeTR % 2) == 0;
    }

    public int calculateResults(DistrEntity distr) {
        // заполнение таблицы TrainSpan в соотв. с TrainStation
        int err = fillTrainSpan(distr);
        // сохранение TrainSpan в БД
        LocDAOFactory.getInstance().getDistrTrainDAO().saveListTrainSpan(distrTrain.trainID, trainSp);
        // расчет итоговых результатов по поезду: только расстояние !!!
        LocDAOFactory.getInstance().getDistrTrainDAO().setTrainResLen(distrTrain);
        // техническое время
        Map<Integer, DirTrainResult> map = getMap();
        for (TrainSpan trsp : trainSp) {
            DistrSpan dsp = distr.getSpan(trsp.spanID);
            DirTrainResult dtr = map.get(dsp.getSpan().dirID);
            if (dtr != null) {
                dtr.Tt += TimeConverter.minutesBetween(trsp.getTimeOn(), trsp.getTimeOff());
            }
        }
        // участковое время : стоянки
        for (TrainStation trst : trainSt) {
            DistrStation dst = distr.getStationByID(trst.stationID);
            DirTrainResult dtr = map.get(dst.getStation().dirID);
            if (dtr != null) {
                dtr.Tu += TimeConverter.minutesBetween(trst.getTimeOnChecked(), trst.getTimeOffChecked());
            }
        }
        // участковое время : движение
        List<DirTrainResult> list = distrTrain.getListRes();
        for (DirTrainResult dtr : list) {
            dtr.Tu += dtr.Tt;
        }
        // сохранение итоговых результатов в БД
        LocDAOFactory.getInstance().getDistrTrainDAO().saveTrainResults(distrTrain);
        return err;
    }

    private Map<Integer, DirTrainResult> getMap() {
        Map<Integer, DirTrainResult> map = new HashMap<>();
        List<DirTrainResult> list = distrTrain.getListRes();
        for (DirTrainResult dtr : list) {
            map.put(dtr.dirID, dtr);
        }
        return map;
    }

    /**
     * заполняет TrainSpan для поезда, возвращает количество ненайденных
     * перегонов
     */
    private int fillTrainSpan(DistrEntity distr) {
        trainSp = new ArrayList<>();
        sortSt();
        int err = 0;
        List<TrainStation> listSt = getTrainSt();
        for (int i = 1; i < listSt.size(); i++) {
            TrainStation stB = listSt.get(i - 1);
            TrainStation stE = listSt.get(i);
            DistrSpan span = null;
            DistrSpan spanO = distr.getSpan(stB.stationID, stE.stationID);
            DistrSpan spanE = distr.getSpan(stE.stationID, stB.stationID);
            if (isOdd()) {
                span = spanO;
                if (spanO == null) {
                    span = spanE;
                }
            } else {
                span = spanE;
                if (spanE == null) {
                    span = spanO;
                }
            }
            if (span != null) {
                TrainSpan trSpan = new TrainSpan();
                trSpan.spanID = span.getSpan().spanID;
                trSpan.trainID = distrTrain.trainID;
                trSpan.setTimeOn(stB.getTimeOffChecked());
                trSpan.setTimeOff(stE.getTimeOnChecked());
                trainSp.add(trSpan);
            } else {
                logger.error(this.getCode() + " на участке '" + distr.getDistrName() + "' не найден перегон: " + stB.stationName.trim() + "-"
                        + stE.stationName.trim());
                err++;
            }
        }
        return err;
    }

    /**
     * расчет технических и участковых поездо-минут для заданной
     * последовательности перегонов
     * 
     * @param listSpan список перегонов участка
     * @param idSt ид спорной станции (не узловая станция на ж.д. участке)
     * @param includeSt включать ли время по спорной станции в расчет участковых поездо-минут
     * @return int[0] - технические поездо-минуты, int[1] - участковые поездо-минуты
     */
    public int[] calculateTrainMinutes(List<DistrSpan> listSpan, int idSt, boolean includeSt) {
        int res[] = new int[3];
        // расчет технических поездо-минут и поездо-метров
        for (TrainSpan sp : trainSp) {
            int l = spanInList(sp, listSpan);
            if (l != -1) {
                int m = TimeConverter.minutesBetween(sp.getTimeOn(), sp.getTimeOff());
                res[0] += m;
                res[2] += l;

            }
        }
        // расчет стоянок на промежуточных станциях
        for (TrainStation st : trainSt) {
            if ((st.stationID != idSt || includeSt) && stationInList(st, listSpan)) {
                int m = TimeConverter.minutesBetween(st.getTimeOnChecked(), st.getTimeOffChecked());
                res[1] += m;
            }
        }
        res[1] += res[0];
        return res;
    }

    private boolean stationInList(TrainStation st, List<DistrSpan> listSpan) {
        for (DistrSpan dsp : listSpan) {
            int idStB = dsp.getSpan().stB;
            int idStE = dsp.getSpan().stE;
            if (st.stationID == idStB || st.stationID == idStE)
                return true;
        }
        return false;
    }

    /**
     * проверяет наличие перегона в списке
     * 
     * @param sp перегон поезда
     * @param listSpan список перегонов участка
     * @return длину перегона если есть и -1 иначе
     */
    private int spanInList(TrainSpan sp, List<DistrSpan> listSpan) {
        for (DistrSpan dsp : listSpan) {
            if (sp.spanID == dsp.getSpan().spanID)
                return dsp.getScale().L;
        }
        return -1;
    }

    @Override
    public String toString() {
        return "" + distrTrain.codeTR + "(" + distrTrain.nameTR + ")";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((distrTrain == null) ? 0 : distrTrain.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TrainThread other = (TrainThread) obj;
        if (distrTrain == null) {
            if (other.distrTrain != null)
                return false;
        } else if (!distrTrain.equals(other.distrTrain))
            return false;
        return true;
    }

}
