package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.train.DistrTrain;
import com.gmail.alaerof.loc.dao.dto.train.DirTrainResult;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.dto.train.TrainSpan;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.dao.dto.train.TrainType;
import com.gmail.alaerof.loc.entity.train.TrainThread;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.application.LinkingLocMain;

class Transfer {
    long trainID;
    int distrID;
    int codeTR;
    String nameTR;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + codeTR;
        result = prime * result + distrID;
        result = prime * result + ((nameTR == null) ? 0 : nameTR.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Transfer other = (Transfer) obj;
        if (codeTR != other.codeTR)
            return false;
        if (distrID != other.distrID)
            return false;
        if (nameTR == null) {
            if (other.nameTR != null)
                return false;
        } else if (!nameTR.equals(other.nameTR))
            return false;
        return true;
    }

}

public class DistrTrainDAO {
    private static Logger logger = LogManager.getLogger(DistrTrainDAO.class);
    private Connection cnLocal;

    public DistrTrainDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    public void clearGDP(int distrID) {
        try (Statement st = cnLocal.createStatement();) {
            String sql = "DELETE FROM DistrTrain WHERE distrID = " + distrID;
            st.executeUpdate(sql);
            // удаление из таблиц TrainStation, TrainSpan должно произойти
            // автоматически
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    private void insertGDP(List<TrainThreadSheet> list) {
        String sql = "INSERT INTO DistrTrain(distrID, categoryID, codeTR, nameTR, colorTR, hidden, "
                + "ainscr, showAI, lineStyle, widthTR) " + "VALUES (?,?,?,?,?,?,?,?,?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                DistrTrain dt = list.get(i).getDistrTrain();
                st.setInt(1, dt.distrID);
                st.setInt(2, dt.categoryID);
                st.setInt(3, dt.codeTR);
                st.setString(4, dt.nameTR.trim());
                st.setInt(5, dt.colorTR);
                st.setBoolean(6, dt.hidden);
                st.setString(7, dt.ainscr.trim());
                st.setBoolean(8, dt.showAI);
                st.setInt(9, dt.lineStyle);
                st.setInt(10, dt.widthTR);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    private Map<Transfer, Long> selectNewTrainID(int distrID) {
        Map<Transfer, Long> map = new HashMap<>();
        String sql = "SELECT trainID, distrID, codeTR, nameTR FROM DistrTrain WHERE distrID = " + distrID;
        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Transfer item = new Transfer();
                    item.trainID = rs.getLong("trainID");
                    item.distrID = rs.getInt("distrID");
                    item.codeTR = rs.getInt("codeTR");
                    item.nameTR = rs.getString("nameTR").trim();
                    map.put(item, item.trainID);
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }

        return map;
    }

    private long getNewTrainID(Map<Transfer, Long> newTrainID, DistrTrain dt) {
        long newID = 0;
        Transfer trans = new Transfer();
        trans.distrID = dt.distrID;
        trans.codeTR = dt.codeTR;
        trans.nameTR = dt.nameTR;
        Long id = newTrainID.get(trans);
        if (id != null) {
            newID = id;
        }
        return newID;
    }

    public void saveListTrainStation(long trainID, List<TrainStation> list) {
        String sql = "INSERT INTO TrainStation (trainID, stationID, timeOn, timeOff, texStop) VALUES (?,?,?,?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                TrainStation item = list.get(i);
                st.setLong(1, trainID);
                st.setInt(2, item.stationID);
                if (item.getTimeOn() != null) {
                    st.setInt(3, item.getTimeOn());
                } else {
                    st.setNull(3, java.sql.Types.NULL);
                }
                if (item.getTimeOff() != null) {
                    st.setInt(4, item.getTimeOff());
                } else {
                    st.setNull(4, java.sql.Types.NULL);
                }
                st.setInt(5, item.texStop);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }

    }

    public void saveListTrainSpan(long trainID, List<TrainSpan> list) {
        deleteTrainSpan(trainID);
        String sql = "INSERT INTO TrainSpan (trainID, spanID, timeOn, timeOff) VALUES (?,?,?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (int i = 0; i < list.size(); i++) {
                TrainSpan item = list.get(i);
                st.setLong(1, trainID);
                st.setInt(2, item.spanID);
                st.setInt(3, item.getTimeOn());
                st.setInt(4, item.getTimeOff());
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
                // st.e
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void saveGDP(int distrID, List<TrainThreadSheet> list) {
        clearGDP(distrID);
        // список поездов
        insertGDP(list);
        // новые ID шники
        Map<Transfer, Long> newTrainID = selectNewTrainID(distrID);
        // расписание
        for (int i = 0; i < list.size(); i++) {
            TrainThreadSheet train = list.get(i);
            DistrTrain dt = train.getDistrTrain();
            long newID = getNewTrainID(newTrainID, dt);
            if (newID > 0) {
                // по станциям
                List<TrainStation> listSt = train.getTrainSt();
                saveListTrainStation(newID, listSt);
                // по перегонам
                List<TrainSpan> listSp = train.getTrainSp();
                saveListTrainSpan(newID, listSp);
            }
        }
    }

    private void loadTrainSp(TrainThread tr) {
        String sql = "SELECT trainID, spanID, timeOn, timeOff FROM TrainSpan WHERE trainID = "
                + tr.getDistrTrain().trainID;
        List<TrainSpan> trainSp = new ArrayList<>();
        tr.setTrainSp(trainSp);
        try (Statement st = cnLocal.createStatement()) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    TrainSpan ts = new TrainSpan();
                    ts.trainID = rs.getLong("trainID");
                    ts.spanID = rs.getInt("spanID");
                    ts.setTimeOn(rs.getInt("timeOn"));
                    ts.setTimeOff(rs.getInt("timeOff"));
                    trainSp.add(ts);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    private void loadTrainSt(TrainThread tr) {
        String sql = "SELECT trainID, TrainStation.stationID, timeOn, timeOff, texStop, Station.name "
                + " FROM TrainStation INNER JOIN Station ON TrainStation.stationID = Station.stationID "
                + " WHERE trainID = "
                + tr.getDistrTrain().trainID;
        List<TrainStation> trainSt = new ArrayList<>();
        tr.setTrainSt(trainSt);
        Integer o = null;
        try (Statement st = cnLocal.createStatement()) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    TrainStation ts = new TrainStation();
                    ts.trainID = rs.getLong("trainID");
                    ts.stationID = rs.getInt("stationID");
                    o = (Integer) rs.getObject("timeOn");
                    ts.setTimeOn(o);
                    o = (Integer) rs.getObject("timeOff");
                    ts.setTimeOff(o);
                    ts.texStop = rs.getInt("texStop");
                    ts.stationName = rs.getString("name");
                    trainSt.add(ts);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * загружает ГДП из БД. возвращает либо TrainThread либо любого наследника в
     * зависимости от sample
     */
    public List<TrainThread> loadGDP(int distrID, TrainThread sample) {
        String sql = " FROM DistrTrain WHERE distrID = " + distrID;
        return loadGDP(sql, sample);
    }

    /** загружает список TrainThread прибывающих в узел */
    public List<TrainThread> getJuncTrainOn(int juncID, TrainThread sample) {
        String sql = " FROM DistrTrain, linktrain WHERE DistrTrain.distrID = linktrain.distrIDon "
                + "AND DistrTrain.codeTR = linktrain.codeTRon AND DistrTrain.nameTR = linktrain.nameTRon "
                + "AND juncID = " + juncID;
        return loadGDP(sql, sample);
    }

    /** загружает список TrainThread отправляющихся из узла */
    public List<TrainThread> getJuncTrainOff(int juncID, TrainThread sample) {
        String sql = " FROM DistrTrain, linktrain WHERE DistrTrain.distrID = linktrain.distrIDoff "
                + "AND DistrTrain.codeTR = linktrain.codeTRoff AND DistrTrain.nameTR = linktrain.nameTRoff "
                + "AND juncID = " + juncID;
        return loadGDP(sql, sample);
    }

    private List<TrainThread> loadGDP(String sql, TrainThread sample) {
        sql = "SELECT DistrTrain.trainID, DistrTrain.distrID, DistrTrain.categoryID, DistrTrain.codeTR, DistrTrain.nameTR, "
                + "DistrTrain.colorTR, DistrTrain.hidden, DistrTrain.ainscr, DistrTrain.showAI, DistrTrain.lineStyle, "
                + "DistrTrain.widthTR " + sql;
        List<TrainThread> list = new ArrayList<>();
        try (Statement st = cnLocal.createStatement()) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    DistrTrain dt = new DistrTrain();
                    dt.trainID = rs.getLong("trainID");
                    dt.distrID = rs.getInt("distrID");
                    dt.categoryID = rs.getInt("categoryID");
                    dt.codeTR = rs.getInt("codeTR");
                    dt.nameTR = rs.getString("nameTR").trim();
                    dt.colorTR = rs.getInt("colorTR");
                    dt.hidden = rs.getBoolean("hidden");
                    dt.ainscr = rs.getString("ainscr");
                    dt.showAI = rs.getBoolean("showAI");
                    dt.lineStyle = rs.getInt("lineStyle");
                    dt.widthTR = rs.getInt("widthTR");
                    // TrainThreadSheet tr = new TrainThreadSheet(dt);
                    TrainThread tr = sample.getClass().newInstance();
                    tr.setDistrTrain(dt);
                    loadTrainCat(tr);
                    // if(dt.codeTR==226){
                    // System.out.println(dt);
                    // }
                    // проверка на категорию поезда (в настройках выставлены
                    // нужные категории)
                    if (LinkingLocMain.mainFrame.mainConfigDialog.isVisibleTypeTrain(tr.getDistrTrain()
                            .getTypeTR())) {
                        loadTrainSt(tr);
                        loadTrainSp(tr);
                        loadTrainResults(dt);
                        list.add(tr);
                        // сразу сортируем станции поезда по ходу движения
                        tr.sortSt();
                    }
                }
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    private void loadTrainCat(TrainThread tr) {
        String sql = "SELECT type FROM category WHERE categoryID = " + tr.getDistrTrain().categoryID;
        try (Statement st = cnLocal.createStatement()) {
            try (ResultSet rs = st.executeQuery(sql)) {
                if (rs.next()) {
                    String typeStr = rs.getString("type").trim();
                    TrainType type = TrainType.getType(typeStr);
                    tr.getDistrTrain().setTypeTR(type);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public boolean updateTrainCat() {
        boolean res = true;
        String sql = "UPDATE DistrTrain SET DistrTrain.categoryID = "
                + "(SELECT MIN(category.categoryID) FROM category "
                + " WHERE DistrTrain.codeTR >= category.trMin AND DistrTrain.codeTR <= category.trMax )";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            res = false;
        }
        sql = "SELECT codeTR, nameTR FROM DistrTrain WHERE categoryID is NULL";
        try (Statement st = cnLocal.createStatement()) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    res = false;
                    String codeTR = rs.getString("codeTR").trim();
                    String nameTR = rs.getString("nameTR").trim();
                    logger.error("для поезда нет категории: " + codeTR + " (" + nameTR + ")");
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            res = false;
        }
        return res;
    }

    public void loadTrainResults(DistrTrain distrTrain) {
        List<DirTrainResult> list = new ArrayList<>();
        distrTrain.setListRes(list);

        String sql = "SELECT trainID, dirID, L, Tt, Tu FROM DirTrainResult" + " WHERE trainID = "
                + distrTrain.trainID;
        try (Statement st = cnLocal.createStatement()) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    DirTrainResult ts = new DirTrainResult();
                    ts.trainID = rs.getLong("trainID");
                    ts.dirID = rs.getInt("dirID");
                    ts.L = rs.getInt("L");
                    ts.Tt = rs.getInt("Tt");
                    ts.Tu = rs.getInt("Tu");
                    list.add(ts);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }

    }

    public void deleteTrainResults(DistrTrain distrTrain) {
        String sql = "DELETE FROM DirTrainResult WHERE trainID = " + distrTrain.trainID;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void saveTrainResults(DistrTrain distrTrain) {
        deleteTrainResults(distrTrain);
        List<DirTrainResult> list = distrTrain.getListRes();
        if (list != null) {
            String sql = "INSERT INTO DirTrainResult (trainID, dirID, L, Tt, Tu) VALUES (?,?,?,?,?)";
            int count = 0;
            try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
                for (DirTrainResult dtr : list) {
                    st.setLong(1, dtr.trainID);
                    st.setInt(2, dtr.dirID);
                    st.setInt(3, dtr.L);
                    st.setInt(4, dtr.Tt);
                    st.setInt(5, dtr.Tu);
                    st.addBatch();
                    count++;
                }
                if (count > 0) {
                    st.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    public void deleteTrainSpan(long trainID) {
        try (Statement st = cnLocal.createStatement();) {
            String sql = "DELETE FROM TrainSpan WHERE trainID = " + trainID;
            st.executeUpdate(sql);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    public void setTrainResLen(DistrTrain distrTrain) {

        List<DirTrainResult> list = new ArrayList<>();
        distrTrain.setListRes(list);

        String sql = " SELECT DistrTrain.trainID, Span.dirID, SUM(Scale.L) as sumL "
                + " FROM TrainSpan, DistrTrain, Span, Scale "
                + " WHERE TrainSpan.trainID = " + distrTrain.trainID
                + " AND TrainSpan.trainID = DistrTrain.trainID "
                + " AND TrainSpan.spanID = Span.spanID "
                + " AND TrainSpan.spanID = Scale.spanID "
                + " AND DistrTrain.distrID = Scale.distrID "
                + " GROUP BY DistrTrain.trainID, Span.dirID ";

        try (Statement st = cnLocal.createStatement()) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    DirTrainResult ts = new DirTrainResult();
                    ts.trainID = rs.getLong("trainID");
                    ts.dirID = rs.getInt("dirID");
                    ts.L = rs.getInt("sumL");
                    list.add(ts);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }

    }

    /**
     * загружает TrainThread (или TrainThreadSheet) из БД по увязке (или прибытие или отправление)
     * @param link увязка
     * @param sample образец объекта TrainThread (класс)
     * @param trOn если True, то возвращает поезд прибытия, иначе отправления
     * @return объект TrainThread
     */
    public TrainThread loadTrain(LinkTrain link, TrainThread sample, boolean trOn) {
        String sql = "";
        if (trOn) {
            sql = " FROM DistrTrain WHERE distrID = " + link.distrIDon + " AND codeTR = " + link.codeTRon
                    + " AND nameTR = '" + link.nameTRon + "'";
        } else {
            sql = " FROM DistrTrain WHERE distrID = " + link.distrIDoff + " AND codeTR = " + link.codeTRoff
                    + " AND nameTR = '" + link.nameTRoff + "'";
        }
        List<TrainThread> list = loadGDP(sql, sample);
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }
}
