package com.gmail.alaerof.loc.dialog.distrturn;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.loc.dao.dto.RangeTR;

public class RangeTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    protected String[] columnNames = { "Название", "Диапазон" };
    protected List<RangeTR> list = new ArrayList<>();

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            RangeTR el = list.get(row);
            switch (col) {
            case 0:
                return el.name;
            case 1:
                return el.getRangeValue();
            default:
                return null;
            }
        }
        return null;
    }
    
    @Override
    public java.lang.Class<?> getColumnClass(int col) {
        switch (col) {
        case 0:
        case 1:
            return String.class;
        }
        return Object.class;
    }
    public List<RangeTR> getList() {
        return list;
    }

    public void setList(List<RangeTR> list) {
        this.list = list;
    }

}
