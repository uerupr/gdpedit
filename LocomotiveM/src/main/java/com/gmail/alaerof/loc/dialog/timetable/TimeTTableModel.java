package com.gmail.alaerof.loc.dialog.timetable;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class TimeTTableModel extends AbstractTableModel{
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { "Станция", "приб.", "отпр.", "ст.", "тех." };
    private List<TimeTStation> list;

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (list != null) {
            TimeTStation el = list.get(rowIndex);
            switch (columnIndex) {
            case 0:
                return el.getStationName();
            case 1:
                return el.getTimeOn();
            case 2:
                return el.getTimeOff();
            case 3:
                return el.gettStop();
            case 4:
                return el.getTexStop();
            }
        }
        return null;
    }

    public List<TimeTStation> getList() {
        return list;
    }

    public void setList(List<TimeTStation> list) {
        this.list = list;
    }

}
