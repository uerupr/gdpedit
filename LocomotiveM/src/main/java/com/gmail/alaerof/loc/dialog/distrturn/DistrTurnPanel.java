package com.gmail.alaerof.loc.dialog.distrturn;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.DistrTurn;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.application.LinkingLocMain;

public class DistrTurnPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(DistrTurnPanel.class);
    private DistrTurnTableModel tableModel = new DistrTurnTableModel();
    private JTable table;
    private DistrTurn selected;
    private int dirID;
    private RangeTRPanel rangePanel;

    public DistrTurnPanel() {
        this.setLayout(new BorderLayout());
        // dialog = LinkingLocMain.mainframe.distrTurnAEDialog;
        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());

        table = new JTable(tableModel);
        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSizeO[] = { 30, 80, 80, 40, 40, 40, 40, 40, 40, 40 };
        com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(table, colSizeO);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane scroll = new JScrollPane(table);
        tablePanel.add(scroll, BorderLayout.CENTER);

        this.add(tablePanel, BorderLayout.CENTER);

        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent ev) {
                selectRow();
            }
        });

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent ev) {
                selectRow();
            }
        });

        {
            rangePanel = new RangeTRPanel();
            rangePanel.setPreferredSize(new Dimension(360, 0));
            this.add(rangePanel, BorderLayout.EAST);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.LEFT));
            this.add(buttonPane, BorderLayout.SOUTH);
            final DistrTurnPanel inv = this;
            {
                JButton button = new JButton("создать");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        DistrTurn distrTurn = new DistrTurn();
                        getDialog().setTitle("создание");
                        getDialog().loadData(inv, distrTurn);
                        getDialog().setVisible(true);

                    }
                });
            }
            {
                JButton button = new JButton("изменить");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        int row = table.getSelectedRow();
                        if (row > -1) {
                            DistrTurn distrTurn = tableModel.getList().get(row);
                            getDialog().setTitle("редактирование");
                            getDialog().loadData(inv, distrTurn);
                            getDialog().setVisible(true);
                        }
                    }
                });
            }
            {
                JButton button = new JButton("сохранить");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            setCursor(LinkingLocMain.waitCursor);
                            List<DistrTurn> listDT = tableModel.getList();
                            for (DistrTurn dt : listDT) {
                                LocDAOFactory.getInstance().getDistrTurnDAO().saveDistrTurnTail(dt);
                            }
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        } finally {
                            setCursor(LinkingLocMain.defCursor);
                        }
                    }
                });
            }
            {
                JButton button = new JButton("удалить");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            int row = table.getSelectedRow();
                            if (row > -1) {
                                DistrTurn distrTurn = tableModel.getList().get(row);
                                tableModel.getList().remove(row);
                                LocDAOFactory.getInstance().getDistrTurnDAO().delDistrTurn(distrTurn);
                                table.repaint();
                            }
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
            {
                JButton button = new JButton("расчет по участкам обращени¤");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // JOptionPane.showMessageDialog(null,
                        // "не забывайте выполн¤ть расчеты по ж.д. участкам");
                        setCursor(LinkingLocMain.waitCursor);
                        DistrTurnUtil util = new DistrTurnUtil();
                        try {
                            List<DistrTurn> list = tableModel.getList();
                            util.calculate(list);
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        } finally {
                            setCursor(LinkingLocMain.defCursor);
                        }
                    }
                });
            }
            {
                JButton button = new JButton("печать");
                buttonPane.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        DistrTurnUtil util = new DistrTurnUtil();
                        if (tableModel.getList().size() > 0) {
                            String fileName = tableModel.getList().get(0).dirName;
                            util.exportDistrTurnToExcel(dirID, fileName);
                        }
                    }
                });
            }
        }
    }

    protected void selectRow() {
        int row = table.getSelectedRow();
        if (row > -1) {
            List<DistrTurn> list = tableModel.getList();
            if (list.size() > 0) {
                // это на случай когда список стал короче, чем ранее
                // выбранная строка в таблице
                if (row > list.size() - 1) {
                    row = 0;
                }
                DistrTurn dt = list.get(row);
                if (!dt.equals(selected)) {
                    selected = dt;
                    // другие действия при выборе участка в списке !!!!
                    rangePanel.setSelectedDistrTurn(selected);
                }
            }
        } else {
            // table.clearSelection();
            selected = null;
            // другие действия при выборе участка в списке !!!!
            rangePanel.setSelectedDistrTurn(selected);
        }
    }

    public int getDirID() {
        return dirID;
    }

    public void setDirID(int dirID) {
        this.dirID = dirID;
        fillListDistrTurn();
        table.clearSelection();
        selectRow();
    }

    public void fillListDistrTurn() {
        List<DistrTurn> list = LocDAOFactory.getInstance().getDistrTurnDAO().loadDistrTurn(dirID);
        Collections.sort(list);
        tableModel.setList(list);
        table.repaint();
    }

    public void loadRangeAll() {
        rangePanel.loadRangeAll();
    }

    private DistrTurnAEDialog getDialog() {
        return LinkingLocMain.mainFrame.distrTurnAEDialog;
    }

}
