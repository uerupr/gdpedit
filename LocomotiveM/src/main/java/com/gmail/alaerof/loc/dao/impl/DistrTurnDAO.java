package com.gmail.alaerof.loc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.DistrTurn;
import com.gmail.alaerof.loc.dao.dto.DistrTurnResult;
import com.gmail.alaerof.loc.dao.dto.RangeTR;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;

public class DistrTurnDAO {
    private static Logger logger = LogManager.getLogger(DistrTurnDAO.class);
    private Connection cnLocal;

    public DistrTurnDAO(Connection cnLocal) {
        this.cnLocal = cnLocal;
    }

    /**
     * обновление основных данных (название, направление, участок, станции)
     * @param dt участок обращения
     */
    public void saveDistrTurnHead(DistrTurn dt) {
        String sql = "UPDATE distrturn SET dirID =?, distrID=?, name=?, stBID=?, stEID=? WHERE distrTurnID=?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, dt.dirID);
            st.setInt(2, dt.distrID);
            st.setString(3, dt.name);
            st.setInt(4, dt.stBID);
            st.setInt(5, dt.stEID);
            st.setInt(6, dt.distrTurnID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * обновление дополнительных данных (вес, уклон, длина)
     * @param dt участок обращения
     */
    public void saveDistrTurnTail(DistrTurn dt) {
        String sql = "UPDATE distrturn SET weightforw=?, weightback=?, gradientforw=?, gradientback=?, lenght=?, num=? WHERE distrTurnID=?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, dt.weightforw);
            st.setInt(2, dt.weightback);
            st.setDouble(3, dt.gradientforw);
            st.setDouble(4, dt.gradientback);
            st.setDouble(5, dt.lenght);
            st.setInt(6, dt.num);
            st.setInt(7, dt.distrTurnID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * Добавление в БД с сохранением основных данных : название, направление,
     * участок, станции.
     * Заполняет distrTurnID для передаваемого параметра,
     * при успешном сохранении
     * @param dt участок обращения
     */
    public void addDistrTurn(DistrTurn dt) {
        boolean err = false;
        String sql = "INSERT INTO distrturn(dirID, distrID, name, stBID, stEID) VALUES(?,?,?,?,?)";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, dt.dirID);
            st.setInt(2, dt.distrID);
            st.setString(3, dt.name);
            st.setInt(4, dt.stBID);
            st.setInt(5, dt.stEID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            err = true;
        }
        if (!err) {
            dt.distrTurnID = getMaxDistrTurnID();
        }
    }
    /**
     * загружает из БД список участков оборота
     * @param sql "хвост" запроса с условиями
     * @return список
     */
    private List<DistrTurn> getListDistrTurn(String sql) {
        List<DistrTurn> list = new ArrayList<>();
        sql = "SELECT distrTurnID, dirID, distrID, name, stBID, stEID, weightforw, weightback, gradientforw, gradientback, lenght, num "
                + sql;
        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    DistrTurn item = new DistrTurn();
                    item.distrTurnID = rs.getInt("distrTurnID");
                    item.dirID = rs.getInt("dirID");
                    item.distrID = rs.getInt("distrID");
                    item.name = rs.getString("name").trim();
                    item.stBID = rs.getInt("stBID");
                    item.stEID = rs.getInt("stEID");
                    item.weightforw = rs.getInt("weightforw");
                    item.weightback = rs.getInt("weightback");
                    item.gradientforw = rs.getDouble("gradientforw");
                    item.gradientback = rs.getDouble("gradientback");
                    item.lenght = rs.getDouble("lenght");
                    item.num = rs.getInt("num");
                    list.add(item);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        fillDistrName(list);
        fillDirName(list);
        fillSt(list);
        return list;
    }
    /**
     * устанавливает название ж.д. участка для участков из списка
     * @param list список участков оборота
     */
    private void fillDistrName(List<DistrTurn> list) {
        String sql = "SELECT name FROM distr WHERE distrID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (DistrTurn dt : list) {
                st.setInt(1, dt.distrID);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        dt.distrName = rs.getString("name").trim();
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }
    /**
     * устанавливает названия станций для участков из списка
     * @param list список участков оборота
     */
    private void fillSt(List<DistrTurn> list) {
        StationDAO stDAO = LocDAOFactory.getInstance().getStationDAO();
        for (DistrTurn dt : list) {
            dt.stB = stDAO.getStationByID(dt.stBID);
            dt.stE = stDAO.getStationByID(dt.stEID);
        }
    }

    /**
     * устанавливает название НОД для участков из списка
     * @param list список участков оборота
     */
    private void fillDirName(List<DistrTurn> list) {
        String sql = "SELECT name FROM Direction WHERE dirID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (DistrTurn dt : list) {
                st.setInt(1, dt.dirID);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        dt.dirName = rs.getString("name").trim();
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * возвращает список ВСЕХ участков оборота
     * @return список
     */
    public List<DistrTurn> loadAllDistrTurn() {
        String sql = " FROM distrturn";
        return getListDistrTurn(sql);
    }

    /**
     * возвращает список участков обращения для НОД
     * @param dirID ид нода
     * @return список
     */
    public List<DistrTurn> loadDistrTurn(int dirID) {
        String sql = " FROM distrturn WHERE dirID = " + dirID;
        return getListDistrTurn(sql);
    }

    /**
     * добавление в БД заведомо новых участков оборота !!!
     * @param list список новых участков
     */
    public void addListDistrTurn(List<DistrTurn> list) {
        String sql = "INSERT INTO distrturn(dirID, distrID, name, stBID, stEID, weightforw, weightback, gradientforw, gradientback, lenght, num) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (DistrTurn dt : list) {
                st.setInt(1, dt.dirID);
                st.setInt(2, dt.distrID);
                st.setString(3, dt.name);
                st.setInt(4, dt.stBID);
                st.setInt(5, dt.stEID);
                st.setInt(6, dt.weightforw);
                st.setInt(7, dt.weightback);
                st.setDouble(8, dt.gradientforw);
                st.setDouble(9, dt.gradientback);
                st.setDouble(10, dt.lenght);
                st.setInt(11, dt.num);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * возвращает список диапазонов из БД
     * @param sql "хвост" запроса
     * @return список
     */
    private List<RangeTR> getListRange(String sql) {
        List<RangeTR> list = new ArrayList<>();
        sql = "SELECT rangeTRID, name, rangeValue " + sql;
        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    RangeTR item = new RangeTR();
                    item.rangeTRID = rs.getInt("rangeTRID");
                    item.name = rs.getString("name").trim();
                    String rangeValue = rs.getString("rangeValue").trim();
                    try {
                        item.setRangeValue(rangeValue);
                        list.add(item);
                    } catch (ParseException e) {
                        // хотя по-идее в БД не должны попасть некорректные
                        // диапазоны
                        logger.error(e.toString(), e);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**
     * возвращает список ВСЕХ диапазонов из БД
     * @return список диапазонов
     */
    public List<RangeTR> loadRangeAll() {
        List<RangeTR> list = getListRange(" FROM RangeTR order by name ");
        return list;
    }

    /**
     * возвращает для участка оборота список диапазонов  из БД 
     * @param distrTurnID код участка оборота
     * @return список диапазонов
     */
    public List<RangeTR> loadRangeDistrTurn(int distrTurnID) {
        String sql = " FROM RangeTR INNER JOIN DistrTurnRangeTR ON RangeTR.rangeTRID = DistrTurnRangeTR.rangeTRID "
                + " WHERE distrTurnID = " + distrTurnID;
        List<RangeTR> list = getListRange(sql);
        return list;
    }

    /**
     * сохранение в БД заведомо новых диапазонов
     * @param list список диапазонов
     */
    public void addListRange(List<RangeTR> list) {
        String sql = "INSERT INTO RangeTR(name, rangeValue) VALUES(?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (RangeTR dt : list) {
                String v = dt.getRangeValue();
                if (dt.isCorrect(v)) {
                    st.setString(1, dt.name);
                    st.setString(2, dt.getRangeValue());
                    st.addBatch();
                    count++;
                }
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }

    }

    /**
     * изменение в БД заведомо имеющихся диапазонов (ну скорее всего)
     * @param list список диапазонов
     */
    public void updateListRange(List<RangeTR> list) {
        String sql = "UPDATE RangeTR SET name = ?, rangeValue = ? WHERE rangeTRID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (RangeTR dt : list) {
                String v = dt.getRangeValue();
                if (dt.isCorrect(v)) {
                    st.setString(1, dt.name);
                    st.setString(2, dt.getRangeValue());
                    st.setInt(3, dt.rangeTRID);
                    st.addBatch();
                    count++;
                }
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * удаляет из БД диапазоны списка
     * @param list список диапазонов
     * @return количество удаленных записей
     */
    public int delListRange(List<RangeTR> list) {
        String sql = "DELETE FROM RangeTR WHERE rangeTRID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (RangeTR dt : list) {
                st.setInt(1, dt.rangeTRID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    /**
     * сохраняет список диапазонов в БД:
     * новые добавляет
     * имеющиеся обновляет
     * @param list список диапазонов
     */
    public void saveListRange(List<RangeTR> list) {
        List<RangeTR> listAdd = new ArrayList<>();
        List<RangeTR> listUpd = new ArrayList<>();
        for (RangeTR r : list) {
            if (r.rangeTRID > 0) {
                listUpd.add(r);
            } else {
                listAdd.add(r);
            }
        }
        addListRange(listAdd);
        updateListRange(listUpd);
    }

    /**
     * возвращает ID участка обращения, добавленного в БД последним или -1 при
     * его отсутствии
     * @return ID участка обращения
     */
    public int getMaxDistrTurnID() {
        String sql = "SELECT MAX(distrTurnID) FROM distrturn";
        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return -1;
    }

    /**
     * удаляет диапазоны для участка обращения
     * @param distrTurn участок обращения
     * @return количество удаленных записей
     */
    private int clearDistrTurnRangeTR(DistrTurn distrTurn) {
        String sql = "DELETE FROM DistrTurnRangeTR WHERE distrTurnID = ?";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, distrTurn.distrTurnID);
            count = st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return count;
    }

    /**
     * сохраняет список диапазонов для участка обращения, предварительно удалив их из БД
     * @param distrTurn участок обращения
     * @param list список диапазонов
     */
    public void saveListDistrTurnRangeTR(DistrTurn distrTurn, List<RangeTR> list) {
        clearDistrTurnRangeTR(distrTurn);
        String sql = "INSERT INTO DistrTurnRangeTR(distrTurnID, rangeTRID) VALUES(?,?)";
        int count = 0;
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            for (RangeTR dt : list) {
                st.setInt(1, distrTurn.distrTurnID);
                st.setInt(2, dt.rangeTRID);
                st.addBatch();
                count++;
            }
            if (count > 0) {
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * удаляет участок обращения из БД
     * @param distrTurn участок обращения
     */
    public void delDistrTurn(DistrTurn distrTurn) {
        String sql = "DELETE FROM distrturn WHERE distrTurnID = ?";
        try (PreparedStatement st = cnLocal.prepareStatement(sql)) {
            st.setInt(1, distrTurn.distrTurnID);
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * сохраняет результаты расчета по участку оборота в БД
     * @param listRes список результатов
     */
    public void saveDistrTurnResult(List<DistrTurnResult> listRes) {
        String sqlDel = "DELETE FROM DistrTurnResult WHERE distrTurnID = ?";
        String sqlIns = "INSERT INTO DistrTurnResult (distrTurnID, rangeTRID, seriesLocID, "
                + "countTrain_o, countTrain_e, countLoc_o, countLoc_e, "
                + "hourTT_o, hourTT_e, hourTU_o, hourTU_e, hourArr_o, hourArr_e, "
                + "hourDep_o, hourDep_e, hourEq_o, hourEq_e, trkm_o, trkm_e) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement psDel = cnLocal.prepareStatement(sqlDel);
            PreparedStatement psIns = cnLocal.prepareStatement(sqlIns);
            try {
                for (DistrTurnResult dtr : listRes) {
                    try {
                        psDel.setInt(1, dtr.distrTurnID);
                        psDel.executeUpdate();

                        psIns.setInt(1, dtr.distrTurnID);
                        psIns.setInt(2, dtr.rangeTRID);
                        psIns.setInt(3, dtr.seriesLocID);
                        psIns.setInt(4, dtr.countTrain_o);
                        psIns.setInt(5, dtr.countTrain_e);
                        psIns.setInt(6, dtr.countLoc_o);
                        psIns.setInt(7, dtr.countLoc_e);
                        psIns.setDouble(8, dtr.hourTT_o);
                        psIns.setDouble(9, dtr.hourTT_e);
                        psIns.setDouble(10, dtr.hourTU_o);
                        psIns.setDouble(11, dtr.hourTU_e);
                        psIns.setDouble(12, dtr.hourArr_o);
                        psIns.setDouble(13, dtr.hourArr_e);
                        psIns.setDouble(14, dtr.hourDep_o);
                        psIns.setDouble(15, dtr.hourDep_e);
                        psIns.setDouble(16, dtr.hourEq_o);
                        psIns.setDouble(17, dtr.hourEq_e);
                        psIns.setDouble(18, dtr.trkm_o);
                        psIns.setDouble(19, dtr.trkm_e);

                        psIns.executeUpdate();
                    } catch (SQLException e) {
                        logger.error(e.toString(), e);
                    }
                }
            } finally {
                psDel.close();
                psIns.close();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * возвращает список результатов расчета по участкам оборота для НОД,
     * попутно заполняя список участков оборота
     * @param dirID ID НОД
     * @param listDT список участков оборота для заполнения
     * @return список результатов расчета
     */
    public List<DistrTurnResult> getDistrTurnResult(int dirID, List<DistrTurn> listDT) {
        List<DistrTurnResult> list = new ArrayList<>();
        String sql = "SELECT DistrTurnResult.distrTurnID, DistrTurnResult.rangeTRID, DistrTurnResult.seriesLocID, "
                + "distrturn.name as nameDT, RangeTR.name as nameR, SeriesLoc.series, "
                + "countTrain_o, countTrain_e, countLoc_o, countLoc_e, "
                + "hourTT_o, hourTT_e, hourTU_o, hourTU_e, "
                + "hourDep_o, hourDep_e, hourArr_o, hourArr_e, hourEq_o, hourEq_e, trkm_o, trkm_e, "
                + "weightforw, weightback, gradientforw, gradientback, lenght "
                + "FROM ((DistrTurnResult Inner Join distrturn ON DistrTurnResult.distrTurnID = distrturn.distrTurnID) "
                + "Inner Join RangeTR ON DistrTurnResult.rangeTRID = RangeTR.rangeTRID) "
                + "Inner Join SeriesLoc ON DistrTurnResult.seriesLocID = SeriesLoc.seriesLocID "
                + "WHERE distrturn.dirID = " + dirID;

        try (Statement st = cnLocal.createStatement();) {
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    DistrTurnResult r = new DistrTurnResult();
                    list.add(r);
                    r.distrTurnID = rs.getInt("distrTurnID");
                    r.rangeTRID = rs.getInt("rangeTRID");
                    r.seriesLocID = rs.getInt("seriesLocID");
                    r.nameDT = rs.getString("nameDT").trim();
                    r.nameR = rs.getString("nameR").trim();
                    r.series = rs.getString("series").trim();
                    r.countTrain_o = rs.getInt("countTrain_o");
                    r.countTrain_e = rs.getInt("countTrain_e");
                    r.countLoc_o = rs.getInt("countLoc_o");
                    r.countLoc_e = rs.getInt("countLoc_e");
                    r.hourTT_o = rs.getDouble("hourTT_o");
                    r.hourTT_e = rs.getDouble("hourTT_e");
                    r.hourTU_o = rs.getDouble("hourTU_o");
                    r.hourTU_e = rs.getDouble("hourTU_e");
                    r.hourDep_o = rs.getDouble("hourDep_o");
                    r.hourDep_e = rs.getDouble("hourDep_e");
                    r.hourArr_o = rs.getDouble("hourArr_o");
                    r.hourArr_e = rs.getDouble("hourArr_e");
                    r.hourEq_o = rs.getDouble("hourEq_o");
                    r.hourEq_e = rs.getDouble("hourEq_e");
                    r.trkm_o = rs.getDouble("trkm_o");
                    r.trkm_e = rs.getDouble("trkm_e");
                    DistrTurn dt = new DistrTurn();
                    dt.distrTurnID = r.distrTurnID;
                    dt.weightforw = rs.getInt("weightforw");
                    dt.weightback = rs.getInt("weightback");
                    dt.gradientforw = rs.getDouble("gradientforw");
                    dt.gradientback = rs.getDouble("gradientback");
                    dt.lenght = rs.getDouble("lenght");
                    listDT.add(dt);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }
}
