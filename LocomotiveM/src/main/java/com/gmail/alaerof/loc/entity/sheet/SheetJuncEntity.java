package com.gmail.alaerof.loc.entity.sheet;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import com.gmail.alaerof.loc.dao.dto.Junc;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.linktrain.LinkTrainSheet;
import com.gmail.alaerof.loc.entity.train.TrainThread;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditPanel;

public class SheetJuncEntity {
    public static final int TOP = 1;
    public static final int BOTTOM = 2;
    public static final int BOTH = 3;
    private Junc junc;
    private int sheetJuncType; // 1 - begin, 2 - end, 3 - both\
    /** id участка сверху этого узла на листе */
    private int distrIDBefore;
    /** id участка снизу этого узла на листе */
    private int distrIDAfter;
    /** id узла примыкающего участка сверху на листе */
    private int juncIDBefore;
    /** id узла примыкающего участка снизу на листе */
    private int juncIDAfter;
    /** коорд. по Y первой линии сверху */
    private int y;
    /** общие настройки отображени¤ */
    private SheetConfig sConf;
    /** список ув¤зок */
    private List<LinkTrainSheet> listLink = new ArrayList<>();
    /** список ув¤зок дл¤ удалени¤ */
    private List<LinkTrainSheet> listDelLink = new ArrayList<>();
    /** список ув¤занных прибывающих поездов */
    private List<TrainThread> listTrOn = new ArrayList<>();
    /** список ув¤занных отправл¤ющихс¤ поездов */
    private List<TrainThread> listTrOff = new ArrayList<>();

    public SheetJuncEntity(Junc junc, SheetConfig sConf) {
        this.junc = junc;
        this.sConf = sConf;
    }

    public Junc getJunc() {
        return junc;
    }

    public int getSheetJuncType() {
        return sheetJuncType;
    }

    public void setSheetJuncType(int sheetJuncType) {
        this.sheetJuncType = sheetJuncType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((junc == null) ? 0 : junc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SheetJuncEntity other = (SheetJuncEntity) obj;
        if (junc == null) {
            if (other.junc != null)
                return false;
        } else if (junc.juncID != other.junc.juncID)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SheetJuncEntity [junc.name=" + junc.name + ", sheetJuncType=" + sheetJuncType + ", distrIDBefore="
                + distrIDBefore + ", distrIDAfter=" + distrIDAfter + "]";
    }

    /** id примыкающего участка сверху на листе */
    public int getDistrIDBefore() {
        return distrIDBefore;
    }

    /** id примыкающего участка сверху на листе */
    public void setDistrIDBefore(int distrIDBefore) {
        this.distrIDBefore = distrIDBefore;
    }

    /** id примыкающего участка снизу на листе */
    public int getDistrIDAfter() {
        return distrIDAfter;
    }

    /** id примыкающего участка снизу на листе */
    public void setDistrIDAfter(int distrIDAfter) {
        this.distrIDAfter = distrIDAfter;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /** расчет верт координаты заданной линии узла */
    public int getY(int lineNumber) {
        if (lineNumber < 1)
            lineNumber = 1;
        return y + (lineNumber - 1) * sConf.gapLine;
    }

    /** загрузка из БД списка увязок для узла */
    public void loadLinksFromDB(SheetGDPEditPanel sheetGDPEditPanel) {
        listTrOn = LocDAOFactory.getInstance().getDistrTrainDAO().getJuncTrainOn(junc.juncID, new TrainThread());
        listTrOff = LocDAOFactory.getInstance().getDistrTrainDAO().getJuncTrainOff(junc.juncID, new TrainThread());
        List<LinkTrain> list = LocDAOFactory.getInstance().getLinkTrainDAO().loadJuncLinkTrain(junc.juncID);
        listLink.clear();
        for (LinkTrain lt : list) {
            LinkTrainSheet lts = new LinkTrainSheet(sheetGDPEditPanel, this, lt);
            listLink.add(lts);
        }
    }

    /** построение и отображение списка увязок */
    public void drawGDP(SheetGDPEditPanel sheetGDPEditPanel) {
        // этот метод вызывается после отображени¤ участков и поездов. т.е. к
        // моменту вызова метода, все имеющиес¤ поезда на листе уже загружены и
        // отображены!!!
        if (listLink.size() != 0) {
            for (int i = 0; i < listLink.size(); i++) {
                LinkTrainSheet lt = listLink.get(i);
                lt.removeFromOwner();
            }
            sheetGDPEditPanel.repaint();
        }
        loadLinksFromDB(sheetGDPEditPanel);
        for (int i = 0; i < listLink.size(); i++) {
            LinkTrainSheet lt = listLink.get(i);
            lt.buildLink();
        }
    }

    /** возвращает список увязанных, прибывающих в узел поездов */
    public List<TrainThread> getListTrOn() {
        return listTrOn;
    }

    /** возвращает список увязанных отправляющихся из узла поездов */
    public List<TrainThread> getListTrOff() {
        return listTrOff;
    }

    /** возвращает поезд из прибывающих по номеру+название */
    public TrainThread getTrainOn(int codeTR, String nameTR) {
        for (TrainThread tr : listTrOn) {
            String name = tr.getDistrTrain().nameTR.trim();
            nameTR = nameTR.trim();
            if (tr.getDistrTrain().codeTR == codeTR && nameTR.equals(name)) {
                return tr;
            }
        }
        return null;
    }

    /** возвращает поезд из отправляющихся по номеру+название */
    public TrainThread getTrainOff(int codeTR, String nameTR) {
        for (TrainThread tr : listTrOff) {
            String name = tr.getDistrTrain().nameTR.trim();
            nameTR = nameTR.trim();
            if (tr.getDistrTrain().codeTR == codeTR && nameTR.equals(name)) {
                return tr;
            }
        }
        return null;
    }

    public void saveGDP() {
        for (int i = 0; i < listDelLink.size(); i++) {
            LinkTrainSheet lt = listDelLink.get(i);
            lt.deleteLink();
        }
        listDelLink.clear();
        for (int i = 0; i < listLink.size(); i++) {
            LinkTrainSheet lt = listLink.get(i);
            lt.saveLink();
        }
    }
    
    /** помещает увязку в список для удаления */
    public void markLinkForDelete(LinkTrainSheet lt){
        lt.removeFromOwner();
        listLink.remove(lt);
        listDelLink.add(lt);
        
    }

    /** id узла примыкающего участка сверху на листе */
    public int getJuncIDBefore() {
        return juncIDBefore;
    }

    /** id узла примыкающего участка сверху на листе */
    public void setJuncIDBefore(int juncIDBefore) {
        this.juncIDBefore = juncIDBefore;
    }

    /** id узла примыкающего участка снизу на листе */
    public int getJuncIDAfter() {
        return juncIDAfter;
    }

    /** id узла примыкающего участка снизу на листе */
    public void setJuncIDAfter(int juncIDAfter) {
        this.juncIDAfter = juncIDAfter;
    }

    /**
     * отрисовка увязок на канве
     * @param grGDP канва
     */
    public void drawGDPImage(Graphics grGDP, int gapLeft, int gapTop) {
        for (int i = 0; i < listLink.size(); i++) {
            LinkTrainSheet lt = listLink.get(i);
            lt.drawInImage(grGDP, gapLeft, gapTop);
        }
        
    }

}
