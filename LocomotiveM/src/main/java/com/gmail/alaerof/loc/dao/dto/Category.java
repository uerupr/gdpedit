package com.gmail.alaerof.loc.dao.dto;

public class Category  extends AbstractDTO implements Comparable<Category> {
    public int categoryID;
    public String name;
    public int prior;
    public int trMin;
    public int trMax;
    public String type;

    @Override
    public int getID() {
        return categoryID;
    }
    
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + categoryID;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + prior;
        result = prime * result + trMax;
        result = prime * result + trMin;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Category other = (Category) obj;
        if (categoryID != other.categoryID)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (prior != other.prior)
            return false;
        if (trMax != other.trMax)
            return false;
        if (trMin != other.trMin)
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }


        @Override
    public String toString() {
        return "category [categoryID=" + categoryID + ", name=" + name + ", prior=" + prior + ", trMin=" + trMin
                + ", trMax=" + trMax + ", type=" + type + "]";
    }


        @Override
        public int compareTo(Category o) {
            return this.trMin - o.trMin;
        }
}
