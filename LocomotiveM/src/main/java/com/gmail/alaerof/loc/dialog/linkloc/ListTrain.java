package com.gmail.alaerof.loc.dialog.linkloc;

import java.util.List;

import com.gmail.alaerof.loc.entity.linktrain.LinkTrainEntity;
import com.gmail.alaerof.loc.entity.train.TrainThread;

public interface ListTrain {
    public List<TrainThread> getList();
    public void setList(List<TrainThread> list);
    public List<LinkTrainEntity> getListLinkTrain();
    public void setListLinkTrain(List<LinkTrainEntity> listLinkTrain);
    public LinkTrainEntity getLinkTrainEntity(int row);
    public void deleteLinkTrain(int row);
    public boolean isArrive();
}
