package com.gmail.alaerof.loc.dialog.sheet;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.BoxLayout;

import com.gmail.alaerof.loc.dao.dto.Distr;
import com.gmail.alaerof.loc.dao.dto.Station;
import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistr;
import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.DistrDAO;
import com.gmail.alaerof.loc.dao.impl.SheetDAO;
import com.gmail.alaerof.loc.dao.impl.StationDAO;
import com.gmail.alaerof.loc.entity.sheet.SheetDistrEntity;
import com.gmail.alaerof.loc.entity.sheet.SheetEntity;

import java.awt.Dimension;

public class SheetDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JComboBox<Distr> distrSelect;
    private JTextField distrNum;
    private JComboBox<Station> stSelect;
    private JTextField stNum;
    private JTable distrTable;
    private JTable stTable;
    private ListSDTableModel distrTableModel;
    private ListSDSTableModel stTableModel;
    private List<SheetDistrEntity> distrList = new ArrayList<>();
    private SheetDistrEntity selectedDistr;
    private JTextField nameSheet;
    private boolean editMode = false;
    private SheetEntity sheetEntity;

    public SheetDialog() {
        initContent();
    }

    public SheetDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private void initContent() {
        setBounds(0, 0, 1100, 450);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        setLocationRelativeTo(null);
        JPanel panelSheet = new JPanel();
        JPanel panelDistr = new JPanel();
        JPanel panelSt = new JPanel();
        panelSheet.add(new JLabel("Название листа:"));
        nameSheet = new JTextField(20);
        panelSheet.add(nameSheet);
        contentPanel.add(panelSheet);
        contentPanel.add(panelDistr);
        contentPanel.add(panelSt);

        panelDistr.setLayout(new BorderLayout(0, 0));
        JScrollPane panelDistrList = new JScrollPane();
        panelDistr.add(panelDistrList, BorderLayout.CENTER);
        {
            distrTableModel = new ListSDTableModel();
            distrTableModel.setList(distrList);
            distrTable = new JTable(distrTableModel);
            panelDistrList.setViewportView(distrTable);
            distrTable.setFillsViewportHeight(true);
            distrTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 35, 165, 40, 60, 60 };
            com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(distrTable, colSize);

            distrTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent ev) {
                    int row = distrTable.getSelectedRow();
                    if (row > -1) {
                        SheetDistrEntity di = distrList.get(row);
                        if (!di.equals(selectedDistr)) {
                            selectedDistr = di;
                            fillStSelect(selectedDistr.getSheetDistr().distrID);
                            List<SheetDistrSt> listSt = selectedDistr.getListSt();
                            Collections.sort(listSt);
                            stTableModel.setList(listSt);
                            stTable.repaint();
                        }
                    }
                }
            });

            distrTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    int row = distrTable.getSelectedRow();
                    if (row > -1) {
                        SheetDistrEntity di = distrList.get(row);
                        if (e.getClickCount() == 1) {
                            if (!di.equals(selectedDistr)) {
                                selectedDistr = di;
                                fillStSelect(selectedDistr.getSheetDistr().distrID);
                                stTableModel.setList(selectedDistr.getListSt());
                                stTable.repaint();
                            }

                        } else {
                            if (e.getClickCount() == 2) {
                                int col = distrTable.getSelectedColumn();
                                switch (col) {
                                case 2:
                                    di.getSheetDistr().evenDown = !di.getSheetDistr().evenDown;
                                    break;
                                case 3:
                                    di.getSheetDistr().hideJuncB = !di.getSheetDistr().hideJuncB;
                                    break;
                                case 4:
                                    di.getSheetDistr().hideJuncE = !di.getSheetDistr().hideJuncE;
                                    break;
                                default:
                                    break;
                                }
                                distrTable.repaint();
                            }
                        }
                    }
                }
            });

        }
        JPanel panelDistrSelect = new JPanel();
        panelDistrSelect.setPreferredSize(new Dimension(280, 10));
        panelDistr.add(panelDistrSelect, BorderLayout.EAST);
        panelDistrSelect.setLayout(new BoxLayout(panelDistrSelect, BoxLayout.Y_AXIS));
        JPanel container = panelDistrSelect;
        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEFT);
            panel.add(new JLabel("Участок"));
            container.add(panel);
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
            distrSelect = new JComboBox<Distr>();
            panel.add(distrSelect);
            container.add(panel);
            fillDistrSelect();
            // distrSelect.setEditable(true);
        }
        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEFT);

            distrNum = new JTextField(5);
            panel.add(distrNum);
            panel.add(new JLabel("Номер участка"));
            container.add(panel);
        }
        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEFT);
            JButton addDistr = new JButton("<<");
            panel.add(addDistr);
            addDistr.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    Distr distr = (Distr) distrSelect.getSelectedItem();
                    if (distr != null) {
                        SheetDistrEntity di = new SheetDistrEntity();
                        SheetDistr shd = new SheetDistr();
                        shd.distrID = distr.distrID;
                        shd.distrName = distr.name;
                        di.setSheetDistr(shd);
                        di.setListSt(new ArrayList<SheetDistrSt>());
                        try {
                            shd.num = Integer.parseInt(distrNum.getText());
                        } catch (Exception e) {
                            shd.num = 0;
                        }
                        if (!distrList.contains(di))
                            distrList.add(di);
                        Collections.sort(distrList);
                        // distrTableModel.setList(distrList);
                        distrTable.repaint();
                    }
                }
            });
            JButton removeDistr = new JButton(">>");
            panel.add(removeDistr);
            container.add(panel);
            removeDistr.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int index = distrTable.getSelectedRow();
                    if (index > -1) {
                        distrList.remove(index);
                        distrTable.repaint();
                    }
                }
            });
        }

        panelSt.setLayout(new BorderLayout(0, 0));
        JScrollPane panelStList = new JScrollPane();
        panelSt.add(panelStList, BorderLayout.CENTER);
        {
            stTableModel = new ListSDSTableModel();
            stTable = new JTable(stTableModel);
            stTable.setFillsViewportHeight(true);
            stTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 35, 140, 40, 25, 140, 65 };
            com.gmail.alaerof.loc.util.TableUtils.setTableColumnSize(stTable, colSize);
            panelStList.setViewportView(stTable);

            stTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    int row = stTable.getSelectedRow();
                    int col = stTable.getSelectedColumn();
                    if (row > -1) {
                        SheetDistrSt di = stTableModel.getList().get(row);
                        if (e.getClickCount() == 2) {
                            if (col == 3)
                                di.hidden = !di.hidden;
                            if (col == 5)
                                di.showVisibleName = !di.showVisibleName;
                            stTable.repaint();
                        }
                    }
                }
            });

        }
        JPanel panelStSelect = new JPanel();
        panelStSelect.setPreferredSize(new Dimension(280, 10));
        panelSt.add(panelStSelect, BorderLayout.EAST);
        panelStSelect.setLayout(new BoxLayout(panelStSelect, BoxLayout.Y_AXIS));
        container = panelStSelect;
        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEFT);
            panel.add(new JLabel("Станция"));
            container.add(panel);
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
            stSelect = new JComboBox<Station>();
            panel.add(stSelect);
            container.add(panel);
        }
        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEFT);

            stNum = new JTextField(5);
            panel.add(stNum);
            panel.add(new JLabel("Номер станции"));
            container.add(panel);
        }
        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEFT);
            JButton addSt = new JButton("<<");
            panel.add(addSt);
            addSt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    Station st = (Station) stSelect.getSelectedItem();
                    if (selectedDistr != null && st != null) {
                        SheetDistrSt di = new SheetDistrSt();
                        di.stationID = st.stationID;
                        di.stationName = st.name;
                        try {
                            di.num = Integer.parseInt(stNum.getText());
                        } catch (Exception e) {
                            di.num = 0;
                        }
                        if (!selectedDistr.getListSt().contains(di))
                            selectedDistr.getListSt().add(di);
                        stTable.repaint();
                    }
                }
            });
            JButton removeSt = new JButton(">>");
            panel.add(removeSt);
            container.add(panel);
            removeSt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    int index = stTable.getSelectedRow();
                    if (index > -1) {
                        stTableModel.getList().remove(index);
                        stTable.repaint();
                    }
                }
            });
        }

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog dialog = this;
            {
                //buttonPane.add(new JLabel("* количество линий для увязки локомотивов"));
                JButton okButton = new JButton("Применить");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LocDAOFactory f = LocDAOFactory.getInstance();
                        SheetDAO sDAO = f.getSheetDAO();
                        String name = nameSheet.getText().trim();
                        if (!editMode) {
                            if (!"".equals(name)) {
                                sDAO.addSheet(name, distrList);
                            }
                        } else {
                            if (!"".equals(name)) {
                                sheetEntity.getSheet().name = name;
                                sDAO.updateSheet(sheetEntity);
                            }
                        }
                        dialog.setVisible(false);
                    }
                });
            }
            {
                JButton cancelButton = new JButton("Отмена");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dialog.setVisible(false);
                    }
                });
            }
        }
    }

    private void fillDistrSelect() {
        LocDAOFactory f = LocDAOFactory.getInstance();
        DistrDAO distrDAO = f.getDistrDAO();
        List<Distr> list = distrDAO.loadAllDistr();
        distrSelect.removeAllItems();
        for (int i = 0; i < list.size(); i++) {
            distrSelect.addItem(list.get(i));
        }
    }

    private void fillStSelect(int distrID) {
        LocDAOFactory f = LocDAOFactory.getInstance();
        StationDAO stDAO = f.getStationDAO();
        List<Station> list = stDAO.getDistrStation(distrID);
        stSelect.removeAllItems();
        for (int i = 0; i < list.size(); i++) {
            stSelect.addItem(list.get(i));
        }

    }

    public SheetEntity getSheetEntity() {
        return sheetEntity;
    }

    public void setSheetEntity(SheetEntity sheetEntity) {
        this.sheetEntity = sheetEntity;
        editMode = sheetEntity != null;
        selectedDistr = null;
        stTableModel.setList(null);
        stTable.repaint();
    }

    public void fillContent() {
        if (sheetEntity != null) {
            setTitle("Изменение листа");
            nameSheet.setText(sheetEntity.toString());
            distrList = sheetEntity.getListDistr();
            distrTableModel.setList(distrList);
            distrTable.repaint();
        } else {
            setTitle("Добавление листа");
        }
    }
}
