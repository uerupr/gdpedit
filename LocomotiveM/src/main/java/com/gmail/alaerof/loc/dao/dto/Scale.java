package com.gmail.alaerof.loc.dao.dto;

public class Scale extends AbstractDTO{
    public int scaleID;
    public int distrID;
    public int spanID;
    public int num;
    public int L;
    public double absKM;
    public double absKM_e;
    
    @Override
    public String toString() {
        return "Scale [scaleID=" + scaleID + ", distrID=" + distrID + ", spanID=" + spanID + ", num=" + num + "]";
    }

    @Override
    public int getID() {
        return scaleID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + L;
        long temp;
        temp = Double.doubleToLongBits(absKM);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(absKM_e);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + distrID;
        result = prime * result + num;
        result = prime * result + scaleID;
        result = prime * result + spanID;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Scale other = (Scale) obj;
        if (L != other.L)
            return false;
        if (Double.doubleToLongBits(absKM) != Double.doubleToLongBits(other.absKM))
            return false;
        if (Double.doubleToLongBits(absKM_e) != Double.doubleToLongBits(other.absKM_e))
            return false;
        if (distrID != other.distrID)
            return false;
        if (num != other.num)
            return false;
        if (scaleID != other.scaleID)
            return false;
        if (spanID != other.spanID)
            return false;
        return true;
    }
    
    
    
}
