package com.gmail.alaerof.loc.entity.train;

import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.dao.dto.train.TrainStation;
import com.gmail.alaerof.loc.entity.sheet.SheetConfig;

public class TimeStation {
    /** расписание */
    private TrainStation trainSt;
    /** станция участка */
    private SheetDistrSt distrSt;
    /** прибытие в минутах */
    int mOn;
    /** отправление в минутах */
    int mOff;
    /** коорд по х прибытия на станцию */
    int xOn;
    /** коорд по х отправления со станции */
    int xOff;
    /** прибытие в минутах */
    int mOnTexStop;
    /** отправление в минутах */
    int mOffTexStop;
    /** коорд по х прибытия на станцию */
    int xOnTexStop;
    /** коорд по х отправления со станции */
    int xOffTexStop;
    /** порядковый номер среди р.п. для поезда по ходу движения, начинается с 0 */
    int order;
    /** признак последней по ходу движения */
    private boolean last;
    /** подпись наклонной линии после станции */
    int showCode = -1;
    // -1 - нет подписи;
    // 0 - Code (номер);
    // 1 - AI_all (общая надпись);
    // 2 - AI_sp (личная надпись);
    

    public TimeStation(TrainStation trainSt, SheetDistrSt distrSt, int order) {
        super();
        this.trainSt = trainSt;
        this.distrSt = distrSt;
        this.order = order;
        mOn = trainSt.getTimeOnChecked();
        mOff = trainSt.getTimeOffChecked();
        mOnTexStop = mOn;
        mOffTexStop = mOff;

        if (order == 0) {
            mOnTexStop = mOn - trainSt.texStop;
        }
    }

    /** расчет горизонтальных координат прибытия и отправления */
    public void calcX(SheetConfig sConf) {
        int x0 = sConf.ltGDP.x;
        xOn = x0 + mOn * sConf.picTimeHor;
        xOff = x0 + mOff * sConf.picTimeHor;
        xOnTexStop = x0 + mOnTexStop * sConf.picTimeHor;
        xOffTexStop = x0 + mOffTexStop * sConf.picTimeHor;
    }

    public TrainStation getTrainSt() {
        return trainSt;
    }

    public SheetDistrSt getDistrSt() {
        return distrSt;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
        if (last) {
            mOffTexStop = mOff + trainSt.texStop;
        }

    }

    /**
     * рассчетные координаты линии станции в пикселах c учетом отступа сверху на
     * все (с масштабированием)
     * 
     * @return координата по Y
     */
    public int getY1() {
        return distrSt.getY1();
    }

    public int getY0() {
        return distrSt.getY0();
    }

}
