package com.gmail.alaerof.loc.entity.train;

import java.util.List;

public class TrainThreadListUtil {
    /**
     * Ищет поезд в списке list по номеру code начиная с позиции start.
     * @param list список поездов
     * @param start начальная позиция поиска
     * @param code номер поезда
     * @return TrainThread Возвращает первый попавшийся или Null
     */
    public static TrainThreadSheet getTrainByCode(List<TrainThreadSheet> list, int start, int code) {
        for (int i = start; i < list.size(); i++) {
            TrainThreadSheet tr = list.get(i);
            int codeTR = tr.getCodeINT();
            if ( codeTR == code) {
                return tr;
            }
        }
        for (int i = 0; i < start; i++) {
            TrainThreadSheet tr = list.get(i);
            int codeTR = tr.getCodeINT();
            if ( codeTR == code) {
                return tr;
            }
        }
        return null;
    }
}
