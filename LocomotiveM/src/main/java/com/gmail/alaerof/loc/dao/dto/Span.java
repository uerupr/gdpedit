package com.gmail.alaerof.loc.dao.dto;

public class Span extends AbstractDTO{
    public int spanID;
    public int dirID;
    public String name;
    public String SCB;
    public int line;
    public int stB;
    public int stE;
    public boolean br;
    
    @Override
    public String toString() {
        return "Span [spanID=" + spanID + ", dirID=" + dirID + ", name=" + name + "]";
    }

    @Override
    public int getID() {
        return spanID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((SCB == null) ? 0 : SCB.hashCode());
        result = prime * result + (br ? 1231 : 1237);
        result = prime * result + dirID;
        result = prime * result + line;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + spanID;
        result = prime * result + stB;
        result = prime * result + stE;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Span other = (Span) obj;
        if (SCB == null) {
            if (other.SCB != null)
                return false;
        } else if (!SCB.equals(other.SCB))
            return false;
        if (br != other.br)
            return false;
        if (dirID != other.dirID)
            return false;
        if (line != other.line)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (spanID != other.spanID)
            return false;
        if (stB != other.stB)
            return false;
        if (stE != other.stE)
            return false;
        return true;
    }
    
    
    
}
