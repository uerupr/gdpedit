package com.gmail.alaerof.loc.mainframe.sheettree;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.loc.dao.dto.sheet.SheetDistrSt;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.dao.impl.SheetDAO;
import com.gmail.alaerof.loc.entity.sheet.SheetDistrEntity;
import com.gmail.alaerof.loc.entity.sheet.SheetEntity;
import com.gmail.alaerof.loc.application.LinkingLoc;
import com.gmail.alaerof.loc.application.LinkingLocMain;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditTabPane;

public class SheetTree {
    private static Logger logger = LogManager.getLogger(SheetTree.class);
    private JTree jtree;
    private JScrollPane content;
    private TreePopupMenuSheet popupSheet = new TreePopupMenuSheet();
    private TreePopupMenuDistr popupDistr = new TreePopupMenuDistr();
    private Object userObject;
    private SheetGDPEditTabPane sheetGDPEditTabPane;

    public SheetTree(SheetGDPEditTabPane sheetGDPEditTabPane) {
        this.sheetGDPEditTabPane = sheetGDPEditTabPane;
        updateTree();
    }

    public JScrollPane updateTree() {
        content = new JScrollPane();
        DefaultMutableTreeNode top = new DefaultMutableTreeNode("+");
        createNodes(top);
        jtree = new JTree(top);
        jtree.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent evt) {
                if (jtree.getRowForLocation(evt.getX(), evt.getY()) != -1) {
                    TreePath curPath = jtree.getPathForLocation(evt.getX(), evt.getY());
                    String comp = curPath.getLastPathComponent().toString();
                    jtree.setToolTipText(comp);
                }
            }

        });

        jtree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        content.setViewportView(jtree);
        jtree.setRootVisible(false);

        jtree.addTreeSelectionListener(new SheetTreeSelectionListener());
        jtree.addMouseListener(new SheetTreeMouseAdapter());
        jtree.addMouseListener(new PopupListener());
        return content;
    }

    public JScrollPane getTreePanel() {
        return content;
    }

    public JTree getTree() {
        return jtree;
    }
    
    public Object getSelectedUserObject(){
        return userObject;
    }
    
    private void createNodes(DefaultMutableTreeNode top) {
        DefaultMutableTreeNode sheetNode = null;
        DefaultMutableTreeNode distrNode = null;
        DefaultMutableTreeNode stNode = null;
        try {
            LocDAOFactory f = LocDAOFactory.getInstance();
            SheetDAO sheetDAO = f.getSheetDAO();

            // каждый лист состоит из собственных объектов (участки и станции)
            List<SheetEntity> listSheet = sheetDAO.loadAllSheet();

            for (int i = 0; i < listSheet.size(); i++) {
                SheetEntity sheet = listSheet.get(i);
                sheetNode = new DefaultMutableTreeNode(sheet);
                top.add(sheetNode);
                List<SheetDistrEntity> listDistr = sheet.getListDistr();
                for (int j = 0; j < listDistr.size(); j++) {
                    SheetDistrEntity distr = listDistr.get(j);
                    distrNode = new DefaultMutableTreeNode(distr);
                    sheetNode.add(distrNode);
                    List<SheetDistrSt> listOpt = distr.getListSt();
                    for (int k = 0; k < listOpt.size(); k++) {
                        stNode = new DefaultMutableTreeNode(listOpt.get(k));
                        distrNode.add(stNode);
                    }
                }
            }

        } catch (Exception e) {
            logger.error(e.toString(), e);
        }

    }

    private class SheetTreeMouseAdapter extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            int selRow = jtree.getRowForLocation(e.getX(), e.getY());
            TreePath selPath = jtree.getPathForLocation(e.getX(), e.getY());
            if (selRow != -1) {
                // двойной клик
                if (e.getClickCount() == 2) {
                    LinkingLocMain.mainFrame.setCursorWait();
                    try {
                        // получаем объект - участок
                        DefaultMutableTreeNode node = (DefaultMutableTreeNode) selPath.getLastPathComponent();
                        Object obj = node.getUserObject();
                        LinkingLoc.statusBar.setMessage(obj.getClass().getName());
                        if (obj instanceof SheetEntity) {
                            SheetEntity se = ((SheetEntity) obj);
                            // активируем соотв. закладку ГДП
                            // или добавляем и активируем новую закладку ГДП
                            sheetGDPEditTabPane.addEditTab(se);
                            LinkingLoc.statusBar.setMessage(se.toString());
                        }
                    } finally {
                        LinkingLocMain.mainFrame.setCursorDefault();
                    }
                }
            }

        }
    }

    private class SheetTreeSelectionListener implements TreeSelectionListener {
        @Override
        public void valueChanged(TreeSelectionEvent arg0) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) jtree.getLastSelectedPathComponent();

            if (node == null) {
                userObject = null;
                return;
            }
            // одиночный клик - просто инфа
            userObject = node.getUserObject();
            String info = userObject.toString();
            if (userObject instanceof SheetDistrEntity) {
                SheetDistrEntity distrEditEntity = (SheetDistrEntity) node.getUserObject();
                info = distrEditEntity.getSourceGDP();
            }
            LinkingLoc.statusBar.setMessage(info);
        }
    }

    class PopupListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            if (userObject != null && e.isPopupTrigger()) {
                String className = userObject.getClass().getName();
                if ("com.gmail.alaerof.loc.entity.sheet.SheetEntity".equals(className)) {
                    popupSheet.setSheetEntity((SheetEntity)userObject);
                    popupSheet.show(e.getComponent(), e.getX(), e.getY());
                }
                if ("com.gmail.alaerof.loc.entity.sheet.SheetDistrEntity".equals(className)) {
                    popupDistr.setSheetDistrEntity((SheetDistrEntity)userObject);
                    popupDistr.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        }
    }

}
