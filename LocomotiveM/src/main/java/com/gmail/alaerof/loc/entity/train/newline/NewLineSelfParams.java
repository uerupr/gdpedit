package com.gmail.alaerof.loc.entity.train.newline;

import java.awt.Color;

public class NewLineSelfParams {
    /** цвет линии */
    public Color color = Color.blue;
    /** стиль линии */
    public LineStyle lineStyle = LineStyle.Solid;
    public int lineWidth = 1;
    /** подпись линии (перегон - центр) */
    private String captionValue = "----";
    /** всплывающий текст линии */
    private String toolTipText;
    /** надпись - время прибытия */
    public String timeA = "";
    /** надпись - время отправления */
    public String timeL = "";

    public String getCaptionValue() {
        return captionValue;
    }

    public void setCaptionValue(String captionValue) {
        if (captionValue != null)
            this.captionValue = captionValue;
    }

    public String getToolTipText() {
        if (toolTipText == null) {
            return captionValue;
        }
        return toolTipText;
    }

    public void setToolTipText(String toolTipText) {
        this.toolTipText = toolTipText;
    }

}
