package com.gmail.alaerof.loc.entity.train;

import java.util.Comparator;

import com.gmail.alaerof.loc.dao.dto.train.TrainStation;

public class TrainStComparator implements Comparator<TrainStation> {
    @Override
    public int compare(TrainStation st1, TrainStation st2) {
        int st1Off = st1.getTimeOffChecked();
        int st2On = st2.getTimeOnChecked();

        boolean f1 = (st1Off < st2On);
        boolean f2 = ((st2On - st1Off) <= 12 * 60);
        boolean f3 = (st1Off > st2On);
        boolean f4 = ((st1Off - st2On) > 12 * 60);

        if (f1 && f2 || f3 && f4)
            return -1;
        else
            return 1;
    }

}
