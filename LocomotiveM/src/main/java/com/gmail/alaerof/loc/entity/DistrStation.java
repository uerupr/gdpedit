package com.gmail.alaerof.loc.entity;

import com.gmail.alaerof.loc.dao.dto.Station;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
/**
 * Класс бизнесс-объекта (сущности) "ж.д. станция".
 * Содержит: 
 * - информацию о станции (Station)
 * 
 * @author Helen Yrofeeva
 *
 */
public class DistrStation {
    private Station station;
    /**
     * в конструкторе происходит загрузка станции из БД
     * @param idSt ID станции
     */
    public DistrStation(int idSt) {
        //this.station;
        LocDAOFactory f = LocDAOFactory.getInstance();
        station = f.getStationDAO().getStationByID(idSt);
    }

    public Station getStation() {
        return station;
    }

}
