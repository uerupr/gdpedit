package com.gmail.alaerof.loc.mainframe.sheettree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.gmail.alaerof.loc.application.LinkingLoc;
import com.gmail.alaerof.loc.application.LinkingLocMain;

public class ActionListenerAddSheet implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        LinkingLoc f= LinkingLocMain.mainFrame;
        f.sheetDialog.setSheetEntity(null);
        f.sheetDialog.fillContent();
        f.sheetDialog.setVisible(true);
        f.mainPaneBuilder.updateTree();
    }

}
