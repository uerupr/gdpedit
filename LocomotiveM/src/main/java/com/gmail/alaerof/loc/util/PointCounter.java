package com.gmail.alaerof.loc.util;

import com.gmail.alaerof.loc.dialog.netbr.DistrLine;
import com.gmail.alaerof.loc.dialog.netbr.DistrLineLeft;
import com.gmail.alaerof.loc.dialog.netbr.DistrLineRight;

public class PointCounter {
    public static class PointRect {
        public int x;
        public int y;
        public int w;
        public int h;
        public DistrLine dl;
    }

    public static PointRect getRectPoints(int x1, int y1, int x2, int y2) {
        PointRect pr = new PointRect();
        pr.x = Math.min(x1, x2);
        pr.y = Math.min(y1, y2);
        int brx = Math.max(x1, x2);
        int bry = Math.max(y1, y2);
        pr.w = brx - pr.x;
        pr.h = bry - pr.y;
        boolean left = (pr.x == x1 && pr.y == y1) || (pr.x == x2 && pr.y == y2);
        if (left) {
            pr.dl = new DistrLineLeft();
        } else {
            pr.dl = new DistrLineRight();
        }

        return pr;
    }
}
