package com.gmail.alaerof.loc.entity.linktrain;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import com.gmail.alaerof.loc.dao.dto.train.LinkTrain;
import com.gmail.alaerof.loc.dao.factory.LocDAOFactory;
import com.gmail.alaerof.loc.entity.sheet.SheetConfig;
import com.gmail.alaerof.loc.entity.sheet.SheetJuncEntity;
import com.gmail.alaerof.loc.entity.train.TrainThread;
import com.gmail.alaerof.loc.entity.train.TrainThreadElement;
import com.gmail.alaerof.loc.entity.train.TrainThreadGeneralParams;
import com.gmail.alaerof.loc.entity.train.TrainThreadSheet;
import com.gmail.alaerof.loc.entity.train.newline.LineHor;
import com.gmail.alaerof.loc.entity.train.newline.LineStyle;
import com.gmail.alaerof.loc.entity.train.newline.LineVert;
import com.gmail.alaerof.loc.entity.train.newline.NewLine;
import com.gmail.alaerof.loc.entity.train.newline.NewLineSelfParams;
import com.gmail.alaerof.loc.entity.train.newline.NewLine.DrawCaption;
import com.gmail.alaerof.loc.mainframe.sheetgdpedit.SheetGDPEditPanel;

public class LinkTrainSheet extends TrainThreadElement {
    protected static Logger logger = LogManager.getLogger(LinkTrainSheet.class);
    private LinkTrain linkTrain;
    /** нитка поезда прибытия из БД (горизонтальные координаты [время!!!]) */
    private TrainThread trainOn;
    /** нитка поезда отправления из БД (горизонтальные координаты [время!!!]) */
    private TrainThread trainOff;
    /** нитка поезда прибытия на листе (вертикальные координаты) */
    private TrainThreadSheet trainSheetOn;
    /** нитка поезда отправления на листе (вертикальные координаты) */
    private TrainThreadSheet trainSheetOff;
    private SheetJuncEntity sheetJuncEntity;
    private SheetGDPEditPanel sheetGDPEditPanel;

    /** гор линия стоянки прибытия */
    private NewLine lineHor1;
    /** вторая половина, если есть переход на след.сутки) */
    private NewLine lineHor2;
    /** верт линия к поезду прибытия */
    private NewLine lineOn;
    /** верт линия к поезду отправления */
    private NewLine lineOff;
    /** времемя прибытия и отправления увязанных поездов */
    private TimeJunc timeJunc;
   
    private boolean selected;
    
    private NewLineSelfParams selfParamsHor;
    private NewLineSelfParams selfParamsVert;

    public LinkTrainSheet(SheetGDPEditPanel sheetGDPEditPanel, SheetJuncEntity sheetJuncEntity,
            LinkTrain linkTrain) {
        super(sheetGDPEditPanel.getGDPOwner(), null, sheetGDPEditPanel.getSheetEntity().getSheetConfig());
        this.linkTrain = linkTrain;
        this.sheetJuncEntity = sheetJuncEntity;
        this.sheetGDPEditPanel = sheetGDPEditPanel;
    }

    public void buildLink() {
        // создание новых элементов (старые будут потеряны...
        // надо удалить их с панели отображения)
        removeFromOwner();
        // if (linkTrain.distrIDon == -1 || linkTrain.distrIDoff == -1) {
        // System.out.println(linkTrain.codeTRon + " " + linkTrain.codeTRoff);
        // }
        trainSheetOn = sheetGDPEditPanel.getTrainThreadSheet(linkTrain.distrIDon, linkTrain.codeTRon,
                linkTrain.nameTRon);
        trainSheetOff = sheetGDPEditPanel.getTrainThreadSheet(linkTrain.distrIDoff, linkTrain.codeTRoff,
                linkTrain.nameTRoff);
        trainOn = sheetJuncEntity.getTrainOn(linkTrain.codeTRon, linkTrain.nameTRon);
        trainOff = sheetJuncEntity.getTrainOff(linkTrain.codeTRoff, linkTrain.nameTRoff);
        // if (trainOn != null || trainOff != null) {
        if (trainSheetOn != null || trainSheetOff != null) {
            try {
                if (trainSheetOn != null) {
                    trainSheetOn.setLinkEnd(this);
                }
                if (trainSheetOff != null) {
                    trainSheetOff.setLinkBegin(this);
                }

                SheetConfig sheetConf = sheetGDPEditPanel.getSheetEntity().getSheetConfig();
                timeJunc = new TimeJunc(trainOn, trainOff, linkTrain);
                timeJunc.calcX(sheetConf);
                TrainThreadGeneralParams params = sheetGDPEditPanel.getGeneralParams();
                selfParamsHor = new NewLineSelfParams();
                selfParamsHor.setToolTipText(linkTrain.getToolTipText());
                selfParamsHor.setCaptionValue(linkTrain.getCaptionValue());
                selfParamsVert = new NewLineSelfParams();
                selfParamsVert.setCaptionValue(linkTrain.getCaptionValue());
                selfParamsVert.setToolTipText(linkTrain.getToolTipText());

                lineHor1 = new LineHor(params, selfParamsHor, this, sheetConf);
                lineHor2 = new LineHor(params, selfParamsHor, this, sheetConf);
                lineOn = new LineVert(params, selfParamsVert, this, sheetConf);
                lineOff = new LineVert(params, selfParamsVert, this, sheetConf);

                selfParamsHor.lineStyle = LineStyle.Solid;
                selfParamsHor.lineWidth = 2;
                selfParamsVert.lineStyle = LineStyle.Solid;
                selfParamsVert.lineWidth = 1;

                lineHor1.setPopupLink();
                lineHor2.setPopupLink();

                addToOwner();
                setLineBounds();

            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    @Override
    public void setLineBounds() {
        if (trainOn != null || trainOff != null) {
            selfParamsHor.color = Color.black;
            selfParamsVert.color = Color.black;
            lineHor1.setCaption(DrawCaption.On);
            lineHor2.setCaption(DrawCaption.NoCaption);
            boolean line1 = false;
            boolean line2 = false;
            boolean lnOn = false;
            boolean lnOff = false;

            if (timeJunc.xOn != timeJunc.xOff) {
                line1 = true; // есть стоянка
            }
            if (timeJunc.xOn > timeJunc.xOff) {
                line2 = true; // есть переход на новые сутки
            }

            lnOn = isTrainOn();
            lnOff = isTrainOff();

            // получение вертикальных координат
            if (linkTrain.lineNumber < 1)
                linkTrain.lineNumber = 1;
            int yMiddle = sheetJuncEntity.getY(linkTrain.lineNumber);
            if (line1) {

                int w1 = 0;
                int w2 = 0;
                // если нет перехода через сутки
                if (!line2) {
                    w1 = Math.max(1, timeJunc.xOff - timeJunc.xOn);
                    Dimension dim = lineHor1.getOriginalSize();
                    dim.width = w1;
                    dim.height = lineHor1.getCaptionSize().height;
                    lineHor1.setPosition(timeJunc.xOn, yMiddle);
                } else {
                    w1 = sheetConf.rbGDP.x - timeJunc.xOn;
                    w2 = timeJunc.xOff - sheetConf.ltGDP.x;
                    Dimension dim = lineHor1.getOriginalSize();
                    dim.width = Math.max(1, w1);
                    dim.height = lineHor1.getCaptionSize().height;

                    dim = lineHor2.getOriginalSize();
                    dim.width = Math.max(1, w2);
                    dim.height = LineHor.NO_CAPTION_HEIGHT;

                    lineHor1.setPosition(timeJunc.xOn, yMiddle);
                    lineHor2.setPosition(sheetConf.ltGDP.x, yMiddle);
                }
            }

            if (lnOn) {
                // если попали сюда, то trainSheetOn!=null на 100%
                // получение вертикальных координат
                int yTr = trainSheetOn.getLastY();
                int h = Math.max(1, Math.abs(yTr - yMiddle));
                Dimension dim = lineOn.getOriginalSize();
                // dim.width = w1;
                dim.height = h;
                int y0 = Math.min(yTr, yMiddle);
                lineOn.setPosition(timeJunc.xOn, y0);
            }

            if (lnOff) {
                // если попали сюда, то trainSheetOn!=null на 100%
                // получение вертикальных координат
                int yTr = trainSheetOff.getFirstY();
                int h = Math.max(1, Math.abs(yTr - yMiddle));
                Dimension dim = lineOff.getOriginalSize();
                // dim.width = w1;
                dim.height = h;
                int y0 = Math.min(yTr, yMiddle);
                lineOff.setPosition(timeJunc.xOff, y0);
            }

            lineHor1.setVisible(line1);
            lineHor2.setVisible(line2);
            lineOn.setVisible(lnOn);
            lineOff.setVisible(lnOff);
            lineHor1.repaint();
            lineHor2.repaint();
            lineOn.repaint();
            lineOff.repaint();
        }
    }

    private boolean isTrainOn() {
        // проверяем, чтобы поезд был с участка, примыкающего к узлу
        int idB = sheetJuncEntity.getDistrIDBefore();
        int idE = sheetJuncEntity.getDistrIDAfter();
        if (linkTrain.distrIDon != idB && linkTrain.distrIDon != idE) {
            return false;
        }
        // проверяем, чтобы участок примыкал к узлу нужной станцией
        int idJB = sheetJuncEntity.getJuncIDBefore();
        if (linkTrain.distrIDon == idB && linkTrain.juncID != idJB) {
            return false;
        }
        int idJE = sheetJuncEntity.getJuncIDAfter();
        if (linkTrain.distrIDon == idE && linkTrain.juncID != idJE) {
            return false;
        }

        // проверяем, чтобы поезд был
        if (trainOn == null) {
            return false;
        }
        // проверяем, чтобы поезд был на листе
        if (trainSheetOn == null) {
            return false;
        }
        // проверяем, чтобы у поезда из БД и поезда на листе айдишник совпадал
        if (trainSheetOn.getDistrTrain().trainID != trainOn.getDistrTrain().trainID) {
            return false;
        }

        return true;
    }

    private boolean isTrainOff() {
        // проверяем, чтобы поезд был с участка, примыкающего к узлу
        int idB = sheetJuncEntity.getDistrIDBefore();
        int idE = sheetJuncEntity.getDistrIDAfter();
        if (linkTrain.distrIDoff != idB && linkTrain.distrIDoff != idE) {
            return false;
        }

        // проверяем, чтобы участок примыкал к узлу нужной станцией
        int idJB = sheetJuncEntity.getJuncIDBefore();
        if (linkTrain.distrIDoff == idB && linkTrain.juncID != idJB) {
            return false;
        }
        int idJE = sheetJuncEntity.getJuncIDAfter();
        if (linkTrain.distrIDoff == idE && linkTrain.juncID != idJE) {
            return false;
        }

        // проверяем, чтобы поезд был и был на листе
        if (trainOff == null) {
            return false;
        }
        // проверяем, чтобы поезд был на листе
        if (trainSheetOff == null) {
            return false;
        }
        // проверяем, чтобы у поезда из БД и поезда на листе айдишник совпадал
        if (trainSheetOff.getDistrTrain().trainID != trainOff.getDistrTrain().trainID) {
            return false;
        }

        return true;
    }

    @Override
    public void removeFromOwner() {
        JComponent owner = sheetGDPEditPanel.getGDPOwner();
        if (lineHor1 != null) {
            owner.remove(lineHor1);
        }
        if (lineHor2 != null) {
            owner.remove(lineHor2);
        }
        if (lineOn != null) {
            owner.remove(lineOn);
        }
        if (lineOff != null) {
            owner.remove(lineOff);
        }
        owner.repaint();
    }

    @Override
    public void addToOwner() {
        JComponent owner = sheetGDPEditPanel.getGDPOwner();
        if (lineHor1 != null) {
            owner.add(lineHor1, 0);
        }
        if (lineHor2 != null) {
            owner.add(lineHor2, 0);
        }
        if (lineOn != null) {
            owner.add(lineOn, 0);
        }
        if (lineOff != null) {
            owner.add(lineOff, 0);
        }
    }

    @Override
    public void repaint() {
        if (lineHor1 != null) {
            lineHor1.repaint();
        }
        if (lineHor2 != null) {
            lineHor2.repaint();
        }
        if (lineOn != null) {
            lineOn.repaint();
        }
        if (lineOff != null) {
            lineOff.repaint();
        }
    }

    @Override
    public TrainThreadSheet getTrainThread() {
        if (trainSheetOn != null)
            return trainSheetOn;
        if (trainSheetOff != null) {
            return trainSheetOff;
        }
        return null;
    }

    public void moveLink(int step) {
        int lineCount = sheetJuncEntity.getJunc().lineCount;
        int lineNumber = linkTrain.lineNumber + step;
        if (lineNumber < 0)
            lineNumber = 0;
        if (lineNumber > lineCount)
            lineNumber = lineCount;
        linkTrain.lineNumber = lineNumber;
        this.setLineBounds();
    }

    /** сохраняет увязку в БД */
    public void saveLink() {
        LocDAOFactory.getInstance().getLinkTrainDAO().saveLink(linkTrain);
    }

    /** удаляет увязку из БД */
    public void deleteLink() {
        LocDAOFactory.getInstance().getLinkTrainDAO().deleteLink(linkTrain);
    }

    /** помечает увязку для удаления */
    public void markLinkForDelete() {
        sheetJuncEntity.markLinkForDelete(this);
    }

    /** возвращает всплывающее сообщение для увязки */
    public String getToolTipText() {
        if (selfParamsHor != null) {
            return selfParamsHor.getToolTipText();
        } else
            return "";
    }

    @Override
    public void drawInImage(Graphics grGDP, int gapLeft, int gapTop) {
        if (lineHor1 != null) {
            lineHor1.drawInPicture(grGDP, gapLeft, gapTop);
        }
        if (lineHor2 != null) {
            lineHor2.drawInPicture(grGDP, gapLeft, gapTop);
        }
        if (lineOn != null) {
            lineOn.drawInPicture(grGDP, gapLeft, gapTop);
        }
        if (lineOff != null) {
            lineOff.drawInPicture(grGDP, gapLeft, gapTop);
        }
    }

    /**
     * устанавливает выбранность увязки (каскадно!!!) и ее поездов соотв.
     * @param trainThreadSheet нитка, на которой был выбор (может быть Null)
     * @param selected признак выбранности
     */
    public void setSelected(TrainThreadSheet trainThreadSheet, boolean selected) {
        this.selected = selected;
        if (trainSheetOn != null && !trainSheetOn.equals(trainThreadSheet)) {
            trainSheetOn.setSelected(this, isSelected());
        }
        if (trainSheetOff != null && !trainSheetOff.equals(trainThreadSheet)) {
            trainSheetOff.setSelected(this, isSelected());
        }
        repaint();
    }
    
    /**
     * меняет избранность увязки на противоположную
     */
    @Override
    public void mouseClicked() {
        boolean sel = isSelected();
        if (sel) {
            // если поезд/увязка selected то просто убираем выделение (каскадно!!!) 
            // и выставляем в SheetGDPEditPanel оба selectedTrain и selectedLink в нулл, 
            // ничего с ними не делая, если они были не null 
            // т.к. если выбранное поезд/увязка было selected, 
            // то это (selectedTrain и selectedLink) могут быть только его частями
            sel = false;
            sheetGDPEditPanel.setSelected(null, null);
        } else {
            // если поезд/увязка не selected, 
            // то сначала нужно убрать имеющееся выделение (каскадно!!!)
            // либо selectedTrain либо selectedLink (если кто-то из них не null)
            // а затем установить (каскадно!!!) выделение для выбранного поезда/увязки
            sel = true;
            sheetGDPEditPanel.setSelected(null, this);
        }
        // тут собственно вызывается каскадное обновление текущего выбора
        setSelected(null, sel);
    }

    public boolean isSelected() {
        return selected;
    }
}
