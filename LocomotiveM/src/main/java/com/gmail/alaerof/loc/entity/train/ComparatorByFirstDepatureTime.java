package com.gmail.alaerof.loc.entity.train;

import java.util.Comparator;

import com.gmail.alaerof.loc.dao.dto.train.TrainStation;

public class ComparatorByFirstDepatureTime implements Comparator<TrainThread>{

    @Override
    public int compare(TrainThread tr1, TrainThread tr2) {
        TrainStation st1 = tr1.getTrainStation(tr1.getFirstStID());
        TrainStation st2 = tr2.getTrainStation(tr2.getFirstStID());
        return st1.getTimeOffChecked() - st2.getTimeOffChecked();
    }

}
