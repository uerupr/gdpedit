package main;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

import server.init.InitParam;
import server.logic.ServerLog;
import server.logic.ServerSocketGDP;

public class ServerTest {
    static{
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }
    
    public static void main(String[] args){
        InitParam ip = new InitParam();
        //ip.loadInitParam("GDPconf");
        ip.loadInitParam("res\\GDPconf.properties");
        ServerSocketGDP server = new ServerSocketGDP();
        ServerLog frame = new ServerLog();
        frame.pack();
        frame.setVisible(true);
        server.execute(ip.getPort(), frame, ip.getSavePath());
    }
}
