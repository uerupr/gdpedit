package server.logic;

import java.awt.AWTException;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

class FinalMark {
    private boolean finalMark = false;

    public boolean isFinalMark() {
        return finalMark;
    }

    public void setFinalMark(boolean finalMark) {
        this.finalMark = finalMark;
    }

}

public class ServerLog extends JDialog {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(ServerLog.class);
    private FinalMark fm = new FinalMark();
    private TrayIcon trayIcon;
    private SystemTray systemTray = SystemTray.getSystemTray();
    private JPanel content = new JPanel(new FlowLayout());
    private File appRunfile = new File("res/run.txt");

    public ServerLog() {
        // super("Server log");
        if (!canStart()) {
            System.exit(0);
        }
        content.add(new Label("настройки (порт, путь сохранения) см. в файле .\\res\\GDPconf.properties"));
        getContentPane().add(content);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setBounds(0, 0, 500, 150);
        setLocationRelativeTo(null);
        try {
            BufferedImage buffImage = ImageIO.read(new File("res/gdpsrv.gif"));
            trayIcon = new TrayIcon(buffImage, "ГДП-xml-приемник");
            trayIcon.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (!isVisible())
                        setVisible(true);
                }
            });

            setIconImage(buffImage);
        } catch (IOException e) {
            logger.error("создание иконки", e);
        }

        PopupMenu popupMenu = new PopupMenu();
        MenuItem item = new MenuItem("Завершить работу");
        item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fm.setFinalMark(true);
                dispose();
                stop();
            }
        });

        popupMenu.add(item);
        trayIcon.setPopupMenu(popupMenu);
        try {
            systemTray.add(trayIcon);
        } catch (AWTException e1) {
            e1.printStackTrace();
        }
    }

    public boolean isFinalMark() {
        return fm.isFinalMark();
    }

    private boolean canStart() {
        boolean canStart = !appRunfile.exists();
        if (canStart) {
            String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
            logger.info(now + " start");
            try {
                PrintWriter writer = new PrintWriter(appRunfile, "UTF-8");
                try {
                    writer.write(now);
                } finally {
                    writer.close();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return canStart;
    }

    private void stop() {
        try {
            appRunfile.delete();
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
        logger.info(now + " stop");
        System.exit(0);
    }
}