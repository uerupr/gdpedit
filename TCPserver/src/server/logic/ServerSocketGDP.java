package server.logic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ServerSocketGDP {
    private final Charset charsetFile = Charset.forName("windows-1251"); // "CP1251" "US-ASCII"
    private final Charset charsetMess = Charset.forName("UTF-8");
    private static Logger logger = LogManager.getLogger(ServerSocketGDP.class);

    public void execute(int port, ServerLog frame, String path) {

        try (ServerSocket server = new ServerSocket(port)) {
            while (!frame.isFinalMark()) {
                try {
                    Socket client = server.accept();
                    new ClientThread(client, path).start();
                } catch (IOException e) {
                    logger.error("accept client connection", e);
                }
            }
        } catch (IOException e) {
            logger.error("open server socket", e);
        }
    }

    class ClientThread extends Thread {
        private Socket client;
        private String path;
        private InetAddress ip;

        public ClientThread(Socket client, String path) {
            this.client = client;
            this.path = path;
            ip = client.getInetAddress();
            logger.info("client IP=" + ip + "; hostName=" + ip.getHostName());
            //logger.log(null, "client IP=" + ip + "; hostName=" + ip.getHostName());
        }

        private String uploadStringFile(File file, BufferedReader br) throws NumberFormatException, IOException {
            Integer countLines = new Integer(br.readLine());
            String res = "новый файл принят";

            // читаем содержимое файла
            logger.info("читаем содержимое файла ");
            ArrayList<String> contents = new ArrayList<String>();
            for (int i = 0; i < countLines; i++) {
                contents.add(br.readLine());
            }
            
            // записываем файл на диск
            if(file.exists()){
                logger.info("перезаписываем существующий файл на диск ");
                res = "существующий файл обновлен";
            }else{
                logger.info("записываем новый файл на диск ");
            }
            
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),charsetFile))) {
                for (String tmp : contents) {
                    bw.write(tmp);
                    bw.write('\n');
                }
                bw.flush();
            }
            logger.info("запись завершилась успешно");
            return res;
        }

        public void run() {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream(),charsetFile))) {
                // читаем из потока клиента имя и размер файла
                logger.info("читаем из потока клиента имя и размер файла");
                String fileName = br.readLine();
                Integer size = new Integer(br.readLine());
                File file = new File(path + File.separator + fileName);

                String res = uploadStringFile(file, br);

                // формируем ответ клиенту
                String message = fileName + " (" + size/1000 + " Kb)" + "  " + res;
                logger.info(message);
                // отсылаем ответ клиенту
                try (PrintStream pw = new PrintStream(client.getOutputStream(),false,charsetMess.name())) {
                    pw.println(message);
                    pw.flush();
                }
                logger.info("ответ отослан");
            } catch (IOException e) {
                logger.error("error in client thread", e);
            }
        }
    }
}
