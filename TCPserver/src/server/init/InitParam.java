package server.init;

import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import util.FileProperties;

public class InitParam {
    private static Logger logger = LogManager.getLogger(InitParam.class);
    private int port = 4040;
    private String savePath = "D:\\";

    public int getPort() {
        return port;
    }

    public String getSavePath() {
        return savePath;
    }

    public void loadInitParam(String conf) {
        try {
            /*
            ResourceBundle rb = ResourceBundle.getBundle(conf);
            port = new Integer(rb.getString("port"));
            savePath = rb.getString("savePath");
            */
            Properties prop = FileProperties.getInstance().getProperties(conf);
            port = new Integer(prop.getProperty("port"));
            savePath = prop.getProperty("savePath");
            logger.info(this.toString());
        } catch (Exception e) {
            logger.error("init param fail", e);
        }
    }

    public String toString() {
        return "InitParam [port=" + port + ", savePath=" + savePath + "]";
    }

}
