package com.gmail.uer.dbtool;

import com.gmail.uer.dbtool.logic.ClearDBUtil;
import com.gmail.uer.dbtool.logic.TransferDataUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Control;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DbConfigToolController {
    private Logger logger = LoggerFactory.getLogger(DbConfigToolController.class);
    @Autowired
    private ClearDBUtil clearDBUtil;
    @Autowired
    private TransferDataUtil transferDataUtil;
    @FXML
    private TextArea textArea;

    @FXML
    private void handleImportDataFromMSSQL(ActionEvent event) {
        textArea.setText("");
        Scene scene = ((Control)event.getSource()).getScene();
        try {
            scene.setCursor(Cursor.WAIT);
            List<String> trace = new ArrayList<>();
            boolean done = clearDBUtil.clearHSQLDB(trace);
            if (done) {
                transferDataUtil.transferData(trace);
            }
            trace.forEach((item) -> textArea.appendText(item + "\n"));
        } catch (Exception e) {
            logger.error(e.toString(), e);
            textArea.setText(e.toString());
        }
        finally {
            scene.setCursor(Cursor.DEFAULT);
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "done");
            alert.setTitle(" =)");
            alert.setHeaderText("");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleClose(ActionEvent event){
        Stage stage = (Stage) ((Control)event.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void handleLogHSQLData(ActionEvent event){
        transferDataUtil.logHSQLDBData();
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "see log file");
        alert.setTitle(" =)");
        alert.setHeaderText("");
        alert.showAndWait();
    }
}
