package com.gmail.uer.dbtool.dao.hsql;

import com.gmail.uer.dbtool.model.hsql.HCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HSQLCategoryRepository extends JpaRepository<HCategory, Integer> {
}
