package com.gmail.uer.dbtool.model.hsql;

import com.gmail.uer.dbtool.model.Category;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CATEGORY")
public class HCategory {
    @Id
    private int idCat;
    private int color;
    @Embedded
    private  Category category;

    public int getIdCat() {
        return idCat;
    }

    public void setIdCat(int idCat) {
        this.idCat = idCat;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "HCategory{" +
                "idCat=" + idCat +
                ", color=" + color +
                ", category=" + category +
                '}';
    }
}
