package com.gmail.uer.dbtool.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({ "classpath:hsqldb.properties" })
@EnableJpaRepositories(
        basePackages = "com.gmail.uer.dbtool.dao.hsql",
        entityManagerFactoryRef = "hEntityManager",
        transactionManagerRef = "hTransactionManager"
)
public class HConfig {
    @Autowired
    private Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean hEntityManager() {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(hDataSource());
        em.setPackagesToScan(
                new String[] { "com.gmail.uer.dbtool.model.hsql" });

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto",
                env.getProperty("spring.jpa.hibernate.ddl-auto"));
        properties.put("hibernate.show-sql",
                env.getProperty("spring.jpa.show-sql"));
        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    public DataSource hDataSource() {
        DriverManagerDataSource dataSource
                = new DriverManagerDataSource();
        dataSource.setDriverClassName(
                env.getProperty("h.datasource.driverClassName"));
        dataSource.setUrl(env.getProperty("h.datasource.url"));
        dataSource.setUsername(env.getProperty("h.datasource.username"));
        dataSource.setPassword(env.getProperty("h.datasource.password"));
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager hTransactionManager() {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                hEntityManager().getObject());
        return transactionManager;
    }

}
