package com.gmail.uer.dbtool.model.transformer;

import java.awt.Color;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
public class ColorConverter {
    public static final Color C1_BROWN = new Color(150, 75, 0);
    public static final Color C2_NEARLY_KHAKI = new Color(75, 75, 0);
    public static final Color C3_GRAY = new Color(128, 128, 128);
    public static final Color C4_BLACK = new Color(0, 0, 0);
    public static final Color C5_FUCHSIA = new Color(255, 0, 255);
    public static final Color C6_NEARLY_PURPLE = new Color(148, 0, 211);
    public static final Color C7_RED = new Color(255, 0, 0);
    public static final Color C8_BLUE = new Color(0, 0, 255);
    public static final Color C9_NEARLY_ORANGE = new Color(255, 140, 0);
    public static final Color C10_YELLOW = new Color(255, 255, 0);
    public static final Color C11_GREEN = new Color(0, 255, 0);
    public static final Color C12_NEARLY_DARK_GREEN = new Color(0, 128, 0);
    // new colors
    public static final Color C13_TOMATO = new Color(255, 99, 71);
    public static final Color C14_DEEP_PINK = new Color(255, 20, 147);
    public static final Color C15_NEARLY_PALE_MAGENTA = new Color(244, 165, 230);
    public static final Color C16_NEARLY_ORCHID = new Color(187, 84, 183);
    public static final Color C17_AQUA = new Color(0, 255, 255);
    public static final Color C18_DODGER_BLUE = new Color(30, 144, 255);
    public static final Color C19_MINT_GREEN = new Color(152, 255, 152);
    public static final Color C20_OLIVE = new Color(128, 128, 0);
    public static final Color C21_SILVER = new Color(192, 192, 192);
    public static final Color C22_MAROON = new Color(128, 0, 0);
    public static final Color C23_NEARLY_CARROT = new Color(200, 120, 30);
    public static final Color C24_PURPLE = new Color(128, 0, 128);


    /**
     * ����������� ������������� �������� ����� �� ������� ���� � ����� ���
     * @param value ������ ��������
     * @return ����� �������������
     */
	public static Color convert(int value){
		Color clr = new Color(value, false);
		int r = clr.getBlue();
		int g = clr.getGreen();
		int b = clr.getRed();
		clr = new Color(r,g,b);
		return clr;
	}
	/**
	 * ����������� ���� � ������ ����� ��� ������� ����
	 * @param clr ���� ���
	 * @return ����� ��� ���
	 */
	public static int convert(Color clr){
		int value = 0;
		int r = clr.getBlue();
		int g = clr.getGreen();
		int b = clr.getRed();
		clr = new Color(r,g,b,0);
		value = clr.getRGB();
		return value;
	}

	/**
	 * ����������� ���� � Object ��� ������� ����
	 * @param clr ���� ���
	 * @return Object ��� ���
	 */
	public static Object convertObj(Color clr){
        // 54 52
        if (C1_BROWN.equals(clr)) {
            byte[] val = {54, 52};
            return val;
        }
        // 51	50	56	57	54
        if (C2_NEARLY_KHAKI.equals(clr)) {
            byte[] val = {51, 50, 56, 57, 54};
            return val;
        }
        // 56	52	50	49	53	48	52
        if (C3_GRAY.equals(clr)) {
            byte[] val = {56, 52, 50, 49, 53, 48, 52};
            return val;
        }
        // 48
        if (C4_BLACK.equals(clr)) {
            byte[] val = {48};
            return val;
        }
        // 49	54	55	49	49	57	51	53
        if (C5_FUCHSIA.equals(clr)) {
            byte[] val = {49, 54, 55, 49, 49, 57, 51, 53};
            return val;
        }
        // 49	54	55	49	49	56	48	56
        if (C6_NEARLY_PURPLE.equals(clr)) {
            byte[] val = {49, 54, 55, 49, 49, 56, 48, 56};
            return val;
        }
        // 50 53 53
        if (C7_RED.equals(clr)) {
            byte[] val = {50, 53, 53};
            return val;
        }
        // 49	54	55	49	49	54	56	48
        if (C8_BLUE.equals(clr)) {
            byte[] val = {49, 54, 55, 49, 49, 54, 56, 48};
            return val;
        }
        // 56	51	56	56	54	48	56
        if (C9_NEARLY_ORANGE.equals(clr)) {
            byte[] val = {56, 51, 56, 56, 54, 48, 56};
            return val;
        }
        // 54	53	53	51	53
        if (C10_YELLOW.equals(clr)) {
            byte[] val = {54, 53, 53, 51, 53};
            return val;
        }
        // 54	53	52	48	56
        if (C11_GREEN.equals(clr)) {
            byte[] val = {54, 53, 52, 48, 56};
            return val;
        }
        // 51	50	55	54	56
        if (C12_NEARLY_DARK_GREEN.equals(clr)) {
            byte[] val = {51, 50, 55, 54, 56};
            return val;
        }
        byte[] val = {48};
        return val;
    }

    /**
     * ����������� Object �������� ����� �� ������� ���� � ����� ���
     * @param val ������ ��������
     * @return ����� �������������
     */
    public static Color convert(Object val) {
        byte[] value = (byte[]) val;
        // 54 52
        if (value.length == 2 && value[0] == 54 && value[1] == 52) {
            return C1_BROWN;
        }
        // 51	50	56	57	54
        if (value.length == 5 && value[0] == 51 && value[2] == 56) {
            return C2_NEARLY_KHAKI;
        }
        // 56	52	50	49	53	48	52
        if (value.length == 7 && value[0] == 56 && value[1] == 52) {
            return C3_GRAY;
        }
        // 48
        if (value.length == 1 && value[0] == 48) {
            return C4_BLACK;
        }
        // 49	54	55	49	49	57	51	53
        if (value.length == 8 && value[0] == 49 && value[7] == 53) {
            return C5_FUCHSIA;
        }
        // 49	54	55	49	49	56	48	56
        if (value.length == 8 && value[0] == 49 && value[7] == 56) {
            return C6_NEARLY_PURPLE;
        }
        // 50 53 53
        if (value.length == 3 && value[0] == 50) {
            return C7_RED;
        }
        // 49	54	55	49	49	54	56	48
        if (value.length == 8 && value[0] == 49 && value[7] == 48) {
            return C8_BLUE;
        }
        // 56	51	56	56	54	48	56
        if (value.length == 7 && value[0] == 56 && value[1] == 51) {
            return C9_NEARLY_ORANGE;
        }
        // 54	53	53	51	53
        if (value.length == 5 && value[0] == 54 && value[2] == 53) {
            return C10_YELLOW;
        }
        // 54	53	52	48	56
        if (value.length == 5 && value[0] == 54 && value[2] == 52) {
            return C11_GREEN;
        }
        // 51	50	55	54	56
        if (value.length == 5 && value[0] == 51 && value[2] == 55) {
            return C12_NEARLY_DARK_GREEN;
        }
        return Color.BLACK;
    }

    public static javafx.scene.paint.Color colorAWTtoFX(Color clrAWT)
    {
        int clrRed = clrAWT.getRed();
        int clrGreen = clrAWT.getGreen();
        int clrBlue = clrAWT.getBlue();
        int iclrAlpha = clrAWT.getAlpha();
        double clrAlpha = iclrAlpha / 255;
        javafx.scene.paint.Color clrFX = javafx.scene.paint.Color.rgb(clrRed, clrGreen, clrBlue, clrAlpha);
        return clrFX;
    }

    public static Color colorFXtoAWT(javafx.scene.paint.Color clrFX)
    {
        double clrRed = clrFX.getRed();
        double clrGreen = clrFX.getGreen();
        double clrBlue = clrFX.getBlue();
        long iclrRed = Math.round(clrRed * 255);
        long iclrGreen = Math.round(clrGreen * 255);
        long iclrBlue = Math.round(clrBlue * 255);
        Color clrAWT = new Color((int) iclrRed, (int) iclrGreen, (int) iclrBlue);
        return clrAWT;
    }

    public static int convertFX(javafx.scene.paint.Color clrFX)
    {
        int value = 0;
        Color clrAWT = colorFXtoAWT(clrFX);
        value = convert(clrAWT);
        return value;
    }

    public static String getHEXString(javafx.scene.paint.Color color) {
        int r = (int) (color.getRed() * 255);
        int g = (int) (color.getGreen() * 255);
        int b = (int) (color.getBlue() * 255);
        String hex = String.format("#%02X%02X%02X", r, g , b);
        return hex;
    }
}
