package com.gmail.uer.dbtool.model.mssql;

import com.gmail.uer.dbtool.model.Category;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "Category")
public class MSCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int idCat;
    @Lob
    private byte[] color;
    @Embedded
    private Category category;

    public int getIdCat() {
        return idCat;
    }

    public void setIdCat(int idCat) {
        this.idCat = idCat;
    }

    public byte[] getColor() {
        return color;
    }

    public void setColor(byte[] color) {
        this.color = color;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "MSCategory{" +
                "idCat=" + idCat +
//                ", color=" + color +
                ", category=" + category +
                '}';
    }
}
