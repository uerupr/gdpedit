package com.gmail.uer.dbtool.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ClearDBUtil {
    private Logger logger = LoggerFactory.getLogger(ClearDBUtil.class);
    @Autowired
    private Environment env;
    @Autowired
    @Qualifier("hDataSource")
    DataSource hDataSource;

    public boolean clearHSQLDB(List<String> warnings) {
        warnings.add("--------- clearHSQLDB ---------");
        try {
            String[] tablesToClearOnly = env.getProperty("tables.clear").split(",");
            String[] tablesToTransfer = env.getProperty("tables.transfer").split(",");
            List<String> allToClear = new ArrayList<>();
            allToClear.addAll(Arrays.asList(tablesToClearOnly));
            allToClear.addAll(Arrays.asList(tablesToTransfer));
            for (String tableName : allToClear) {
                clearTable(tableName);
                warnings.add("-- " + tableName + " cleaned --");
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
            warnings.add(e.toString());
        }
        return true;
    }

    private void clearTable(String tableName) throws SQLException {
        if (!tableName.isEmpty()) {
            String sql = "DELETE FROM " + tableName.toUpperCase();
            try (Statement st = hDataSource.getConnection().createStatement()) {
                st.execute(sql);
            }
        }
    }
}
