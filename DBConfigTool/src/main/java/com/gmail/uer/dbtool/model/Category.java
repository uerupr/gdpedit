package com.gmail.uer.dbtool.model;

import javax.persistence.Embeddable;

@Embeddable
public class Category {
    private String name;
    private int prioritet_;
    private int trMin;
    private int trMax;
    private String type;
    private boolean bType;
    private String base;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrioritet_() {
        return prioritet_;
    }

    public void setPrioritet_(int prioritet_) {
        this.prioritet_ = prioritet_;
    }

    public int getTrMin() {
        return trMin;
    }

    public void setTrMin(int trMin) {
        this.trMin = trMin;
    }

    public int getTrMax() {
        return trMax;
    }

    public void setTrMax(int trMax) {
        this.trMax = trMax;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isbType() {
        return bType;
    }

    public void setbType(boolean bType) {
        this.bType = bType;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", prioritet_=" + prioritet_ +
                ", trMin=" + trMin +
                ", trMax=" + trMax +
                ", type='" + type + '\'' +
                ", bType=" + bType +
                ", base='" + base + '\'' +
                '}';
    }
}
