package com.gmail.uer.dbtool.dao.mssql;

import com.gmail.uer.dbtool.model.mssql.MSCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MSSQLCategoryRepository extends JpaRepository<MSCategory, Integer> {
}
