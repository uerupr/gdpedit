package com.gmail.uer.dbtool.model.transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;

public abstract class EntityTransformer<F, T> {
    protected Logger logger = LoggerFactory.getLogger(EntityTransformer.class);

    abstract protected T transform(F msItem);

    public List<T> transform(List<F> msList) {
        List<T> hList = new ArrayList<>();
        msList.forEach(item -> hList.add(transform(item)));
        return hList;
    }

    public void doTransfer(){
        List<F> fromList = getMSRepository().findAll();
        //logger.debug(fromList.toString());
        List<T> toList = transform(fromList);
        //logger.debug(toList.toString());
        getHRepository().saveAll(toList);
    }

    protected abstract JpaRepository getHRepository();

    protected abstract JpaRepository getMSRepository();
}
