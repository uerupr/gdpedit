package com.gmail.uer.dbtool.logic;

import com.gmail.uer.dbtool.model.transformer.CategoryTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Component
public class TransferDataUtil {
    private Logger logger = LoggerFactory.getLogger(TransferDataUtil.class);
    @Autowired
    private CategoryTransformer categoryTransformer;
    @Autowired
    private Environment env;

    @Autowired
    @Qualifier("hDataSource")
    DataSource hDataSource;
    @Autowired
    @Qualifier("msDataSource")
    DataSource msDataSource;


    public void transferData(List<String> warnings) {
        try {
            warnings.add("--------- transferData ---------");
            logger.debug("--------- transferData ---------");
            categoryTransformer.doTransfer();
            warnings.add("-- Category transferred --");
            logger.debug("-- Category transferred --");

            String[] tablesToTransfer = env.getProperty("tables.transfer").split(",");

            for (String tableName : tablesToTransfer) {
                transferTableData(tableName);
                warnings.add("-- " + tableName + "  data transferred ");
                logger.debug("-- " + tableName + "  data transferred ");
            }

        } catch (Exception e) {
            logger.error(e.toString(), e);
            warnings.add(e.toString());
        }
    }

    private void transferTableData(String tableName) throws SQLException {
        if(!tableName.isEmpty()) {
            String selectSQL = "SELECT * FROM " + tableName;
            try (Statement msst = msDataSource.getConnection().createStatement();) {
                try (ResultSet msrs = msst.executeQuery(selectSQL)) {
                    StringBuilder cols = new StringBuilder();
                    StringBuilder vals = new StringBuilder();
                    for (int i = 1; i <= msrs.getMetaData().getColumnCount(); i++) {
                        String colName = msrs.getMetaData().getColumnLabel(i);
                        if (!"SCHEME".equalsIgnoreCase(colName)) {
                            cols.append(colName.toUpperCase());
                            vals.append("?");
                            if (i < msrs.getMetaData().getColumnCount()) {
                                cols.append(", ");
                                vals.append(", ");
                            }
                        }
                    }
                    String insertSQL = "INSERT INTO " + tableName.toUpperCase() + "(" + cols.toString() + ") VALUES(" + vals.toString() + ")";
                    logger.debug(insertSQL);
                    int count = 0;
                    try (PreparedStatement hps = hDataSource.getConnection().prepareStatement(insertSQL)) {
                        while (msrs.next()) {
                            int colNumber = 1;
                            for (int i = 1; i <= msrs.getMetaData().getColumnCount(); i++) {
                                String colName = msrs.getMetaData().getColumnLabel(i);
                                if (!"SCHEME".equalsIgnoreCase(colName)) {
                                    Object value = msrs.getObject(i);
                                    hps.setObject(colNumber, value);
                                    colNumber++;
                                }
                            }
                            hps.addBatch();
                            count++;
                        }
                        if (count > 0) {
                            hps.executeBatch();
                        }
                    }
                }
            }
        }
    }

    public void logHSQLDBData(){
        try {
            logger.debug("--------- log HSQLDB Data ---------");
            String[] tablesToTransfer = env.getProperty("tables.transfer").split(",");

            for (String tableName : tablesToTransfer) {
                logTableData(tableName);
            }

        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void logTableData(String tableName) throws SQLException {
        logger.debug(" -- " + tableName.toUpperCase());
        String selectSQL = "SELECT * FROM " + tableName.toUpperCase();
        try (Statement st = hDataSource.getConnection().createStatement()) {
            try (ResultSet rs = st.executeQuery(selectSQL)) {
                int colCount = rs.getMetaData().getColumnCount();
                while (rs.next()) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 1; i <= colCount; i++) {
                        sb.append(rs.getObject(i)).append("; ");
                    }
                    logger.debug(sb.toString());
                }
            }
        }
    }

}
