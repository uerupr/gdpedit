package com.gmail.uer.dbtool.model.transformer;

import com.gmail.uer.dbtool.dao.hsql.HSQLCategoryRepository;
import com.gmail.uer.dbtool.dao.mssql.MSSQLCategoryRepository;
import com.gmail.uer.dbtool.model.hsql.HCategory;
import com.gmail.uer.dbtool.model.mssql.MSCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class CategoryTransformer extends EntityTransformer<MSCategory, HCategory> {
    @Autowired
    private HSQLCategoryRepository hRepository;
    @Autowired
    private MSSQLCategoryRepository msRepository;

    protected HCategory transform(MSCategory msCategory) {
        HCategory hCategory = new HCategory();
        hCategory.setIdCat(msCategory.getIdCat());
        hCategory.setCategory(msCategory.getCategory());
        byte[] clr = msCategory.getColor();
        Color color = ColorConverter.convert(clr);
        int colorInt =  ColorConverter.convert(color);
        hCategory.setColor(colorInt);
        return hCategory;
    }
    @Override
    protected JpaRepository getHRepository() {
        return hRepository;
    }

    @Override
    protected JpaRepository getMSRepository() {
        return msRepository;
    }
}
