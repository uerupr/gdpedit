package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.gdp.GDParm;
import com.gmail.alaerof.dao.interfacedao.IGDParmDAO;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class GDParmDAOSQL implements IGDParmDAO {

  protected static Logger logger = LogManager.getLogger(GDParmDAOSQL.class);
  private final ConnectionManager cm;
  private static final String SQL_SELECT = "SELECT GDParmID,IDdistr,dateBegin,dateEnd,gdpXML,dateOn,author," +
          "Description,norm,Arch,dateArch,HasCalculation,dateKTC,responceKTC " +
          "FROM GDParm WHERE IDdistr = ?";
  private static final String SQL_SELECT_ACTUAL = "SELECT GDParmID,IDdistr,dateBegin,dateEnd,gdpXML,dateOn,author," +
          "Description,norm,Arch,dateArch,HasCalculation,dateKTC,responceKTC " +
          "FROM GDParm WHERE IDdistr = ? AND Arch=0";
  private static final String SQL_SELECT_ARCHIVE = "SELECT GDParmID,IDdistr,dateBegin,dateEnd,gdpXML,dateOn,author," +
          "Description,norm,Arch,dateArch,HasCalculation,dateKTC,responceKTC " +
          "FROM GDParm WHERE IDdistr = ? AND Arch=1";
  private static final String SQL_SELECT_NORMATIVE = "SELECT GDParmID,IDdistr,dateBegin,dateEnd,gdpXML,dateOn,author," +
          "Description,norm,Arch,dateArch,HasCalculation,dateKTC,responceKTC " +
          "ROM GDParm WHERE IDdistr = ? AND norm=1";
  private static final String SQL_SELECT_VARIANT = "SELECT GDParmID,IDdistr,dateBegin,dateEnd,gdpXML,dateOn,author," +
          "Description,norm,Arch,dateArch,HasCalculation,dateKTC,responceKTC " +
          "FROM GDParm WHERE IDdistr = ? AND norm=0";
  private static final String SQL_UPDATE = "UPDATE GDParm SET Arch=? WHERE GDParmID = ?";
  private static final String SQL_INSERT = "INSERT INTO GDParm (IDdistr, dateBegin, dateEnd, gdpXML, dateOn, author, Description, norm) " +
          "VALUES (?, convert(datetime,?,121),  convert(datetime,?,121), ?, convert(datetime,?,121), ?, ?, ?)";
  private static final String SELECT_MAX_ID = "SELECT MAX(GDParmID) as maxID FROM GDParm";

  public enum GDP {
    ACTUAL,
    ARCHIVE,
    NORMATIVE,
    VARIANT,
    ALL
  }

  public GDParmDAOSQL(ConnectionManager cm) {
    this.cm = cm;
  }

  private List<GDParm> getListGDP(int idDistr, String sql) throws PoolManagerException {
    List<GDParm> list = new ArrayList<>();
    try (PreparedStatement ps = cm.getConnection().prepareStatement(sql)) {
      ps.setLong(1, idDistr);
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          list.add(createGDParm(rs));
        }
      }
    } catch (SQLException e) {
      logger.error("error SQL select GDParm");
    }
    return list;
  }

  @Override
  public List<GDParm> getListGDParm(int idDistr) throws PoolManagerException {
    return getListGDP(idDistr, SQL_SELECT);
  }

  public List<GDParm> getListGDParm(int idDistr, GDP gdp) throws PoolManagerException {
    switch (gdp) {
      case ACTUAL:
        return getListGDParmActual(idDistr);
      case ARCHIVE:
        return getListGDParmArchive(idDistr);
      case NORMATIVE:
        return getListGDParmNormative(idDistr);
      case VARIANT:
        return getListGDParmVariant(idDistr);
    }
    return getListGDParm(idDistr);
  }

  @Override
  public List<GDParm> getListGDParmActual(int idDistr) throws PoolManagerException {
    return getListGDP(idDistr, SQL_SELECT_ACTUAL);
  }

  @Override
  public List<GDParm> getListGDParmArchive(int idDistr) throws PoolManagerException {
    return getListGDP(idDistr, SQL_SELECT_ARCHIVE);
  }

  @Override
  public List<GDParm> getListGDParmNormative(int idDistr) throws PoolManagerException {
    return getListGDP(idDistr, SQL_SELECT_NORMATIVE);
  }

  @Override
  public List<GDParm> getListGDParmVariant(int idDistr) throws PoolManagerException {
    return getListGDP(idDistr, SQL_SELECT_VARIANT);
  }

  public GDParm createGDParm(ResultSet rs) throws SQLException {
    int idGDParm = rs.getInt("GDParmID");
    Long idDistr = 1L;//rs.getLong("IDDistr");

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    java.sql.Date currentDate = new java.sql.Date(new java.util.Date().getTime());

    String dateBegin = df.format(rs.getDate("dateBegin"));
    String dateEnd = df.format(rs.getDate("dateEnd"));
    String dateOn = df.format(currentDate);
    String author = rs.getString("author");
    String description = rs.getString("description");
    String dateKTC = rs.getString("dateKTC");
    Boolean norm = rs.getBoolean("norm");
    Boolean arch = rs.getBoolean("arch");
    String responceKTC = rs.getString("responceKTC");
    String gdpXML = rs.getString("gdpXML");

    return new GDParm(idGDParm, idDistr, dateBegin, dateEnd, dateOn, author, description, dateKTC, norm, arch, responceKTC, gdpXML);
  }


  @Override
  public boolean updateGDParm(int idGDParm, int arch) throws PoolManagerException {
    try (PreparedStatement ps = cm.getConnection().prepareStatement(SQL_UPDATE)) {
      ps.setInt(1, arch);
      ps.setInt(2, idGDParm);
      ps.executeUpdate();
    } catch (SQLException e) {
      logger.error("error SQL update GDParm");
    }
    return false;
  }

  @Override
  public int insertGDParm(GDParm aGDParm) throws PoolManagerException {
    try (PreparedStatement ps = cm.getConnection().prepareStatement(SQL_INSERT)) {
      ps.setLong(1, aGDParm.getIdDistr());
      ps.setString(2, aGDParm.getDateBegin());
      ps.setString(3, aGDParm.getDateEnd());
      ps.setString(4, aGDParm.getGdpXML());
      ps.setString(5, aGDParm.getDateOn());
      ps.setString(6, aGDParm.getAuthor());
      ps.setString(7, aGDParm.getDescription());
      ps.setBoolean(8, aGDParm.getNorm());
      ps.executeUpdate();
      return getMaxId();
    } catch (SQLException e) {
      logger.error("error SQL insert GDParm " + e.toString(), e);
    }
    return 0;
  }


  @Override
  public int getMaxId() throws PoolManagerException {
    int maxIdGDParm = 0;
    try (PreparedStatement ps = cm.getConnection().prepareStatement(SELECT_MAX_ID)) {
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          maxIdGDParm = rs.getInt("maxID");
        }
      }
    } catch (SQLException e) {
      logger.error("error select max idGDParm");
    }
    return maxIdGDParm;
  }

}
