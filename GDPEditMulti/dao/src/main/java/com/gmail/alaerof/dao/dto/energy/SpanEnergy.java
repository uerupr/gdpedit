package com.gmail.alaerof.dao.dto.energy;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.energy.Energy;
import java.util.HashMap;
import java.util.Map;

public class SpanEnergy {
    private int idScale;
    /** �������� */
    private Map<LocomotiveType, Energy> locEnergyO = new HashMap<>();
    /** ������ */
    private Map<LocomotiveType, Energy> locEnergyE = new HashMap<>();

    public int getIdScale() {
        return idScale;
    }

    public void setIdScale(int idScale) {
        this.idScale = idScale;
    }

    public Map<LocomotiveType, Energy> getLocEnergyO() {
        return locEnergyO;
    }

    public Map<LocomotiveType, Energy> getLocEnergyE() {
        return locEnergyE;
    }
}
