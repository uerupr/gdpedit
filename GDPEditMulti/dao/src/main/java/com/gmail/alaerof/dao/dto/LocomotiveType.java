package com.gmail.alaerof.dao.dto;

/**
 * ��� ��������� ����������/�������� (������������/��������/�����������)
 * @author Helen Yrofeeva
 * 
 */
public class LocomotiveType {
    public static final int LENGTH = 4;

    private int idLtype;
    private String ltype = "";
    private int colorLoc;
    private boolean showInGDP = true;

    public int getIdLType() {
        return idLtype;
    }

    public void setIdLtype(int idLtype) {
        this.idLtype = idLtype;
        if (idLtype == 3) {
            showInGDP = false;
        }
    }

    public String getLtype() {
        return ltype;
    }

    public void setLtype(String ltype) {
        if (ltype != null) {
            this.ltype = ltype.trim();
        }
    }

    public String getShortLtype() {
        if (ltype == null) {
            return "";
        } else {
            String st = new String(String.copyValueOf(ltype.toCharArray(), 0, LENGTH));
            return st;
        }
    }

    @Override
    public String toString() {
        return getLtype();
    }

    public int getColorLoc() {
        return colorLoc;
    }

    public void setColorLoc(int colorLoc) {
        this.colorLoc = colorLoc;
    }

    public boolean isShowInGDP() {
        return showInGDP;
    }

    public void setShowInGDP(boolean showInGDP) {
        this.showInGDP = showInGDP;
    }

    /**
     * ������ ��� ID ��������������� !!! �.�. ������������ � ������ ������ �� Delphi
     * @return ����������� �������������� ���� �������� (����������)
     */
    public boolean isEditable() {
        if (getIdLType() > 3 || getIdLType() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocomotiveType that = (LocomotiveType) o;

        return idLtype == that.idLtype;
    }

    @Override
    public int hashCode() {
        return idLtype;
    }
}
