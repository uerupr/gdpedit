package com.gmail.alaerof.dao.dto.groupp;

import java.util.ArrayList;
import java.util.List;

/**
 * ��������� (������) ������� ��� ������� ����������� ��� �������
 */
public class GroupP implements Comparable<GroupP>{
    private static int lastID = -1;
    private int idGroup = lastID--;
    private String name;
    private boolean byColor;
    private int ord;
    /** IDltype ���� �������� (����������) ��� ��������� ����������� �������� */
    private int idLtype;

    private List<GroupRange> rangeList = new ArrayList<>();
    private List<GroupColor> colorList = new ArrayList<>();

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isByColor() {
        return byColor;
    }

    public void setByColor(boolean byColor) {
        this.byColor = byColor;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public List<GroupRange> getRangeList() {
        return rangeList;
    }

    public void setRangeList(List<GroupRange> rangeList) {
        this.rangeList = rangeList;
    }

    public List<GroupColor> getColorList() {
        return colorList;
    }

    public void setColorList(List<GroupColor> colorList) {
        this.colorList = colorList;
    }

    public int getIdLtype() {
        return idLtype;
    }

    public void setIdLtype(int idLtype) {
        this.idLtype = idLtype;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupP groupP = (GroupP) o;

        if (idGroup != groupP.idGroup) return false;
        if (idLtype != groupP.idLtype) return false;
        return name.equals(groupP.name);
    }

    @Override
    public int hashCode() {
        int result = idGroup;
        result = 31 * result + name != null ? name.hashCode() : 12;
        result = 31 * result + idLtype;
        return result;
    }

    @Override
    public String toString() {
        return "GroupP{" +
                "idGroup=" + idGroup +
                ", name='" + name + '\'' +
                ", byColor=" + byColor +
                ", ord=" + ord +
                ", idLtype=" + idLtype +
                '}';
    }

    @Override
    public int compareTo(GroupP o) {
        return this.ord - o.ord;
    }

    public boolean isTrainInGroup(int codeTR) {
        for (GroupRange range : this.getRangeList()) {
            if (codeTR >= range.getMinCode() && codeTR <= range.getMaxCode()) {
                return true;
            }
        }
        return false;
    }

    public boolean isTrainInGroupByColor(int color) {
        for (GroupColor groupColor : this.getColorList()) {
            if (color == groupColor.getColor()) {
                return true;
            }
        }
        return false;
    }

    public String getColorString() {
        if (byColor) {
            return "*";
        }
        return "";
    }

    public void setColorString(String value) {
        byColor = value.contains("*");
    }

}
