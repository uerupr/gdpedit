package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.List;

public interface IDistrCombDAO {
    public List<Integer> getDistrCombList(int idDistr) throws DataManageException;
    public void deleteDistrComb(int idDistr) throws DataManageException;
    public void addDistrCombList(List<Distr> distrCombList, int idDistr) throws DataManageException;
}
