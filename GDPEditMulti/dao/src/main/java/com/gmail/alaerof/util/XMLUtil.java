package com.gmail.alaerof.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

public class XMLUtil {
    protected static Logger logger = LogManager.getLogger(XMLUtil.class);

    /**
     * ������� int ������� �� xml �������� �� �����
     * � ���������� ������
     * @param item  xml �������
     * @param attrName ��� ��������
     * @return �������� ��� 0
     */
    public static int getAttributeInt(Element item, String attrName){
        int value = 0;
        try {
            value = Integer.parseInt(item.getAttribute(attrName));
        } catch (Exception e) {
            logger.error("attrName=" + attrName + ": " + item.getAttribute(attrName));
            logger.error(e.toString());
        }
        return value;
    }

    /**
     * ������� long ������� �� xml �������� �� �����
     * � ���������� ������
     * @param item  xml �������
     * @param attrName ��� ��������
     * @return �������� ��� 0
     */
    public static long getAttributeLong(Element item, String attrName){
        long value = 0;
        try {
            value = Long.parseLong(item.getAttribute(attrName));
        } catch (Exception e) {
            logger.error("attrName=" + attrName + ": " + item.getAttribute(attrName));
            logger.error(e.toString());
        }
        return value;
    }

    /**
     * ������� double ������� �� xml �������� �� �����
     * � ���������� ������
     * @param item  xml �������
     * @param attrName ��� ��������
     * @return �������� ��� 0
     */
    public static double getAttributeDouble(Element item, String attrName){
        double value = 0;
        String valueStr = "";
        try {
            valueStr = item.getAttribute(attrName);
            value = Double.parseDouble(valueStr);
        } catch (Exception e) {
            valueStr = valueStr.replace(',', '.');
            try {
                value = Double.parseDouble(valueStr);
            } catch (Exception ne) {
                logger.warn(e.toString() + "\t attrName=" + attrName + ": " + item.getAttribute(attrName));
            }
        }
        return value;
    }

    /**
     * ������� int ������� �� xml �������� �� �����
     * � ���������� ������
     * @param item  xml �������
     * @param attrName ��� ��������
     * @return �������� ��� 0
     */
    public static boolean getAttributeBoolean(Element item, String attrName){
        boolean value = false;
        try {
            value = Boolean.valueOf(item.getAttribute(attrName));
        } catch (Exception e) {
            logger.error("attrName=" + attrName + ": " + item.getAttribute(attrName));
            logger.error(e.toString());
        }
        return value;
    }

    /**
     * ������� String ������� �� xml �������� �� �����
     * � ���������� �������� �� ����� � ���������� ������
     * @param item  xml �������
     * @param attrName ��� ��������
     * @return �������� ��� ""
     */
    public static String getAttribute(Element item, String attrName){
        String value = "";
        try {
            value = item.getAttribute(attrName.trim());
        } catch (Exception e) {
            logger.error("attrName=" + attrName + ": " + item.getAttribute(attrName));
            logger.error(e.toString());
        }
        return value;
    }
}
