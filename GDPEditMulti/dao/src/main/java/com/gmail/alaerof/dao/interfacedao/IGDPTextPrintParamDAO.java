package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.TextPrintParam;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.List;

/**
 * Created by Paul M. Bui on 31.05.2018.
 */

public interface IGDPTextPrintParamDAO {
    void deleteTextPrintParam(int idDistr) throws DataManageException;
    public void addTextPrintParam(List<TextPrintParam> listTextPrintParam) throws DataManageException;
    List<TextPrintParam> getListTextPrintParam(int idDistr) throws DataManageException;
}
