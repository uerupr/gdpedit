package com.gmail.alaerof.dao.dto;
/**
 * ������� ���� ��� ������ ���� ��������
 * @author Helen Yrofeeva
 *
 */
public class Time implements Comparable<Time>{
	private float timeMove;
	private float timeUp;
	private float timeDown;
	private float timeTw;
	
	public float getTimeMove() {
		return timeMove;
	}
	public void setTimeMove(float timeMove) {
		this.timeMove = timeMove;
	}
	public float getTimeUp() {
		return timeUp;
	}
	public void setTimeUp(float timeUp) {
		this.timeUp = timeUp;
	}
	public float getTimeDown() {
		return timeDown;
	}
	public void setTimeDown(float timeDown) {
		this.timeDown = timeDown;
	}
	
	@Override
	public String toString() {
		return "Time [timeMove=" + timeMove + ", timeUp=" + timeUp
				+ ", timeDown=" + timeDown + "]";
	}
	public float getTimeTw() {
		return timeTw;
	}
	public void setTimeTw(float timeTw) {
		this.timeTw = timeTw;
	}
	@Override
	public int compareTo(Time o) {
		return (int)(timeMove - o.timeMove);
	}
}
