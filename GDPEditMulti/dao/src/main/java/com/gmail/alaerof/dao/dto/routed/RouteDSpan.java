package com.gmail.alaerof.dao.dto.routed;
/**
 * @author Helen Yrofeeva
 */
public class RouteDSpan implements Comparable<RouteDSpan>{
    private int idRouteDS;// IDrouteDS �� ����� ��� ����
    private int idRouteD; // IDrouteD
    private int idSpan;   // IDspan
    private int num;      // num

    public int getIdRouteDS() {
        return idRouteDS;
    }
    public void setIdRouteDS(int idRouteDS) {
        this.idRouteDS = idRouteDS;
    }
    public int getIdRouteD() {
        return idRouteD;
    }
    public void setIdRouteD(int idRouteD) {
        this.idRouteD = idRouteD;
    }
    public int getIdSpan() {
        return idSpan;
    }
    public void setIdSpan(int idSpan) {
        this.idSpan = idSpan;
    }
    public int getNum() {
        return num;
    }
    public void setNum(int num) {
        this.num = num;
    }
    @Override
    public int compareTo(RouteDSpan sp) {
        return this.getNum() - sp.getNum();
    }
    @Override
    public String toString() {
        return "RouteDSpan [idRouteD=" + idRouteD + ", idSpan=" + idSpan + ", num=" + num + "]";
    }
}
