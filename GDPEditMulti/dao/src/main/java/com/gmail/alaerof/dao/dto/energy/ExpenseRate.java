package com.gmail.alaerof.dao.dto.energy;

import com.gmail.alaerof.dao.dto.gdp.TrainType;

public class ExpenseRate {
    private float moveExpRate;
    private float stopExpRate;
    private TrainType trainType;
    private EnergyType energyType;

    public ExpenseRate(float moveExpRate, float stopExpRate, TrainType trainType, EnergyType energyType) {
        this.moveExpRate = moveExpRate;
        this.stopExpRate = stopExpRate;
        this.trainType = trainType;
        this.energyType = energyType;
    }

    public float getMoveExpRate() {
        return moveExpRate;
    }

    public float getStopExpRate() {
        return stopExpRate;
    }

    public TrainType getTrainType() {
        return trainType;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public void setMoveExpRate(float moveExpRate) {
        this.moveExpRate = moveExpRate;
    }

    public void setStopExpRate(float stopExpRate) {
        this.stopExpRate = stopExpRate;
    }

    public void setTrainType(TrainType trainType) {
        this.trainType = trainType;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }
}
