package com.gmail.alaerof.dao.dto.gdp;

/**
 * ���������� ����� ������ �� �.�.
 */
public class TrainStop {
    private long idTrain; // IDtrain
    private int idSpanst; // IDspanst
    private int tstop; // Tstop
    private String occupyOn; // occupyOn
    private String occupyOff; // occupyOff
    private String sk_ps = ""; // sk_ps
    private int idDistr; // IDdistr �� ����� ���... �� ����� ����� (������� ���
                         // ������� �����-���� �����������

    public long getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(long idTrain2) {
        this.idTrain = idTrain2;
    }

    public int getIdSpanst() {
        return idSpanst;
    }

    public void setIdSpanst(int idSpanst) {
        this.idSpanst = idSpanst;
    }

    public int getTstop() {
        return tstop;
    }

    public void setTstop(int tstop) {
        this.tstop = tstop;
    }

    public String getOccupyOn() {
        return occupyOn;
    }

    public void setOccupyOn(String occupyOn) {
        this.occupyOn = occupyOn;
    }

    public String getOccupyOff() {
        return occupyOff;
    }

    public void setOccupyOff(String occupyOff) {
        this.occupyOff = occupyOff;
    }

    public String getSk_ps() {
        return sk_ps;
    }

    public void setSk_ps(String sk_ps) {
        if (sk_ps != null) {
            this.sk_ps = sk_ps;
        }
    }

    public int getIdDistr() {
        return idDistr;
    }

    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }

    @Override
    public String toString() {
        return "TrainStop [idTrain=" + idTrain + ", idSpanst=" + idSpanst + ", occupyOn=" + occupyOn
                + ", occupyOff=" + occupyOff + ", sk_ps=" + sk_ps + "]";
    }

}
