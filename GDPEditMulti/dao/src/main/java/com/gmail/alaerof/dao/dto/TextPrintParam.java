package com.gmail.alaerof.dao.dto;

import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul M. Bui on 27.05.2018.
 */
public class TextPrintParam {
    private int idDistr;      // IDdistr
    private int tpType;       // tpType
    private int choise;       // choise
    private String textField; // text
    private String fontField; // font
    private int num;          // num

    public TextPrintParam(){}

    public TextPrintParam(int idDistr, int tpType, int choise, String textField, String fontField, int num){
        this.idDistr = idDistr;
        this.tpType = tpType;
        this.choise = choise;
        this.textField = textField;
        this.fontField = fontField;
        this.num = num;
    }

    public int getIdDistr() {
        return idDistr;
    }

    public int getTpType() {
        return tpType;
    }

    public Integer getChoise() {
        return choise;
    }

    public Boolean getBChoise() {
        if (choise == 1){
            return true;
        }
        else {
            return false;
        }
    }

    public String getTextField() {
        return textField;
    }

    public String getFontField() {
        return fontField;
    }

    public String getFontName() {
        return fontField.replaceAll(".*name=|,.*", "");
    }

    public String getFontFamily() {
        return fontField.replaceAll(".*family=|,.*", "");
    }

    public String getFontStyle() {
        return fontField.replaceAll(".*style=|,.*", "");
    }

    public int getFontStyleAwt() {
        int i = 0;
        String ss = fontField.replaceAll(".*style=|,.*", "");
        switch (ss) {
            case "Regular": i = Font.PLAIN; break;
            case "Bold": i = Font.BOLD; break;
            case "Italic": i = Font.ITALIC; break;
            case "Bold Italic": i = Font.ITALIC | Font.BOLD; break;
        }
        return i;
    }

    public double getFontSize() {
        return Double.parseDouble(fontField.replaceAll(".*size=|].*", ""));
    }

    public int getFontSizeAwt() {
        double dSize = Double.valueOf(fontField.replaceAll(".*size=|].*", ""));
        int iSyze = (int) dSize;
        return iSyze;
    }

    public int getNum() {
        return num;
    }

    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }

    public void setTpType(int tpType) {
        this.tpType = tpType;
    }

    public void setChoise(int choise) {
        this.choise = choise;
    }

    public void setText(String textField) {
        this.textField = textField;
    }

    public void setFont(String fontField) {
        this.fontField = fontField;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "TextPrintParam [idDistr=" + idDistr + ", tpType=" + tpType + ", choise=" + choise + ", textField=" + textField + ", fontField=" + fontField + ", num=" + num + "]";
    }
}
