package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.util.ColorConverter;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CategoryDAOSQL implements ICategoryDAO {
    protected static Logger logger = LogManager.getLogger(CategoryDAOSQL.class);
    private final ConnectionManager cm;
    private final ConnectionManager cmAccess;

    public CategoryDAOSQL(ConnectionManager cm, ConnectionManager cmAccess) {
        this.cm = cm;
        this.cmAccess = cmAccess;
    }

    /**
     * ��������� ������ category ���������� ������ �� ��
     * @param category {@link Category} ������ ��� ����������
     * @param rs {@link ResultSet} ������ �� ��
     * @throws DataManageException
     */
    private void fillItem(Category category, ResultSet rs) throws DataManageException {
        try {
            category.setIdCat(rs.getInt("IDcat"));
            category.setName(rs.getString("name").trim());
            category.setPriority(rs.getInt("prioritet_"));
            category.setTrMin(rs.getInt("trMin"));
            category.setTrMax(rs.getInt("trMax"));
            category.setType(rs.getString("type").trim());
            if (cm.isHSQLDB()) {
                Integer clr = rs.getInt("color");
                Color color = ColorConverter.convert(clr.intValue());
                //Color color = new Color(clr, false);
                category.setColor(color);
            } else {
                try {
                    Object clr = rs.getObject("color");
                    Color color = ColorConverter.convert(clr);
                    category.setColor(color);
                } catch (Exception e) {
                    logger.error(category + ": " + e.toString(), e);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("Category: " + category, e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public List<Category> getCategories() throws DataManageException {
        List<Category> categoryList = new ArrayList<>();
        String sql = "SELECT IDcat, name, prioritet_, trMin, trMax, type, color FROM Category " +
                "ORDER BY trMin";
        try {
            Connection cn = cm.getConnection();
            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    Category category = new Category();
                    fillItem(category, rs);
                    categoryList.add(category);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return categoryList;
    }

    /**{@inheritDoc}*/
    @Override
    public Category getCategory(int idCat) throws DataManageException {
        Category category = null;
        String sql = "SELECT IDcat, name, prioritet_, trMin, trMax, type, color FROM Category " +
                " WHERE IDcat = " + idCat +
                " ORDER BY trMin";
        try {
            Connection cn = cm.getConnection();
            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);) {
                if (rs.next()) {
                    category = new Category();
                    fillItem(category, rs);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return category;
    }

    @Override
    public void saveToDB(List<Category> categoryList, List<Category> deletedCategoryList) throws DataManageException {
        deleteFromCategory(deletedCategoryList);
        String updCategory = "UPDATE Category SET name=?, prioritet_=?, trMin=?, trMax=?, type=?, color=? WHERE IDcat = ?";
        String insCategory = "INSERT INTO Category(name, prioritet_, trMin, trMax, type, color, bType) VALUES(?,?,?,?,?,?,?)";
        String maxCategoryID = "SELECT MAX(IDcat) FROM Category";

        try {
            Connection cn = cm.getConnection();
            try (PreparedStatement psUpdCategory = cn.prepareStatement(updCategory);
                 PreparedStatement psInsCategory = cn.prepareStatement((insCategory));
                 Statement stMaxIdCategory = cn.createStatement())
            {
                for (Category category : categoryList) {
                    if (category.getIdCat() > 0) {
                        psUpdCategory.setString(1,category.getName());
                        psUpdCategory.setInt(2,category.getPriority());
                        psUpdCategory.setInt(3,category.getTrMin());
                        psUpdCategory.setInt(4,category.getTrMax());
                        psUpdCategory.setString(5,category.getType().getString());
                        if (cm.isHSQLDB()) {
                            psUpdCategory.setInt(6, ColorConverter.convert(category.getColor()));
                        } else {
                            psUpdCategory.setObject(6, ColorConverter.convertObj(category.getColor()));
                        }
                        psUpdCategory.setInt(7,category.getIdCat());
                        psUpdCategory.executeUpdate();
                    } else {
                        psInsCategory.setString(1,category.getName());
                        psInsCategory.setInt(2,category.getPriority());
                        psInsCategory.setInt(3,category.getTrMin());
                        psInsCategory.setInt(4,category.getTrMax());
                        psInsCategory.setString(5,category.getType().getString());
                        if (cm.isHSQLDB()) {
                            psUpdCategory.setInt(6, ColorConverter.convert(category.getColor()));
                        } else {
                            psInsCategory.setObject(6, ColorConverter.convertObj(category.getColor()));
                        }
                        psInsCategory.setBoolean(7,false);
                        psInsCategory.executeUpdate();

                        //����� ID �� ������� Category
                        int categoryID = 0;
                        ResultSet rs = stMaxIdCategory.executeQuery(maxCategoryID);
                        if (rs.next()) {
                            categoryID = rs.getInt(1);
                        } else {
                            throw new SQLException("Category max ID was failed !!!");
                        }
                        category.setIdCat(categoryID);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private void deleteFromCategory(List<Category> deletedCategoryList) throws DataManageException {
        if (deletedCategoryList.size() > 0) {
            String sql = "DELETE FROM Category WHERE IDcat = ?";
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement ps = cn.prepareStatement(sql)) {
                    for (Category category : deletedCategoryList) {
                        ps.setInt(1, category.getIdCat());
                        ps.addBatch();
                    }
                    ps.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(e.toString(), e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    @Override
    public void refreshToDB() throws DataManageException {
        List<Category> categoryList = getCategories();
        String updCategoryTrains = "UPDATE Train SET IDCat = ? WHERE (code >= ?) AND (code <=?)";
        try {
            Connection cnAccess = cmAccess.getConnection();
            try (PreparedStatement psUpdCategoryTrains = cnAccess.prepareStatement(updCategoryTrains);) {
                for (Category category : categoryList) {
                    if (category.getTrMin() > 0 && category.getTrMax() > 0) {
                        psUpdCategoryTrains.setInt(1, category.getIdCat());
                        psUpdCategoryTrains.setInt(2, category.getTrMin());
                        psUpdCategoryTrains.setInt(3, category.getTrMax());
//                        logger.info(category.getIdCat());
                        psUpdCategoryTrains.executeUpdate();
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }
}
