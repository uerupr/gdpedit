package com.gmail.alaerof.dao.dto.gdp;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * ���������� ����� ������ �� �������
 * 
 * @author Helen Yrofeeva
 * 
 */
public class LineStTrain {
    private static Logger logger = LogManager.getLogger(LineStTrain.class);
    private int idLineSt; // IDlineSt
    private int idSt; // IDst
    private long idTrain; // IDtrain
    private String occupyOn; // ����� ������� ���� ������� ������� (00:00)
    private String occupyOff; // ����� ������������ ���� ������� ������� (00:00)
    /** ����� ������� �������� (��� ������� ���)*/
    private int tstopPL; // TstopPL
    /** ����� ������� ��������� */
    private int tstop; // Tstop
    /** ����� ������ ������������ �� ������� (0-���; 1-�� �����; 2-�� �������) */
    private int stop; // Stop
    /** ����������� ����� ������� */
    private int texStop; // TtexStop
    /** ������� ���. ��������� ��� ����. ������� */
    private String sk_ps = ""; // sk_ps
    /** ������� ��� occupyOn ��� null �.�. ������� ����������� */
    private boolean nullOn;
    /** ������� ��� occupyOff ��� null �.�. ������� �������� */
    private boolean nullOff;
    /**
     * ����� ����. ������ ��� ��������, ������������� ���� �������� �� ����
     * ��������
     */
    private int overTime;

    public int getIdLineSt() {
        return idLineSt;
    }

    public void setIdLineSt(int idLineSt) {
        this.idLineSt = idLineSt;
    }

    public int getIdSt() {
        return idSt;
    }

    public void setIdSt(int idSt) {
        this.idSt = idSt;
    }

    public long getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }

    public String getOccupyOn() {
        return occupyOn;
    }

    public void setOccupyOn(String occupyOn) {
        this.occupyOn = occupyOn;
    }

    public String getOccupyOff() {
        return occupyOff;
    }

    public void setOccupyOff(String occupyOff) {
        this.occupyOff = occupyOff;
    }

    public int getTstopPL() {
        return tstopPL;
    }

    public void setTstopPL(int tstopPL) {
        this.tstopPL = tstopPL;
    }

    public int getTstop() {
        return tstop;
    }

    public void setTstop(int tstop) {
        this.tstop = tstop;
    }

    public int getStop() {
        return stop;
    }

    public void setStop(int stop) {
        this.stop = stop;
    }

    public int getTexStop() {
        return texStop;
    }

    public boolean isNullOn() {
        return nullOn;
    }

    public void setNullOn(boolean nullOn) {
        this.nullOn = nullOn;
    }

    public boolean isNullOff() {
        return nullOff;
    }

    public void setNullOff(boolean nullOff) {
        this.nullOff = nullOff;
    }

    /**
     * @param texStop TtexStop
     */
    public void setTexStop(Integer texStop) {
        if (texStop != null) {
            this.texStop = texStop;
        }
    }

    public String getSk_ps() {
        return sk_ps;
    }

    public void setSk_ps(String sk_ps) {
        if (sk_ps != null) {
            this.sk_ps = sk_ps;
        }
    }

    public int getOverTime() {
        return overTime;
    }

    public void setOverTime(int overTime) {
        this.overTime = overTime;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + idSt;
        result = prime * result + (int) (idTrain ^ (idTrain >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LineStTrain other = (LineStTrain) obj;
        if (idSt != other.idSt)
            return false;
        if (idTrain != other.idTrain)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TrainStTime [idSt=" + idSt + ", idTrain=" + idTrain + ", occupyOn=" + occupyOn
                + ", occupyOff=" + occupyOff + ", sk_ps=" + sk_ps + "]";
    }

}
