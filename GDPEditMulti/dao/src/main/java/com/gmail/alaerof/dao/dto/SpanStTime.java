package com.gmail.alaerof.dao.dto;
/**
 * ����� ���� ����� �.�. ��������
 * @author Helen Yrofeeva
 *
 */
public class SpanStTime implements Comparable<SpanStTime>{
    private int idScale; //IDscale
    private int idLType; // IDltype
    /** ���������� ����� �.�. ����� �������� ������ ����� ����
     * num=0 ��������:
     * ����� ���� �� ���������� �.� �� ������� �.�. ��� �����
     * ����� ���� �� ��������� �.�. �� ���������� �.�. ��� ��� */
    private int num;  
    /** ����� ���� ����� �� ������, ��� */
    private double tmoveO; // Tmove_o  
    /** ����� ���� ��� �� ������, ��� */
    private double tmoveE; // Tmove_e  
    
    
    public int getIdScale() {
        return idScale;
    }
    
    public void setIdScale(int idSpan) {
        this.idScale = idSpan;
    }
    
    public int getIdLType() {
        return idLType;
    }
    
    public void setIdLType(int idLType) {
        this.idLType = idLType;
    }
    
    public int getNum() {
        return num;
    }
    
    public void setNum(int num) {
        this.num = num;
    }
    
    public double getTmoveO() {
        return tmoveO;
    }
    
    public void setTmoveO(double tmoveO) {
        this.tmoveO = tmoveO;
    }
    
    public double getTmoveE() {
        return tmoveE;
    }
    
    public void setTmoveE(double tmoveE) {
        this.tmoveE = tmoveE;
    }

    @Override
    public int compareTo(SpanStTime tm) {
        int r = this.getIdLType() - tm.getIdLType();
        if(r==0){
            r = this.getNum() - tm.getNum();
        }
        return r;
    }
    
    
}
