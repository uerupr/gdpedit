package com.gmail.alaerof.util;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.factory.IGDPDAOFactory;

/**
 * @author Helen Yrofeeva
 */
public class ConnectionDBFileHandlerUtil {
    public static final String dbResPath = "connectionconfig";
    public final static Charset charsetWin = Charset.forName("cp1251"); // "CP1251"
                                                                        // "US-ASCII"
    public final static Charset charsetUTF = Charset.forName("UTF-8");
    private static Logger logger = LogManager.getLogger(ConnectionDBFileHandlerUtil.class);

    public static Properties loadPoolInfo(String base) {
        Properties poolInfo = new Properties();
        File file = new File(dbResPath + File.separator + base + ".properties");
        try {
            InputStreamReader streamReader = new InputStreamReader(new FileInputStream(file), charsetWin);
            poolInfo.load(streamReader);
        } catch (FileNotFoundException e) {
            logger.error("���� � ����������� ����������� �� ������ " + e.toString(), e);
        } catch (IOException e) {
            logger.error("������ ������ ���� � �� " + e.toString(), e);
        }
        return poolInfo;
    }

    public static void savePoolInfo(String base, String[] properties) {
        File file = new File(dbResPath + File.separator + base);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            try {
                for (String property : properties) {
                    bw.write(property);
                    bw.write("\n");
                }
                bw.flush();
            } finally {
                bw.close();
            }
        } catch (IOException e) {
            logger.error("������ ������ �������� ����������� " + e.toString(), e);
        }
    }

    /**
     * ���������� Properties �� ini-�����:
     * ��� ��������� ������ [Server, Local], �������� ��
     * DataSourceType=Server
     * MDB-SERVER=GDP.mdb
     * MDB-Local=GDP_local.mdb
     * HSQLDB=true
     * printPDF=true
     * oldJob=false
     * @return  Properties
     */
    public static Properties readGDPEditINI() {
        Properties res = new Properties();
        String [] propertyNames = {"MDB-SERVER", "MDB-LOCAL", "HSQLDB", "printPDF", "oldJob", "printAPI", "printPDFServiceName", "title"};
        File file = new File("GDPEdit.ini");
        String line;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),
                charsetUTF))) {
            while ((line = br.readLine()) != null) {
                if (Pattern.matches("(SERVER)(//s)*=(.+)", line)) {
                    String[] s = line.split("=");
                    String value = new String(s[1].trim());
                    if ("1".equals(value)) {
                        res.setProperty("DataSourceType", "Server");
                    } else {
                        res.setProperty("DataSourceType", "Local");
                    }
                }
                for (String propertyName : propertyNames) {
                    if (Pattern.matches("(" + propertyName + ")(//s)*=(.+)", line)) {
                        String[] s = line.split("=");
                        String value = new String(s[1].trim());
                        res.setProperty(propertyName, value);
                    }

                }
            }
        } catch (IOException | NumberFormatException e) {
            logger.error("������ ������ �������� ����������� " + e.toString(), e);
        }
        return res;
    }

}
