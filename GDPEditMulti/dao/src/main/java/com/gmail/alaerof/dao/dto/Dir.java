package com.gmail.alaerof.dao.dto;

import java.util.Objects;

/**
 * ��������� ������ (�����������)
 * 
 * @author Helen Yrofeeva
 * 
 */
public class Dir {
    private int idDir; // IDdir
    private String name = ""; // Name
    private String ps = ""; // PS
    private int ord;

    public int getIdDir() {
        return idDir;
    }

    public void setIdDir(int idDir) {
        this.idDir = idDir;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        if (ps != null) {
            this.ps = ps;
        }
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    @Override
    public String toString() {
        return "Dir [idDir=" + idDir + ", name=" + name + ", ord=" + ord + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dir dir = (Dir) o;
        return idDir == dir.idDir;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDir);
    }
}
