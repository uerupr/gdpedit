package com.gmail.alaerof.dao.dto.gdp;

/**
 * @author Helen Yrofeeva
 */
public enum OE {
    odd, even;

    public String getString() {
        switch (this) {
        case odd:
            return "�����";
        case even:
            return "���";
        default:
            return null;
        }
    }

    public static OE getValue(int code) {
        if (code % 2 == 0) {
            return even;
        } else {
            return odd;
        }
    }

    public static OE getValue(String name) {
        switch (name) {
            case "�����":
                return odd;
            case "���":
                return even;
            default:
                throw new IllegalArgumentException("name: " + name);
        }
    }
}