package com.gmail.alaerof.dao.interfacedao;

import java.util.List;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.SpanStTime;
import com.gmail.alaerof.dao.dto.SpanTime;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;

/**
 * @author Helen Yrofeeva
 */
public interface ISpanTimeDAO {
    /**
     * ���������� ���������� ������� ����
     * @param idSpan �� ��������
     * @param lt ������ ����� ����������� (��������)
     * @return ������ SpanTime, ���������� ��� ���������� ������� ���� ��� �������� ����� ����������� (���
     *         ��������)
     * @throws DataManageException
     */
    SpanTime getSpanTime(int idSpan, List<LocomotiveType> lt) throws DataManageException;

    /**
     * ���������� ������ ������ ���� ����� �.�. ��������
     * @param idSpan �� ��������
     * @return ������ �������� SpanStTime ������ ���� ����� �.�. ��������
     * @throws DataManageException
     */
    List<SpanStTime> getSpanStTime(int idSpan) throws DataManageException;

    /**
     * ��������� ������� ���� ����� �.�. � ��
     * @param list ������ ������ ����
     * @throws DataManageException
     */
    void saveSpanStTime(List<SpanStTime> list) throws DataManageException;

    /**
     * ��������� ��������� ������� ���� � ��
     * @param idSpan �� ��������
     * @param spanTime ���������� ������� ����
     * @throws DataManageException
     */
    void saveSpanTime(int idSpan, SpanTime spanTime) throws DataManageException;
}
