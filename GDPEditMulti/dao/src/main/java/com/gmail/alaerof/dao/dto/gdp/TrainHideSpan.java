package com.gmail.alaerof.dao.dto.gdp;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class TrainHideSpan {
    private long idTrain; // IDtrain
    private int idDistr; // IDdistr
    /** ��� �������, ����� ������� ���������� ����� */
    private int idStB; // IDstB
    /** ��� ����� */
    private String shape; // �������� �(���) ��������� (;hor;span)
    
    public long getIdTrain() {
        return idTrain;
    }
    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }
    public int getIdDistr() {
        return idDistr;
    }
    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }
    public int getIdStB() {
        return idStB;
    }
    public void setIdStB(int idStB) {
        this.idStB = idStB;
    }
    public String getShape() {
        return shape;
    }
    public void setShape(String shape) {
        this.shape = shape.trim();
    }
    
    public boolean isHor(){
        return shape.contains("hor");
    }
    
    public boolean isSpan(){
        return shape.contains("span");
    } 
    
    @Override
    public String toString() {
        return "TrainHideSpan [idTrain=" + idTrain + ", idDistr=" + idDistr + ", idStB=" + idStB + ", shape="
                + shape + "]";
    }
    
    
}
