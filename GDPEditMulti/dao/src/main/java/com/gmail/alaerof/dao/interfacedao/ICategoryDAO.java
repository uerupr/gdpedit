package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.List;

/**
 * @author Helen Yrofeeva
 */
public interface ICategoryDAO {
    /**
     * ���������� ������ ���� ���������
     * @return {@link Category} ������
     */
    List<Category> getCategories() throws DataManageException;

    /**
     * ���������� ��������� �� ID
     * @param idCat ID ���������
     * @return {@link Category}
     */
    Category getCategory(int idCat) throws DataManageException;

    /**
     * ��������� � �� ������ ���������
     * @param categoryList ������ {@link Category}
     * @throws DataManageException
     */
    public void saveToDB(List<Category> categoryList, List<Category> deletedCategoryList) throws DataManageException;

    public void refreshToDB() throws DataManageException;
}
