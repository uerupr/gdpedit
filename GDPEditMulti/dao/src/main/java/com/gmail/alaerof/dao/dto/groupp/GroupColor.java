package com.gmail.alaerof.dao.dto.groupp;

/**
 * ������ ������� �� �����
 */
public class GroupColor {
    private int idColorGR = -1;
    private int idGroup;
    private int color;

    public int getIdColorGR() {
        return idColorGR;
    }

    public void setIdColorGR(int idColorGR) {
        this.idColorGR = idColorGR;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "GroupColor{" +
                "idColorGR=" + idColorGR +
                ", idGroup=" + idGroup +
                ", color=" + color +
                '}';
    }
}
