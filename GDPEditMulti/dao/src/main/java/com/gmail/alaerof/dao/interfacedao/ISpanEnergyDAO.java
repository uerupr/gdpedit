package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.energy.ExpenseRate;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateEnergy;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateNormative;
import com.gmail.alaerof.dao.dto.energy.SpanEnergy;
import com.gmail.alaerof.dao.dto.energy.SpanEnergyTract;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.List;

public interface ISpanEnergyDAO {
    /**
     * ���������� ���������� ������� �������
     * @param idScale �� ��������
     * @param lt ������ ����� ����������� (��������)
     * @return ������ SpanEnergy, ���������� ��� ���������� ������� ������� ��� �������� ����� �����������
     *         (��� ��������)
     * @throws DataManageException
     */
    SpanEnergy getSpanEnergy(int idScale, List<LocomotiveType> lt) throws DataManageException;

    /**
     * ��������� ��� ���������� ������� ������� � ��
     * @param idScale �� ��������
     * @param spanEnergy ���������� ������� �������
     * @throws DataManageException
     */
    void saveSpanEnergy(int idScale, SpanEnergy spanEnergy) throws DataManageException;

    /**
     *
     * @param spanEnergyTract
     * @throws DataManageException
     */
    void saveSpanEnergyTract(SpanEnergyTract spanEnergyTract) throws DataManageException;

    /**
     * @param spanEnergyTractList
     * @throws DataManageException
     */
    void saveSpanEnergyTractList(List<SpanEnergyTract> spanEnergyTractList) throws DataManageException;

    /**
     * @param expenseRateEnergyList
     * @throws DataManageException
     */
    void saveExpenseRateEnergy(List<ExpenseRateEnergy> expenseRateEnergyList) throws DataManageException;

    /**
    * @return List<ExpenseRateEnergy>
     */
    List<ExpenseRateEnergy> getExpenseRateEnergy() throws DataManageException;

    /**
     * @param expenseRateList
     * @throws DataManageException
     */
    void saveExpenseRate(List<ExpenseRate> expenseRateList) throws DataManageException;

    /**
     * @return List<ExpenseRate>
     */
    List<ExpenseRate> getExpenseRate() throws DataManageException;

    /**
     * @param expenseRateNormativeList
     * @throws DataManageException
     */
    void saveExpenseRateNormative(List<ExpenseRateNormative> expenseRateNormativeList) throws DataManageException;

    /**
     * @return List<ExpenseRateNormative>
     */
    List<ExpenseRateNormative> getExpenseRateNormative() throws DataManageException;
}
