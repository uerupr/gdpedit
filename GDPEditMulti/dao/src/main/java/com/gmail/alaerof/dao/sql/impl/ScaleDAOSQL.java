package com.gmail.alaerof.dao.sql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.IScaleDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Helen Yrofeeva
 */
public class ScaleDAOSQL implements IScaleDAO {
    protected static Logger logger = LogManager.getLogger(ScaleDAOSQL.class);
    private final ConnectionManager cm;

    public ScaleDAOSQL(ConnectionManager cm) {
        this.cm = cm;
    }

    @Override
    public List<Scale> getListScale(int idDistr) throws DataManageException {
        List<Scale> listS = new ArrayList<>();
        Scale s = null;
        String sql = "SELECT Scale.IDscale, Scale.IDdistr, Scale.IDspan, Scale.num, Scale.scale, " +
                "Scale.fSt, Scale.lSt, Scale.OpenStf, Scale.OpenStl, Scale.WidthLine_fst, Scale.WidthLine_lst, " +
                "Scale.scale_lst, Scale.absKM, Scale.absKM_e, Scale.km, Scale.pic, Scale.m, " +
                "Scale.km_e, Scale.pic_e ,Scale.m_e, Scale.L, " +
                "Span.Name, Span.SCB, Span.line, Span.IDst, "
                + "Span.IDst_e, Span.BR, Span.IDdir "
                + "FROM Span INNER JOIN Scale ON Span.IDspan = Scale.IDspan " + "WHERE Scale.IDdistr = "
                + idDistr + " ORDER BY Scale.num";

        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    s = new Scale();
                    fillItem(s, rs);
                    listS.add(s);
                    fillListSpanSt(s);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return listS;
    }

    @Override
    public Scale getScale(int idDistr, int idSpan) throws ObjectNotFoundException,
            DataManageException {
        Scale s = null;
        String sql = "SELECT Scale.IDscale, Scale.IDdistr, Scale.IDspan, Scale.num, Scale.scale, " +
                "Scale.fSt, Scale.lSt, Scale.OpenStf, Scale.OpenStl, Scale.WidthLine_fst, Scale.WidthLine_lst, " +
                "Scale.scale_lst, Scale.absKM, Scale.absKM_e, Scale.km, Scale.pic, Scale.m, " +
                "Scale.km_e, Scale.pic_e ,Scale.m_e, Scale.L, " +
                "Span.Name, Span.SCB, Span.line, Span.IDst, "
                + "Span.IDst_e, Span.BR, Span.IDdir "
                + "FROM Span INNER JOIN Scale ON Span.IDspan = Scale.IDspan " + "WHERE Scale.IDdistr = "
                + idDistr + " AND Span.IDspan = " + idSpan + " ORDER BY Scale.num";

        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                if (rs.next()) {
                    s = new Scale();
                    fillItem(s, rs);
                    fillListSpanSt(s);
                } else {
                    throw new ObjectNotFoundException("Scale IDspan = " + idSpan);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return s;
    }

    @Override
    public void updateWidthLine(Scale scale) throws DataManageException {
        String sqlUpdate = "UPDATE Scale SET WidthLine_fst = ?, WidthLine_lst = ? " +
                " WHERE IDscale = ?";
        try {
            Connection cn = cm.getConnection();
            try (PreparedStatement pst = cn.prepareStatement(sqlUpdate)) {
                pst.setInt(1, scale.getWidthLine_fst());
                pst.setInt(2, scale.getWidthLine_lst());
                pst.setInt(3, scale.getIdScale());
                pst.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    @Override
    public void updateScale(List<Scale> scaleList) throws DataManageException {
        String sqlUpdate = "UPDATE Scale SET fSt = ?, lSt = ?, scale = ?  " +
                " WHERE IDscale = ?";
        try {
            Connection cn = cm.getConnection();
            try (PreparedStatement pst = cn.prepareStatement(sqlUpdate)) {
                for (int i = 0; i < scaleList.size(); i++) {
                    pst.setBoolean(1, scaleList.get(i).isfSt());
                    pst.setBoolean(2, scaleList.get(i).islSt());
                    pst.setInt(3, scaleList.get(i).getScale());
                    pst.setInt(4, scaleList.get(i).getIdScale());
                    pst.executeUpdate();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private void fillItem(Scale s, ResultSet rs) throws DataManageException {
        try {
            // ���� ���� ��������� ������ �� ������� ������
            // Scale
            s.setIdScale(rs.getInt("IDscale"));
            s.setIdDistr(rs.getInt("IDdistr"));
            s.setIdSpan(rs.getInt("IDspan"));
            s.setNum(rs.getInt("num"));
            s.setScale(rs.getInt("scale"));
            s.setfSt(rs.getBoolean("fSt"));
            s.setlSt(rs.getBoolean("lSt"));
            s.setOpenStf(rs.getBoolean("OpenStf"));
            s.setOpenStl(rs.getBoolean("OpenStl"));
            s.setWidthLine_fst(rs.getInt("WidthLine_fst"));
            s.setWidthLine_lst(rs.getInt("WidthLine_lst"));
            s.setScale_lst(rs.getInt("scale_lst"));
            s.setAbsKM(rs.getFloat("absKM"));
            s.setAbsKM_E(rs.getFloat("absKM_e"));
            s.setKm(rs.getInt("km"));
            s.setPic(rs.getInt("pic"));
            s.setM(rs.getInt("m"));
            s.setKmE(rs.getInt("km_e"));
            s.setPicE(rs.getInt("pic_e"));
            s.setmE(rs.getInt("m_e"));
            s.setL(rs.getInt("L"));
            // Span
            s.setName(rs.getString("Name"));
            s.setScb(rs.getString("SCB"));
            s.setLine(rs.getInt("line"));
            s.setIdSt(rs.getInt("IDst"));
            s.setIdStE(rs.getInt("IDst_e"));
            s.setBr(rs.getBoolean("BR"));
            s.setIdDir(rs.getInt("IDdir"));

        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Scale " + s, e);
        }
    }

    private void fillSpanSt(SpanSt spSt, ResultSet rs) throws DataManageException {
        try {
            spSt.setIdSpanst(rs.getInt("IDspanst"));
            spSt.setIdSpan(rs.getInt("IDspan"));
            spSt.setNum(rs.getInt("num"));
            spSt.setKm(rs.getInt("km"));
            spSt.setPic(rs.getInt("pic"));
            spSt.setM(rs.getInt("m"));
            spSt.setL2(rs.getInt("L2"));
            spSt.setL1(rs.getInt("L1"));
            spSt.setName(trim(rs.getString("name")));
            spSt.setCodeESR(trim(rs.getString("CodeESR")));
            spSt.setCodeExpress(trim(rs.getString("CodeExpress")));
            spSt.setTstop(rs.getInt("Tstop"));
            spSt.setN2(rs.getInt("N2"));
            spSt.setN1(rs.getInt("N1"));
            spSt.setAbsKM(rs.getFloat("absKM"));
            spSt.setCodeESR2(rs.getString("CodeESR2").trim());
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Span_St " + spSt, e);
        }
    }

    private String trim(String str) {
        if (str != null) return str.trim();
        return str;
    }

    private void fillListSpanSt(Scale s) throws DataManageException {
        List<SpanSt> list = new ArrayList<SpanSt>();
        s.setSpanSt(list);
        SpanSt spSt = null;
        String sql = "SELECT IDspanst, IDspan, num, km, pic, m, L2, L1, name, CodeESR, CodeExpress, "
                + "Tstop, N2, N1, absKM, CodeESR2 FROM Span_St WHERE IDspan = " + s.getIdSpan()
                + " ORDER BY num";
        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    spSt = new SpanSt();
                    fillSpanSt(spSt, rs);
                    list.add(spSt);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

}
