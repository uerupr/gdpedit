package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.IDistrGDPXMLDAO;
import com.gmail.alaerof.util.CategoryUtil;
import com.gmail.alaerof.util.TimeConverter;
import com.gmail.alaerof.util.XMLUtil;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javafx.util.Pair;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class DistrGDPXMLDAOSQL implements IDistrGDPXMLDAO {
    protected static Logger logger = LogManager.getLogger(DistrGDPXMLDAOSQL.class);
    private final ConnectionManager cmServer;
    private final ConnectionManager cmAccess;
    private Set<String> deprecatedColumns;
    private String allIDst;
    private String allIDspan;
    private static final String decimalPattern = "####.##";
    private DecimalFormat decimalFormatter;
    private Map<QueryName, Map<String, String>> xmlTagMap;

    {
        deprecatedColumns = new HashSet<>();
        deprecatedColumns.add("HDtxt".toLowerCase());
        deprecatedColumns.add("PARAMtxt".toLowerCase());
    }

    public DistrGDPXMLDAOSQL(ConnectionManager cmServer, ConnectionManager cmAccess) {
        this.cmServer = cmServer;
        this.cmAccess = cmAccess;
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        decimalFormatter = new DecimalFormat(decimalPattern, otherSymbols);

        String resourceName = "xmltag.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties xmlTags = new Properties();
        try {
            try(InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
                xmlTags.load(resourceStream);
            }
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }

        xmlTagMap = buildXmlTagMap(xmlTags);
    }

    /**
     * @param xmlTagsProperties properties
     * @return map
     */
    private Map<QueryName, Map<String, String>> buildXmlTagMap(Properties xmlTagsProperties) {
        Map<QueryName, Map<String, String>> map = new HashMap<>();
        for (QueryName queryName : QueryName.values()) {
            String xmlTags = xmlTagsProperties.getProperty(queryName.toString());
            if (xmlTags != null) {
                String[] tags = xmlTags.split(",");
                Map<String, String> tagMap = new HashMap<>();
                map.put(queryName, tagMap);
                for (String tag : tags) {
                    tag = tag.trim();
                    tagMap.put(tag.toUpperCase(), tag);
                }
            }
        }
        return map;
    }

    /**{@inheritDoc}*/
    @Override
    public Element buildDistrGDPXML(QueryName tg, int idDistr, Document doc) {
        Element element = doc.createElement(tg.name() + "s");
        if (QueryName.Distr.equals(tg)) {
            buildDistrXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Span.equals(tg)) {
            buildSpanXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.SpanTime.equals(tg)) {
            buildSpanTimeXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Station.equals(tg)) {
            buildStationXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.LineSt.equals(tg)) {
            buildLineStXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Span_St.equals(tg)) {
            buildSpanStXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Distr_Train.equals(tg)) {
            buildDistrTrainXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.SpanTrain.equals(tg)) {
            buildSpanTrainXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.LineSt_Tr.equals(tg)) {
            buildLineStTrainXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.TrainStop.equals(tg)) {
            buildTrainStopXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.SpTOcc.equals(tg)) {
            buildSpTOccXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.TrainCode.equals(tg)) {
            buildTrainCodeXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.HideSpan.equals(tg)) {
            buildHideSpanXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.LineStyleSpan.equals(tg)) {
            buildLineStyleSpanXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.ShowParam.equals(tg)) {
            buildShowParamXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.DistrComb.equals(tg)) {
            buildDistrCombXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Distr_TrainF.equals(tg)) {
            buildDistrTrainFXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.LineSt_TrF.equals(tg)) {
            buildLineStTrFXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Train_SpanColor.equals(tg)) {
            builTrainSpanColorXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Train_SpanInscr.equals(tg)) {
            builTrainSpanInscrXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Category.equals(tg)) {
            buildCategoryXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.LocomotiveType.equals(tg)) {
            buildLocomotiveTypeXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Span_StTime.equals(tg)) {
            buildSpanStTimeXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.Wind.equals(tg)) {
            buildWindXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.RouteD.equals(tg)) {
            buildRouteDXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.RouteDSp.equals(tg)) {
            buildRouteDSpXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.RouteDTR.equals(tg)) {
            buildRouteDTRXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.TextPrintParam.equals(tg)) {
            buildTextPrintParamXML(tg, idDistr, doc, element);
            return element;
        }
        if (QueryName.SpanEnergy.equals(tg)) {
            buildSpanEnergyXML(tg, idDistr, doc, element);
            return element;
        }


        return element;
    }

    /**
     * SpanEnergy
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildSpanEnergyXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlID = "SELECT IDscale FROM Scale WHERE IDdistr = " + idDistr;
        String allID = getIDs(sqlID);
        String sql = "SELECT IDscale, IDltype, EnMove, EnDown, EnUp, oe " +
                " FROM SpanEnergy " +
                " WHERE IDscale IN (" + allID + ")";
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * TextPrintParam
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildTextPrintParamXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDdistr, tpType, choise, textField, fontField, num " +
                "FROM GDPTextPrintParam WHERE IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * RouteD + RouteD_Train
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildRouteDTRXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT RouteD.IDdistr, RouteD_Train.IDrouteD, IDrouteDTR, trMin, trMax " +
                " FROM RouteD INNER JOIN RouteD_Train ON RouteD.IDrouteD = RouteD_Train.IDrouteD " +
                " WHERE RouteD.IDdistr = " + idDistr +
                " ORDER BY RouteD.IDrouteD, trMin" ;
        buildCommonXML(tg, doc, element, sql, cmServer);
    }

    /**
     * RouteD + RouteD_Span
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildRouteDSpXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT RouteD.IDdistr, RouteD_Span.IDrouteD, IDrouteDS, IDspan, num " +
                " FROM RouteD INNER JOIN RouteD_Span ON RouteD.IDrouteD = RouteD_Span.IDrouteD " +
                " WHERE RouteD.IDdistr =  " + idDistr +
                " ORDER BY RouteD.IDrouteD, num " ;
        buildCommonXML(tg, doc, element, sql, cmServer);
    }

    /**
     * RouteD
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildRouteDXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDrouteD, IDdistr, TypeR, NameR " +
                " FROM RouteD WHERE IDdistr =" + idDistr;
        buildCommonXML(tg, doc, element, sql, cmServer);
    }

    /**
     * Wind
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildWindXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDwind, IDspan, T1, T2, tm, Line, Cause, IDdistrW, Line_o, Line_e, Line_oe, y0 " +
                " FROM Wind WHERE IDdistrW = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * Span_StTime + Scale
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildSpanStTimeXML(QueryName tg, int idDistr, Document doc, Element element) {
        //SELECT * FROM Span_StTime WHERE IDscale IN (SELECT IDscale FROM Scale WHERE IDdistr = :idD)
        String sqlID = "SELECT IDscale FROM Scale WHERE IDdistr = " + idDistr;

        String allID = getIDs(sqlID);
        String sql = "SELECT IDscale, IDltype, num, Tmove_o, Tmove_e " +
                " FROM Span_StTime " +
                " WHERE IDscale IN (" + allID + ")";
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * LocomotiveType
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildLocomotiveTypeXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDltype, ltype, colorLoc " +
                "FROM LocomotiveType";
        buildCommonXML(tg, doc, element, sql, cmServer);
    }

    /**
     * Category
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildCategoryXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDcat, name, prioritet_, trMin, trMax, color, [type], bType, [base] FROM Category";
        if (cmServer.isHSQLDB()) {
            sql = "SELECT IDcat, name, prioritet_, trMin, trMax, color, type, bType, base FROM Category";
        }
        buildCommonXML(tg, doc, element, sql, cmServer);
    }

    /**
     * Train_SpanInscr
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void builTrainSpanInscrXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDtrain, IDstB, IDdistr, AInscr " +
                "FROM Train_SpanInscr WHERE IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * Train_SpanColor
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void builTrainSpanColorXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDtrain, IDstB, IDdistr, colorTR " +
                "FROM Train_SpanColor WHERE IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     *
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildLineStTrFXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlSt = "SELECT CodeExpress, CodeESR, CodeESR2  " +
                "FROM Station WHERE IDst = ?";
        String sqlLineSt = "SELECT LineSt.N, LineSt.PS " +
                "FROM LineSt WHERE IDlineSt = ?";
        String sqlLSTT = "SELECT IDlineSt, IDst, IDtrain, IDdistr, occupyOn, occupyOff, TstopPL, Tstop, Stop, TtexStop, sk_ps, endLeave, overTime " +
                "FROM LineSt_Train " +
                "WHERE (IDtrain IN (SELECT IDtrain FROM Distr_Train WHERE IDdistr IN " +
                "(SELECT IDdistrC FROM DistrComb WHERE IDdistr = " + idDistr + " ))) " +
                "AND IDst IN (" + this.allIDst + ") ORDER BY IDtrain";
        logger.debug(sqlLSTT);
        try {
            Connection cnSv = cmServer.getConnection();
            PreparedStatement psSt = cnSv.prepareStatement(sqlSt);
            PreparedStatement psLineSt = cnSv.prepareStatement(sqlLineSt);
            Connection cnAcs = cmAccess.getConnection();
            Statement stLSTT = cnAcs.createStatement();
            ResultSet rsLSTT = stLSTT.executeQuery(sqlLSTT);

            //printMetaData(rsLSTT.getMetaData());
            //printMetaData(psSt.getMetaData());
            //printMetaData(psLineSt.getMetaData());

            try {
                while (rsLSTT.next()) {
                    int idSt = rsLSTT.getInt("IDst");
                    int idLineSt = rsLSTT.getInt("IDlineSt");
                    psSt.setInt(1, idSt);
                    psLineSt.setInt(1,idLineSt);
                    ResultSet rsSt = psSt.executeQuery();
                    ResultSet rsLineSt = psLineSt.executeQuery();
                    rsSt.next();
                    rsLineSt.next();
                    buildQueryXML(tg, doc, element, rsLSTT, rsSt, rsLineSt);
                }
            } finally {
                if (psSt != null) {
                    psSt.close();
                }
                if (psLineSt != null) {
                    psLineSt.close();
                }
                if (stLSTT != null) {
                    stLSTT.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * Distr_Train + DistrComb + Train
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildDistrTrainFXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT Distr_Train.IDdistr, Distr_Train.IDtrain, Distr_Train.ColorTR, " +
                "Train.Name, Train.code " +
                "FROM Train INNER JOIN Distr_Train ON Train.IDtrain = Distr_Train.IDtrain " +
                "WHERE Distr_Train.IDdistr IN (SELECT IDdistrC FROM DistrComb WHERE IDdistr = " + idDistr + " )";
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * DistrComb + Distr
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildDistrCombXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlX = "SELECT Distr.name " +
                "FROM Distr " +
                "WHERE IDdistr = ? "; //(DistrComb.IDdistrC)
        String sql = "SELECT IDdistrcomb, IDdistr, IDdistrC " +
                "FROM DistrComb " +
                "WHERE DistrComb.IDdistr = " + idDistr;
        logger.debug(sql);
        try {
            Connection cnSv = cmServer.getConnection();
            Connection cnAcs = cmAccess.getConnection();
            try (PreparedStatement psX = cnSv.prepareStatement(sqlX);
                 Statement st = cnAcs.createStatement();
                 ResultSet rs = st.executeQuery(sql))
            {
                while (rs.next()) {
                    int idX = rs.getInt("IDdistrC");
                    if (idX > 0) {
                        psX.setInt(1, idX);
                        ResultSet rsX = psX.executeQuery();
                        if (rsX.next()) {
                            buildQueryXML(tg, doc, element, rs, rsX);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * Distr_Train_ShowParam
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildShowParamXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT Distr_Train_ShowParam.IDtrain, IDdistr, hidden, ShowCodeI, colorTR, AInscr, LineStyle, widthTR, "+
                "Train.Name, Train.code " +
                "FROM Train INNER JOIN Distr_Train_ShowParam ON Train.IDtrain = Distr_Train_ShowParam.IDtrain " +
                "WHERE Distr_Train_ShowParam.IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * Train_LineStyleSpan
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildLineStyleSpanXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDtrain, IDstB, IDdistr, shape, LineStyle "+
                "FROM Train_LineStyleSpan WHERE IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * Train_HideSpan
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildHideSpanXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDtrain, IDstB, IDdistr, shape "+
                "FROM Train_HideSpan WHERE IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * TrainCode
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildTrainCodeXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDtrain, IDdistr, IDstB, ViewCode "+
                "FROM TrainCode WHERE IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * SpTOcc
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildSpTOccXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDspan, IDtrain, occupyOn, occupyOff, oe, NextDay, IDdistr, Tmove "+
                "FROM SpTOcc WHERE IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * TrainStop
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildTrainStopXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlSt = "SELECT IDspan, num, CodeExpress " +
                "FROM Span_St " +
                "WHERE IDspanst = ?";
        String sql = "SELECT  IDtrain, IDspanst, IDdistr, Tstop, occupyOn, occupyOff, sk_ps " +
                "FROM TrainStop " +
                "WHERE TrainStop.IDdistr = " + idDistr;
        logger.debug(sql);
        try {
            Connection cnSv = cmServer.getConnection();
            PreparedStatement psSt = cnSv.prepareStatement(sqlSt);
            Connection cnAcs = cmAccess.getConnection();
            Statement st = cnAcs.createStatement();
            ResultSet rs = st.executeQuery(sql);

            //printMetaData(rs.getMetaData());
            //printMetaData(psSt.getMetaData());

            try {
                while (rs.next()) {
                    int idSt = rs.getInt("IDspanst");
                    if (idSt > 0) {
                        psSt.setInt(1, idSt);
                        ResultSet rsSt = psSt.executeQuery();
                        rsSt.next();
                        buildQueryXML(tg, doc, element, rs, rsSt);
                    }
                }
            } finally {
                if (psSt != null) {
                    psSt.close();
                }
                if (st != null) {
                    st.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * LineSt_Train
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildLineStTrainXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlSt = "SELECT CodeExpress, CodeESR, CodeESR2  " +
                "FROM Station WHERE IDst = ?";
        String sqlLineSt = "SELECT LineSt.N, LineSt.PS " +
                "FROM LineSt WHERE IDlineSt = ?";
        String sqlLSTT = "SELECT IDlineSt, IDst, IDtrain, IDdistr, occupyOn, occupyOff, TstopPL, Tstop, Stop, TtexStop, sk_ps, endLeave, overTime " +
                "FROM LineSt_Train " +
                "WHERE (IDtrain IN (SELECT IDtrain FROM Distr_Train WHERE IDdistr = " + idDistr + ")) " +
                "AND (IDst IN ( " + this.allIDst + " )) ORDER BY IDtrain";
//        logger.debug(sqlSt);
//        logger.debug(sqlLineSt);
        logger.debug(sqlLSTT);
        try {
            Connection cnSv = cmServer.getConnection();
            PreparedStatement psSt = cnSv.prepareStatement(sqlSt);
            PreparedStatement psLineSt = cnSv.prepareStatement(sqlLineSt);
            Connection cnAcs = cmAccess.getConnection();
            Statement stLSTT = cnAcs.createStatement();
            ResultSet rsLSTT = stLSTT.executeQuery(sqlLSTT);

            //printMetaData(rsLSTT.getMetaData());
            //printMetaData(psSt.getMetaData());
            //printMetaData(psLineSt.getMetaData());

            try {
                while (rsLSTT.next()) {
                    int idSt = rsLSTT.getInt("IDst");
                    int idLineSt = rsLSTT.getInt("IDlineSt");
                    psSt.setInt(1, idSt);
                    psLineSt.setInt(1,idLineSt);
                    ResultSet rsSt = psSt.executeQuery();
                    ResultSet rsLineSt = psLineSt.executeQuery();
                    rsSt.next();
                    rsLineSt.next();
                    buildQueryXML(tg, doc, element, rsLSTT, rsSt, rsLineSt);
                }
            } finally {
                if (psSt != null) {
                    psSt.close();
                }
                if (psLineSt != null) {
                    psLineSt.close();
                }
                if (stLSTT != null) {
                    stLSTT.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * SpanTrain
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildSpanTrainXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDspan, IDtrain, Tmove, Tdown, Tup, Tw "+
                "FROM SpanTrain WHERE IDtrain IN " +
                "(SELECT IDtrain FROM Distr_Train WHERE IDdistr = " + idDistr + ")" +
                "AND IDspan IN (" + this.allIDspan + ")";
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * Distr_Train
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildDistrTrainXML(QueryName tg, int idDistr, Document doc, Element element) {
        /*String sql = "SELECT IDdistr_train, Distr_Train.IDtrain, IDdistr, occupyOff, oe, Tu, Tt, Vu, Vt, Eu, Et, IDltype, Messages, L, el, ps, hidden, ShowCodeI, colorTR, AInscr, LineStyle, widthTR, tOff, tOn, tOn_e, tOff_e, "+
                "Train.Name, Train.code, Train.type, Train.IDcat " +
                "FROM Train INNER JOIN Distr_Train ON Train.IDtrain = Distr_Train.IDtrain " +
                "WHERE Distr_Train.IDdistr = " + idDistr;*/
        String sql = "SELECT Distr_Train.IDtrain, IDdistr, occupyOff, oe, Tu, Tt, Vu, Vt, Eu, Et, IDltype, Messages, L, el, ps, hidden, ShowCodeI, colorTR, AInscr, LineStyle, widthTR, tOff, tOn, tOn_e, tOff_e, "+
                "Train.Name, Train.code, Train.type, Train.IDcat " +
                "FROM Train INNER JOIN Distr_Train ON Train.IDtrain = Distr_Train.IDtrain " +
                "WHERE Distr_Train.IDdistr = " + idDistr;
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * Span_St
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildSpanStXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDspanst, IDspan, num, km, pic, m, L2, L1, name, CodeESR, CodeExpress, Tstop, N2, N1, absKM, CodeESR2 "+
                " FROM Span_St WHERE IDspan IN " +
                "(SELECT IDspan FROM Scale WHERE IDdistr = " + idDistr + ")" +
                " ORDER BY num";
        buildCommonXML(tg, doc, element, sql, cmServer);
    }

    /**
     * LineSt
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildLineStXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sql = "SELECT IDlineSt, IDst, N, [close], PS, len, el, pz, gr_o, gr_e, pass_o, pass_e, CloseInGDP, colorLn ";
        if (cmServer.isHSQLDB()) {
            sql = "SELECT IDlineSt, IDst, N, close, PS, len, el, pz, gr_o, gr_e, pass_o, pass_e, CloseInGDP, colorLn ";
        }
        sql = sql + " FROM LineSt WHERE IDst IN (" + this.allIDst + ") ORDER BY IDst, N";
        buildCommonXML(tg, doc, element, sql, cmServer);
    }

    /**
     * Station
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildStationXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlID = "SELECT DISTINCT IDst FROM Station " +
                "WHERE (Station.IDst IN (SELECT Span.IDst    FROM Span WHERE IDspan IN (SELECT IDspan FROM Scale WHERE IDdistr = " + idDistr + "))) " +
                "OR    (Station.IDst IN (SELECT Span.IDst_e  FROM Span WHERE IDspan IN (SELECT IDspan FROM Scale WHERE IDdistr = " + idDistr + "))) ";

        this.allIDst = getIDs(sqlID);
        String sql = "SELECT IDst, CodeESR, CodeExpress, name, type, MaxMass_o, MaxMass_e, scheme, distr, PassBld, countLine_o, countLine_e, BR, joint, IDdir, CodeESR2 "+
                " FROM Station " +
                " WHERE IDst IN (" + this.allIDst + ")";
        buildCommonXML(tg, doc, element, sql, cmServer);
    }

    /**
     * SpanTime
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildSpanTimeXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlID = "SELECT IDspan FROM Scale WHERE IDdistr = " + idDistr;
        this.allIDspan = getIDs(sqlID);
        /*String sql = "SELECT IDspTime, IDspan, type, Tmove, Tdown, Tup, oe, Tw, IDltype "+
                "FROM SpanTime WHERE IDspan IN (" + this.allIDspan + ") ";*/
        String sql = "SELECT IDspan, type, Tmove, Tdown, Tup, oe, Tw, IDltype "+
                "FROM SpanTime WHERE IDspan IN (" + this.allIDspan + ") ";
        buildCommonXML(tg, doc, element, sql, cmAccess);
    }

    /**
     * Builds string with required IDs
     * @return string
     */
    private String getIDs(String sql) {
        StringBuilder ids = new StringBuilder();
        logger.debug(sql);

        try {
            Connection cnSv = cmServer.getConnection();
            Statement st = cnSv.createStatement();
            ResultSet rs = st.executeQuery(sql);

            //printMetaData(rs.getMetaData());
            try {
                while (rs.next()) {
                    String id = rs.getString(1);
                    ids.append(id).append(",");
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        int indx = ids.lastIndexOf(",");
        ids.delete(indx, indx + 1);
        return ids.toString();
    }

    /**
     * Span
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildSpanXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlScale = "SELECT IDscale, IDdistr, Span.IDspan, num, scale, fSt, lSt, OpenStf, OpenStl, WidthLine_fst, WidthLine_lst, scale_lst, absKM, absKM_e, km, pic, m, km_e, pic_e, m_e, L, "+
                "Span.Name, Span.SCB, Span.line, Span.BR, " +
                "Span.IDst, Span.IDst_e, Station.CodeESR, Station.CodeESR2," +
                "Station.CodeExpress, Station.name AS NameSt, Station_1.CodeESR AS CodeESR_e, Station_1.CodeESR2 AS CodeESR2_e, " +
                "Station_1.CodeExpress AS CodeExpress_e, Station_1.name AS NameSt_e " +
                "FROM (Station INNER JOIN (Scale INNER JOIN Span ON Scale.IDspan = Span.IDspan) " +
                "ON Station.IDst = Span.IDst) INNER JOIN Station AS Station_1 " +
                "ON Span.IDst_e = Station_1.IDst " +
                "WHERE (Scale.IDdistr = " + idDistr + ") " +
                "ORDER BY Scale.num";
        buildCommonXML(tg, doc, element, sqlScale, cmServer);
    }

    /**
     * Distr + DistrParam
     * @param tg �������� ���� xml
     * @param idDistr id distr
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     */
    private void buildDistrXML(QueryName tg, int idDistr, Document doc, Element element) {
        String sqlDistr = "SELECT Distr.IDdir, Distr.INDX, Distr.name " +
                "FROM Distr " +
                "WHERE Distr.IDdistr = " + idDistr;
        /*String sqlDistrParam = "SELECT IDdistr, GDPsource, a_pass, b_pass, c_pass, d_pass, a_o_gr, c_o_gr, d_o_gr, a_e_gr, c_e_gr, d_e_gr, V_u, V_t, V_u_sb, V_t_sb, T_u, T_t, T_u_sb, T_t_sb, sL, sL_sb, ti, mm, tot, HDtxt, PARAMtxt " +
                "FROM DistrParam " +
                "WHERE IDdistr = " + idDistr;*/
        String sqlDistrParam = "SELECT IDdistr, GDPsource, a_pass, b_pass, c_pass, d_pass, a_o_gr, c_o_gr, d_o_gr, a_e_gr, c_e_gr, d_e_gr, V_u, V_t, V_u_sb, V_t_sb, T_u, T_t, T_u_sb, T_t_sb, sL, sL_sb, ti, mm, tot " +
                "FROM DistrParam " +
                "WHERE IDdistr = " + idDistr;
        logger.debug(sqlDistr);
        logger.debug(sqlDistrParam);
        try {
            Connection cnDistr = cmServer.getConnection();
            Statement stDistr = cnDistr.createStatement();
            ResultSet rsDistr = stDistr.executeQuery(sqlDistr);
            Connection cnDistrP = cmAccess.getConnection();
            Statement stDistrP = cnDistrP.createStatement();
            ResultSet rsDistrP = stDistrP.executeQuery(sqlDistrParam);

            //printMetaData(rsDistr.getMetaData());
            //printMetaData(rsDistrP.getMetaData());

            try {
                if (rsDistr.next() && rsDistrP.next()) {
                    buildQueryXML(tg, doc, element, rsDistr, rsDistrP);
                }
            } finally {
                if (rsDistr != null) {
                    rsDistr.close();
                }
                if (stDistr != null) {
                    stDistr.close();
                }
                if (rsDistrP != null) {
                    rsDistrP.close();
                }
                if (stDistrP != null) {
                    stDistrP.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ��������� �������� �������� � ��������� tg � ������� element
     * @param tg �������� ���� xml
     * @param doc {@link Document}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     * @param sql sql ������ �� ������� ������
     * @param connectionManager �� ��� ����������� ������
     */
    private void buildCommonXML(QueryName tg, Document doc, Element element, String sql,
                                ConnectionManager connectionManager) {
        logger.debug(sql);
        try {
            Connection cnSv = connectionManager.getConnection();
            Statement st = cnSv.createStatement();
            ResultSet rs = st.executeQuery(sql);

            //printMetaData(rs.getMetaData());
            try {
                while (rs.next()) {
                    buildQueryXML(tg, doc, element, rs);
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ������ ��������������� ������� XML ��������������� ����� ������ � ��
     * @param tg ��� ���� xml
     * @param doc {@link Double}
     * @param element {@link Element} ������������ �������, � ������� ����� ��������� ����������
     * @param resultSets  ������ {@link ResultSet} � ������� ������� (������)
     * @throws SQLException ���� ���-�� ������ �� ���
     */
    private void buildQueryXML(QueryName tg, Document doc, Element element, ResultSet ... resultSets) throws SQLException {
        Element row = doc.createElement(tg.name());
        for (int rsIndex = 0; rsIndex < resultSets.length; rsIndex++) {
            ResultSet  rs = resultSets[rsIndex];
            if (rs.getRow() > 0) {
                ResultSetMetaData metaData = rs.getMetaData();
                Map<String, String> attributesMap = this.xmlTagMap.get(tg);
                int colCount = metaData.getColumnCount();
                for (int col = 1; col <= colCount; col++) {

                    //String colName = metaData.getColumnName(col);
                    String colName = metaData.getColumnLabel(col);
                    int colType = metaData.getColumnType(col);
                    String attributeName = attributesMap != null ? attributesMap.get(colName.toUpperCase()) : colName;
                    if (attributeName == null) {
                        attributeName = colName;
                    }
                    // blob
                    if (deprecatedColumns.contains(colName.toLowerCase()) || colType == 2004) {
                        String value = "";
                        row.setAttribute(attributeName, value.trim());
                    } else {
                        String value = rs.getString(col);
                        switch (colType){
                            case 93: // time
                                value = TimeConverter.formatTime(rs.getTime(col));
                                break;
                            case 8:// float, double
                                case 7: // real
                                value = decimalFormatter.format(rs.getDouble(col));
                                if (value != null) {
                                    value = value.replace('.', ',');
                                }
                                break;
                            case -7: // boolean
                                value = "0".equals(value) ? "False" : "True";
                                break;
                        }

                        if (!rs.wasNull()) {
                            row.setAttribute(attributeName, value.trim());
                        }
                    }
                }
            }
        }
        element.appendChild(row);
    }

    /**
     * ����������� ���� ���������� � ���������� ������� (�������� ����� � ���� ������)
     * @param metaData {@link ResultSetMetaData}
     * @throws SQLException ���� ���-�� ������ �� ���
     */
    private void printMetaData(ResultSetMetaData metaData) throws SQLException {
        int colCount = metaData.getColumnCount();
        logger.debug("\t" + metaData.getTableName(1));
        for (int i = 1; i <= colCount; i++) {
            logger.debug("i=" + i +
                    " \tname=\t" + metaData.getColumnName(i) +
                    " \tlabel=\t" + metaData.getColumnLabel(i) +
                    " \ttypeName=\t" + metaData.getColumnTypeName(i) +
                    " \ttype=\t" + metaData.getColumnType(i)
            );
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void checkAllTrain(Document doc, String tagName, Map<Long, Pair<Element, DistrTrain>> allTrain,
                              List<Category> categoryList, List<String> warningList) {
        String sqlSelect = "SELECT IDtrain, Name, code " +
                " FROM Train " +
                " WHERE Name=? AND code=?";
        String sqlInsert = "INSERT INTO Train (IDcat, code, Name, prioritet_, type, oe)"+
                " VALUES (?,?,?,?,?,?)";
        boolean needCreate = "Distr_Train".equals(tagName);
        try {
            Connection cnAcs = cmAccess.getConnection();
            try (
                    PreparedStatement psSelect = cnAcs.prepareStatement(sqlSelect);
                    PreparedStatement psInsert = cnAcs.prepareStatement(sqlInsert);
            ) {
                NodeList nodeList = doc.getElementsByTagName(tagName);
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element element = (Element) nodeList.item(i);
                    long idTrainXML = XMLUtil.getAttributeLong(element, "IDtrain");
                    int codeXML = XMLUtil.getAttributeInt(element, "code");
                    String nameXML = XMLUtil.getAttribute(element, "Name");
                    if (!"".equals(nameXML) && codeXML != 0) {
                        DistrTrain train = getTrainByCodeName(codeXML, nameXML, psSelect);
                        Category cat = CategoryUtil.defineCategory(categoryList, codeXML);
                        if (cat == null) {
                            String warn = "Can't find Category for code: " + codeXML;
                            warningList.add(warn);
                            logger.error(warn);
                        }
                        if (train == null && cat != null && needCreate) {
                            createTrainByCodeName(codeXML, nameXML, cat, psInsert);
                            train = getTrainByCodeName(codeXML, nameXML, psSelect);
                        }
                        if (train != null) {
                            if (cat != null) {
                                train.setIdCat(cat.getIdCat());
                                train.setTypeTR(cat.getType());
                            }
                            allTrain.put(idTrainXML, new Pair<>(element, train));
                        }
                    } else {
                        String warn = "Noname train will be ignored: " + element.toString();
                        warningList.add(warn);
                        logger.error(warn);
                    }
                }
            } catch (SQLException e){
                logger.error(e.toString(), e);
            }
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void loadWind(int idDistr, Document domDoc, Map<Integer, Pair<Element, Scale>> allSp) {
        String sql = "INSERT INTO Wind(IDspan, T1, T2, Line, Cause, IDdistrW, Line_o, Line_e, Line_oe, y0) " +
                "VALUES(:IDspan, :T1, :T2, :Line, :Cause, :IDdistrW, :Line_o, :Line_e, :Line_oe, :y0)";
    }

    /**{@inheritDoc}*/
    @Override
    public void loadSpanTime(Document domDoc, Map<Integer, Pair<Element, Scale>> allSp,
                             Map<Integer, Pair<Element, LocomotiveType>> allLoc) throws PoolManagerException {
        Connection cnAcs = cmAccess.getConnection();
        String sqlSel = "SELECT IDspan FROM SpanTime " +
                " WHERE (IDspan = ?) AND (oe = ?) AND (IDltype = ?)";
        String sqlUpd = "UPDATE SpanTime SET Tmove = ?, Tdown = ?, Tup = ?, Tw = ?, type = ?" +
                " WHERE (IDspan = ?) AND (oe = ?) AND (IDltype = ?)";
        String sqlIns = "INSERT INTO SpanTime (Tmove, Tdown, Tup, Tw, type, IDspan, oe, IDltype) " +
                " VALUES(?,?,?,?,?,?,?,?)";

        NodeList nodeList = domDoc.getElementsByTagName("SpanTime");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            int idSpanXML = XMLUtil.getAttributeInt(item, "IDspan");
            int idLocXML = XMLUtil.getAttributeInt(item, "IDltype");
            Pair<Element, Scale> pairS =  allSp.get(idSpanXML);
            Pair<Element, LocomotiveType> pairL =  allLoc.get(idLocXML);
            if (pairS != null && pairL != null) {
                int idSpanDB = pairS.getValue().getIdSpan();
                LocomotiveType lType = pairL.getValue();
                String atr;
                try (PreparedStatement ps = cnAcs.prepareStatement(sqlSel)) {
                    // select
                    ps.setString(1, String.valueOf(idSpanDB));
                    atr = XMLUtil.getAttribute(item, "oe");
                    ps.setString(2, atr);
                    ps.setString(3, String.valueOf(lType.getIdLType()));
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            // update
                            try (PreparedStatement psUpd = cnAcs.prepareStatement(sqlUpd)) {
                                psUpd.setDouble(1, XMLUtil.getAttributeDouble(item, "Tmove"));
                                psUpd.setDouble(2, XMLUtil.getAttributeDouble(item, "Tdown"));
                                psUpd.setDouble(3, XMLUtil.getAttributeDouble(item, "Tup"));
                                psUpd.setDouble(4, XMLUtil.getAttributeDouble(item, "Tw"));
                                psUpd.setString(5, lType.getLtype());
                                psUpd.setString(6, String.valueOf(idSpanDB));
                                atr = XMLUtil.getAttribute(item, "oe");
                                psUpd.setString(7, atr);
                                psUpd.setString(8, String.valueOf(lType.getIdLType()));
                                psUpd.executeUpdate();
                            }
                        } else {
                            // insert
                            try (PreparedStatement psIns = cnAcs.prepareStatement(sqlIns)) {
                                psIns.setDouble(1, XMLUtil.getAttributeDouble(item, "Tmove"));
                                psIns.setDouble(2, XMLUtil.getAttributeDouble(item, "Tdown"));
                                psIns.setDouble(3, XMLUtil.getAttributeDouble(item, "Tup"));
                                psIns.setDouble(4, XMLUtil.getAttributeDouble(item, "Tw"));
                                psIns.setString(5, lType.getLtype());
                                psIns.setString(6, String.valueOf(idSpanDB));
                                atr = XMLUtil.getAttribute(item, "oe");
                                psIns.setString(7, atr);
                                psIns.setString(8, String.valueOf(lType.getIdLType()));
                                psIns.executeUpdate();
                            }
                        }
                    }
                } catch (SQLException e) {
                    logger.error(pairS.getValue().getName() + ": " +  e.toString(), e);
                }
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void loadAppearanceScale(int idDistr, Map<Integer, Pair<Element, Scale>> allSp) throws PoolManagerException {
        Connection cn = cmServer.getConnection();
        String sqlUpd = "UPDATE Scale SET fSt = ?, lSt = ?, OpenStf = ?, OpenStl = ?," +
                " WidthLine_fst = ?, WidthLine_lst = ?, scale = ?" +
                " WHERE (IDspan = ?) AND (IDdistr = " + idDistr + ")";

        for (Pair<Element, Scale> pair : allSp.values()) {
            Element item = pair.getKey();
            int idSpanXML = XMLUtil.getAttributeInt(item, "IDspan");
            Pair<Element, Scale> pairS = allSp.get(idSpanXML);
            int idSpanDB = pairS.getValue().getIdSpan();
            String atr;
            // update
            try (PreparedStatement psUpd = cn.prepareStatement(sqlUpd)) {
                atr = XMLUtil.getAttribute(item, "fSt");
                psUpd.setString(1, atr);
                atr = XMLUtil.getAttribute(item, "lSt");
                psUpd.setString(2, atr);
                atr = XMLUtil.getAttribute(item, "OpenStf");
                psUpd.setString(3, atr);
                atr = XMLUtil.getAttribute(item, "OpenStl");
                psUpd.setString(4, atr);
                atr = XMLUtil.getAttribute(item, "WidthLine_fst");
                psUpd.setString(5, atr);
                atr = XMLUtil.getAttribute(item, "WidthLine_lst");
                psUpd.setString(6, atr);
                atr = XMLUtil.getAttribute(item, "scale");
                psUpd.setString(7, atr);
                psUpd.setInt(8, idSpanDB);
                psUpd.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void loadAppearanceLineSt(Map<Integer, Pair<Element, LineSt>> allLst) throws PoolManagerException {
        Connection cn = cmServer.getConnection();
        String sqlUpd = "UPDATE LineSt SET CloseInGDP = ?, colorLn = ?" +
                " WHERE (IDlineSt = ?)";

        for (Pair<Element, LineSt> pair : allLst.values()) {
            Element item = pair.getKey();
            int idDB = pair.getValue().getIdLineSt();
            String atr;
            // update
            try (PreparedStatement psUpd = cn.prepareStatement(sqlUpd)) {
                atr = XMLUtil.getAttribute(item, "CloseInGDP");
                psUpd.setString(1, atr);
                atr = XMLUtil.getAttribute(item, "colorLn");
                psUpd.setString(2, atr);
                psUpd.setInt(3, idDB);
                psUpd.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void loadDistrComb(int idDistr, Map<Integer, Pair<Element, Distr>> allDistrComb) throws PoolManagerException {
        Connection cn = cmAccess.getConnection();
        String sqlDel = "DELETE FROM DistrComb " +
                " WHERE IDdistr = " + idDistr;
        String sql = "INSERT INTO DistrComb (IDdistr, IDdistrC)" +
                " VALUES (?,?)";
        try {
            cn.createStatement().executeUpdate(sqlDel);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }

        for (Pair<Element, Distr> pair : allDistrComb.values()) {
            int idDB = pair.getValue().getIdDistr();
            // insert
            try (PreparedStatement psUpd = cn.prepareStatement(sql)) {
                psUpd.setInt(1, idDistr);
                psUpd.setInt(2, idDB);
                psUpd.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    void createTrainByCodeName(int code, String name, Category category, PreparedStatement ps) throws SQLException {
        // "INSERT INTO Train (IDcat, code, Name, prioritet_, type, oe)"
        String oe = OE.getValue(code).getString();
        ps.setInt(1, category.getIdCat());
        ps.setInt(2, code);
        ps.setString(3, name);
        ps.setInt(4, category.getPriority());
        ps.setString(5, category.getType().getString());
        ps.setString(6, oe);
        ps.executeUpdate();
        logger.debug("train created: code=" + code + " name=" + name);
    }

    private DistrTrain getTrainByCodeName(int code, String name, PreparedStatement psSelect) throws SQLException {
        psSelect.setString(1, name);
        psSelect.setInt(2, code);
        try (ResultSet rsSelect = psSelect.executeQuery();) {
            if (rsSelect.next()) {
                DistrTrain train = new DistrTrain();
                fillItem(train, rsSelect);
                return train;
            }
        }
        return null;
    }

    private void fillItem(DistrTrain item, ResultSet rs) throws SQLException {
        item.setIdTrain(rs.getLong("IDtrain"));
        item.setNameTR(rs.getString("Name").trim());
        int code = rs.getInt("code");
        item.setCodeTR(String.valueOf(code));
        item.setOe(OE.getValue(code));
    }
}
