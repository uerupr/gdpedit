package com.gmail.alaerof.dao.dto;

/**
 * ������������ ����� ��������.
 * ������������� ���������� �� ����������� ������ num
 *
 * @author Helen Yrofeeva
 */
public class SpanSt implements Comparable<SpanSt>{
    private int idSpanst; // IDspanst
    private int idSpan; // IDspan
    private int num;
    private int km;
    private int pic;
    private int m;
    private int L2;
    private int L1;
    private String name = "";
    private String codeESR = ""; // CodeESR
    private String codeExpress = ""; // CodeExpress
    private int tstop; // Tstop
    private int N2; // N2
    private int N1; // N1
    private float absKM; //
    private String codeESR2 = ""; // CodeESR2

    public int getIdSpanst() {
        return idSpanst;
    }

    public void setIdSpanst(int idSpanst) {
        this.idSpanst = idSpanst;
    }

    public int getIdSpan() {
        return idSpan;
    }

    public void setIdSpan(int idSpan) {
        this.idSpan = idSpan;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getL2() {
        return L2;
    }

    public void setL2(int l2) {
        L2 = l2;
    }

    public int getL1() {
        return L1;
    }

    public void setL1(int l1) {
        L1 = l1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public String getCodeESR() {
        return codeESR;
    }

    public void setCodeESR(String codeESR) {
        if (codeESR != null) {
            this.codeESR = codeESR;
        }
    }

    public String getCodeExpress() {
        return codeExpress;
    }

    public void setCodeExpress(String codeExpress) {
        if (codeExpress != null) {
            this.codeExpress = codeExpress;
        }
    }

    public int getTstop() {
        return tstop;
    }

    public void setTstop(int tstop) {
        this.tstop = tstop;
    }

    public int getN2() {
        return N2;
    }

    public void setN2(int n2) {
        N2 = n2;
    }

    public int getN1() {
        return N1;
    }

    public void setN1(int n1) {
        N1 = n1;
    }

    public float getAbsKM() {
        return absKM;
    }

    public void setAbsKM(float absKM) {
        this.absKM = absKM;
    }

    public String getCodeESR2() {
        return codeESR2;
    }

    public void setCodeESR2(String codeESR2) {
        if (codeESR2 != null) {
            this.codeESR2 = codeESR2;
        }
    }

    @Override
    public String toString() {
        return "SpanSt [idSpanst=" + idSpanst + ", idSpan=" + idSpan + ", num=" + num + ", name=" + name
                + ", codeESR=" + codeESR + ", codeExpress=" + codeExpress + "]";
    }

    @Override
    public int compareTo(SpanSt st) {
        return this.getNum() - st.getNum();
    }

}
