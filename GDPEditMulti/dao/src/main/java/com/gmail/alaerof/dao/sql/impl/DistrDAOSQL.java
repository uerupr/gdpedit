package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.routed.RouteDTrain;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import com.gmail.alaerof.dao.dto.routed.RouteDSpan;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.dao.common.GDPGridConfig;

/**
 * @author Helen Yrofeeva
 */
public class DistrDAOSQL implements IDistrDAO {
    protected static Logger logger = LogManager.getLogger(DistrDAOSQL.class);
    private final ConnectionManager cmServer;

    public DistrDAOSQL(ConnectionManager cmServer) {
        this.cmServer = cmServer;
    }

    private void fillItem(Distr d, ResultSet rs) throws DataManageException {
        try {
            // ���� ���� ��������� ������ �� ������� ������
            d.setIdDistr(rs.getInt("IDdistr"));
            d.setIdDir(rs.getInt("IDdir"));
            d.setIndx(rs.getString("INDX"));
            d.setName(rs.getString("name"));
            d.setMain(rs.getInt("main"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(" Distr " + d, e);
        }

    }

    /**{@inheritDoc}*/
    @Override
    public Distr getDistr(int idDistr) throws ObjectNotFoundException, DataManageException {
        Distr d = null;
        String sql = "SELECT IDdistr, IDdir, INDX, name, main FROM Distr WHERE IDdistr = " + idDistr;
        try {
            Connection cn = cmServer.getConnection();

            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);) {
                if (rs.next()) {
                    d = new Distr();
                    fillItem(d, rs);
                } else {
                    throw new ObjectNotFoundException("Distr IDdistr = " + idDistr);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return d;
    }

    /**{@inheritDoc}*/
    @Override
    public List<Distr> getDistrList(int idDir) throws DataManageException {
        List<Distr> listDistr = new ArrayList<>();
        Distr d = null;
        String sql = "SELECT IDdistr, IDdir, INDX, name, main FROM Distr WHERE IDdir = " + idDir + " ORDER BY name";
        if (idDir == 0) {
            sql = "SELECT IDdistr, IDdir, INDX, name, main FROM Distr ORDER BY IDdistr";
        }
        try {
            Connection cn = cmServer.getConnection();

            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    d = new Distr();
                    fillItem(d, rs);
                    listDistr.add(d);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return listDistr;
    }

    /**{@inheritDoc}*/
    @Override
    public List<LocomotiveType> getListLocomotiveType() throws DataManageException {
        List<LocomotiveType> list = new ArrayList<>();
        LocomotiveType lt = null;
        String sql = "SELECT IDltype, ltype, colorLoc FROM LocomotiveType";
        try {
            Connection cn = cmServer.getConnection();
            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    lt = new LocomotiveType();
                    lt.setIdLtype(rs.getInt("IDltype"));
                    lt.setLtype(rs.getString("ltype"));
                    lt.setColorLoc(rs.getInt("colorLoc"));
                    list.add(lt);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**{@inheritDoc}*/
    @Override
    public GDPGridConfig getConfigGDPGrid(int idDistr) {
        GDPGridConfig gridConfig = new GDPGridConfig();
        String content = null;
        try {
            String fileName = "gdpgridconfig" + File.separator + "distr_" + idDistr;
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            try {
                content = in.readLine();
            } finally {
                in.close();
            }
        } catch (FileNotFoundException e) {
            //logger.warn(e.toString());
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }

        if (content == null) {
            content = "";
        }
        gridConfig.setContent(content);
        return gridConfig;
    }

    /**{@inheritDoc}*/
    @Override
    public void saveConfigGDPGrid(int idDistr, String nameDistr, GDPGridConfig conf) {
        String content = conf.getContent();
        try {
            String fileName = "gdpgridconfig" + File.separator + "distr_" + idDistr;
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
            content += "nameDistr=" + nameDistr + ";";
            try {
                out.write(content);
            } finally {
                out.flush();
                out.close();
            }
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
    }

    private void fillRouteSp(RouteD routeD) throws DataManageException, PoolManagerException {
        List<RouteDSpan> list = new ArrayList<>();
        routeD.setListSpan(list);
        int idR = routeD.getIdRouteD();
        String sql = "SELECT IDrouteDS, IDrouteD, IDspan, num FROM RouteD_Span WHERE IDrouteD = " + idR;
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    RouteDSpan sp = new RouteDSpan();
                    sp.setIdRouteDS(rs.getInt("IDrouteDS"));
                    sp.setIdRouteD(rs.getInt("IDrouteD"));
                    sp.setIdSpan(rs.getInt("IDspan"));
                    sp.setNum(rs.getInt("num"));
                    list.add(sp);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private void fillRouteTR(RouteD routeD) throws PoolManagerException, DataManageException {
        List<RouteDTrain> list = new ArrayList<>();
        routeD.setListTR(list);
        int idR = routeD.getIdRouteD();
        String sql = "SELECT IDrouteDTR, IDrouteD, trMin, trMax FROM RouteD_Train WHERE IDrouteD = " + idR;
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    RouteDTrain tr = new RouteDTrain();
                    tr.setIdRouteDTR(rs.getInt("IDrouteDTR"));
                    tr.setIdRouteD(rs.getInt("IDrouteD"));
                    tr.setTrMin(rs.getInt("trMin"));
                    tr.setTrMax(rs.getInt("trMax"));
                    list.add(tr);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public List<RouteD> getRouteDList(int idDistr) throws DataManageException {
        List<RouteD> list = new ArrayList<>();
        String sql = "SELECT IDrouteD, IDdistr, TypeR, NameR FROM RouteD WHERE IDdistr = " + idDistr;
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    RouteD routeD = new RouteD();
                    routeD.setIdRouteD(rs.getInt("IDrouteD"));
                    routeD.setIdDistr(rs.getInt("IDdistr"));
                    routeD.setTypeR(rs.getInt("TypeR"));
                    routeD.setNameR(rs.getString("NameR"));
                    fillRouteSp(routeD);
                    fillRouteTR(routeD);
                    list.add(routeD);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**{@inheritDoc}*/
    @Override
    public void saveRouteDs(List<RouteD> routeDList, List<RouteD> deleted) throws DataManageException {
        if (routeDList.size() > 0 || deleted.size() > 0) {
            String sqlDel = "DELETE FROM RouteD WHERE IDrouteD = ?";
            String sqlSel = "SELECT IDrouteD FROM RouteD WHERE IDdistr=? AND TypeR=? AND NameR=?";
            String sqlInsR = "INSERT INTO RouteD (IDdistr, TypeR, NameR) VALUES (?,?,?)";
            String sqlInsS = "INSERT INTO RouteD_Span (IDrouteD, IDspan, num) VALUES(?,?,?)";
            String sqlInsT = "INSERT INTO RouteD_Train (IDrouteD, trMin, trMax) VALUES(?,?,?)";
            try {
                Connection cn = cmServer.getConnection();
                try(PreparedStatement psDel = cn.prepareStatement(sqlDel);
                    PreparedStatement psSel = cn.prepareStatement(sqlSel);
                    PreparedStatement psInsR = cn.prepareStatement(sqlInsR);
                    PreparedStatement psInsS = cn.prepareStatement(sqlInsS);
                    PreparedStatement psInsT = cn.prepareStatement(sqlInsT))
                {
                    for (RouteD routeD : deleted) {
                        psDel.setInt(1, routeD.getIdRouteD());
                        psDel.executeUpdate();
                        cn.commit();
                    }
                    for (RouteD routeD : routeDList) {
                        psDel.setInt(1, routeD.getIdRouteD());
                        psDel.executeUpdate();
                        cn.commit();

                        psInsR.setInt(1, routeD.getIdDistr());
                        psInsR.setInt(2, routeD.getTypeR());
                        psInsR.setString(3, routeD.getNameR().trim());
                        psInsR.executeUpdate();
                        cn.commit();

                        psSel.setInt(1, routeD.getIdDistr());
                        psSel.setInt(2, routeD.getTypeR());
                        psSel.setString(3, routeD.getNameR().trim());
                        ResultSet rs = psSel.executeQuery();
                        if (rs.next()) {
                            int newID = rs.getInt("IDrouteD");
                            routeD.setIdRouteD(newID);

                            for (RouteDSpan dSpan : routeD.getListSpan()) {
                                psInsS.setInt(1, newID);
                                psInsS.setInt(2, dSpan.getIdSpan());
                                psInsS.setInt(3, dSpan.getNum());
                                psInsS.executeUpdate();
                            }

                            for (RouteDTrain dTrain : routeD.getListTR()) {
                                psInsT.setInt(1, newID);
                                psInsT.setInt(2, dTrain.getTrMin());
                                psInsT.setInt(3, dTrain.getTrMax());
                                psInsT.executeUpdate();
                            }
                        }
                    }
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void saveLocomotiveTypes(List<LocomotiveType> list) throws DataManageException {
        if (list != null && list.size() > 0) {
            String sql = "UPDATE LocomotiveType SET ltype = ?, colorLoc = ? WHERE IDltype = ?";
            try {
                Connection cn = cmServer.getConnection();
                try(PreparedStatement st = cn.prepareStatement(sql)) {
                    for (LocomotiveType loc : list) {
                        if (loc.isEditable() && loc.getIdLType() > 0) {
                            st.setString(1, loc.getLtype().trim());
                            // ��� ��������� ������ �� ����� ������������
                            st.setInt(2, loc.getColorLoc());
                            st.setInt(3, loc.getIdLType());
                            st.executeUpdate();
                        }
                    }
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void addLocomotiveTypes(List<LocomotiveType> list) throws DataManageException {
        if (list != null && list.size() > 0) {
            String sql = "INSERT INTO LocomotiveType(ltype, colorLoc) VALUES (?,?)";
            try {
                Connection cn = cmServer.getConnection();
                try(PreparedStatement st = cn.prepareStatement(sql);) {
                    for (LocomotiveType loc : list) {
                        if (loc.getIdLType() == 0) {
                            st.setString(1, loc.getLtype().trim());
                            // ��� ��������� ������ �� ����� ������������
                            st.setInt(2, loc.getColorLoc());
                            st.executeUpdate();
                        }
                    }
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void deleteLocomotiveTypes(List<LocomotiveType> list) throws DataManageException {
        if (list != null && list.size() > 0) {
            String sql = "DELETE FROM LocomotiveType WHERE IDltype = ?";
            try {
                Connection cn = cmServer.getConnection();
                PreparedStatement st = cn.prepareStatement(sql);
                try {
                    for (LocomotiveType loc : list) {
                        if (loc.getIdLType() > 0) {
                            st.setInt(1, loc.getIdLType());
                            st.executeUpdate();
                        }
                    }
                } finally {
                    if (st != null) {
                        st.close();
                    }
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

}
