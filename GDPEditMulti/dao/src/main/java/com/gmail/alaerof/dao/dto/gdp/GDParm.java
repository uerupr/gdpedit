package com.gmail.alaerof.dao.dto.gdp;

public class GDParm {
  private int idGDParm;
  private Long idDistr;
  private String dateBegin;
  private String dateEnd;
  private String dateOn;
  private String author;
  private String description;
  private String dateKTC;
  private Boolean norm;
  private String normStr;
  private Boolean arch;
  private String responceKTC;
  private String gdpXML;

  public GDParm() {
  }

  public GDParm(int idGDParm, Long idDistr, String dateBegin, String dateEnd, String dateOn,
                String author, String description, String dateKTC, Boolean norm, Boolean arch, String responceKTC, String gdpXML) {
    this.idGDParm = idGDParm;
    this.idDistr = idDistr;
    this.dateBegin = dateBegin;
    this.dateEnd = dateEnd;
    this.dateOn = dateOn;
    this.author = author;
    this.description = description;
    this.dateKTC = dateKTC;
    this.norm = norm;
    if (norm == true) {
      normStr = "*";
    } else {
      normStr = "";
    }
    this.arch = arch;
    this.responceKTC = responceKTC;
    this.gdpXML = gdpXML;
  }

  @Override
  public String toString() {
    return "GDParm{" +
            "idGDParm=" + idGDParm +
            ", idDistr=" + idDistr +
            ", dateBegin='" + dateBegin + '\'' +
            ", dateEnd='" + dateEnd + '\'' +
            ", dateOn='" + dateOn + '\'' +
            ", author='" + author + '\'' +
            ", description='" + description + '\'' +
            ", dateKTC='" + dateKTC + '\'' +
            ", norm=" + norm +
            ", normStr='" + normStr + '\'' +
            ", arch=" + arch +
            ", responceKTC='" + responceKTC + '\'' +
            ", gdpXML='" + gdpXML + '\'' +
            '}';
  }

  public int getIdGDParm() {
    return idGDParm;
  }

  public void setIdGDParm(int idGDParm) {
    this.idGDParm = idGDParm;
  }

  public Long getIdDistr() {
    return idDistr;
  }

  public void setIdDistr(Long idDistr) {
    this.idDistr = idDistr;
  }

  public String getDateBegin() {
    return dateBegin;
  }

  public void setDateBegin(String dateBegin) {
    this.dateBegin = dateBegin;
  }

  public String getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(String dateEnd) {
    this.dateEnd = dateEnd;
  }

  public String getDateOn() {
    return dateOn;
  }

  public void setDateOn(String dateOn) {
    this.dateOn = dateOn;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDateKTC() {
    return dateKTC;
  }

  public void setDateKTC(String dateKTC) {
    this.dateKTC = dateKTC;
  }

  public Boolean getNorm() {
    return norm;
  }

  public void setNorm(Boolean norm) {
    this.norm = norm;
  }

  public String getNormStr() {
    return normStr;
  }

  public void setNormStr(String normStr) {
    this.normStr = normStr;
  }

  public Boolean getArch() {
    return arch;
  }

  public void setArch(Boolean arch) {
    this.arch = arch;
  }

  public String getResponceKTC() {
    return responceKTC;
  }

  public void setResponceKTC(String responceKTC) {
    this.responceKTC = responceKTC;
  }

  public String getGdpXML() {
    return gdpXML;
  }

  public void setGdpXML(String gdpXML) {
    this.gdpXML = gdpXML;
  }
}
