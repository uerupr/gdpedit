package com.gmail.alaerof.dao.dto;

/**
 * ���� �������
 * 
 * @author Helen Yrofeeva
 * 
 */
public class LineSt {
    private int idLineSt; // IDlineSt
    private int idSt; // IDst
    private int n; // N
    private boolean close;
    private String ps = ""; // PS
    private int len;
    private boolean el;
    private boolean pz;
    private boolean grO; // gr_o
    private boolean grE; // gr_e
    private boolean passO; // pass_o
    private boolean passE; // pass_e
    private boolean closeInGDP; // CloseInGDP
    private int colorLn;

    public int getIdLineSt() {
        return idLineSt;
    }

    public void setIdLineSt(int idLineSt) {
        this.idLineSt = idLineSt;
    }

    public int getIdSt() {
        return idSt;
    }

    public void setIdSt(int idSt) {
        this.idSt = idSt;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        if (ps != null) {
            this.ps = ps;
        }
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public boolean isEl() {
        return el;
    }

    public void setEl(boolean el) {
        this.el = el;
    }

    public boolean isPz() {
        return pz;
    }

    public void setPz(boolean pz) {
        this.pz = pz;
    }

    public boolean isGrO() {
        return grO;
    }

    public void setGrO(boolean grO) {
        this.grO = grO;
    }

    public boolean isGrE() {
        return grE;
    }

    public void setGrE(boolean grE) {
        this.grE = grE;
    }

    public boolean isPassO() {
        return passO;
    }

    public void setPassO(boolean passO) {
        this.passO = passO;
    }

    public boolean isPassE() {
        return passE;
    }

    public void setPassE(boolean passE) {
        this.passE = passE;
    }

    public boolean isCloseInGDP() {
        return closeInGDP;
    }

    public void setCloseInGDP(boolean closeInGDP) {
        this.closeInGDP = closeInGDP;
    }

    public int getColorLn() {
        return colorLn;
    }

    public void setColorLn(int colorLn) {
        this.colorLn = colorLn;
    }

    @Override
    public String toString() {
        return "LineSt [idLineSt=" + idLineSt + ", idSt=" + idSt + ", n=" + n + ", ps=" + ps + "]";
    }

}
