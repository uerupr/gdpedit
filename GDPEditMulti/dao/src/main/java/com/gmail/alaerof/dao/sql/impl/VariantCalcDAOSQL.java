package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.VariantCalc;
import com.gmail.alaerof.dao.dto.VariantCalcTime;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.IVariantCalcDAO;

public class VariantCalcDAOSQL implements IVariantCalcDAO {
    protected static Logger logger = LogManager.getLogger(VariantCalcDAOSQL.class);
    private final ConnectionManager cm;

    public VariantCalcDAOSQL(ConnectionManager cm) {
        this.cm = cm;
    }

    @Override
    public List<VariantCalc> getListVarCalc(int idDistr) throws DataManageException {
        List<VariantCalc> list = new ArrayList<>();
        String sql = "SELECT IDvarCalc, name, [date], description, oe, IDvarCalcAdd, descriptionAdd"
                + " FROM VariantCalc WHERE IDvarCalc IN ("
                + " SELECT IDvarCalc FROM Distr_varCalc WHERE IDdistr = " + idDistr
                + ") ORDER BY oe DESC, [date] DESC";
        if (cm.isHSQLDB()) {
            sql = "SELECT IDvarCalc, name, date, description, oe, IDvarCalcAdd, descriptionAdd"
                    + " FROM VariantCalc WHERE IDvarCalc IN ("
                    + " SELECT IDvarCalc FROM Distr_varCalc WHERE IDdistr = " + idDistr
                    + ") ORDER BY oe DESC, date DESC";
        }

        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    VariantCalc vc = new VariantCalc();
                    vc.setIDvarCalc(rs.getInt("IDvarCalc"));
                    String name = rs.getString("name");
                    if (name != null) {
                        vc.setName(name.trim());
                    }
                    vc.setDate(rs.getDate("date"));
                    String descr = rs.getString("description");
                    if (descr != null) {
                        vc.setDescription(descr.trim());
                    }
                    vc.setOe(rs.getInt("oe"));
                    vc.setIDvarCalcAdd(rs.getInt("IDvarCalcAdd"));
                    descr = rs.getString("descriptionAdd");
                    if (descr != null) {
                        vc.setDescriptionAdd(descr.trim());
                    }
                    list.add(vc);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    @Override
    public List<VariantCalcTime> getListVarCalcTime(int idVarCalc) throws DataManageException {
        List<VariantCalcTime> list = new ArrayList<>();
        String sql = "SELECT IDvarCalc, name, CodeESR, type, num, L, T1, Tdown, Tup, abs, dT, TupAdd, TdownAdd " +
                "FROM St_Time WHERE IDvarCalc = " + idVarCalc;

        try {
            Connection cn = cm.getConnection();
            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    VariantCalcTime vc = new VariantCalcTime();
                    vc.setIdvarCalc(rs.getInt("IDvarCalc"));
                    String name = rs.getString("name");
                    if (name != null) {
                        vc.setName(name.trim());
                    }
                    String codeESR = rs.getString("CodeESR");
                    if (codeESR != null) {
                        vc.setCodeESR(codeESR.trim());
                    }
                    String type = rs.getString("type");
                    if (type != null) {
                        vc.setType(type.trim());
                    }
                    vc.setNum(rs.getInt("num"));
                    vc.setL(rs.getDouble("L"));
                    vc.setT1(rs.getDouble("T1"));
                    vc.setTdown(rs.getDouble("Tdown"));
                    vc.setTup(rs.getDouble("Tup"));
                    vc.setAbs(rs.getDouble("abs"));
                    vc.setDt(rs.getDouble("dT"));
                    vc.setTupAdd(rs.getDouble("TupAdd"));
                    vc.setTdownAdd(rs.getDouble("TdownAdd"));
                    list.add(vc);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    @Override
    public void deleteVariantCalc(int idVarCalc) throws DataManageException {
        String sql = "DELETE FROM VariantCalc WHERE IDvarCalc = " + idVarCalc;
        try {
            Connection cn = cm.getConnection();

            try(Statement st = cn.createStatement();) {
                st.executeUpdate(sql);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }
}
