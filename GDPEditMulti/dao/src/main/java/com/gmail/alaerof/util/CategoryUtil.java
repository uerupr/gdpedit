package com.gmail.alaerof.util;

import com.gmail.alaerof.dao.dto.Category;
import java.util.List;

public class CategoryUtil {
    public static  Category defineCategory(List<Category> categoryList, int codeTR)  {
        for (Category category : categoryList) {
            if (category.getTrMin() <= codeTR && category.getTrMax() >= codeTR) {
                return category;
            }
        }
        return null;
    }
}
