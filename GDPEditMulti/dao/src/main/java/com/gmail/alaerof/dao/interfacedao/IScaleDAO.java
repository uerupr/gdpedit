package com.gmail.alaerof.dao.interfacedao;

import java.util.List;

import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;

/**
 * @author Helen Yrofeeva
 */
public interface IScaleDAO {
	List<Scale> getListScale(int idDistr) throws DataManageException;
	Scale getScale(int idDistr, int idSpan) throws ObjectNotFoundException,DataManageException;
	void updateWidthLine(Scale scale) throws DataManageException;
	void updateScale(List<Scale> scaleList) throws DataManageException;

}
