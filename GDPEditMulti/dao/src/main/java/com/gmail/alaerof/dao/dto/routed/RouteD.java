package com.gmail.alaerof.dao.dto.routed;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Helen Yrofeeva
 */
public class RouteD {
    private static int lastID = -1;
    private int idRouteD = lastID--; // IDrouteD
    private int idDistr;  // IDdistr
    private int typeR; // TypeR
    private String nameR; // NameR
    /** RouteD_Span */
    private List<RouteDSpan> listSpan = new ArrayList<>();
    /** RouteD_Train */
    private List<RouteDTrain> listTR = new ArrayList<>();

    public int getIdRouteD() {
        return idRouteD;
    }
    public void setIdRouteD(int idRouteD) {
        this.idRouteD = idRouteD;
    }
    public int getIdDistr() {
        return idDistr;
    }
    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }
    public int getTypeR() {
        return typeR;
    }
    public void setTypeR(int typeR) {
        this.typeR = typeR;
    }
    public String getNameR() {
        return nameR;
    }
    public void setNameR(String nameR) {
        this.nameR = nameR;
    }
    
    public List<RouteDSpan> getListSpan() {
        return listSpan;
    }
    
    public void setListSpan(List<RouteDSpan> listSpan) {
        this.listSpan = listSpan;
    }

    public List<RouteDTrain> getListTR() {
        return listTR;
    }

    public void setListTR(List<RouteDTrain> listTR) {
        this.listTR = listTR;
    }

    public String getTypeRAsString(){
        return typeR == 0 ? "*" : "";
    }

    public void setTypeRAsString(String s){
        typeR = s.contains("*") ? 0 : 1;
    }

    @Override
    public String toString() {
        return getNameR();
    }

}
