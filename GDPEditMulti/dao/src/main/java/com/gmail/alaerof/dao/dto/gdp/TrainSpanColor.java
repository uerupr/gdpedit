package com.gmail.alaerof.dao.dto.gdp;
/**
 */
public class TrainSpanColor {
    private long idTrain; // IDtrain
    private int idDistr; // IDdistr
    /** ��� �������, ����� ������� �������� ���� ������ */
    private int idStB; // IDstB
    private int colorTR=0;
    
    public long getIdTrain() {
        return idTrain;
    }
    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }
    public int getIdDistr() {
        return idDistr;
    }
    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }
    public int getIdStB() {
        return idStB;
    }
    public void setIdStB(int idStB) {
        this.idStB = idStB;
    }
    public int getColorTR() {
        return colorTR;
    }
    public void setColorTR(int colorTR) {
        this.colorTR = colorTR;
    }
    
    
}
