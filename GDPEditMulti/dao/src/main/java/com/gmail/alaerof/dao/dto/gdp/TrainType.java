package com.gmail.alaerof.dao.dto.gdp;
/**
 */
public enum TrainType {
    gr, pass, town, undefined;
    public String getString() {
        switch (this) {
            case gr:
                return "грузовой";
            case pass:
                return "пассажирский";
            case town:
                return "пригородный";
            default:
                return "";
        }
    }

    public static TrainType getType(String type){
        if("пассажирский".equals(type)){
            return pass;
        }
        if("пригородный".equals(type)){
            return town;
        }
        if("грузовой".equals(type)){
            return gr;
        }
        return undefined;
    }
}
