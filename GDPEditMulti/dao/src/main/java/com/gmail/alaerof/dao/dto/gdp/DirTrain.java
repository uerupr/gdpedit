package com.gmail.alaerof.dao.dto.gdp;

import java.util.Objects;

/**
 *  DirTrain - ���������� ������ �� ���
 */
public class DirTrain {
    private int idDir;
    private int idDistr;
    private long idTrain;
    private double tu;  // Tu, ���
    private double tt;  // Tt, ���
    private double len; // L, �
    private int stopCount; // ���-�� ������� ��� �������������������

    public int getIdDir() {
        return idDir;
    }

    public void setIdDir(int idDir) {
        this.idDir = idDir;
    }

    public int getIdDistr() {
        return idDistr;
    }

    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }

    public long getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }

    public double getTu() {
        return tu;
    }

    public void setTu(double tu) {
        this.tu = tu;
    }

    public double getTt() {
        return tt;
    }

    public void setTt(double tt) {
        this.tt = tt;
    }

    public double getLen() {
        return len;
    }

    public void setLen(double len) {
        this.len = len;
    }

    public int getStopCount() {
        return stopCount;
    }

    public void setStopCount(int stopCount) {
        this.stopCount = stopCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DirTrain dirTrain = (DirTrain) o;
        return idDir == dirTrain.idDir &&
                idDistr == dirTrain.idDistr &&
                idTrain == dirTrain.idTrain &&
                Double.compare(dirTrain.tu, tu) == 0 &&
                Double.compare(dirTrain.tt, tt) == 0 &&
                Double.compare(dirTrain.len, len) == 0 &&
                stopCount == dirTrain.stopCount;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDir, idDistr, idTrain, tu, tt, len, stopCount);
    }

    @Override
    public String toString() {
        return "DirTrain{" +
                "idDir=" + idDir +
                ", idDistr=" + idDistr +
                ", idTrain=" + idTrain +
                ", tu=" + tu +
                ", tt=" + tt +
                ", len=" + len +
                ", stopCount=" + stopCount +
                '}';
    }

    public double getVt() {
        if (tt > 0) {
            double hh = tt / 60.0;
            double km = len / 1000.0;
            return km / hh;
        }
        return 0;
    }

    public double getVu() {
        if (tu > 0) {
            double hh = tu / 60.0;
            double km = len / 1000.0;
            return km / hh;
        }
        return 0;
    }
}
