package com.gmail.alaerof.util;

import java.io.File;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.io.FilenameUtils;

public class FileUtil {
    public enum Mode {
        open, save
    }
    public static final String CSV_SEPARATOR = ";";
    public static File lastFileChooserPath;

    /**
     * Swing ���������� ���� ��������/���������� ����� � �������� �������
     * @param mode ������ ��� ��������(true) ��� ���������� (false)
     * @param title title
     * @param defaultFileName �������� ����� �� ���������
     * @param fileNameExtFilter ������ ����������
     * @param frame ������������ ���� ��� ��������� �����
     * @return ���� File
     */
    public static File chooseFile(Mode mode, String title, String defaultFileName, JFrame frame,
                                  FileNameExtensionFilter fileNameExtFilter) {
        File file = null;
        JFileChooser chooser = new JFileChooser();
        if (title != null) {
            chooser.setDialogTitle(title);
        }
        String fileName = defaultFileName;
        if (lastFileChooserPath != null) {
            fileName = lastFileChooserPath + File.separator + defaultFileName;
        }
        chooser.setFileFilter(fileNameExtFilter);
        chooser.setSelectedFile(new File(fileName));
        int returnVal = 0;
        if (mode == Mode.open) {
            returnVal = chooser.showOpenDialog(frame);
        } else {
            returnVal = chooser.showSaveDialog(frame);
        }
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
        }
        if (file != null) {
            String fileNn = FilenameUtils.getName(file.getAbsolutePath());
            String filePath = file.getAbsolutePath().replace(fileNn, "");
            lastFileChooserPath = new File(filePath);
        }
        return file;
    }

    /**
     * FX FileChooser ���������� ���� ��������/���������� ����� � �������� �������
     * @param mode open/save
     * @param title title, ����� ���� null
     * @param defaultFileName file name
     * @param stage ������������ ����, ����� ���� null
     * @param fileNameExtFilter �������
     * @return File or null
     */
    public static File chooseFile(Mode mode, String title, String defaultFileName, Stage stage,
                                  FileChooser.ExtensionFilter... fileNameExtFilter) {
        if (title == null) {
            if (mode == Mode.open) {
                title = "Open";
            } else {
                title = "Save";
            }
        }
        File file;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser.setInitialDirectory(FileUtil.lastFileChooserPath);
        fileChooser.getExtensionFilters().addAll(fileNameExtFilter);
        fileChooser.setInitialFileName(defaultFileName);
        if (mode == Mode.open) {
            file = fileChooser.showOpenDialog(stage);
        } else {
            file = fileChooser.showSaveDialog(stage);
        }
        if (file != null) {
            String fileNn = FilenameUtils.getName(file.getAbsolutePath());
            String filePath = file.getAbsolutePath().replace(fileNn, "");
            lastFileChooserPath = new File(filePath);
        }
        return file;
    }
}
