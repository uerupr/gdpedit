package com.gmail.alaerof.application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class TestDAOApp extends Application {
    private static final int WIDTH = 400;
    private static final int HEIGHT = 400;
    private static final int BORDER = 20;

    @Override
    public void start(Stage stage) throws Exception {
        Pane pane = new Pane();
        Button btn = new Button();
        btn.setText("XXX");
        pane.getChildren().add(btn);
        stage.setTitle("-- XXX --");
        stage.setScene(new Scene(pane, WIDTH + BORDER * 4, HEIGHT + BORDER * 4));
        stage.show();
    }
}
