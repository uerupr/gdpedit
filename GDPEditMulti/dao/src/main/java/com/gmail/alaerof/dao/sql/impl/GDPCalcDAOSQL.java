package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainShowParam;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.SpTOcc;
import com.gmail.alaerof.dao.interfacedao.IGDPCalcDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.util.TimeConverter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class GDPCalcDAOSQL implements IGDPCalcDAO {
    protected static Logger logger = LogManager.getLogger(GDPCalcDAOSQL.class);
    private final ConnectionManager cm;

    public GDPCalcDAOSQL(ConnectionManager cmAccess) {
        this.cm = cmAccess;
    }

    /** {@inheritDoc} */
    @Override
    public List<DistrTrain> getSpanDistrTrains(int idSpan, String trainIDs, int idDistr) throws DataManageException {
        List<DistrTrain> listTR = new ArrayList<>();
        String sql = "SELECT  " +
                " Train.IDtrain, Train.code, Train.Name, Train.type, " +
                " SpTOcc.IDspan, SpTOcc.IDdistr, SpTOcc.occupyOn, SpTOcc.occupyOff, SpTOcc.oe, SpTOcc.NextDay, SpTOcc.Tmove " +
                " FROM Train INNER JOIN SpTOcc ON Train.IDtrain = SpTOcc.IDtrain " +
//                " WHERE SpTOcc.IDspan = " + idSpan + " AND NOT (Train.IDtrain IN (" +trainIDs +"))";
                " WHERE SpTOcc.IDspan = " + idSpan + " AND SpTOcc.IDdistr <> " + idDistr;
        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);){
                while (rs.next()) {
                    DistrTrain tr = new DistrTrain();
                    fillTrain(tr, rs);
                    fillSpTOcc(tr, rs);
                    fillShowParam(tr, cn);
                    if (!tr.getShowParam().isHidden()) {
                        listTR.add(tr);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return listTR;
    }

    /** {@inheritDoc} */
    @Override
    public List<DistrTrain> getStationDistrTrains(int idSt, String trainIDs, int idDistr) throws DataManageException {
        List<DistrTrain> listTR = new ArrayList<>();
        String sql = "SELECT  " +
                " Train.IDtrain, Train.code, Train.Name, Train.type, " +
                " LineSt_Train.IDlineSt, LineSt_Train.IDst, LineSt_Train.occupyOn, LineSt_Train.occupyOff, LineSt_Train.TtexStop " +
                " FROM Train INNER JOIN LineSt_Train ON Train.IDtrain = LineSt_Train.IDtrain " +
//                " WHERE LineSt_Train.IDst = " + idSt + " AND NOT (Train.IDtrain IN (" +trainIDs +"))";
                " WHERE LineSt_Train.IDst = " + idSt + " AND LineSt_Train.IDdistr <> " +idDistr;
        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);){
                while (rs.next()) {
                    DistrTrain tr = new DistrTrain();
                    fillTrain(tr, rs);
                    fillLSTT(tr, rs);
                    fillShowParam(tr, cn);
                    if (!tr.getShowParam().isHidden()) {
                        listTR.add(tr);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return listTR;
    }

    private void fillLSTT(DistrTrain tr, ResultSet rs) throws DataManageException {
        List<LineStTrain> list = new ArrayList();
        LineStTrain item = new LineStTrain();
        try {
            item.setIdLineSt(rs.getInt("IDlineSt"));
            item.setIdSt(rs.getInt("IDst"));
            item.setIdTrain(tr.getIdTrain());
            item.setOccupyOn(TimeConverter.formatTime(rs.getTime("occupyOn")));
            item.setOccupyOff(TimeConverter.formatTime(rs.getTime("occupyOff")));
            item.setTexStop(rs.getInt("TtexStop"));
            list.add(item);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " LineSt_Train  " + item, e);
        }
        tr.fillLineStTrMap(list);
    }

    private void fillShowParam(DistrTrain tr, Connection cn) throws DataManageException {
        String sql = "SELECT hidden " +
                " FROM Distr_Train_ShowParam WHERE IDdistr = " + tr.getIdDistr() +
                " AND IDtrain = " + tr.getIdTrain();
        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            if (rs.next()) {
                DistrTrainShowParam sp = tr.getShowParam();
                sp.setHidden(rs.getBoolean("hidden"));
                tr.setShowParam(sp);
            }
        }
        catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Distr_Train_ShowParam " + tr, e);
        }

    }

    private void fillTrain(DistrTrain tr, ResultSet rs) throws DataManageException {
        try {
            tr.setIdTrain(rs.getLong("IDtrain"));
            String code = rs.getString("code");
            tr.setCodeTR(code);
            tr.setNameTR(rs.getString("Name"));
            tr.setTypeTR(rs.getString("type").trim());
            tr.setOe();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Distr_Train " + tr, e);
        }
    }

    private void fillSpTOcc(DistrTrain tr, ResultSet rs) throws DataManageException {
        Map<Integer, SpTOcc> mapSpTOcc = new HashMap<>();
        tr.setMapSpTOcc(mapSpTOcc);
        SpTOcc item = new SpTOcc();
        try {
            item.setIdSpan(rs.getInt("IDspan"));
            item.setIdDistr(rs.getInt("IDdistr"));
            item.setIdTrain(tr.getIdTrain());
            item.setOccupyOn(TimeConverter.formatTime(rs.getTime("occupyOn")));
            item.setOccupyOff(TimeConverter.formatTime(rs.getTime("occupyOff")));
            item.setOe(rs.getString("oe"));
            item.setNextDay(rs.getInt("NextDay"));
            item.setTmove(rs.getInt("Tmove"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " SpTOcc " + item, e);
        }
        mapSpTOcc.put(item.getIdSpan(), item);
        // � ������� SpTOcc �� ����� ���� ���� IDtrain � ������� IDdistr
        tr.setIdDistr(item.getIdDistr());
    }


}
