package com.gmail.alaerof.dao.sql;

import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.dao.interfacedao.IGDPCalcDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrCombDAO;
import com.gmail.alaerof.dao.interfacedao.IGDParmDAO;
import com.gmail.alaerof.dao.interfacedao.IGroupPDAO;
import com.gmail.alaerof.dao.interfacedao.IUsersDAO;
import com.gmail.alaerof.dao.sql.impl.CategoryDAOSQL;
import com.gmail.alaerof.dao.sql.impl.DirDAOSQL;
import com.gmail.alaerof.dao.sql.impl.DistrCombDAOSQL;
import com.gmail.alaerof.dao.sql.impl.DistrDAOSQL;
import com.gmail.alaerof.dao.sql.impl.DistrGDPXMLDAOSQL;
import com.gmail.alaerof.dao.sql.impl.DistrTrainDAOSQL;
import com.gmail.alaerof.dao.sql.impl.GDPCalcDAOSQL;
import com.gmail.alaerof.dao.sql.impl.GDPTextPrintParamDAOSQL;
import com.gmail.alaerof.dao.sql.impl.GDParmDAOSQL;
import com.gmail.alaerof.dao.sql.impl.GroupPDAOSQL;
import com.gmail.alaerof.dao.sql.impl.SQLQueryGDParm;
import com.gmail.alaerof.dao.sql.impl.ScaleDAOSQL;
import com.gmail.alaerof.dao.sql.impl.SpanEnergyDAOSQL;
import com.gmail.alaerof.dao.sql.impl.SpanTimeDAOSQL;
import com.gmail.alaerof.dao.sql.impl.StationDAOSQL;
import com.gmail.alaerof.dao.sql.impl.UsersDAOSQL;
import com.gmail.alaerof.dao.sql.impl.VariantCalcDAOSQL;
import com.gmail.alaerof.dao.interfacedao.IDirDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrGDPXMLDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrTrainDAO;
import com.gmail.alaerof.dao.interfacedao.IGDPTextPrintParamDAO;
import com.gmail.alaerof.dao.interfacedao.IScaleDAO;
import com.gmail.alaerof.dao.interfacedao.ISpanEnergyDAO;
import com.gmail.alaerof.dao.interfacedao.ISpanTimeDAO;
import com.gmail.alaerof.dao.interfacedao.IStationDAO;
import com.gmail.alaerof.dao.interfacedao.IVariantCalcDAO;
import com.gmail.alaerof.util.ConnectionDBFileHandlerUtil;

import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.factory.IGDPDAOFactory;

/**
 * @author Helen Yrofeeva
 */
public class GDPDAOFactorySQL implements IGDPDAOFactory {
    private final ConnectionManager managerServer;
    private final ConnectionManager managerAccess;
    public static final String BASE_NAME_SERVER = "dbserver";
    public static final String BASE_NAME_ACCESS = "dbaccess";
    public static final String BASE_NAME_LOCAL = "dblocal";
    public static final String BASE_NAME_HSQLDB_SERVER = "hsqldbserver";
    public static final String BASE_NAME_HSQLDB_LOCAL = "hsqldblocal";
    private static GDPDAOFactorySQL factory;
    protected static Logger logger = LogManager.getLogger(GDPDAOFactorySQL.class);
    private DataSourceType serverMode;
    private boolean modeHSQLDB;

    private GDPDAOFactorySQL() throws PoolManagerException {
        Properties gdpEditINI = ConnectionDBFileHandlerUtil.readGDPEditINI();
        try {
            serverMode = DataSourceType.valueOf(gdpEditINI.getProperty("DataSourceType"));
        } catch (Exception e) {
            logger.error(e.toString(), e);
            serverMode = DataSourceType.Server;
        }
        try {
            modeHSQLDB = Boolean.valueOf(gdpEditINI.getProperty("HSQLDB"));
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }

        logger.debug("mode : " + serverMode);
        logger.debug("HSQLDB : " + modeHSQLDB);
        if (serverMode == DataSourceType.Server) {
            managerServer = new ConnectionManagerMSSQL(BASE_NAME_SERVER);
            if (modeHSQLDB) {
                managerAccess = new ConnectionManagerHSQDB(BASE_NAME_HSQLDB_SERVER);
            } else {
                managerAccess = new ConnectionManagerMSSQL(BASE_NAME_ACCESS);
            }
        } else {
            if (modeHSQLDB) {
                managerServer = new ConnectionManagerHSQDB(BASE_NAME_HSQLDB_LOCAL);
                managerAccess = new ConnectionManagerHSQDB(BASE_NAME_HSQLDB_LOCAL);
            } else {
                managerServer = new ConnectionManagerMSSQL(BASE_NAME_LOCAL);
                managerAccess = new ConnectionManagerMSSQL(BASE_NAME_LOCAL);
            }
        }
    }

    public static ConnectionManager getNewPoolManager(String baseName) throws PoolManagerException {
        return new ConnectionManagerMSSQL(baseName);
    }

    public static IGDPDAOFactory getInstance() throws PoolManagerException {
        factory = new GDPDAOFactorySQL();
        return factory;
    }

    @Override
    public IScaleDAO getScaleDAO() {
        IScaleDAO scaleDAO = new ScaleDAOSQL(managerServer);
        return scaleDAO;
    }

    @Override
    public IStationDAO getStationDAO() {
        IStationDAO dao = new StationDAOSQL(managerServer, managerAccess);
        return dao;
    }

    @Override
    public IDistrDAO getDistrDAO() {
        IDistrDAO dao = new DistrDAOSQL(managerServer);
        return dao;
    }

    @Override
    public ISpanTimeDAO getSpanTimeDAO() {
        ISpanTimeDAO dao = new SpanTimeDAOSQL(managerAccess);
        return dao;
    }

    @Override
    public ISpanEnergyDAO getSpanEnergyDAO() {
        ISpanEnergyDAO dao = new SpanEnergyDAOSQL(managerServer, managerAccess);
        return dao;
    }

    @Override
    public IDirDAO getDirDAO() {
        IDirDAO dao = new DirDAOSQL(managerServer);
        return dao;
    }

    @Override
    public IGDPTextPrintParamDAO getTextPrintParamDAO() {
        IGDPTextPrintParamDAO dao = new GDPTextPrintParamDAOSQL(managerAccess);
        return dao;
    }

    @Override
    public IDistrCombDAO getDistrCombDAO() {
        IDistrCombDAO dao = new DistrCombDAOSQL(managerAccess);
        return dao;
    }

    @Override
    public ICategoryDAO getCategoryDAO() {
        ICategoryDAO dao = new CategoryDAOSQL(managerServer, managerAccess);
        return dao;
    }

    @Override
    public IGroupPDAO getGroupPDAO() {
        IGroupPDAO dao = new GroupPDAOSQL(managerServer);
        return dao;
    }

    @Override
    public IGDPCalcDAO getGDPCalcDAO() {
        IGDPCalcDAO dao = new GDPCalcDAOSQL(managerAccess);
        return dao;
    }

    @Override
    public IGDParmDAO getGDParmDAO() {
        IGDParmDAO dao = new GDParmDAOSQL(managerServer);
        return dao;
    }

    @Override
    public IUsersDAO getUsersDAO() {
        IUsersDAO dao = new UsersDAOSQL(managerServer);
        return dao;
    }

    @Override
    public SQLQueryGDParm getSQLGDParm() {
        SQLQueryGDParm dao = new SQLQueryGDParm(managerServer, managerAccess);
        return dao;
    }

    @Override
    public IDistrTrainDAO getDistrTrainDAO() {
        return new DistrTrainDAOSQL(managerAccess);
    }

    @Override
    public String getPath() {
        return managerAccess.getPath();
    }

    @Override
    public IVariantCalcDAO getVariantCalcDAO() {
        return new VariantCalcDAOSQL(managerServer);
    }

    @Override
    public IDistrGDPXMLDAO getDistrGDPXMLDAO() {
        return new DistrGDPXMLDAOSQL(managerServer, managerAccess);
    }

    @Override
    public boolean isServer() {
        return serverMode == DataSourceType.Server;
    }

    @Override
    public boolean isHSQLDB() {
        return modeHSQLDB;
    }

    @Override
    public void closeConnections() {
        managerServer.closeConnection();
        managerAccess.closeConnection();
    }
}
