package com.gmail.alaerof.dao.sql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.SpanStTime;
import com.gmail.alaerof.dao.dto.SpanTime;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.ISpanTimeDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.Set;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 */
public class SpanTimeDAOSQL implements ISpanTimeDAO {
    protected static Logger logger = LogManager.getLogger(SpanTimeDAOSQL.class);
    private final ConnectionManager cm;

    public SpanTimeDAOSQL(ConnectionManager cm) {
        this.cm = cm;
    }

    void fillItem(Time tm, ResultSet rs) throws DataManageException {
        try {
            tm.setTimeMove(rs.getFloat("Tmove"));
            tm.setTimeDown(rs.getFloat("Tdown"));
            tm.setTimeUp(rs.getFloat("Tup"));
            tm.setTimeTw(rs.getFloat("Tw"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " SpanTime " + tm, e);
        }
    }

    @Override
    public SpanTime getSpanTime(int idSpan, List<LocomotiveType> lt) throws DataManageException {
        SpanTime stm = new SpanTime();
        stm.setIdSpan(idSpan);
        Map<LocomotiveType, Time> mapTmO = stm.getLocTimeO();
        Map<LocomotiveType, Time> mapTmE = stm.getLocTimeE();
        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement()) {
                for (LocomotiveType l : lt) {
                    String sql = "SELECT IDspan, Tmove, Tdown, Tup, oe, Tw, IDltype FROM SpanTime WHERE IDspan = "
                            + idSpan + " AND IDltype = " + l.getIdLType() + " AND oe = '�����'";
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            Time tm = new Time();
                            fillItem(tm, rs);
                            mapTmO.put(l, tm);
                        }
                    } catch (DataManageException e) {
                        throw new DataManageException(" Span " + idSpan + " loc = " + l, e);
                    }
                    sql = "SELECT IDspan, Tmove, Tdown, Tup, oe, Tw, IDltype FROM SpanTime WHERE IDspan = "
                            + idSpan + " AND IDltype = " + l.getIdLType() + " AND oe = '���'";
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            Time tm = new Time();
                            fillItem(tm, rs);
                            mapTmE.put(l, tm);
                        }
                    } catch (DataManageException e) {
                        throw new DataManageException(" Span " + idSpan + " loc = " + l, e);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("SpanTime", e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return stm;
    }

    void fillItem(SpanStTime tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdScale(rs.getInt("IDscale"));
            tm.setIdLType(rs.getInt("IDltype"));
            tm.setNum(rs.getInt("num"));
            tm.setTmoveO(rs.getFloat("Tmove_o"));
            tm.setTmoveE(rs.getFloat("Tmove_e"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("Span_StTime " + tm, e);
        }
    }

    @Override
    public List<SpanStTime> getSpanStTime(int idScale) throws DataManageException {
        List<SpanStTime> list = new ArrayList<>();
        String sql = "SELECT IDscale, IDltype, num, Tmove_o, Tmove_e FROM Span_StTime WHERE IDscale = "
                + idScale;
        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    SpanStTime tm = new SpanStTime();
                    fillItem(tm, rs);
                    list.add(tm);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("idScale = " + idScale, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**{@inheritDoc}*/
    @Override
    public void saveSpanStTime(List<SpanStTime> list) throws DataManageException {
        Set<Integer> scaleIDSet = getAllScale(list);
        String sqlDel = "DELETE FROM Span_StTime WHERE IDscale = ?";
        String sql = "INSERT INTO Span_StTime (IDscale, IDltype, num, Tmove_o, Tmove_e) VALUES (?, ?, ?, ?, ?)";
        try {
            Connection cn = cm.getConnection();
            try (PreparedStatement pst = cn.prepareStatement(sql);
                 PreparedStatement pstDel = cn.prepareStatement(sqlDel)) {
                for (Integer scaleID : scaleIDSet) {
                    pstDel.setInt(1, scaleID);
                    pstDel.executeUpdate();
                }
                for (int i = 0; i < list.size(); i++) {
                    SpanStTime tm = list.get(i);
                    pst.setInt(1, tm.getIdScale());
                    pst.setInt(2, tm.getIdLType());
                    pst.setInt(3, tm.getNum());
                    pst.setDouble(4, tm.getTmoveO());
                    pst.setDouble(5, tm.getTmoveE());
                    pst.executeUpdate();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("saveSpanStTime: " + e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private Set<Integer> getAllScale(List<SpanStTime> list) {
        Set<Integer> set = new HashSet<>();
        for(SpanStTime time: list){
            set.add(time.getIdScale());
        }
        return set;
    }

    @Override
    public void saveSpanTime(int idSpan, SpanTime spanTime) throws DataManageException {
        String del = "DELETE FROM SpanTime WHERE IDspan = " + idSpan;
        String sql = "INSERT INTO SpanTime (IDspan, Tmove, Tdown, Tup, Tw, oe, IDltype, type)" +
                " VALUES (?,?,?,?,?,?,?,?) ";
        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 PreparedStatement pst = cn.prepareStatement(sql);) {
                st.executeUpdate(del);
                saveSpanTime(idSpan, spanTime.getLocTimeO(), OE.odd, pst);
                saveSpanTime(idSpan, spanTime.getLocTimeE(), OE.even, pst);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private void saveSpanTime(int idSpan, Map<LocomotiveType, Time> map, OE oe, PreparedStatement pst)
            throws SQLException {
        for (Entry<LocomotiveType, Time> ent : map.entrySet()) {
            LocomotiveType lt = ent.getKey();
            Time tm = ent.getValue();
            pst.setInt(1, idSpan);
            pst.setFloat(2, tm.getTimeMove());
            pst.setFloat(3, tm.getTimeDown());
            pst.setFloat(4, tm.getTimeUp());
            pst.setFloat(5, tm.getTimeTw());
            pst.setString(6, oe.getString());
            pst.setInt(7, lt.getIdLType());
            pst.setString(8, lt.getLtype());
            pst.executeUpdate();
        }
    }
}
