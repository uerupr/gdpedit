package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.IntGR;
import com.gmail.alaerof.dao.dto.StationIntGR;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.IStationDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import java.util.Map;
import javax.imageio.ImageIO;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


import static org.apache.commons.lang.StringUtils.trim;

/**
 */
public class StationDAOSQL implements IStationDAO {
    protected static Logger logger = LogManager.getLogger(StationDAOSQL.class);
    private final ConnectionManager cmServer;
    private final ConnectionManager cmAccess;


    public StationDAOSQL(ConnectionManager cmServer, ConnectionManager cmAccess) {
        this.cmServer = cmServer;
        this.cmAccess = cmAccess;
    }

    private void fillItem(Station s, ResultSet rs) throws DataManageException {
        try {
            s.setCodeESR(getValueAfterTrim(rs.getString("CodeESR")));
            s.setCodeExpress(rs.getString("CodeExpress"));
            s.setName(getValueAfterTrim(rs.getString("name")));
            s.setType(getValueAfterTrim(rs.getString("type")));
            s.setPassBld(rs.getInt("PassBld"));
            s.setCountLineO(rs.getInt("countLine_o"));
            s.setCountLineE(rs.getInt("countLine_e"));
            s.setBr(rs.getBoolean("BR"));
            s.setJoint(rs.getBoolean("joint")); // patch ��� ��������� �� !!!
            s.setIdDir(rs.getInt("IDdir"));
            s.setCodeESR2(getValueAfterTrim(rs.getString("CodeESR2")));
            s.setMaxMassO(rs.getInt("MaxMass_o"));
            s.setMaxMassE(rs.getInt("MaxMass_e"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("Station " + s, e);
        }
    }

    private String getValueAfterTrim(String value) {
        if (value != null) {
            return value.trim();
        }
        return "0";
    }

    private void fillLines(Station s) throws PoolManagerException, DataManageException {
        List<LineSt> list = new ArrayList<>();
        s.setListLine(list);
        LineSt l = null;
        String sqlL = "SELECT IDlineSt, IDst, N, [close], PS, len, el, pz, gr_o, gr_e, pass_o, pass_e, CloseInGDP, colorLn "
                + " FROM LineSt WHERE IDst = " + s.getIdSt() + " ORDER BY N";
        if (cmServer.isHSQLDB()) {
            sqlL = "SELECT IDlineSt, IDst, N, close, PS, len, el, pz, gr_o, gr_e, pass_o, pass_e, CloseInGDP, colorLn "
                    + " FROM LineSt WHERE IDst = " + s.getIdSt() + " ORDER BY N";
        }
        Connection cn = cmServer.getConnection();
        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sqlL)) {
            while (rs.next()) {
                String ps = rs.getString("PS");
                if (ps != null && !ps.trim().isEmpty()) {
                    l = new LineSt();
                    list.add(l);
                    l.setIdLineSt(rs.getInt("IDlineSt"));
                    l.setIdSt(rs.getInt("IDst"));
                    l.setN(rs.getInt("N"));
                    l.setClose(rs.getBoolean("close"));
                    l.setPs(rs.getString("PS"));
                    l.setLen(rs.getInt("len"));
                    l.setEl(rs.getBoolean("el"));
                    l.setPz(rs.getBoolean("pz"));
                    l.setGrO(rs.getBoolean("gr_o"));
                    l.setGrE(rs.getBoolean("gr_e"));
                    l.setPassO(rs.getBoolean("pass_o"));
                    l.setPassE(rs.getBoolean("pass_e"));
                    l.setCloseInGDP(rs.getBoolean("CloseInGDP"));
                    l.setColorLn(rs.getInt("colorLn"));
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("LineSt " + l, e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public Station getStation(int idSt) throws ObjectNotFoundException, DataManageException {
        Station s = null;
        String sql = "SELECT CodeESR, CodeExpress, name, type, PassBld, "
                + "countLine_o, countLine_e, BR, joint, IDdir, CodeESR2, MaxMass_o, MaxMass_e "
                + " FROM Station  WHERE IDst = " + idSt;
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                if (rs.next()) {
                    s = new Station();
                    s.setIdSt(idSt);
                    fillItem(s, rs);
                } else {
                    throw new ObjectNotFoundException("Station IDst = " + idSt);
                }
                if (s != null) {
                    fillLines(s);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return s;
    }

    /**{@inheritDoc}*/
    @Override
    public List<IntGR> getAllIntGR() throws DataManageException {
        List<IntGR> list = new ArrayList<>();
        String sql = "SELECT IDintGR, name, type1, type2, stop1, stop2, val, image " +
                " FROM IntGR";
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    IntGR item = new IntGR();
                    fillItem(item, rs);
                    list.add(item);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("AllIntGR", e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    private void fillItem(IntGR item, ResultSet rs) throws DataManageException {
        try {
            item.setIdIntGR(rs.getInt("IDintGR"));
            item.setName(rs.getString("name"));
            String type1 = rs.getString("type1");
            item.setFirstType(TrainType.getType(type1));
            String type2 = rs.getString("type2");
            item.setSecondType(TrainType.getType(type2));
            item.setFirstStop(rs.getBoolean("stop1"));
            item.setSecondStop(rs.getBoolean("stop2"));
            item.setDefaultValue(rs.getInt("val"));
            try {
                Blob blob = rs.getBlob("image");
                InputStream in = blob.getBinaryStream();
                Image image = ImageIO.read(in);
                item.setImage(image);
            } catch (IOException e) {
                logger.error(e.toString(), e);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(item.toString(), e);
        }
    }

    @Override
    public List<IntGR> getLineStInt(int idSt) throws DataManageException {
        List<IntGR> list = new ArrayList<>();
        String sql = "SELECT LineSt_Int.IDst, LineSt_Int.IDintGR, IntGR.[name], IntGR.type1, IntGR.type2, IntGR.stop1, IntGR.stop2, LineSt_Int.val, IntGR.[image] ";
        if(cmServer.isHSQLDB()){
            sql = "SELECT LineSt_Int.IDst, LineSt_Int.IDintGR, IntGR.name, IntGR.type1, IntGR.type2, IntGR.stop1, IntGR.stop2, LineSt_Int.val, IntGR.image ";
        }
        sql = sql + " FROM IntGR, LineSt_Int " +
                " WHERE LineSt_Int.IDst = " + idSt + " AND LineSt_Int.IDintGR = IntGR.IDintGR ";
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    IntGR item = new IntGR();
                    item.setIdIntGR(rs.getInt("IDintGR"));
                    item.setName(rs.getString("name"));
                    String type1 = rs.getString("type1");
                    item.setFirstType(TrainType.getType(type1));
                    String type2 = rs.getString("type2");
                    item.setSecondType(TrainType.getType(type2));
                    item.setFirstStop(rs.getBoolean("stop1"));
                    item.setSecondStop(rs.getBoolean("stop2"));
                    item.setDefaultValue(rs.getInt("val"));
                    try {
                        Blob blob = rs.getBlob("image");
                        InputStream in = blob.getBinaryStream();
                        Image image = ImageIO.read(in);
                        item.setImage(image);
                    } catch (IOException e) {
                        logger.error(e.toString(), e);
                    }
                    list.add(item);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("idSt = " + idSt, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**{@inheritDoc}*/
    @Override
    public List<StationIntGR> getStationIntGR(int idSt) throws DataManageException {
        List<StationIntGR> list = new ArrayList<>();
        String sql = "SELECT IDst, IDintGR, val FROM LineSt_Int WHERE IDst = " + idSt;
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    StationIntGR item = new StationIntGR();
                    fillItem(item, rs);
                    list.add(item);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("idSt = " + idSt, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**{@inheritDoc}*/
    @Override
    public Map<Integer, List<StationIntGR>> getStationIntGR(List<Integer> idSts) throws DataManageException {
        Map<Integer, List<StationIntGR>> map = new HashMap<>();
        StringBuilder ids = new StringBuilder();
        for(Integer idSt: idSts){
            ids.append(idSt).append(",");
        }
        ids.deleteCharAt(ids.lastIndexOf(","));

        String sql = "SELECT IDst, IDintGR, val FROM LineSt_Int WHERE IDst IN (" + ids.toString() + ")";
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    StationIntGR item = new StationIntGR();
                    fillItem(item, rs);
                    List<StationIntGR> list = map.get(item.getIdSt());
                    if (list == null) {
                        list = new ArrayList<>();
                        map.put(item.getIdSt(), list);
                    }
                    list.add(item);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("idSt: = " + idSts, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return map;
    }

    private void fillItem(StationIntGR item, ResultSet rs) throws DataManageException {
        try {
            item.setIdSt(rs.getInt("IDst"));
            item.setIdIntGR(rs.getInt("IDintGR"));
            item.setValue(rs.getInt("val"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(item.toString(), e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void updateStation(Station station) throws DataManageException {
        String sql = "UPDATE Station ";
        if (cmServer.isHSQLDB()) {
            sql = sql + " SET CodeESR = ?, CodeExpress = ?,  name = ?, type = ?, MaxMass_o = ?, MaxMass_e = ?, ";
        } else {
            sql = sql + " SET CodeESR = ?, CodeExpress = ?,  [name] = ?, [type] = ?, MaxMass_o = ?, MaxMass_e = ?, ";
        }
        sql = sql + " PassBld = ?, countLine_o = ?, countLine_e = ?, BR = ?, IDdir = ?, CodeESR2 = ?"
                + " WHERE IDst = ? ";

        try {
            Connection cn = cmServer.getConnection();
            try (PreparedStatement st = cn.prepareStatement(sql)) {
                st.setString(1, trim(station.getCodeESR()));
                st.setString(2, trim(station.getCodeExpress()));
                st.setString(3, trim(station.getName()));
                st.setString(4, trim(station.getType()));
                st.setInt(5, station.getMaxMassO());
                st.setInt(6, station.getMaxMassE());
                st.setInt(7, station.getPassBld());
                st.setInt(8, station.getCountLineO());
                st.setInt(9, station.getCountLineE());
                st.setBoolean(10, station.isBr());
                st.setInt(11, station.getIdDir());
                st.setString(12, trim(station.getCodeESR2()));
                st.setInt(13, station.getIdSt());
                st.addBatch();
                st.executeBatch();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void updateLineSt(List<LineSt> lineStList, int idSt) throws DataManageException {
        String sql = "UPDATE LineSt SET N = ?, [close] = ?, PS = ?, len = ?, el = ?, pz = ?, gr_o = ?, gr_e = ?, pass_o = ?, pass_e = ?, CloseInGDP = ?, colorLn = ? ";
        if (cmServer.isHSQLDB()) {
            sql = "UPDATE LineSt SET N = ?, close = ?, PS = ?, len = ?, el = ?, pz = ?, gr_o = ?, gr_e = ?, pass_o = ?, pass_e = ?, CloseInGDP = ?, colorLn = ? ";
        }
        sql = sql + "WHERE IDst = " + idSt + " AND IDlineSt = ?";
        String sqlNew = "INSERT INTO LineSt (IDst, N, [close], PS, len, el, pz, gr_o, gr_e, pass_o, pass_e, CloseInGDP, colorLn) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        if (cmServer.isHSQLDB()) {
            sqlNew = "INSERT INTO LineSt (IDst, N, close, PS, len, el, pz, gr_o, gr_e, pass_o, pass_e, CloseInGDP, colorLn) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        }
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 PreparedStatement pst = cn.prepareStatement(sql);
                 PreparedStatement pstNew = cn.prepareStatement(sqlNew);) {
                for (LineSt lineSt : lineStList) {
                    if (lineSt.getIdLineSt() != 0) {
                        pst.setInt(1, lineSt.getN());
                        pst.setBoolean(2, lineSt.isClose());
                        pst.setString(3, trim(lineSt.getPs()));
                        pst.setInt(4, lineSt.getLen());
                        pst.setBoolean(5, lineSt.isEl());
                        pst.setBoolean(6, lineSt.isPz());
                        pst.setBoolean(7, lineSt.isGrO());
                        pst.setBoolean(8, lineSt.isGrE());
                        pst.setBoolean(9, lineSt.isPassO());
                        pst.setBoolean(10, lineSt.isPassE());
                        pst.setBoolean(11, lineSt.isCloseInGDP());
                        pst.setInt(12, lineSt.getColorLn());
                        pst.setInt(13, lineSt.getIdLineSt());
                        pst.executeUpdate();
                    } else {
                        pstNew.setInt(1, idSt);
                        pstNew.setInt(2, lineSt.getN());
                        pstNew.setBoolean(3, lineSt.isClose());
                        pstNew.setString(4, trim(lineSt.getPs()));
                        pstNew.setInt(5, lineSt.getLen());
                        pstNew.setBoolean(6, lineSt.isEl());
                        pstNew.setBoolean(7, lineSt.isPz());
                        pstNew.setBoolean(8, lineSt.isGrO());
                        pstNew.setBoolean(9, lineSt.isGrE());
                        pstNew.setBoolean(10, lineSt.isPassO());
                        pstNew.setBoolean(11, lineSt.isPassE());
                        pstNew.setBoolean(12, lineSt.isCloseInGDP());
                        pstNew.setInt(13, lineSt.getColorLn());
                        pstNew.executeUpdate();
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void deleteLineSt(int idSt, int idLineSt) throws DataManageException {
        String sqlDel = "DELETE FROM LineSt WHERE IDlineSt = " + idLineSt + " AND IDst = " + idSt;
        try {
            Connection cn = cmServer.getConnection();
            Statement st = cn.createStatement();
            st.executeUpdate(sqlDel);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public void saveLineStInt(List<StationIntGR> stationIntGRList, int idSt) throws DataManageException {
        String sqlDel = "DELETE FROM LineSt_Int WHERE IDst = " + idSt;
        String sql = "INSERT INTO LineSt_Int (IDst, IDintGR, val) VALUES (?, ?, ?)";
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement();
                 PreparedStatement pst = cn.prepareStatement(sql);) {
                st.executeUpdate(sqlDel);
                for (StationIntGR stationIntGR : stationIntGRList) {
                    pst.setInt(1, idSt);
                    pst.setInt(2, stationIntGR.getIdIntGR());
                    pst.setInt(3, stationIntGR.getValue());
                    pst.executeUpdate();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public String getTrainCombSource(int idSt) throws DataManageException {
        String decouplingPath = "";
        String sql = "SELECT IDst, CombSource FROM StationParam WHERE IDst = " + idSt;
        try {
            Connection cn = cmAccess.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                if (rs.next()) {
                    decouplingPath = rs.getString("CombSource");
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("idSt = " + idSt, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return decouplingPath;
    }

    /**{@inheritDoc}*/
    @Override
    public void saveTrainCombSource(int idSt, String decouplingPath) throws DataManageException {
        String sqlDel = "DELETE FROM StationParam WHERE IDst = " + idSt;
        String sql = "INSERT INTO StationParam (IDst, CombSource) VALUES (?, ?)";
        try {
            Connection cn = cmAccess.getConnection();
            try (Statement st = cn.createStatement();
                 PreparedStatement pst = cn.prepareStatement(sql);) {
                st.executeUpdate(sqlDel);
                pst.setInt(1, idSt);
                pst.setString(2, decouplingPath);
                pst.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }
}
