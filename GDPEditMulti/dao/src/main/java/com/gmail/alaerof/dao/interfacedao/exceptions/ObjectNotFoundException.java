package com.gmail.alaerof.dao.interfacedao.exceptions;
/**
 * @author Helen Yrofeeva
 */
public class ObjectNotFoundException extends Exception {
	public ObjectNotFoundException(String msg) {
		super(msg);
	}
}
