package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.List;

public interface IGDPCalcDAO {
    /**
     * ������ ������� (�� '�������' �� ����� �������) �� SpTOcc ��� ��������� �������� idSpan,
     * �� ��� ������ �  DistrTrain ����� ��������������, � ������ ����������� �� Train � SpTOcc
     * Train:
     *  - IDcat ?
     *  - code
     *  - Name
     *  - type
     *  - oe ?
     *
     * SpTOcc:
     *  - ���
     *
     * @param idSpan
     * @param trainIDs
     * @param idDistr
     * @return ������ DistrTrain
     * @throws DataManageException
     */
    List<DistrTrain> getSpanDistrTrains(int idSpan, String trainIDs, int idDistr) throws DataManageException;

    /**
     * ������ ������� (�� '�������' �� ����� �������) �� LineSt_Train ��� �������� ������� idSt,
     * �� ��� ������ �  DistrTrain ����� ��������������, � ������ ����������� �� Train � LineSt_Train
     *
     * @param idSt
     * @param trainIDs
     * @param idDistr
     * @return ������ DistrTrain
     * @throws DataManageException
     */
    List<DistrTrain> getStationDistrTrains(int idSt, String trainIDs, int idDistr) throws DataManageException;
}
