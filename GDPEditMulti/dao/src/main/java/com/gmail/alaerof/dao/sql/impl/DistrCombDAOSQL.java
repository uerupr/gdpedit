package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.interfacedao.IDistrCombDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class DistrCombDAOSQL implements IDistrCombDAO {
    protected static Logger logger = LogManager.getLogger(DistrCombDAOSQL.class);
    private final ConnectionManager cm;

    public DistrCombDAOSQL(ConnectionManager cm) {
        this.cm = cm;
    }

    @Override
    public List<Integer> getDistrCombList(int idDistr) throws DataManageException {
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT IDdistrcomb, IDdistr, IDdistrC FROM DistrComb WHERE IDdistr = " + idDistr;
        try {
            Connection cn = cm.getConnection();
            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql))
            {
                while (rs.next()) {
                    list.add(rs.getInt("IDdistrC"));
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    @Override
    public void deleteDistrComb(int idDistr) throws DataManageException {
        if (idDistr > 0) {
            String sql = "DELETE FROM DistrComb WHERE IDdistr = ?";
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement st = cn.prepareStatement(sql)) {
                    st.setInt(1, idDistr);
                    st.executeUpdate();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    @Override
    public void addDistrCombList(List<Distr> distrCombList, int idDistr) throws DataManageException {
        if (distrCombList != null && distrCombList.size() > 0) {
            String sql = "INSERT INTO DistrComb (IDdistr, IDdistrC) VALUES (?,?)";
            try {
                Connection cn = cm.getConnection();
                int count = 0;
                try(PreparedStatement st = cn.prepareStatement(sql)) {
                    for (Distr distr : distrCombList) {
                        if (distr.getIdDistr() > 0) {
                            st.setInt(1, idDistr);
                            st.setInt(2, distr.getIdDistr());
                            st.addBatch();
                            count++;
                        }
                    }
                    if (count > 0) {
                        st.executeBatch();
                    }
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

}
