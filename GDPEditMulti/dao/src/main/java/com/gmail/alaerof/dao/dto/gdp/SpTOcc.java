package com.gmail.alaerof.dao.dto.gdp;

/**
 * ���������� ����� ������ �� ��������
 * 
 * @author Helen Yrofeeva
 * 
 */
public class SpTOcc {
    /** ���  �������� */
    private int idSpan; // IDspan
    private long idTrain; // IDtrain
    private String occupyOn;
    private String occupyOff;
    private String oe;
    private int nextDay; // NextDay 0 ���� ��� �������� �� ����� ����� �� ���� ��������, 1 ���� ����
    private int idDistr; // IDdistr
    private int tmove; // Tmove
    public int getIdSpan() {
        return idSpan;
    }
    public void setIdSpan(int idSpan) {
        this.idSpan = idSpan;
    }
    public long getIdTrain() {
        return idTrain;
    }
    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }
    public String getOccupyOn() {
        return occupyOn;
    }
    public void setOccupyOn(String occupyOn) {
        this.occupyOn = occupyOn;
    }
    public String getOccupyOff() {
        return occupyOff;
    }
    public void setOccupyOff(String occupyOff) {
        this.occupyOff = occupyOff;
    }
    public String getOe() {
        return oe;
    }
    public void setOe(String oe) {
        this.oe = oe;
    }
    public int getNextDay() {
        return nextDay;
    }
    public void setNextDay(int nextDay) {
        this.nextDay = nextDay;
    }
    public int getIdDistr() {
        return idDistr;
    }
    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }
    public int getTmove() {
        return tmove;
    }
    public void setTmove(int tmove) {
        this.tmove = tmove;
    }
    
    @Override
    public String toString() {
        return "SpTOcc [idSpan=" + idSpan + ", idTrain=" + idTrain + ", occupyOn=" + occupyOn
                + ", occupyOff=" + occupyOff + "]";
    }

    
    
}
