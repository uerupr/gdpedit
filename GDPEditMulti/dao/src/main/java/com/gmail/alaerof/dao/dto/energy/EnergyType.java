package com.gmail.alaerof.dao.dto.energy;

public enum EnergyType {
    electro, teplo;

    public String getString() {
        switch (this) {
            case electro:
                return "электровоз";
            case teplo:
                return "тепловоз";
            default:
                return null;
        }
    }

    public static EnergyType getType(String type){
        if("электровоз".equals(type)){
            return electro;
        }
        return teplo;
    }
}
