package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.gdp.GDParm;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.sql.impl.GDParmDAOSQL;

import java.util.List;

public interface IGDParmDAO {
  List<GDParm> getListGDParm(int idDistr) throws PoolManagerException;

  List<GDParm> getListGDParm(int idDistr, GDParmDAOSQL.GDP gdp) throws PoolManagerException;

  List<GDParm> getListGDParmActual(int idDistr) throws PoolManagerException;

  List<GDParm> getListGDParmArchive(int idDistr) throws PoolManagerException;

  List<GDParm> getListGDParmNormative(int idDistr) throws PoolManagerException;

  List<GDParm> getListGDParmVariant(int idDistr) throws PoolManagerException;

  boolean updateGDParm(int idGDParm, int arch) throws PoolManagerException;

  int insertGDParm(GDParm aGDParm) throws PoolManagerException;

  int getMaxId() throws PoolManagerException;
}
