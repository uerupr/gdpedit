package com.gmail.alaerof.dao.dto;

import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.util.ColorConverter;
import java.awt.Color;

/**
 *
 */
public class Category {
    private int idCat;
    // ������������
    private String name;
    // ���������, ��� ������ ��������, ��� ������
    private int priority; // prioritet_
    // c �
    private int trMin;
    // �� �
    private int trMax;
    // ��������� (��������� ��������: ����, ��, ����)
    private TrainType type;
    // ����
    private Color color = Color.BLACK;
    // �������� �� ��������� ������� ��� ����
    // todo �� ������������ ��� ����������
    private boolean bType;
    // ��������� ��� �������� ��������� �������
    // todo �� ������������ ��� ����������
    private TrainType base;

    public int getIdCat() {
        return idCat;
    }

    public void setIdCat(int idCat) {
        this.idCat = idCat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getTrMin() {
        return trMin;
    }

    public void setTrMin(int trMin) {
        this.trMin = trMin;
    }

    public int getTrMax() {
        return trMax;
    }

    public void setTrMax(int trMax) {
        this.trMax = trMax;
    }

    public TrainType getType() {
        return type;
    }

    public void setType(TrainType type) {
        this.type = type;
    }

    public void setType(String type) {
        TrainType t = TrainType.getType(type);
        this.type = t;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Category{" +
                "idCat=" + idCat +
                ", name='" + name + '\'' +
                ", priority=" + priority +
                ", trMin=" + trMin +
                ", trMax=" + trMax +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        return idCat == category.idCat;
    }

    @Override
    public int hashCode() {
        return idCat;
    }
}
