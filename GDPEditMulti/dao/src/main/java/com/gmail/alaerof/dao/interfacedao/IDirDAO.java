package com.gmail.alaerof.dao.interfacedao;

import java.util.List;

import com.gmail.alaerof.dao.dto.Dir;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;

/**
 * @author Helen Yrofeeva
 */
public interface IDirDAO {
	List<Dir> getListDir() throws DataManageException;
	Dir getDir(int idDir) throws ObjectNotFoundException, DataManageException;

}
