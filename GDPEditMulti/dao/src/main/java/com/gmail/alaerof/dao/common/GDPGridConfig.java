package com.gmail.alaerof.dao.common;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import com.gmail.alaerof.util.CalcString;
import com.gmail.alaerof.util.ColorConverter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * �������� ������ ��� ��������� ���
 * @author Helen Yrofeeva
 */
public class GDPGridConfig {
    private static Logger logger = LogManager.getLogger(GDPGridConfig.class);

    // ��� ���� � ! ����������� � ����������������� ����� getContent/setContent

    /** ! ������ �������� �����������, ������������ � ����� ������ */
    public List<String> listSelectedLoc = new ArrayList<>();
    /** ! ������ �������� �������, ��� ������� ����� ��������� ����� */
    public List<String> listSelectedSt = new ArrayList<>();
    
    // ------- �� �� --------
    // ���������� + �������
    /** ! �������� � ���, ���� */
    public int picTimeVert = 5;
    /** ! �������� � ���, ��� */
    public int picTimeHor = 3;
    /** ! ������� � �������� �� 0.3 �� 1 */
    // ��������� ���� ������� ������ �� ������������,
    // ����... ������ ������ ���� ��������
    public float scale = 1f;

    // ����� �����
    /**
     * ����� ���� �������, ������ ������� � �������� (�� ������� �����������
     * ���� �����+���)
     */
    public int spanTimeW = 56;
    /** ���������� ������� ������ ���� */
    public int spanTimeCount = 2;
    /** �������� �������, ������ ������� � �������� */ // ��������� !!!
    public int stationNameW = 160;
    /** �������� ������������, ������ ������� � �������� */
    public int scbW = 27;
    /** ���-�� �����, ������ ������� � �������� */
    public int lineStW = 40;
    /** ���� ������, ������ � �������� */
    public int passBuildWidth = 4;
    /** ���� ������, ������ � �������� */
    public int passBuildHeight = 10;

    // �������
    /** �� ����� �� ������, ������ � �������� */
    public int indentVert = 45;
    /** �� ����� �� ������, ������ � �������� */
    public int indentHor = 12;
    /** �� ����� ������� ������ ���� � ���� ������, ������ � �������� */
    public int gdpTop = 60;
    /** �� �������, ������ � �������� */
    public int gdpBottom = 30;
    /** ����������� ����� �������� � ������� */
    public int minSpanLen = 10;
    /** ����� ��������� (= min ����� ��������) */
    public int indentDistr = 15;
    /** ����� ������ ������� */
    public int indentLine = 15;

    /** ! ����� ������ (�����) �� ����� �������, ������ � �������� */
    public int codeStepO = 15;
    /** ! ����� ������ (���) �� ����� �������, ������ � �������� */
    public int codeStepE = 24;

    // ���� � ������
    /** ! ���� ����� ������� */
    public Color gdpGridColor = new Color(75, 75, 0);
    /** ! ���� ������� � ����� */
    public Color gdpStHourColor = new Color(0, 0, 0);
    /** ! ����, ����� */
    public Font gdpHourFont = new Font("Sans Serif", Font.BOLD, 18);
    /** ! �����, ����� */
    public Font headFont = new Font("Sans Serif", 0, 12);
    /** ! ������/����������, ����� */
    public Font updownFont = new Font("Sans Serif", 0, 10);
    /** ! ����� ����, ����� */
    public Font moveFont = new Font("Sans Serif", 0, 12);
    /** ! �������� �.�., ����� */
    public Font stationFont = new Font("Sans Serif", 0, 14);
    /** ! �������� �����, ����� */
    public Font stationLineFont = new Font("Sans Serif", 0, 10);

    /** ! ��������/����������� ������, ����� */
    public Font trainTimeFont = new Font("Sans Serif", 0, 12);
    /** ! ������� ������ (�������), ����� */
    public Font trainCaptionSpFont = new Font("Times New Roman", Font.BOLD, 14);
    /** ! ������� ������ (�������), ����� */
    public Font trainCaptionStFont = new Font("Times New Roman", 0, 10);
    // ����������� ��������
    /** ! ������ ������ ���� */
    public boolean evenDown = false;
    /** ! ������ ������ ���� */
    public boolean zeroOnGrid = false;
    // ------- ��������� --------
    /** ������ ��� (����� ������� ��� ��� ��������) � �������� */
    public int gdpHeight;
    /** ������ ��� (�������� ������) � �������� */
    public int gdpWidth;
    /** ������ ��� (����� �� �����) � �������� */
    public int gdpWidth24;
    /** ����� ������ ����� ������ � �������� */
    public int stationPanelWidth;
    /** ������ ����� �������, ������������ �� ���������, ����� ����������*/
    public int hourB = 0;
    /** ����� ����� �������, ������������ �� ���������, ����� ����������*/
    public int hourE = 24;

    /** ������ ����������� */
    public GDPGridConfig(){ }

    /** ���������� ����������� */
    public GDPGridConfig(GDPGridConfig config) {
        String content = config.getContent();
        this.setContent(content);
    }

    /**
     * ������ ������� = ������ �� ����� ������� ������ ���� + ����� ������� +
     * ������ �����
     * @return ���������������� ������ �������
     */
    public int getScaledMainImageHeight() {
        //return (int) ((gdpTop + gdpHeight + indentVert) * scale);
        return (int) (gdpTop + (gdpHeight + indentVert) * scale);
    }

    public int getScaledHourHeight() {
        return CalcString.getStringH(gdpHourFont) + 4;
    }

    /**
     * ������ ����� * 2 + ������ ��� (������ �����)
     * @return ���������������� ������ �������
     */
    public int getScaledMainImageWidth() {
        return (int) ((indentHor * 2 + gdpWidth) * scale);
    }

    /**
     * @return ���������������� ������ ������� ����� ������
     */
    public int getScaledLeftWidth() {
        if (scale < 1) {
            return (int) ((stationPanelWidth + 1) * 1);
        }
        return (int) ((stationPanelWidth + 1) * scale);
    }

    /**
     * ���������� ����� ���������� �� ������
     * @param content  ���������� � ���� ������
     */
    public void setContent(String content) {
        try {
            String[] listFields = content.split(";");
            for (int i = 0; i < listFields.length; i++) {
                String[] value = listFields[i].split("=");
                if (value.length == 2) {
                    int size = 10;
                    switch (value[0]) {
                    case "picTimeVert":
                        try {
                            picTimeVert = Integer.parseInt(value[1]);
                        } catch (NumberFormatException e) {
                            picTimeVert = 5;
                            logger.error("picTimeVert " + e.toString(), e);
                        }
                        break;
                    case "picTimeHor":
                        try {
                            picTimeHor = Integer.parseInt(value[1]);
                        } catch (NumberFormatException e) {
                            picTimeHor = 3;
                            logger.error("picTimeHor " + e.toString(), e);
                        }
                        break;
                    case "scale":
                        try {
                            scale = Float.parseFloat(value[1]);
                        } catch (NumberFormatException e) {
                            scale = 1f;
                            logger.error("scale " + e.toString(), e);
                        }
                        break;
                    case "codeStepO":
                        try {
                            codeStepO = Integer.parseInt(value[1]);
                        } catch (NumberFormatException e) {
                            codeStepO = 15;
                            logger.error("codeStepO " + e.toString(), e);
                        }
                        break;
                    case "codeStepE":
                        try {
                            codeStepE = Integer.parseInt(value[1]);
                        } catch (NumberFormatException e) {
                            codeStepE = 24;
                            logger.error("codeStepE " + e.toString(), e);
                        }
                        break;
                    case "gdpGridColor":
                        try {
                            gdpGridColor = ColorConverter.convert(Integer.parseInt(value[1]));
                        } catch (NumberFormatException e) {
                            gdpGridColor = new Color(75, 75, 0);
                            logger.error("gdpGridColor " + e.toString(), e);
                        }
                        break;
                    case "gdpStHourColor":
                        try {
                            gdpStHourColor = ColorConverter.convert(Integer.parseInt(value[1]));
                        } catch (NumberFormatException e) {
                            gdpStHourColor = new Color(0, 0, 0);
                            logger.error("gdpStHourColor " + e.toString(), e);
                        }
                        break;
                    case "gdpHourFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            gdpHourFont = new Font("Sans Serif", Font.BOLD, size);
                        } catch (NumberFormatException e) {
                            logger.error("gdpHourFont " + e.toString(), e);
                        }
                        break;
                    case "headFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            headFont = new Font("Sans Serif", 0, size);
                        } catch (NumberFormatException e) {
                            logger.error("headFont " + e.toString(), e);
                        }
                        break;
                    case "updownFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            updownFont = new Font("Sans Serif", 0, size);
                        } catch (NumberFormatException e) {
                            logger.error("updownFont " + e.toString(), e);
                        }
                        break;
                    case "moveFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            moveFont = new Font("Sans Serif", 0, size);
                        } catch (NumberFormatException e) {
                            logger.error("moveFont " + e.toString(), e);
                        }
                        break;
                    case "stationFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            stationFont = new Font("Sans Serif", 0, size);
                        } catch (NumberFormatException e) {
                            logger.error("stationFont " + e.toString(), e);
                        }
                        break;
                    case "stationLineFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            stationLineFont = new Font("Sans Serif", 0, size);
                        } catch (NumberFormatException e) {
                            logger.error("stationLineFont " + e.toString(), e);
                        }
                        break;
                    case "trainTimeFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            trainTimeFont = new Font("Sans Serif", 0, size);
                        } catch (NumberFormatException e) {
                            logger.error("trainTimeFont " + e.toString(), e);
                        }
                        break;
                    case "trainCaptionSpFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            trainCaptionSpFont = new Font("Times New Roman", Font.BOLD, size);
                        } catch (NumberFormatException e) {
                            logger.error("trainCaptionSpFont " + e.toString(), e);
                        }
                        break;
                    case "trainCaptionStFont":
                        try {
                            size = Integer.parseInt(value[1]);
                            trainCaptionStFont = new Font("Times New Roman", 0, size);
                        } catch (NumberFormatException e) {
                            logger.error("trainCaptionStFont " + e.toString(), e);
                        }
                        break;
                    case "evenDown":
                        evenDown = Boolean.parseBoolean(value[1]);
                        break;
                    case "zeroOnGrid":
                        zeroOnGrid = Boolean.parseBoolean(value[1]);
                        break;
                    case "listLoc":
                        stringToListLoc(value[1]);
                        break;
                    case "listSt":
                        stringToListSt(value[1]);
                        break;
                    default:
                        break;
                    }
                }
            }
            if(listSelectedLoc.size()==0){
                listSelectedLoc.add("��������");
                listSelectedLoc.add("������������");
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ��������� ���������� � ���� ������ ��� ���������� � ��
     * @return ���������� � ���� ������
     */
    public String getContent() {
        StringBuilder content = new StringBuilder();
        content.append("picTimeVert=").append(picTimeVert).append(";");
        content.append("picTimeHor=").append(picTimeHor).append(";");
        content.append("scale=").append(scale).append(";");
        content.append("codeStepO=").append(codeStepO).append(";");
        content.append("codeStepE=").append(codeStepE).append(";");
        content.append("gdpGridColor=").append(ColorConverter.convert(gdpGridColor)).append(";");
        content.append("gdpStHourColor=").append(ColorConverter.convert(gdpStHourColor)).append(";");
        // ��� ������� ������ ������
        content.append("gdpHourFont=").append(gdpHourFont.getSize()).append(";");
        content.append("headFont=").append(headFont.getSize()).append(";");
        content.append("updownFont=").append(updownFont.getSize()).append(";");
        content.append("moveFont=").append(moveFont.getSize()).append(";");
        content.append("stationFont=").append(stationFont.getSize()).append(";");
        content.append("stationLineFont=").append(stationLineFont.getSize()).append(";");
        content.append("trainTimeFont=").append(trainTimeFont.getSize()).append(";");
        content.append("trainCaptionSpFont=").append(trainCaptionSpFont.getSize()).append(";");
        content.append("trainCaptionStFont=").append(trainCaptionStFont.getSize()).append(";");
        content.append("evenDown=").append(evenDown).append(";");
        content.append("listLoc=").append(listToString(listSelectedLoc)).append(";");
        content.append("listSt=").append(listToString(listSelectedSt)).append(";");
        content.append("zeroOnGrid=").append(zeroOnGrid).append(";");
        return content.toString();
    }

    private String listToString(List<String> list) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            res.append(list.get(i)).append("&");
        }
        return res.toString().trim();
    }

    private void stringToListLoc(String str) {
        listSelectedLoc = new ArrayList<>();
        String[] listValues = str.split("&");
        for (int i = 0; i < listValues.length; i++) {
            String s = listValues[i].trim();
            if (s.length() > 0) {
                listSelectedLoc.add(s);
            }
        }
    }

    private void stringToListSt(String str) {
        listSelectedSt = new ArrayList<>();
        String[] listValues = str.split("&");
        for (int i = 0; i < listValues.length; i++) {
            String s = listValues[i].trim();
            if (s.length() > 0) {
                listSelectedSt.add(s);
            }
        }
    }
 }
