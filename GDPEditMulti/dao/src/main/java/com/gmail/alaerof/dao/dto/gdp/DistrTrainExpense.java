package com.gmail.alaerof.dao.dto.gdp;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.energy.EnergyType;

public class DistrTrainExpense {
    private LocomotiveType locType;
    private EnergyType energyType;

    // Distr_Train (Energy)
    /** ����������� (�� ���������) ����� ���������� ������ �� ������� � ��������� � ������������, ��� */
    private double timeNormAll;
    /** ����������� (�� ���������) ����� ���������� ������ �� ������� ����� ��� �������� � ����������, ��� */
    private double timeNormMove;
    /** Eu ������� ������� � ������ �������, */
    private double energyWithStop;
    /** Et ������� ������� ��� ����� ������� (�� � ������ �������� � ����������),  */
    private double energyWithoutStop;
    /** Et ������� ������� ��� ����� �������, �������� � ����������,  */
    private double energyMove;

    // ������� �� ����� �������, ���
    /** ������� �� ������-���� � �������� */
    private double moveExpense;
    /** ������� �� ������-���� ������� */
    private double stopExpense;
    /** ������� �� ������ � ���������� */
    private double downUpExpense;
    /** ����������� ������� */
    private double normExpense;

    public DistrTrainExpense(LocomotiveType locType, EnergyType energyType) {
        this.locType = locType;
        this.energyType = energyType;
    }

    public LocomotiveType getLocType() {
        return locType;
    }

    public void setLocType(LocomotiveType locType) {
        this.locType = locType;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }

    public double getTimeNormAll() {
        return timeNormAll;
    }

    public void setTimeNormAll(double timeNormAll) {
        this.timeNormAll = timeNormAll;
    }

    public double getTimeNormMove() {
        return timeNormMove;
    }

    public void setTimeNormMove(double timeNormMove) {
        this.timeNormMove = timeNormMove;
    }

    public double getEnergyWithStop() {
        return energyWithStop;
    }

    public void setEnergyWithStop(double energyWithStop) {
        this.energyWithStop = energyWithStop;
    }

    public double getEnergyWithoutStop() {
        return energyWithoutStop;
    }

    public void setEnergyWithoutStop(double energyWithoutStop) {
        this.energyWithoutStop = energyWithoutStop;
    }

    public double getEnergyMove() {
        return energyMove;
    }

    public void setEnergyMove(double energyMove) {
        this.energyMove = energyMove;
    }

    public double getMoveExpense() {
        return moveExpense;
    }

    public void setMoveExpense(double moveExpense) {
        this.moveExpense = moveExpense;
    }

    public double getStopExpense() {
        return stopExpense;
    }

    public void setStopExpense(double stopExpense) {
        this.stopExpense = stopExpense;
    }

    public double getDownUpExpense() {
        return downUpExpense;
    }

    public void setDownUpExpense(double downUpExpense) {
        this.downUpExpense = downUpExpense;
    }

    public double getNormExpense() {
        return normExpense;
    }

    public void setNormExpense(double normExpense) {
        this.normExpense = normExpense;
    }

    public String getExpenseKeyString(){
        return locType.getLtype() + "-" + energyType.getString();
    }
}
