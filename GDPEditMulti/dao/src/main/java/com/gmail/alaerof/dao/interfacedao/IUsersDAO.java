package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.gdp.User;
import com.gmail.alaerof.dao.sql.PoolManagerException;

public interface IUsersDAO {
    User getUser(String userLogin) throws PoolManagerException;
}
