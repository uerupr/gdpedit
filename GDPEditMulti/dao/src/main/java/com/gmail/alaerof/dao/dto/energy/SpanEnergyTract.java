package com.gmail.alaerof.dao.dto.energy;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.energy.Energy;
import com.gmail.alaerof.dao.dto.gdp.OE;

public class SpanEnergyTract {
    private int idScale;
    private LocomotiveType locomotiveType;
    private OE oe;
    private Energy energy;

    public int getIdScale() {
        return idScale;
    }

    public LocomotiveType getLocomotiveType() {
        return locomotiveType;
    }

    public OE getOe() {
        return oe;
    }

    public Energy getEnergy() {
        return energy;
    }

    public void setIdScale(int idScale) {
        this.idScale = idScale;
    }

    public void setLocomotiveType(LocomotiveType locomotiveType) {
        this.locomotiveType = locomotiveType;
    }

    public void setOe(OE oe) {
        this.oe = oe;
    }

    public void setEnergy(Energy energy) {
        this.energy = energy;
    }

    @Override
    public String toString() {
        return "SpanEnergyTract{" +
                "idScale=" + idScale +
                ", locomotiveType=" + locomotiveType +
                ", energy=" + energy +
                ", oe=" + oe +
                '}';
    }
}
