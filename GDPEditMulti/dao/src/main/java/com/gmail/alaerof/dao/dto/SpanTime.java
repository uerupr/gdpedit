package com.gmail.alaerof.dao.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import java.util.Map;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * ������� ���� �� �������� (��� ���� ����� ��������)
 * 
 * @author Helen Yrofeeva
 * 
 */
public class SpanTime {
    private static Logger logger = LogManager.getLogger(SpanTime.class);
    private int idSpan;
    /** �������� */
    private Map<LocomotiveType, Time> locTimeO = new HashMap<>();
    /** ������ */
    private Map<LocomotiveType, Time> locTimeE = new HashMap<>();

    public int getIdSpan() {
        return idSpan;
    }

    public void setIdSpan(int idSpan) {
        this.idSpan = idSpan;
    }

    public Map<LocomotiveType, Time> getLocTimeO() {
        return locTimeO;
    }

    public Map<LocomotiveType, Time> getLocTimeE() {
        return locTimeE;
    }

    @Override
    public String toString() {
        return "SpanTime [idSpan=" + idSpan + ", locTimeO=" + locTimeO + ", locTimeE=" + locTimeE + "]";
    }

    public float getMaxO() {
        try {
            ArrayList<Time> lt = new ArrayList<>(locTimeO.values());
            if (lt.size() > 0)
                return Collections.max(lt).getTimeMove();
        } catch (Exception e) {
            logger.error(e.toString() + "\n" + locTimeO, e);
        }
        return 0;
    }

    public float getMaxE() {
        try {
            ArrayList<Time> lt = new ArrayList<>(locTimeE.values());
            if (lt.size() > 0)
                return Collections.max(lt).getTimeMove();
        } catch (Exception e) {
            logger.error(e.toString() + "\n" + locTimeE, e);
        }
        return 0;
    }
}
