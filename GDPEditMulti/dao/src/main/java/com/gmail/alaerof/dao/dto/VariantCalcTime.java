package com.gmail.alaerof.dao.dto;

/**
 * ������� St_Time
 * ���� ������ �� �������
 * @author Helen Yrofeeva
 */
public class VariantCalcTime implements Comparable<VariantCalcTime> {
    public static enum TypeSt {
        station, town;
        public String getString() {
            switch (this) {
            case station:
                return "�.�.";
            case town:
                return "�.�.";
            default:
                return null;
            }
        }

        /** ������ ����� "�.�." � "�.�." ���������� ������ enum */
        public static TypeSt getTypeSt(String type) {
            if ("�.�.".equals(type)) {
                return station;
            }
            if ("�.�.".equals(type)) {
                return town;
            }
            return null;
        }
    }

    private int idvarCalc; // IDvarCalc;
    /** �������� ������ */
    private String name;
    /** ��� ��� ������  */
    private String codeESR; // CodeESR
    /** �.�. ��� �.�. */
    private TypeSt type;
    private int num;
    //----------- � ��� ��� ��� ��� ������� ������ ����� �.�. !!! -------

    /** ���������� */
    private double l; // L
    /** ����� �������� �� ����� ������ (� �����������) */
    private double t1; // T1
    // private double t2; // T2
    // private double tstop; // Tstop
    /** ���������� �� ���� ����� */
    private double tdown; // Tdown
    /** ������ � ����� ������ */
    private double tup; // Tup
    private double tw; // Tw
    private double abs; // abs
    /** ���. ����� ���� */
    private double dt; //dT REAL, 
    /** ������ � ����� ������ �� ���. ������� */
    private double tupAdd; // TupAdd REAL, 
    /** ���������� �� ���� ����� �� ���. ������� */
    private double tdownAdd; //TdownAdd REAL,

    //----------- � ��� ��� ��� ��� ��� ������� ������ ����� �.�. !!! -------
    /** ���������� */
    private double townL; // L
    /** ����� �������� �� ����� ������ (� �����������) */
    private double townT1; // T1
    /** ���. ����� ���� */
    private double townDt; //dT REAL, 

    public int getIdvarCalc() {
        return idvarCalc;
    }

    public void setIdvarCalc(int idvarCalc) {
        this.idvarCalc = idvarCalc;
    }

    /** �������� ������ (�.�. ��� �.�.) */
    public String getName() {
        return name;
    }

    /** �������� ������ (�.�. ��� �.�.) */
    public void setName(String name) {
        this.name = name;
    }

    /** ��� ��� ������ (�.�. ��� �.�.) */
    public String getCodeESR() {
        return codeESR;
    }

    /** ��� ��� ������ (�.�. ��� �.�.) */
    public void setCodeESR(String codeESR) {
        this.codeESR = codeESR;
    }

    /** �.�. ��� �.�. */
    public String getType() {
        return type.getString();
    }

    /** �.�. ��� �.�. */
    public void setType(String type) {
        TypeSt ts = TypeSt.getTypeSt(type);
        if (ts == null) {
            ts = TypeSt.station;
        }
        this.type = ts;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getL() {
        return l;
    }

    public void setL(double l) {
        this.l = l;
    }

    public double getT1() {
        return t1;
    }

    public void setT1(double t1) {
        this.t1 = t1;
    }

    public double getTdown() {
        return tdown;
    }

    public void setTdown(double tdown) {
        this.tdown = tdown;
    }

    public double getTup() {
        return tup;
    }

    public void setTup(double tup) {
        this.tup = tup;
    }

    public double getAbs() {
        return abs;
    }

    public void setAbs(double abs) {
        this.abs = abs;
    }

    public boolean isTown(){
        return TypeSt.town.equals(this.type);
    }

    @Override
    public int compareTo(VariantCalcTime o) {
        return this.num - o.num;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codeESR == null) ? 0 : codeESR.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VariantCalcTime other = (VariantCalcTime) obj;
        if (codeESR == null) {
            if (other.codeESR != null)
                return false;
        } else if (!codeESR.equals(other.codeESR))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public double getTw() {
        return tw;
    }

    public void setTw(double tw) {
        this.tw = tw;
    }

    public double getDt() {
        return dt;
    }

    public void setDt(double dt) {
        this.dt = dt;
    }

    public double getTupAdd() {
        return tupAdd;
    }

    public void setTupAdd(double tupAdd) {
        this.tupAdd = tupAdd;
    }

    public double getTdownAdd() {
        return tdownAdd;
    }

    public void setTdownAdd(double tdownAdd) {
        this.tdownAdd = tdownAdd;
    }

    public double getTownL() {
        return townL;
    }

    public void setTownL(double townL) {
        this.townL = townL;
    }

    public double getTownT1() {
        return townT1;
    }

    public void setTownT1(double townT1) {
        this.townT1 = townT1;
    }

    public double getTownDt() {
        return townDt;
    }

    public void setTownDt(double townDt) {
        this.townDt = townDt;
    }

}
