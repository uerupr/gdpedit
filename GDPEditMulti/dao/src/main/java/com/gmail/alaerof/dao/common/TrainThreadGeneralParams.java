package com.gmail.alaerof.dao.common;

import java.awt.Font;
/**
 * ��������� �����������, ����� ��� ���� ����� (������, �������)
 *@author Helen Yrofeeva
 */
public class TrainThreadGeneralParams {
    /** ����� ������ (�����) �� ����� �������, ������ � �������� �� ���������*/
    public int codeStepO = 15;
    /** ����� ������ (���) �� ����� �������, ������ � �������� �� ���������*/
    public int codeStepE = 20;
    /** ����� ������� ������ �� �������� */
    public Font captionSp;
    /** ����� ������� ������ �� ������� */
    public Font captionSt;
    /** ����� ����� ��������/����������� */
    public Font time;
    /** ������ ����� ��� �� � */
    public int xLeft;
    /** ����� ����� ��� �� � */
    public int xRight;
    /** ����� ����� ��� �� � ��� 24 ����� */
    public int xRight24;
    /** ����� ����� ����� �� � � ����������� ��� */
    public int xLRightJoint;
    /** ������ ������ � ����������� ��� */
    public int yIndTop;
    /** ������� � �������� �� 0.3 �� 1 {���� ����� �� �������� � = 1} */
    public double scale;
    
    @Override
    public String toString() {
        return "TrainThreadGeneralParams [codeStepO=" + codeStepO + ", codeStepE=" + codeStepE
                + ", captionSp=" + captionSp + ", captionSt=" + captionSt + ", time=" + time + ", xLeft="
                + xLeft + ", xRight=" + xRight + ", scale=" + scale + "]";
    }
 }
