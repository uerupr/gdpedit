package com.gmail.alaerof.dao.sql;

import com.gmail.alaerof.util.ConnectionDBFileHandlerUtil;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hsqldb.Server;

/**
 * @author Helen Yrofeeva
 */

public class ConnectionManagerHSQDB implements ConnectionManager{
    public static Logger logger = LogManager.getLogger(ConnectionManagerHSQDB.class);
    private static Server server;
    private Connection connection;
    private Properties poolInfo;

    private void startServer(Properties poolInfo) {
        if (server != null) {
            return;
        }
        String dblog = poolInfo.getProperty("dblog");   // logs/db.log
        String adress = poolInfo.getProperty("adress"); // localhost
        String dbname = poolInfo.getProperty("dbname"); // gdpdb
        String dbpath = poolInfo.getProperty("dbpath"); // file:database/hsqldb/
        // default = 9001
        int port = -1;
        try {
            port = Integer.parseInt(poolInfo.getProperty("port"));     // 9009
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }

        server = new Server();
        server.setTrace(true);
        try {
            server.setLogWriter(new PrintWriter(new FileWriter(dblog)));
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }

        server.setAddress(adress);
        server.setDatabaseName(0, dbname);
        server.setDatabasePath(0, dbpath + dbname);
        if (port > 0) {
            server.setPort(port);
        }
        server.start();
    }

    private void stopServer() {
        if (server != null) {
            if (server.getState() == 1)
                server.shutdown();
        }
    }

    public ConnectionManagerHSQDB(String base) throws PoolManagerException {
        poolInfo = ConnectionDBFileHandlerUtil.loadPoolInfo(base);
        if (poolInfo != null) {
            startServer(poolInfo);
            buildConnection();
        } else {
            throw new PoolManagerException(PoolManagerException.CONFIG_ERR);
        }
    }

    private void buildConnection() throws PoolManagerException {
        String adress = poolInfo.getProperty("adress"); // localhost
        String dbname = poolInfo.getProperty("dbname"); // gdpdb
        String port = poolInfo.getProperty("port");     // 9009

        String driver = poolInfo.getProperty("driver"); // org.hsqldb.jdbc.JDBCDriver
        // jdbc:hsqldb:hsql://localhost:9009/gdpdb
        String url = poolInfo.getProperty("url");       // jdbc:hsqldb:hsql://
        String startprop = poolInfo.getProperty("startprop");
        url = url+ adress;
        if (port != null && !port.isEmpty()) {
            url = url + ":" + port;
        }
        url = url + "/" + dbname + ";" + startprop;

        String charSet = poolInfo.getProperty("charSet"); // UTF-8
        String user = poolInfo.getProperty("user");       // SA
        String pass = poolInfo.getProperty("pass");       //
        Properties connInfo = new Properties();
        connInfo.put("user", user);
        connInfo.put("pass", pass);
        if (charSet != null) {
            connInfo.put("charSet", charSet);
        }
        logger.debug(driver);
        logger.debug(url);
        try {
            connection = DriverManager.getConnection(url, connInfo);
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e);
            throw new PoolManagerException(PoolManagerException.CONNECTION_ERR + e.toString() + " url=" + url, e);
        }
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void closeConnection() {
        try {
            connection.close();
            stopServer();
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Override
    public String getPath(){
        return poolInfo.getProperty("dbpath");
    }

    @Override
    public boolean isHSQLDB() {
        return true;
    }

}
