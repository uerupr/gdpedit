package com.gmail.alaerof.dao.dto.energy;

public class ExpenseRateEnergy {
    private EnergyType energyType;
    private float expRateEnValue;

    public ExpenseRateEnergy(EnergyType energyType, float expRateEnValue) {
        this.energyType = energyType;
        this.expRateEnValue = expRateEnValue;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public Float getExpRateEnValue() {
        return expRateEnValue;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }

    public void setExpRateEnValue(Float expRateEnValue) {
        this.expRateEnValue = expRateEnValue;
    }
}
