package com.gmail.alaerof.dao.dto;

import com.gmail.alaerof.dao.dto.gdp.TrainType;
import java.awt.Image;

/**
 * �����������/����������� ��������
 */
public class IntGR {
    /** ������������ ��������, ���� ���-�� ����� 0 */
    public static int MAX_INTGR = 10;
    /** ������ ����������� ���������� */
    public enum IntGRTitle{
        Cross,
        NonSimultaneousOppositeDirection,
        PassingArrive,
        NonStopCross,
        PassingDeparture,
        NonSimultaneousArrivePassingDeparture,
        NonSimultaneousDeparturePassingArrive;

        public String getString(){
            switch (this) {
                case Cross:
                    return "���������";
                case NonSimultaneousOppositeDirection:
                    return "���������������� �������� ������� ��������������� �����������";
                case PassingArrive:
                    return "��������� ��������";
                case NonStopCross:
                    return "���������������� ���������";
                case PassingDeparture:
                    return "��������� �����������";
                case NonSimultaneousArrivePassingDeparture:
                    return "���������������� �������� � ��������� �����������";
                case NonSimultaneousDeparturePassingArrive:
                    return "���������������� ����������� � ��������� ��������";
                default:
                    return null;
            }
        }

        public static IntGRTitle getValue(String name) {
            switch (name) {
                case "���������":
                    return Cross;
                case "���������������� �������� ������� ��������������� �����������":
                    return NonSimultaneousOppositeDirection;
                case "��������� ��������":
                    return PassingArrive;
                case "���������������� ���������":
                    return NonStopCross;
                case "��������� �����������":
                    return PassingDeparture;
                case "���������������� �������� � ��������� �����������":
                    return NonSimultaneousArrivePassingDeparture;
                case "���������������� ����������� � ��������� ��������":
                    return NonSimultaneousDeparturePassingArrive;
                default:
                    throw new IllegalArgumentException("name: " + name);
            }
        }
    }

    /***/
    private IntGRTitle intGRTitle;

    /** ID ���������*/
    private int idIntGR;
    /** �������� ��������� */
    private String name;
    /** ������ ����� */
    private TrainType firstType;
    /** ������ ����� */
    private TrainType secondType;
    /** ������ � ���������� */
    private boolean firstStop;
    /** ������ � ���������� */
    private boolean secondStop;
    /** �������� ��-��������� ��� ���� ������� */
    private int defaultValue;
    /** �������� ��������� */
    private Image image;
    /*
SwingFXUtils
public static WritableImage toFXImage(java.awt.image.BufferedImage bimg, WritableImage wimg)
bimg - the BufferedImage object to be converted
wimg - an optional WritableImage object that can be used to store the returned pixel data
    */

    public int getIdIntGR() {
        return idIntGR;
    }

    public void setIdIntGR(int idIntGR) {
        this.idIntGR = idIntGR;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
        intGRTitle = IntGRTitle.getValue(this.name);
    }

    public IntGRTitle getIntGRTitle() {
        return intGRTitle;
    }

    public TrainType getFirstType() {
        return firstType;
    }

    public void setFirstType(TrainType firstType) {
        this.firstType = firstType;
    }

    public TrainType getSecondType() {
        return secondType;
    }

    public void setSecondType(TrainType secondType) {
        this.secondType = secondType;
    }

    public boolean isFirstStop() {
        return firstStop;
    }

    public void setFirstStop(boolean firstStop) {
        this.firstStop = firstStop;
    }

    public boolean isSecondStop() {
        return secondStop;
    }

    public void setSecondStop(boolean secondStop) {
        this.secondStop = secondStop;
    }

    public int getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(int defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "IntGR{" +
                "idIntGR=" + idIntGR +
                ", name='" + name + '\'' +
                ", firstType=" + firstType +
                ", secondType=" + secondType +
                ", firstStop=" + firstStop +
                ", secondStop=" + secondStop +
                ", defaultValue=" + defaultValue +
                '}';
    }
}
