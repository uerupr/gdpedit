package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.List;

import com.gmail.alaerof.dao.dto.VariantCalc;
import com.gmail.alaerof.dao.dto.VariantCalcTime;

public interface IVariantCalcDAO {
    /** ���������� �������� ������� �������� ��� ������� */
    List<VariantCalc> getListVarCalc(int idDistr) throws DataManageException;

    /** ���������� ������� �� �������� ��� �������� ������� �������� */
    List<VariantCalcTime> getListVarCalcTime(int idVarCalc) throws DataManageException;

    /**
     * ������� ������� ������� �������� �� �� �������
     * @param idVarCalc
     */
    void deleteVariantCalc(int idVarCalc) throws DataManageException;

}
