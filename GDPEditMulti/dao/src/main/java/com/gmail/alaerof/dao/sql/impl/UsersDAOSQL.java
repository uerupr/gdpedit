package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.gdp.GDParm;
import com.gmail.alaerof.dao.dto.gdp.User;
import com.gmail.alaerof.dao.interfacedao.IUsersDAO;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersDAOSQL implements IUsersDAO {
    protected static Logger logger = LogManager.getLogger(GDParmDAOSQL.class);
    private final ConnectionManager cm;
    private static final String SQL_SELECT = "SELECT * FROM Users WHERE userLogin=?";

    public UsersDAOSQL(ConnectionManager cm) {
        this.cm = cm;
    }

    public User getUser(String userLogin) throws PoolManagerException {
        User user = new User();
        try (PreparedStatement ps = cm.getConnection().prepareStatement(SQL_SELECT)) {
            ps.setString(1, userLogin);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    user.setUserID(rs.getLong("userID"));
                    user.setUserLogin(rs.getString("userLogin").trim());
                    user.setName(rs.getString("userName").trim());
                    user.setUserPass(rs.getString("userPass").trim());
                }
            }
        } catch (SQLException e) {
            logger.error("error SQL select Users");
        }
        return user;
    }

}
