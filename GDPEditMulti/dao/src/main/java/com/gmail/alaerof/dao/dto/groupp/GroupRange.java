package com.gmail.alaerof.dao.dto.groupp;

import java.util.List; /**
 * ������ - �������� ������� ������
 */
public class GroupRange implements Comparable<GroupRange>{
    private int idRange = -1;
    private int idGroup;
    private int minCode;
    private int maxCode;

    public int getIdRange() {
        return idRange;
    }

    public void setIdRange(int idRange) {
        this.idRange = idRange;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public int getMinCode() {
        return minCode;
    }

    public void setMinCode(int minCode) {
        this.minCode = minCode;
    }

    public int getMaxCode() {
        return maxCode;
    }

    public void setMaxCode(int maxCode) {
        this.maxCode = maxCode;
    }

    @Override
    public int compareTo(GroupRange o) {
        return this.minCode - o.minCode;
    }

    @Override
    public String toString() {
        return "GroupRange{" +
                "idRange=" + idRange +
                ", idGroup=" + idGroup +
                ", minCode=" + minCode +
                ", maxCode=" + maxCode +
                '}';
    }
}
