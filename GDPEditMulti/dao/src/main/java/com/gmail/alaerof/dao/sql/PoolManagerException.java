package com.gmail.alaerof.dao.sql;
/**
 * @author Helen Yrofeeva
 */
public class PoolManagerException extends Exception {
	private static final long serialVersionUID = 1L;
	public static final String CONFIG_ERR = "Error reading database connection settings ";
    public static final String CONNECTION_ERR = "Error in connection to the database ";

	public PoolManagerException(String mess) {
		super(mess);
	}

	public PoolManagerException(String mess, Throwable t) {
		super(mess, t);
	}
}
