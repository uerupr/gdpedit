package com.gmail.alaerof.application;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.factory.IGDPDAOFactory;
import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.sql.GDPDAOFactorySQL;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

public class TestDAO {
    // public static IGDPDAOFactory fact;
    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }

    private static void testCommon() throws ClassNotFoundException, SQLException {
        // ALAEROF/SQLEXPRESS17
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        String connectionUrl = "jdbc:sqlserver://ALAEROF\\SQLEXPRESS;databaseName=GDP;user=sa;password=1;";
        Connection con = DriverManager.getConnection(connectionUrl);
        Statement st =  con.createStatement();
        ResultSet rs = st.executeQuery("SELECT Name FROM Distr");
        while (rs.next()){
            System.out.println(rs.getString("Name"));
        }
    }

    private static void testCategoryColor(){
        try {
            ICategoryDAO dao = GDPDAOFactorySQL.getInstance().getCategoryDAO();
            List<Category> categoryList = dao.getCategories();

        } catch (PoolManagerException e) {
            e.printStackTrace();
        } catch (DataManageException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //testCommon();
        testCategoryColor();
    }
}
