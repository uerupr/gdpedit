package com.gmail.alaerof.dao.dto;

public class Wind {
    private int idWind;
    private int idSpan;
    private String timeBegin; // T1
    private String timeEnd;   // T2
    /** ����������������� ����, ��� */
    private int tm;
    /** ����� �����, ��� ������� ���� (1,2) */
    private int lineCount;
    /** ������� ������������ ���� */
    private String cause;
    private int IDdistrW;
    private boolean lineO;
    private boolean lineE;
    private boolean LineOE;
    /** ������������ ���������� ���� */
    private int y0;

    public int getIdWind() {
        return idWind;
    }

    public void setIdWind(int idWind) {
        this.idWind = idWind;
    }

    public int getIdSpan() {
        return idSpan;
    }

    public void setIdSpan(int idSpan) {
        this.idSpan = idSpan;
    }

    public String getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(String timeBegin) {
        this.timeBegin = timeBegin;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public int getTm() {
        return tm;
    }

    public void setTm(int tm) {
        this.tm = tm;
    }

    public int getLineCount() {
        return lineCount;
    }

    public void setLineCount(int lineCount) {
        this.lineCount = lineCount;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public int getIDdistrW() {
        return IDdistrW;
    }

    public void setIDdistrW(int IDdistrW) {
        this.IDdistrW = IDdistrW;
    }

    public boolean isLineO() {
        return lineO;
    }

    public void setLineO(boolean lineO) {
        this.lineO = lineO;
    }

    public boolean isLineE() {
        return lineE;
    }

    public void setLineE(boolean lineE) {
        this.lineE = lineE;
    }

    public boolean isLineOE() {
        return LineOE;
    }

    public void setLineOE(boolean lineOE) {
        LineOE = lineOE;
    }

    public int getY0() {
        return y0;
    }

    public void setY0(int y0) {
        this.y0 = y0;
    }
}
