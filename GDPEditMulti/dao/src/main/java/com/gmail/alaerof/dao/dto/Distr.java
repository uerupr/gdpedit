package com.gmail.alaerof.dao.dto;

/**
 * �.�. �������
 * @author Helen Yrofeeva
 * 
 */
public class Distr {
    private int idDistr; // IDdistr
    private int idDir; // IDdir
    private String indx = ""; // INDX
    private String name = ""; // name
    private int main;

    public int getIdDistr() {
        return idDistr;
    }

    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }

    public int getIdDir() {
        return idDir;
    }

    public void setIdDir(int idDir) {
        this.idDir = idDir;
    }

    public String getIndx() {
        return indx;
    }

    public void setIndx(String indx) {
        if (indx != null) {
            this.indx = indx;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public int getMain() {
        return main;
    }

    public void setMain(int main) {
        this.main = main;
    }

    @Override
    public String toString() {
        return "Distr [idDistr=" + idDistr + ", idDir=" + idDir + ", indx=" + indx + ", name=" + name + "]";
    }

}
