package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.energy.Energy;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.energy.EnergyType;
import com.gmail.alaerof.dao.dto.energy.ExpenseRate;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateEnergy;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateNormative;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.energy.SpanEnergy;
import com.gmail.alaerof.dao.dto.energy.SpanEnergyTract;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.ISpanEnergyDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


import static org.apache.commons.lang.StringUtils.trim;

/**
 *
 */
public class SpanEnergyDAOSQL implements ISpanEnergyDAO{
    protected static Logger logger = LogManager.getLogger(SpanEnergyDAOSQL.class);
    private final ConnectionManager cmServer;
    private final ConnectionManager cmAccess;

    public SpanEnergyDAOSQL(ConnectionManager cmServer, ConnectionManager cmAccess) {
        this.cmServer = cmServer;
        this.cmAccess = cmAccess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpanEnergy getSpanEnergy(int idScale, List<LocomotiveType> lt) throws DataManageException {
        SpanEnergy stm = new SpanEnergy();
        stm.setIdScale(idScale);
        Map<LocomotiveType, Energy> mapTmO = stm.getLocEnergyO();
        Map<LocomotiveType, Energy> mapTmE = stm.getLocEnergyE();
        try {
            Connection cn = cmAccess.getConnection();
            try (Statement st = cn.createStatement()) {
                for (LocomotiveType l : lt) {
                    String sql = "SELECT IDscale, IDltype, EnMove, EnDown, EnUp, oe FROM SpanEnergy WHERE IDscale = "
                            + idScale + " AND IDltype = " + l.getIdLType() + " AND oe = '�����'";
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            Energy en = new Energy();
                            fillItem(en, rs);
                            mapTmO.put(l, en);
                        }
                    } catch (DataManageException e) {
                        throw new DataManageException(" IDscale = " + idScale + " loc = " + l, e);
                    }

                    sql = "SELECT IDscale, IDltype, EnMove, EnDown, EnUp, oe FROM SpanEnergy WHERE IDscale = "
                            + idScale + " AND IDltype = " + l.getIdLType() + " AND oe = '���'";
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            Energy en = new Energy();
                            fillItem(en, rs);
                            mapTmE.put(l, en);
                        }
                    } catch (DataManageException e) {
                        throw new DataManageException(" IDscale = " + idScale + " loc = " + l, e);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return stm;
    }

    /**
     * @param en Energy
     * @param rs ResultSet
     */
    void fillItem(Energy en, ResultSet rs) throws DataManageException {
        try {
            en.setEnMove(rs.getFloat("EnMove"));
            en.setEnDown(rs.getFloat("EnDown"));
            en.setEnUp(rs.getFloat("EnUp"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException("SpanEnergy " + en, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveSpanEnergy(int idScale, SpanEnergy spanEnergy) throws DataManageException {
        String sqlDel = "DELETE FROM SpanEnergy WHERE IDscale = " + idScale;
        String sql = "INSERT INTO SpanEnergy (IDscale, IDltype, EnMove, EnDown, EnUp, oe) VALUES (?, ?, ?, ?, ?, ?)";

        try {
            Connection cn = cmAccess.getConnection();
            try (Statement st = cn.createStatement();
                 PreparedStatement pst = cn.prepareStatement(sql);) {
                st.executeUpdate(sqlDel);
                saveSpanEnergy(idScale, spanEnergy.getLocEnergyO(), OE.odd, pst);
                saveSpanEnergy(idScale, spanEnergy.getLocEnergyE(), OE.even, pst);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * @param idScale IDscale
     * @param locEnergy LocomotiveType- Energy
     * @param oe ��������
     * @param pst PreparedStatement
     */
    private void saveSpanEnergy(int idScale, Map<LocomotiveType, Energy> locEnergy, OE oe, PreparedStatement pst) throws SQLException {
        for (Map.Entry<LocomotiveType, Energy> ent : locEnergy.entrySet()) {
            LocomotiveType lt = ent.getKey();
            Energy energy = ent.getValue();
            pst.setInt(1, idScale);
            pst.setInt(2, lt.getIdLType());
            pst.setFloat(3, energy.getEnMove());
            pst.setFloat(4,energy.getEnDown());
            pst.setFloat(5, energy.getEnUp());
            pst.setString(6, oe.getString());
            pst.executeUpdate();
        }
    }

    @Override
    public void saveSpanEnergyTract(SpanEnergyTract spanEnergyTract) throws DataManageException {
        int idScale = spanEnergyTract.getIdScale();
        int idLType = spanEnergyTract.getLocomotiveType().getIdLType();
        OE oe = spanEnergyTract.getOe();
        Energy energy = spanEnergyTract.getEnergy();
        String sqlDel = "DELETE FROM SpanEnergy WHERE IDscale = " + idScale + " AND IDltype = " + idLType + " AND oe = '" + oe.getString() + "'";
        String sql = "INSERT INTO SpanEnergy (IDscale, IDltype, EnMove, EnDown, EnUp, oe) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            Connection cn = cmAccess.getConnection();
            try (Statement st = cn.createStatement();
                 PreparedStatement pst = cn.prepareStatement(sql);) {
                st.executeUpdate(sqlDel);
                pst.setInt(1, idScale);
                pst.setInt(2, idLType);
                pst.setFloat(3, energy.getEnMove());
                pst.setFloat(4, energy.getEnDown());
                pst.setFloat(5, energy.getEnUp());
                pst.setString(6, oe.getString());
                pst.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    @Override
    public void saveSpanEnergyTractList(List<SpanEnergyTract> spanEnergyTractList) throws DataManageException {
        Set<Integer> scaleIDSet = getAllScale(spanEnergyTractList);
        /**/
        String sqlDel = "DELETE FROM SpanEnergy WHERE IDscale = ? ";
        String sql = "INSERT INTO SpanEnergy (IDscale, IDltype, EnMove, EnDown, EnUp, oe) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            Connection cn = cmAccess.getConnection();
            try (PreparedStatement pst = cn.prepareStatement(sql);
                 PreparedStatement pstDel = cn.prepareStatement(sqlDel)) {
                for (Integer scaleID : scaleIDSet) {
                    pstDel.setInt(1, scaleID);
                    pstDel.executeUpdate();
                }
                for (int i = 0; i < spanEnergyTractList.size(); i++){
                    SpanEnergyTract spanEnergyTract = spanEnergyTractList.get(i);
                    int idScale = spanEnergyTract.getIdScale();
                    int idLType = spanEnergyTract.getLocomotiveType().getIdLType();
                    OE oe = spanEnergyTract.getOe();
                    Energy energy = spanEnergyTract.getEnergy();
                    pst.setInt(1, idScale);
                    pst.setInt(2, idLType);
                    pst.setFloat(3, energy.getEnMove());
                    pst.setFloat(4, energy.getEnDown());
                    pst.setFloat(5, energy.getEnUp());
                    pst.setString(6, oe.getString());
                    pst.executeUpdate();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private Set<Integer> getAllScale(List<SpanEnergyTract> list) {
        Set<Integer> set = new HashSet<>();
        for(SpanEnergyTract time: list){
            set.add(time.getIdScale());
        }
        return set;
    }

    @Override
    public void saveExpenseRateEnergy(List<ExpenseRateEnergy> expenseRateEnergyList) throws DataManageException {
        String sqlDel = "DELETE FROM ExpenseRateEnergy ";
        String sql = "INSERT INTO ExpenseRateEnergy (EnergyType, ExpRateEnValue) VALUES (?, ?)";
        try {
            Connection cn = cmServer.getConnection();
            try (PreparedStatement pst = cn.prepareStatement(sql);
                 PreparedStatement pstDel = cn.prepareStatement(sqlDel)) {
                pstDel.executeUpdate();
                for (int i = 0; i < expenseRateEnergyList.size(); i++) {
                    pst.setString(1, trim(expenseRateEnergyList.get(i).getEnergyType().getString()));
                    pst.setFloat(2, expenseRateEnergyList.get(i).getExpRateEnValue());
                    pst.executeUpdate();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    @Override
    public List<ExpenseRateEnergy> getExpenseRateEnergy() throws DataManageException {
        List<ExpenseRateEnergy> expenseRateEnergyList = new ArrayList<>();
        String sql = "SELECT EnergyType, ExpRateEnValue FROM ExpenseRateEnergy";
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement()) {
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    String ss = trim(rs.getString("EnergyType"));
                    ExpenseRateEnergy expenseRateEnergy = new ExpenseRateEnergy(EnergyType.getType(ss),rs.getFloat("ExpRateEnValue"));
                    expenseRateEnergyList.add(expenseRateEnergy);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return expenseRateEnergyList;
    }

    @Override
    public void saveExpenseRate(List<ExpenseRate> expenseRateList) throws DataManageException {
        String sqlDel = "DELETE FROM ExpenseRate ";
        String sql = "INSERT INTO ExpenseRate (MoveExpRate, StopExpRate, TrainType, EnergyType) VALUES (?, ?, ?, ?)";
        try {
            Connection cn = cmServer.getConnection();
            try (PreparedStatement pst = cn.prepareStatement(sql);
                 PreparedStatement pstDel = cn.prepareStatement(sqlDel)) {
                pstDel.executeUpdate();
                for (int i = 0; i < expenseRateList.size(); i++){
                    pst.setFloat(1, (float) expenseRateList.get(i).getMoveExpRate());
                    pst.setFloat(2, (float) expenseRateList.get(i).getStopExpRate());
                    pst.setString(3, trim(expenseRateList.get(i).getTrainType().getString()));
                    pst.setString(4, trim(expenseRateList.get(i).getEnergyType().getString()));
                    pst.executeUpdate();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    @Override
    public List<ExpenseRate> getExpenseRate() throws DataManageException {
        List<ExpenseRate> expenseRateList = new ArrayList<>();
        String sql = "SELECT MoveExpRate, StopExpRate, TrainType, EnergyType FROM ExpenseRate";
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement()) {
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    float move = rs.getFloat("MoveExpRate");
                    float stop = rs.getFloat("StopExpRate");
                    String tr = trim(rs.getString("TrainType"));
                    String en = trim(rs.getString("EnergyType"));
                    ExpenseRate expenseRate = new ExpenseRate(move, stop, TrainType.getType(tr), EnergyType.getType(en));
                    expenseRateList.add(expenseRate);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return expenseRateList;
    }

    @Override
    public void saveExpenseRateNormative(List<ExpenseRateNormative> expenseRateNormativeList) throws DataManageException {
        String sqlDel = "DELETE FROM ExpenseRateNormative ";
        String sql = "INSERT INTO ExpenseRateNormative (IDltype, EnergyType, DownUpExp) VALUES (?, ?, ?)";
        try {
            Connection cn = cmServer.getConnection();
            try (PreparedStatement pst = cn.prepareStatement(sql);
                 PreparedStatement pstDel = cn.prepareStatement(sqlDel)) {
                pstDel.executeUpdate();
                for (int i = 0; i < expenseRateNormativeList.size(); i++){
                    pst.setInt(1, expenseRateNormativeList.get(i).getIdLType());
                    pst.setString(2, trim(expenseRateNormativeList.get(i).getEnergyType().getString()));
                    pst.setFloat(3, (float) expenseRateNormativeList.get(i).getDownUpExp());
                    pst.executeUpdate();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    @Override
    public List<ExpenseRateNormative> getExpenseRateNormative() throws DataManageException {
        List<ExpenseRateNormative> expenseRateNormativeList = new ArrayList<>();
        String sql = "SELECT IDltype, EnergyType, DownUpExp FROM ExpenseRateNormative";
        try {
            Connection cn = cmServer.getConnection();
            try (Statement st = cn.createStatement()) {
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    int id = rs.getInt("IDltype");
                    String en = trim(rs.getString("EnergyType"));
                    float du = rs.getFloat("DownUpExp");
                    ExpenseRateNormative expenseRateNormative = new ExpenseRateNormative(id, EnergyType.getType(en), du);
                    expenseRateNormativeList.add(expenseRateNormative);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return expenseRateNormativeList;
    }
}
