package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLQueryGDParm {
  protected static Logger logger = LogManager.getLogger(GDParmDAOSQL.class);
  private final ConnectionManager cmSQL, cmAccess;
  private static final String SQL_SELECT = "SELECT GDParmID FROM GDParm " +
          "WHERE  (IDdistr = ?) and (Arch = 0) and (dateBegin = convert(datetime,?,121)) and (dateEnd = convert(datetime,?,121))";
  private static final String SQL_DELETE_DISTR_TRAIN = "DELETE FROM xDistrTrain WHERE GDParmID = ?";
  private static final String SQL_DELETE_WIND = "DELETE FROM xWind WHERE GDParmID = ?";
  private static final String SQL_DELETE_SPAN_TIME = "DELETE FROM xSpanTime WHERE GDParmID = ?";
  private static final String SQL_DELETE_GROUP_VAL = "DELETE FROM xGroupVal WHERE GDParmID = ?";
  private static final String SQL_DELETE_GROUP_VAL_DIR = "DELETE FROM xGroupValDir WHERE GDParmID = ?";
  private static final String SQL_DELETE_STATION_DISTR = "DELETE FROM StationDistr";
  private static final String SQL_UPDATE_STATION_DISTR = "UPDATE StationDistr SET IDdistr = ?";
  private static final String SQL_SELECT_STATION = "SELECT IDst FROM Station " +
          "WHERE Station.IDst IN (" +
          "SELECT Span.IDst FROM Span WHERE IDspan IN (SELECT IDspan FROM Scale WHERE IDdistr = ?) " +
          "UNION ALL " +
          "SELECT Span.IDst_e FROM Span WHERE IDspan IN (SELECT IDspan FROM Scale WHERE IDdistr = ?) " +
          ")";
  private static final String SQL_INSERT_STATION_DISTR = "INSERT INTO StationDistr (IDst) " +
          " SELECT Station.IDst FROM Station " +
          " WHERE Station.IDst IN  (?)";
  private static final String SQL_SELECT_TRAIN_DISTR = "SELECT Distr_Train.*, Train.* " +
          " FROM Train INNER JOIN Distr_Train ON Train.IDtrain = Distr_Train.IDtrain " +
          " WHERE Distr_Train.IDdistr ";

  private static final String SQL_INSERT_TRAIN_XDISTR = "INSERT INTO xDistrTrain " +
          "(GDParmID, IDcat, code, oe, typeTR, mass, len, occupyOff, " +
          "Tu, Tt, Vu, Vt, L, el, ps, " +
          "hidden, ShowCodeI, colorTR, AInscr, LineStyle, widthTR, " +
          "  tOff,  tOn, tOn_e,  tOff_e) " +
          "VALUES ";

  public SQLQueryGDParm(ConnectionManager server, ConnectionManager access) {
    this.cmSQL = server;
    this.cmAccess = access;
  }

  public boolean selectGDParm(int idDistr, String dateBegin, String dateEnd) throws PoolManagerException {
//    GDPDAOFactorySQL instance = GDPDAOFactorySQL.getInstance(GDPDAOFactorySQL.SERVER);
//    instance.getGDParmDAO();
    try (PreparedStatement ps = cmSQL.getConnection().prepareStatement(SQL_SELECT)) {
      ps.setLong(1, idDistr);
      ps.setString(2, dateBegin);
      ps.setString(3, dateEnd);
      try (ResultSet rs = ps.executeQuery()) {
        if (rs.next()) {
          return true;
        }
      }
    } catch (SQLException e) {
      logger.error(e.toString(), e);
    }
    return false;
  }

  private int deleteSQL(int id, String sql) throws PoolManagerException {
    try (PreparedStatement ps = cmSQL.getConnection().prepareStatement(sql)) {
      ps.setInt(1, id);
      return ps.executeUpdate();
    } catch (SQLException e) {
      logger.error(e.toString(), e);
    }
    return 0;
  }

  public int deleteXDistrTrain(int idGDParm) throws PoolManagerException {
    return deleteSQL(idGDParm, SQL_DELETE_DISTR_TRAIN);
  }

  public int deleteXWind(int idGDParm) throws PoolManagerException {
    return deleteSQL(idGDParm, SQL_DELETE_WIND);
  }

  public int deleteXxSpanTime(int idGDParm) throws PoolManagerException {
    return deleteSQL(idGDParm, SQL_DELETE_SPAN_TIME);
  }

  public int deleteXGroupVal(int idGDParm) throws PoolManagerException {
    return deleteSQL(idGDParm, SQL_DELETE_GROUP_VAL);
  }

  public int deleteXGroupValDir(int idGDParm) throws PoolManagerException {
    return deleteSQL(idGDParm, SQL_DELETE_GROUP_VAL_DIR);
  }

  public int deleteStationDistr() throws PoolManagerException {
    try (PreparedStatement ps = cmAccess.getConnection().prepareStatement(SQL_DELETE_STATION_DISTR)) {
      return ps.executeUpdate();
    } catch (SQLException e) {
      logger.error(e.toString(), e);
    }
    return 0;
  }

  public int insertStationDistr(int idDistr) throws PoolManagerException {
    String sqlScale = SQL_SELECT_STATION;
    String idSpanScale = "";
    try (PreparedStatement ps = cmSQL.getConnection().prepareStatement(sqlScale)) {
      ps.setInt(1, idDistr);
      ps.setInt(2, idDistr);
      try (ResultSet rss = ps.executeQuery()) {
        while (rss.next()) {
          idSpanScale = idSpanScale + rss.getString("IDst") + ",";
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    idSpanScale = idSpanScale + "0";

    try (PreparedStatement ps = cmAccess.getConnection().prepareStatement(SQL_INSERT_STATION_DISTR)) {
      ps.setString(1, idSpanScale);
      return ps.executeUpdate();
    } catch (SQLException e) {
      logger.error(e.toString(), e);
    }
    return 0;
  }

  public int updateStationDistr(int idDistr) throws PoolManagerException {
    try (PreparedStatement ps = cmAccess.getConnection().prepareStatement(SQL_UPDATE_STATION_DISTR)) {
      ps.setInt(1, idDistr);
      return ps.executeUpdate();
    } catch (SQLException e) {
      logger.error(e.toString(), e);
    }
    return 0;
  }

  public boolean saveGDP(int idDistr, int idGDParm) throws PoolManagerException {
    String sqlSelectTrain = SQL_SELECT_TRAIN_DISTR + " = " + idDistr;
    String cTR = "";
    int oldID = 0;

    String sqlInsertTrain;
    try (PreparedStatement ps1 = cmAccess.getConnection().prepareStatement(sqlSelectTrain)) {
      try (ResultSet rs = ps1.executeQuery()) {
        while (rs.next()) {
          cTR = rs.getString("code");
          oldID = rs.getInt("IDTrain");
          boolean el = rs.getBoolean("el");
          int elInt = (el) ? 0 : -1;
          boolean hidden = rs.getBoolean("hidden");
          int hiddenInt = (hidden) ? 0 : -1;
          boolean showCodeI = rs.getBoolean("ShowCodeI");
          int showCodeIInt = (showCodeI) ? 0 : -1;

          sqlInsertTrain = SQL_INSERT_TRAIN_XDISTR + "("
                  + idGDParm + ","
                  + getNormalValue(rs.getString("IDcat")) + ","
                  + getNormalValue(rs.getString("code")) + ",'"
                  + rs.getString("oe") + "','"
                  + rs.getString("type") + "',"
                  + getNormalValue(rs.getString("mass")) + ","
                  + getNormalValue(rs.getString("len")) + ","
                  + getNormalDate(rs.getString("occupyOff")) + ","
                  + getNormalValue(rs.getString("Tu")) + ","
                  + getNormalValue(rs.getString("Tt")) + ","
                  + getNormalValue(rs.getString("Vu")) + ","
                  + getNormalValue(rs.getString("Vt")) + ","
                  + getNormalValue(rs.getString("L")) + ","
                  + elInt + ","
                  + getNormalValue(rs.getString("ps")) + ","
                  + hiddenInt + ","
                  + showCodeIInt + ","
                  + getNormalValue(rs.getString("colorTR")) + ",'"
                  + rs.getString("AInscr") + "',"
                  + getNormalValue(rs.getString("LineStyle")) + ","
                  + getNormalValue(rs.getString("widthTR")) + ","
                  + getNormalDate(rs.getString("tOff")) + ","
                  + getNormalDate(rs.getString("tOn")) + ","
                  + getNormalDate(rs.getString("tOn_e")) + ","
                  + getNormalDate(rs.getString("tOff_e")) + ")";

          try (PreparedStatement ps2 = this.cmSQL.getConnection().prepareStatement(sqlInsertTrain)) {
            ps2.executeUpdate();
          }

          String sql3 = "SELECT MAX(IDtrain) as MaxID FROM xDistrTrain";
          int newID = 0;
          try (PreparedStatement ps3 = this.cmSQL.getConnection().prepareStatement(sql3)) {
            try (ResultSet rs3 = ps3.executeQuery()) {
              if (rs3.next()) {
                newID = rs3.getInt("MaxID");
              }
            }
          }

          if (newID > 0) {

            String sqlSelect = "SELECT IDlineSt, IDst, occupyOn, occupyOff, TstopPL, Tstop, TtexStop, sk_ps, overTime " +
                    "FROM LineSt_Train WHERE IDtrain = " + oldID + " AND IDdistr = " + idDistr;

            try (PreparedStatement ps = cmAccess.getConnection().prepareStatement(sqlSelect)) {
              try (ResultSet rss = ps.executeQuery()) {
                while (rss.next()) {
                  String sqlInsert = "INSERT INTO xLineSt_Train " +
                          " (IDlineSt, IDst, IDtrain, occupyOn, occupyOff, TstopPL, Tstop, TtexStop, sk_ps, overTime )" +
                          "VALUES (" +
                          getNormalValue(rss.getString("IDlineSt")) + "," +
                          getNormalValue(rss.getString("IDst")) + "," +
                          newID + "," +
                          getNormalDate(rss.getString("occupyOn")) + "," +
                          getNormalDate(rss.getString("occupyOff")) + "," +
                          getNormalValue(rss.getString("TstopPL")) + "," +
                          getNormalValue(rss.getString("Tstop")) + "," +
                          getNormalValue(rss.getString("TtexStop")) + "," +
                          getNormalValue(rss.getString("sk_ps")) + "," +
                          getNormalValue(rss.getString("overTime")) + ")";
                  try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                    pss.executeUpdate();
                  }

                  //rs.getInt("MaxID");
                }
              }
            } catch (SQLException e) {
              logger.error(e.toString(), e);
            }

            sqlSelect = "SELECT IDspanst, Tstop, occupyOn, occupyOff, sk_ps " +
                    "FROM TrainStop WHERE IDtrain = " + oldID + " AND IDdistr = " + idDistr;

            try (PreparedStatement ps = cmAccess.getConnection().prepareStatement(sqlSelect)) {
              try (ResultSet rss = ps.executeQuery()) {
                while (rss.next()) {
                  String sqlInsert = "INSERT INTO xTrainStop " +
                          " (IDtrain, IDspanst, Tstop, occupyOn, occupyOff, sk_ps )" +
                          "VALUES (" +
                          newID + "," +
                          getNormalValue(rss.getString("IDspanst")) + "," +
                          getNormalValue(rss.getString("Tstop")) + "," +
                          getNormalDate(rss.getString("occupyOn")) + "," +
                          getNormalDate(rss.getString("occupyOff")) + "," +
                          getNormalValue(rss.getString("sk_ps")) + ")";
                  System.out.println(sqlInsert);
                  try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                    pss.executeUpdate();
                  }

                  //rs.getInt("MaxID");
                }
              }
            } catch (SQLException e) {
              logger.error(e.toString(), e);
            }

            sqlSelect = "SELECT IDspan, occupyOn, occupyOff, NextDay, Tmove " +
                    "FROM SpTOcc WHERE IDtrain = " + oldID + " AND IDdistr = " + idDistr;

            try (PreparedStatement ps = cmAccess.getConnection().prepareStatement(sqlSelect)) {
              try (ResultSet rss = ps.executeQuery()) {
                while (rss.next()) {
                  String sqlInsert = "INSERT INTO xSpTOcc " +
                          " (IDspan, IDtrain, occupyOn, occupyOff, NextDay, Tmove)" +
                          "VALUES (" +
                          getNormalValue(rss.getString("IDspan")) + "," +
                          newID + "," +
                          getNormalDate(rss.getString("occupyOn")) + "," +
                          getNormalDate(rss.getString("occupyOff")) + "," +
                          getNormalValue(rss.getString("NextDay")) + "," +
                          getNormalValue(rss.getString("Tmove")) + ")";
                  try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                    pss.executeUpdate();
                  }

                  //rs.getInt("MaxID");
                }
              }
            } catch (SQLException e) {
              logger.error(e.toString(), e);
            }


            sqlSelect =
                    "SELECT IDdir, Tu, Tt, L " +
                            "FROM DirTrain WHERE IDtrain = " + oldID + " AND IDdistr = " + idDistr;

            try (PreparedStatement ps = cmAccess.getConnection().prepareStatement(sqlSelect)) {
              try (ResultSet rss = ps.executeQuery()) {
                while (rss.next()) {
                  String sqlInsert = "INSERT INTO xDirTrain " +
                          " (IDdir, IDtrain, Tu, Tt, L)" +
                          "VALUES (" +
                          getNormalValue(rss.getString("IDdir")) + "," +
                          newID + "," +
                          getNormalValue(rss.getString("Tu")) + "," +
                          getNormalValue(rss.getString("Tt")) + "," +
                          getNormalValue(rss.getString("L")) + ")";
                  try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                    pss.executeUpdate();
                  }

                  //rs.getInt("MaxID");
                }
              }
            } catch (SQLException e) {
              logger.error(e.toString(), e);
            }


            sqlSelect =
                    "SELECT IDspan,  IDtrain, Tmove, Tdown, Tup, Tw " +
                            "FROM SpanTrain WHERE IDtrain IN (SELECT IDtrain FROM Distr_Train WHERE IDdistr = " + idDistr + ")";

            try (PreparedStatement ps = cmAccess.getConnection().prepareStatement(sqlSelect)) {
              try (ResultSet rss = ps.executeQuery()) {
                while (rss.next()) {
                  String sqlInsert = "INSERT INTO xSpanTrain " +
                          " (IDspan,  IDtrain, Tmove, Tdown, Tup, Tw)" +
                          "VALUES (" +
                          getNormalValue(rss.getString("IDspan")) + "," +
                          newID + "," +
                          getNormalValue(rss.getString("Tmove")) + "," +
                          getNormalValue(rss.getString("Tdown")) + "," +
                          getNormalValue(rss.getString("Tup")) + "," +
                          getNormalValue(rss.getString("Tw")) + ")";
                  try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                    pss.executeUpdate();
                  }

                  //rs.getInt("MaxID");
                }
              }
            } catch (SQLException e) {
              logger.error(e.toString(), e);
            }

          }  // if newID; !!!!!!!


          String sqlScale = "SELECT IDspan FROM Scale WHERE IDdistr = " + idDistr;
          String idSpanScale = "";
          try (PreparedStatement ps = cmSQL.getConnection().prepareStatement(sqlScale)) {
            try (ResultSet rss = ps.executeQuery()) {
              while (rss.next()) {
                idSpanScale = idSpanScale + rss.getString("IDspan") + ",";
              }
            }
          }
          idSpanScale = idSpanScale + "0";
          String sqlSelect =
                  "SELECT IDspan, type, Tmove, Tdown, Tup, oe, Tw " +
                          "FROM SpanTime WHERE IDspan IN (" + idSpanScale + ")";
          ConnectionManager cmA = cmAccess;
          try (PreparedStatement ps = cmA.getConnection().prepareStatement(sqlSelect)) {
            try (ResultSet rss = ps.executeQuery()) {
              while (rss.next()) {
                String sqlInsert = "INSERT INTO xSpanTime " +
                        " (IDspan, GDParmID, type, Tmove, Tdown, Tup, oe, Tw)" +
                        "VALUES (" +
                        getNormalValue(rss.getString("IDspan")) + "," +
                        idGDParm + "," +
                        getNormalValue(rss.getString("type")) + "," +
                        getNormalValue(rss.getString("Tmove")) + "," +
                        getNormalValue(rss.getString("Tdown")) + "," +
                        getNormalValue(rss.getString("Tup")) + "," +
                        getNormalValue(rss.getString("oe")) + "," +
                        getNormalValue(rss.getString("Tw")) + ")";
                try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                  pss.executeUpdate();
                }
              }
            }
          } catch (SQLException e) {
            logger.error(e.toString(), e);
          }

          sqlSelect =
                  "SELECT IDgroup, cnt_o, cnt_e, cnt_all, trkm_o, trkm_e, trkm_all, " +
                          " trht_o, trht_e, trht_all, trhu_o, trhu_e, trhu_all, Vt_o, Vt_e, Vt_all, " +
                          " Vu_o, Vu_e, Vu_all " +
                          "FROM GroupVal WHERE IDdistr = " + idDistr;
          try (PreparedStatement ps = cmA.getConnection().prepareStatement(sqlSelect)) {
            try (ResultSet rss = ps.executeQuery()) {
              while (rss.next()) {
                String sqlInsert = "INSERT INTO xGroupVal " +
                        "(GDParmID, IDgroup, cnt_o, cnt_e, cnt_all, trkm_o, trkm_e, trkm_all, " +
                        " trht_o, trht_e, trht_all, trhu_o, trhu_e, trhu_all, Vt_o, Vt_e, Vt_all, " +
                        " Vu_o, Vu_e, Vu_all) " +
                        "VALUES (" +
                        idGDParm + "," +
                        getNormalValue(rss.getString("IDgroup")) + "," +
                        getNormalValue(rss.getString("cnt_o")) + "," +
                        getNormalValue(rss.getString("cnt_e")) + "," +
                        getNormalValue(rss.getString("cnt_all")) + "," +
                        getNormalValue(rss.getString("trkm_o")) + "," +
                        getNormalValue(rss.getString("trkm_e")) + "," +
                        getNormalValue(rss.getString("trkm_all")) + "," +
                        getNormalValue(rss.getString("trht_o")) + "," +
                        getNormalValue(rss.getString("trht_e")) + "," +
                        getNormalValue(rss.getString("trht_all")) + "," +
                        getNormalValue(rss.getString("trhu_o")) + "," +
                        getNormalValue(rss.getString("trhu_e")) + "," +
                        getNormalValue(rss.getString("trhu_all")) + "," +
                        getNormalValue(rss.getString("Vt_o")) + "," +
                        getNormalValue(rss.getString("Vt_e")) + "," +
                        getNormalValue(rss.getString("Vt_all")) + "," +
                        getNormalValue(rss.getString("Vu_o")) + "," +
                        getNormalValue(rss.getString("Vu_e")) + "," +
                        getNormalValue(rss.getString("Vu_all")) + ")";
                try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                  pss.executeUpdate();
                }
              }
            }
          } catch (SQLException e) {
            logger.error(e.toString(), e);
          }

          sqlSelect =
                  "SELECT IDgroup, IDdir, cnt_o, cnt_e, trkm_o, trkm_e, trht_o, trht_e, trhu_o, trhu_e " +
                          "FROM GroupValDir WHERE IDdistr = " + idDistr;
          try (PreparedStatement ps = cmA.getConnection().prepareStatement(sqlSelect)) {
            try (ResultSet rss = ps.executeQuery()) {
              while (rss.next()) {
                String sqlInsert = "INSERT INTO xGroupValDir " +
                        "(GDParmID, IDgroup, IDdir, cnt_o, cnt_e, trkm_o, trkm_e, trht_o, trht_e, trhu_o, trhu_e) " +
                        "VALUES (" +
                        idGDParm + "," +
                        getNormalValue(rss.getString("IDgroup")) + "," +
                        getNormalValue(rss.getString("IDdir")) + "," +
                        getNormalValue(rss.getString("cnt_o")) + "," +
                        getNormalValue(rss.getString("cnt_e")) + "," +
                        getNormalValue(rss.getString("trkm_o")) + "," +
                        getNormalValue(rss.getString("trkm_e")) + "," +
                        getNormalValue(rss.getString("trht_o")) + "," +
                        getNormalValue(rss.getString("trht_e")) + "," +
                        getNormalValue(rss.getString("trhu_o")) + "," +
                        getNormalValue(rss.getString("trhu_e")) + ")";
                try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                  pss.executeUpdate();
                }
              }
            }
          } catch (SQLException e) {
            logger.error(e.toString(), e);
          }

          sqlSelect =
                  "SELECT IDspan, T1, T2, tm, Line, Cause, Line_o, Line_e, Line_oe " +
                          "FROM Wind WHERE IDdistrW = " + idDistr;
          try (PreparedStatement ps = cmA.getConnection().prepareStatement(sqlSelect)) {
            try (ResultSet rss = ps.executeQuery()) {
              while (rss.next()) {
                String sqlInsert = "INSERT INTO xWind " +
                        "(GDParmID, IDspan, T1, T2, tm, Line, Cause, Line_o, Line_e, Line_oe) " +
                        "VALUES (" +
                        idGDParm + "," +
                        getNormalValue(rss.getString("IDspan")) + "," +
                        getNormalValue(rss.getString("T1")) + "," +
                        getNormalValue(rss.getString("T2")) + "," +
                        getNormalValue(rss.getString("tm")) + "," +
                        getNormalValue(rss.getString("Line")) + "," +
                        getNormalValue(rss.getString("Cause")) + "," +
                        getNormalValue(rss.getString("Line_o")) + "," +
                        getNormalValue(rss.getString("Line_e")) + "," +
                        getNormalValue(rss.getString("Line_oe")) + ")";
                try (PreparedStatement pss = this.cmSQL.getConnection().prepareStatement(sqlInsert)) {
                  pss.executeUpdate();
                }
              }
            }
          } catch (SQLException e) {
            logger.error(e.toString(), e);
          }

        }  // rs.next
      } catch (SQLException e) {
        logger.error(e.toString(), e);
      }
    } catch (SQLException e) {
      logger.error(e.toString(), e);
    }
    return true;
  }

  private String getNormalDate(String date) {
    if (date != null) {
      if (date.length() > 23) date = date.substring(0, 23);
      return "convert(datetime,'" + date + "',121)";
    }
    return "convert(datetime,0,121)";
  }

  private String getNormalValue(String value) {
    if (value != null) return "'" + value + "'";
    return "'0'";
  }
}
