package com.gmail.alaerof.dao.sql;

import java.sql.Connection;

public interface ConnectionManager {
    Connection getConnection() throws PoolManagerException;
    void closeConnection();
    String getPath();
    boolean isHSQLDB();
}
