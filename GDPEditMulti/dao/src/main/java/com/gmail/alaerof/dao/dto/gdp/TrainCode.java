package com.gmail.alaerof.dao.dto.gdp;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class TrainCode {
    /** 0 - ����� ������ */
    public static int CODE_TRAIN = 0; // ViewCode
    /** 1 - ����� ������� */
    public static int AINSCR_TRAIN = 1; // ViewCode
    /** 2 - ������ ������� */
    public static int AINSCR_SELF = 2; // ViewCode

    private long idTrain; // IDtrain
    private int idDistr; // IDdistr
    /** ��� �������, ����� �������  ��������� ����� ������ */
    private int idStB; // IDstB
    /** ������� ������ ������ */
    private int viewCode; // ViewCode

    
    public long getIdTrain() {
        return idTrain;
    }
    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }
    public int getIdDistr() {
        return idDistr;
    }
    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }
    public int getIdStB() {
        return idStB;
    }
    public void setIdStB(int idStB) {
        this.idStB = idStB;
    }
    public int getViewCode() {
        return viewCode;
    }
    public void setViewCode(int viewCode) {
        this.viewCode = viewCode;
    }
    @Override
    public String toString() {
        return "TrainCode [idTrain=" + idTrain + ", idDistr=" + idDistr + ", idStB=" + idStB + ", viewCode="
                + viewCode + "]";
    }
    
    
}
