package com.gmail.alaerof.dao.dto.gdp;

import com.gmail.alaerof.dao.dto.Time;

/**
 * �������������� ����� ���� ������ �� ��������
 * @author Helen Yrofeeva
 */
public class SpanTrain {
    //todo ������� �� �� ���� IDsptr
    private int idSpan;
    private long idTrain;
    private Time time;

    public int getIdSpan() {
        return idSpan;
    }

    public void setIdSpan(int idSpan) {
        this.idSpan = idSpan;
    }

    public long getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }
}
