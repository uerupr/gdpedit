package com.gmail.alaerof.dao.dto;

/**
 * �������� ��������� ��� ���������� �������
 */
public class StationIntGR {
    private int idIntGR;
    private int idSt;
    private int value;

    public int getIdIntGR() {
        return idIntGR;
    }

    public void setIdIntGR(int idIntGR) {
        this.idIntGR = idIntGR;
    }

    public int getIdSt() {
        return idSt;
    }

    public void setIdSt(int idSt) {
        this.idSt = idSt;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
