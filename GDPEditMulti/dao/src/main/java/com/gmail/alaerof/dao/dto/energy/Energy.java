package com.gmail.alaerof.dao.dto.energy;

/**
 * ������� ������� �� �������� ��� ������ ���� ����������
 */
public class Energy {
    private float enMove;
    private float enUp;
    private float enDown;

    public float getEnMove() {
        return enMove;
    }

    public void setEnMove(float enMove) {
        this.enMove = enMove;
    }

    public float getEnUp() { return enUp; }

    public void setEnUp(float enUp) {
        this.enUp = enUp;
    }

    public float getEnDown() {
        return enDown;
    }

    public void setEnDown(float enDown) {
        this.enDown = enDown;
    }

    @Override
    public String toString() {
        return "Energy[" +
                "enMove=" + enMove +
                ", enUp=" + enUp +
                ", enDown=" + enDown +
                ']';
    }
}
