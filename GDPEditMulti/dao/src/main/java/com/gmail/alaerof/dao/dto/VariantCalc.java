package com.gmail.alaerof.dao.dto;

import java.util.Date;

/**
 * ������� ������� �������� ��� �������
 * @author Helen Yrofeeva
 * 
 */
public class VariantCalc {
    public static final int ODD = 1;
    public static final int EVEN = 0;
    private int IDvarCalc;
    private String name;
    /** ���� ���������� �� ������ */
    private Date date;
    private String description;
    /** ����������� �������� 0-������, 1-�������� */
    private int oe;
    private int IDvarCalcAdd;
    private String descriptionAdd;

    public int getIDvarCalc() {
        return IDvarCalc;
    }

    public void setIDvarCalc(int iDvarCalc) {
        IDvarCalc = iDvarCalc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /** ���� ���������� �� ������ */
    public Date getDate() {
        return date;
    }

    /** ���� ���������� �� ������ */
    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /** ����������� �������� 0-������, 1-�������� */
    public int getOe() {
        return oe;
    }

    /** ����������� �������� 0-������, 1-�������� */
    public void setOe(int oe) {
        this.oe = oe;
    }

    public String getStrOE() {
        if (oe == ODD) {
            return "�����";
        }
        return "���";
    }

    public int getIDvarCalcAdd() {
        return IDvarCalcAdd;
    }

    public void setIDvarCalcAdd(int iDvarCalcAdd) {
        IDvarCalcAdd = iDvarCalcAdd;
    }

    public String getDescriptionAdd() {
        return descriptionAdd;
    }

    public void setDescriptionAdd(String descriptionAdd) {
        this.descriptionAdd = descriptionAdd;
    }

    public boolean hasAdd() {
        return IDvarCalcAdd>0;
    }
}
