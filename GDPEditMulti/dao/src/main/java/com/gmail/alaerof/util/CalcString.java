package com.gmail.alaerof.util;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Formatter;
import java.util.regex.Pattern;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
public class CalcString {
    private static BufferedImage canvas = new BufferedImage(30, 10, BufferedImage.TYPE_INT_ARGB);
    private static Graphics g = canvas.createGraphics();

    /**
     * ���������� ������ ������ � �������� ��� ��������� ������
     * 
     * @param f
     *            �����
     * @param str
     *            ������
     * @return ������ ������ � ��������
     */
    public static int getStringW(Font f, String str) {
        int w = 0;
        g.setFont(f);
        FontMetrics fm = g.getFontMetrics();
        if (str != null) {
            w = fm.stringWidth(str);
        }
        return w;
    }

    /**
     * ���������� ������ ������ � �������� ��� ��������� ������
     * 
     * @param f
     *            �����
     * @return ������ ������ � ��������
     */
    public static int getStringH(Font f) {
        int h = 0;
        g.setFont(f);
        FontMetrics fm = g.getFontMetrics();
        h = fm.getHeight();
        return h;
    }

    /**
     * ����������� ������� � ������
     * 
     * @param value
     *            �����
     * @param intP
     *            ������ �� �����
     * @param decP
     *            ������ ����� �������
     * @return ����������������� ����� � ���� ������
     */
    public static String formatFloat(float value, int intP, int decP) {
        String str = "%" + intP + '.' + decP + 'f';
        Formatter f = new Formatter();
        f.format(str, value);
        str = f.toString();
        String p = ".*\\,0.*";
        if (Pattern.matches(p, str)) {
            int x = (int) value;
            if (x > 0) {
                str = String.valueOf(x);
            } else {
                str = "";
            }
        }
        return str;
    }
}
