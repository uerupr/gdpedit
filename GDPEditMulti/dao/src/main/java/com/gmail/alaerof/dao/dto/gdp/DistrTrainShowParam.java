package com.gmail.alaerof.dao.dto.gdp;

/**
 * ��������� ����������� ����� ������ �� �������
 * @author Helen Yrofeeva
 */
public class DistrTrainShowParam {
    private boolean hidden;
    private boolean showCodeI; // ShowCodeI ���� true, �� ������������ ainscr,
                               // ����� ������������ codeTR
    private int colorTR = 0; // ColorTR (color) = ���� ������ �� ������� (����
                             // �� ������� ���, �� ������ �� ���������) 3
                             // ������� ��
    private String ainscr = ""; // AInscr �������������� ����������� ������ 
                                // (�� �� ���������)
    /** ����� �����
     * 0 - �������� (Solid);
     * 1 - ����� (Dash);
     * 2 - �����-������� (DashDot);
     * 3 - ��������� (Wave);
     * 4 - � ���������(Double)
     */
    private int lineStyle;
                                                   
    private int widthTR = 1;

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isShowCodeI() {
        return showCodeI;
    }

    public void setShowCodeI(boolean showCodeI) {
        this.showCodeI = showCodeI;
    }

    public int getColorTR() {
        return colorTR;
    }

    public void setColorTR(int colorTR) {
        this.colorTR = colorTR;
    }

    public String getAinscr() {
        return ainscr;
    }

    public void setAinscr(String ainscr) {
        if (ainscr != null) {
            this.ainscr = new String(ainscr.trim());
        }
    }

    public int getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(int lineStyle) {
        this.lineStyle = lineStyle;
    }

    public int getWidthTR() {
        return widthTR;
    }

    public void setWidthTR(int widthTR) {
        this.widthTR = widthTR;
    }

    @Override
    public String toString() {
        return "DistrTrainShowParam [hidden=" + hidden + ", colorTR=" + colorTR + ", lineStyle=" + lineStyle
                + ", widthTR=" + widthTR + "]";
    }

}
