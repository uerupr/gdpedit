package com.gmail.alaerof.util;

import com.gmail.alaerof.dao.dto.TextPrintParam;
import java.util.Comparator;

/**
 * Created by Paul M. Bui on 07.06.2018.
 */
public class ListTextPrintParamComparator implements Comparator<TextPrintParam> {
    @Override
    public int compare(TextPrintParam textPrintParam1, TextPrintParam textPrintParam2) {
        if ((textPrintParam1.getTpType() < textPrintParam2.getTpType()) || ((textPrintParam1.getTpType() == textPrintParam2.getTpType()) && (textPrintParam1.getNum() <= textPrintParam2.getNum()))){
            return -1;
        }else{
            return 0;
        }
    }
}
