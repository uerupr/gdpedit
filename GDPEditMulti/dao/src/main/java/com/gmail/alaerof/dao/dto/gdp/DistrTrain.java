package com.gmail.alaerof.dao.dto.gdp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gmail.alaerof.dao.common.TrainStTableDataBase;
import com.gmail.alaerof.util.TimeConverter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * �������� ����� ������ �� �������
 */
public class DistrTrain {
    private static Logger logger = LogManager.getLogger(DistrTrain.class);
    // Train
    private long idTrain; // IDtrain
    private String codeTR = ""; // code
    private String nameTR = ""; // Name
    private int idCat;
    private int priority; // prioritet_;
    // type �� ������ � ����� �������� (����������) ���� ������������ ����������� ��� ������ ����
    private TrainType typeTR;
    // ������������� ���� ����������
    private int idLtype;
    // �����/���
    private OE oe;

    // Distr_Train
    /**
     * ��� �������, �������� ����������� �����,
     * �� ����� ���������� �� ���� �������, �� ������� ������������
     */
    private int idDistr; // IDdistr
    private String ps = ""; // PS
    // ����������� � ��������� ������� �������
    private String occupyOff;
    // �������� �� ������ �� �������
    private String tOn;
    // ����������� � ������ �� �������
    private String tOff;
    // �������� �� ��������� �� �������
    private String tOn_e;
    // ����������� � ��������� �� �������
    private String tOff_e;

    /** Tu ����� ���������� ������ �� �������, ��� */
    private double timeWithStop;
    /** Tt ����� ���������� ������ �� ������� ��� ����� ������� �������, ��� */
    private double timeWithoutStop;
    /** L ����������, ���������� ������� �� �������, � */
    private double len;
    /** ���-�� ��������� (����� ��������� � ��������) */
    private int stopCount;
    /** ���������� ������ �� ��� */
    private List<DirTrain> dirTrainList = new ArrayList<>();

    // Distr_Train_ShowParam
    private DistrTrainShowParam showParam = new DistrTrainShowParam();
    /** ���������� �������������������� ������ �� �������, key = ExpenseKeyString = locType + EnergyType */
    private Map<String, DistrTrainExpense> distrTrainExpenseMap = new HashMap<>();

    /** ����� ���������� ������ �� �.�. �� �������, ���� IDst */
    private Map<Integer, LineStTrain> mapLineStTr = new HashMap<>();
    /** ����� ���������� ������ �� o.�. �� �������, ���� IDspanst */
    private Map<Integer, TrainStop> mapTrainStop = new HashMap<>();
    /** ����� ������� ��������� �������, ���� IDspan */
    private Map<Integer, SpTOcc> mapSpTOcc = new HashMap<>();
    /** ����� ����������� �������, ���� IDst */
    private Map<Integer, TrainCode> mapTrainCode = new HashMap<>();
    /** ����� ������� �����, ���� IDst */
    private Map<Integer, TrainHideSpan> mapTrainHideSpan = new HashMap<>();
    /** ����� ����� �����, ���� IDst */
    private Map<Integer, TrainLineStyleSpan> mapTrainLineStyleSpan = new HashMap<>();
    /** ����� ����� �����, ���� IDst */
    private Map<Integer, TrainSpanColor> mapTrainSpanColor = new HashMap<>();
    /** ����� �������� �����, ���� IDst */
    private Map<Integer, TrainSpanInscr> mapTrainSpanInscr = new HashMap<>();
    /** ����� �������������� ������ ����, ���� IDspan */
    private Map<Integer, SpanTrain> mapSpanTrain = new HashMap<>();

    // ��������� �������� ��������� ����������� ��� �����!!!!
    private boolean onlyStation = false;

    public String getTimeOnStation(int idSt) {
        String res = null;
        LineStTrain lst = mapLineStTr.get(idSt);
        if (lst != null) {
            String tOn = lst.getOccupyOn();
            String tOff = lst.getOccupyOff();
            if (tOn != null) {
                res = tOn;
            } else {
                res = tOff;
            }
        }
        return res;
    }

    public Map<String, DistrTrainExpense> getDistrTrainExpenseMap() {
        return distrTrainExpenseMap;
    }

    public void setDistrTrainExpenseMap(Map<String, DistrTrainExpense> distrTrainExpenseMap) {
        this.distrTrainExpenseMap = distrTrainExpenseMap;
    }

    public int getStopCount() {
        return stopCount;
    }

    public void setStopCount(int stopCount) {
        this.stopCount = stopCount;
    }

    public long getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }

    public int getIdCat() {
        return idCat;
    }

    public void setIdCat(int idCat) {
        this.idCat = idCat;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getIdDistr() {
        return idDistr;
    }

    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }

    public String getCodeTR() {
        return codeTR;
    }

    public void setCodeTR(String codeTR) {
        if (codeTR != null) {
            this.codeTR = codeTR;
        }
    }

    public String getNameTR() {
        return nameTR;
    }

    public void setNameTR(String nameTR) {
        if (nameTR != null) {
            this.nameTR = new String(nameTR.trim());
        }
    }

    public TrainType getTypeTR() {
        return typeTR;
    }

    public void setTypeTR(TrainType typeTR) {
        if (typeTR != null) {
            this.typeTR = typeTR;
        }
    }

    public void setTypeTR(String type) {
        TrainType t = TrainType.getType(type);
        this.typeTR = t;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        if (ps != null) {
            this.ps = ps;
        }
    }

    public OE getOe() {
        return oe;
    }

    public void setOe() {
        if (Integer.parseInt(codeTR) % 2 == 1) {
            setOe(OE.odd);
        } else {
            setOe(OE.even);
        }
    }

    public void setOe(OE oe) {
        if (oe != null) {
            this.oe = oe;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public int getIdLtype() {
        return idLtype;
    }

    public void setIdLtype(int idLtype) {
        this.idLtype = idLtype;
    }

    public DistrTrainShowParam getShowParam() {
        return showParam;
    }

    public void setShowParam(DistrTrainShowParam showParam) {
        this.showParam = showParam;
    }

    @Override
    public String toString() {
        return "DistrTrain [idTrain=" + idTrain + ", idDistr=" + idDistr + ", codeTR=" + codeTR + ", nameTR="
                + nameTR + ", showParam=" + showParam + "]";
    }

    public Collection<LineStTrain> getListLineStTrain() {
        return (Collection<LineStTrain>) mapLineStTr.values();
    }

    /**
     * ����� ���������� ������ �� �.�. �� �������, ���� IDst
     * @return map
     */
    public Map<Integer, LineStTrain> getMapLineStTr() {
        return mapLineStTr;
    }

    public void fillLineStTrMap(List<LineStTrain> listLineStTr) {
        mapLineStTr = new HashMap<>();
        for (LineStTrain lst : listLineStTr) {
            mapLineStTr.put(lst.getIdSt(), lst);
        }
    }

    public Collection<TrainStop> getTrainStops() {
        if (mapTrainStop != null) {
            return mapTrainStop.values();
        } else
            return null;
    }

    /**
     * ����� ���������� ������ �� o.�. �� �������, ���� IDspanst
     * @return map
     */
    public Map<Integer, TrainStop> getMapTrainStop() {
        return mapTrainStop;
    }

    public void setMapTrainStop(Map<Integer, TrainStop> mapTrainStop) {
        this.mapTrainStop = mapTrainStop;
    }

    /**
     * ����� ������� ��������� �������, ���� IDspan
     * @return map
     */
    public Map<Integer, SpTOcc> getMapSpTOcc() {
        return mapSpTOcc;
    }

    public void setMapSpTOcc(Map<Integer, SpTOcc> mapSpTOcc) {
        this.mapSpTOcc = mapSpTOcc;
    }

    public String getOccupyOff() {
        return occupyOff;
    }

    public void setOccupyOff(String occupyOff) {
        this.occupyOff = occupyOff;
    }

    /**
     * ����� ����������� �������, ���� IDst
     * @return map
     */
    public Map<Integer, TrainCode> getMapTrainCode() {
        return mapTrainCode;
    }

    public void setMapTrainCode(Map<Integer, TrainCode> mapTrainCode) {
        this.mapTrainCode = mapTrainCode;
    }

    public List<TrainCode> getListTrainCode() {
        ArrayList<TrainCode> list = null;
        if (mapTrainCode != null) {
            Collection<TrainCode> col = mapTrainCode.values();
            if (col != null) {
                list = new ArrayList<>(col);
            }
        }
        return list;
    }

    /**
     * ����� ������� �����, ���� IDst
     * @return map
     */
    public Map<Integer, TrainHideSpan> getMapTrainHideSpan() {
        return mapTrainHideSpan;
    }

    public void setMapTrainHideSpan(HashMap<Integer, TrainHideSpan> mapTrainHideSpan) {
        this.mapTrainHideSpan = mapTrainHideSpan;
    }

    public List<TrainHideSpan> getListTrainHideSpan() {
        ArrayList<TrainHideSpan> list = null;
        if (mapTrainHideSpan != null) {
            Collection<TrainHideSpan> col = mapTrainHideSpan.values();
            if (col != null) {
                list = new ArrayList<>(col);
            }
        }
        return list;
    }

    /**
     * ����� ����� �����, ���� IDst
     * @return map
     */
    public Map<Integer, TrainLineStyleSpan> getMapTrainLineStyleSpan() {
        return mapTrainLineStyleSpan;
    }

    public void setMapTrainLineStyleSpan(Map<Integer, TrainLineStyleSpan> trainLineStyleSpan) {
        this.mapTrainLineStyleSpan = trainLineStyleSpan;
    }

    public List<TrainLineStyleSpan> getListTrainLineStyleSpan() {
        ArrayList<TrainLineStyleSpan> list = null;
        if (mapTrainLineStyleSpan != null) {
            Collection<TrainLineStyleSpan> col = mapTrainLineStyleSpan.values();
            if (col != null) {
                list = new ArrayList<>(col);
            }
        }
        return list;
    }

    /**
     * ����� ����� �����, ���� IDst
     * @return map
     */
    public Map<Integer, TrainSpanColor> getMapTrainSpanColor() {
        return mapTrainSpanColor;
    }

    public void setMapTrainSpanColor(HashMap<Integer, TrainSpanColor> mapTrainSpanColor) {
        this.mapTrainSpanColor = mapTrainSpanColor;
    }

    public List<TrainSpanColor> getListTrainSpanColor() {
        ArrayList<TrainSpanColor> list = null;
        if (mapTrainSpanColor != null) {
            Collection<TrainSpanColor> col = mapTrainSpanColor.values();
            if (col != null) {
                list = new ArrayList<>(col);
            }
        }
        return list;
    }

    /**
     * ����� �������� �����, ���� IDst
     * @return map
     */
    public Map<Integer, TrainSpanInscr> getMapTrainSpanInscr() {
        return mapTrainSpanInscr;
    }

    public void setMapTrainSpanInscr(HashMap<Integer, TrainSpanInscr> mapTrainSpanInscr) {
        this.mapTrainSpanInscr = mapTrainSpanInscr;
    }

    public List<TrainSpanInscr> getListTrainSpanInscr() {
        ArrayList<TrainSpanInscr> list = null;
        if (mapTrainSpanInscr != null) {
            Collection<TrainSpanInscr> col = mapTrainSpanInscr.values();
            if (col != null) {
                list = new ArrayList<>(col);
            }
        }
        return list;
    }

    public TrainCode getTrainCode(int idStB) {
        if (mapTrainCode != null) {
            return mapTrainCode.get(idStB);
        } else
            return null;
    }

    public TrainHideSpan getTrainHideSpan(int idStB) {
        if (mapTrainHideSpan != null) {
            return mapTrainHideSpan.get(idStB);
        } else
            return null;
    }

    public TrainLineStyleSpan getTrainLineStyleSpan(int idStB) {
        if (mapTrainLineStyleSpan != null) {
            return mapTrainLineStyleSpan.get(idStB);
        } else
            return null;
    }

    public TrainSpanColor getTrainSpanColor(int idStB) {
        if (mapTrainSpanColor != null)
            return mapTrainSpanColor.get(idStB);
        else
            return null;
    }

    public void removeTrainSpanColor(int idStB) {
        if (mapTrainSpanColor != null)
            mapTrainSpanColor.remove(idStB);
    }

    public TrainSpanInscr getTrainSpanInscr(int idStB) {
        if (mapTrainSpanInscr != null)
            return mapTrainSpanInscr.get(idStB);
        else
            return null;
    }

    public void removeTrainSpanInscr(int idStB) {
        if (mapTrainSpanInscr != null)
            mapTrainSpanInscr.remove(idStB);
    }

    public void setTrainStop(TrainStTableDataBase stTableData, int idStopPoint) {
        Integer id = idStopPoint;
        if (mapTrainStop == null) {
            mapTrainStop = new HashMap<>();
        }
        TrainStop trStop = mapTrainStop.get(id);
        if (trStop == null) {
            trStop = new TrainStop();
            mapTrainStop.put(id, trStop);
        }
        trStop.setIdTrain(idTrain);
        trStop.setIdDistr(idDistr);
        trStop.setIdSpanst(id);
        trStop.setSk_ps(stTableData.sk_ps);
        trStop.setOccupyOn(stTableData.tOn);
        trStop.setOccupyOff(stTableData.tOff);

        int tstop = 0;
        if (stTableData.tOn != null && stTableData.tOff != null) {
            tstop = TimeConverter.minutesBetween(stTableData.tOn, stTableData.tOff);
        }

        trStop.setTstop(tstop);
    }

    public void removeTrainStop(Integer id) {
        if (mapTrainStop != null) {
            mapTrainStop.remove(id);
        }
    }

    public String gettOn() {
        return tOn;
    }

    public void settOn(String tOn) {
        this.tOn = tOn;
    }

    public String gettOff() {
        return tOff;
    }

    public void settOff(String tOff) {
        this.tOff = tOff;
    }

    public String gettOn_e() {
        return tOn_e;
    }

    public void settOn_e(String tOn_e) {
        this.tOn_e = tOn_e;
    }

    public String gettOff_e() {
        return tOff_e;
    }

    public void settOff_e(String tOff_e) {
        this.tOff_e = tOff_e;
    }

    public void setTrainSpanColor(int idDistr, int idSt, int color) {
        TrainSpanColor sp = mapTrainSpanColor.get(idSt);
        if (sp == null) {
            sp = new TrainSpanColor();
            sp.setIdTrain(idTrain);
            sp.setIdDistr(idDistr);
            sp.setIdStB(idSt);
            mapTrainSpanColor.put(idSt, sp);
        }
        sp.setColorTR(color);
    }

    public void setTrainHideSpan(int idDistr, int idSt, String shape) {
        if (mapTrainHideSpan == null) {
            mapTrainHideSpan = new HashMap<>();
        }
        TrainHideSpan sp = mapTrainHideSpan.get(idSt);
        if (sp == null) {
            sp = new TrainHideSpan();
            sp.setIdTrain(idTrain);
            sp.setIdDistr(idDistr);
            sp.setIdStB(idSt);
            mapTrainHideSpan.put(idSt, sp);
        }
        sp.setShape(shape);
    }

    public void removeTrainHideSpan(int idSt) {
        if (mapTrainHideSpan != null) {
            mapTrainHideSpan.remove(idSt);
        }
    }

    public void setTrainCode(int idSt, int viewCode, int idDistr) {
        if (mapTrainCode == null) {
            mapTrainCode = new HashMap<>();
        }
        TrainCode sp = mapTrainCode.get(idSt);
        if (sp == null) {
            sp = new TrainCode();
            sp.setIdTrain(idTrain);
            sp.setIdDistr(idDistr);
            sp.setIdStB(idSt);
            mapTrainCode.put(idSt, sp);
        }
        sp.setViewCode(viewCode);

    }

    public void removeTrainCode(int idSt) {
        if (mapTrainCode != null) {
            mapTrainCode.remove(idSt);
        }
    }

    public void setTrainLineStyleSpan(int idSt, int style, String shape, int idDistr) {
        if (mapTrainLineStyleSpan == null) {
            mapTrainLineStyleSpan = new HashMap<>();
        }
        TrainLineStyleSpan sp = mapTrainLineStyleSpan.get(idSt);
        if (sp == null) {
            sp = new TrainLineStyleSpan();
            sp.setIdTrain(idTrain);
            sp.setIdDistr(idDistr);
            sp.setIdStB(idSt);
            mapTrainLineStyleSpan.put(idSt, sp);
        } 
        sp.setShape(shape);
        sp.setLineStyle(style);
    }

    public void removeTrainLineStyleSpan(int idSt) {
        if (mapTrainLineStyleSpan != null) {
            mapTrainLineStyleSpan.remove(idSt);
        }
    }
    
    public void setTrainSpanInscr(int idSt, String ainscr, int idDistr) {
        if (mapTrainSpanInscr == null) {
            mapTrainSpanInscr = new HashMap<>();
        }
        TrainSpanInscr sp = mapTrainSpanInscr.get(idSt);
        if (sp == null) {
            sp = new TrainSpanInscr();
            sp.setIdTrain(idTrain);
            sp.setIdDistr(idDistr);
            sp.setIdStB(idSt);
            mapTrainSpanInscr.put(idSt, sp);
        }
        sp.setAInscr(ainscr);
    }

    public int getCodeTRInt() {
        int res = 0;
        try {
            res = Integer.parseInt(this.getCodeTR());
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return res;
    }

    public double getTimeWithStop() {
        return timeWithStop;
    }

    public void setTimeWithStop(double timeWithStop) {
        this.timeWithStop = timeWithStop;
    }

    public double getTimeWithoutStop() {
        return timeWithoutStop;
    }

    public void setTimeWithoutStop(double timeWithoutStop) {
        this.timeWithoutStop = timeWithoutStop;
    }

    public double getLen() {
        return len;
    }

    public void setLen(double len) {
        this.len = len;
    }

    public boolean isOnlyStation() {
        return onlyStation;
    }

    public void setOnlyStation(boolean onlyStation) {
        this.onlyStation = onlyStation;
    }

    /**
     * ����� �������������� ������ ����, ���� IDspan
     * @return map
     */
    public Map<Integer, SpanTrain> getMapSpanTrain() {
        return mapSpanTrain;
    }

    public void setMapSpanTrain(Map<Integer, SpanTrain> mapSpanTrain) {
        this.mapSpanTrain = mapSpanTrain;
    }

    public void clearPreviousCalc() {
        /** Tu ����� ���������� ������ �� �������, ��� */
        timeWithStop=0;
        /** Tt ����� ���������� ������ �� ������� ��� ����� ������� �������, ��� */
        timeWithoutStop=0;
        /** L ����������, ���������� ������� �� �������, � */
        len=0;
        /** ���-�� ��������� (����� ��������� � ��������) */
        stopCount=0;

        distrTrainExpenseMap.clear();
    }

    public boolean equalsByID(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DistrTrain that = (DistrTrain) o;

        return idTrain == that.idTrain;
    }

    public double getVt(){
        double Vt = 0;
        if (getTimeWithoutStop() > 0) {
            Vt = ((int) getLen() / 1000) / (getTimeWithoutStop() / 60);
        }
        return Vt;
    }

    public double getVu(){
        double Vt = 0;
        if (getTimeWithStop() > 0) {
            Vt = ((int) getLen() / 1000) / (getTimeWithStop() / 60);
        }
        return Vt;
    }

    public DirTrain getDirTrain(int idDir) {
        for (DirTrain dirTrain : dirTrainList) {
            if (dirTrain.getIdDir() == idDir) {
                return dirTrain;
            }
        }
        return null;
    }

    public List<DirTrain> getDirTrainList() {
        return dirTrainList;
    }

}
