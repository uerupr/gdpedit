package com.gmail.alaerof.dao.dto.gdp;

/**
 */
public class TrainLineStyleSpan {
    private long idTrain; // IDtrain
    private int idDistr; // IDdistr
    /** ��� �������, ����� ������� ��������������� �������������� ����� ����� */
    private int idStB; // IDstB
    /** ��� ����� */
    private String shape; // �������� �(���) ��������� (;hor;span)
    /** ����� �����
     * 0 - �������� (Solid);
     * 1 - ����� (Dash);
     * 2 - �����-������� (DashDot);
     * 3 - ��������� (Wave);
     * 4 - � ���������(Double)
     */
    private int lineStyle; // LineStyle

    public long getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }

    public int getIdDistr() {
        return idDistr;
    }

    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }

    public int getIdStB() {
        return idStB;
    }

    public void setIdStB(int idStB) {
        this.idStB = idStB;
    }

    public int getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(int style) {
        this.lineStyle = style;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape.trim();
    }
    
    public boolean isHor(){
        return shape.contains("hor");
    }
    
    public boolean isSpan(){
        return shape.contains("span");
    }

    @Override
    public String toString() {
        return "TrainLineStyle [idTrain=" + idTrain + ", idDistr=" + idDistr + ", idStB=" + idStB
                + ", shape=" + shape + ", lineStyle=" + lineStyle + "]";
    }
}
