package com.gmail.alaerof.dao.factory;

import com.gmail.alaerof.dao.interfacedao.*;
import com.gmail.alaerof.dao.sql.impl.SQLQueryGDParm;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public interface IGDPDAOFactory {
	enum DataSourceType{
		Server, Local
	}
	IDirDAO getDirDAO();
	IDistrDAO getDistrDAO();
	IScaleDAO getScaleDAO();
	IStationDAO getStationDAO();
	ISpanTimeDAO getSpanTimeDAO();
	ISpanEnergyDAO getSpanEnergyDAO();
	IDistrTrainDAO getDistrTrainDAO();
	IVariantCalcDAO getVariantCalcDAO();
	IDistrGDPXMLDAO getDistrGDPXMLDAO();
	IGDPTextPrintParamDAO getTextPrintParamDAO();
	IDistrCombDAO getDistrCombDAO();
	ICategoryDAO getCategoryDAO();
	IGroupPDAO getGroupPDAO();
	IGDPCalcDAO getGDPCalcDAO();
	IGDParmDAO getGDParmDAO();
	String getPath();
	IUsersDAO getUsersDAO();
	SQLQueryGDParm getSQLGDParm();

	/**
	 * ����� ������ ������������ � �����
	 * GDPEdit.ini 
	 * ���� SERVER=1, �� ��� ����� ������ � ��������
	 * ����� - ��������� �����
	 * @return true ���� ����� ������ � ��������
	 */
	boolean isServer();

	/**
	 * ����� �� ������������ Access ��� HSQLDB
	 * @return boolean
	 */
	boolean isHSQLDB();

	/**
	 * �������� ����������
	 */
	void closeConnections();
}

