package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.groupp.GroupColor;
import com.gmail.alaerof.dao.dto.groupp.GroupP;
import com.gmail.alaerof.dao.dto.groupp.GroupRange;
import com.gmail.alaerof.dao.interfacedao.IGroupPDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * ��������� (������) ������� ��� ������� ����������� ��� �������
 * �������������� � ��
 */
public class GroupPDAOSQL implements IGroupPDAO {
    protected static Logger logger = LogManager.getLogger(GroupPDAOSQL.class);
    private final ConnectionManager cm;

    public GroupPDAOSQL(ConnectionManager cmServer) {
        this.cm = cmServer;
    }

    /**{@inheritDoc}*/
    @Override
    public List<GroupP> getGroupPList() throws  DataManageException {
        List<GroupP> list = new ArrayList<>();
        String sqlGroupP = "SELECT IDgroup, Name, ByColor, Ord FROM GroupP";
        String sqlRange = "SELECT IDrange, IDgroup, minCode, maxCode FROM GroupRange";
        String sqlColor = "SELECT IDcolorGR, IDgroup, color FROM GroupColor";

        try {
            Connection cn = cm.getConnection();
            Map<Integer, GroupP> groupMap = null;
            try (Statement st = cn.createStatement();
                 ResultSet rsGroupP = st.executeQuery(sqlGroupP))
            {
                groupMap = getGroupP(rsGroupP);
            }
            if (groupMap != null) {
                try (Statement st = cn.createStatement();
                     ResultSet rsRange = st.executeQuery(sqlRange);) {
                    fillGroupRange(groupMap, rsRange);
                }
                try (Statement st = cn.createStatement();
                     ResultSet rsColor = st.executeQuery(sqlColor);) {
                    fillGroupColor(groupMap, rsColor);
                }
                list.addAll(groupMap.values());
                //Collections.sort(list);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    /**{@inheritDoc}*/
    @Override
    public void saveToDB(List<GroupP> groupPList,
                         List<GroupP> deletedGroupP,
                         List<GroupRange> deletedRange,
                         List<GroupColor> deletedColor) throws DataManageException
    {
        deleteFromGroupP(deletedGroupP);
        deleteFromRange(deletedRange);
        deleteFromColor(deletedColor);
        String updGR = "UPDATE GroupP SET Name=?, ByColor=?, Ord=? WHERE IDgroup = ?";
        String insGR = "INSERT INTO GroupP(Name, ByColor, Ord) VALUES(?,?,?)";
        String maxID = "SELECT MAX(IDgroup) FROM GroupP";

        String updRange = "UPDATE GroupRange SET minCode=?, maxCode=? WHERE IDrange=?";
        String insRange = "INSERT INTO GroupRange (IDgroup, minCode, maxCode) VALUES(?,?,?)";

        String updColor = "UPDATE GroupColor SET color=? WHERE IDcolorGR=?";
        String insColor = "INSERT INTO GroupColor (IDgroup, color) VALUES(?,?)";

        try {
            Connection cn = cm.getConnection();
            try (PreparedStatement psUpdGR = cn.prepareStatement(updGR);
                 PreparedStatement psInsGR = cn.prepareStatement(insGR);
                 Statement stMaxID = cn.createStatement();
                 PreparedStatement psUpdRange = cn.prepareStatement(updRange);
                 PreparedStatement psInsRange = cn.prepareStatement(insRange);
                 PreparedStatement psUpdColor = cn.prepareStatement(updColor);
                 PreparedStatement psInsColor = cn.prepareStatement(insColor))
            {
                for (GroupP groupP : groupPList) {
                    if (groupP.getIdGroup() > 0) {
                        psUpdGR.setString(1, groupP.getName());
                        psUpdGR.setBoolean(2, groupP.isByColor());
                        psUpdGR.setInt(3, groupP.getOrd());
                        psUpdGR.setInt(4, groupP.getIdGroup());
                        psUpdGR.executeUpdate();
                    } else {
                        psInsGR.setString(1, groupP.getName());
                        psInsGR.setBoolean(2, groupP.isByColor());
                        psInsGR.setInt(3, groupP.getOrd());
                        psInsGR.executeUpdate();
                        // ����� ID �� ��
                        int groupID = 0;
                        ResultSet rs = stMaxID.executeQuery(maxID);
                        if (rs.next()) {
                           groupID = rs.getInt(1);
                        } else {
                            throw new SQLException("GroupP max ID was failed !!!");
                        }
                        groupP.setIdGroup(groupID);
                    }
                    saveGroupRange(psUpdRange, psInsRange, groupP);
                    saveGroupColor(psUpdColor, psInsColor, groupP);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(e.toString(), e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    public void saveGroupColor(PreparedStatement psUpdColor,
                               PreparedStatement psInsColor, GroupP groupP) throws SQLException
    {
        for (GroupColor color : groupP.getColorList()) {
            if (color.getIdColorGR() > 0) {
                psUpdColor.setInt(1, color.getColor());
                psUpdColor.setInt(2, color.getIdColorGR());
                psUpdColor.executeUpdate();
            } else {
                psInsColor.setInt(1, groupP.getIdGroup());
                psInsColor.setInt(2, color.getColor());
                psInsColor.executeUpdate();
            }
        }
    }

    public void saveGroupRange(PreparedStatement psUpdRange,
                               PreparedStatement psInsRange, GroupP groupP) throws SQLException
    {
        for (GroupRange range : groupP.getRangeList()) {
            if (range.getIdRange() > 0) {
                psUpdRange.setInt(1, range.getMinCode());
                psUpdRange.setInt(2, range.getMaxCode());
                psUpdRange.setInt(3, range.getIdRange());
                psUpdRange.executeUpdate();
            } else {
                psInsRange.setInt(1, groupP.getIdGroup());
                psInsRange.setInt(2, range.getMinCode());
                psInsRange.setInt(3, range.getMaxCode());
                psInsRange.executeUpdate();
            }
        }
    }
    private void deleteFromColor(List<GroupColor> deletedColor) throws DataManageException {
        if (deletedColor.size() > 0) {
            String sql = "DELETE FROM GroupColor WHERE IDcolorGR = ?";
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement ps = cn.prepareStatement(sql)) {
                    for (GroupColor color : deletedColor) {
                        ps.setInt(1, color.getIdColorGR());
                        ps.addBatch();
                    }
                    ps.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(e.toString(), e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    private void deleteFromRange(List<GroupRange> deletedRange) throws DataManageException {
        if (deletedRange.size() > 0) {
            String sql = "DELETE FROM GroupRange WHERE IDrange = ?";
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement ps = cn.prepareStatement(sql)) {
                    for (GroupRange range : deletedRange) {
                        ps.setInt(1, range.getIdRange());
                        ps.addBatch();
                    }
                    ps.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(e.toString(), e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    private void deleteFromGroupP(List<GroupP> deletedGroupP) throws DataManageException {
        if (deletedGroupP.size() > 0) {
            String sql = "DELETE FROM GroupP WHERE IDgroup = ?";
            try {
                Connection cn = cm.getConnection();

                try (PreparedStatement ps = cn.prepareStatement(sql)) {
                    for (GroupP groupP : deletedGroupP) {
                        ps.setInt(1, groupP.getIdGroup());
                        ps.addBatch();
                    }
                    ps.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(e.toString(), e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    /**
     * ��������� groupMap ���������� �� rs
     * @param groupMap map(IDgroupP, GroupP)
     * @param rs ResultSet - GroupColor
     */
    private void fillGroupColor(Map<Integer, GroupP> groupMap, ResultSet rs) throws SQLException {
        while (rs.next()) {
            GroupColor color = new GroupColor();
            color.setIdColorGR(rs.getInt("IDcolorGR"));
            color.setIdGroup(rs.getInt("IDgroup"));
            color.setColor(rs.getInt("color"));
            GroupP groupP = groupMap.get(color.getIdGroup());
            if (groupP != null) {
                groupP.getColorList().add(color);
            } else {
                logger.error("Can't find GroupP for GroupColor: " + color);
            }
        }
    }

    /**
     * ��������� groupMap ���������� �� rs
     * @param groupMap map(IDgroupP, GroupP)
     * @param rs ResultSet - GroupRange
     */
    private void fillGroupRange(Map<Integer, GroupP> groupMap, ResultSet rs) throws SQLException {
        while (rs.next()) {
            GroupRange range = new GroupRange();
            range.setIdRange(rs.getInt("IDrange"));
            range.setIdGroup(rs.getInt("IDgroup"));
            range.setMinCode(rs.getInt("minCode"));
            range.setMaxCode(rs.getInt("maxCode"));
            GroupP groupP = groupMap.get(range.getIdGroup());
            if (groupP != null) {
                groupP.getRangeList().add(range);
            } else {
                logger.error("Can't find GroupP for GroupRange: " + range);
            }
        }
    }

    /**
     * map(IDgroupP, GroupP)
     * @param rs ResultSet - GroupP
     * @return map
     */
    private Map<Integer, GroupP> getGroupP(ResultSet rs) throws SQLException {
        Map<Integer, GroupP> map = new HashMap<>();
        while (rs.next()) {
            GroupP groupP = new GroupP();
            groupP.setIdGroup(rs.getInt("IDgroup"));
            groupP.setName(rs.getString("Name").trim());
            groupP.setByColor(rs.getBoolean("ByColor"));
            groupP.setOrd(rs.getInt("Ord"));
            map.put(groupP.getIdGroup(), groupP);
        }
        return map;
    }

}
