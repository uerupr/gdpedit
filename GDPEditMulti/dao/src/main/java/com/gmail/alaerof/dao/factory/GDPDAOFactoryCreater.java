package com.gmail.alaerof.dao.factory;

import com.gmail.alaerof.dao.sql.GDPDAOFactorySQL;
import com.gmail.alaerof.dao.sql.PoolManagerException;

/**
 * @author Helen Yrofeeva
 */
public class GDPDAOFactoryCreater {
    private static volatile IGDPDAOFactory factory;

    public static void initGDPDAOFactory() throws PoolManagerException {
        synchronized (GDPDAOFactoryCreater.class) {
            factory = GDPDAOFactorySQL.getInstance();
        }
    }

    public static IGDPDAOFactory getGDPDAOFactory() {
        return factory;
    }
}
