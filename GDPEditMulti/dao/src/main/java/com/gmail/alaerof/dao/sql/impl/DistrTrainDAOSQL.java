package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.dto.gdp.DirTrain;
import com.gmail.alaerof.dao.dto.gdp.SpanTrain;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.util.CategoryUtil;
import com.gmail.alaerof.util.ColorConverter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.gmail.alaerof.dao.dto.TrainCombSt;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainShowParam;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.SpTOcc;
import com.gmail.alaerof.dao.dto.gdp.TrainCode;
import com.gmail.alaerof.dao.dto.gdp.TrainHideSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainLineStyleSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanColor;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanInscr;
import com.gmail.alaerof.dao.dto.gdp.TrainStop;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.IDistrTrainDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.util.TimeConverter;

import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


import static org.apache.commons.lang.StringUtils.trim;

/**
 */
public class DistrTrainDAOSQL implements IDistrTrainDAO {
    protected static Logger logger = LogManager.getLogger(DistrTrainDAOSQL.class);
    private final ConnectionManager cm;
    private static final String SQL_LSTT_DEL = "DELETE FROM LineSt_Train WHERE IDst = ? AND IDtrain = ? ";
    private static final String SQL_LSTT_INS = "INSERT INTO LineSt_Train (IDtrain, IDst, IDdistr, IDLineSt, "
            + "occupyOn, occupyOff, Tstop, TtexStop, sk_ps, overTime, TstopPL, Stop) " +
            " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_DISTR_TRAIN_INS = "INSERT INTO Distr_Train "
            + "(IDtrain, IDdistr, occupyOff, oe, tOn, tOff, tOn_e, tOff_e, "
            + " hidden, ShowCodeI, colorTR, AInscr, LineStyle, widthTR, IDltype) " +
            " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_DISTR_TRAIN_INS_X = "INSERT INTO Distr_Train "
            + "(IDtrain, IDdistr, occupyOff, oe, "
            + " hidden, ShowCodeI, colorTR, AInscr, LineStyle, widthTR) " +
            " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_TRAIN_STOP_DEL = "DELETE FROM TrainStop WHERE IDspanst = ? AND IDtrain = ? ";
    private static final String SQL_TRAIN_STOP_INS = "INSERT INTO TrainStop " +
            "(IDtrain, IDspanst, Tstop, occupyOn, occupyOff, sk_ps, IDdistr) " +
            " VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_SPTOCC_INS = "INSERT INTO SpTOcc " +
            "(IDtrain, IDspan, occupyOn, occupyOff, oe, Tmove, NextDay, IDdistr) " +
            " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String SQL_SHOWPARAM_INS = "INSERT INTO Distr_Train_ShowParam " +
            "(IDtrain, IDdistr, hidden, colorTR, AInscr, LineStyle, widthTR) " +
            " VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_HIDE_SPAN_INS = "INSERT INTO Train_HideSpan (IDtrain, IDdistr, IDstB, shape) " +
            "VALUES (?, ?, ?, ?)";
    private static final String SQL_TRAIN_CODE_INS = "INSERT INTO TrainCode (IDtrain, IDdistr, IDstB, ViewCode) " +
            "VALUES (?, ?, ?, ?)";
    private static final String SQL_SPAN_STYLE_INS = "INSERT INTO Train_LineStyleSpan (IDtrain, IDdistr, IDstB, shape, LineStyle ) " +
            " VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_SPAN_INSCR_INS = "INSERT INTO Train_SpanInscr (IDtrain, IDdistr, IDstB, AInscr ) " +
            "VALUES (?, ?, ?, ?)";
    private static final String SQL_SPAN_COLOR_INS = "INSERT INTO Train_SpanColor (IDtrain, IDdistr, IDstB, colorTR ) " +
            "VALUES (?, ?, ?, ?)";

    public DistrTrainDAOSQL(ConnectionManager cm) {
        this.cm = cm;
    }

    private void fillTrainOnly(DistrTrain tr, ResultSet rs) throws DataManageException {
        try {
            tr.setIdTrain(rs.getLong("IDtrain"));
            String codeTr = rs.getString("code");
            if (codeTr != null) {
                codeTr = trim(codeTr);
            }
            tr.setCodeTR(codeTr);
            String nameTr = rs.getString("Name");
            if (nameTr != null) {
                nameTr = trim(nameTr);
            }
            tr.setNameTR(nameTr);
            String typeTr = rs.getString("type");
            if (typeTr != null) {
                typeTr = trim(typeTr);
            }
            tr.setTypeTR(typeTr);
            tr.setIdDistr(rs.getInt("IDdistr"));
            tr.setPriority(rs.getInt("prioritet_"));
            tr.setIdCat(rs.getInt("IDcat"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Distr_Train " + tr, e);
        }
    }

    private void fillTrain(DistrTrain tr, ResultSet rs) throws DataManageException {
        try {
            tr.setIdTrain(rs.getLong("IDtrain"));
            String code = rs.getString("code");
            tr.setCodeTR(code);
            tr.setNameTR(rs.getString("Name"));
            tr.setTypeTR(rs.getString("type").trim());
            tr.setIdDistr(rs.getInt("IDdistr"));
            tr.setOe();
            tr.setPs(rs.getString("PS"));
            tr.setOccupyOff(TimeConverter.formatTime(rs.getTime("occupyOff")));
            DistrTrainShowParam sp = tr.getShowParam();
            sp.setHidden(rs.getBoolean("hidden"));
            sp.setShowCodeI(rs.getBoolean("ShowCodeI"));
            sp.setColorTR(rs.getInt("colorTR"));
            sp.setAinscr(rs.getString("AInscr"));
            sp.setLineStyle(rs.getInt("LineStyle"));
            sp.setWidthTR(rs.getInt("widthTR"));

            tr.settOn(TimeConverter.formatTime(rs.getTime("tOn")));
            tr.settOff(TimeConverter.formatTime(rs.getTime("tOff")));
            tr.settOn_e(TimeConverter.formatTime(rs.getTime("tOn_e")));
            tr.settOff_e(TimeConverter.formatTime(rs.getTime("tOff_e")));
            tr.setIdLtype(rs.getInt("IDltype"));

            tr.setTimeWithoutStop(rs.getDouble("Tt"));
            tr.setTimeWithStop(rs.getDouble("Tu"));
            tr.setLen(rs.getInt("L"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Distr_Train " + tr, e);
        }
    }

    private void fillShowParam(DistrTrain tr, ResultSet rs) throws DataManageException {
        try {
            DistrTrainShowParam sp = tr.getShowParam();
            sp.setHidden(rs.getBoolean("hidden"));
            sp.setShowCodeI(rs.getBoolean("ShowCodeI"));
            sp.setColorTR(rs.getInt("colorTR"));
            sp.setAinscr(rs.getString("AInscr"));
            sp.setLineStyle(rs.getInt("LineStyle"));
            sp.setWidthTR(rs.getInt("widthTR"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Distr_Train_ShowParam " + tr, e);
        }

    }

    private void fillLineStTrainItem(LineStTrain tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdLineSt(rs.getInt("IDlineSt"));
            tm.setIdSt(rs.getInt("IDst"));
            tm.setIdTrain(rs.getLong("IDtrain"));
            tm.setOccupyOn(TimeConverter.formatTime(rs.getTime("occupyOn")));
            tm.setOccupyOff(TimeConverter.formatTime(rs.getTime("occupyOff")));
            tm.setTstopPL(rs.getInt("TstopPL"));
            tm.setTstop(rs.getInt("Tstop"));
            tm.setStop(rs.getInt("Stop"));
            tm.setTexStop(rs.getInt("TtexStop"));
            String sk_ps = rs.getString("sk_ps");
            tm.setSk_ps(sk_ps != null ? sk_ps.trim() : "");
            tm.setOverTime(rs.getInt("overTime"));
            if (tm.getOccupyOn() == null) {
                tm.setNullOn(true);
            }
            if (tm.getOccupyOff() == null) {
                tm.setNullOff(true);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " LineSt_Train " + tm, e);
        }
    }

    private void fillLineStTrain(DistrTrain tr, Connection cn) throws DataManageException {
        Map<Integer, LineStTrain> mapLineStTrain = tr.getMapLineStTr();
        String sql = "SELECT IDlineSt, IDst, IDtrain, occupyOn, occupyOff, TstopPL, Tstop, Stop, TtexStop, sk_ps, overTime "
                + "FROM LineSt_Train WHERE IDtrain = " + tr.getIdTrain();

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                LineStTrain tm = new LineStTrain();
                fillLineStTrainItem(tm, rs);
                mapLineStTrain.put(tm.getIdSt(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private void fillLineStTrain(DistrTrain tr, int idSt, Connection cn) throws DataManageException {
        Map<Integer, LineStTrain> mapLineStTrain = tr.getMapLineStTr();
        String sql = "SELECT IDlineSt, IDst, IDtrain, occupyOn, occupyOff, TstopPL, Tstop, Stop, TtexStop, sk_ps, overTime "
                + "FROM LineSt_Train WHERE IDtrain = " + tr.getIdTrain() + " AND IDst = " + idSt;

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                LineStTrain tm = new LineStTrain();
                fillLineStTrainItem(tm, rs);
                mapLineStTrain.put(tm.getIdSt(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private void fillTrainStopItem(TrainStop tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdTrain(rs.getLong("IDtrain"));
            tm.setIdSpanst(rs.getInt("IDspanst"));
            tm.setTstop(rs.getInt("Tstop"));
            tm.setOccupyOn(TimeConverter.formatTime(rs.getTime("occupyOn")));
            tm.setOccupyOff(TimeConverter.formatTime(rs.getTime("occupyOff")));
            tm.setSk_ps(rs.getString("sk_ps"));
            tm.setIdDistr(rs.getInt("IDdistr"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " TrainStop " + tm, e);
        }
    }

    private void fillTrainStop(DistrTrain tr, Connection cn) throws DataManageException {
        HashMap<Integer, TrainStop> mapTrainStop = new HashMap<>();
        tr.setMapTrainStop(mapTrainStop);
        String sql = "SELECT IDtrain, IDspanst, Tstop, occupyOn, occupyOff, sk_ps, IDdistr "
                + "FROM TrainStop " + "WHERE IDtrain = " + tr.getIdTrain();

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                TrainStop tm = new TrainStop();
                fillTrainStopItem(tm, rs);
                mapTrainStop.put(tm.getIdSpanst(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private void fillSpTOccItem(SpTOcc tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdSpan(rs.getInt("IDspan"));
            tm.setIdTrain(rs.getLong("IDtrain"));
            tm.setOccupyOn(TimeConverter.formatTime(rs.getTime("occupyOn")));
            tm.setOccupyOff(TimeConverter.formatTime(rs.getTime("occupyOff")));
            tm.setOe(rs.getString("oe"));
            tm.setNextDay(rs.getInt("NextDay"));
            tm.setIdDistr(rs.getInt("IDdistr"));
            tm.setTmove(rs.getInt("Tmove"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " SpTOcc " + tm, e);
        }
    }

    private void fillSpTOcc(DistrTrain tr, Connection cn) throws DataManageException {
        Map<Integer, SpTOcc> mapSpTOcc = new HashMap<>();
        tr.setMapSpTOcc(mapSpTOcc);
        String sql = "SELECT IDspan, IDtrain, occupyOn, occupyOff, oe, NextDay, IDdistr, Tmove "
                + "FROM SpTOcc " + "WHERE IDtrain = " + tr.getIdTrain();

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                SpTOcc tm = new SpTOcc();
                fillSpTOccItem(tm, rs);
                mapSpTOcc.put(tm.getIdSpan(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    @Override
    public void updateTrainCategories(List<DistrTrain> distrTrainList) throws DataManageException {
        String sql = "UPDATE Train "
                + " SET IDcat = ?, prioritet_ = ? "
                + " WHERE IDtrain = ? ";
        if (distrTrainList.size() > 0) {
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement st = cn.prepareStatement(sql)) {
                    for (int i = 0; i < distrTrainList.size(); i++) {
                        st.setInt(1, distrTrainList.get(i).getIdCat());
                        st.setInt(2, distrTrainList.get(i).getPriority());
                        st.setLong(3, distrTrainList.get(i).getIdTrain());
                        st.addBatch();
                    }
                    st.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    @Override
    public void updateTrainPriorities(List<DistrTrain> distrTrainList) throws DataManageException {
        String sql = "UPDATE Train "
                + " SET prioritet_ = ? "
                + " WHERE IDtrain = ? ";
        if (distrTrainList.size() > 0) {
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement st = cn.prepareStatement(sql)) {
                    for (int i = 0; i < distrTrainList.size(); i++) {
                        st.setInt(1, distrTrainList.get(i).getPriority());
                        st.setLong(2, distrTrainList.get(i).getIdTrain());
                        st.addBatch();
                    }
                    st.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    @Override
    public void deleteTrains(List<Long> trainIDList) throws DataManageException {
        if (trainIDList.size() > 0) {
            String trainIDs = buildTrainIDs(trainIDList);
            String sql = "DELETE FROM Train WHERE IDtrain IN (" + trainIDs + ")";
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement st = cn.prepareStatement(sql)) {
                    st.executeUpdate();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    private String buildTrainIDs(List<Long> trainIDList) {
        StringBuilder sb = new StringBuilder();
        for (Long id : trainIDList) {
            sb.append(id).append(", ");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }

    @Override
    public List<DistrTrain> geTrains() throws DataManageException {
        List<DistrTrain> listTR = new ArrayList<>();
        String sql = "SELECT Train.IDtrain, Train.IDcat, Train.code, Train.Name, Train.type, Train.prioritet_, "
                + " Distr_Train.IDdistr "
                + " FROM Train LEFT JOIN Distr_Train ON Train.IDtrain = Distr_Train.IDtrain ";

        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    DistrTrain tr = new DistrTrain();
                    fillTrainOnly(tr, rs);
                    listTR.add(tr);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return listTR;
    }

    public List<DistrTrain> getDistrTrains(int idDistr) throws DataManageException {
        long tm1 = Calendar.getInstance().getTimeInMillis();
        List<DistrTrain> listTR = new ArrayList<>();
        String sql = "SELECT Train.IDtrain, Train.code, Train.Name, Train.type, "
                + "Distr_Train.IDdistr, Distr_Train.ps, Distr_Train.occupyOff, "
                + "hidden, ShowCodeI, colorTR, AInscr,LineStyle, widthTR, tOn, tOff, tOn_e, tOff_e, IDltype, "
                + "Tt, Tu, L "
                + "FROM Train INNER JOIN Distr_Train ON Train.IDtrain = Distr_Train.IDtrain "
                + "WHERE (IDdistr = " + idDistr
                + ") OR (Distr_Train.IDdistr  IN (SELECT IDdistrC FROM  DistrComb WHERE IDdistr = " + idDistr
                + ")) ORDER BY Distr_Train.IDtrain ";

        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    DistrTrain tr = new DistrTrain();
                    fillTrain(tr, rs);

                    listTR.add(tr);
                    // show parameters
                    // ������ ����� �� Distr_Train_ShowParam ��� ���� �����
                    // ��� �������� ������������� ���������� ���� � � Distr_Train
                    // � � Distr_Train_ShowParam
                    String sqlSP = "SELECT hidden, ShowCodeI, colorTR, AInscr,LineStyle, widthTR "
                            + " FROM Distr_Train_ShowParam WHERE IDdistr = " + idDistr + " AND IDtrain = "
                            + tr.getIdTrain();
                    try (Statement pst = cn.createStatement(); ResultSet rsSP = pst.executeQuery(sqlSP)) {
                        if (rsSP.next()) {
                            fillShowParam(tr, rsSP);
                        }
                    }
                    fillLineStTrain(tr, cn);
                    fillTrainStop(tr, cn);
                    fillSpTOcc(tr, cn);
                    fillSpanTrain(tr, cn);
                    fillTrainCode(tr, cn, idDistr);
                    fillTrainHideSpan(tr, cn, idDistr);
                    fillTrainLineStyle(tr, cn, idDistr);
                    fillTrainSpanColor(tr, cn, idDistr);
                    fillTrainSpanInscr(tr, cn, idDistr);
                    fillTrainSpanInscr(tr, cn, idDistr);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        long tm2 = Calendar.getInstance().getTimeInMillis();
        logger.debug("getDistrTrains: " + ((double) (tm2 - tm1)) / 1000.0 + " sec");
        return listTR;
    }

    /**
     * ������ �� �� ��������������� ������ ���� ������
     *
     * @param tr �����
     * @param cn ���������
     */
    private void fillSpanTrain(DistrTrain tr, Connection cn) throws DataManageException {
        Map<Integer, SpanTrain> mapSpanTrain = new HashMap<>();
        tr.setMapSpanTrain(mapSpanTrain);
        String sql = "SELECT IDspan, IDtrain, Tmove, Tdown, Tup, Tw "
                + "FROM SpanTrain " + "WHERE IDtrain = " + tr.getIdTrain();

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                SpanTrain tm = new SpanTrain();
                fillSpanTrainItem(tm, rs);
                mapSpanTrain.put(tm.getIdSpan(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    /**
     * ���������� ������� item ��������������� ������ ���� ������
     *
     * @param item ������
     * @param rs   ������ �� ��
     */
    private void fillSpanTrainItem(SpanTrain item, ResultSet rs) throws DataManageException {
        try {
            item.setIdSpan(rs.getInt("IDspan"));
            item.setIdTrain(rs.getLong("IDtrain"));
            Time tm = new Time();
            tm.setTimeMove(rs.getFloat("Tmove"));
            tm.setTimeDown(rs.getFloat("Tdown"));
            tm.setTimeUp(rs.getFloat("Tup"));
            tm.setTimeTw(rs.getFloat("Tw"));
            item.setTime(tm);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " SpanTrain " + item, e);
        }
    }

    private void fillTrainSpanInscrItem(TrainSpanInscr tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdTrain(rs.getLong("IDtrain"));
            tm.setIdDistr(rs.getInt("IDdistr"));
            tm.setIdStB(rs.getInt("IDstB"));
            tm.setAInscr(rs.getString("AInscr"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " TrainSpanInscr " + tm, e);
        }
    }

    private void fillTrainSpanInscr(DistrTrain tr, Connection cn, int idDistr) throws DataManageException {
        HashMap<Integer, TrainSpanInscr> map = new HashMap<>();
        tr.setMapTrainSpanInscr(map);
        TrainSpanInscr tm = null;
        String sql = "SELECT IDtrain, IDdistr, IDstB, AInscr " + "FROM Train_SpanInscr " + "WHERE IDtrain = "
                + tr.getIdTrain() + " AND IDdistr = " + idDistr;

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                tm = new TrainSpanInscr();
                fillTrainSpanInscrItem(tm, rs);
                map.put(tm.getIdStB(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private void fillTrainSpanColorItem(TrainSpanColor tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdTrain(rs.getLong("IDtrain"));
            tm.setIdDistr(rs.getInt("IDdistr"));
            tm.setIdStB(rs.getInt("IDstB"));
            tm.setColorTR(rs.getInt("colorTR"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " TrainSpanColor " + tm, e);
        }
    }

    private void fillTrainSpanColor(DistrTrain tr, Connection cn, int idDistr) throws DataManageException {
        HashMap<Integer, TrainSpanColor> map = new HashMap<>();
        tr.setMapTrainSpanColor(map);
        TrainSpanColor tm = null;
        String sql = "SELECT IDtrain, IDdistr, IDstB, colorTR " + "FROM Train_SpanColor "
                + "WHERE IDtrain = " + tr.getIdTrain() + " AND IDdistr = " + idDistr;

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                tm = new TrainSpanColor();
                fillTrainSpanColorItem(tm, rs);
                map.put(tm.getIdStB(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private void fillTrainCodeItem(TrainCode tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdTrain(rs.getLong("IDtrain"));
            tm.setIdDistr(rs.getInt("IDdistr"));
            tm.setIdStB(rs.getInt("IDstB"));
            tm.setViewCode(rs.getInt("ViewCode"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " TrainCode " + tm, e);
        }
    }

    private void fillTrainCode(DistrTrain tr, Connection cn, int idDistr) throws DataManageException {
        HashMap<Integer, TrainCode> map = new HashMap<>();
        tr.setMapTrainCode(map);
        TrainCode tm = null;
        String sql = "SELECT IDtrain, IDdistr, IDstB, ViewCode " + "FROM TrainCode " + "WHERE IDtrain = "
                + tr.getIdTrain() + " AND IDdistr = " + idDistr;

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                tm = new TrainCode();
                fillTrainCodeItem(tm, rs);
                map.put(tm.getIdStB(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private void fillTrainHideSpanItem(TrainHideSpan tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdTrain(rs.getLong("IDtrain"));
            tm.setIdDistr(rs.getInt("IDdistr"));
            tm.setIdStB(rs.getInt("IDstB"));
            tm.setShape(rs.getString("shape"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Train_HideSpan " + tm, e);
        }
    }

    private void fillTrainHideSpan(DistrTrain tr, Connection cn, int idDistr) throws DataManageException {
        HashMap<Integer, TrainHideSpan> map = new HashMap<>();
        tr.setMapTrainHideSpan(map);
        TrainHideSpan tm = null;
        String sql = "SELECT IDtrain, IDdistr, IDstB, shape " + "FROM Train_HideSpan " + "WHERE IDtrain = "
                + tr.getIdTrain() + " AND IDdistr = " + idDistr;

        try (Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                tm = new TrainHideSpan();
                fillTrainHideSpanItem(tm, rs);
                map.put(tm.getIdStB(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private void fillTrainLineStyleItem(TrainLineStyleSpan tm, ResultSet rs) throws DataManageException {
        try {
            tm.setIdTrain(rs.getLong("IDtrain"));
            tm.setIdDistr(rs.getInt("IDdistr"));
            tm.setIdStB(rs.getInt("IDstB"));
            tm.setShape(rs.getString("shape"));
            tm.setLineStyle(rs.getInt("LineStyle"));
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " Train_HideSpan " + tm, e);
        }
    }

    private void fillTrainLineStyle(DistrTrain tr, Connection cn, int idDistr) throws DataManageException {
        HashMap<Integer, TrainLineStyleSpan> map = new HashMap<>();
        tr.setMapTrainLineStyleSpan(map);
        TrainLineStyleSpan tm = null;
        String sql = "SELECT IDtrain, IDdistr, IDstB, shape, LineStyle " + "FROM Train_LineStyleSpan "
                + "WHERE IDtrain = " + tr.getIdTrain() + " AND IDdistr = " + idDistr;

        try (Statement st = cn.createStatement(); ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                tm = new TrainLineStyleSpan();
                fillTrainLineStyleItem(tm, rs);
                map.put(tm.getIdStB(), tm);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    @Override
    public List<DistrTrain> getDistrTrainsOnStation(int idSt) throws DataManageException {
        List<DistrTrain> listTR = new ArrayList<>();
        DistrTrain tr = null;
        String sql = "SELECT Train.IDtrain, Train.code, Train.Name, Train.type, "
                + "Distr_Train.IDdistr, Distr_Train.ps, Distr_Train.occupyOff, "
                + "hidden, ShowCodeI, colorTR, AInscr,LineStyle, widthTR, tOn, tOff, tOn_e, tOff_e, IDltype, "
                + "Tt, Tu, L "
                + "FROM Train INNER JOIN Distr_Train ON Train.IDtrain = Distr_Train.IDtrain "
                + "WHERE Train.IDtrain IN (SELECT IDtrain FROM LineSt_Train WHERE IDst = " + idSt + " )";

        try {
            Connection cn = cm.getConnection();
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    tr = new DistrTrain();
                    fillTrain(tr, rs);
                    listTR.add(tr);

                    fillLineStTrain(tr, idSt, cn); // ����� ������ � 1 �������

                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return listTR;
    }

    private void execQuery(String sql, Connection cn) throws DataManageException {
        try (PreparedStatement st = cn.prepareStatement(sql)) {
            st.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    /**
     * �������� ���������� �� ��, ���� idTrain!=0, �� ������ ����� ������, �����
     * ����� ��� �������
     */
    @Override
    public void clearDistrGDP(int idDistr, Collection<DistrTrain> trains, long idTrain, boolean clearSpanTrain, boolean delWind)
            throws DataManageException {
        try {
            long tm1 = Calendar.getInstance().getTimeInMillis();
            long tm0;
            long tm2;
            Connection cn = cm.getConnection();
            // ������� ��� Distr_Train �������� ������� �� ����������
//            String sqlDel = "DELETE FROM Distr_Train WHERE IDdistr NOT IN (SELECT IDdistr FROM Distr)";
//            execQuery(sqlDel, cn);

            // ������� ��� ������������ ���������� (� ����� ������)
            tm0 = Calendar.getInstance().getTimeInMillis();
            clearDistrPokaz(idDistr, cn);
            tm2 = Calendar.getInstance().getTimeInMillis();
            logger.debug("clearDistrPokaz: " + ((double) (tm2 - tm0)) / 1000.0 + " sec");

            // ���������� �� �.�. � �.�.
            tm0 = Calendar.getInstance().getTimeInMillis();
            clearLineSt_Train(idDistr, trains, idTrain, cn);
            tm2 = Calendar.getInstance().getTimeInMillis();
            logger.debug("clearLineSt_Train: " + ((double) (tm2 - tm0)) / 1000.0 + " sec");

            // ��������� ���������, �����. ������� ����, �������� � �������
            tm0 = Calendar.getInstance().getTimeInMillis();
            clearDistr_Train(idDistr, idTrain, clearSpanTrain, cn);
            tm2 = Calendar.getInstance().getTimeInMillis();
            logger.debug("clearDistr_Train: " + ((double) (tm2 - tm0)) / 1000.0 + " sec");

            if (delWind) {
                // ��� ����
                String sql = "DELETE FROM Wind WHERE IDdistrW = " + idDistr;
                execQuery(sql, cn);
            }

            tm2 = Calendar.getInstance().getTimeInMillis();
            logger.debug("clearDistrGDP: " + ((double) (tm2 - tm1)) / 1000.0 + " sec");
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private void clearDistrPokaz(int idDistr, Connection cn) throws DataManageException {

        String sql = "DELETE FROM GroupVal WHERE IDdistr = " + idDistr;
        execQuery(sql, cn);
        sql = "DELETE FROM GroupValDir WHERE IDdistr = " + idDistr;
        execQuery(sql, cn);
        sql = "DELETE FROM DirTrain WHERE IDdistr = " + idDistr;
        execQuery(sql, cn);
    }

    private void clearLineSt_Train(int idDistr, Collection<DistrTrain> trains, long idTrain, Connection cn) throws DataManageException {
        String sql = "";
        // ���������� �� �.�.
        sql = "DELETE FROM LineSt_Train WHERE IDdistr = " + idDistr;
        if (idTrain != 0) {
            sql += " AND  IDtrain = " + idTrain;
        }
        execQuery(sql, cn);

        // ���������� �� �.�.
        sql = "DELETE FROM TrainStop WHERE IDdistr = " + idDistr;
        if (idTrain != 0) {
            sql += " AND  IDtrain = " + idTrain;
        }
        execQuery(sql, cn);
    }

    @Override
    public void clearDistrGDP_ShowParam(int idDistr, long idTrain) throws DataManageException {
        try {
            Connection cn = cm.getConnection();
            String sql = "";
            sql = "DELETE FROM Distr_Train_ShowParam WHERE IDdistr = " + idDistr;
            if (idTrain != 0) {
                sql += " AND  IDtrain = " + idTrain;
            }
            execQuery(sql, cn);

            sql = "DELETE FROM Train_HideSpan WHERE IDdistr = " + idDistr;
            if (idTrain != 0) {
                sql += " AND  IDtrain = " + idTrain;
            }
            execQuery(sql, cn);

            sql = "DELETE FROM Train_LineStyleSpan WHERE IDdistr = " + idDistr;
            if (idTrain != 0) {
                sql += " AND  IDtrain = " + idTrain;
            }
            execQuery(sql, cn);

            sql = "DELETE FROM TrainCode WHERE IDdistr = " + idDistr;
            if (idTrain != 0) {
                sql += " AND  IDtrain = " + idTrain;
            }
            execQuery(sql, cn);

            // ������� � ������ 13.0.1
            sql = "DELETE FROM Train_SpanColor WHERE IDdistr = " + idDistr;
            if (idTrain != 0) {
                sql += " AND  IDtrain = " + idTrain;
            }
            execQuery(sql, cn);

            sql = "DELETE FROM Train_SpanInscr WHERE IDdistr = " + idDistr;
            if (idTrain != 0) {
                sql += " AND  IDtrain = " + idTrain;
            }
            execQuery(sql, cn);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private void clearDistr_Train(int idDistr, long idTrain, boolean clearSpanTrain, Connection cn)
            throws DataManageException {
        String sql = "";
        sql = "DELETE FROM SpTOcc WHERE IDdistr = " + idDistr;
        if (idTrain != 0) {
            sql += " AND  IDtrain = " + idTrain;
        }
        execQuery(sql, cn);

        // �������������� ������� ���� �� ���������
        if (clearSpanTrain) {
            if (idTrain == 0) {
                sql = "DELETE FROM SpanTrain WHERE (IDspan IN "
                        + "(SELECT IDspan FROM Scale WHERE IDdistr = " + idDistr + "))AND(IDtrain IN "
                        + "(SELECT IDtrain FROM Distr_Train WHERE IDdistr = " + idDistr + "))";
            } else {
                sql = "DELETE FROM SpanTrain WHERE (IDspan IN "
                        + "(SELECT IDspan FROM Scale WHERE IDdistr = " + idDistr + ")) AND IDtrain = "
                        + idTrain;
            }
            execQuery(sql, cn);
        }

        sql = "DELETE FROM Distr_Train WHERE IDdistr = " + idDistr;
        if (idTrain != 0) {
            sql += " AND  IDtrain = " + idTrain;
        }
        execQuery(sql, cn);
    }

    private int prepareBatchDistrTrainShowParam(DistrTrain train, int idDistr, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        try {
            int i = 1;
            psIns.setLong(i++, train.getIdTrain());
            psIns.setInt(i++, train.getIdDistr());

            DistrTrainShowParam param = train.getShowParam();
            psIns.setBoolean(i++, param.isHidden());
            psIns.setInt(i++, param.getColorTR());
            psIns.setString(i++, param.getAinscr());
            psIns.setInt(i++, param.getLineStyle());
            psIns.setInt(i++, param.getWidthTR());

            psIns.addBatch();
            res++;
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " DistrTrainShowParam " +
                    train.getCodeTR() + "(" + train.getNameTR() + ")", e);
        }
        return res;
    }

    private int prepareBatchDistrTrainHideSpan(DistrTrain train, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        Collection<TrainHideSpan> list = null;
        Map<Integer, TrainHideSpan> map = train.getMapTrainHideSpan();
        if (map != null) {
            list = map.values();
        }
        if (list != null) {
            try {
                for (TrainHideSpan el : list) {
                    int i = 1;
                    psIns.setLong(i++, el.getIdTrain());
                    psIns.setInt(i++, el.getIdDistr());
                    psIns.setInt(i++, el.getIdStB());
                    psIns.setString(i++, el.getShape());

                    psIns.addBatch();
                    res++;
                }

            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " HideSpan " +
                        train.getCodeTR() + "(" + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    private int prepareBatchDistrTrainCode(DistrTrain train, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        Collection<TrainCode> list = null;
        Map<Integer, TrainCode> map = train.getMapTrainCode();
        if (map != null) {
            list = map.values();
        }

        if (list != null) {
            try {
                for (TrainCode el : list) {
                    int i = 1;
                    psIns.setLong(i++, el.getIdTrain());
                    psIns.setInt(i++, el.getIdDistr());
                    psIns.setInt(i++, el.getIdStB());
                    psIns.setInt(i++, el.getViewCode());

                    psIns.addBatch();
                    res++;
                }

            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " TrainCode " +
                        train.getCodeTR() + "(" + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    private int prepareBatchDistrTrainLineStyleSpan(DistrTrain train, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        Collection<TrainLineStyleSpan> list = null;
        Map<Integer, TrainLineStyleSpan> map = train.getMapTrainLineStyleSpan();
        if (map != null) {
            list = map.values();
        }

        if (list != null) {
            try {
                for (TrainLineStyleSpan el : list) {
                    int i = 1;
                    psIns.setLong(i++, el.getIdTrain());
                    psIns.setInt(i++, el.getIdDistr());
                    psIns.setInt(i++, el.getIdStB());
                    psIns.setString(i++, el.getShape());
                    psIns.setInt(i++, el.getLineStyle());

                    psIns.addBatch();
                    res++;
                }

            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " LineStyleSpan " +
                        train.getCodeTR() + "(" + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    private int prepareBatchDistrTrainSpanInscr(DistrTrain train, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        Collection<TrainSpanInscr> list = null;
        Map<Integer, TrainSpanInscr> map = train.getMapTrainSpanInscr();
        if (map != null) {
            list = map.values();
        }

        if (list != null) {
            try {
                for (TrainSpanInscr el : list) {
                    int i = 1;
                    psIns.setLong(i++, el.getIdTrain());
                    psIns.setInt(i++, el.getIdDistr());
                    psIns.setInt(i++, el.getIdStB());
                    psIns.setString(i++, el.getAInscr());

                    psIns.addBatch();
                    res++;
                }

            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " SpanInscr " + train.getCodeTR() + "("
                        + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    private int prepareBatchDistrTrainSpanColor(DistrTrain train, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        Collection<TrainSpanColor> list = null;
        Map<Integer, TrainSpanColor> map = train.getMapTrainSpanColor();
        if (map != null) {
            list = map.values();
        }
        if (list != null) {
            try {
                for (TrainSpanColor el : list) {
                    int i = 1;
                    psIns.setLong(i++, el.getIdTrain());
                    psIns.setInt(i++, el.getIdDistr());
                    psIns.setInt(i++, el.getIdStB());
                    psIns.setInt(i++, el.getColorTR());

                    psIns.addBatch();
                    res++;
                }

            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " SpanColor " + train.getCodeTR() + "("
                        + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    @Override
    public void saveListTrainGDP_ShowParam(Collection<DistrTrain> trains, int idDistr) throws DataManageException {
        long tm1 = Calendar.getInstance().getTimeInMillis();
        try {
            Connection cn = cm.getConnection();
            try (PreparedStatement psShowParamIns = cn.prepareStatement(SQL_SHOWPARAM_INS);
                 PreparedStatement psHideSpanIns = cn.prepareStatement(SQL_HIDE_SPAN_INS);
                 PreparedStatement psTrainCodeIns = cn.prepareStatement(SQL_TRAIN_CODE_INS);
                 PreparedStatement psLineStyleIns = cn.prepareStatement(SQL_SPAN_STYLE_INS);
                 PreparedStatement psSpanInscrIns = cn.prepareStatement(SQL_SPAN_INSCR_INS);
                 PreparedStatement psSpanColorIns = cn.prepareStatement(SQL_SPAN_COLOR_INS)) {
                int batchedShowParam = 0;
                int batchedHideSpan = 0;
                int batchedTrainCode = 0;
                int batchedLineStyleSpan = 0;
                int batchedSpanInscr = 0;
                int batchedSpanColor = 0;
                for (DistrTrain train : trains) {
                    // ��, ��� ������ �� ��������, �� �������� ��������� �����������!!!
                    if (!train.isOnlyStation()) {
                        if (trainIsForeign(train, idDistr)) {
                            // ��������� ����������� � ������� Distr_Train_ShowParam
                            // (������ ��� ������������)
                            batchedShowParam += prepareBatchDistrTrainShowParam(train, idDistr, psShowParamIns);
                        }
                        // Train_HideSpan
                        batchedHideSpan += prepareBatchDistrTrainHideSpan(train, psHideSpanIns);
                        // TrainCode
                        batchedTrainCode += prepareBatchDistrTrainCode(train, psTrainCodeIns);
                        // Train_LineStyleSpan
                        batchedLineStyleSpan += prepareBatchDistrTrainLineStyleSpan(train, psLineStyleIns);
                        // Train_SpanInscr
                        batchedSpanInscr += prepareBatchDistrTrainSpanInscr(train, psSpanInscrIns);
                        // Train_SpanColor
                        batchedSpanColor += prepareBatchDistrTrainSpanColor(train, psSpanColorIns);
                    }
                }
                if (batchedShowParam > 0) {
                    psShowParamIns.executeBatch();
                }
                if (batchedHideSpan > 0) {
                    psHideSpanIns.executeBatch();
                }
                if (batchedTrainCode > 0) {
                    psTrainCodeIns.executeBatch();
                }
                if (batchedLineStyleSpan > 0) {
                    psLineStyleIns.executeBatch();
                }
                if (batchedSpanInscr > 0) {
                    psSpanInscrIns.executeBatch();
                }
                if (batchedSpanColor > 0) {
                    psSpanColorIns.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        long tm2 = Calendar.getInstance().getTimeInMillis();
        logger.debug("saveListTrainGDP_ShowParam: " + ((double) (tm2 - tm1)) / 1000.0 + " sec");
    }

    @Override
    public void saveTrainGDP_ShowParam(DistrTrain train, int idDistr) throws DataManageException {
        // ��, ��� ������ �� ��������, �� �������� ��������� �����������!!!
        if (!train.isOnlyStation()) {
            long tm1 = Calendar.getInstance().getTimeInMillis();
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement psShowParamIns = cn.prepareStatement(SQL_SHOWPARAM_INS);
                     PreparedStatement psHideSpanIns = cn.prepareStatement(SQL_HIDE_SPAN_INS);
                     PreparedStatement psTrainCodeIns = cn.prepareStatement(SQL_TRAIN_CODE_INS);
                     PreparedStatement psLineStyleIns = cn.prepareStatement(SQL_SPAN_STYLE_INS);
                     PreparedStatement psSpanInscrIns = cn.prepareStatement(SQL_SPAN_INSCR_INS);
                     PreparedStatement psSpanColorIns = cn.prepareStatement(SQL_SPAN_COLOR_INS)) {
                    int batchedShowParam = 0;
                    int batchedHideSpan = 0;
                    int batchedTrainCode = 0;
                    int batchedLineStyleSpan = 0;
                    int batchedSpanInscr = 0;
                    int batchedSpanColor = 0;
                    if (trainIsForeign(train, idDistr)) {
                        // ��������� ����������� � ������� Distr_Train_ShowParam
                        // (������ ��� ������������)
                        batchedShowParam += prepareBatchDistrTrainShowParam(train, idDistr, psShowParamIns);
                    }
                    // Train_HideSpan
                    batchedHideSpan += prepareBatchDistrTrainHideSpan(train, psHideSpanIns);
                    // TrainCode
                    batchedTrainCode += prepareBatchDistrTrainCode(train, psTrainCodeIns);
                    // Train_LineStyleSpan
                    batchedLineStyleSpan += prepareBatchDistrTrainLineStyleSpan(train, psLineStyleIns);
                    // Train_SpanInscr
                    batchedSpanInscr += prepareBatchDistrTrainSpanInscr(train, psSpanInscrIns);
                    // Train_SpanColor
                    batchedSpanColor += prepareBatchDistrTrainSpanColor(train, psSpanColorIns);

                    if (batchedShowParam > 0) {
                        psShowParamIns.executeBatch();
                    }
                    if (batchedHideSpan > 0) {
                        psHideSpanIns.executeBatch();
                    }
                    if (batchedTrainCode > 0) {
                        psTrainCodeIns.executeBatch();
                    }
                    if (batchedLineStyleSpan > 0) {
                        psLineStyleIns.executeBatch();
                    }
                    if (batchedSpanInscr > 0) {
                        psSpanInscrIns.executeBatch();
                    }
                    if (batchedSpanColor > 0) {
                        psSpanColorIns.executeBatch();
                    }
                } catch (SQLException e) {
                    logger.error(e.toString(), e);
                }
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
            long tm2 = Calendar.getInstance().getTimeInMillis();
            logger.debug("saveTrainGDP_ShowParam: " + ((double) (tm2 - tm1)) / 1000.0 + " sec");
        }
    }

    @Override
    public void saveListTrainGDP(Collection<DistrTrain> trains, int idDistr) throws DataManageException {
        long tm1 = Calendar.getInstance().getTimeInMillis();
        try {
            Connection cn = cm.getConnection();
            // delete from LineSt_Train, TrainStop
            try (PreparedStatement psLSTTDel = cn.prepareStatement(SQL_LSTT_DEL);
                 PreparedStatement psTrainStopDel = cn.prepareStatement(SQL_TRAIN_STOP_DEL)) {
                int batchedLSTT = 0;
                int batchedTrainStop = 0;
                for (DistrTrain train : trains) {
                    // ���������� �� �.�.
                    batchedLSTT += prepareBatchDelLineSt_Train(train, train.getIdDistr(), psLSTTDel);
                    // ���������� �� �.�.
                    batchedTrainStop += prepareBatchDelTrainStop(train, train.getIdDistr(), psTrainStopDel);
                }
                if (batchedLSTT > 0) {
                    psLSTTDel.executeBatch();
                }
                if (batchedTrainStop > 0) {
                    psTrainStopDel.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }

            // insert to LineSt_Train, TrainStop, DistrTrain, SpTOcc
            try (PreparedStatement psLSTTIns = cn.prepareStatement(SQL_LSTT_INS);
                 PreparedStatement psTrainStopIns = cn.prepareStatement(SQL_TRAIN_STOP_INS);
                 PreparedStatement psDistrTrainIns = cn.prepareStatement(SQL_DISTR_TRAIN_INS);
                 PreparedStatement psDistrTrainInsX = cn.prepareStatement(SQL_DISTR_TRAIN_INS_X);
                 PreparedStatement psSpTOccIns = cn.prepareStatement(SQL_SPTOCC_INS))
            {
                int batchedLSTT = 0;
                int batchedTrainStop = 0;
                int batchedDistrTrain = 0;
                int batchedSpTOcc = 0;
                for (DistrTrain train : trains) {
                    // ���������� �� �.�.
                    batchedLSTT += prepareBatchInsLineSt_Train(train, idDistr, psLSTTIns);
                    // ���������� �� �.�.
                    batchedTrainStop += prepareBatchInsTrainStop(train, idDistr, psTrainStopIns);
                    if (!trainIsForeign(train, idDistr)) {
                        // �������� � �������
                        if (train.gettOn() == null || train.gettOn().isEmpty()) {
                            // �� ������, ���� tOff= "" tOn= "" tOn_e= "" tOff_e= ""
                            batchedDistrTrain += prepareBatchInsDistrTrain(train, idDistr, psDistrTrainInsX);
                        } else {
                            batchedDistrTrain += prepareBatchInsDistrTrain(train, idDistr, psDistrTrainIns);
                        }
                        // ��������� ���������
                        batchedSpTOcc += prepareBatchInsSpTOcc(train, idDistr, psSpTOccIns);
                    }
                }
                if (batchedLSTT > 0) {
                    psLSTTIns.executeBatch();
                }
                if (batchedTrainStop > 0) {
                    psTrainStopIns.executeBatch();
                }
                if (batchedDistrTrain > 0) {
                    psDistrTrainIns.executeBatch();
                }
                if (batchedSpTOcc > 0) {
                    psSpTOccIns.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " saveListTrainGDP ", e);
            }
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " saveListTrainGDP ", e);
        }
        long tm2 = Calendar.getInstance().getTimeInMillis();
        logger.debug("saveListTrainGDP: " + ((double) (tm2 - tm1)) / 1000.0 + " sec");
    }

    private int prepareBatchDelLineSt_Train(DistrTrain train, int idDistr, PreparedStatement psLSTTDel) throws DataManageException {
        // ������� ���������� "�����" ������� �� ���������� �������,
        // ������ ��� �� �� ����� ����� ������� � ����� ����
        int res = 0;
        if (trainIsForeign(train, idDistr)) {
            try {
                for (LineStTrain lst : train.getListLineStTrain()) {
                    psLSTTDel.setInt(1, lst.getIdSt());
                    psLSTTDel.setLong(2, lst.getIdTrain());
                    psLSTTDel.addBatch();
                    res++;
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " prepareBatchDelLineSt_Train " +
                        train.getCodeTR() + "(" + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    private int prepareBatchDelTrainStop(DistrTrain train, int idDistr, PreparedStatement psTrainStopDel) throws DataManageException {
        // ������� ���������� "�����" ������� �� ���������� ��������,
        // ������ ��� �� �� ����� ����� ������� � ����� ����
        int res = 0;
        if (trainIsForeign(train, idDistr)) {
            try {
                for (TrainStop trStop : train.getTrainStops()) {
                    // �������� ��� "�����"
                    psTrainStopDel.setInt(1, trStop.getIdSpanst());
                    psTrainStopDel.setLong(2, train.getIdTrain());
                    psTrainStopDel.addBatch();
                    res++;
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " prepareBatchDelTrainStop " +
                        train.getCodeTR() + "(" + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    private int prepareBatchInsLineSt_Train(DistrTrain train, int idDistr, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        try {
            for (LineStTrain lst : train.getListLineStTrain()) {
                String tOn = null;
                String tOff = null;
                if (!lst.isNullOn()) {
                    tOn = lst.getOccupyOn();
                }
                if (!lst.isNullOff()) {
                    tOff = lst.getOccupyOff();
                }
                if (tOn != null || tOff != null) {
                    // ���������� ���������� �� �.�.
                    int i = 1;
                    psIns.setString(i++, String.valueOf(lst.getIdTrain()));
                    psIns.setInt(i++, lst.getIdSt());
                    psIns.setInt(i++, train.getIdDistr());
                    psIns.setInt(i++, lst.getIdLineSt());

                    if (tOn != null) {
                        psIns.setTime(i++, TimeConverter.formatTime(tOn));
                    } else {
                        psIns.setNull(i++, Types.TIME);
                    }
                    if (tOff != null) {
                        psIns.setTime(i++, TimeConverter.formatTime(tOff));
                    } else {
                        psIns.setNull(i++, Types.TIME);
                    }

                    psIns.setInt(i++, lst.getTstop());
                    psIns.setInt(i++, lst.getTexStop());
                    psIns.setString(i++, lst.getSk_ps());
                    psIns.setInt(i++, lst.getOverTime());
                    psIns.setInt(i++, lst.getTstopPL());
                    psIns.setInt(i++, lst.getStop());

                    psIns.addBatch();
                    res++;
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " prepareBatchInsLineSt_Train " +
                    train.getCodeTR() + "(" + train.getNameTR() + ")", e);
        }
        return res;
    }

    private int prepareBatchInsTrainStop(DistrTrain train, int idDistr, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        Collection<TrainStop> list = train.getTrainStops();
        if (list != null) {
            try {
                for (TrainStop trStop : list) {
                    // ���������� ���������� �� �.�.
                    psIns.setLong(1, train.getIdTrain());
                    psIns.setInt(2, trStop.getIdSpanst());
                    psIns.setInt(3, trStop.getTstop());
                    psIns.setTime(4, TimeConverter.formatTime(trStop.getOccupyOn()));
                    psIns.setTime(5, TimeConverter.formatTime(trStop.getOccupyOff()));
                    psIns.setString(6, trStop.getSk_ps());
                    psIns.setInt(7, idDistr);
                    //psIns.setInt(7, train.getIdDistr());

                    psIns.addBatch();
                    res++;
                }

            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " prepareBatchInsTrainStop " +
                        train.getCodeTR() + "(" + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    private int prepareBatchInsDistrTrain(DistrTrain train, int idDistr, PreparedStatement psIns) throws DataManageException {
        int res = 0;
        DistrTrain dtr = train;
        try {
            int i = 1;
            psIns.setLong(i++, train.getIdTrain());
            psIns.setInt(i++, idDistr);

            if (dtr.getOccupyOff() != null) {
                psIns.setTime(i++, TimeConverter.formatTime(dtr.getOccupyOff()));
            } else {
                psIns.setNull(i++, Types.TIME);
            }

            String oe = train.getOe().getString();
            psIns.setString(i++, oe);
            if (!(train.gettOn() == null || train.gettOn().isEmpty())) {
                psIns.setTime(i++, TimeConverter.formatTime(dtr.gettOn()));
                psIns.setTime(i++, TimeConverter.formatTime(dtr.gettOff()));
                psIns.setTime(i++, TimeConverter.formatTime(dtr.gettOn_e()));
                psIns.setTime(i++, TimeConverter.formatTime(dtr.gettOff_e()));
            }

            // ���� ����� ��� ������������� �� ������ ����������
            // ��������� ����������� ��� '�����' ����� ��������� � Distr_Train �� ��� ���, ���� �� ��������� �� ������ ���������
            DistrTrainShowParam param = dtr.getShowParam();

            psIns.setBoolean(i++, param.isHidden());
            psIns.setBoolean(i++, true);
            psIns.setInt(i++, param.getColorTR());
            psIns.setString(i++, param.getAinscr().trim());
            psIns.setInt(i++, param.getLineStyle());
            psIns.setInt(i++, param.getWidthTR());
            if (!(train.gettOn() == null || train.gettOn().isEmpty())) {
                psIns.setInt(i++, dtr.getIdLtype());
            }

            psIns.addBatch();
            res++;

        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + " prepareBatchInsDistrTrain " +
                    train.getCodeTR() + "(" + train.getNameTR() + ")", e);
        }
        return res;
    }

    private int prepareBatchInsSpTOcc(DistrTrain train, int idDistr, PreparedStatement psSpTOccIns) throws DataManageException {
        int res = 0;
        Map<Integer, SpTOcc> map = train.getMapSpTOcc();
        Collection<SpTOcc> list = null;
        if (map != null) {
            list = map.values();
        }
        if (list != null) {
            try {
                for (SpTOcc spTOcc : list) {
                    int i = 1;
                    psSpTOccIns.setLong(i++, train.getIdTrain());
                    psSpTOccIns.setInt(i++, spTOcc.getIdSpan());
                    psSpTOccIns.setTime(i++, TimeConverter.formatTime(spTOcc.getOccupyOn()));
                    psSpTOccIns.setTime(i++, TimeConverter.formatTime(spTOcc.getOccupyOff()));
                    psSpTOccIns.setString(i++, spTOcc.getOe());
                    psSpTOccIns.setInt(i++, spTOcc.getTmove());
                    psSpTOccIns.setInt(i++, spTOcc.getNextDay());
                    psSpTOccIns.setInt(i++, idDistr);
                    psSpTOccIns.addBatch();
                    res++;
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR + " prepareBatchInsSpTOcc " +
                        train.getCodeTR() + "(" + train.getNameTR() + ")", e);
            }
        }
        return res;
    }

    @Override
    public void saveTrainGDP(DistrTrain train, int idDistr) throws DataManageException {
        long tm1 = Calendar.getInstance().getTimeInMillis();
        try {
            Connection cn = cm.getConnection();
            // delete from LineSt_Train, TrainStop
            try (PreparedStatement psLSTTDel = cn.prepareStatement(SQL_LSTT_DEL);
                 PreparedStatement psTrainStopDel = cn.prepareStatement(SQL_TRAIN_STOP_DEL)) {
                int batchedLSTT = 0;
                int batchedTrainStop = 0;
                // ���������� �� �.�.
                batchedLSTT += prepareBatchDelLineSt_Train(train, train.getIdDistr(), psLSTTDel);
                // ���������� �� �.�.
                batchedTrainStop += prepareBatchDelTrainStop(train, train.getIdDistr(), psTrainStopDel);
                if (batchedLSTT > 0) {
                    psLSTTDel.executeBatch();
                }
                if (batchedTrainStop > 0) {
                    psTrainStopDel.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }

            // insert to LineSt_Train, TrainStop, DistrTrain, SpTOcc
            try (PreparedStatement psLSTTIns = cn.prepareStatement(SQL_LSTT_INS);
                 PreparedStatement psTrainStopIns = cn.prepareStatement(SQL_TRAIN_STOP_INS);
                 PreparedStatement psDistrTrainIns = cn.prepareStatement(SQL_DISTR_TRAIN_INS);
                 PreparedStatement psDistrTrainInsX = cn.prepareStatement(SQL_DISTR_TRAIN_INS_X);
                 PreparedStatement psSpTOccIns = cn.prepareStatement(SQL_SPTOCC_INS)) {
                int batchedLSTT = 0;
                int batchedTrainStop = 0;
                int batchedDistrTrain = 0;
                int batchedSpTOcc = 0;
                // ���������� �� �.�.
                batchedLSTT += prepareBatchInsLineSt_Train(train, idDistr, psLSTTIns);
                // ���������� �� �.�.
                batchedTrainStop += prepareBatchInsTrainStop(train, idDistr, psTrainStopIns);
                if (!trainIsForeign(train, idDistr)) {
                    // �������� � �������
                    if (train.gettOn() == null || train.gettOn().isEmpty()) {
                        // �� ������, ���� tOff= "" tOn= "" tOn_e= "" tOff_e= ""
                        batchedDistrTrain += prepareBatchInsDistrTrain(train, idDistr, psDistrTrainInsX);
                    } else {
                        batchedDistrTrain += prepareBatchInsDistrTrain(train, idDistr, psDistrTrainIns);
                    }
                    // ��������� ���������
                    batchedSpTOcc += prepareBatchInsSpTOcc(train, idDistr, psSpTOccIns);
                }
                if (batchedLSTT > 0) {
                    psLSTTIns.executeBatch();
                }
                if (batchedTrainStop > 0) {
                    psTrainStopIns.executeBatch();
                }
                if (batchedDistrTrain > 0) {
                    psDistrTrainIns.executeBatch();
                }
                if (batchedSpTOcc > 0) {
                    psSpTOccIns.executeBatch();
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            }

        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        long tm2 = Calendar.getInstance().getTimeInMillis();
        logger.debug("saveTrainGDP: " + ((double) (tm2 - tm1)) / 1000.0 + " sec");
    }

    @Override
    public void saveAllTrainTimeEnergy(Collection<DistrTrain> trains) throws DataManageException {
        if (trains.size() > 0) {
            String sql = "UPDATE Distr_Train "
                    + " SET Tt = ?, Tu = ?, Vt = ?, Vu = ?, L = ? "
                    + " WHERE IDtrain = ? AND IDdistr = ?";
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement ps = cn.prepareStatement(sql)) {
                    for (DistrTrain train : trains) {
                        try {
                            DistrTrain dtr = train;
                            ps.setDouble(1, dtr.getTimeWithoutStop());
                            ps.setDouble(2, dtr.getTimeWithStop());
                            ps.setDouble(3, dtr.getVt());
                            ps.setDouble(4, dtr.getVu());
                            ps.setInt(5, (int) dtr.getLen());
                            ps.setLong(6, dtr.getIdTrain());
                            ps.setInt(7, dtr.getIdDistr());
                            ps.addBatch();
                        } catch (SQLException e) {
                            logger.error(e.toString(), e);
                            throw new DataManageException(DataManageException.MANAGE_ERR + train.getCodeTR() + "("
                                    + train.getNameTR() + ")", e);
                        }
                    }
                    ps.executeBatch();
                } catch (SQLException e) {
                    logger.error(e.toString(), e);
                    throw new DataManageException(DataManageException.MANAGE_ERR, e);
                }
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    @Override
    public void saveDirTrain(Collection<DistrTrain> trains) throws DataManageException {
        if (trains.size() > 0) {
            String sqlDel = "DELETE FROM DirTrain WHERE IDdir = ? AND IDtrain = ? ";
            String sql = "INSERT INTO DirTrain (IDdir, IDdistr, IDtrain, Tu, Tt, L) VALUES (?,?,?,?,?,?) ";
            try {
                Connection cn = cm.getConnection();
                try (PreparedStatement psDel = cn.prepareStatement(sqlDel);
                        PreparedStatement ps = cn.prepareStatement(sql))
                {
                    for (DistrTrain train : trains) {
                        DistrTrain dtr = train;
                        for (DirTrain dirTrain : dtr.getDirTrainList()) {
                            try {
                                psDel.setInt(1, dirTrain.getIdDir());
                                psDel.setLong(2, dirTrain.getIdTrain());
                                psDel.addBatch();
                            } catch (SQLException e) {
                                logger.error(e.toString(), e);
                                throw new DataManageException("delete from DirTrain" + train.getCodeTR() + "("
                                        + train.getNameTR() + ")", e);
                            }
                        }
                    }
                    psDel.executeBatch();

                    for (DistrTrain train : trains) {
                        DistrTrain dtr = train;
                        for (DirTrain dirTrain : dtr.getDirTrainList()) {
                            try {
                                ps.setInt(1, dirTrain.getIdDir());
                                ps.setInt(2, dirTrain.getIdDir());
                                ps.setLong(3, dirTrain.getIdTrain());
                                ps.setDouble(4, dirTrain.getTu());
                                ps.setDouble(5, dirTrain.getTt());
                                ps.setDouble(6, dirTrain.getLen());
                                ps.addBatch();
                            } catch (SQLException e) {
                                logger.error(e.toString(), e);
                                throw new DataManageException("insert into DirTrain" + train.getCodeTR() + "("
                                        + train.getNameTR() + ")", e);
                            }
                        }
                    }
                    ps.executeBatch();
                } catch (SQLException e) {
                    logger.error(e.toString(), e);
                    throw new DataManageException(DataManageException.MANAGE_ERR, e);
                }
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    private void fillTrainCombSt(TrainCombSt trc, ResultSet rs) throws DataManageException {
        try {
            trc.IDst = rs.getInt("IDst");
            trc.IDtrainOn = rs.getLong("IDtrainOn");
            trc.IDtrainOff = rs.getLong("IDtrainOff");
            trc.IDdistrOn = rs.getInt("IDdistrOn");
            trc.IDdistrOff = rs.getInt("IDdistrOff");
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    @Override
    public List<TrainCombSt> getListTrainCombSt(int idSt) throws DataManageException {
        ArrayList<TrainCombSt> list = new ArrayList<>();
        TrainCombSt trc = null;
        String sql = "SELECT IDst, IDtrainOn, IDtrainOff, IDdistrOn, IDdistrOff "
                + "FROM TrainCombSt  WHERE IDst = " + idSt;

        try {
            Connection cn = cm.getConnection();

            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    trc = new TrainCombSt();
                    fillTrainCombSt(trc, rs);
                    list.add(trc);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    @Override
    public String getSourceGDP(int idDistr) throws DataManageException {
        String sourceGDP = "";
        String sql = "SELECT GDPsource " + " FROM DistrParam  WHERE IDdistr = " + idDistr;

        try {
            Connection cn = cm.getConnection();

            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    sourceGDP = rs.getString(1);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return sourceGDP;
    }

    @Override
    public void updateSourceGDP(int idDistr, String sourceGDP) throws DataManageException {
        String sql = "SELECT GDPsource " + " FROM DistrParam  WHERE IDdistr = " + idDistr;
        String sqlUpdate = "UPDATE DistrParam SET GDPsource = ? WHERE IDdistr = ?";
        String sqlInsert = "INSERT INTO DistrParam (IDdistr, GDPsource) VALUES( ?, ?)";

        try {
            Connection cn = cm.getConnection();
            boolean update;
            try (Statement st = cn.createStatement();
                 ResultSet rs = st.executeQuery(sql)) {
                update = rs.next();
            }
            if (update) {
                try (PreparedStatement pst = cn.prepareStatement(sqlUpdate)) {
                    pst.setString(1, sourceGDP);
                    pst.setInt(2, idDistr);
                    pst.executeUpdate();
                }
            } else {
                try (PreparedStatement pst = cn.prepareStatement(sqlInsert)) {
                    pst.setInt(1, idDistr);
                    pst.setString(2, sourceGDP);
                    pst.executeUpdate();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private DistrTrain getTrainByCodeName(int code, String name, Connection cn) throws DataManageException {
        DistrTrain tr = null;
        String sql = "SELECT IDtrain, type FROM Train WHERE code = ? AND name = ? ";
        try (PreparedStatement ps = cn.prepareStatement(sql);) {
            ps.setInt(1, code);
            ps.setString(2, name);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    tr = new DistrTrain();
                    tr.setIdTrain(rs.getLong("IDtrain"));
                    tr.setTypeTR(rs.getString("type").trim());
                    tr.setCodeTR(String.valueOf(code));
                    tr.setNameTR(name);
                    tr.setOe(OE.getValue(code));
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
        return tr;
    }

    @Override
    public DistrTrain newTrain(int code, String name) throws DataManageException {
        DistrTrain tr = null;
        try {
            Connection cn = cm.getConnection();
            // ���� ���������
            tr = getTrainByCodeName(code, name, cn);
            // ����������� ������ �� ���������
            Category category = defineCategory(code);
            if (category != null) {
                if (tr == null) {
                    String oe = OE.getValue(code).getString();
                    String sql = "INSERT INTO Train (IDcat, code, Name, prioritet_, type, oe) VALUES (?,?,?,?,?,?)";
                    try (PreparedStatement ps = cn.prepareStatement(sql)) {
                        ps.setInt(1, category.getIdCat());
                        ps.setInt(2, code);
                        ps.setString(3, name);
                        ps.setInt(4, category.getPriority());
                        ps.setString(5, category.getType().getString());
                        ps.setString(6, oe);
                        ps.executeUpdate();
                    }
                    tr = getTrainByCodeName(code, name, cn);
                }
                if (tr != null) {
                    tr.setIdCat(category.getIdCat());
                    DistrTrainShowParam sp = tr.getShowParam();
                    sp.setHidden(false);
                    sp.setShowCodeI(false);
                    sp.setColorTR(ColorConverter.convert(category.getColor()));
                    sp.setAinscr(null);
                    sp.setLineStyle(0);
                    sp.setWidthTR(1);
                }
            } else {
                logger.error("Category was not found: " + code);
            }

        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return tr;
    }

    private Category defineCategory(int code) throws DataManageException {
        ICategoryDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO();
        List<Category> categories = dao.getCategories();
        return CategoryUtil.defineCategory(categories, code);
    }

    @Override
    public void saveAllTrainStDouble(int idSt, Collection<TrainCombSt> trainCombSts)
            throws DataManageException {
        try {
            Connection cn = cm.getConnection();
            cn.setAutoCommit(true);
            deleteTrainCombSt(idSt, cn);
            for (TrainCombSt trCombSt : trainCombSts) {
                saveTrainCombSt(trCombSt, cn);
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
    }

    private void deleteTrainCombSt(int idSt, Connection cn) throws DataManageException {
        String sql = "DELETE FROM TrainCombSt WHERE IDst = " + idSt;
        try (Statement st = cn.createStatement()) {
            st.executeUpdate(sql);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR + idSt, e);
        }
    }

    private void saveTrainCombSt(TrainCombSt trCombSt, Connection cn) throws DataManageException {
        try (Statement st = cn.createStatement();) {
            StringBuilder sql = new StringBuilder("INSERT INTO TrainCombSt(IDst, IDtrainOn, IDtrainOff, IDdistrOn, IDdistrOff)");
            sql.append(" VALUES( ");
            sql.append(trCombSt.IDst).append(" , ");
            sql.append(trCombSt.IDtrainOn).append(" , ");
            sql.append(trCombSt.IDtrainOff).append(" , ");
            sql.append(trCombSt.IDdistrOn).append(" , ");
            sql.append(trCombSt.IDdistrOff);
            sql.append(")");
            String sss = sql.toString();
            st.executeUpdate(sss);
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        }
    }

    private boolean trainIsForeign(DistrTrain train, int idDistr) {
        return train.getIdDistr() != idDistr;
    }
}
