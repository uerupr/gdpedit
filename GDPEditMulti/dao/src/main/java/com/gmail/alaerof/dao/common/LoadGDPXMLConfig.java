package com.gmail.alaerof.dao.common;

public class LoadGDPXMLConfig {
    /** ����� �� ����� xml '����' */
    public boolean loadWind;
    /** ����� �� ����� xml �������������� ������� ���� */
    public boolean loadSpanTrain;
    /**
     * ����� �� ����� xml ��������� ����������� �������:
     * - ��������� ����� � �����
     * - ����������
     * - ������� ����������� ������� �� ������� � ������� ���������
     * - �������/����� �����
     * - ������������ �������
     */
    public boolean loadDistrAppearance;
    /** ����� �� ����� xml �������� ��� ���������� ������� */
    public boolean loadRoutes = true;
    /** ����� �� ����� xml ������� ���� �� ��������� */
    public boolean loadSpanTime = true;
    /** ����� �� ����� xml ������� ���� ����� �.�. */
    public boolean loadSpanStTime = true;
    /** �������� ��� */
    public String sourceGDP = "";
}
