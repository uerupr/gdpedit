package com.gmail.alaerof.dao.interfacedao.exceptions;

/**
 * @author Helen Yrofeeva
 */
public class DataManageException extends Exception {
    public static final String MANAGE_ERR = "Data manage ERROR ";

    public DataManageException(String msg, Throwable t) {
        super(msg, t);
    }
}
