package com.gmail.alaerof.dao.dto;

import java.util.List;

/**
 * ������� (���������� �����)
 * @author Helen Yrofeeva
 * 
 */
public class Station {
    public static final String TYPE_TECH = "���.";
    public static final String TYPE_SP = "�.�.";

    private int idSt; // IDst
    private String codeExpress = ""; // CodeExpress
    private String codeESR = ""; // CodeESR
    private String codeESR2 = ""; // CodeESR2
    private String name = "";
    private String type = ""; // ���. // �.�.
    private int passBld; // PassBld
    private int countLineO; // countLine_o
    private int countLineE; // countLine_e
    private boolean br; // BR
    private boolean joint; // ������� �����
    private int idDir; // IDdir
    private List<LineSt> listLine;
    /** ����������� ��������� ����� ������ ��� ��������� ����� */
    private int maxMassO; // MaxMass_o
    /** ����������� ��������� ����� ������ ��� ��������� ��� */
    private int maxMassE; // MaxMass_o

    public String getCodeESR() {
        return codeESR;
    }

    public void setCodeESR(String codeESR) {
        if (codeESR != null) {
            this.codeESR = codeESR;
        }
    }

    public String getCodeExpress() {
        return codeExpress;
    }

    public void setCodeExpress(String codeExpress) {
        if (codeExpress != null) {
            this.codeExpress = codeExpress;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPassBld() {
        return passBld;
    }

    public void setPassBld(int passBld) {
        this.passBld = passBld;
    }

    public int getCountLineO() {
        return countLineO;
    }

    public void setCountLineO(int countLineO) {
        this.countLineO = countLineO;
    }

    public int getCountLineE() {
        return countLineE;
    }

    public void setCountLineE(int countLineE) {
        this.countLineE = countLineE;
    }

    public boolean isBr() {
        return br;
    }

    public void setBr(boolean br) {
        this.br = br;
    }

    public boolean isJoint() {
        return joint;
    }

    public void setJoint(boolean joint) {
        this.joint = joint;
    }

    public String getCodeESR2() {
        return codeESR2;
    }

    public void setCodeESR2(String codeESR2) {
        if (codeESR2 != null) {
            this.codeESR2 = codeESR2;
        }
    }

    public int getIdSt() {
        return idSt;
    }

    public void setIdSt(int idSt) {
        this.idSt = idSt;
    }

    public int getIdDir() {
        return idDir;
    }

    public void setIdDir(int isDir) {
        this.idDir = isDir;
    }

    public int getMaxMassO() {
        return maxMassO;
    }

    public void setMaxMassO(int maxMassO) {
        this.maxMassO = maxMassO;
    }

    public int getMaxMassE() {
        return maxMassE;
    }

    public void setMaxMassE(int maxMassE) {
        this.maxMassE = maxMassE;
    }

    @Override
    public String toString() {
        return "Station [idSt=" + idSt + ", codeESR=" + codeESR + ", codeExpress=" + codeExpress + ", name="
                + name + "]";
    }

    public List<LineSt> getListLine() {
        return listLine;
    }

    public void setListLine(List<LineSt> listLine) {
        this.listLine = listLine;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codeESR == null) ? 0 : codeESR.hashCode());
        result = prime * result + ((codeESR2 == null) ? 0 : codeESR2.hashCode());
        result = prime * result + ((codeExpress == null) ? 0 : codeExpress.hashCode());
        result = prime * result + idSt;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Station other = (Station) obj;
        if (codeESR == null) {
            if (other.codeESR != null)
                return false;
        } else if (!codeESR.equals(other.codeESR))
            return false;
        if (codeESR2 == null) {
            if (other.codeESR2 != null)
                return false;
        } else if (!codeESR2.equals(other.codeESR2))
            return false;
        if (codeExpress == null) {
            if (other.codeExpress != null)
                return false;
        } else if (!codeExpress.equals(other.codeExpress))
            return false;
        if (idSt != other.idSt)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}
