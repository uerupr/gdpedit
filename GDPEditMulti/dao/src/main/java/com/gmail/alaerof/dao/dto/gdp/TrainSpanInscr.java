package com.gmail.alaerof.dao.dto.gdp;
/**
 */
public class TrainSpanInscr {
    private long idTrain; // IDtrain
    private int idDistr; // IDdistr
    /** ��� �������, ����� ������� ��������� ����� ������ */
    private int idStB; // IDstB
    private String AInscr = ""; // ������� ��� ������

    public long getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }

    public int getIdDistr() {
        return idDistr;
    }

    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }

    public int getIdStB() {
        return idStB;
    }

    public void setIdStB(int idStB) {
        this.idStB = idStB;
    }

    public String getAInscr() {
        return AInscr;
    }

    public void setAInscr(String aInscr) {
        if (aInscr != null) {
            AInscr = new String(aInscr.trim());
        }
    }

}
