package com.gmail.alaerof.dao.sql;

import com.gmail.alaerof.util.FileUtil;
import java.io.File;

import java.util.Properties;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.gmail.alaerof.util.ConnectionDBFileHandlerUtil;

/**
 */
public class SelectDB {
    public static final String JDBC_ODBC = "sun.jdbc.odbc.JdbcOdbcDriver";
    public static final String JDBC_ODBC_URL = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=";
    public static final String JDBC_UCANACCESS_URL = "jdbc:ucanaccess://";

    public static boolean selectDB(JFrame frame) {
        boolean res = false;
        JFileChooser chooser = null;
        if (FileUtil.lastFileChooserPath != null) {
            chooser = new JFileChooser(FileUtil.lastFileChooserPath);
        } else {
            chooser = new JFileChooser();
        }
        FileNameExtensionFilter filter = new FileNameExtensionFilter("exe files", "exe");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(frame);
        File file = null;
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
            if (!"GraphicServ.exe".equals(file.getName())) {
                JOptionPane.showMessageDialog(frame, "��������� ������� ���� GraphicServ.exe",
                        "��������������", JOptionPane.WARNING_MESSAGE);
            } else {
                String pth = file.getPath();
                String nm = file.getName();
                pth = pth.substring(0, pth.length() - nm.length()) + "Data" + File.separator;
                Properties properties = ConnectionDBFileHandlerUtil.readGDPEditINI();
                String nameS = properties.getProperty("MDB-SERVER");
                String nameL = properties.getProperty("MDB-LOCAL");
                pth = pth.replace('\\','/');
                if (nameS != null && nameL != null) {
                    // ����� ������� �������� �������� ����� ���������� jdbc.odbc ��� ucanaccess
                    Properties poolInfo = ConnectionDBFileHandlerUtil.loadPoolInfo(GDPDAOFactorySQL.BASE_NAME_ACCESS);
                    String driver = poolInfo.getProperty("driver");
                    // ������ ���� ��� � ����������� �� ��������
                    String urlS = buildURL(driver, pth, nameS);
                    String urlL = buildURL(driver, pth, nameL);
                    String[] dbaccess = {"driver=" + driver, "url=" + urlS, "pathMDB=" + pth};
                    ConnectionDBFileHandlerUtil.savePoolInfo("dbaccess.properties", dbaccess);
                    String[] dblocal = {"driver=" + driver, "url=" + urlL, "pathMDB=" + pth};
                    ConnectionDBFileHandlerUtil.savePoolInfo("dblocal.properties", dblocal);
                    res = true;
                }
            }
        }
        return res;
    }

    private static String buildURL(String driver, String pth, String nameMDB) {
        String url = "";
        if (JDBC_ODBC.equals(driver)) {
            url = JDBC_ODBC_URL + pth + nameMDB;
        } else {
            url = JDBC_UCANACCESS_URL + pth + nameMDB + ";memory=true;SingleConnection=true;";
        }
        return url;
    }
}
