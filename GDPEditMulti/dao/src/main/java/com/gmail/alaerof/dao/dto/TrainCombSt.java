package com.gmail.alaerof.dao.dto;
/**
 */
public class TrainCombSt {
    // �������� �� ��
    /** �� ������� */
    public int IDst;
    /** �� ������ �������� */
    public long IDtrainOn;
    /** �� ������ ����������� */
    public long IDtrainOff;
    /** �� ������� �������� */
    public int IDdistrOn;
    /** �� ������� ����������� */
    public int IDdistrOff;

    // ������������� ��� �������� ������ �� ���� �� �������
    /** ����� ������� ���� ������� ������� (00:00) */
    public String occupyOn; 
    /** ����� ������������ ���� ������� ������� (00:00) */
    public String occupyOff; 
    /** ����� ������� ��������� */
    public int tstop;
    /** ��� ���� */
    public int idLineSt;
    
    @Override
    public String toString() {
        return "TrainCombSt [IDst=" + IDst + ", IDtrainOn=" + IDtrainOn + ", IDtrainOff=" + IDtrainOff
                + ", IDdistrOn=" + IDdistrOn + ", IDdistrOff=" + IDdistrOff + ", occupyOn=" + occupyOn
                + ", occupyOff=" + occupyOff + ", tstop=" + tstop + "]";
    }
    
    
}
