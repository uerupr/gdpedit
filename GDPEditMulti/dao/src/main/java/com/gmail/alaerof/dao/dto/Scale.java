package com.gmail.alaerof.dao.dto;

import java.util.List;

/**
 * ������� �� �������
 * @author Helen Yrofeeva
 * 
 */
public class Scale implements Comparable<Scale> {
    /* Span */
    private int idSpan; // IDspan
    private String name = ""; // Name
    private String scb = ""; // SCB
    private int line; // ����� ����� �� ��������
    private int idSt; // IDst
    private int idStE; // IDst_e
    private boolean br; // BR �������������� ��
    private int idDir; // IDdir
    /* Scale */
    private int idScale; // IDscale
    private int idDistr; // IDdistr
    private int num;
    private int scale;
    private boolean fSt;
    private boolean lSt;
    private boolean openStf; // OpenStf
    private boolean openStl; // OpenStl
    private int widthLine_fst; // WidthLine_fst
    private int widthLine_lst; // WidthLine_lst
    private int scale_lst; // scale_lst
    private float absKM;
    private float absKM_E; // absKM_e
    private int km;
    private int pic;
    private int m;
    private int kmE; // km_e
    private int picE; // pic_e
    private int mE; // m_e
    private int l; // L, ����������, �
    private List<SpanSt> spanSt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public String getScb() {
        return scb;
    }

    public void setScb(String scb) {
        if (scb != null) {
            this.scb = scb;
        }
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public boolean isBr() {
        return br;
    }

    public void setBr(boolean br) {
        this.br = br;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public boolean isfSt() {
        return fSt;
    }

    public void setfSt(boolean fSt) {
        this.fSt = fSt;
    }

    public boolean islSt() {
        return lSt;
    }

    public void setlSt(boolean lSt) {
        this.lSt = lSt;
    }

    public boolean isOpenStf() {
        return openStf;
    }

    public void setOpenStf(boolean openStf) {
        this.openStf = openStf;
    }

    public boolean isOpenStl() {
        return openStl;
    }

    public void setOpenStl(boolean openStl) {
        this.openStl = openStl;
    }

    public int getScale_lst() {
        return scale_lst;
    }

    public void setScale_lst(int scale_lst) {
        this.scale_lst = scale_lst;
    }

    public float getAbsKM() {
        return absKM;
    }

    public void setAbsKM(float absKM) {
        this.absKM = absKM;
    }

    public float getAbsKM_E() {
        return absKM_E;
    }

    public void setAbsKM_E(float absKM_E) {
        this.absKM_E = absKM_E;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getKmE() {
        return kmE;
    }

    public void setKmE(int kmE) {
        this.kmE = kmE;
    }

    public int getPicE() {
        return picE;
    }

    public void setPicE(int picE) {
        this.picE = picE;
    }

    public int getmE() {
        return mE;
    }

    public void setmE(int mE) {
        this.mE = mE;
    }
    /**
     * ����� �������� � ������
     * @return ����� �������� � ������
     */
    public int getL() {
        return l;
    }

    public void setL(int l) {
        this.l = l;
    }

    public int getIdSpan() {
        return idSpan;
    }

    public void setIdSpan(int idSpan) {
        this.idSpan = idSpan;
    }

    public int getIdSt() {
        return idSt;
    }

    public void setIdSt(int idSt) {
        this.idSt = idSt;
    }

    public int getIdStE() {
        return idStE;
    }

    public void setIdStE(int idStE) {
        this.idStE = idStE;
    }

    public int getIdDir() {
        return idDir;
    }

    public void setIdDir(int idDir) {
        this.idDir = idDir;
    }

    public int getIdScale() {
        return idScale;
    }

    public void setIdScale(int idScale) {
        this.idScale = idScale;
    }

    public int getIdDistr() {
        return idDistr;
    }

    public void setIdDistr(int idDistr) {
        this.idDistr = idDistr;
    }

    public int getWidthLine_fst() {
        return widthLine_fst;
    }

    public void setWidthLine_fst(int widthLine_fst) {
        this.widthLine_fst = widthLine_fst;
    }

    public int getWidthLine_lst() {
        return widthLine_lst;
    }

    public void setWidthLine_lst(int widthLine_lst) {
        this.widthLine_lst = widthLine_lst;
    }

    public List<SpanSt> getSpanSt() {
        return spanSt;
    }

    public void setSpanSt(List<SpanSt> spanSt) {
        this.spanSt = spanSt;
    }
    
    @Override
    public int compareTo(Scale sc) {
        return this.num - sc.num;
    }
    
    @Override
    public String toString() {
        return "Scale [idSpan=" + idSpan + ", name=" + name 
                + ", idSt=" + idSt + ", idStE=" + idStE + ", br="
                + br + ", num=" + num  
                + ", fSt=" + fSt + ", lSt=" + lSt + ", scale=" + scale + ", scale_lst="
                + scale_lst + ", absKM=" + absKM + ", absKM_E=" + absKM_E + ", l=" + l + "]";
    }
}
