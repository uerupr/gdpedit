package com.gmail.alaerof.dao.interfacedao;

import java.util.Collection;
import java.util.List;

import com.gmail.alaerof.dao.dto.TrainCombSt;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;

/**
 */
public interface IDistrTrainDAO {

    /**
     *
     * @param distrTrainList
     * @return
     * @throws DataManageException
     */
    void updateTrainCategories(List<DistrTrain> distrTrainList) throws DataManageException;

    /**
     *
     * @param distrTrainList
     * @return
     * @throws DataManageException
     */
    void updateTrainPriorities(List<DistrTrain> distrTrainList) throws DataManageException;

    /**
     *
     * @param trainIDList
     * @return
     * @throws DataManageException
     */
    void deleteTrains(List<Long> trainIDList) throws DataManageException;
    /**
     *
     * @return
     * @throws DataManageException
     */
    List<DistrTrain> geTrains() throws DataManageException;
    /**
     *
     * @param idDistr
     * @return
     * @throws DataManageException
     */
    List<DistrTrain> getDistrTrains(int idDistr) throws DataManageException;

    /**
     *
     * @param idSt
     * @return
     * @throws DataManageException
     */
    List<DistrTrain> getDistrTrainsOnStation(int idSt) throws DataManageException;

    /**
     * �������� ��� ������� �� ��
     * 
     * @param idDistr ��� �������
     * @param trains ������ �������
     * @param idTrain ��� ������
     * @param clearSpanTrain ������� �������� �������������� ������ ����
     * @param delWind ������� �������� ����
     * @throws DataManageException
     */
    void clearDistrGDP(int idDistr, Collection<DistrTrain> trains, long idTrain, boolean clearSpanTrain, boolean delWind)
            throws DataManageException;

    /**
     * �������� �������� ����������� ����� ��� �� ��
     * 
     * @param idDistr   ��� �������
     * @param idTrain   ��� ������
     * @throws DataManageException
     */
    void clearDistrGDP_ShowParam(int idDistr, long idTrain) throws DataManageException;

    /**
     * ���������� �������� ����������� ����� � ��
     * 
     * @param train
     * @param idDistr
     * @throws DataManageException
     */
    void saveTrainGDP_ShowParam(DistrTrain train, int idDistr) throws DataManageException;

    /**
     * ���������� �������� ����������� ����� � ��
     * @param trains
     * @param idDistr
     * @throws DataManageException
     */
    void saveListTrainGDP_ShowParam(Collection<DistrTrain> trains, int idDistr) throws DataManageException;

    /**
     * ���������� ���������� ����� � ��
     * 
     * @param train
     * @param idDistr
     * @throws DataManageException
     */
    void saveTrainGDP(DistrTrain train, int idDistr) throws DataManageException;

    /**
     * ���������� ���������� ������ ����� � ��
     * 
     * @param trains
     * @param idDistr
     * @throws DataManageException
     */
    void saveListTrainGDP(Collection<DistrTrain> trains, int idDistr) throws DataManageException;

    /**
     * �������� ������ ������ ������� �� �������
     * @param idSt
     * @return
     * @throws ObjectNotFoundException
     * @throws DataManageException
     */
    List<TrainCombSt> getListTrainCombSt(int idSt) throws DataManageException;

    /**
     *
     * @param idDistr
     * @return
     * @throws DataManageException
     */
    String getSourceGDP(int idDistr) throws DataManageException;

    /**
     *
     * @param idDistr
     * @param sourceGDP
     * @return
     * @throws DataManageException
     */
    void updateSourceGDP(int idDistr, String sourceGDP) throws DataManageException;

    /**
     *
     * @param code
     * @param name
     * @return
     * @throws DataManageException
     */
    DistrTrain newTrain(int code, String name) throws DataManageException;

    /**
     * ��������� ������ ������ �� �������
     * @param idSt
     * @param trainCombSts
     * @throws DataManageException
     */
    void saveAllTrainStDouble(int idSt, Collection<TrainCombSt> trainCombSts) throws DataManageException;

    /**
     * ��������� Distr_Train ���������� � ��������������� ����������
     * @param trains TrainThread list
     * @throws DataManageException
     */
    void saveAllTrainTimeEnergy(Collection<DistrTrain> trains) throws DataManageException;

    /**
     * ��������� DirTrain ������������
     * @param trains TrainThread list
     * @throws DataManageException
     */
    void saveDirTrain(Collection<DistrTrain> trains) throws DataManageException;
}
