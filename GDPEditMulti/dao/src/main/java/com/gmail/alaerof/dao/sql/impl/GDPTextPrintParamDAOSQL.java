package com.gmail.alaerof.dao.sql.impl;

import com.gmail.alaerof.dao.dto.TextPrintParam;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.IGDPTextPrintParamDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Created by Paul M. Bui on 31.05.2018.
 */
public class GDPTextPrintParamDAOSQL implements IGDPTextPrintParamDAO {
    protected static Logger logger = LogManager.getLogger(GDPTextPrintParamDAOSQL.class);
    private final ConnectionManager cm;

    public GDPTextPrintParamDAOSQL(ConnectionManager cm) {
        this.cm = cm;
    }

    @Override
    public List<TextPrintParam> getListTextPrintParam(int idDistr) throws DataManageException {
        List<TextPrintParam> list = new ArrayList<>();
        TextPrintParam textPrintParam = null;
        String sql = "SELECT IDdistr, tpType, choise, textField, fontField, num FROM GDPTextPrintParam WHERE IDdistr = " + idDistr;
        try {
            Connection cn = cm.getConnection();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            try {
                while (rs.next()) {
                    textPrintParam = new TextPrintParam();
                    textPrintParam.setIdDistr(rs.getInt("IDdistr"));
                    textPrintParam.setTpType(rs.getInt("tpType"));
                    textPrintParam.setChoise(rs.getInt("choise"));
                    textPrintParam.setText(rs.getString("textField"));
                    textPrintParam.setFont(rs.getString("fontField"));
                    textPrintParam.setNum(rs.getInt("num"));
                    list.add(textPrintParam);
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return list;
    }

    @Override
    public void deleteTextPrintParam(int idDistr) throws DataManageException {
        if (idDistr > 0) {
            String sql = "DELETE FROM GDPTextPrintParam WHERE IDdistr = ?";
            try {
                Connection cn = cm.getConnection();
                cn.setAutoCommit(false);
                PreparedStatement st = cn.prepareStatement(sql);
                try {
                    st.setInt(1, idDistr);
                    st.executeUpdate();
                } finally {
                    cn.commit();
                    cn.setAutoCommit(true);
                    if (st != null) {
                        st.close();
                    }
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }

    @Override
    public void addTextPrintParam(List<TextPrintParam> listTextPrintParam) throws DataManageException {
        if (listTextPrintParam != null && listTextPrintParam.size() > 0) {
            String sql = "INSERT INTO GDPTextPrintParam (IDdistr, tpType, choise, textField, fontField, num) VALUES (?,?,?,?,?,?)";
            try {
                Connection cn = cm.getConnection();
                cn.setAutoCommit(false);
                PreparedStatement st = cn.prepareStatement(sql);
                try {
                    for (TextPrintParam textPrintParam : listTextPrintParam) {
                        if (textPrintParam.getIdDistr() > 0) {
                            st.setInt(1, textPrintParam.getIdDistr());
                            st.setInt(2, textPrintParam.getTpType());
                            st.setInt(3, textPrintParam.getChoise());
                            st.setString(4, textPrintParam.getTextField());
                            st.setString(5, textPrintParam.getFontField());
                            st.setInt(6, textPrintParam.getNum());
                            st.executeUpdate();
                            cn.commit();
                        }
                    }
                } finally {
                    cn.setAutoCommit(true);
                    if (st != null) {
                        st.close();
                    }
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
                throw new DataManageException(DataManageException.MANAGE_ERR, e);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
        }
    }
}
