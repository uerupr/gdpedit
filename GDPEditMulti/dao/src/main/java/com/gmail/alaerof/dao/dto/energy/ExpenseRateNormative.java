package com.gmail.alaerof.dao.dto.energy;

public class ExpenseRateNormative {
    private int idLType;
    private String ltype;
    private EnergyType energyType;
    private float downUpExp;

    public ExpenseRateNormative(int idLType, EnergyType energyType, float downUpExp) {
        this.idLType = idLType;
        this.ltype = "";
        this.energyType = energyType;
        this.downUpExp = downUpExp;
    }

    public ExpenseRateNormative(int idLType, String ltype, EnergyType energyType, float downUpExp) {
        this.idLType = idLType;
        this.ltype = ltype;
        this.energyType = energyType;
        this.downUpExp = downUpExp;
    }

    public int getIdLType() {
        return idLType;
    }

    public String getLtype() {
        return ltype;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public float getDownUpExp() {
        return downUpExp;
    }

    public void setIdLType(int idLType) {
        this.idLType = idLType;
    }

    public void setLtype(String ltype) {
        this.ltype = ltype;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }

    public void setDownUpExp(float downUpExp) {
        this.downUpExp = downUpExp;
    }
}
