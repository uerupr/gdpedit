package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.IntGR;
import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.StationIntGR;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import java.util.List;
import java.util.Map;

/**
 * @author Helen Yrofeeva
 */
public interface IStationDAO {
    /**
     * ��������� �� �� ������� �� ID
     * @param idSt
     * @return {@link Station}
     * @throws ObjectNotFoundException
     */
	Station getStation(int idSt) throws ObjectNotFoundException, DataManageException;

    /**
     * ��������� �� �� ��� ���������
     * @return ������ {@link IntGR}
     */
	List<IntGR> getAllIntGR() throws DataManageException;

    /**
     * ��������� �� �� ��� ��������� �������
     * @return ������ {@link IntGR}
     */
    List<IntGR> getLineStInt(int idSt) throws DataManageException;

	/**
     * ��������� �� �� ��� ���������
     * @return ������ {@link IntGR}
     */
    List<StationIntGR> getStationIntGR(int idSt) throws DataManageException;

    /**
     * ��������� ������ �������������� ���������� ��� ������ ������� �� ������
     * @param idSts ������ ID �������
     * @return map (idSt, List(StationIntGR))
     */
    Map<Integer, List<StationIntGR>> getStationIntGR(List<Integer> idSts) throws DataManageException;

    /**
     * ��������� � �� ��������� �������
     * @param station �������
     */
    void updateStation(Station station) throws DataManageException;

    /**
     * ��������� � �� ������ ����� ��� �������
     * @param lineStList ������ ����� �������
     * @param idSt ��� �������
     */
    void updateLineSt(List<LineSt> lineStList, int idSt) throws DataManageException;

    /**
     * ������� �� �� ���� ��� �������
     * @param idSt ��� �������
     * @param idLineSt ��� ����
     */
    void deleteLineSt(int idSt, int idLineSt) throws DataManageException;

    /**
     * ��������� � �� ������ ����� ��� �������
     * @param stationIntGRList ������ ���������� �������
     * @param idSt ��� �������
     */
    void saveLineStInt(List<StationIntGR> stationIntGRList, int idSt) throws DataManageException;

    /**
     * ��������� �� �� ���� � ����� ��������
     * @param idSt ��� �������
     * @return String
     */
    String getTrainCombSource(int idSt) throws DataManageException;

    /**
     * ���������� � �� ���� � ����� ��������
     * @param idSt ��� �������
     * @param decouplingPath ���� � ����� ��������
     */
    void saveTrainCombSource(int idSt, String decouplingPath) throws DataManageException;
}
