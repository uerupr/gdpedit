package com.gmail.alaerof.dao.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.gmail.alaerof.util.ConnectionDBFileHandlerUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Helen Yrofeeva
 */

public class ConnectionManagerMSSQL implements ConnectionManager{
    public static Logger logger = LogManager.getLogger(ConnectionManagerMSSQL.class);
    private Connection connection;
    private Properties poolInfo;

    public ConnectionManagerMSSQL(String base) throws PoolManagerException {
        poolInfo = ConnectionDBFileHandlerUtil.loadPoolInfo(base);
        if (poolInfo != null) {
            String driver = poolInfo.getProperty("driver");
            String url = poolInfo.getProperty("url");

            logger.debug(driver);
            logger.debug(url);
            try {
                Connection conn = DriverManager.getConnection(url);
                this.connection = new SpaceConnection(conn);
            } catch (SQLException e) {
                logger.error(e);
                throw new PoolManagerException(PoolManagerException.CONNECTION_ERR + e.toString() + " url=" + url, e);
            }

        } else {
            throw new PoolManagerException(PoolManagerException.CONFIG_ERR);
        }
    }

    @Override
    public Connection getConnection() throws PoolManagerException {
        return connection;
    }

    @Override
    public String getPath(){
        return poolInfo.getProperty("pathMDB");
    }

    @Override
    public boolean isHSQLDB() {
        return false;
    }

    @Override
    public void closeConnection(){
        try {
            connection.close();
        } catch (Exception e) {
            logger.error(e);
        }
    }
   
}
