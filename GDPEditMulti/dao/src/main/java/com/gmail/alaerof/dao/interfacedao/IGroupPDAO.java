package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.groupp.GroupColor;
import com.gmail.alaerof.dao.dto.groupp.GroupP;
import com.gmail.alaerof.dao.dto.groupp.GroupRange;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.List;

public interface IGroupPDAO {
    /**
     * ��������� �� �� ������ ��������� ��� ������� ����������� ��� �������
     * @return ������ {@link GroupP}
     * @throws DataManageException
     */
    List<GroupP> getGroupPList() throws DataManageException;

    /**
     * ��������� � �� ������ ��������� ��� ������� ����������� ��� �������
     * @param groupPList  ������ {@link GroupP}
     * @param deletedGroupP
     * @param deletedRange
     * @param deletedColor
     * @throws DataManageException
     */
    void saveToDB(List<GroupP> groupPList, List<GroupP> deletedGroupP, List<GroupRange> deletedRange, List<GroupColor> deletedColor) throws DataManageException;
}
