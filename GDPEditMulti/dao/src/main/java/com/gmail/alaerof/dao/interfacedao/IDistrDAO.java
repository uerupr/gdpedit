package com.gmail.alaerof.dao.interfacedao;

import java.util.List;

import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.dao.common.GDPGridConfig;

/**
 * @author Helen Yrofeeva
 */
public interface IDistrDAO {
    /**
     * E��� idDir = 0 �� ������ ������� ��� �������
     * @param idDir �� ����
     * @return ������ ��������
     * @throws DataManageException
     */
	List<Distr> getDistrList(int idDir) throws DataManageException;

    /**
     * ���������� ������� �� ID
     * @param idDistr ID
     * @return {@link Distr}
     * @throws ObjectNotFoundException
     * @throws DataManageException
     */
	Distr getDistr(int idDistr) throws ObjectNotFoundException, DataManageException;

    /**
     * ���������� ��������� ����������� (GDPGridConfig) ��� ������� �� ID
     * @param idDistr ID
     * @return {@link GDPGridConfig}
     */
	GDPGridConfig getConfigGDPGrid(int idDistr);

    /**
     * ��������� ��������� ����������� �������
     * @param idDistr ID
     * @param nameDistr �������� �������
     * @param config  {@link GDPGridConfig}
     */
	void saveConfigGDPGrid(int idDistr, String nameDistr, GDPGridConfig config);

	/**
     * ���������� ���� �������� �� ������� LocomotiveType
     * @return ������ ����� �������� �� ������� LocomotiveType
     */
	List<LocomotiveType> getListLocomotiveType() throws DataManageException;

    /**
     * ���������� ������ ��������� (��� ���������� ���) �������
     * @param idDistr ID
     * @return ������ ���������
     * @throws DataManageException
     */
	List<RouteD> getRouteDList(int idDistr) throws DataManageException;

    /**
     * ��������� ������ ��������� � ��
     * @param routeDList ������ ���������
     * @param deleted ������  ��������� ���������
     */
    void saveRouteDs(List<RouteD> routeDList, List<RouteD> deleted) throws DataManageException;

    /**
     * ��������� � �� ������ ����� ��������/�����������
     * ��� ������ Edit, ����������� ������ ��, ��� � ID
     * @param list ������ ����������� ��� ���������� ���������
     */
    void saveLocomotiveTypes(List<LocomotiveType> list) throws DataManageException;

    /**
     * ��������� ����� (�������� ������ ���� ����������� �.�. ��� ���������� ��� �� �����������!!!)
     * �������� �������� ������ ��, ��� ��� ID
     * @param list ����� ��� ����������
     */
    void addLocomotiveTypes(List<LocomotiveType> list) throws DataManageException;

    /**
     * ������� ������ ����� ��������/����������� �� ��
     * ������� ������ ��, ��� � ID
     * @param list ������ ��� ��������
     */
    void deleteLocomotiveTypes(List<LocomotiveType> list) throws DataManageException;
}
