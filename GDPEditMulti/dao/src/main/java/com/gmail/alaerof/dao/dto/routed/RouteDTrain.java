package com.gmail.alaerof.dao.dto.routed;

public class RouteDTrain {
    private int idRouteDTR;
    private int idRouteD;
    private int trMin;
    private int trMax;

    public int getIdRouteDTR() {
        return idRouteDTR;
    }

    public void setIdRouteDTR(int idRouteDTR) {
        this.idRouteDTR = idRouteDTR;
    }

    public int getIdRouteD() {
        return idRouteD;
    }

    public void setIdRouteD(int idRouteD) {
        this.idRouteD = idRouteD;
    }

    public int getTrMin() {
        return trMin;
    }

    public void setTrMin(int trMin) {
        this.trMin = trMin;
    }

    public int getTrMax() {
        return trMax;
    }

    public void setTrMax(int trMax) {
        this.trMax = trMax;
    }
}
