package com.gmail.alaerof.util;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

import com.gmail.alaerof.dao.common.GDPGridConfig;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * ��������������� ����� ��� ������ �� ��������
 * @author Helen Yrofeeva
 * 
 */
public class TimeConverter {
    protected static Logger logger = LogManager.getLogger(TimeConverter.class);
    /**
     * @param tm ����� � ������� "hh:nn"
     * @return ���������� �����
     */
    public static int timeToMinutes(String tm) {
        int res = 0;
        if (tm != null) {
            String[] ss = tm.split(":");
            int h = Integer.parseInt(ss[0]);
            int m = Integer.parseInt(ss[1]);
            res = h * 60 + m;
        }
        return res;
    }

    /**
     * ��������� ������ � ��������� ������������� ������� � ������� "hh:nn"
     * @param m ����� � �������
     * @return ��������� �������������
     */
    public static String minutesToTime(int m) {
        int hh = m / 60;
        int mm = m - hh * 60;
        String sh = "" + hh;
        if (hh >= 24)
            hh = 0;
        if (hh < 10) {
            sh = "0" + hh;
        }
        String sm = "" + mm;
        if (mm < 10) {
            sm = "0" + mm;
        }
        return sh + ":" + sm;
    }

    /**
     * ��������� ������ � �������
     * @param m ����� � �������
     * @param generalParams ����� ��������� ����������� ������� (������ �����)
     * @param conf �������� ������ ��� ��������� (����������)
     * @return ����� � ��������
     */
    public static int minutesToX(int m, TrainThreadGeneralParams generalParams, GDPGridConfig conf) {
        int shiftedM = m - conf.hourB * 60;
        if (shiftedM < 0) {
            shiftedM += 24 * 60;
        }
        int xT = generalParams.xLeft + (int) (shiftedM * conf.picTimeHor * conf.scale);
        return xT;
    }

    /**
     * ������ ����� ����� ��������� � �������� �������� � ������ �������� ����� �����
     * @param tOn ��������� ����� "hh:nn"
     * @param tOff �������� ����� "hh:nn"
     * @return ���������� �����
     */
    public static int minutesBetween(String tOn, String tOff) {
        int mOn = timeToMinutes(tOn);
        int mOff = timeToMinutes(tOff);
        return minutesBetween(mOn, mOff);
    }

    /**
     * ������ ����� ����� ��������� � �������� �������� � ������ �������� ����� �����
     * @param mOn ��������� ����� � �������
     * @param mOff �������� ����� � �������
     * @return ���������� �����
     */
    public static int minutesBetween(int mOn, int mOff) {
        int res = 0;
        if (mOn <= mOff) {
            res = mOff - mOn;
        } else {
            res = 24 * 60 - (mOn - mOff);
        }
        return res;
    }

    /**
     * ��������� �� ������� � ������� �������� ���������� ����� � ������ �������� ����� �����
     * @param time ����� � �������
     * @param min ����������� ������
     * @return ���������� �����
     */
    public static int addMinutes(int time, int min) {
        int res = time + min;
        if (res >= 24 * 60) {
            res = res - 24 * 60;
        }
        if (res < 0) {
            res = 24 * 60 + res;
        }
        return res;
    }

    /**
     * ���������� ������� ����-����� � ������� "YYMMDD-hhnnssll"
     * ������������ ��� ������� ��������
     * @return ��������� ������������� ������� ����-�������
     */
    public static String getNowDateTime() {
        String str = "";
        Date d = new Date();
        Formatter f = new Formatter();
        str = f.format("%ty%tm%td-%tH%tM%tS%tL", d, d, d, d, d, d, d).toString();
        return str;
    }

    public static String formatTime(Time tm) {
        String t = null;
        if (tm != null) {
            t = new SimpleDateFormat("HH:mm").format(tm);
        }
        return t;
    }

    /**
     * ����������� ������ � ���� ��� ���������� � ��
     * @param tm String � ������� "HH:mm"
     * @return sql time
     */
    public static Time formatTime(String tm) {
        Time sqlTime = null;
        if (tm != null) {
            try {
                java.util.Date javaTime = new SimpleDateFormat("HH:mm").parse(tm);
                sqlTime = new java.sql.Time(javaTime.getTime());
            } catch (ParseException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return sqlTime;
    }

    /**
     * ���������� �������� ������ ���� ����� ��� tm
     * @param tm String � ������� "HH:mm"
     * @return int
     */
    public static int extractHour(String tm) {
        int m = timeToMinutes(tm);
        return extractHour(m);
    }

    /**
     * ���������� �������� ������ ���� ����� ��� m
     * @param m ������
     * @return int
     */
    public static int extractHour(int m) {
        int hh = m / 60;
        return hh;
    }

    /**
     * ��������� �� 1 � ������ �������� �����
     * @param hour
     * @return hour-1
     */
    public static int decHour(int hour) {
        int res = hour - 1;
        if (res < 0) {
            res = 23;
        }
        return res;
    }

    /**
     * ����������� �� 1 � ������ �������� �����
     * @param hour
     * @return hour+1
     */
    public static int incHour(int hour) {
        int res = hour + 1;
        if (res == 24) {
            res = 0;
        }
        return res;
    }
}
