package com.gmail.alaerof.dao.interfacedao;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface IDistrGDPXMLDAO {
    enum QueryName {
        Distr, Span, SpanTime, Station, LineSt, Span_St, RouteD, RouteDSp, RouteDTR,
        Wind, Distr_Train, SpanTrain, LineSt_Tr, TrainStop, SpTOcc, TrainCode, HideSpan,
        LineStyleSpan, ShowParam, DistrComb, Distr_TrainF, LineSt_TrF, Train_SpanColor,
        Train_SpanInscr, Category, LocomotiveType, Span_StTime, TextPrintParam, SpanEnergy
    }

    /**
     * ���������� ���������� ������� � ���� ���� xml
     * @param tg ��� ����
     * @param idDistr �� �������
     * @param doc {@link Document}
     * @return ���� xml
     */
    Element buildDistrGDPXML(QueryName tg, int idDistr, Document doc);

    /**
     * �������� ������� ������� �� xml � �� � �������� ��� �������������
     * @param doc xml
     * @param tagName ��� ����
     * @param allTrain ����� ������� ��� ����������
     * @param categoryList ������ ���������
     * @param warningList ������ ���������
     */
    void checkAllTrain(Document doc, String tagName, Map<Long, Pair<Element, DistrTrain>> allTrain,
                       List<Category> categoryList, List<String> warningList);

    /**
     * '����' �� �� xml � ��
     * @param idDistr �� �������
     * @param domDoc xml
     * @param allSp ������������ �������� (��)
     */
    void loadWind(int idDistr, Document domDoc, Map<Integer, Pair<Element, Scale>> allSp);

    /**
     * ���������� ������� ���� �� xml � ��
     * @param domDoc xml
     * @param allSp ����������� �������� (��)
     * @param allLoc ����������� ��� (��)
     */
    void loadSpanTime(Document domDoc, Map<Integer, Pair<Element, Scale>> allSp, Map<Integer, Pair<Element, LocomotiveType>> allLoc) throws PoolManagerException;

    /**
     * ������� ����������� ������� �� ������� � ������� ���������
     * @param idDistr �� �������
     * @param allSp ������������ �������� (��)
     */
    void loadAppearanceScale(int idDistr, Map<Integer, Pair<Element, Scale>> allSp) throws PoolManagerException;

    /**
     * �������/����� ����� ������ �� ��� (��� �������������)
     * @param allLst ������������ ���� ������� (��)
     */
    void loadAppearanceLineSt(Map<Integer, Pair<Element, LineSt>> allLst) throws PoolManagerException;

    /**
     * ������������ �������
     * @param idDistr �� �������
     * @param allDistrComb ������������ ������������ ������� (��)
     */
    void loadDistrComb(int idDistr, Map<Integer, Pair<Element, Distr>> allDistrComb) throws PoolManagerException;

    //void loadHDPARAM(int idDistr, Document domDoc);
}
