package com.gmail.alaerof.dao.sql.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.gmail.alaerof.dao.dto.Dir;
import com.gmail.alaerof.dao.sql.ConnectionManager;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.interfacedao.IDirDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Helen Yrofeeva
 */
public class DirDAOSQL implements IDirDAO {
    protected static Logger logger = LogManager.getLogger(DirDAOSQL.class);
    private final ConnectionManager cm;

    public DirDAOSQL(ConnectionManager cm) {
        this.cm = cm;
    }

    private void fillItem(Dir d, ResultSet rs) throws DataManageException {
        try {
            // ���� ���� ��������� ������ �� ������� ������ (JDBC-ODBC driver ������ ����)
            d.setIdDir(rs.getInt("IDdir"));
            d.setName(rs.getString("Name"));
            d.setPs(rs.getString("PS"));
            d.setOrd(rs.getInt("ord"));
        } catch (SQLException e) {
           logger.error(e.toString(), e);
           throw new DataManageException(" Distr: " + d, e);
        }

    }

    @Override
    public List<Dir> getListDir() throws DataManageException {
        List<Dir> listDir = new ArrayList<>();
        String sql = "SELECT IDdir, Name, PS, ord FROM Direction " + "WHERE ord<>1 ORDER BY ord, name";
        try {
            Connection cn = cm.getConnection();
            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);) {
                while (rs.next()) {
                    Dir d = new Dir();
                    fillItem(d, rs);
                    listDir.add(d);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return listDir;
    }

    @Override
    public Dir getDir(int idDir) throws ObjectNotFoundException, DataManageException {
        String sql = "SELECT IDdir, Name, PS, ord FROM Direction WHERE IDdir = " + idDir;
        Dir d = null;
        try {
            Connection cn = cm.getConnection();
            try(Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);) {
                if (rs.next()) {
                    d = new Dir();
                    fillItem(d, rs);
                } else {
                    throw new ObjectNotFoundException("Direction IDdir = " + idDir);
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new DataManageException(DataManageException.MANAGE_ERR, e);
        } catch (PoolManagerException e) {
            logger.error(e.toString(), e);
        }
        return d;
    }

}
