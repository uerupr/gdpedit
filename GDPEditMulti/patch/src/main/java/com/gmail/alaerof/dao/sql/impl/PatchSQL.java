package com.gmail.alaerof.dao.sql.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Helen Yrofeeva
 */
public class PatchSQL {
    protected static Logger logger = LogManager.getLogger(PatchSQL.class);
    private static final int FAILED = -1;
    private static final int NO_NEED = 0;
    private static final int SUCCESSFUL = 1;
    private Connection pmAccess;
    private Connection pmServer;

    public PatchSQL(Connection pmA, Connection pmS) {
        this.pmAccess = pmA;
        this.pmServer = pmS;
    }

    /**
     * @param mode 1 - server mode; 0 - local mode
     * @return message
     */
    public String applyPatch(int mode) {
        return applyPatch(mode,null);
    }

    public String applyPatch(int mode, Integer patchNum) {
        String mess = "";
        // ���������� �������� ����� � ������� �������
        if (patchNum == null || 0 == patchNum) {
            int upd = patch0();
            mess = buildMess(mess, upd, 0);
        }

        // ���������� ������� ����������� � �� �� �������
        if (patchNum == null || 1 == patchNum) {
                int upd = patch1();
                mess = buildMess(mess, upd, 1);
        }

        // ���������� ������� SpanTime � ��������� ��
        if (patchNum == null || 2 == patchNum) {
            int upd = patch2();
            mess = buildMess(mess, upd, 2);
        }

        // �������� ��������� ���� ������� LineSt_Train
        // todo Feature not supported yet.
        if (patchNum == null || 3 == patchNum) {
//            int upd = patch3();
//            mess = buildMess(mess, upd, 3);
        }

        // ���������� ������ ��� �������� ������� �������� ����� �.�.
        if (patchNum == null || 4 == patchNum) {
            int upd = patch4();
            mess = buildMess(mess, upd, 4);
        }

        // ���������� ���� main � ������� Distr
        if (patchNum == null || 5 == patchNum) {
            int upd = patch5();
            mess = buildMess(mess, upd, 5);
        }

        // ���������� ������� GDPTextPrintParam ��� �������� ��������� ����� ����� � �������� ��� ������ �������
        if (patchNum == null || 6 == patchNum) {
            int upd = patch6();
            mess = buildMess(mess, upd, 6);
        }

        // ���������� ������� SpanEnergy ��� �������� ��������-�������������� ������ �������� �� ��������
        // ��� ����������� ����������
        if (patchNum == null || 7 == patchNum) {
            int upd = patch7();
            mess = buildMess(mess, upd, 7);
        }

        // ���������� � Distr_Train (��������� ��):
        // Eu ����� ���������� ������ �� �������, ���
        // Et ����� ���������� ������ �� ������� ��� ����� ������� �������, ���
        // IDltype �� ���� �������� (����������) ����� ������(����� LocomotiveType)
        if (patchNum == null || 8 == patchNum) {
            int upd = patch8();
            mess = buildMess(mess, upd, 8);
        }

        // ���������� � xDistrTrain (������):
        // Eu ����� ���������� ������ �� �������, ���
        // Et ����� ���������� ������ �� ������� ��� ����� ������� �������, ���
        // IDltype �� ���� �������� (����������) ����� ������(����� LocomotiveType)
        if (mode == 1 && (patchNum == null || 9 == patchNum)) {
            int upd = patch9();
            mess = buildMess(mess, upd, 9);
        }

        // ���������� ������� StationParam ��� �������� ���������� ������� (��� ������ ��� ���� � ����� ������)
        if (patchNum == null || 10 == patchNum) {
            int upd = patch10();
            mess = buildMess(mess, upd, 10);
        }


        // ���������� ������� ExpenseRateEnergy �� �������
        if (patchNum == null || 11 == patchNum) {
            int upd = patch11();
            mess = buildMess(mess, upd, 11);
        }

        // ���������� ������� ExpenseRate �� �������
        if (patchNum == null || 12 == patchNum) {
            int upd = patch12();
            mess = buildMess(mess, upd, 12);
        }

        // ���������� ������� ExpenseRateNormative �� �������
        if (patchNum == null || 13 == patchNum) {
            int upd = patch13();
            mess = buildMess(mess, upd, 13);
        }


        return mess;
    }

    /**
     * @param mess ��������� ���������
     * @param upd  ������ ����������
     * @param indexUpd ������ ����������
     * @return �������� ����������
     */
    private String buildMess(String mess, int upd, int indexUpd) {
        StringBuilder sb = new StringBuilder(mess);
        switch (upd) {
            case -1:
                sb.append("\n(").append(indexUpd).append(") �� ����������;");
                break;
            case 0:
                sb.append("\n(").append(indexUpd).append(") ���������� �� ���������;");
                break;
            case 1:
                sb.append("\n(").append(indexUpd).append(") ����������;");
                break;
            default:
                break;
        }

        return sb.toString();
    }

    /**
     * ���������� �������� ����� � ������� �������
     * @return update status
     */
    private int patch0() {
        logger.debug("patch0 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM Station WHERE IDst = 0";
        ResultSet rs = null;
        Connection cn = pmServer;
        try (Statement st = cn.createStatement();) {
            try {
                rs = st.executeQuery(sql);
                ResultSetMetaData md = rs.getMetaData();
                boolean extF = false;
                for (int i = 1; i <= md.getColumnCount(); i++) {
                    if (md.getColumnName(i).equals("joint")) {
                        extF = true;
                    }
                }
                if (!extF) {
                    ext = FAILED;
                    // ������� ����� ���� � ������� (������� �������� ������)
                    sql = "ALTER TABLE Station ADD COLUMN joint BIT";
                    st.executeUpdate(sql);
                    ext = SUCCESSFUL;
                }
            } catch (SQLException e) {
                logger.error(e.toString(), e);
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return ext;

    }

    /**
     * CREATE TABLE LocomotiveType
     * INSERT INTO LocomotiveType
     * @return update status
     */
    private int patch1() {
        logger.debug("patch1 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM LocomotiveType";
        ResultSet rs = null;
        Connection cnS = pmServer;
        try (Statement stS = cnS.createStatement();) {
            try {
                rs = stS.executeQuery(sql);
            } catch (SQLException e) {
                String mess = e.getMessage();
                if (mess.contains("LocomotiveType") || mess.contains("LocomotiveType".toUpperCase())) {
                    ext = FAILED;
                    System.out.println("--- no table ---");
                    cnS.setAutoCommit(false);
                    try {
                        // ������� ����� ������� ���� �� ��� � ��
                        sql = "CREATE TABLE LocomotiveType (IDltype AUTOINCREMENT  CONSTRAINT key1 PRIMARY KEY, ltype VARCHAR(50), colorLoc INTEGER )";
                        stS.executeUpdate(sql);
                        // ��������� ��� �������� ��� ������������� � ����.
                        // �������
                        sql = "INSERT INTO LocomotiveType (ltype, colorLoc) VALUES('��������', 0)";
                        stS.executeUpdate(sql);
                        sql = "INSERT INTO LocomotiveType (ltype, colorLoc) VALUES('������������', 255)";
                        stS.executeUpdate(sql);
                        sql = "INSERT INTO LocomotiveType (ltype, colorLoc) VALUES('�����������', 16711680)";
                        stS.executeUpdate(sql);
                        cnS.commit();
                        cnS.setAutoCommit(true);
                        ext = SUCCESSFUL;
                    } catch (Exception ex) {
                        logger.error(ex.toString(),ex);
                        cnS.rollback();
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;
    }

    /**
     * ALTER TABLE SpanTime UPDATE SpanTime
     * @return update status
     */
    public int patch2() {
        logger.debug("patch2 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM SpanTime WHERE IDspan = 0";
        ResultSet rs = null;
        boolean extF = false;
        // cn.setAutoCommit(false);
        Connection cn = pmAccess;
        try (Statement st = cn.createStatement();) {
            try {
                rs = st.executeQuery(sql);
                ResultSetMetaData md = rs.getMetaData();

                for (int i = 1; i <= md.getColumnCount(); i++) {
                    if (md.getColumnName(i).equals("IDltype")) {
                        extF = true;
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
            if (!extF) {
                ext = FAILED;
                // ������� ����� ���� � �������
                sql = "ALTER TABLE SpanTime ADD COLUMN IDltype INTEGER ";
                // +
                // "CONSTRAINT keyLT FOREIGN KEY (IDltype) REFERENCES LocomotiveType (IDltype) ";
                // cn.commit();
                // cn.setAutoCommit(true);
                st.executeUpdate(sql);
                ext = SUCCESSFUL;
            }

            sql = "UPDATE SpanTime SET IDltype= 1 " + " WHERE type = '��������'";
            st.executeUpdate(sql);
            sql = "UPDATE SpanTime SET IDltype= 2 " + " WHERE type = '������������'";
            st.executeUpdate(sql);
            sql = "UPDATE SpanTime SET IDltype= 3 " + " WHERE type = '�����������'";
            st.executeUpdate(sql);

        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;

    }

    /**
     * @return update status
     */
    @SuppressWarnings("unused")
    private int patch3() {
        logger.debug("patch3 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM LineSt_Train WHERE IDst = 0";
        ResultSet rs = null;
        Connection cn = pmAccess;
        try (Statement st = cn.createStatement();) {
            try {
                rs = st.executeQuery(sql);
                ResultSetMetaData md = rs.getMetaData();
                boolean extF = false;
                for (int i = 1; i <= md.getColumnCount(); i++) {
                    if (md.getColumnName(i).equals("IDlineSt_Train")) {
                        extF = true;
                    }
                }
                if (extF) {
                    ext = FAILED;
                    // ������� ���� �� �������
                    sql = "ALTER TABLE LineSt_Train DROP COLUMN IDlineSt_Train";
                    //sql = "ALTER TABLE LineSt_Train DROP COLUMN newCol";
                    st.executeUpdate(sql);
                    ext = SUCCESSFUL;
                }
            } catch (SQLException e) {
                logger.error(e.toString(),e);
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;
    }
    /**
     * ���������� ������ ��� �������� ������� �������� ����� �.�.
     * @return update status
     */
    private int patch4() {
        logger.debug("patch4 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM Span_StTime WHERE IDscale = 0";
        ResultSet rs = null;
        Connection cn = pmAccess;
        try (Statement stS = cn.createStatement();) {
            try {
                rs = stS.executeQuery(sql);
                
            } catch (SQLException e) {
                String mess = e.getMessage();
                if (mess.contains("Span_StTime")|| mess.contains("Span_StTime".toUpperCase())) {
                    ext = FAILED;
                    System.out.println("--- no table ---");
                    cn.setAutoCommit(false);
                    try {
                        // ������� ����� ������� ���� �� ��� � ��
                        sql = "CREATE TABLE Span_StTime (IDscale INTEGER, IDltype INTEGER, num INTEGER, Tmove_o REAL, Tmove_e REAL)";
                        stS.executeUpdate(sql);
                        cn.commit();
                        cn.setAutoCommit(true);
                        ext = SUCCESSFUL;
                    } catch (Exception ex) {
                        logger.error(ex.toString(),ex);
                        cn.rollback();
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;
    }

    /**
     * ���������� main � Distr
     * @return update status
     */
    private int patch5() {
        logger.debug("patch5 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM Distr WHERE IDdistr = 0";
        ResultSet rs = null;
        Connection cn = pmServer;
        try (Statement st = cn.createStatement();) {
            try {
                rs = st.executeQuery(sql);
                ResultSetMetaData md = rs.getMetaData();
                boolean extF = false;
                for (int i = 1; i <= md.getColumnCount(); i++) {
                    if (md.getColumnName(i).equals("main")) {
                        extF = true;
                    }
                }
                if (!extF) {
                    ext = FAILED;
                    // ��������� ���� � �������
                    sql = "ALTER TABLE Distr ADD COLUMN main INT";
                    st.executeUpdate(sql);
                    ext = SUCCESSFUL;
                }
            } catch (SQLException e) {
                logger.error(e.toString(),e);
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;
    }

    /**
     * CREATE TABLE GDPTextPrintParam ��� �������� ��������� ����� ����� � �������� ��� ������ �������
     * @return update status
     */
    private int patch6() {
        logger.debug("patch6 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM GDPTextPrintParam WHERE IDtextParam = 0";
        ResultSet rs = null;
        Connection cn = pmAccess;
        try (Statement stS = cn.createStatement();) {
            try {
                rs = stS.executeQuery(sql);
            } catch (SQLException e) {
                String mess = e.getMessage();
                if (mess.contains("GDPTextPrintParam")|| mess.contains("GDPTextPrintParam".toUpperCase())) {
                    ext = FAILED;
                    System.out.println("--- no table ---");
                    cn.setAutoCommit(false);
                    try {
                        // ������� ����� ������� ���� �� ��� � ��
                        sql = "CREATE TABLE GDPTextPrintParam (IDtextParam AUTOINCREMENT  CONSTRAINT key1 PRIMARY KEY, IDdistr INTEGER," +
                        "tpType INTEGER, choise INTEGER, textField TEXT(255), fontField TEXT(255), num INTEGER )";
                        stS.executeUpdate(sql);
                        cn.commit();
                        cn.setAutoCommit(true);
                        ext = SUCCESSFUL;
                    } catch (Exception ex) {
                        logger.error(ex.toString(),ex);
                        cn.rollback();
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;
    }

    /**
     * CREATE TABLE SpanEnergy
     * ���������� ������� SpanEnergy ��� �������� ��������-�������������� ������ �������� �� ��������
     * ��� ����������� ����������
     *
     * @return update status
     */
    private int patch7() {
        logger.debug("patch7 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM SpanEnergy WHERE IDscale = 0";
        ResultSet rs = null;
        Connection cn = pmAccess;
        try (Statement stS = cn.createStatement();) {
            try {
                rs = stS.executeQuery(sql);
            } catch (SQLException e) {
                String mess = e.getMessage();
                if (mess.contains("SpanEnergy") || mess.contains("SpanEnergy".toUpperCase())) {
                    ext = FAILED;
                    logger.debug("--- no table ---");
                    cn.setAutoCommit(false);
                    try {
                        // ������� ����� ������� ���� �� ��� � ��
                        sql = "CREATE TABLE SpanEnergy (" +
                                "IDscale INTEGER, IDltype INTEGER, " +
                                "EnMove REAL, EnDown REAL, EnUp REAL, " +
                                "oe TEXT(10)" +
                                ")";
                        stS.executeUpdate(sql);
                        cn.commit();
                        cn.setAutoCommit(true);
                        ext = SUCCESSFUL;
                    } catch (Exception ex) {
                        logger.error(ex.toString(), ex);
                        cn.rollback();
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return ext;
    }

    /**
     * ���������� � Distr_Train
     * Eu ����� ���������� ������ �� �������, ���
     * Et ����� ���������� ������ �� ������� ��� ����� ������� �������, ���
     * IDltype  �� ���� �������� (����������) ����� ������(����� LocomotiveType)
     *
     * @return update status
     */
    private int patch8() {
        logger.debug("patch8 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM Distr_Train WHERE IDdistr = 0";
        ResultSet rs = null;
        Connection cn = pmAccess;
        try (Statement st = cn.createStatement();) {
            try {
                rs = st.executeQuery(sql);
                ResultSetMetaData md = rs.getMetaData();
                boolean extF = false;
                for (int i = 1; i <= md.getColumnCount(); i++) {
                    if (md.getColumnName(i).toUpperCase().equals("IDltype".toUpperCase())) {
                        extF = true;
                    }
                }
                if (!extF) {
                    ext = FAILED;
                    // ��������� ���� � �������
                    sql = "ALTER TABLE Distr_Train ADD COLUMN IDltype INT";
                    st.executeUpdate(sql);
                    sql = "ALTER TABLE Distr_Train ADD COLUMN Et REAL";
                    st.executeUpdate(sql);
                    sql = "ALTER TABLE Distr_Train ADD COLUMN Eu REAL";
                    st.executeUpdate(sql);

                    // ��������� IDltype ���������� ����������
                    /*sql = "UPDATE Distr_Train SET IDltype= 1 " +
                            " WHERE IDtrain IN (SELECT IDtrain FROM Train WHERE type = '��������')";
                    st.executeUpdate(sql);
                    sql = "UPDATE Distr_Train SET IDltype= 2 " +
                            " WHERE IDtrain IN (SELECT IDtrain FROM Train WHERE type = '������������')";
                    st.executeUpdate(sql);
                    sql = "UPDATE Distr_Train SET IDltype= 3 " +
                            " WHERE IDtrain IN (SELECT IDtrain FROM Train WHERE type = '�����������')";
                    st.executeUpdate(sql);
                    */
                    ext = SUCCESSFUL;
                }

            } catch (SQLException e) {
                logger.error(e.toString(),e);
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;
    }
    /**
     * ���������� � xDistrTrain
     * Eu ����� ���������� ������ �� �������, ���
     * Et ����� ���������� ������ �� ������� ��� ����� ������� �������, ���
     * IDltype  �� ���� �������� (����������) ����� ������(����� LocomotiveType)
     *
     * @return update status
     */
    private int patch9() {
        logger.debug("patch9 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM xDistrTrain WHERE IDtrain = 0";
        ResultSet rs = null;
        Connection cn = pmServer;
        try (Statement st = cn.createStatement();) {
            try {
                rs = st.executeQuery(sql);
                ResultSetMetaData md = rs.getMetaData();
                boolean extF = false;
                for (int i = 1; i <= md.getColumnCount(); i++) {
                    if (md.getColumnName(i).toUpperCase().equals("IDltype".toUpperCase())) {
                        extF = true;
                    }
                }
                if (!extF) {
                    ext = FAILED;
                    // ��������� ���� � �������
                    sql = "ALTER TABLE xDistrTrain ADD IDltype INT";
                    st.executeUpdate(sql);
                    sql = "ALTER TABLE xDistrTrain ADD Et REAL";
                    st.executeUpdate(sql);
                    sql = "ALTER TABLE xDistrTrain ADD Eu REAL";
                    st.executeUpdate(sql);

                    // ��������� IDltype ���������� ����������
                    sql = "UPDATE xDistrTrain SET IDltype= 1 WHERE typeTR = '��������'";
                    st.executeUpdate(sql);
                    sql = "UPDATE xDistrTrain SET IDltype= 2 WHERE typeTR = '������������'";
                    st.executeUpdate(sql);
                    sql = "UPDATE xDistrTrain SET IDltype= 3 WHERE typeTR = '�����������'";
                    st.executeUpdate(sql);
                    ext = SUCCESSFUL;
                }

            } catch (SQLException e) {
                logger.error(e.toString(),e);
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;
    }

    /**
     * CREATE TABLE StationParam ��� �������� ���������� ������� (��� ������ ��� ���� � ����� ������)
     * @return update status
     */
    private int patch10() {
        logger.debug("patch10 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM StationParam WHERE IDst = 0";
        ResultSet rs = null;
        Connection cn = pmAccess;
        try (Statement stS = cn.createStatement();) {
            try {
                rs = stS.executeQuery(sql);
            } catch (SQLException e) {
                String mess = e.getMessage();
                if (mess.contains("StationParam")|| mess.contains("StationParam".toUpperCase())) {
                    ext = FAILED;
                    System.out.println("--- no table ---");
                    cn.setAutoCommit(false);
                    try {
                        // ������� ����� ������� ���� �� ��� � ��
                        sql = "CREATE TABLE StationParam (IDst INTEGER, CombSource TEXT(255))";
                        stS.executeUpdate(sql);
                        cn.commit();
                        cn.setAutoCommit(true);
                        ext = SUCCESSFUL;
                    } catch (Exception ex) {
                        logger.error(ex.toString(),ex);
                        cn.rollback();
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(),e);
        }
        return ext;
    }

    /**
     * CREATE TABLE ExpenseRateEnergy
     * @return update status
     */

    private int patch11() {
        logger.debug("patch11 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM ExpenseRateEnergy";
        ResultSet rs = null;
        Connection cnS = pmServer;
        try (Statement stS = cnS.createStatement();) {
            try {
                rs = stS.executeQuery(sql);
            } catch (SQLException e) {
                String mess = e.getMessage();
                if (mess.contains("ExpenseRateEnergy") || mess.contains("ExpenseRateEnergy".toUpperCase())) {
                    ext = FAILED;
                    System.out.println("--- no table ---");
                    cnS.setAutoCommit(false);
                    try {
                        // ������� ����� ������� ���� �� ��� � ��
                        sql = "CREATE TABLE ExpenseRateEnergy (EnergyType VARCHAR(50), ExpRateEnValue REAL )";
                        stS.executeUpdate(sql);
                        cnS.commit();
                        cnS.setAutoCommit(true);
                        ext = SUCCESSFUL;
                    } catch (Exception ex) {
                        logger.error(ex.toString(), ex);
                        cnS.rollback();
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return ext;
    }


    /**
     * CREATE TABLE ExpenseRate
     * @return update status
     */

    private int patch12() {
        logger.debug("patch12 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM ExpenseRate";
        ResultSet rs = null;
        Connection cnS = pmServer;
        try (Statement stS = cnS.createStatement();) {
            try {
                rs = stS.executeQuery(sql);
            } catch (SQLException e) {
                String mess = e.getMessage();
                if (mess.contains("ExpenseRate") || mess.contains("ExpenseRate".toUpperCase())) {
                    ext = FAILED;
                    System.out.println("--- no table ---");
                    cnS.setAutoCommit(false);
                    try {
                        // ������� ����� ������� ���� �� ��� � ��
                        sql = "CREATE TABLE ExpenseRate (MoveExpRate REAL, StopExpRate REAL, TrainType VARCHAR(50), EnergyType VARCHAR(50) )";
                        stS.executeUpdate(sql);
                        cnS.commit();
                        cnS.setAutoCommit(true);
                        ext = SUCCESSFUL;
                    } catch (Exception ex) {
                        logger.error(ex.toString(), ex);
                        cnS.rollback();
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return ext;
    }


    /**
     * CREATE TABLE ExpenseRateNormative
     * @return update status
     */

    private int patch13() {
        logger.debug("patch13 start");
        int ext = NO_NEED;
        String sql = "SELECT * FROM ExpenseRateNormative";
        ResultSet rs = null;
        Connection cnS = pmServer;
        try (Statement stS = cnS.createStatement();) {
            try {
                rs = stS.executeQuery(sql);
            } catch (SQLException e) {
                String mess = e.getMessage();
                if (mess.contains("ExpenseRateNormative") || mess.contains("ExpenseRateNormative".toUpperCase())) {
                    ext = FAILED;
                    System.out.println("--- no table ---");
                    cnS.setAutoCommit(false);
                    try {
                        // ������� ����� ������� ���� �� ��� � ��
                        sql = "CREATE TABLE ExpenseRateNormative (IDltype INTEGER, EnergyType VARCHAR(50), DownUpExp REAL )";
                        stS.executeUpdate(sql);
                        cnS.commit();
                        cnS.setAutoCommit(true);
                        ext = SUCCESSFUL;
                    } catch (Exception ex) {
                        logger.error(ex.toString(), ex);
                        cnS.rollback();
                    }
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
        return ext;
    }

}
