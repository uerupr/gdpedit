package com.gmail.alaerof.application;

import com.gmail.alaerof.dao.sql.ConnectionManager;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.gmail.alaerof.dao.sql.GDPDAOFactorySQL;
import com.gmail.alaerof.dao.sql.ConnectionManagerMSSQL;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.sql.impl.PatchSQL;
/**
 * @author Helen Yrofeeva
 */
class Finish extends JFrame {
    private static final long serialVersionUID = 1L;

    public Finish(String mess1, String mess2) {
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        JTextArea ta = new JTextArea(mess1);
        ta.setEnabled(false);
        getContentPane().add(ta);
        ta = new JTextArea(mess2);
        ta.setEnabled(false);
        getContentPane().add(ta);
        setBounds(200, 200, 250, 550);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}

public class PatchDB {
    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }
    protected static Logger logger = LogManager.getLogger(PatchDB.class);

    public static void main(String[] args) {
        String messL = updateLocalMode();
        String messS = updateServerMode();
        new Finish("���������� ��������� ��:    " + messL, "\n���������� ��������� ��:    " + messS)
                .setVisible(true);
    }

    private static String updateServerMode() {
        try {
            ConnectionManager pmS = GDPDAOFactorySQL.getNewPoolManager(GDPDAOFactorySQL.BASE_NAME_SERVER);
            ConnectionManager pmA = GDPDAOFactorySQL.getNewPoolManager(GDPDAOFactorySQL.BASE_NAME_ACCESS);
            Connection conS = pmS.getConnection();
            Connection conA = pmA.getConnection();
            try {
                PatchSQL pt = new PatchSQL(conA, conS);
                String messS = pt.applyPatch(1);
                return messS;
            } finally {
                if (conS != null)
                    conS.close();
                if (conA != null)
                    conA.close();
            }
        } catch (PoolManagerException | SQLException e) {
            logger.error(e);
        }
        return "failed";
    }

    private static String  updateLocalMode() {
        try {
            ConnectionManager pmL = GDPDAOFactorySQL.getNewPoolManager(GDPDAOFactorySQL.BASE_NAME_LOCAL);
            Connection conL = pmL.getConnection();
            try {
                PatchSQL pt = new PatchSQL(conL, conL);
                String messL = pt.applyPatch(0);
                return messL;
            } finally {
                if (conL != null)
                    conL.close();
            }
        } catch (PoolManagerException | SQLException e) {
            logger.error(e);
        }
        return "failed";
    }

}
