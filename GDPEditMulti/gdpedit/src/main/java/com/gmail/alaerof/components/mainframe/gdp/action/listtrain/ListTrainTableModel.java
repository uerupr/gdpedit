package com.gmail.alaerof.components.mainframe.gdp.action.listtrain;

import java.util.Collections;

import java.util.List;
import java.util.ResourceBundle;
import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.entity.train.TrainComparatorByCode;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ListTrainTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = {bundle.getString("listtrain.number"),
            bundle.getString("listtrain.nameshort"),
            bundle.getString("listtrain.hidden"),
            bundle.getString("listtrain.commoninscription")};
    /** ������ ������� �� ������� (� ����� � "�����") */
    private List<TrainThread> listTrain;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.listtrain", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (listTrain != null) {
            return listTrain.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (listTrain != null) {
            TrainThread tr = listTrain.get(row);
            if (tr != null) {
                switch (col) {
                case 0:
                    return new String(tr.getCode().trim());
                case 1:
                    return new String(tr.getNameTR().trim());
                case 2:
                    if (tr.isHidden()) {
                        return "*";
                    } else {
                        return "";
                    }
                case 3:
                    return new String(tr.getAInscr().trim());
                default:
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 3) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (listTrain != null && aValue != null) {
            TrainThread train = listTrain.get(row);
            if (train != null) {
                train.getDistrTrain().getShowParam().setAinscr(aValue.toString());
                train.setTrainLocation();
            }
        }
    }

    public List<TrainThread> getListTrain() {
        return listTrain;
    }

    public TrainThread getTrain(int index) {
        TrainThread train = null;
        if (listTrain != null) {
            if (index > -1 && index < listTrain.size()) {
                train = listTrain.get(index);
            }
        }
        return train;
    }

    public void setListTrain(List<TrainThread> listTrain) {
        if (listTrain != null) {
            Collections.sort(listTrain, new TrainComparatorByCode());
        }
        this.listTrain = listTrain;
    }

    public int getTrainIndex(TrainThread train) {
        return listTrain.indexOf(train);
    }

    public void addTrain(TrainThread train) {
        listTrain.add(train);
        Collections.sort(listTrain, new TrainComparatorByCode());
    }

}
