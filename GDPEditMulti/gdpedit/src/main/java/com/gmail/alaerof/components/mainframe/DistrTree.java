package com.gmail.alaerof.components.mainframe;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.components.menu.MenuDistr;
import com.gmail.alaerof.components.menu.MenuGDP;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.gmail.alaerof.manager.LocaleManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.Dir;
import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDirDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.entity.DistrEditEntity;

/**
 * @author Helen Yrofeeva
 */
public class DistrTree {
    protected static Logger logger = LogManager.getLogger(DistrTree.class);
    private JTree distrTree;
    private JScrollPane content;
    private Object userObject;
    private static ResourceBundle bundle;

    public DistrTree() {
        DefaultMutableTreeNode top = new DefaultMutableTreeNode(bundle.getString("gdp.nod"));
        createNodes(top);
        distrTree = new JTree(top);

        distrTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        buildDistrTreeEventListeners();

        content = new JScrollPane(distrTree);
        distrTree.setRootVisible(false);

    }

    static {
        bundle = ResourceBundle.getBundle("bundles.gdp", LocaleManager.getLocale());
    }

    /**
     * ���������� ��������� ������� � ������ ��������
     */
    private void buildDistrTreeEventListeners() {
        JPopupMenu menu = buildMenu();
        distrTree.add(menu);

        // ��������� ������� ������ ������� � ������ �������� (� ����������)
        distrTree.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) distrTree.getLastSelectedPathComponent();
                if (node == null) {
                    userObject = null;
                    return;
                }
                userObject = node.getUserObject();
                if (node == null)
                    return;
                // ��������� ���� - ������ ����
                Object userObject = node.getUserObject();
                String info = userObject.toString();
                if (userObject instanceof DistrEditEntity) {
                    DistrEditEntity distrEditEntity = (DistrEditEntity) node.getUserObject();
                    info = distrEditEntity.getSoureGDP();
                    GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
                    if (editPanel == null) {
                        if (distrEditEntity.listTrainLoaded()) {
                            GDPEdit.gdpEdit.setCurrentDistrEditEntity(distrEditEntity);
                        } else {
                            GDPEdit.gdpEdit.getGDPActionPanel().clearTab();
                        }
                    }
                }
                GDPEdit.statusBar.setMessage(info);
            }
        });

        // ��������� ������� ������ ������� � ������ �������� (������)
        distrTree.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int selRow = distrTree.getRowForLocation(e.getX(), e.getY());
                TreePath selPath = distrTree.getPathForLocation(e.getX(), e.getY());
                if (selRow != -1) {
                    // �������� ������ - �������
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) selPath.getLastPathComponent();
                    Object obj = node.getUserObject();
                    // ������� ����
                    if (e.getClickCount() == 2) {
                        GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.waitCursor);
                        try {
                            if (obj instanceof DistrEditEntity) {
                                DistrEditEntity de = ((DistrEditEntity) obj);
                                // ���������� �����. �������� ���
                                // ��� ��������� � ���������� ����� �������� ���
                                GDPEdit.gdpEdit.getGDPEditTabPane().addGDPEditTab(de);
                            }
                        } finally {
                            GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.defCursor);
                        }
                    }
                    // ����
                    if (e.getClickCount() == 1) {
                        if (e.getButton() == 3) {
                            if (obj instanceof DistrEditEntity) {
                                menu.show(e.getComponent(), e.getX(), e.getY());
                            }
                        }
                    }
                }
            }
        });

        distrTree.addMouseMotionListener(new MouseMotionAdapter() {

            @Override
            public void mouseMoved(MouseEvent evt) {
                if (distrTree.getRowForLocation(evt.getX(), evt.getY()) != -1) {
                    TreePath curPath = distrTree.getPathForLocation(evt.getX(), evt.getY());
                    String comp = curPath.getLastPathComponent().toString();
                    distrTree.setToolTipText(comp);
                }
            }

        });
    }

    private JPopupMenu buildMenu() {
        JPopupMenu menu = new JPopupMenu();
        JMenu mnDistr = new MenuDistr();
        JMenu mnGDP = new MenuGDP();
        menu.add(mnDistr);
        menu.add(mnGDP);
        return menu;
    }

    private void createNodes(DefaultMutableTreeNode top) {
        DefaultMutableTreeNode nod = null;
        DefaultMutableTreeNode distr = null;

        if (GDPDAOFactoryCreater.getGDPDAOFactory() != null) {
            try {
                IDirDAO dirDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getDirDAO();
                IDistrDAO distrDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO();

                List<Dir> listDir;
                try {
                    listDir = dirDAO.getListDir();
                    for (Dir dir : listDir) {
                        nod = new DefaultMutableTreeNode(dir.getName());
                        top.add(nod);
                        List<Distr> listDistr = distrDAO.getDistrList(dir.getIdDir());
                        for (Distr d : listDistr) {
                            distr = new DefaultMutableTreeNode(new DistrEditEntity(d.getIdDistr()));
                            nod.add(distr);
                        }
                    }
                } catch ( DataManageException e) {
                    logger.error(e.toString(), e);
                }
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    public JScrollPane getDistrTreePanel() {
        return content;
    }

    public JTree getDistrTree() {
        return distrTree;
    }

    /**
     * return ID selected distr
     * @return ID selected distr
     */
    public int getIDdistr() {
        DistrEditEntity dee = getDistrEditEntity();
        if (dee!=null) {
            return dee.getIdDistr();
        }
        return -1;
    }
    /**
     * return selected DistrEditEntity object or null
     * @return selected DistrEditEntity object or null
     */
    public DistrEditEntity getDistrEditEntity(){
        if (userObject != null && userObject instanceof DistrEditEntity) {
            return (DistrEditEntity) userObject;
        }
        return null;
    }
}
