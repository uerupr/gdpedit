package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.Dir;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DirModel {
    private Dir dir;
    private StringProperty dirName;

    public DirModel(Dir dir) {
        this.dir = dir;
        dirName = new SimpleStringProperty(dir.getName());
    }

    public String getDirName() {
        return dirName.get();
    }

    public StringProperty dirNameProperty() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName.set(dirName);
    }

    public Dir getDir() {
        return dir;
    }
}
