package com.gmail.alaerof.javafx.dialog.energyfromtr;

import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrSpanSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.javafx.dialog.energyfromtr.model.ImportTRModel;
import com.gmail.alaerof.util.DataXMLUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ImportEnergyFromTRXML {
    private static Logger logger = LogManager.getLogger(ImportEnergyFromTRXML.class);

    public static List<ImportTRModel> loadEnergyTRList(File file, DistrEditEntity distrEditEntity){
        List<ImportTRModel> list = new ArrayList<>();
        try {
            Document doc = DataXMLUtil.loadDocument(file);
            NodeList nodeList = doc.getElementsByTagName("StTime");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element elITRM = (Element) nodeList.item(i);
                ImportTRModel iTRM = new ImportTRModel();
                list.add(iTRM);
                iTRM.nameStProperty().set(elITRM.getAttribute("Name"));
                iTRM.codeESRProperty().set(elITRM.getAttribute("CodeESR"));
                iTRM.t1OilProperty().set(Double.parseDouble(elITRM.getAttribute("T1Oil").replace(',','.')));
                iTRM.t2OilProperty().set(Double.parseDouble(elITRM.getAttribute("T2Oil").replace(',','.')));
                iTRM.tUpOilProperty().set(Double.parseDouble(elITRM.getAttribute("TupOil").replace(',','.')));
                iTRM.tDownOilProperty().set(Double.parseDouble(elITRM.getAttribute("TdownOil").replace(',','.')));
                iTRM.newNameStProperty().set("");
                iTRM.newCodeESRProperty().set("");
                iTRM.codeESR2Property().set("");
                iTRM.stationOrSpanStProperty().set(0);
                iTRM.connectProperty().set("");
                for (int j = 0; j < distrEditEntity.getDistrEntity().getListDistrPoint().size(); j++){
                    DistrPoint dp = distrEditEntity.getDistrEntity().getListDistrPoint().get(j);
                    if (dp.getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)){
                        DistrStation ds = (DistrStation) dp;
                        Station station = ds.getStation();
                        if (station.getCodeESR().equals(iTRM.codeESRProperty().get())) {
                            iTRM.idStationProperty().set(station.getIdSt());
                            iTRM.stationOrSpanStProperty().set(1);
                            iTRM.connectProperty().set("Ok");
                        }
                    }
                    if (dp.getTypePoint().equals(DistrPoint.TypePoint.StopPoint)){
                        DistrSpanSt dss = (DistrSpanSt) dp;
                        SpanSt spanSt = dss.getSpanSt();
                        if (spanSt.getCodeESR().equals(iTRM.codeESRProperty().get())) {
                            iTRM.idSpanStProperty().set(spanSt.getIdSpanst());
                            iTRM.stationOrSpanStProperty().set(2);
                            iTRM.connectProperty().set("Ok");
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return list;
    }
}
