package com.gmail.alaerof.dialog.timeimport.spans;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.entity.DistrSpan;

/**
 * ����� ��� ���������� ������� ���� �� ��������
 * (��� ��������� ���� � ����������� ��������)
 * @author Helen Yrofeeva
 */
public class SpanTimeHolder implements Comparable<SpanTimeHolder> {
    private DistrSpan span;
    /* ��� ����� ��� ������� */
    private Time time;
    private Time timeAdd;
    private LocomotiveType locType;
    /** ����������� �������� 0-������, 1-�������� */
    private OE oe;

    public DistrSpan getSpan() {
        return span;
    }

    public void setSpan(DistrSpan span) {
        this.span = span;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public LocomotiveType getLocType() {
        return locType;
    }

    public void setLocType(LocomotiveType locType) {
        this.locType = locType;
    }

    public OE getOe() {
        return oe;
    }

    public void setOe(OE oe) {
        this.oe = oe;
    }

    /** ����������� �������� 0-������, 1-�������� */
    public void setOe(int oe) {
        if (oe == 0) {
            this.oe = OE.even;
        } else {
            this.oe = OE.odd;
        }
    }

    @Override
    public int compareTo(SpanTimeHolder o) {
        return this.span.getSpanScale().getNum() - o.span.getSpanScale().getNum();
    }

    public Time getTimeAdd() {
        return timeAdd;
    }

    public void setTimeAdd(Time timeAdd) {
        this.timeAdd = timeAdd;
    }

    /***
     * ��������� ����� ���� � ������������ � �������� ���. ������
     * @param isUpDownFromAdd  ����� ������� � ���������� �� ���. �������
     */
    public void updateTimeToAdd(boolean isUpDownFromAdd) {
        time.setTimeTw(timeAdd.getTimeMove());
        if (isUpDownFromAdd) {
            time.setTimeUp(timeAdd.getTimeUp());
            time.setTimeDown(timeAdd.getTimeDown());
        }
    }
}
