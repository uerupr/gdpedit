package com.gmail.alaerof.javafx.control;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import javafx.beans.property.SimpleDoubleProperty;

public class RoundedDoubleProperty extends SimpleDoubleProperty {
    /**
     * Format pattern
     */
    private static final String pattern = "####.##";

    public RoundedDoubleProperty(double value) {
        super(value);
    }

    public RoundedDoubleProperty() {
        super(0.0);
    }

//    @Override
//    public String toString(){
//        double value = this.get();
//        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
//        otherSymbols.setDecimalSeparator('.');
//        DecimalFormat myFormatter = new DecimalFormat(pattern, otherSymbols);
//        String output = myFormatter.format(value);
//        return output;
//    }

    @Override
    public double get(){
        double v = super.get()*100;
        return ((double)Math.round(v))/100.0;
    }

}
