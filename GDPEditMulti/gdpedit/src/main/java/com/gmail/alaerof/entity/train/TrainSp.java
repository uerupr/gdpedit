package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import java.util.List;
import javax.swing.JComponent;

import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.entity.train.NewLine.Direction;
import com.gmail.alaerof.entity.train.NewLine.DrawCaption;

/**
 * ������� ����� �� ��������
 * 
 * @author Helen Yrofeeva
 * 
 */
public class TrainSp extends TrainThreadElement {
    /** */
    private NewLine.Direction direction;
    // ��� �������� ����� �������� �� �� ������ (�� ��� ������� �� 1)
    /** ���������� ������� */
    private List<NewLine> lines = new ArrayList<>();
    /** ������ ������� */
    private List<DistrStation> subListSt;
    /** ��������� ������� �������� */
    private TimeStation timeStBegin;
    /** �������� ������� �������� */
    private TimeStation timeStEnd;
    /** ������� ���������� �������� ����� */
    private boolean lastSp;
    /** �������������� ��������� ����������� ��� �������� */
    private NewLineSelfParams selfParams = new NewLineSelfParams();

    private void hideAllLines() {
        for (NewLine line : lines) {
            line.setVisible(false);
        }
    }

    @Override
    public void setLineBounds() {
        try {
            if (timeStBegin.hideSpan || trainThread.isHidden()) {
                hideAllLines();
            } else {
                buildLines();
                TrainThreadGeneralParams generalParams = trainThread.getGeneralParams();
                boolean zeroOnGrid = trainThread.getDistrEntity().getConfigGDP().zeroOnGrid;
                boolean viewTimeA = false;
                boolean viewTimeL = false;
                if ((lastSp || (timeStEnd.mOn != timeStEnd.mOff)) &&
                        (!("0".equals(selfParams.timeA)) || zeroOnGrid)
                        ) {
                    viewTimeA = true;
                }
                int w = 0;

                if (timeStBegin.xOff <= timeStEnd.xOn) {
                    // ��� �������� �� ���� �����
                    w = timeStEnd.xOn - timeStBegin.xOff;
                } else {
                    // ���� ������� �� ���� �����
                    int w1 = generalParams.xRight24 - timeStBegin.xOff;
                    int w2 = timeStEnd.xOn - generalParams.xLeft;
                    w = w1 + w2;
                }
                int h = 0;

                // ������� ������� ����� ������
                for (int i = 0; i < subListSt.size() - 1; i++) {
                    DistrStation st1 = subListSt.get(i);
                    DistrStation st2 = subListSt.get(i + 1);
                    h += st2.getY1() - st1.getYE1();
                }
                float xL = timeStBegin.xOff;
                int indexLine = 0;
                int indexSt = 0;
                int yTop = 0;
                boolean condition = subListSt.size() > 1;
                if (direction == Direction.left) {
                    indexSt = 0;
                } else {
                    indexSt = subListSt.size() - 2;
                }
                // ������ ������ ������ �����
                while (condition) {
                    DistrStation st1 = subListSt.get(indexSt);
                    DistrStation st2 = subListSt.get(indexSt + 1);
                    if (direction == Direction.left) {
                        indexSt++;
                        condition = indexSt < subListSt.size() - 1;
                    } else {
                        indexSt--;
                        condition = indexSt >= 0;
                    }
                    yTop = st1.getYE1();
                    int hl = st2.getY1() - st1.getYE1();
                    float wl = w * hl / h;
                    int xNext = (int) (xL + wl);
                    // ��� � ��� ������� �� ���� �����
                    if (xNext > generalParams.xRight) {

                        float w1 = 0; //= Math.max(0, generalParams.xRight - xL);
                        float w2 = 0; //= generalParams.xRight24 - generalParams.xRight;
                        float w3 = 0; //= xNext - generalParams.xRight;
                        if (xL < generalParams.xRight) {
                            w1 = generalParams.xRight - xL;
                        }
                        if (xNext > generalParams.xRight24) {
                            w2 = generalParams.xRight24 - Math.max(generalParams.xRight, xL);
                            w3 = xNext - generalParams.xRight24;
                        } else {
                            w2 = xNext - generalParams.xRight;
                        }

                        int h1 = (int) (w1 * h / w);
                        int h2 = (int) (w2 * h / w);
                        int h3 = (int) (w3 * h / w);

                        // ������ - �� ����������� � ���. ������� �� ����� �������� ���
                        if (w1 > 0) {
                            NewLine line = lines.get(indexLine);
                            viewTimeL = true;
                            Dimension dim = line.getOriginalSize();
                            if (direction == Direction.left) {
                                dim.width = (int) w1;
                                dim.height = Math.max(1, h1);
                                line.setPosition((int) xL, yTop);
                            } else {
                                dim.width = (int) w1;
                                dim.height = Math.max(1, h1);
                                line.setPosition((int) xL, (int) (yTop + h2 + h3));
                            }
                            line.setVisible(true);
                            line.repaint();

                            indexLine++;
                        }
                        // ��������� - �� ����� �������� ��� �� ����� ����� (24)
                        if (w2 > 0) {
                            // ������ �� ������, �.�. ��� ������ �� ����� ����������
                        }
                        // ����� - �� ������ �������� ��� �� �������� �� ���. �������
                        if (w3 > 0) {
                            NewLine line = lines.get(indexLine);
                            Dimension dim = line.getOriginalSize();
                            if (direction == Direction.left) {
                                dim.width = (int) w3;
                                dim.height = Math.max(1, h3);
                                line.setPosition(generalParams.xLeft, (int) (yTop + h1 + h2));
                            } else {
                                dim.width = (int) w3;
                                dim.height = Math.max(1, h3);
                                line.setPosition(generalParams.xLeft, yTop);
                            }
                            line.setVisible(true);
                            line.repaint();

                            indexLine++;
                        }
                        xL = generalParams.xLeft + w3;
                    } else {
                        // � ��� ���
                        viewTimeL = true;
                        NewLine line = lines.get(indexLine);
                        Dimension dim = line.getOriginalSize();
                        dim.width = (int) wl;
                        dim.height = Math.max(1, hl);
                        line.setPosition((int) xL, yTop);
                        line.setVisible(true);
                        line.repaint();

                        indexLine++;
                        xL += wl;
                    }
                }

                if (lines.size() > 0) {
                    NewLine line = lines.get(0);
                    line.setTimeL((!("0".equals(selfParams.timeL)) || zeroOnGrid) && viewTimeL);
                    line = lines.get(lines.size() - 1);
                    line.setTimeA(viewTimeA);
                }
            }
        } catch (Exception e) {
            logger.error("setLineBounds,  train: " +
                    trainThread.getCode() +
                    "\t" + trainThread.getNameTR() +
                    "\t stB:\t" + timeStBegin.getDistrStation().getNamePoint() +
                    "\t stB:\t" + timeStEnd.getDistrStation().getNamePoint(), e);
        }
    }

    private void buildLines() {
        try {
            TrainThreadGeneralParams params = trainThread.getGeneralParams();

            selfParams.color = timeStBegin.colorTR;
            String str = trainThread.getCode();

            if (timeStBegin.showCode == 1) {
                str = trainThread.getAInscr();
            }
            if (timeStBegin.showCode == 2) {
                str = timeStBegin.ainscr;
            }
            if (str == null)
                str = "";
            selfParams.setCaptionValue(str);

            //����� * ����� ������� ����������� ������
            LineStTrain lineStTrain = timeStBegin.getLineStTrain();
            if (lineStTrain.getSk_ps() != null) {
                selfParams.timeL = "" + timeStBegin.mOff % 10 + lineStTrain.getSk_ps();
            } else {
                selfParams.timeL = "" + timeStBegin.mOff % 10;
            }
            selfParams.timeA = "" + timeStEnd.mOn % 10;

            Direction newDirection = null;
            // ����������� ��������
            if (timeStBegin.station.getY0() <= timeStEnd.station.getY0()) {
                newDirection = NewLine.Direction.left;
            } else {
                newDirection = NewLine.Direction.right;
            }

            if (newDirection != direction) {
                lines.clear();
                direction = newDirection;
            }

            // ����� �����
            selfParams.lineStyle = timeStBegin.lineStyleSpan;
            selfParams.lineWidth = trainThread.getWidthTrain();

            // ���������� ���������� �����
            int lineCount = calcLineCount();

            int minCount = Math.min(lineCount, lines.size());
            for (int i = 0; i < minCount; i++) {
                lines.get(i).setSelfParams(selfParams);
                lines.get(i).setGeneralParams(params);
            }

            if (lineCount < lines.size()) {
                // ������� ������
                int lc = lines.size() - 1;
                for (int i = lc; i >= lineCount; i--) {
                    NewLine ln = lines.get(i);
                    owner.remove(ln);
                    lines.remove(i);
                }
            } else {
                // ��������� �����������
                int lc = lineCount - lines.size();
                for (int i = 0; i < lc; i++) {
                    NewLine ln;
                    if (direction == Direction.left) {
                        ln = new LineLeft(params, selfParams, this);
                    } else {
                        ln = new LineRight(params, selfParams, this);
                    }
                    lines.add(ln);
                    owner.add(ln, 0);
                }
            }

            if (lines.size() > 0) {
                // ������� ����������� ������� �� �����
                if (timeStBegin.showCode != -1) {
                    lines.get(0).setCaption(DrawCaption.Center);
                } else {
                    lines.get(0).setCaption(DrawCaption.NoCaption);
                }
            }
        } catch (Exception e) {
            logger.error("buildLines,  train: " + trainThread.getCode() + "\t" + trainThread.getNameTR(), e);
        }
    }

    private int calcLineCount() {
        int res = 1;
        List<DistrStation> listSt = trainThread.getDistrEntity().getListSt();
        DistrStation st1 = timeStBegin.station;
        DistrStation st2 = timeStEnd.station;
        int indx1 = listSt.indexOf(st1);
        int indx2 = listSt.indexOf(st2);
        if (indx1 > -1 && indx2 > -1) {
            subListSt = new ArrayList<>(listSt.subList(Math.min(indx1, indx2), Math.max(indx1, indx2)));
            // ������ ��������� ��� ������� 2 �������
            subListSt.add(listSt.get(Math.max(indx1, indx2)));
            res = subListSt.size() - 1; // ���-�� ����� ������ ���-�� ������� �� 1
            TrainThreadGeneralParams generalParams = trainThread.getGeneralParams();
            // ���� ���� ������� �� ���� ����� � "����������" ����� �������� � ������������ �������� ��������
            if (timeStBegin.xOff > timeStEnd.xOn && timeStBegin.xOff < generalParams.xRight) {
                res += 1;
            }
        } else {
            // ��������, ��� ���� �� 1 ������� ������ ��� ����� ������� ��
            // �������,
            // �.�. � �������� �� �����
            res = 0;
            subListSt = new ArrayList<>();
        }
        return res;
    }

    public TrainSp(JComponent owner, TrainThread trainThread, TimeStation timeSt01, TimeStation timeSt02,
            boolean lastSp) {
        super(owner, trainThread);
        this.timeStBegin = timeSt01;
        this.timeStEnd = timeSt02;
        this.lastSp = lastSp;
        setLineBounds();
    }

    @Override
    public void updateLineListener() {
        for (NewLine line : lines) {
            line.updateLineListener();
        }
    }

    @Override
    public void repaint() {
        for (NewLine line : lines) {
            line.repaint();
        }
    }

    @Override
    public void removeFromOwner() {
        for (NewLine line : lines) {
            owner.remove(line);
        }
    }

    @Override
    public void addToOwner() {
        for (NewLine line : lines) {
            owner.add(line, 0);
        }
    }

    @Override
    public TimeStation getClickedTimeSt() {
        int pos = -1;
        if (lines.size() > 1) {
            NewLine clickedLine = getClickedLine();
            if (clickedLine != null) {
                NewLine l1 = lines.get(0);
                if (clickedLine.equals(l1)) {
                    pos = 0;
                } else {
                    NewLine l2 = lines.get(lines.size() - 1);
                    if (clickedLine.equals(l2)) {
                        pos = 1;
                    }
                }

            }
        } else {
            pos = lines.get(0).getClickPosition();
        }
        switch (pos) {
        case 0:
            timeStBegin.setClickPos(1);
            return timeStBegin;
        case 1:
            timeStEnd.setClickPos(0);
            return timeStEnd;
        default:
            return null;
        }
    }

    @Override
    public TimeStation getStationBegin() {
        return timeStBegin;
    }

    @Override
    public void drawInPicture(Graphics g) {
        for (int i = 0; i < lines.size(); i++) {
            NewLine line = lines.get(i);
            if (line != null && line.isVisible())
                line.drawInPicture(g);
        }
    }
}
