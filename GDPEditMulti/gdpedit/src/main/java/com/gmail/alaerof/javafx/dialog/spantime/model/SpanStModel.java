package com.gmail.alaerof.javafx.dialog.spantime.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SpanStModel {
    private int idSpanSt;
    private int idSpan;
    private IntegerProperty num;
    private StringProperty express;
    private StringProperty esr;
    private StringProperty esr2;
    private StringProperty name;
    private DoubleProperty absKM;
    private IntegerProperty tStop;

    public SpanStModel(int idSpanSt, int idSpan, int num, String express,
                       String esr, String esr2, String name, double absKM, int tStop) {
        this.idSpanSt = idSpanSt;
        this.idSpan = idSpan;
        this.num = new SimpleIntegerProperty(num);
        this.express = new SimpleStringProperty(express);
        this.esr = new SimpleStringProperty(esr);
        this.esr2 = new SimpleStringProperty(esr2);
        this.name = new SimpleStringProperty(name);
        this.absKM = new SimpleDoubleProperty(absKM);
        this.tStop = new SimpleIntegerProperty(tStop);
    }

    public int getIdSpanSt() {
        return idSpanSt;
    }

    public int getIdSpan() {
        return idSpan;
    }

    public int getNum() {
        return num.get();
    }

    public IntegerProperty numProperty() {
        return num;
    }

    public void setNum(int num) {
        this.num.set(num);
    }

    public String getExpress() {
        return express.get();
    }

    public StringProperty expressProperty() {
        return express;
    }

    public void setExpress(String express) {
        this.express.set(express);
    }

    public String getEsr() {
        return esr.get();
    }

    public StringProperty esrProperty() {
        return esr;
    }

    public void setEsr(String esr) {
        this.esr.set(esr);
    }

    public String getEsr2() {
        return esr2.get();
    }

    public StringProperty esr2Property() {
        return esr2;
    }

    public void setEsr2(String esr2) {
        this.esr2.set(esr2);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public double getAbsKM() {
        return absKM.get();
    }

    public DoubleProperty absKMProperty() {
        return absKM;
    }

    public void setAbsKM(double absKM) {
        this.absKM.set(absKM);
    }

    public int getTStop() {
        return tStop.get();
    }

    public IntegerProperty tStopProperty() {
        return tStop;
    }

    public void setTStop(int tStop) {
        this.tStop.set(tStop);
    }
}
