package com.gmail.alaerof.javafx.dialog.distrcomb.model;

import com.gmail.alaerof.dao.dto.Distr;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DistrCombModel {
    // Name
    private StringProperty nameDistrComb;
    // IDdistrComb
    private int idDistrComb;
    // Distr
    private Distr distr;

    public DistrCombModel(Distr distr) {
        this.distr = distr;
        this.nameDistrComb = new SimpleStringProperty(distr.getName());
        this.idDistrComb = distr.getIdDistr();
    }
    public StringProperty nameDistrCombProperty() {
        return nameDistrComb;
    }

    public int getIDDistrComb() {return idDistrComb;}

    public Distr getDistr() {
        return distr;
    }

    @Override
    public String toString() {
        return nameDistrComb.get();
    }
}
