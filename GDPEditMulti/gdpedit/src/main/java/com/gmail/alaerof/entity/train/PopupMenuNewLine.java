package com.gmail.alaerof.entity.train;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import com.gmail.alaerof.dao.dto.gdp.TrainCode;
import com.gmail.alaerof.dao.dto.gdp.TrainHideSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanColor;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanInscr;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.train.NewLine.Direction;
import com.gmail.alaerof.dialog.ColorDialog;
import com.gmail.alaerof.manager.LocaleManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class PopupMenuNewLine extends JPopupMenu {
    protected Logger logger = LogManager.getLogger(PopupMenuNewLine.class);
    private static final long serialVersionUID = 1L;
    private NewLine line;
    private static ResourceBundle bundle;

    public PopupMenuNewLine() {
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.train", LocaleManager.getLocale());
    }
    // ���������� ����� ������ ���
    final JCheckBoxMenuItem itemViewCode = new JCheckBoxMenuItem(bundle.getString("train.display.train.code"));
    // ���������� ����� ������� ���
    final JCheckBoxMenuItem itemViewTrainAInscr = new JCheckBoxMenuItem(bundle.getString("train.display.train.ainscr"));
    // ���������� ���� ������� ���
    final JCheckBoxMenuItem itemViewSelfAInscr = new JCheckBoxMenuItem(bundle.getString("train.display.self.ainscr"));

    // ------ ��� ����� �������
    final JCheckBoxMenuItem itemStyleDash = new JCheckBoxMenuItem(bundle.getString("train.style.dash"));
    // -- - -- ��� ����� �����-�������
    final JCheckBoxMenuItem itemStyleDashDot = new JCheckBoxMenuItem(bundle.getString("train.style.dash.dot"));
    // $$$$ ��� ����� ���������
    final JCheckBoxMenuItem itemStyleWave = new JCheckBoxMenuItem(bundle.getString("train.style.wave"));
    // --$$-- ��� ����� ���������
    final JCheckBoxMenuItem itemStyleThisWave = new JCheckBoxMenuItem(bundle.getString("train.style.this.wave"));
    // --==-- ��� ����� � ���������
    final JCheckBoxMenuItem itemStyleThisDouble = new JCheckBoxMenuItem(bundle.getString("train.style.this.double"));

    private void initContent() {
        // ���������� ����� ������ ��� (checkbox)
        // 0 - ����� ������;
        {
            this.add(itemViewCode);
            itemViewCode.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    TimeStation timeSt = line.getTrainThreadElement().getStationBegin();
                    if (itemViewCode.isSelected()) {
                        itemViewTrainAInscr.setSelected(false);
                        itemViewSelfAInscr.setSelected(false);
                        train.setViewCode(true, TrainCode.CODE_TRAIN, timeSt.station.getIdPoint());
                    }
                    train.setViewCode(itemViewCode.isSelected(), TrainCode.CODE_TRAIN, timeSt.station.getIdPoint());
                    train.setTrainLocation();
                }
            });
        }

        // ���������� ����� ������� ��� (checkbox)
        // 1 - ����� �������;
        {
            this.add(itemViewTrainAInscr);
            itemViewTrainAInscr.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    TimeStation timeSt = line.getTrainThreadElement().getStationBegin();
                    if (itemViewTrainAInscr.isSelected()) {
                        itemViewCode.setSelected(false);
                        itemViewSelfAInscr.setSelected(false);
                    }
                    train.setViewCode(itemViewTrainAInscr.isSelected(), TrainCode.AINSCR_TRAIN, timeSt.station.getIdPoint());
                    train.setTrainLocation();
                }
            });
        }

        // ���������� ���� ������� ��� (checkbox)
        // 2 - ������ �������
        {
            this.add(itemViewSelfAInscr);
            itemViewSelfAInscr.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    TimeStation timeSt = line.getTrainThreadElement().getStationBegin();
                    if (itemViewSelfAInscr.isSelected()) {
                        itemViewTrainAInscr.setSelected(false);
                        itemViewCode.setSelected(false);
                    }
                    train.setViewCode(itemViewSelfAInscr.isSelected(), TrainCode.AINSCR_SELF, timeSt.station.getIdPoint());
                    train.setTrainLocation();
                }
            });
        }
        // ������ � ���������� ����� ������� ���
        {
            JMenuItem item = new JMenuItem(bundle.getString("train.set.train.ainscr"));
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    TimeStation timeSt = line.getTrainThreadElement().getStationBegin();
                    int idSt = timeSt.station.getIdPoint();
                    String value = null;
                    String ainscr = train.getDistrTrain().getShowParam().getAinscr();
                    if (ainscr != null) {
                        value = ainscr;
                    }
                    value = JOptionPane.showInputDialog(bundle.getString("train.set.ainscr.dialog"), value);
                    logger.debug(item.getText() + " : " + value);
                    if (value != null) {
                        itemViewSelfAInscr.setSelected(true);
                        itemViewTrainAInscr.setSelected(false);
                        itemViewCode.setSelected(false);
                        train.setAInscr(value);
                        train.setViewCode(true, TrainCode.AINSCR_TRAIN, idSt);
                        train.setTrainLocation();
                    }
                }
            });
        }
        // ������ � ���������� ���� ������� ���
        {
            JMenuItem item = new JMenuItem(bundle.getString("train.set.self.ainscr"));
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    TimeStation timeSt = line.getTrainThreadElement().getStationBegin();
                    int idSt = timeSt.station.getIdPoint();
                    String value = null;
                    TrainSpanInscr tsi = train.getDistrTrain().getTrainSpanInscr(idSt);
                    if (tsi != null) {
                        value = tsi.getAInscr();
                    }
                    value = JOptionPane.showInputDialog(bundle.getString("train.set.ainscr.dialog"), value);
                    logger.debug(item.getText() + " : " + value);
                    if (value != null) {
                        itemViewSelfAInscr.setSelected(true);
                        itemViewTrainAInscr.setSelected(false);
                        itemViewCode.setSelected(false);
                        train.setAInscrSelf(value, timeSt.station.getIdPoint());
                        train.setViewCode(true, TrainCode.AINSCR_SELF, idSt);
                        train.setTrainLocation();
                    }
                }
            });
        }
        // ������ ��� �����
        {
            JMenuItem item = new JMenuItem(bundle.getString("train.hide.line"));
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String shape = "";
                    if (line.getDirection() == Direction.hor) {
                        shape = "hor";
                    } else {
                        if (line.getDirection() == Direction.left || line.getDirection() == Direction.right) {
                            shape = "span";
                        }
                    }
                    if (shape.length() > 1) {
                        TrainThread train = line.getTrainThread();
                        int idStB = line.getTrainThreadElement().getStationBegin().station.getIdPoint();
                        train.setHidden(true, idStB, shape);
                        train.setTrainLocation();
                    }
                }
            });
        }
        // ���������� ��� ����� ����� ������
        {
            JMenuItem item = new JMenuItem(bundle.getString("train.show.all.lines"));
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    train.getDistrTrain().setMapTrainHideSpan(new HashMap<Integer, TrainHideSpan>());
                    train.updateShowParamTimeSt();
                    train.setTrainLocation();
                }
            });
        }
        // ------ ��� ����� ������� (checkbox)
        {
            this.add(itemStyleDash);
            itemStyleDash.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    if (itemStyleDash.isSelected()) {
                        itemStyleDashDot.setSelected(false);
                        itemStyleWave.setSelected(false);
                        itemStyleThisWave.setSelected(false);
                        itemStyleThisDouble.setSelected(false);
                        train.setLineStyle(LineStyle.Dash);
                    } else {
                        train.setLineStyle(LineStyle.Solid);
                    }
                    train.setTrainLocation();
                }
            });
        }
        // -- - -- ��� ����� �����-������� (checkbox)
        {
            this.add(itemStyleDashDot);
            itemStyleDashDot.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    if (itemStyleDashDot.isSelected()) {
                        itemStyleDash.setSelected(false);
                        itemStyleWave.setSelected(false);
                        itemStyleThisWave.setSelected(false);
                        itemStyleThisDouble.setSelected(false);
                        train.setLineStyle(LineStyle.DashDot);
                    } else {
                        train.setLineStyle(LineStyle.Solid);
                    }
                    train.setTrainLocation();
                }
            });
        }
        // $$$$$$ ��� ����� ��������� (checkbox)
        {
            this.add(itemStyleWave);
            itemStyleWave.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    if (itemStyleWave.isSelected()) {
                        itemStyleDash.setSelected(false);
                        itemStyleDashDot.setSelected(false);
                        itemStyleThisWave.setSelected(false);
                        itemStyleThisDouble.setSelected(false);
                        train.setLineStyle(LineStyle.Wave);
                    } else {
                        train.setLineStyle(LineStyle.Solid);
                    }
                    train.setTrainLocation();
                }
            });
        }
        // --$$-- ��� ����� ��������� (checkbox)
        {
            this.add(itemStyleThisWave);
            itemStyleThisWave.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    String shape = "span";
                    if (line.getDirection() == Direction.hor) {
                        shape = "hor";
                    }
                    TrainThread train = line.getTrainThread();
                    TimeStation timeSt = line.getTrainThreadElement().getStationBegin();
                    if (itemStyleThisWave.isSelected()) {
                        itemStyleDash.setSelected(false);
                        itemStyleDashDot.setSelected(false);
                        itemStyleWave.setSelected(false);
                        itemStyleThisDouble.setSelected(false);
                        train.setLineStyle(LineStyle.Wave, shape, timeSt.station.getIdPoint());
                    } else {
                        train.setLineStyle(train.getLineStyle(), shape,
                                timeSt.station.getIdPoint());
                    }
                    train.setTrainLocation();
                }
            });
        }
        // --==-- ��� ����� � ��������� (checkbox)
        {
            this.add(itemStyleThisDouble);
            itemStyleThisDouble.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (line.getDirection() == Direction.left || line.getDirection() == Direction.right) {
                        String shape = ";span";
                        TrainThread train = line.getTrainThread();
                        TimeStation timeSt = line.getTrainThreadElement().getStationBegin();
                        if (itemStyleThisDouble.isSelected()) {
                            itemStyleDash.setSelected(false);
                            itemStyleDashDot.setSelected(false);
                            itemStyleWave.setSelected(false);
                            itemStyleThisWave.setSelected(false);
                            train.setLineStyle(LineStyle.Double, shape, timeSt.station.getIdPoint());
                        } else {
                            train.setLineStyle(train.getLineStyle(), shape,
                                    timeSt.station.getIdPoint());
                        }
                        train.setTrainLocation();
                    }
                }
            });
        }

        // �������� ���� ���� �����
        {
            JMenuItem item = new JMenuItem(bundle.getString("train.change.color.line"));
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ColorDialog cd = GDPEdit.gdpEdit.getDialogManager().getColorDialog();
                    cd.setVisible(true);
                    if (cd.getResult() > 0) {
                        Color color = cd.getColor();
                        TrainThread train = line.getTrainThread();
                        TimeStation timeSt = line.getTrainThreadElement().getStationBegin();
                        train.setColor(color, timeSt.station.getIdPoint());
                        train.setTrainLocation();
                    }
                }
            });
        }
        // �������� ���� ����� � ������ ����� �����
        {
            JMenuItem item = new JMenuItem(bundle.getString("train.align.color.lines"));
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    train.getDistrTrain().setMapTrainSpanColor(new HashMap<Integer, TrainSpanColor>());
                    train.updateShowParamTimeSt();
                    train.setTrainLocation();
                }
            });
        }
        // ��������� � �� ����� ������
        {
            JMenuItem item = new JMenuItem(bundle.getString("train.save.db"));
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    train.saveGDPtoDB(false);
                }
            });
        }
        // �������� ��� ����� ����� ������
        {
            JMenuItem item = new JMenuItem(bundle.getString("train.repaint"));
            this.add(item);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TrainThread train = line.getTrainThread();
                    train.setTrainLocation();
                    train.repaint();
                }
            });
        }
    }

    public NewLine getLine() {
        return line;
    }

    public void setLine(NewLine line) {
        this.line = line;
        // ��������� ������� ��������
        itemViewCode.setSelected(false);
        itemViewTrainAInscr.setSelected(false);
        itemViewSelfAInscr.setSelected(false);

        TrainThreadElement element = line.getTrainThreadElement();
        if (element != null) {
            TimeStation st = element.getStationBegin();
            switch (st.showCode) {
                case 0:
                    itemViewCode.setSelected(true);
                    break;
                case 1:
                    itemViewTrainAInscr.setSelected(true);
                    break;
                case 2:
                    itemViewSelfAInscr.setSelected(true);
                    break;
                default:
                    break;
            }
        }
        itemStyleDash.setSelected(false);
        itemStyleDashDot.setSelected(false);
        itemStyleWave.setSelected(false);
        itemStyleThisWave.setSelected(false);
        itemStyleThisDouble.setSelected(false);
        if (line.getSelfParams().lineStyle == LineStyle.Dash) {
            itemStyleDash.setSelected(true);
        }
        if (line.getSelfParams().lineStyle == LineStyle.DashDot) {
            itemStyleDashDot.setSelected(true);
        }
        if (line.getSelfParams().lineStyle == LineStyle.Wave) {
            TrainThread train = line.getTrainThread();
            if (train.getLineStyle() == LineStyle.Wave) {
                itemStyleWave.setSelected(true);
            } else {
                itemStyleThisWave.setSelected(true);
            }
        }
        if (line.getSelfParams().lineStyle == LineStyle.Double) {
            itemStyleThisDouble.setSelected(true);
        }
    }
}
