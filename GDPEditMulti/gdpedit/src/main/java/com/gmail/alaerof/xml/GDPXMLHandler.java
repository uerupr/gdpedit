package com.gmail.alaerof.xml;

import com.gmail.alaerof.dao.common.LoadGDPXMLConfig;
import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.TextPrintParam;
import com.gmail.alaerof.dao.dto.energy.Energy;
import com.gmail.alaerof.dao.dto.energy.SpanEnergy;
import com.gmail.alaerof.dao.dto.energy.SpanEnergyTract;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import com.gmail.alaerof.dao.dto.routed.RouteDSpan;
import com.gmail.alaerof.dao.dto.routed.RouteDTrain;
import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.SpanStTime;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainShowParam;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.SpTOcc;
import com.gmail.alaerof.dao.dto.gdp.SpanTrain;
import com.gmail.alaerof.dao.dto.gdp.TrainCode;
import com.gmail.alaerof.dao.dto.gdp.TrainHideSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainLineStyleSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanColor;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanInscr;
import com.gmail.alaerof.dao.dto.gdp.TrainStop;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrGDPXMLDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrTrainDAO;
import com.gmail.alaerof.dao.interfacedao.IGDPTextPrintParamDAO;
import com.gmail.alaerof.dao.interfacedao.ISpanEnergyDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrLineSt;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.XMLUtil;
import com.gmail.alaerof.xml.loadgdp.DistrStruct;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.util.Pair;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * ���������� ��� ������� � xml
 * �������� ��� ������� �� xml
 */
public class GDPXMLHandler {
    private static Logger logger = LogManager.getLogger(GDPXMLHandler.class);
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.GDPXMLHandler", LocaleManager.getLocale());
    }

    /**
     * ���������� ��� ������� � ����. ��� ������� �� ��,
     * � �� �� {@link com.gmail.alaerof.entity.DistrEditEntity}
     *
     * ������� ������ ��� ��������� � ���� ����� ��������� ��� � ��
     *
     * @param file ���� ��� ����������
     * @param idDistr IDdistr
     */
    public static void saveGDPToXML(File file, int idDistr){
        Document doc = com.gmail.alaerof.util.DataXMLUtil.getNewDocument();
        Element rootElement = doc.createElement("DistrGDP");
        doc.appendChild(rootElement);
        Element version = doc.createElement("version");
        rootElement.appendChild(version);
        version.setAttribute("number", "17.0.0");

        IDistrGDPXMLDAO distrGDPXMLDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrGDPXMLDAO();
        // -- ���������� Distr --
        Element child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Distr, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Scale --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Span, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� SpanTime --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.SpanTime, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Station --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Station, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� LineSt --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.LineSt, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Span_St --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Span_St, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Wind --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Wind, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Distr_Train --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Distr_Train, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� SpanTrain --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.SpanTrain, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� LineSt_Train --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.LineSt_Tr, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� TrainStop --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.TrainStop, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� SpTOcc --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.SpTOcc, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� TrainCode --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.TrainCode, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� HideSpan --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.HideSpan, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� LineStyleSpan --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.LineStyleSpan, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� ShowParam --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.ShowParam, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� DistrComb --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.DistrComb, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Distr_Train_Foreign --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Distr_TrainF, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� LineSt_Train_Foreign --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.LineSt_TrF, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Train_SpanColor --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Train_SpanColor, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Train_SpanInscr --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Train_SpanInscr, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Category --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Category, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� LocomotiveType --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.LocomotiveType, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� Span_StTime --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.Span_StTime, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� RouteD --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.RouteD, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� RouteDSp --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.RouteDSp, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� RouteDTR --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.RouteDTR, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� TextPrintParam --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.TextPrintParam, idDistr, doc);
        rootElement.appendChild(child);

        // -- ���������� SpanEnergy --
        child = distrGDPXMLDAO.buildDistrGDPXML(IDistrGDPXMLDAO.QueryName.SpanEnergy, idDistr, doc);
        rootElement.appendChild(child);

        com.gmail.alaerof.util.DataXMLUtil.saveDocument(file, doc);
    }

    /**
     * ��������� ��� ������� �� xml � ��
     * @param domDoc �������� �� ����� xml
     * @param distrEditEntity �������
     * @param distrStruct {@link DistrStruct} ��������� � �������� �������� ������� ������������
     * @param loadGDPXMLConfig ��� ����� �� �����
     */
    public static void loadGDPFromXML(Document domDoc, DistrEditEntity distrEditEntity, DistrStruct distrStruct,
                                      LoadGDPXMLConfig loadGDPXMLConfig)
            throws PoolManagerException, DataManageException
    {
        int idDistr = distrEditEntity.getIdDistr();
        IDistrGDPXMLDAO distrGDPXMLDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrGDPXMLDAO();
        IDistrTrainDAO distrTrainDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();

        // -- Wind -- allSp
        if (loadGDPXMLConfig.loadWind) {
            distrGDPXMLDAO.loadWind(idDistr, domDoc, distrStruct.getAllSp());
        }
        // -- SpanTime -- allSp
        if (loadGDPXMLConfig.loadSpanTime) {
            distrGDPXMLDAO.loadSpanTime(domDoc, distrStruct.getAllSp(), distrStruct.getAllLoc());
        }
        // -- Span_StTime -- allSp, allLoc
        if (loadGDPXMLConfig.loadSpanStTime) {
            List<SpanStTime> spanStTimeList = loadSpanStTime(domDoc, distrStruct.getAllSp(), distrStruct.getAllLoc());
            GDPDAOFactoryCreater.getGDPDAOFactory().getSpanTimeDAO().saveSpanStTime(spanStTimeList);
        }
        // -- Route --
        if (loadGDPXMLConfig.loadRoutes) {
            logger.warn("loadRoutes doesn't work for a while !!!");
            // todo ������� �������� � �� ��������� ��� ��������� ��������
//            List<RouteD> listRoute = loadRoute(idDistr, domDoc, distrStruct.getAllSp());
//            GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO().saveRouteDs(listRoute);
        }

        // -- TextPrintParam --
        loadTextPrintParam(idDistr, domDoc);

        // -- SpanEnergy --
        loadSpanEnergy(domDoc, distrStruct.getAllSp(), distrStruct.getAllLoc());

        // -- Distr_Train -- allTrn
        loadDistrTR(idDistr, domDoc, distrStruct.getAllTrain());
        // -- SpTOcc -- allSp, allTrn
        loadSpTOcc(idDistr, domDoc, distrStruct.getAllSp(), distrStruct.getAllTrain());
        // -- LineSt_Train -- allSt, allLst, allTrn
        loadLSTTR(domDoc, distrStruct.getAllSt(), distrStruct.getAllLst(), distrStruct.getAllTrain());
        // -- TrainStop -- allTwn, allTrn
        loadTrainStop(idDistr, domDoc, distrStruct.getAllTwn(), distrStruct.getAllTrain());
        // -- SpanTrain -- allSp, allTrn
        if (loadGDPXMLConfig.loadSpanTrain) {
            loadSpanTrain(domDoc, distrStruct.getAllSp(), distrStruct.getAllTrain());
        }

        // -- TrainCode -- allSt, allTrn
        loadTrainCode(idDistr, domDoc, distrStruct.getAllSt(), distrStruct.getAllTrain());
        // -- HideSpan -- allSt, allTrn
        loadHideSpan(idDistr, domDoc, distrStruct.getAllSt(), distrStruct.getAllTrain());
        // -- LineStyleSpan -- allSt, allTrn
        loadLineStyleSpan(idDistr, domDoc, distrStruct.getAllSt(), distrStruct.getAllTrain());
        // -- Train_SpanColor -- allSt, allTrn
        loadSpanColor(idDistr, domDoc, distrStruct.getAllSt(), distrStruct.getAllTrain());
        // -- Train_SpanInscr -- allSt, allTrn
        loadSpanInscr(idDistr, domDoc, distrStruct.getAllSt(), distrStruct.getAllTrain());
        // -- ShowParam -- allTrn (��� ������������)
        // Distr_TrainF ����� �� ������������ �.�. ����� ������ ��������� ����������� ShowParam
        loadShowParam(domDoc, distrStruct.getAllTrain(), distrStruct.getAllDistrComb());

        Collection<DistrTrain> distrTrains = distrStruct.getDistrTrains();
        distrTrainDAO.saveListTrainGDP(distrTrains, idDistr);
        distrTrainDAO.saveListTrainGDP_ShowParam(distrTrains, idDistr);

        if (loadGDPXMLConfig.loadDistrAppearance) {
            // todo ������� ����� ����������.. �������
            // HDtxt, PARAMtxt - ��������� ����� � ����� + ����������
            //distrGDPXMLDAO.loadHDPARAM(idDistr, domDoc);
            // -- Scale -- allSp
            distrGDPXMLDAO.loadAppearanceScale(idDistr, distrStruct.getAllSp());
            // -- LineSt --   (�������/����� �����) allLst
            distrGDPXMLDAO.loadAppearanceLineSt(distrStruct.getAllLst());
            // -- DistrComb -- allDistr
            distrGDPXMLDAO.loadDistrComb(idDistr,distrStruct.getAllDistrComb());
        }

        // �������� ���
        distrTrainDAO.updateSourceGDP(idDistr, loadGDPXMLConfig.sourceGDP);
    }

    /**
     * ��������� ��� ��� ������������� ������ ���
     * @param doc xml
     * @return boolean
     */
    public static boolean firstCheck4Format(Document doc) {
        NodeList nodeList = doc.getElementsByTagName("DistrGDP");
        if (nodeList.getLength() == 0) {
            // ������ ��� ����� �� xml � ��������
            return false;
        }
        return true;
    }

    /**
     * ��������� ���������� idDistr � �������� �� doc � distrEditEntity
     * @param doc from XML
     * @param distrEditEntity {@link DistrEditEntity}
     * @return name from XML
     */
    public static String firstCheck4Distr(Document doc, DistrEditEntity distrEditEntity) {
        NodeList nodeList = doc.getElementsByTagName("Distr");
        Element item = (Element) nodeList.item(0);
        String name = item.getAttribute("name");
        if (!name.equals(distrEditEntity.getDistrEntity().getDistrName())) {
            return name;
        }
        return "";
    }

    /**
     * ���������� ��������� ������� �� xml �� ������������ ��������� � ��
     * @param domDoc xml
     * @param distrEditEntity ������� �� ��
     * @param express ���� true �� �������� �����. ������� �� Express, ����� �� ESR+ESR2
     * @return {@link DistrStruct} ��������� ������������ � ���������
     */
    public static DistrStruct checkAllDistrStruct(Document domDoc, DistrEditEntity distrEditEntity, boolean express){
        DistrStruct distrStruct = new DistrStruct();
        // �������� � �� ������� ���� ��������� ��������� �������: �������
        NodeList nodeList = domDoc.getElementsByTagName("Stations");
        if (nodeList.getLength() > 0) {
            Element element = (Element) nodeList.item(0);
            CheckStations(element, distrEditEntity, distrStruct, express);
        }
        // �������� � �� ������� ���� ��������� ��������� �������: ���� �������
        nodeList = domDoc.getElementsByTagName("LineSts");
        if(nodeList.getLength()>0) {
            CheckLineSts((Element)nodeList.item(0), distrEditEntity, distrStruct);
        }
        // �������� � �� ������� ���� ��������� ��������� �������: ��������
        nodeList = domDoc.getElementsByTagName("Spans");
        if(nodeList.getLength()>0) {
            CheckSpans((Element)nodeList.item(0), distrEditEntity, distrStruct);
        }
        // �������� � �� ������� ���� ��������� ��������� �������: ������������ ������
        nodeList = domDoc.getElementsByTagName("Span_Sts");
        if(nodeList.getLength()>0) {
            CheckTowns((Element)nodeList.item(0), distrEditEntity, distrStruct, express);
        }
        // �������� � �� ������� ���� ��������� ��������� �������: ���� �����������
        nodeList = domDoc.getElementsByTagName("LocomotiveTypes");
        if (nodeList.getLength() > 0) {
            CheckLocTypes((Element) nodeList.item(0), distrEditEntity, distrStruct);
        }
        // �������� � �� ���. �������� (DistrComb)
        nodeList = domDoc.getElementsByTagName("DistrCombs");
        if (nodeList.getLength() > 0) {
            CheckDistrCombs((Element) nodeList.item(0), distrStruct);
        }
        return distrStruct;
    }

    /**
     * �������� � �� ���. �������� (DistrComb)
     * @param rootItem xml
     * @param distrStruct {@link DistrStruct} ��������� ������������ � ���������
     */
    private static void CheckDistrCombs(Element rootItem, DistrStruct distrStruct) {
        List<Distr> distrList = null;
        try {
            IDistrDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO();
            distrList = dao.getDistrList(0);
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
        NodeList nodeList = rootItem.getElementsByTagName("DistrComb");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            // �������� ���������� �� �����
            int idDistrCXML = XMLUtil.getAttributeInt(item, "IDdistrC");
            String nameCXML = XMLUtil.getAttribute(item, "name");
            Distr distr = findDistr(distrList, nameCXML);
            if (distr != null) {
                distrStruct.getAllDistrComb().put(idDistrCXML, new Pair<>(item, distr));
            } else {
                // * ���. ������� �� ������ � ��, ���� ���. ������� ����������� �� �����:
                String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.distrcomb.notfound") +
                        " '" + nameCXML + "'";
                distrStruct.getWarningList().add(warnMessage);
                logger.error(warnMessage);
            }
        }
    }

    private static Distr findDistr(List<Distr> distrList, String nameCXML) {
        for (Distr distr : distrList) {
            if (nameCXML.equals(distr.getName())) {
                return distr;
            }
        }
        return null;
    }

    /**
     * �������� � �� ������� ���� ��������� ��������� �������: ���� �����������
     * @param rootItem xml
     * @param distrEditEntity ������� �� ��
     * @param distrStruct {@link DistrStruct} ��������� ������������ � ���������
     */
    private static void CheckLocTypes(Element rootItem, DistrEditEntity distrEditEntity, DistrStruct distrStruct) {
        NodeList nodeList = rootItem.getElementsByTagName("LocomotiveType");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            // �������� ���������� �� �����
            int idLTypeXML = XMLUtil.getAttributeInt(item, "IDltype");
            String ltypeXML = XMLUtil.getAttribute(item, "ltype");
            LocomotiveType ltypeDB = findLocomotiveType(distrEditEntity.getDistrEntity(), ltypeXML);
            if (ltypeDB != null) {
                distrStruct.getAllLoc().put(idLTypeXML, new Pair<>(item, ltypeDB));
            } else {
                // * ��� �������� �� ������ � ��
                String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.loctype.notfound") +
                        " '" + ltypeXML + "'";
                distrStruct.getWarningList().add(warnMessage);
                logger.error(warnMessage);
            }
        }
    }

    private static LocomotiveType findLocomotiveType(DistrEntity distrEntity, String ltypeXML) {
        for (LocomotiveType locomotiveType : distrEntity.getListDistrLoc()) {
            if (locomotiveType.getLtype().equals(ltypeXML)) {
                return locomotiveType;
            }
        }
        return null;
    }

    /**
     * �������� � �� ������� ���� ��������� ��������� �������: ������������ ������
     * @param rootItem xml
     * @param distrEditEntity ������� �� ��
     * @param distrStruct {@link DistrStruct} ��������� ������������ � ���������
     * @param express
     */
    private static void CheckTowns(Element rootItem, DistrEditEntity distrEditEntity, DistrStruct distrStruct, boolean express) {
        NodeList nodeList = rootItem.getElementsByTagName("Span_St");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            // �������� ���������� �� �����
            int idSpanXML = XMLUtil.getAttributeInt(item, "IDspan");
            int idSpanStXML = XMLUtil.getAttributeInt(item, "IDspanst");
            String nameSpanStXML = XMLUtil.getAttribute(item, "name");
            String codeExpress = XMLUtil.getAttribute(item, "CodeExpress");
            String codeESR = XMLUtil.getAttribute(item, "CodeESR");
            String codeESR2 = XMLUtil.getAttribute(item, "CodeESR2");
            Pair<Element, Scale> scalePair = distrStruct.getAllSp().get(idSpanXML);
            if (scalePair != null) {
                Scale scale = scalePair.getValue();
                SpanSt spanSt;
                String messageSt;
                if (express) {
                    // c ����� �������� =
                    messageSt = bundle.getString("xml.loadgpd.checkdistrstruct.station.express") + codeExpress;
                    spanSt = findSpanStExpress(scale, codeExpress);
                } else {
                    // � ����� ��� =
                    messageSt = bundle.getString("xml.loadgpd.checkdistrstruct.station.esr") + codeESR + " " + codeESR2;
                    spanSt = findSpanStESR(scale, codeESR, codeESR2);
                }
                if (spanSt != null) {
                    distrStruct.getAllTwn().put(idSpanStXML, new Pair<>(item, spanSt));
                    // �������� �� ���������� �������� �.�.
                    if (!nameSpanStXML.equals(spanSt.getName())) {
                        // * �������� �.�. � ����� �� ��������� � ��������� � ��, ����� ����������� ��������� � ��
                        String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.spanst.name") +
                                " " + messageSt +
                                " '" + nameSpanStXML + "'" +
                                " '" + spanSt.getName() + "'";
                        distrStruct.getWarningList().add(warnMessage);
                        logger.error(warnMessage);
                    }
                } else {
                    // * �.�. �� ����� �� ��������� � ��
                    String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.spanst.notfound") +
                            " " + messageSt +
                            " '" + nameSpanStXML + "'";
                    distrStruct.getWarningList().add(warnMessage);
                    logger.error(warnMessage);                }
            } else {
                // * ������� ��� �.�. �� ����� �� ��������� � ��
                String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.spanst.span") +
                        " IDspan(xml)=" + idSpanXML +
                        ";  '" + nameSpanStXML + "'";
                distrStruct.getWarningList().add(warnMessage);
                logger.error(warnMessage);
            }
        }
    }

    private static SpanSt findSpanStESR(Scale scale, String codeESR, String codeESR2) {
        for (SpanSt spanSt : scale.getSpanSt()) {
            if (spanSt.getCodeESR().equals(codeESR) && spanSt.getCodeESR2().equals(codeESR2)) {
                return spanSt;
            }
        }
        return null;
    }

    private static SpanSt findSpanStExpress(Scale scale, String codeExpress) {
        for (SpanSt spanSt : scale.getSpanSt()) {
            if (spanSt.getCodeExpress().equals(codeExpress)) {
                return spanSt;
            }
        }
        return null;
    }

    /**
     * �������� � �� ������� ���� ��������� ��������� �������: ��������
     * @param rootItem xml
     * @param distrEditEntity ������� �� ��
     * @param distrStruct {@link DistrStruct} ��������� ������������ � ���������
     */
    private static void CheckSpans(Element rootItem, DistrEditEntity distrEditEntity, DistrStruct distrStruct) {
        NodeList nodeList = rootItem.getElementsByTagName("Span");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            // �������� ���������� �� �����
            int idSpanXML = XMLUtil.getAttributeInt(item, "IDspan");
            String nameSpXML = XMLUtil.getAttribute(item, "Name");
            int idStBXML = XMLUtil.getAttributeInt(item, "IDst");
            int idStEXML = XMLUtil.getAttributeInt(item, "IDst_e");
            if (nameSpXML.length() > 0) {
                Pair<Element, Station> stBPair = distrStruct.getAllSt().get(idStBXML);
                Pair<Element, Station> stEPair = distrStruct.getAllSt().get(idStEXML);
                if (stBPair != null && stEPair != null) {
                    Station stB = stBPair.getValue();
                    Station stE = stEPair.getValue();
                    DistrSpan distrSpan = distrEditEntity.getDistrEntity().getExactDistrSpan(stB.getIdSt(), stE.getIdSt());
                    if (distrSpan != null) {
                        distrStruct.getAllSp().put(idSpanXML, new Pair<>(item, distrSpan.getSpanScale()));
                        // ������� �� ������������ ��������
                        if (!nameSpXML.equals(distrSpan.getSpanScale().getName())) {
                            // * �������� �������� � ����� �� ��������� � ��������� � ��, ����� ����������� ��������� � ��
                            String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.span.namematch") +
                                    ": xml='" + nameSpXML + "'" +
                                    "; db= '" + distrSpan.getSpanScale().getName();
                            distrStruct.getWarningList().add(warnMessage);
                            logger.error(warnMessage);
                        }
                    } else {
                        // * �� ������ �������:
                        String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.span.notfound") +
                                " '" + nameSpXML + "'" +
                                "; stB = '"+ stB.getName()+ "'"+
                                "; stE = '"+ stE.getName() + "'";
                        distrStruct.getWarningList().add(warnMessage);
                        logger.error(warnMessage);
                    }
                } else {
                    // * �� ������� ��������� ������� ��� ��������:
                    String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.span.nost") +
                            " '" + nameSpXML + "'";
                    distrStruct.getWarningList().add(warnMessage);
                    logger.error(warnMessage);
                }
            } else {
                //  * � ����� ���� ������� ��� �������� !!!
                String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.span.noname");
                distrStruct.getWarningList().add(warnMessage);
                logger.error(warnMessage);
            }
        }
    }

    /**
     * �������� � �� ������� ���� ��������� ��������� �������: ���� �������
     * @param rootItem xml
     * @param distrEditEntity ������� �� ��
     * @param distrStruct {@link DistrStruct} ��������� ������������ � ���������
     */
    private static void CheckLineSts(Element rootItem, DistrEditEntity distrEditEntity, DistrStruct distrStruct) {
        NodeList nodeList = rootItem.getElementsByTagName("LineSt");
        String warn = bundle.getString("xml.loadgpd.checkdistrstruct.linest");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            // �������� ���������� �� �����
            int idStXML = XMLUtil.getAttributeInt(item, "IDst");
            int idlineStXML = XMLUtil.getAttributeInt(item, "IDlineSt");
            String psXML = XMLUtil.getAttribute(item, "PS");
            if (!"".equals(psXML)) {
                Pair<Element, Station> stationPair = distrStruct.getAllSt().get(idStXML);
                if (stationPair != null) {
                    int idStDB = stationPair.getValue().getIdSt();

                    LineSt lineStDB = findLineSt(distrEditEntity.getDistrEntity(), idStDB, psXML);
                    if (lineStDB != null) {
                        distrStruct.getAllLst().put(idlineStXML, new Pair<>(item, lineStDB));
                    } else {
                        // ���� ������� �� ������ � ��
                        String warnMessage = warn +
                                " IDst(xml)=" + idStXML +
                                "; IDlineSt(xml)=" + idlineStXML +
                                "; PS(xml)=" + psXML +
                                "; station(DB)=" + stationPair.getValue().getName();
                        distrStruct.getWarningList().add(warnMessage);
                        logger.error(warnMessage);
                    }
                } else {
                    // ���� �� ������ � ��
                    String warnMessage = warn +
                            " IDst(xml)=" + idStXML +
                            "; IDlineSt(xml)=" + idlineStXML +
                            "; PS(xml)=" + psXML;
                    distrStruct.getWarningList().add(warnMessage);
                    logger.error(warnMessage);
                }
            }
            /* �� �������, ������ ��� ����� ������� ����� � ��� �� �� �����.
            else{
                // ���� � ����� ��� ��������
                String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.lineSt.noname") +
                        " IDst(xml)=" + idStXML +
                        "; IDlineSt(xml)=" + idlineStXML +
                        "; PS(xml)=" + psXML;
                logger.error(warnMessage);

            }
            */
        }
    }

    private static LineSt findLineSt(DistrEntity distrEntity, int idStDB, String psXML) {
        DistrStation distrStation = distrEntity.getDistrStationFromAll(idStDB);
        for(DistrLineSt distrLineSt: distrStation.getListLines()){
            if(distrLineSt.getLineSt().getPs().equals(psXML)){
                return distrLineSt.getLineSt();
            }
        }
        return null;
    }

    /**
     * �������� � �� ������� ���� ��������� ��������� �������: �������
     * @param rootItem Stations �� xml
     * @param distrEditEntity ������� �� ��
     * @param distrStruct {@link DistrStruct} ��������� ������������ � ���������
     * @param express ���� true �� �������� �����. �� Express, ����� �� ESR+ESR2
     */
    private static void CheckStations(Element rootItem, DistrEditEntity distrEditEntity, DistrStruct distrStruct, boolean express) {
        NodeList nodeList = rootItem.getElementsByTagName("Station");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            // �������� ���������� �� �����
            int idStXML = XMLUtil.getAttributeInt(item,"IDst");
            String nameStXML = XMLUtil.getAttribute(item,"name");
            String codeExpress = XMLUtil.getAttribute(item, "CodeExpress");
            String codeESR = XMLUtil.getAttribute(item, "CodeESR");
            String codeESR2 = XMLUtil.getAttribute(item, "CodeESR2");
            String messageSt;
            Station stationDB;
            if (express) {
                // c ����� �������� =
                messageSt = bundle.getString("xml.loadgpd.checkdistrstruct.station.express") + codeExpress;
                // ���� �����. ������� �� ��
                stationDB = findStEspress(distrEditEntity.getDistrEntity(), codeExpress);
            } else {
                // � ����� ��� =
                messageSt = bundle.getString("xml.loadgpd.checkdistrstruct.station.esr") + codeESR + " " + codeESR2;
                // ���� �����. ������� �� ��
                stationDB = findStESR(distrEditEntity.getDistrEntity(), codeESR, codeESR2);
            }
            // ��������� ������� � ������ �������
            if (stationDB != null) {
                distrStruct.getAllSt().put(idStXML, new Pair<>(item, stationDB));

                // �������� �� ���������� �������� �������
                if (!nameStXML.equals(stationDB.getName())) {
                    // * �������� ������� � ����� �� ��������� � ��������� � ��, ����� ������������ ��������� � ��
                     String warnMessage = bundle.getString("xml.loadgpd.checkdistrstruct.station.name") +
                            messageSt +
                            " '" + nameStXML + "'" +
                            " '" + stationDB.getName() + "'";
                    distrStruct.getWarningList().add(warnMessage);
                    logger.error(warnMessage);
                }
            } else {
                String mess1 = bundle.getString("xml.loadgpd.checkdistrstruct.station.notfound");
                String mess2 = bundle.getString("xml.loadgpd.checkdistrstruct.station.namexml");
                String warnMessage = mess1 + " " + messageSt + ", " +mess2 + " '" + nameStXML + "'";
                distrStruct.getWarningList().add(warnMessage);
                logger.error(warnMessage);
            }
        }
   }

    private static Station findStESR(DistrEntity distrEntity, String codeESR, String codeESR2) {
        for (DistrStation distrStation : distrEntity.getMapSt().values()) {
            if (distrStation.getStation().getCodeESR().equals(codeESR) &&
                    distrStation.getStation().getCodeESR2().equals(codeESR2)) {
                return distrStation.getStation();
            }
        }
        return null;
    }

    private static Station findStEspress(DistrEntity distrEntity, String codeExpress) {
        for (DistrStation distrStation : distrEntity.getMapSt().values()) {
            if (distrStation.getStation().getCodeExpress().equals(codeExpress)) {
                return distrStation.getStation();
            }
        }
        return null;
    }

    /**
     * ��������� ������ SpanEnergy � Access-���� �� xml
     * @param domDoc xml
     */
    private static void loadSpanEnergy(Document domDoc, Map<Integer, Pair<Element, Scale>> allSp,
                                       Map<Integer, Pair<Element, LocomotiveType>> allLoc) throws DataManageException {
        Map<Integer, Pair<Element, Scale>> allScale = buildScaleMap(allSp);
        ISpanEnergyDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getSpanEnergyDAO();
        NodeList nodeList = domDoc.getElementsByTagName("SpanEnergy");
        List<SpanEnergyTract> spanEnergyTractList = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            SpanEnergyTract spanEnergyTract = new SpanEnergyTract();
            Element item = (Element) nodeList.item(i);
            int idScaleXML = XMLUtil.getAttributeInt(item, "IDscale");
            int idLocXML = XMLUtil.getAttributeInt(item, "IDltype");
            Pair<Element, Scale> pairS =  allScale.get(idScaleXML);
            Pair<Element, LocomotiveType> pairL =  allLoc.get(idLocXML);
            if (pairS != null && pairL != null) {
                spanEnergyTract.setIdScale(pairS.getValue().getIdScale());
                LocomotiveType locomotiveType = new LocomotiveType();
                locomotiveType.setIdLtype(pairL.getValue().getIdLType());
                spanEnergyTract.setLocomotiveType(locomotiveType);
                Energy energy = new Energy();
                energy.setEnMove((float)XMLUtil.getAttributeDouble(item, "EnMove"));
                energy.setEnDown((float)XMLUtil.getAttributeDouble(item, "EnDown"));
                energy.setEnUp((float)XMLUtil.getAttributeDouble(item, "EnUp"));
                spanEnergyTract.setEnergy(energy);
                OE oe = OE.getValue(XMLUtil.getAttribute(item, "oe"));
                spanEnergyTract.setOe(oe);
                spanEnergyTractList.add(spanEnergyTract);
                //dao.saveSpanEnergyTract(spanEnergyTract);
            }
        }
        dao.saveSpanEnergyTractList(spanEnergyTractList);
    }

    /**
     * ��������� ������ TextPrintParam � Access-���� �� xml
     * @param idDistr id �������
     * @param domDoc xml
     */
    private static void loadTextPrintParam(int idDistr, Document domDoc) throws DataManageException {
        NodeList nodeList = domDoc.getElementsByTagName("TextPrintParam");
        List<TextPrintParam> listTextPrintParam = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            int tpType = XMLUtil.getAttributeInt(item, "tpType");
            int choise = XMLUtil.getAttributeInt(item, "choise");
            String textField = XMLUtil.getAttribute(item, "textField");
            String fontField = XMLUtil.getAttribute(item, "fontField");
            int num = XMLUtil.getAttributeInt(item, "num");
            listTextPrintParam.add(new TextPrintParam(idDistr, tpType, choise, textField, fontField, num));
        }
        IGDPTextPrintParamDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getTextPrintParamDAO();
        dao.deleteTextPrintParam(idDistr);
        dao.addTextPrintParam(listTextPrintParam);
    }

    /**
     * ��������� ������ �� xml � DistrTrain
     * @param idDistr id �������
     * @param domDoc xml
     * @param allTrain ������ ������������ � �� DistrTrain ��� ����������
     */
    private static void loadDistrTR(int idDistr, Document domDoc, Map<Long, Pair<Element, DistrTrain>> allTrain) {
        NodeList nodeList = domDoc.getElementsByTagName("Distr_Train");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idXML = XMLUtil.getAttributeLong(item, "IDtrain");
            Pair<Element, DistrTrain> pair =  allTrain.get(idXML);
            if (pair != null) {
                DistrTrain train = pair.getValue();
                train.setIdDistr(idDistr); // ��� '����' ������, ������� � ��� idDistr � ����� ����� �������
                //atr = getAttribute(item, "Name"); ��� �������� ��� ��������
                //atr = getAttribute(item, "code"); ��� �������� ��� ��������
                //atr = getAttribute(item, "oe"); ��� ��������� �� code � �������� ��� ��������
                //atr = getAttribute(item, "IDcat"); ��� ��������� �� code � �������� ��� ��������
                //atr = getAttribute(item, "type"); ��� ��������� �� code � �������� ��� ��������

                train.setOccupyOff(XMLUtil.getAttribute(item, "occupyOff"));
                train.setTimeWithStop(XMLUtil.getAttributeDouble(item, "Tu"));
                train.setTimeWithoutStop(XMLUtil.getAttributeDouble(item, "Tt"));
                //atr = getAttribute(item, "Vu"); // �� �����
                //atr = getAttribute(item, "Vt"); // �� �����
                train.setLen(XMLUtil.getAttributeDouble(item, "L"));
                //atr = getAttribute(item, "el"); // ��������
                //atr = getAttribute(item, "ps"); // ��������
                //atr = getAttribute(item, "Messages"); // �� ��� ��� � �����

                // ��������� ����������� ��� '�����' ����� ��������� � Distr_Train �� ��� ���, ���� �� ��������� �� ������ ���������
                DistrTrainShowParam param = train.getShowParam();
                param.setHidden(XMLUtil.getAttributeBoolean(item, "hidden"));
                param.setShowCodeI(XMLUtil.getAttributeBoolean(item, "ShowCodeI"));
                param.setColorTR(XMLUtil.getAttributeInt(item, "colorTR"));
                param.setAinscr(XMLUtil.getAttribute(item, "AInscr"));
                param.setLineStyle(XMLUtil.getAttributeInt(item, "LineStyle"));
                param.setWidthTR(XMLUtil.getAttributeInt(item, "widthTR"));

                train.settOn(XMLUtil.getAttribute(item, "tOn"));
                train.settOff(XMLUtil.getAttribute(item, "tOff"));
                train.settOn_e(XMLUtil.getAttribute(item, "tOn_e"));
                train.settOff_e(XMLUtil.getAttribute(item, "tOff_e"));
            }
        }
    }

    /**
     * ��������� ��������� �� XML � DistrTrain (allTrain)
     * @param idDistr id ������� (�� �� ���� ������ � ���������)
     * @param domDoc xml
     * @param allSp ��������
     * @param allTrain ������
     */
    private static void loadSpTOcc(int idDistr, Document domDoc, Map<Integer, Pair<Element, Scale>> allSp,
                                   Map<Long, Pair<Element, DistrTrain>> allTrain) {
        // IDspan, IDtrain, IDdistr, occupyOn, occupyOff, oe, Tmove, NextDay
        NodeList nodeList = domDoc.getElementsByTagName("SpTOcc");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idSpXML = XMLUtil.getAttributeInt(item, "IDspan");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Scale> pairSp =  allSp.get(idSpXML);
            if (pairTr != null && pairSp != null) {
                DistrTrain train = pairTr.getValue();
                Scale scale = pairSp.getValue();
                SpTOcc spTOcc = new SpTOcc();

                spTOcc.setIdSpan(scale.getIdSpan());
                spTOcc.setIdTrain(train.getIdTrain());
                spTOcc.setIdDistr(idDistr);

                spTOcc.setOccupyOn(XMLUtil.getAttribute(item, "occupyOn"));
                spTOcc.setOccupyOff(XMLUtil.getAttribute(item, "occupyOff"));
                spTOcc.setOe(XMLUtil.getAttribute(item, "oe"));
                spTOcc.setTmove(XMLUtil.getAttributeInt(item, "Tmove"));
                spTOcc.setNextDay(XMLUtil.getAttributeInt(item, "NextDay"));

                Map<Integer, SpTOcc> spTOccMap = train.getMapSpTOcc();
                spTOccMap.put(scale.getIdSpan(), spTOcc);
            }
        }
    }

    /**
     * �������������� ������� ���� �� XML � DistrTrain (allTrain)
     * @param domDoc xml
     * @param allSp ��������
     * @param allTrain ������
     */
    private static void loadSpanTrain(Document domDoc, Map<Integer, Pair<Element, Scale>> allSp,
                                      Map<Long, Pair<Element, DistrTrain>> allTrain) {
        // SpanTrain: IDspan, IDtrain, Tmove, Tdown, Tup, Tw
        NodeList nodeList = domDoc.getElementsByTagName("SpanTrain");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idSpXML = XMLUtil.getAttributeInt(item, "IDspan");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Scale> pairSp =  allSp.get(idSpXML);
            if (pairTr != null && pairSp != null) {
                DistrTrain train = pairTr.getValue();
                Scale scale = pairSp.getValue();
                SpanTrain spanTrain = new SpanTrain();

                spanTrain.setIdSpan(scale.getIdSpan());
                spanTrain.setIdTrain(train.getIdTrain());

                Time time = new Time();
                time.setTimeMove((float)XMLUtil.getAttributeDouble(item, "Tmove"));
                time.setTimeMove((float)XMLUtil.getAttributeDouble(item, "Tdown"));
                time.setTimeMove((float)XMLUtil.getAttributeDouble(item, "Tup"));
                time.setTimeMove((float)XMLUtil.getAttributeDouble(item, "Tw"));
                spanTrain.setTime(time);

                Map<Integer, SpanTrain> spanTrainMap = train.getMapSpanTrain();
                spanTrainMap.put(scale.getIdSpan(), spanTrain);
            }
        }
    }

    /**
     * ���������� �������� �� ��������
     * @param domDoc xml
     * @param allSt �������
     * @param allLst ���� �������
     * @param allTrain ������
     */
    private static void loadLSTTR(Document domDoc,
                                  Map<Integer, Pair<Element, Station>> allSt,
                                  Map<Integer, Pair<Element, LineSt>> allLst,
                                  Map<Long, Pair<Element, DistrTrain>> allTrain)
    {
        // LineSt_Tr: IDtrain, IDst, IDdistr, IDLineSt, occupyOn, occupyOff, Tstop, TtexStop, sk_ps, overTime, TstopPL, Stop, endLeave
        NodeList nodeList = domDoc.getElementsByTagName("LineSt_Tr");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idStXML = XMLUtil.getAttributeInt(item, "IDst");
            int idLStXML = XMLUtil.getAttributeInt(item, "IDlineSt");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Station> pairSt =  allSt.get(idStXML);
            Pair<Element, LineSt> pairLSt = allLst.get(idLStXML);
            int idLineSt = pairLSt != null ? pairLSt.getValue().getIdLineSt() : 0;
            if (pairTr != null && pairSt != null) {
                DistrTrain train = pairTr.getValue();
                Station station = pairSt.getValue();
                LineStTrain lineStTrain = new LineStTrain();

                lineStTrain.setIdTrain(train.getIdTrain());
                lineStTrain.setIdSt(station.getIdSt());
                lineStTrain.setIdLineSt(idLineSt);

                String atr = XMLUtil.getAttribute(item, "occupyOn");
                if (atr.length() > 0) {
                    lineStTrain.setOccupyOn(atr);
                }
                atr = XMLUtil.getAttribute(item, "occupyOff");
                if (atr.length() > 0) {
                    lineStTrain.setOccupyOff(atr);
                }
                lineStTrain.setTstop(XMLUtil.getAttributeInt(item, "Tstop"));
                lineStTrain.setTexStop(XMLUtil.getAttributeInt(item, "TtexStop"));
                lineStTrain.setSk_ps(XMLUtil.getAttribute(item, "sk_ps"));
                lineStTrain.setOverTime(XMLUtil.getAttributeInt(item, "overTime"));
                lineStTrain.setTstopPL(XMLUtil.getAttributeInt(item, "TstopPL"));
                lineStTrain.setStop(XMLUtil.getAttributeInt(item, "Stop"));
                // getAttribute(item, "endLeave"); // �� ��� ��� � �����

                Map<Integer, LineStTrain> lineStTrainMap = train.getMapLineStTr();
                lineStTrainMap.put(station.getIdSt(), lineStTrain);
            }
        }
    }

    /**
     * ������� �� �.�. �� xml � DistrTrain
     * @param idDistr id ������� (�� �� ���� ������ � ���������)
     * @param domDoc xml
     * @param allTwn ��������� �.�.
     * @param allTrain ������
     */
    private static void loadTrainStop(int idDistr, Document domDoc,
                                      Map<Integer, Pair<Element, SpanSt>> allTwn,
                                      Map<Long, Pair<Element, DistrTrain>> allTrain)
    {
        // TrainStop: IDtrain, IDst, IDdistr, IDLineSt, occupyOn, occupyOff, Tstop, TtexStop, sk_ps, overTime, TstopPL, Stop, endLeave
        NodeList nodeList = domDoc.getElementsByTagName("TrainStop");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idStXML = XMLUtil.getAttributeInt(item, "IDspanst");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, SpanSt> pairSt =  allTwn.get(idStXML);
            if (pairTr != null && pairSt != null) {
                DistrTrain train = pairTr.getValue();
                SpanSt station = pairSt.getValue();
                TrainStop trainStop = new TrainStop();

                trainStop.setIdTrain(train.getIdTrain());
                trainStop.setIdSpanst(station.getIdSpanst());
                trainStop.setIdDistr(idDistr);

                trainStop.setOccupyOn(XMLUtil.getAttribute(item, "occupyOn"));
                trainStop.setOccupyOff(XMLUtil.getAttribute(item, "occupyOff"));
                trainStop.setTstop(XMLUtil.getAttributeInt(item, "Tstop"));
                trainStop.setSk_ps(XMLUtil.getAttribute(item, "sk_ps"));
                // getAttribute(item, "endLeave"); // �� ��� ��� � �����

                Map<Integer, TrainStop> trainStopMap  = train.getMapTrainStop();
                trainStopMap.put(station.getIdSpanst(), trainStop);
            }
        }
    }

    /**
     * ��������� ����������� ������� ������� �� xml � DistrTrain
     * @param idDistr id ������� (�� �� ���� ������ � ���������)
     * @param domDoc xml
     * @param allSt ��������� �������
     * @param allTrain ������
     */
    private static void loadTrainCode(int idDistr, Document domDoc,
                                      Map<Integer, Pair<Element, Station>> allSt,
                                      Map<Long, Pair<Element, DistrTrain>> allTrain)
    {
        // TrainCode: IDtrain, IDdistr, IDstB, ViewCode
        NodeList nodeList = domDoc.getElementsByTagName("TrainCode");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idStXML = XMLUtil.getAttributeInt(item, "IDstB");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Station> pairSt =  allSt.get(idStXML);
            if (pairTr != null && pairSt != null) {
                DistrTrain train = pairTr.getValue();
                Station station = pairSt.getValue();
                TrainCode value = new TrainCode();

                value.setIdTrain(train.getIdTrain());
                value.setIdStB(station.getIdSt());
                value.setIdDistr(idDistr);

                value.setViewCode(XMLUtil.getAttributeInt(item, "ViewCode"));

                Map<Integer, TrainCode> map  = train.getMapTrainCode();
                map.put(station.getIdSt(), value);
            }
        }
    }

    /**
     * HideSpan �� xml � DistrTrain
     * @param idDistr id ������� (�� �� ���� ������ � ���������)
     * @param domDoc xml
     * @param allSt ��������� �������
     * @param allTrain ������
     */
    private static void loadHideSpan(int idDistr, Document domDoc,
                                     Map<Integer, Pair<Element, Station>> allSt,
                                     Map<Long, Pair<Element, DistrTrain>> allTrain)
    {
        // HideSpan: IDtrain, IDstB, IDdistr, shape
        NodeList nodeList = domDoc.getElementsByTagName("HideSpan");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idStXML = XMLUtil.getAttributeInt(item, "IDstB");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Station> pairSt =  allSt.get(idStXML);
            if (pairTr != null && pairSt != null) {
                DistrTrain train = pairTr.getValue();
                Station station = pairSt.getValue();
                TrainHideSpan value = new TrainHideSpan();

                value.setIdTrain(train.getIdTrain());
                value.setIdStB(station.getIdSt());
                value.setIdDistr(idDistr);

                value.setShape(XMLUtil.getAttribute(item, "shape"));

                Map<Integer, TrainHideSpan> map  = train.getMapTrainHideSpan();
                map.put(station.getIdSt(), value);
            }
        }
    }

    /**
     * LineStyleSpan �� xml � DistrTrain
     * @param idDistr id ������� (�� �� ���� ������ � ���������)
     * @param domDoc xml
     * @param allSt ��������� �������
     * @param allTrain ������
     */
    private static void loadLineStyleSpan(int idDistr, Document domDoc,
                                          Map<Integer, Pair<Element, Station>> allSt,
                                          Map<Long, Pair<Element, DistrTrain>> allTrain)
    {
        // LineStyleSpan: IDtrain, IDstB, IDdistr, shape, LineStyle
        NodeList nodeList = domDoc.getElementsByTagName("LineStyleSpan");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idStXML = XMLUtil.getAttributeInt(item, "IDstB");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Station> pairSt =  allSt.get(idStXML);
            if (pairTr != null && pairSt != null) {
                DistrTrain train = pairTr.getValue();
                Station station = pairSt.getValue();
                TrainLineStyleSpan value = new TrainLineStyleSpan();

                value.setIdTrain(train.getIdTrain());
                value.setIdStB(station.getIdSt());
                value.setIdDistr(idDistr);

                value.setShape(XMLUtil.getAttribute(item, "shape"));
                value.setLineStyle(XMLUtil.getAttributeInt(item, "LineStyle"));

                Map<Integer, TrainLineStyleSpan> map  = train.getMapTrainLineStyleSpan();
                map.put(station.getIdSt(), value);
            }
        }
    }

    /**
     * Train_SpanColor �� xml � DistrTrain
     * @param idDistr id ������� (�� �� ���� ������ � ���������)
     * @param domDoc xml
     * @param allSt ��������� �������
     * @param allTrain ������
     */
    private static void loadSpanColor(int idDistr, Document domDoc,
                                      Map<Integer, Pair<Element, Station>> allSt,
                                      Map<Long, Pair<Element, DistrTrain>> allTrain)
    {
        // Train_SpanColor: IDtrain, IDstB, IDdistr, colorTR
        NodeList nodeList = domDoc.getElementsByTagName("Train_SpanColor");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idStXML = XMLUtil.getAttributeInt(item, "IDstB");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Station> pairSt =  allSt.get(idStXML);
            if (pairTr != null && pairSt != null) {
                DistrTrain train = pairTr.getValue();
                Station station = pairSt.getValue();
                TrainSpanColor value = new TrainSpanColor();

                value.setIdTrain(train.getIdTrain());
                value.setIdStB(station.getIdSt());
                value.setIdDistr(idDistr);

                value.setColorTR(XMLUtil.getAttributeInt(item, "colorTR"));

                Map<Integer, TrainSpanColor> map  = train.getMapTrainSpanColor();
                map.put(station.getIdSt(), value);
            }
        }
    }

    /**
     * Train_SpanInscr �� xml � DistrTrain
     * @param idDistr id ������� (�� �� ���� ������ � ���������)
     * @param domDoc xml
     * @param allSt ��������� �������
     * @param allTrain ������
     */
    private static void loadSpanInscr(int idDistr, Document domDoc,
                                      Map<Integer, Pair<Element, Station>> allSt,
                                      Map<Long, Pair<Element, DistrTrain>> allTrain)
    {
        // Train_SpanInscr: IDtrain, IDstB, IDdistr, AInscr
        NodeList nodeList = domDoc.getElementsByTagName("Train_SpanInscr");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idStXML = XMLUtil.getAttributeInt(item, "IDstB");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Station> pairSt =  allSt.get(idStXML);
            if (pairTr != null && pairSt != null) {
                DistrTrain train = pairTr.getValue();
                Station station = pairSt.getValue();
                TrainSpanInscr value = new TrainSpanInscr();

                value.setIdTrain(train.getIdTrain());
                value.setIdStB(station.getIdSt());
                value.setIdDistr(idDistr);

                value.setAInscr(XMLUtil.getAttribute(item, "AInscr"));

                Map<Integer, TrainSpanInscr> map  = train.getMapTrainSpanInscr();
                map.put(station.getIdSt(), value);
            }
        }
    }

    /**
     * ShowParam �� xml � DistrTrain
     * ��������� ����������� '�����' ������� �� �������
     * @param domDoc xml
     * @param allTrain ������
     * @param allDistrComb �������
     */
    private static void loadShowParam(Document domDoc,
                                      Map<Long, Pair<Element, DistrTrain>> allTrain,
                                      Map<Integer, Pair<Element, Distr>> allDistrComb)
    {
        // ShowParam: IDtrain, IDdistr, hidden, ShowCodeI, colorTR, AInscr, LineStyle, widthTR
        // Name= "05" code= "3312"
        NodeList nodeList = domDoc.getElementsByTagName("ShowParam");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            long idTrXML = XMLUtil.getAttributeLong(item, "IDtrain");
            int idDistrXML = XMLUtil.getAttributeInt(item, "IDdistr");
            Pair<Element, DistrTrain> pairTr =  allTrain.get(idTrXML);
            Pair<Element, Distr> pairD =  allDistrComb.get(idDistrXML);
            if (pairTr != null && pairD != null) {
                DistrTrain train = pairTr.getValue();
                Distr distr = pairD.getValue();
                train.setIdDistr(distr.getIdDistr()); // ��� '�����' � ������� � ���� ������ IdDistr, ��� ��� ���� ���������

                DistrTrainShowParam param  = train.getShowParam();

                param.setHidden(XMLUtil.getAttributeBoolean(item, "hidden"));
                param.setShowCodeI(XMLUtil.getAttributeBoolean(item, "ShowCodeI"));
                param.setColorTR(XMLUtil.getAttributeInt(item, "colorTR"));
                param.setAinscr(XMLUtil.getAttribute(item, "AInscr"));
                param.setLineStyle(XMLUtil.getAttributeInt(item, "LineStyle"));
                param.setWidthTR(XMLUtil.getAttributeInt(item, "widthTR"));
            }
            if (pairTr != null && pairD == null) {
                allTrain.remove(idTrXML);
            }
        }
    }

    /**
     * ������� ���� ����� �.�.
     * @param domDoc xml
     * @param allSp ������������
     * @param allLoc ������������
     * @return SpanStTime ������
     */
    private static List<SpanStTime> loadSpanStTime(Document domDoc,
                                                   Map<Integer, Pair<Element, Scale>> allSp,
                                                   Map<Integer, Pair<Element, LocomotiveType>> allLoc) {
        List<SpanStTime> spanStTimeList = new ArrayList<>();
        // key = IDscale
        Map<Integer, Pair<Element, Scale>> allScale = buildScaleMap(allSp);

        // Span_StTime: IDltype, IDscale, Tmove_e, Tmove_o, num
        NodeList nodeList = domDoc.getElementsByTagName("Span_StTime");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            int idScaleXML = XMLUtil.getAttributeInt(item, "IDscale");
            int idLocXML = XMLUtil.getAttributeInt(item, "IDltype");
            Pair<Element, Scale> pairS =  allScale.get(idScaleXML);
            Pair<Element, LocomotiveType> pairL =  allLoc.get(idLocXML);
            if (pairS != null && pairL != null) {
                SpanStTime time = new SpanStTime();
                time.setIdLType(pairL.getValue().getIdLType());
                time.setIdScale(pairS.getValue().getIdScale());
                time.setNum(XMLUtil.getAttributeInt(item, "num"));
                time.setTmoveO(XMLUtil.getAttributeDouble(item, "Tmove_o"));
                time.setTmoveE(XMLUtil.getAttributeDouble(item, "Tmove_e"));
                spanStTimeList.add(time);
            }
        }
        return spanStTimeList;
    }

    /**
     * ��������������  allSp ��� key = IDspan(XML) � allScale ��� key = IDscale(XML)
     * @param allSp key=IDspan(XML) value = pair(Element,Scale)
     * @return allScale: key=IDscale(XML) value = pair(Element,Scale)
     */
    private static Map<Integer, Pair<Element, Scale>> buildScaleMap(Map<Integer, Pair<Element, Scale>> allSp) {
        Map<Integer, Pair<Element, Scale>> map = new HashMap<>();
        for (Pair<Element, Scale> pair : allSp.values()) {
            Element item = pair.getKey();
            int idScaelXML = XMLUtil.getAttributeInt(item, "IDscale");
            map.put(idScaelXML, pair);
        }
        return map;
    }

    /**
     * �������� ������� ��� ���������� ���
     * @param idDistr �� �������
     * @param domDoc xml
     * @param allSp ����������� ��������
     * @return ������ RouteD
     */
    private static List<RouteD> loadRoute(int idDistr, Document domDoc,
                                          Map<Integer, Pair<Element, Scale>> allSp)
    {
        List<RouteD> list = new ArrayList<>();
        Map<Integer, RouteD> routeDMap = new HashMap<>();
        // RouteD: IDdistr, IDrouteD, NameR, TypeR
        NodeList nodeList = domDoc.getElementsByTagName("RouteD");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            RouteD routeD = new RouteD();
            routeD.setIdDistr(idDistr);
            list.add(routeD);
            int idXML = XMLUtil.getAttributeInt(item, "IDrouteD");
            routeD.setTypeR(XMLUtil.getAttributeInt(item, "TypeR"));
            routeD.setNameR(XMLUtil.getAttribute(item,"NameR"));
            routeDMap.put(idXML, routeD);
        }
        fillRouteDSpXML(domDoc, routeDMap, allSp);
        fillRouteDTrXML(domDoc, routeDMap);
        return list;
    }

    private static void fillRouteDSpXML(Document domDoc, Map<Integer, RouteD> routeDMap, Map<Integer, Pair<Element, Scale>> allSp) {
        // RouteDSp: IDrouteD, IDspan, num
        NodeList nodeList = domDoc.getElementsByTagName("RouteDSp");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            int idXML = XMLUtil.getAttributeInt(item, "IDrouteD");
            int idSpXML = XMLUtil.getAttributeInt(item, "IDspan");
            Pair<Element, Scale> pairS = allSp.get(idSpXML);
            RouteD  routeD = routeDMap.get(idXML);
            if (routeD != null && pairS!=null) {
                RouteDSpan dSpan = new RouteDSpan();
                dSpan.setIdRouteD(routeD.getIdRouteD());
                dSpan.setIdSpan(pairS.getValue().getIdSpan());
                dSpan.setNum(XMLUtil.getAttributeInt(item, "num"));
                routeD.getListSpan().add(dSpan);
            }
        }
    }

    private static void fillRouteDTrXML(Document domDoc, Map<Integer, RouteD> routeDMap) {
        // RouteDTR: IDrouteD, trMax, trMin
        NodeList nodeList = domDoc.getElementsByTagName("RouteDTR");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element item = (Element) nodeList.item(i);
            int idXML = XMLUtil.getAttributeInt(item, "IDrouteD");
            RouteD routeD = routeDMap.get(idXML);
            if (routeD != null) {
                RouteDTrain dTrain = new RouteDTrain();
                dTrain.setIdRouteD(routeD.getIdRouteD());
                dTrain.setTrMin(XMLUtil.getAttributeInt(item, "trMin"));
                dTrain.setTrMax(XMLUtil.getAttributeInt(item, "trMax"));
                routeD.getListTR().add(dTrain);
            }
        }
    }

}
