package com.gmail.alaerof.javafx.dialog.loadfromxml.view;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.common.LoadGDPXMLConfig;
import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrGDPXMLDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.TaskWithCursorExecutor;
import com.gmail.alaerof.xml.GDPXMLHandler;
import com.gmail.alaerof.xml.loadgdp.DistrStruct;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.controlsfx.control.StatusBar;
import org.w3c.dom.Document;

public class LoadFromXMLLayoutController implements Initializable {
    /** logger */
    protected Logger logger;
    {
        logger = LogManager.getLogger(this.getClass());
    }
    /***/
    private String sourceGDP;
    /** Internationalization */
    protected ResourceBundle bundle;
    /***/
    private DistrEditEntity distrEditEntity;
    /***/
    private Document doc;
    /***/
    private Scene scene;
    /***/
    protected JDialog dialogFrame;
    /***/
    StatusBar statusBar;
    @FXML
    private RadioButton radioButtonExpress;
    @FXML
    private RadioButton radioButtonESR;
    @FXML
    private Button apply;
    @FXML
    private VBox bottomVBox;
    @FXML
    private Label desctiption1;
    @FXML
    private TextArea warningTextArea;
    @FXML
    private CheckBox loadSpanTrain;
    @FXML
    private CheckBox loadSpanTime;
    @FXML
    private CheckBox loadSpanStTime;
    @FXML
    private CheckBox loadDistrAppearance;
    @FXML
    private CheckBox loadRoutes;
    @FXML
    private CheckBox loadWind;

    /***/
    DistrStruct distrStruct;

    /**
     * ������������� ���������� ��� �������� �� FXML ����������� (FXMLLoader)
     * @param location {@link URL}
     * @param resources {@ResourceBundle} �������������� ��������
     */
    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        ToggleGroup group = new ToggleGroup();
        radioButtonESR.setToggleGroup(group);
        radioButtonExpress.setToggleGroup(group);
        radioButtonExpress.setSelected(true);

        statusBar = new StatusBar();
        bottomVBox.getChildren().add(statusBar);

        desctiption1.setWrapText(true);
        desctiption1.setTextAlignment(TextAlignment.JUSTIFY);

        warningTextArea.setWrapText(true);
    }

    @FXML
    private void handleCheckAllDistrStruct(ActionEvent event){
        logger.debug("handleCheckAllDistrStruct");
        try {
            statusBar.setText(bundle.getString("javafx.dialog.loadfromxml.statusbar"));
            warningTextArea.clear();
            Task task = new Task<Void>() {
                @Override
                protected Void call() {
                    distrStruct = GDPXMLHandler.checkAllDistrStruct(doc, distrEditEntity, radioButtonExpress.isSelected());
                    return null;
                }
            };
            TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, ()->{
                showWarnings(distrStruct);
                warningTextArea.setWrapText(true);
                apply.setDisable(false);
            });
        } catch (Exception e) {
            logger.error(e.toString(), e);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("");
            alert.setHeaderText(e.toString());
            alert.showAndWait();
        }
    }

    private void showWarnings(DistrStruct distrStruct) {
        StringBuilder sb = new StringBuilder();
        if (distrStruct != null) {
            for (String warn : distrStruct.getWarningList()) {
                sb.append(warn).append("\n");
            }
        }
        if(sb.length()==0){
            sb.append("100% ok !!!");
        }
        warningTextArea.setText(sb.toString());
    }

    @FXML
    private void handleApply(ActionEvent event){
        logger.debug("handleApply");
        Task task = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    // �������� ��� ������� �� ��
                    clearGDP();
                    // �������� ��� �� ����� xml � ��
                    loadGDPFromXML();
                } catch (DataManageException | PoolManagerException e) {
                    logger.error(e.toString(), e);
                    throw new RuntimeException(e);
                }
                return null;
            }
        };

        TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, ()->{
            closeFrame(dialogFrame);
        });
    }

    private void closeFrame(JDialog frameToClose) {
        SwingUtilities.invokeLater(()-> frameToClose.setVisible(false));
    }

    /**
     * �������� ��� �� ����� xml � ��
     */
    private void loadGDPFromXML() throws DataManageException, PoolManagerException {
        logger.debug("loadGDPFromXML: " + distrEditEntity.getDistrEntity().getDistrName());
        logger.debug("from file:  " + sourceGDP);
        long tm1 = Calendar.getInstance().getTimeInMillis();
        try {
            ICategoryDAO categoryDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO();
            List<Category> categoryList = categoryDAO.getCategories();
            IDistrGDPXMLDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrGDPXMLDAO();
            // ���� ������
            dao.checkAllTrain(doc,"Distr_Train", distrStruct.getAllTrain(), categoryList, distrStruct.getWarningList());
            // ������ � ���. ��������
            // Distr_TrainF ����� �� ������������ �.�. ����� ������ ��������� ����������� ShowParam
            dao.checkAllTrain(doc,"Distr_TrainF", distrStruct.getAllTrain(), categoryList, distrStruct.getWarningList());

            LoadGDPXMLConfig loadGDPXMLConfig = createLoadGDPXMLConfig();
            GDPXMLHandler.loadGDPFromXML(doc, distrEditEntity, distrStruct, loadGDPXMLConfig);
            distrEditEntity.clearTrainThreadMap();
            //distrEditEntity.getDistrEntity().reloadFromDB();
        } finally {
            long tm2 = Calendar.getInstance().getTimeInMillis();
            double sec = (double) (tm2 - tm1) / 1000.0;
            logger.debug("loadGDPFromXML done: " + sec + " sec");
        }
    }

    private LoadGDPXMLConfig createLoadGDPXMLConfig() {
        LoadGDPXMLConfig config = new LoadGDPXMLConfig();
        config.loadRoutes = loadRoutes.isSelected();
        config.loadSpanStTime = loadSpanStTime.isSelected();
        config.loadSpanTime = loadSpanTime.isSelected();
        config.loadDistrAppearance = loadDistrAppearance.isSelected();
        config.loadSpanTrain = loadSpanTrain.isSelected();
        config.loadWind = loadWind.isSelected();
        config.sourceGDP = this.sourceGDP;
        return config;
    }

    /**
     * �������� ��� ������� �� ��
     */
    private void clearGDP() {
        logger.debug("clearGDP");
        long tm1 = Calendar.getInstance().getTimeInMillis();
        try {
            distrEditEntity.clearDistrGDPFromDB(false);
        } finally {
            long tm2 = Calendar.getInstance().getTimeInMillis();
            double sec = (double) (tm2 - tm1) / 1000.0;
            logger.debug("clear GDP done: " + sec + " sec");
        }
    }

    @FXML
    private void handleCancel(ActionEvent event){
        logger.debug("handleCancel");
        closeFrame();
    }

    private void closeFrame() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                dialogFrame.setVisible(false);
            }
        });
    }

    public void setContent(Document doc, DistrEditEntity distrEditEntity, String sourceGDP){
         this.sourceGDP = sourceGDP;
         this.doc = doc;
         this.distrEditEntity = distrEditEntity;
         apply.setDisable(true);
         warningTextArea.clear();
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public void setDialogFrame(JDialog dialogFrame) {
        this.dialogFrame = dialogFrame;
    }
}
