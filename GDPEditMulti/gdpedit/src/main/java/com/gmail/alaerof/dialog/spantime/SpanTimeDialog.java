package com.gmail.alaerof.dialog.spantime;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.IconImageUtil;

import java.awt.FlowLayout;
import java.util.ResourceBundle;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class SpanTimeDialog extends JDialog {

    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private DistrSpan distrSpan;
    private LocomotiveTypeTableModel locTableModel;
    private SpanTimePanel spanTimePanel;
    private SpanStPanel spanStPanel;
    
    private JLabel stB;
    private JLabel stE;
    private JLabel kmB;
    private JLabel kmE;
    private JLabel len;

    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spantime", LocaleManager.getLocale());
    }

    /**
     * Create the dialog.
     */
    public SpanTimeDialog() {
        setIconImage(IconImageUtil.getImage(IconImageUtil.MAIN));
        initContent();
    }
    
    private void initContent(){
        setBounds(100, 100, 650, 600);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));

        locTableModel = new LocomotiveTypeTableModel();
        spanTimePanel = new SpanTimePanel();
        spanStPanel = new SpanStPanel(locTableModel, this);
/*
        {
            JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
            contentPanel.add(tabbedPane, BorderLayout.CENTER);
            {
                //JPanel panel = new JPanel();
                tabbedPane.addTab("������� ����", null, spanTimePanel, null);
            }
            {
                //JPanel panel = new JPanel();
                tabbedPane.addTab("������������ ������", null, spanStPanel, null);
            }
        }
*/
        {
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            contentPanel.add(panel, BorderLayout.CENTER);
            panel.add(spanTimePanel, BorderLayout.NORTH);
            panel.add(spanStPanel, BorderLayout.CENTER);
            
        }
        {
            JPanel panelSp = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panelSp.getLayout();
            flowLayout.setAlignment(FlowLayout.LEADING);
            contentPanel.add(panelSp, BorderLayout.NORTH);
            
            stB = new JLabel(bundle.getString("spantime.newlabel"));
            panelSp.add(stB);
            
            kmB = new JLabel(bundle.getString("spantime.newlabel"));
            panelSp.add(kmB);
            
            stE = new JLabel(bundle.getString("spantime.newlabel"));
            panelSp.add(stE);
            
            kmE = new JLabel(bundle.getString("spantime.newlabel"));
            panelSp.add(kmE);
            
            JLabel lblNewLabel = new JLabel(bundle.getString("spantime.lengthofstage"));
            panelSp.add(lblNewLabel);
            
            len = new JLabel(bundle.getString("spantime.newlabel"));
            panelSp.add(len);
            
        }

    }

    public DistrSpan getDistrSpan() {
        return distrSpan;
    }

    public void setDistrSpan(DistrSpan distrSpan) {
        this.setTitle(distrSpan.getSpanScale().getName());
        this.distrSpan = distrSpan;
        spanTimePanel.setDistrSpan(distrSpan);
        spanStPanel.setDistrSpan(distrSpan);
        
        stB.setText(distrSpan.getStB().getNamePoint());
        stE.setText(distrSpan.getStE().getNamePoint());
        kmB.setText(((Float)distrSpan.getSpanScale().getAbsKM()).toString());
        kmE.setText(((Float)distrSpan.getSpanScale().getAbsKM_E()).toString());
        len.setText(((Integer)distrSpan.getSpanScale().getL()).toString());
        
    }
    
    public void setListLoc(List<LocomotiveType> list){
        locTableModel.setList(list);
    }
    
    public List<LocomotiveType> getListLoc(){
        return locTableModel.getList();
    }

}
