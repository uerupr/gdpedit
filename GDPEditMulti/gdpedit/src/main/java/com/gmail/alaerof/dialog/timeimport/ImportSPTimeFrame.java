package com.gmail.alaerof.dialog.timeimport;

import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.IconImageUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.VariantCalc;
import com.gmail.alaerof.dao.dto.VariantCalcTime;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IVariantCalcDAO;
import com.gmail.alaerof.dialog.timeimport.stations.StationDependencyDialog;
import com.gmail.alaerof.util.TableUtils;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
public class ImportSPTimeFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(ImportSPTimeFrame.class);;
    private final JPanel contentPanel = new JPanel();
    private int distrID;
    private VarCalcTableModel vcTableModel = new VarCalcTableModel();
    private JTable vcTable = new JTable(vcTableModel);
    private VariantCalc selectedVC;
    private VarCalcTimeTableModel vctTableModel = new VarCalcTimeTableModel();
    private JTable vctTable = new JTable(vctTableModel);
    private JComboBox<LocomotiveType> locComboBox;
    protected StationDependencyDialog stationDependencyDialog;
    private static ResourceBundle bundle;

    /**
     * Create the dialog.
     */
    public ImportSPTimeFrame(String title) {
        super(title);
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.timeimport", LocaleManager.getLocale());
    }

    /**
     * Create the dialog.
     */
    public ImportSPTimeFrame() {
        initContent();
    }

    /**
     * инициализация контента
     */
    private void initContent() {
        try {
            this.setIconImage(IconImageUtil.getImage(IconImageUtil.ISPT64));
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        stationDependencyDialog = new StationDependencyDialog(this,
                bundle.getString("timeimport.matchingstations"), true);

        setBounds(0, 0, 850, 600);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);

        JPanel varCalcPanel = new JPanel();
        varCalcPanel.setLayout(new BorderLayout());
        varCalcPanel.setPreferredSize(new Dimension(450, 0));
        contentPanel.add(varCalcPanel, BorderLayout.WEST);

        vcTable.setFillsViewportHeight(true);
        vcTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSizeO[] = { 40, 60, 120, 250 };
        TableUtils.setTableColumnSize(vcTable, colSizeO);
        vcTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(vcTable);
        varCalcPanel.add(scroll, BorderLayout.CENTER);

        vcTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent ev) {
                int row = vcTable.getSelectedRow();
                if (row > -1) {
                    VariantCalc vc = vcTableModel.getList().get(row);
                    selectVC(vc);
                }
            }
        });

        vcTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = vcTable.getSelectedRow();
                if (row > -1) {
                    VariantCalc vc = vcTableModel.getList().get(row);
                    if (e.getClickCount() == 1) {
                        selectVC(vc);
                    }
                }
            }
        });
        {
            JPanel buttonPanel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) buttonPanel.getLayout();
            flowLayout.setAlignment(FlowLayout.RIGHT);
            varCalcPanel.add(buttonPanel, BorderLayout.SOUTH);
            JButton del = new JButton(bundle.getString("timeimport.delete"));
            del.setToolTipText(bundle.getString("timeimport.deletethisoptionfromgraphist"));
            buttonPanel.add(del);
            del.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ev) {
                    try {
                        int row = vcTable.getSelectedRow();
                        if (row > -1) {
                            VariantCalc vc = vcTableModel.getList().get(row);
                            int n = JOptionPane.showConfirmDialog(ImportSPTimeFrame.this,
                                    bundle.getString("timeimport.doyoureallywanttodelete")
                                            + vc.getName(),bundle.getString("timeimport.warning"), JOptionPane.YES_NO_OPTION);
                            if (n == 0) {
                                vcTableModel.getList().remove(vc);
                                GDPDAOFactoryCreater.getGDPDAOFactory().getVariantCalcDAO().deleteVariantCalc(vc.getIDvarCalc());
                                vcTable.repaint();
                                clearVCT();
                            }
                        }
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }
                }
            });
        }

        JPanel varCalcTimePanel = new JPanel();
        varCalcTimePanel.setLayout(new BorderLayout());
        contentPanel.add(varCalcTimePanel, BorderLayout.CENTER);

        vctTable.setFillsViewportHeight(true);
        vctTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSize[] = { 25, 25, 120, 60, 40, 40, 40, 40, 40 };
        TableUtils.setTableColumnSize(vctTable, colSize);
        vctTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scroll = new JScrollPane(vctTable);
        varCalcTimePanel.add(scroll, BorderLayout.CENTER);

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);

            {
                JButton okButton = new JButton(bundle.getString("timeimport.ok"));
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            if (selectedVC != null) {
                                LocomotiveType loc = (LocomotiveType) locComboBox.getSelectedItem();
                                stationDependencyDialog.searchStationsBeforeOpen(vctTableModel.getList(), distrID, loc, selectedVC.getOe(), selectedVC);
                                stationDependencyDialog.setVisible(true);
                            }
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
            {
                JButton cancelButton = new JButton(bundle.getString("timeimport.cancel"));
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // ImportSPTimeFrame.this.setVisible(false);
                        ImportSPTimeFrame.this.dispose();
                    }
                });
            }
        }

        JPanel locPanel = new JPanel();
        varCalcTimePanel.add(locPanel, BorderLayout.NORTH);
        locPanel.add(new JLabel(bundle.getString("timeimport.selectingtypeofmovement")));
        locComboBox = new JComboBox<LocomotiveType>();
        locPanel.add(locComboBox);
        try {
            List<LocomotiveType> listLT = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO().getListLocomotiveType();
            for (LocomotiveType lt : listLT) {
                locComboBox.addItem(lt);
            }
        } catch ( DataManageException ex) {
            logger.error(ex.toString(), ex);
        }

    }

    protected void clearVCT() {
        if (vctTableModel.getList() != null) {
            vctTableModel.getList().clear();
            vctTable.repaint();
        }
    }

    protected void selectVC(VariantCalc vc) {
        if (vc != selectedVC) {
            selectedVC = vc;
            List<VariantCalcTime> list;
            IVariantCalcDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getVariantCalcDAO();
            try {
                list = dao.getListVarCalcTime(vc.getIDvarCalc());
            } catch (DataManageException e) {
                list = new ArrayList<>();
                logger.error(e.toString(), e);
            }
            Collections.sort(list);
            vctTableModel.setList(list);
            vctTable.repaint();
        }
    }

    public int getDistrID() {
        return distrID;
    }

    public void setDistrID(int distrID) {
        this.distrID = distrID;
        fillTable();
    }

    private void fillTable() {
        try {
            IVariantCalcDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getVariantCalcDAO();
            logger.debug("dao");
            List<VariantCalc> list = dao.getListVarCalc(distrID);
            logger.debug("distrID = " + distrID + " list = " + list);
            vcTableModel.setList(list);
            vcTable.repaint();
            clearVCT();
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

}
