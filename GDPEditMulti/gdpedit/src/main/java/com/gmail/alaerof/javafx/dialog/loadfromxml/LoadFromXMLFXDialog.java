package com.gmail.alaerof.javafx.dialog.loadfromxml;

import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.loadfromxml.view.LoadFromXMLLayoutController;
import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import org.w3c.dom.Document;

public class LoadFromXMLFXDialog extends SwingFXDialogBase {
    /***/
    private LoadFromXMLLayoutController controller;

    public LoadFromXMLFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/loadfromxml/view/LoadFromXMLLayout.fxml";
        resourceBundleBaseName = "bundles.LoadFromXMLFXDialog";
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
        controller.setDialogFrame(this);
    }

    public void setDialogContent(Document doc, DistrEditEntity distrEditEntity, String sourceGDP) {
        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(doc, distrEditEntity, sourceGDP);
            }
        });
    }
}
