package com.gmail.alaerof.javafx.dialog.stationinfo.model;

import com.gmail.alaerof.dao.dto.IntGR;
import java.awt.Image;
import java.util.ResourceBundle;

import com.gmail.alaerof.manager.LocaleManager;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LineStIntModel {
    // idIntGR
    private int idIntGR;
    // name
    private StringProperty name;
    // name of firstType
    private StringProperty firstType;
    // name of secondType
    private StringProperty secondType;
    // firstStop
    private StringProperty firstStop;
    private boolean firstStopB;
    // secondStop
    private StringProperty secondStop;
    private boolean secondStopB;
    // defaultValue
    private IntegerProperty value;
    // image
    private Image image;
    //bundle
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.StationInfoFXDialog", LocaleManager.getLocale());
    }

    public LineStIntModel(IntGR intGR) {
        this.idIntGR = intGR.getIdIntGR();
        this.name = new SimpleStringProperty(intGR.getName());
        this.firstType = new SimpleStringProperty(intGR.getFirstType().getString());
        this.secondType = new SimpleStringProperty(intGR.getSecondType().getString());
        String word;
        if (intGR.isFirstStop()) {
            word = bundle.getString("stationinfo.yes");
        } else {
            word = bundle.getString("stationinfo.no");
        }
        this.firstStop = new SimpleStringProperty(word);
        this.firstStopB = intGR.isFirstStop();
        if (intGR.isSecondStop()) {
            word = bundle.getString("stationinfo.yes");
        } else {
            word = bundle.getString("stationinfo.no");
        }
        this.secondStop = new SimpleStringProperty(word);
        this.secondStopB = intGR.isSecondStop();
        this.value = new SimpleIntegerProperty(intGR.getDefaultValue());
        this.image = intGR.getImage();
    }

    public int getIdIntGR() {
        return idIntGR;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getFirstType() {
        return firstType.get();
    }

    public StringProperty firstTypeProperty() {
        return firstType;
    }

    public String getSecondType() {
        return secondType.get();
    }

    public StringProperty secondTypeProperty() {
        return secondType;
    }

    public String isFirstStop() {
        return firstStop.get();
    }

    public StringProperty firstStopProperty() {
        return firstStop;
    }

    public boolean isFirstStopB() { return firstStopB; }

    public String isSecondStop() {
        return secondStop.get();
    }

    public StringProperty secondStopProperty() {
        return secondStop;
    }

    public boolean isSecondStopB() { return secondStopB; }

    public int getValue() {
        return value.get();
    }

    public IntegerProperty valueProperty() {
        return value;
    }

    public Image getImage() {
        return image;
    }

    public void setIdIntGR(int idIntGR) {
        this.idIntGR = idIntGR;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setFirstType(String firstTypeName) {
        this.firstType.set(firstTypeName);
    }

    public void setSecondType(String secondTypeName) {
        this.secondType.set(secondTypeName);
    }

    public void setFirstStop(String firstStop) {
        this.firstStop.set(firstStop);
    }

    public void setSecondStop(String secondStop) {
        this.secondStop.set(secondStop);
    }

    public void setValue(int value) {
        this.value.set(value);
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
