package com.gmail.alaerof.javafx.dialog.trainpropertys;

import com.gmail.alaerof.javafx.dialog.trainpropertys.model.TrainModel;
import java.util.Comparator;

public class TrainModelComparator implements Comparator<TrainModel> {
    @Override
    public int compare(TrainModel trainModel1, TrainModel trainModel2) {
        String sCode1 = trainModel1.codeTrainProperty().get();
        int iCode1 = Integer.parseInt(sCode1);
        String name1 = trainModel1.nameTrainProperty().get();
        String sCode2 = trainModel2.codeTrainProperty().get();
        int iCode2 = Integer.parseInt(sCode2);
        String name2 = trainModel2.nameTrainProperty().get();
        if (!sCode1.equals(sCode2)){
            int ans = 1;
            if (iCode1 < iCode2){
                ans = -1;
            }
            return ans;
        } else {
            return name1.compareTo(name2);
        }
    }
}
