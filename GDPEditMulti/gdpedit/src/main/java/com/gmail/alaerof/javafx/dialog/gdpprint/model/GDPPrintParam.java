package com.gmail.alaerof.javafx.dialog.gdpprint.model;

import com.gmail.alaerof.dao.dto.TextPrintParam;
import java.util.ArrayList;
import java.util.List;
import javafx.print.Paper;

/**
 * Created by Paul M. Bui on 25.05.2018.
 */
public class GDPPrintParam {
    // �������� ������
    private int realHeight;
    // �������� ������
    private int realWidth;
    // ����������������� ������
    private int scalledHeight;
    // ����������������� ������
    private int scalledWidth;
    // ��� ������ �����
    private int hourBeg;
    // ��� ����� �����
    private int hourEnd;
    // ����� ������
    private boolean leftPanel;
    // ������ ������
    private boolean rightPanel;
    // ���������
    private boolean topPanel;
    // �������
    private boolean buttomPanel;
    // ������ ���������������: 1 - �� �������� ��������; 2 - �������������; 3 - �����������
    private int scallingType;
    // ������: �0; �1; �2; �3; �4; ���� == null
    private Paper format;
    // ��������� ���������
    private boolean saveProportionl;
    // �������
    private int scale;
    // ������
    private int height;
    // ������
    private int width;
    // ���������
    private List<TextPrintParam> headTextFields = new ArrayList<>();
    // ���������
    private List<TextPrintParam> bossTextFields = new ArrayList<>();
    // ������� (����������)
    private List<TextPrintParam> workerTextFields = new ArrayList<>();

    public int getRealHeight() {
        return realHeight;
    }

    public int getRealWidth() {
        return realWidth;
    }

    public int getScalledHeight() {
        return scalledHeight;
    }

    public int getScalledWidth() {
        return scalledWidth;
    }

    public int getHourBeg() {
        return hourBeg;
    }

    public int getHourEnd() {
        return hourEnd;
    }

    public boolean getLeftPanel() {
        return leftPanel;
    }

    public boolean getRightPanel() {
        return rightPanel;
    }

    public boolean getTopPanel() {
        return topPanel;
    }

    public boolean getButtomPanel() {
        return buttomPanel;
    }

    public int getScallingType() {
        return scallingType;
    }

    public Paper getFormat() {
        return format;
    }

    public boolean getSaveProportionl() {
        return saveProportionl;
    }

    public int getScale() {
        return scale;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public List<TextPrintParam> getHeadTextFields() {
        return headTextFields;
    }

    public List<TextPrintParam> getBossTextFields() {
        return bossTextFields;
    }

    public List<TextPrintParam> getWorkerTextFields() {
        return workerTextFields;
    }

    public void setRealHeight(int realHeight) {
        this.realHeight = realHeight;
    }

    public void setRealWidth(int realWidth) {
        this.realWidth = realWidth;
    }

    public void setScalledHeight(int scalledHeight) {
        this.scalledHeight = scalledHeight;
    }

    public void setScalledWidth(int scalledWidth) {
        this.scalledWidth = scalledWidth;
    }

    public void setHourBeg(int hourBeg) {
        this.hourBeg = hourBeg;
    }

    public void setHourEnd(int hourEnd) {
        this.hourEnd = hourEnd;
    }

    public void setLeftPanel(boolean leftPanel) {
        this.leftPanel = leftPanel;
    }

    public void setRightPanel(boolean rightPanel) {
        this.rightPanel = rightPanel;
    }

    public void setTopPanel(boolean topPanel) {
        this.topPanel = topPanel;
    }

    public void setButtomPanel(boolean buttomPanel) {
        this.buttomPanel = buttomPanel;
    }

    public void setScallingType(int scallingType) {
        this.scallingType = scallingType;
    }

    public void setFormat(Paper format) {
        this.format = format;
    }

    public void setSaveProportionl(boolean saveProportionl) {
        this.saveProportionl = saveProportionl;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void addHeadTextFields(TextPrintParam headTextFields) {
        this.headTextFields.add(headTextFields);
    }

    public void addBossTextFields(TextPrintParam bossTextFields) {
        this.bossTextFields.add(bossTextFields);
    }

    public void addWorkerTextFields(TextPrintParam wockerTextFields) {
        this.workerTextFields.add(wockerTextFields);
    }

    public void clearHeadTextFields() {
        this.headTextFields.clear();
    }

    public void clearBossTextFields() {
        this.bossTextFields.clear();
    }

    public void clearWorkerTextFields() {
        this.workerTextFields.clear();
    }

    public double[] getPageSize(boolean portrait){
        double wFormat = 0;
        double hFormat = 0;

        if (portrait) {
            wFormat = getFormat().getWidth();
            hFormat = getFormat().getHeight();
        }   else {
            wFormat = getFormat().getHeight();
            hFormat = getFormat().getWidth();
        }
        double[] size = {wFormat, hFormat};
        return size;
    }
}
