package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import com.gmail.alaerof.entity.train.NewLine.DrawCaption;
import com.gmail.alaerof.entity.train.NewLine.DrawTex;

/**
 * ������� ����� �� ������� (���� �����)
 * 
 * @author Helen Yrofeeva
 * 
 */
public class TrainSt extends TrainThreadElement {
    // super.trainThread - ����� ��������
    /** ���� ����� �������� */
    private NewLine vertLineOn;
    /** ���� ����� ����������� */
    private NewLine vertLineOff;
    /** ��� ����� ������� �������� */
    private NewLine horLine1;
    /**
     * ��� ����� ������� �������� (������ ��������, ���� ���� ������� �� ����.
     * �����)
     */
    private NewLine horLine2;
    /** ������� ������ �������� */
    public TimeStation timeSt;
    /** ���������� ������� ������ */
    private TimeStation timeStBefore;
    /** ����������� ������� ������ */
    private TimeStation timeStAfter;

    private NewLineSelfParams selfParams;
    private NewLineSelfParams selfParamsVert;

    public TrainSt(JComponent owner, TrainThread trainThread, TimeStation timeSt, TimeStation timeStBefore,
            TimeStation timeStAfter) {
        super(owner, trainThread);
        this.timeSt = timeSt;
        this.timeStBefore = timeStBefore;
        this.timeStAfter = timeStAfter;
        try {
            // GeneralParams � ���� ����� ����������
            TrainThreadGeneralParams params = trainThread.getGeneralParams();
            selfParams = new NewLineSelfParams();
            selfParams.setCaptionValue(trainThread.getCode());

            horLine1 = new LineHor(params, selfParams, this);
            horLine2 = new LineHor(params, selfParams, this);

            // ����� �����
            selfParams.lineStyle = timeSt.lineStyleSpan;
            selfParams.lineWidth = trainThread.getWidthTrain();
            owner.add(horLine1, 0);
            owner.add(horLine2, 0);

            // ���� ����� ����� ���� ������� �� ����� �������
            // � �� ������� ���� ����
            // �� ������� ������������ �����, ����� ��� �� ����� ������
            selfParamsVert = new NewLineSelfParams();
            if (trainThread.isShowInStation() && this.timeSt.getLineCount() > 0) {
                selfParamsVert.setCaptionValue(trainThread.getCode());
                vertLineOn = new LineVert(params, selfParamsVert, this);
                vertLineOff = new LineVert(params, selfParamsVert, this);
                selfParamsVert.lineStyle = LineStyle.Solid;
                selfParamsVert.lineWidth = trainThread.getWidthTrain();
                owner.add(vertLineOn, 0);
                owner.add(vertLineOff, 0);
            }

            setLineBounds();

        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ������� ���������� �� �����
     */
    public void setLineBoundsExpanded() {
        // ���� ���� ��� ������� 
        boolean isTexStop = (timeSt.isTexOff() || timeSt.isTexOn());
        // ������� ����������� ������� �� �����
        horLine1.setCaption(DrawCaption.NoCaption);
        if (trainThread.isShowInStation() && timeSt.getLineCount() > 0) {
            horLine1.setCaption(DrawCaption.On);
            if (timeSt.isTexOff())
                horLine1.setCaption(DrawCaption.texOff);
            if (timeSt.isTexOn())
                horLine1.setCaption(DrawCaption.texOn);

        }

        TrainThreadGeneralParams generalParams = this.getTrainThread().getGeneralParams();

        // �������� ������������� �������� (���������)
        boolean vertOn = false;
        boolean vertOff = false;
        boolean line1 = false;
        boolean line2 = false;

        int timeStxOn = timeSt.xOn;
        int timeStxOff = timeSt.xOff;
        if (isTexStop) {
            timeStxOn = timeSt.xOnTexStop;
            timeStxOff = timeSt.xOffTexStop;
        }

        if (!timeSt.hideHor && !trainThread.isHidden() && trainThread.isShowInStation()) {
            // ���� ������� ��� �������� �� ���� �����
            if (timeStxOn < timeStxOff && timeStxOn < generalParams.xRight ) {
                line1 = true;
            }
            // ���� ������� � ���� ������� �� ����� �����
            if (timeStxOn > timeStxOff) {
                if (timeStxOn < generalParams.xRight) {
                    line1 = true;
                }
                if (timeStxOff > generalParams.xLeft) {
                    line2 = true;
                }
            }
            boolean s = trainThread.getListTimeSt().size() > 1;
            if (!timeSt.isFirst() && s && timeStxOn < generalParams.xRight) {
                vertOn = true;
            }
            if (!timeSt.isLast() && s && timeStxOff < generalParams.xRight) {
                vertOff = true;
            }
        }

        // ���� ���� ���� ���-��, �� ������� ����������
        if (vertOn || vertOff || line1 || line2) {
            // ������ ������������ ���������
            int yTop = 0;
            int yBot = 0;
            int yMiddle = 0;

            int idLineSt = timeSt.lineStTrain.getIdLineSt();
            yMiddle = timeSt.station.getLineY(idLineSt);
            yTop = timeSt.station.getY1();
            yBot = timeSt.station.getYE1();

            if (yMiddle == -1) {
                if (TimeStation.getOnDirection(timeSt, timeStBefore) == TimeStation.DirectionVert.onFromTop) {
                    yMiddle = yTop;
                } else {
                    yMiddle = yBot;
                }
                // � ������, ����� ��� ���������� �������, ������� ���������
                if (yMiddle == -1) {
                    if (TimeStation.getOffDirection(timeSt, timeStAfter) == TimeStation.DirectionVert.offToTop) {
                        yMiddle = yTop;
                    } else {
                        yMiddle = yBot;
                    }
                }
            }

            int wH = LineVert.VERT_WIDTH;
            // ������� ��������
            if (vertOn) {
                int yOn = yMiddle;
                int hOn = yBot - yMiddle;
                if (TimeStation.getOnDirection(timeSt, timeStBefore) == TimeStation.DirectionVert.onFromTop) {
                    yOn = yTop;
                    hOn = yMiddle - yTop;
                }
                if (vertLineOn != null) {
                    vertLineOn.getOriginalSize().width = wH;
                    vertLineOn.getOriginalSize().height = hOn;
                    vertLineOn.setPosition(timeStxOn, yOn);
                }
            }
            // ������� �����������
            if (vertOff) {
                int yOff = yMiddle;
                int hOff = yBot - yMiddle;
                if (TimeStation.getOffDirection(timeSt, timeStAfter) == TimeStation.DirectionVert.offToTop) {
                    yOff = yTop;
                    hOff = yMiddle - yTop;
                }
                if (vertLineOff != null) {
                    vertLineOff.getOriginalSize().width = wH;
                    vertLineOff.getOriginalSize().height = hOff;
                    vertLineOff.setPosition(timeStxOff, yOff);
                }
            }
            // ������ �������������� ���������
            if (line1 || line2) {
                int h = timeSt.station.getNameHeight() + 2;
                // ���� ��� �������� ����� �����
                if (timeStxOn < timeStxOff) {
                    int xOff = Math.min(timeStxOff, generalParams.xRight);
                    int w1 = Math.max(1, xOff - timeStxOn);
                    Dimension dim = horLine1.getOriginalSize();
                    dim.width = w1;
                    dim.height = h;
                    horLine1.setPosition(timeStxOn, yMiddle);
                    horLine1.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                } else {
                    if (line1) {
                        int w1 = generalParams.xRight - timeStxOn;
                        Dimension dim = horLine1.getOriginalSize();
                        dim.width = Math.max(1, w1);
                        dim.height = h;
                        horLine1.setPosition(timeStxOn, yMiddle);
                        horLine1.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                    }
                    if (line2) {
                        int w2 = timeStxOff - generalParams.xLeft;
                        Dimension dim = horLine2.getOriginalSize();
                        dim.width = Math.max(1, w2);
                        dim.height = h;
                        horLine2.setPosition(generalParams.xLeft, yMiddle);
                        horLine2.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                    }
                }
            }
        }

        horLine1.setDrawTex(DrawTex.NoTex);
        horLine2.setDrawTex(DrawTex.NoTex);
        if (timeSt.isTexOn()) {
            horLine1.setDrawTex(DrawTex.TexOn);
        }
        if (timeSt.isTexOff()) {
            if (line2) {
                horLine2.setDrawTex(DrawTex.TexOff);
            } else {
                horLine1.setDrawTex(DrawTex.TexOff);
            }
        }

        horLine1.setVisible(line1);
        horLine1.repaint();
        horLine2.setVisible(line2);
        horLine2.repaint();
        if (vertLineOn != null) {
            vertLineOn.setVisible(vertOn);
            vertLineOn.repaint();
        }
        if (vertLineOff != null) {
            vertLineOff.setVisible(vertOff);
            vertLineOff.repaint();
        }
    }

    /**
     * ������� �� ���������� �� �����
     */
    public void setLineBoundsNoExpanded() {
        if (vertLineOn != null) {
            vertLineOn.setVisible(false);
        }
        if (vertLineOff != null) {
            vertLineOff.setVisible(false);
        }
        horLine1.setCaption(DrawCaption.NoCaption);
        horLine2.setCaption(DrawCaption.NoCaption);

        TrainThreadGeneralParams generalParams = this.getTrainThread().getGeneralParams();

        boolean line1 = false;
        boolean line2 = false;

        if (!timeSt.hideHor && !trainThread.isHidden()) {
            // ���� ������� ��� �������� �� ���� �����
            if (timeSt.xOn < timeSt.xOff && timeSt.xOn < generalParams.xRight ) {
                line1 = true;
            }
            // ���� ������� � ���� ������� �� ����� �����
            if (timeSt.xOn > timeSt.xOff) {
                if (timeSt.xOn < generalParams.xRight) {
                    line1 = true;
                }
                if (timeSt.xOff > generalParams.xLeft) {
                    line2 = true;
                }
            }
        }

        if (line1 || line2) {
            // ������ ������������ ���������
            int yMiddle = timeSt.station.getY1();
            // ������ �������������� ���������
            // ���� ��� �������� ����� �����
            if (timeSt.xOn < timeSt.xOff) {
                int timeStxOff = Math.min(timeSt.xOff, generalParams.xRight);
                int w1 = Math.max(1, timeStxOff - timeSt.xOn);
                Dimension dim = horLine1.getOriginalSize();
                dim.width = w1;
                dim.height = LineHor.NO_CAPTION_HEIGHT;
                horLine1.setPosition(timeSt.xOn, yMiddle);
                horLine1.selfParams.lineWidth = Math.max(1, trainThread.getWidthTrain());
            } else {
                if (line1) {
                    int w1 = generalParams.xRight - timeSt.xOn;
                    Dimension dim = horLine1.getOriginalSize();
                    dim.width = Math.max(1, w1);
                    dim.height = LineHor.NO_CAPTION_HEIGHT;
                    horLine1.setPosition(timeSt.xOn, yMiddle);
                    horLine1.selfParams.lineWidth = Math.max(1, trainThread.getWidthTrain());
                }
                if (line2) {
                    int w2 = timeSt.xOff - generalParams.xLeft;
                    Dimension dim = horLine2.getOriginalSize();
                    dim.width = Math.max(1, w2);
                    dim.height = LineHor.NO_CAPTION_HEIGHT;
                    horLine2.setPosition(generalParams.xLeft, yMiddle);
                    horLine2.selfParams.lineWidth = Math.max(1, trainThread.getWidthTrain());
                }
            }
        }

        horLine1.setVisible(line1);
        horLine2.setVisible(line2);
        horLine1.repaint();
        horLine2.repaint();
    }

    @Override
    public void setLineBounds() {
        selfParams.color = timeSt.colorTR;
        selfParamsVert.color = timeSt.colorTR;
        selfParams.lineStyle = timeSt.lineStyleHor;
        if (timeSt.station.isExpanded()) {
            setLineBoundsExpanded();
        } else {
            setLineBoundsNoExpanded();
        }
    }

    @Override
    public void updateLineListener() {
        horLine1.updateLineListener();
        horLine2.updateLineListener();
        if (vertLineOn != null) {
            vertLineOn.updateLineListener();
        }
        if (vertLineOff != null) {
            vertLineOff.updateLineListener();
        }
    }

    @Override
    public void repaint() {
        horLine1.repaint();
        horLine2.repaint();
        if (vertLineOn != null) {
            vertLineOn.repaint();
        }
        if (vertLineOff != null) {
            vertLineOff.repaint();
        }
    }

    @Override
    public void removeFromOwner() {
        if (horLine1 != null)
            owner.remove(horLine1);
        if (horLine2 != null)
            owner.remove(horLine2);
        if (vertLineOn != null)
            owner.remove(vertLineOn);
        if (vertLineOff != null)
            owner.remove(vertLineOff);
    }

    @Override
    public void addToOwner() {
        if (horLine1 != null)
            owner.add(horLine1, 0);
        if (horLine2 != null)
            owner.add(horLine2, 0);
        if (vertLineOn != null)
            owner.add(vertLineOn, 0);
        if (vertLineOff != null)
            owner.add(vertLineOff, 0);
    }

    @Override
    public TimeStation getClickedTimeSt() {
        return timeSt;
    }

    @Override
    public TimeStation getStationBegin() {
        return timeSt;
    }

    @Override
    public void drawInPicture(Graphics g) {
        if (horLine1 != null && horLine1.isVisible())
            horLine1.drawInPicture(g);
        if (horLine2 != null && horLine2.isVisible())
            horLine2.drawInPicture(g);
        if (vertLineOn != null && vertLineOn.isVisible())
            vertLineOn.drawInPicture(g);
        if (vertLineOff != null && vertLineOff.isVisible())
            vertLineOff.drawInPicture(g);
    }

    public int getIdLineSt() {
        return timeSt.lineStTrain.getIdLineSt();
    }

    public void setIdLineSt(int idLine) {
        int id = getIdLineSt();
        if (id != idLine) {
            String nameLine = timeSt.station.getLineName(idLine);
            if ("".equals(nameLine)) {
                idLine = 0;
            }
            trainThread.setLineStTr(timeSt.station.getIdPoint(), idLine);
            setLineBounds();
        }
    }

}
