package com.gmail.alaerof.dialog.locomotivetypeedit;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.util.ColorConverter;

public class LocTypeCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer{
    private static final long serialVersionUID = 1L;
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int col) {
        if (value != null)
            setText(value.toString());
        
        List<LocomotiveType> list = ((LocTypeTableModel) table.getModel()).getList();
        LocomotiveType el = list.get(row);

        Color background;
        Color foreground = Color.BLACK;
        if (row % 2 == 0) {
            background = Color.WHITE;
        } else {
            background = new Color(242, 242, 242);
        }
        if (isSelected) {
            background = Color.LIGHT_GRAY;
        }

        Font oldFont = getFont();
        if (col == 1) {
            Font newFont = new java.awt.Font(oldFont.getName(), java.awt.Font.BOLD, oldFont.getSize());
            setFont(newFont);
            Color clr = ColorConverter.convert(el.getColorLoc());
            setForeground(clr);
            background = clr;
        } else {
            Font newFont = new java.awt.Font(oldFont.getName(), 0, oldFont.getSize());
            setFont(newFont);
            setForeground(foreground);
        }

        setBackground(background);

        return this;
    }

}
