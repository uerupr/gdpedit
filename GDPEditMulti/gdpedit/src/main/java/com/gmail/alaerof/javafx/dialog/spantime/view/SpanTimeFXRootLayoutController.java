package com.gmail.alaerof.javafx.dialog.spantime.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class SpanTimeFXRootLayoutController extends  CommonDistrSpanController {
    @FXML
    private Label stBegin;
    @FXML
    private Label stBeginABS;
    @FXML
    private Label stEnd;
    @FXML
    private Label stEndABS;
    @FXML
    private Label spanLen;

    /** {@inheritDoc} */
    @Override
    protected void initialize() {

    }

    /** {@inheritDoc} */
    @Override
    protected void updateModelWithContent() {
        stBegin.setText(distrSpan.getStB().getNamePoint());
        stEnd.setText(distrSpan.getStE().getNamePoint());
        stBeginABS.setText(((Float)distrSpan.getSpanScale().getAbsKM()).toString());
        stEndABS.setText(((Float)distrSpan.getSpanScale().getAbsKM_E()).toString());
        spanLen.setText(((Integer)distrSpan.getSpanScale().getL()).toString());
    }
}
