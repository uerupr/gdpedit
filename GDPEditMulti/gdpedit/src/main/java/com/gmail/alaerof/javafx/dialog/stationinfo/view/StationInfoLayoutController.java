package com.gmail.alaerof.javafx.dialog.stationinfo.view;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.dto.Dir;
import com.gmail.alaerof.dao.dto.IntGR;
import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.StationIntGR;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDirDAO;
import com.gmail.alaerof.dao.interfacedao.IScaleDAO;
import com.gmail.alaerof.dao.interfacedao.IStationDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.dialog.ColorDialog;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.TaskWithCursorExecutor;
import com.gmail.alaerof.javafx.dialog.stationinfo.model.DirModel;
import com.gmail.alaerof.javafx.dialog.stationinfo.model.LineStIntModel;
import com.gmail.alaerof.javafx.dialog.stationinfo.model.LineStModel;
import com.gmail.alaerof.javafx.dialog.stationinfo.model.PassBldModel;
import com.gmail.alaerof.util.ColorConverter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Callback;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import java.util.ResourceBundle;


import static org.apache.commons.lang.StringUtils.trim;

public class StationInfoLayoutController implements Initializable {
    protected Logger logger = LogManager.getLogger(StationInfoLayoutController.class);
    protected ResourceBundle bundle;
    private DistrStation distrStation;
    private Station station;
    private Scene scene;

    /** ������� ����� */
    private TableView<LineStModel> lineStModelTable;
    /** ���� ����� */
    private List<LineStModel> lineStModelList = new ArrayList<>();
    /** ������� ���������� */
    private TableView<LineStIntModel> lineStIntModelTable;
    /** ���� ���������� */
    private List<LineStIntModel> lineStIntModelList = new ArrayList<>();
    /** ���� ��� ���� ��� */
    private List<DirModel> dirModelList = new ArrayList<>();
    /** ���� ��� ���� TypeSt */
    private List<String> typeStList = new ArrayList<>();
    /** ���� ��� ���� PassBld */
    private List<PassBldModel.PassBld> passBldList = new ArrayList<>();

    @FXML
    private BorderPane rootLineStPane;
    @FXML
    private BorderPane rootLineStIntPane;
    @FXML
    private AnchorPane mainInfoPane;

    //Station Info
    @FXML
    private TextField tfName;
    @FXML
    private TextField tfCodeESR1;
    @FXML
    private TextField tfCodeESR2;
    @FXML
    private TextField tfCodeExpress;
    @FXML
    private TextField tfMassOdd;
    @FXML
    private TextField tfMassEven;
    @FXML
    private TextField tfPutOdd;
    @FXML
    private TextField tfPutEven;
    @FXML
    private CheckBox cbBch;
    @FXML
    private ComboBox<String> cbTypeSt;
    @FXML
    private ComboBox<DirModel> cbDir;
    @FXML
    private ComboBox<PassBldModel.PassBld> cbPassBld;

    //LineStModel
    @FXML
    private Circle circleColor;
    @FXML
    private TextField tfTrainLength;
    @FXML
    private CheckBox cbClosePut;
    @FXML
    private CheckBox cbElPut;
    @FXML
    private CheckBox cbPlatPut;
    @FXML
    private CheckBox cbShadowPut;
    @FXML
    private CheckBox cbGrOdd;
    @FXML
    private CheckBox cbPassOdd;
    @FXML
    private CheckBox cbGrEven;
    @FXML
    private CheckBox cbPassEven;

    @FXML
    private TextField tfLineWidth;
    @FXML
    private Button buLineWidthApply;


    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        mainInfoPane.setDisable(GDPDAOFactoryCreater.getGDPDAOFactory().isServer());

        // ��� ����� ���������� �� ��� ����� � cbDir.
        Callback<ListView<DirModel>, ListCell<DirModel>> factory = lv -> new ListCell<DirModel>() {
            //@Override
            protected void updateItem(DirModel dirModel, boolean empty) {
                super.updateItem(dirModel, empty);
                setText(empty ? "" : dirModel.nameDirProperty().get());
            }
        };
        cbDir.setCellFactory(factory);

        // ��� ����� ���������� �� ��� ����� � cbTypeSt.
        Callback<ListView<String>, ListCell<String>> factory2 = lv -> new ListCell<String>() {
            //@Override
            protected void updateItem(String typeSt, boolean empty) {
                super.updateItem(typeSt, empty);
                setText(empty ? "" : typeSt);
            }
        };
        cbTypeSt.setCellFactory(factory2);

        // ��� ����� ���������� �� ��� ����� � cbPassBld.
        Callback<ListView<PassBldModel.PassBld>, ListCell<PassBldModel.PassBld>> factory3 = lv -> new ListCell<PassBldModel.PassBld>() {
            //@Override
            protected void updateItem(PassBldModel.PassBld passBld, boolean empty) {
                super.updateItem(passBld, empty);
                setText(empty ? "" : passBld.getString());
            }
        };
        cbPassBld.setCellFactory(factory3);

        // lineStModelTable
        rootLineStPane.autosize();
        EditableTableViewCreator<LineStModel> builderImp = new EditableTableViewCreator<>();
        lineStModelTable = builderImp.createEditableTableView();
        lineStModelTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
        rootLineStPane.setCenter(lineStModelTable);
        rootLineStPane.getCenter().autosize();
        String title;
        title = bundle.getString("stinfo.n");
        lineStModelTable.getColumns().add(builderImp.createIntegerColumn(title, LineStModel::nProperty, true));
        title = bundle.getString("stinfo.ps");
        lineStModelTable.getColumns().add(builderImp.createStringColumn(title, LineStModel::psProperty, true));
        lineStModelTable.getColumns().get(0).setPrefWidth(100);
        lineStModelTable.getColumns().get(1).setPrefWidth(100);
        for (int i = 0; i < lineStModelTable.getColumns().size(); i++){
            lineStModelTable.getColumns().get(i).setSortable(false);
        }
        TableView.TableViewSelectionModel<LineStModel> selectionModel = lineStModelTable.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<LineStModel>(){

            public void changed(ObservableValue<? extends LineStModel> val, LineStModel oldVal, LineStModel newVal){
                if(newVal != null) {
                    tfTrainLength.setText(String.valueOf(newVal.getLineSt().getLen()));
                    cbClosePut.setSelected(newVal.getLineSt().isClose());
                    cbElPut.setSelected(newVal.getLineSt().isEl());
                    cbPlatPut.setSelected(newVal.getLineSt().isPz());
                    cbShadowPut.setSelected(newVal.getLineSt().isCloseInGDP());
                    cbGrOdd.setSelected(newVal.getLineSt().isGrO());
                    cbPassOdd.setSelected(newVal.getLineSt().isPassO());
                    cbGrEven.setSelected(newVal.getLineSt().isGrE());
                    cbPassEven.setSelected(newVal.getLineSt().isPassE());
                    java.awt.Color clrAWT = ColorConverter.convert(newVal.getLineSt().getColorLn());
                    Color clrFX = ColorConverter.colorAWTtoFX(clrAWT);
                    circleColor.setFill(clrFX);
                }
            }
        });

        // lineStintGRTable
        rootLineStIntPane.autosize();
        EditableTableViewCreator<LineStIntModel> builderImp2 = new EditableTableViewCreator<>();
        lineStIntModelTable = builderImp2.createEditableTableView();
        lineStIntModelTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
        rootLineStIntPane.setCenter(lineStIntModelTable);
        rootLineStIntPane.getCenter().autosize();
        title = bundle.getString("stinfo.name");
        lineStIntModelTable.getColumns().add(builderImp2.createStringColumn(title, LineStIntModel::nameProperty, false));
        title = bundle.getString("stinfo.firsttype");
        lineStIntModelTable.getColumns().add(builderImp2.createStringColumn(title, LineStIntModel::firstTypeProperty, false));
        title = bundle.getString("stinfo.secondtype");
        lineStIntModelTable.getColumns().add(builderImp2.createStringColumn(title, LineStIntModel::secondTypeProperty, false));
        title = bundle.getString("stinfo.firststop");
        lineStIntModelTable.getColumns().add(builderImp2.createStringColumn(title, LineStIntModel::firstStopProperty, false));
        title = bundle.getString("stinfo.secondstop");
        lineStIntModelTable.getColumns().add(builderImp2.createStringColumn(title, LineStIntModel::secondStopProperty, false));
        title = bundle.getString("stinfo.defaultvalue");
        lineStIntModelTable.getColumns().add(builderImp2.createIntegerColumn(title, LineStIntModel::valueProperty, true));
        lineStIntModelTable.getColumns().get(0).setPrefWidth(180);
        lineStIntModelTable.getColumns().get(1).setPrefWidth(80);
        lineStIntModelTable.getColumns().get(2).setPrefWidth(80);
        lineStIntModelTable.getColumns().get(3).setPrefWidth(40);
        lineStIntModelTable.getColumns().get(4).setPrefWidth(40);
        lineStIntModelTable.getColumns().get(5).setPrefWidth(64);
        for (int i = 0; i < lineStIntModelTable.getColumns().size(); i++){
            lineStIntModelTable.getColumns().get(i).setSortable(false);
        }
    }

    private void showInformation(String titleMain, String titleMiddle, String titleSmall) {
        Alert alert;
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        alert.setContentText(titleSmall);
        alert.showAndWait();
    }

    @FXML
    private void chooseColor(ActionEvent event){
        ColorDialog cd = GDPEdit.gdpEdit.getDialogManager().getColorDialog();
        cd.setVisible(true);
        if (cd.getResult() > 0) {
            java.awt.Color clrAWT = cd.getColor();
            Color clrFX = ColorConverter.colorAWTtoFX(clrAWT);
            circleColor.setFill(clrFX);
        }
    }

    @FXML
    private void saveStation (ActionEvent event) throws DataManageException, ObjectNotFoundException {
        logger.debug("saveStationToDB");
        String titleMain = bundle.getString("javafx.dialog.stationinfo.dialog.attention");;
        String titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.changesave") + "?";
        String titleSmall = station.getName();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        alert.setContentText(titleSmall);
        if (alert.showAndWait().get() == ButtonType.OK){
            try {
                scene.setCursor(Cursor.WAIT);
                station.setCodeESR(tfCodeESR1.getText());
                station.setCodeExpress(tfCodeExpress.getText());
                station.setName(tfName.getText());
                station.setCodeESR2(tfCodeESR2.getText());
                station.setMaxMassO(Integer.parseInt(tfMassOdd.getText()));
                station.setMaxMassE(Integer.parseInt(tfMassEven.getText()));
                station.setCountLineO(Integer.parseInt(tfPutOdd.getText()));
                station.setCountLineE(Integer.parseInt(tfPutEven.getText()));
                station.setBr(cbBch.isSelected());
                int rowImport = cbPassBld.getSelectionModel().getSelectedIndex();
                station.setPassBld(rowImport);
                rowImport = cbDir.getSelectionModel().getSelectedIndex();
                station.setIdDir(dirModelList.get(rowImport).getDir().getIdDir());
                rowImport = cbTypeSt.getSelectionModel().getSelectedIndex();
                station.setType(typeStList.get(rowImport));
                IStationDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO();
                dao.updateStation(station);
                titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.good");
                showInformation(titleMain, titleMiddle, titleSmall);
            } catch (Exception e) {
                logger.error(e.toString(), e);
                titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.bad");
                showInformation(titleMain, titleMiddle, titleSmall);
            } finally {
                scene.setCursor(Cursor.DEFAULT);
            }
        }
    }

    @FXML
    private void addLineSt (ActionEvent event) {
        logger.debug("addLineSt");
        String titleMain = bundle.getString("javafx.dialog.stationinfo.dialog.attention");
        String titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.addlinest") + "?";
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        if (alert.showAndWait().get() == ButtonType.OK){
            int maxLineStN = 0;
            for (LineStModel aLineStModelList : lineStModelList) {
                if (maxLineStN < aLineStModelList.nProperty().get()) {
                    maxLineStN = aLineStModelList.nProperty().get();
                }
            }
            LineSt lineSt = new LineSt();
            lineSt.setLen(0);
            lineSt.setIdSt(station.getIdSt());
            lineSt.setIdLineSt(0);
            lineSt.setN(maxLineStN + 1);
            LineStModel lineStModel = new LineStModel(lineSt);
            lineStModelList.add(lineStModel);
            lineStModelTable.setItems(FXCollections.observableArrayList(lineStModelList));
        }
    }

    @FXML
    private void delLineSt (ActionEvent event) {
        logger.debug("delLineSt");
        int rowImport = lineStModelTable.getSelectionModel().getSelectedIndex();
        if (rowImport >= 0){
            String lineStName = String.valueOf(lineStModelList.get(rowImport).nProperty().get()) + " (" + lineStModelList.get(rowImport).psProperty().get() + ")";
            String titleMain = bundle.getString("javafx.dialog.stationinfo.dialog.attention");
            String titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.dellinest") + "?";
            String titleSmall = lineStName;
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            if (alert.showAndWait().get() == ButtonType.OK){
                try {
                    scene.setCursor(Cursor.WAIT);
                    IStationDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO();
                    dao.deleteLineSt(station.getIdSt(),lineStModelList.get(rowImport).getLineSt().getIdLineSt());
                    lineStModelList.remove(rowImport);
                    lineStModelTable.setItems(FXCollections.observableArrayList(lineStModelList));
                    List<LineSt> lineStList = new ArrayList<>();
                    for (LineStModel aLineStModelList : lineStModelList) {
                        lineStList.add(aLineStModelList.getLineSt());
                    }
                    station.setListLine(lineStList);
                    titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                    titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.good");
                    showInformation(titleMain, titleMiddle, titleSmall);
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                    titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                    titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.bad");
                    showInformation(titleMain, titleMiddle, titleSmall);
                } finally {
                    scene.setCursor(Cursor.DEFAULT);
                }
            }
        }
    }

    @FXML
    private void useLineSt (ActionEvent event) throws DataManageException {
        logger.debug("useLineSt");
        String titleMain = bundle.getString("javafx.dialog.stationinfo.dialog.attention");
        String titleMiddle;
        String titleSmall;
        int rowImport = lineStModelTable.getSelectionModel().getSelectedIndex();
        LineStModel lineStModel = lineStModelList.get(rowImport);
        try {
            if (existChenges(lineStModel)) {
                titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.changelinest");
                titleSmall = Integer.toString(lineStModelList.get(rowImport).getN()) + " (" + lineStModelList.get(rowImport).getPs() + ")";
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle(titleMain);
                alert.setHeaderText(titleMiddle);
                alert.setContentText(titleSmall);
                if (alert.showAndWait().get() == ButtonType.OK){
                    scene.setCursor(Cursor.WAIT);
                    LineSt lineSt = lineStModel.getLineSt();
                    lineSt.setN(lineStModel.getN());
                    lineSt.setPs(lineStModel.getPs());
                    lineSt.setLen(Integer.parseInt(tfTrainLength.getText()));
                    lineSt.setColorLn(ColorConverter.convertFX((Color) circleColor.fillProperty().get()));
                    lineSt.setClose(cbClosePut.isSelected());
                    lineSt.setEl(cbElPut.isSelected());
                    lineSt.setPz(cbPlatPut.isSelected());
                    lineSt.setCloseInGDP(cbShadowPut.isSelected());
                    lineSt.setGrO(cbGrOdd.isSelected());
                    lineSt.setGrE(cbGrEven.isSelected());
                    lineSt.setPassO(cbPassOdd.isSelected());
                    lineSt.setPassE(cbPassEven.isSelected());
                    lineStModel.setLineSt(lineSt);
                    lineStModelList.set(rowImport, lineStModel);
                    lineStModelTable.setItems(FXCollections.observableArrayList(lineStModelList));
                    titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                    titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.good");
                    showInformation(titleMain, titleMiddle, titleSmall);
                }
            } else {
                titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.nosave1");
                titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.nosave2");
                showInformation(titleMain, titleMiddle, titleSmall);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
            titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
            titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.bad");
            showInformation(titleMain, titleMiddle, titleSmall);
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    private boolean existChenges (LineStModel lineStModel) throws DataManageException {
        boolean boo = false;
        LineSt lineSt = lineStModel.getLineSt();
        if ((lineSt.getN() != lineStModel.getN()) || (!lineSt.getPs().equals(lineStModel.getPs()))) {
            boo = true;
        }
        if (lineSt.getLen() != Integer.parseInt(tfTrainLength.getText())) {
            boo = true;
        }
        int c = ColorConverter.convertFX((Color) circleColor.fillProperty().get());
        if (lineSt.getColorLn() != c) {
            boo = true;
        }
        if ((lineSt.isClose() != cbClosePut.isSelected()) || (lineSt.isEl() != cbElPut.isSelected()) || (lineSt.isPz() != cbPlatPut.isSelected()) || (lineSt.isCloseInGDP() != cbShadowPut.isSelected())) {
            boo = true;
        }
        if ((lineSt.isGrO() != cbGrOdd.isSelected()) || (lineSt.isGrE() != cbGrEven.isSelected()) || (lineSt.isPassO() != cbPassOdd.isSelected()) || (lineSt.isPassE() != cbPassEven.isSelected())) {
            boo = true;
        }
        return boo;
    }

    @FXML
    private void saveLineSt (ActionEvent event) {
        logger.debug("saveLineStToDB");
        String titleMain = bundle.getString("javafx.dialog.stationinfo.dialog.attention");
        String titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.changesave2") + "?";
        String titleSmall = station.getName();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        alert.setContentText(titleSmall);
        if (alert.showAndWait().get() == ButtonType.OK) {
            try {
                scene.setCursor(Cursor.WAIT);
                List<LineSt> lineStList = new ArrayList<>();
                for (LineStModel aLineStModelList : lineStModelList) {
                    lineStList.add(aLineStModelList.getLineSt());
                }
                IStationDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO();
                dao.updateLineSt(lineStList,station.getIdSt());
                station.setListLine(lineStList);
                titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.good");
                showInformation(titleMain, titleMiddle, titleSmall);
            } catch (Exception e) {
                logger.error(e.toString(), e);
                titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.bad");
                showInformation(titleMain, titleMiddle, titleSmall);
            } finally {
                scene.setCursor(Cursor.DEFAULT);
            }
        }
    }

    @FXML
    private void addLineStInt (ActionEvent event) throws DataManageException {
        logger.debug("addLineStInt");
        IStationDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO();
        List<IntGR> intGRList = dao.getAllIntGR();
        for (IntGR anIntGRList : intGRList) {
            boolean dopusk = true;
            for (LineStIntModel aLineStIntModelList : lineStIntModelList) {
                if (anIntGRList.getName().equals(aLineStIntModelList.getName()) &&
                    anIntGRList.getFirstType().getString().equals(aLineStIntModelList.getFirstType()) &&
                    anIntGRList.getSecondType().getString().equals(aLineStIntModelList.getSecondType()) &&
                    anIntGRList.isFirstStop() == aLineStIntModelList.isFirstStopB() &&
                    anIntGRList.isSecondStop() == aLineStIntModelList.isSecondStopB()) {
                    dopusk = false;
                }
            }
            if (dopusk) {
                LineStIntModel lineStIntModel = new LineStIntModel(anIntGRList);
                lineStIntModelList.add(lineStIntModel);
            }
        }
        lineStIntModelTable.setItems(FXCollections.observableArrayList(lineStIntModelList));
    }

    @FXML
    private void delLineStInt (ActionEvent event) throws DataManageException, ObjectNotFoundException {
        logger.debug("delLineStInt");
        int rowImport = lineStIntModelTable.getSelectionModel().getSelectedIndex();
        if (rowImport >= 0){
            String lineStIntName = lineStIntModelList.get(rowImport).nameProperty().get();
            String titleMain = bundle.getString("javafx.dialog.stationinfo.dialog.attention");
            String titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.dellinestint") + "?";
            String titleSmall = lineStIntName;
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            if (alert.showAndWait().get() == ButtonType.OK){
                lineStIntModelList.remove(rowImport);
                lineStIntModelTable.setItems(FXCollections.observableArrayList(lineStIntModelList));
            }
        }
    }

    @FXML
    private void saveLineStInt (ActionEvent event) {
        logger.debug("saveLineStIntToDB");
        String titleMain = bundle.getString("javafx.dialog.stationinfo.dialog.attention");
        String titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.changesave3") + "?";
        String titleSmall = station.getName();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        alert.setContentText(titleSmall);
        if (alert.showAndWait().get() == ButtonType.OK) {
            try {
                scene.setCursor(Cursor.WAIT);
                List<StationIntGR> stationIntGRSList = new ArrayList<>();
                for (int i = 0; i < lineStIntModelList.size(); i++){
                    StationIntGR stationIntGR = new StationIntGR();
                    stationIntGR.setIdSt(station.getIdSt());
                    stationIntGR.setIdIntGR(lineStIntModelList.get(i).getIdIntGR());
                    stationIntGR.setValue(lineStIntModelList.get(i).getValue());
                    stationIntGRSList.add(stationIntGR);
                }
                IStationDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO();
                dao.saveLineStInt(stationIntGRSList,station.getIdSt());
                titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.good");
                showInformation(titleMain, titleMiddle, titleSmall);
            } catch (Exception e) {
                logger.error(e.toString(), e);
                titleMiddle = bundle.getString("javafx.dialog.stationinfo.dialog.save");
                titleSmall = bundle.getString("javafx.dialog.stationinfo.dialog.bad");
                showInformation(titleMain, titleMiddle, titleSmall);
            } finally {
                scene.setCursor(Cursor.DEFAULT);
            }
        }
    }

    @FXML
    private void handleLineWidthApply(ActionEvent event){
        logger.debug("handleLineWidthApply - start");
        try {
            Task task = new Task<Void>() {
                @Override
                protected Void call() {
                    logger.debug("tfLineWidth : " + tfLineWidth.getText());
                    try {
                        int width = Integer.parseInt(tfLineWidth.getText());
                        distrStation.setWidthLineSt(width);
                        Scale scale = distrStation.getSpan().getSpanScale();
                        IScaleDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getScaleDAO();
                        dao.updateWidthLine(scale);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                        throw  new RuntimeException(e);
                    }
                    return null;
                }
            };
            TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, ()->{
                logger.debug("handleLineWidthApply - end");
            });
        } catch (Exception e) {
            logger.error(e.toString(), e);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("");
            alert.setHeaderText(e.toString());
            alert.showAndWait();
        }
    }

    public void setContent(DistrStation distrStation) {
        this.distrStation = distrStation;
        this.station = distrStation.getStation();
        scene.setCursor(Cursor.CROSSHAIR);
        try {
            logger.debug("updateModelWithContent");

            // station info
            tfLineWidth.setText("" + distrStation.getWidthLineSt());

            tfName.setText(trim(station.getName()));
            tfCodeESR1.setText(trim(station.getCodeESR()));
            tfCodeESR2.setText(trim(station.getCodeESR2()));
            tfCodeExpress.setText(trim(station.getCodeExpress()));
            tfMassOdd.setText(String.valueOf(station.getMaxMassO()));
            tfMassEven.setText(String.valueOf(station.getMaxMassE()));
            tfPutOdd.setText(String.valueOf(station.getCountLineO()));
            tfPutEven.setText(String.valueOf(station.getCountLineE()));
            cbBch.setSelected(station.isBr());
            dirModelList.clear();
            IDirDAO dirDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getDirDAO();
            List<Dir> dirList = dirDAO.getListDir();
            int lineDir = 0;
            for (int i = 0; i < dirList.size(); i++){
                Dir dir = dirList.get(i);
                dirModelList.add(new DirModel(dir));
                if (dir.getIdDir() == station.getIdDir()) {
                    lineDir = i;
                }
            }
            cbDir.setItems(FXCollections.observableArrayList(dirModelList));
            cbDir.getSelectionModel().select(lineDir);
            typeStList.clear();
            typeStList.add(station.TYPE_TECH);
            typeStList.add(station.TYPE_SP);
            cbTypeSt.setItems(FXCollections.observableArrayList(typeStList));
            if (station.getType().equals(station.TYPE_TECH)){
                cbTypeSt.getSelectionModel().select(0);
            } else {
                cbTypeSt.getSelectionModel().select(1);
            }
            passBldList.clear();
            passBldList.add(PassBldModel.PassBld.Non);
            passBldList.add(PassBldModel.PassBld.Odd);
            passBldList.add(PassBldModel.PassBld.Even);
            passBldList.add(PassBldModel.PassBld.Island);
            cbPassBld.setItems(FXCollections.observableArrayList(passBldList));
            cbPassBld.getSelectionModel().select(station.getPassBld());

            // lineStModelList -> lineStModelTabel
            lineStModelList = buildLineStModelList();
            lineStModelTable.setItems(FXCollections.observableArrayList(lineStModelList));
            lineStModelTable.getSelectionModel().select(0);

            // lineStIntModelList -> lineStIntModelTabel
            lineStIntModelList = buildLineStIntModelList(station.getIdSt());
            lineStIntModelTable.setItems(FXCollections.observableArrayList(lineStIntModelList));
            lineStIntModelTable.getSelectionModel().select(0);

        } catch (DataManageException | ObjectNotFoundException e) {
            logger.error(e.toString(), e);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(bundle.getString("stationinfo.error"));
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    private List<LineStModel> buildLineStModelList() throws DataManageException, ObjectNotFoundException {
        List<LineStModel> itemList = new ArrayList<>();
        for (int i = 0; i < station.getListLine().size(); i++){
             LineStModel lineStModel = new LineStModel(station.getListLine().get(i));
             itemList.add(lineStModel);
        }
        return itemList;
    }

    private List<LineStIntModel> buildLineStIntModelList(int idSt) throws DataManageException, ObjectNotFoundException {
        List<LineStIntModel> itemList = new ArrayList<>();
        IStationDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO();
        List<IntGR> intGRList = dao.getLineStInt(idSt);
        for (IntGR anIntGRList : intGRList) {
            LineStIntModel lineStIntModel = new LineStIntModel(anIntGRList);
            itemList.add(lineStIntModel);
        }
        return itemList;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
