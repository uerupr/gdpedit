package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;

public class DrawLineVert extends DrawLine {

    public DrawLineVert(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams) {
        super(generalParams, selfParams);
        direction = NewLine.Direction.vert;
    }

    @Override
    protected void calculateAllPoints() {
        final int npoints = 10;
        int x[] = new int[npoints];
        int y[] = new int[npoints];
        int pw = 0; //this.getIndent().width;
        int h = getSize().height;
        x[0] = pw;
        y[0] = 0;

        x[1] = x[0] + pw;
        y[1] = y[0];

        x[5] = x[4] = x[3] = x[2] = x[1];
        y[5] = y[4] = y[3] = y[2] = generalParams.codeStepO;

        x[6] = x[1];
        y[6] = h;

        x[7] = x[0];
        y[7] = y[6];

        x[8] = 0;
        y[8] = y[6];

        x[9] = x[8];
        y[9] = y[0];

        captionPointGDP.x = x[2]-pw;
        captionPointGDP.y = y[2];
        beginGDP.x = x[0]-pw;
        beginGDP.y = y[0];
        endGDP.x = x[7]-pw;
        endGDP.y = y[7];
    }
}
