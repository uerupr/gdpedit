package com.gmail.alaerof.javafx.dialog.gdpprint.view;

import com.gmail.alaerof.util.ListTextPrintParamComparator;
import com.gmail.alaerof.dao.dto.TextPrintParam;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IGDPTextPrintParamDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrEditEntity;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.controlsfx.dialog.FontSelectorDialog;


/**
 * Created by Paul M. Bui on 02.05.2018.
 */
public class GDPPrintTextParamLayoutController  implements Initializable {
    protected Logger logger = LogManager.getLogger(GDPPrintTextParamLayoutController.class);
    protected ResourceBundle bundle;
    private GDPEditPanel editPanel;
    private DistrEditEntity distrEntity;
    private Scene scene;

    @FXML
    private TextField tfHeadOne;
    @FXML
    private TextField tfHeadTwo;
    @FXML
    private TextField tfHeadThree;
    @FXML
    private TextField tfHeadFour;
    @FXML
    private TextField tfBossOne;
    @FXML
    private TextField tfBossTwo;
    @FXML
    private TextField tfBossThree;
    @FXML
    private TextField tfBossFour;
    @FXML
    private TextField tfWorkerOne;
    @FXML
    private TextField tfWorkerTwo;
    @FXML
    private CheckBox cbHeadOne;
    @FXML
    private CheckBox cbHeadTwo;
    @FXML
    private CheckBox cbHeadThree;
    @FXML
    private CheckBox cbHeadFour;
    @FXML
    private CheckBox cbBossOne;
    @FXML
    private CheckBox cbBossTwo;
    @FXML
    private CheckBox cbBossThree;
    @FXML
    private CheckBox cbBossFour;
    @FXML
    private CheckBox cbWorkerOne;
    @FXML
    private CheckBox cbWorkerTwo;
    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
    }

    @FXML
    private void saveToDB (ActionEvent event){
        logger.debug("saveToDB");
        String status = "Ok";
        scene.setCursor(Cursor.WAIT);
        // �������� � ����
        try {
            if (editPanel != null) {
                List<TextPrintParam> listTextPrintParam = new ArrayList<>();
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 1, (cbHeadOne.isSelected()) ? 1 : 0, tfHeadOne.getText(), tfHeadOne.getFont().toString(), 1));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 1, (cbHeadTwo.isSelected()) ? 1 : 0, tfHeadTwo.getText(), tfHeadTwo.getFont().toString(), 2));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 1, (cbHeadThree.isSelected()) ? 1 : 0, tfHeadThree.getText(), tfHeadThree.getFont().toString(), 3));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 1, (cbHeadFour.isSelected()) ? 1 : 0, tfHeadFour.getText(), tfHeadFour.getFont().toString(), 4));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 2, (cbBossOne.isSelected()) ? 1 : 0, tfBossOne.getText(), tfBossOne.getFont().toString(), 1));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 2, (cbBossTwo.isSelected()) ? 1 : 0, tfBossTwo.getText(), tfBossTwo.getFont().toString(), 2));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 2, (cbBossThree.isSelected()) ? 1 : 0, tfBossThree.getText(), tfBossThree.getFont().toString(), 3));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 2, (cbBossFour.isSelected()) ? 1 : 0, tfBossFour.getText(), tfBossFour.getFont().toString(), 4));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 3, (cbWorkerOne.isSelected()) ? 1 : 0, tfWorkerOne.getText(), tfWorkerOne.getFont().toString(), 1));
                listTextPrintParam.add(new TextPrintParam(distrEntity.getIdDistr(), 3, (cbWorkerTwo.isSelected()) ? 1 : 0, tfWorkerTwo.getText(), tfWorkerTwo.getFont().toString(), 2));
                IGDPTextPrintParamDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getTextPrintParamDAO();
                dao.deleteTextPrintParam(distrEntity.getIdDistr());
                dao.addTextPrintParam(listTextPrintParam);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
            status = "ERR: " + e.getMessage();
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("");
        alert.setHeaderText(status);
        alert.show();
    }

    @FXML
    private void setFont (ActionEvent event){
        logger.debug("setFont");
        scene.setCursor(Cursor.WAIT);
        try {
            Pattern pIDButton = Pattern.compile("id=(.*?),");
            Matcher mIDButton = pIDButton.matcher(event.getSource().toString());
            if (mIDButton.find()) {
                String buttonName = mIDButton.group().subSequence(3, mIDButton.group().length() - 1).toString();
                FontSelectorDialog fontSelector = new FontSelectorDialog(Font.getDefault());
                switch (buttonName) {
                    case "bHeadOne":
                        fontSelector = new FontSelectorDialog(tfHeadOne.getFont());
                        break;
                    case "bHeadTwo":
                        fontSelector = new FontSelectorDialog(tfHeadTwo.getFont());
                        break;
                    case "bHeadThree":
                        fontSelector = new FontSelectorDialog(tfHeadThree.getFont());
                        break;
                    case "bHeadFour":
                        fontSelector = new FontSelectorDialog(tfHeadFour.getFont());
                        break;
                    case "bBossOne":
                        fontSelector = new FontSelectorDialog(tfBossOne.getFont());
                        break;
                    case "bBossTwo":
                        fontSelector = new FontSelectorDialog(tfBossTwo.getFont());
                        break;
                    case "bBossThree":
                        fontSelector = new FontSelectorDialog(tfBossThree.getFont());
                        break;
                    case "bBossFour":
                        fontSelector = new FontSelectorDialog(tfBossFour.getFont());
                        break;
                    case "bWorkerOne":
                        fontSelector = new FontSelectorDialog(tfWorkerOne.getFont());
                        break;
                    case "bWorkerTwo":
                        fontSelector = new FontSelectorDialog(tfWorkerTwo.getFont());
                        break;
                }
                fontSelector.showAndWait();
                switch (buttonName) {
                    case "bHeadOne":
                        tfHeadOne.setFont(fontSelector.getResult());
                        break;
                    case "bHeadTwo":
                        tfHeadTwo.setFont(fontSelector.getResult());
                        break;
                    case "bHeadThree":
                        tfHeadThree.setFont(fontSelector.getResult());
                        break;
                    case "bHeadFour":
                        tfHeadFour.setFont(fontSelector.getResult());
                        break;
                    case "bBossOne":
                        tfBossOne.setFont(fontSelector.getResult());
                        break;
                    case "bBossTwo":
                        tfBossTwo.setFont(fontSelector.getResult());
                        break;
                    case "bBossThree":
                        tfBossThree.setFont(fontSelector.getResult());
                        break;
                    case "bBossFour":
                        tfBossFour.setFont(fontSelector.getResult());
                        break;
                    case "bWorkerOne":
                        tfWorkerOne.setFont(fontSelector.getResult());
                        break;
                    case "bWorkerTwo":
                        tfWorkerTwo.setFont(fontSelector.getResult());
                        break;
                }
            }
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    public void setContent(DistrEditEntity distrEntity, GDPEditPanel editPanel) {
        logger.debug("setContent");
        this.distrEntity = distrEntity;
        this.editPanel = editPanel;
        try {
            List<TextPrintParam> listTextPrintParam = GDPDAOFactoryCreater.getGDPDAOFactory().getTextPrintParamDAO().getListTextPrintParam(distrEntity.getIdDistr());
            // ���������� listTextPrintParam �� tpType � �� num
            Collections.sort(listTextPrintParam, new ListTextPrintParamComparator());
            // ���������� ����� (���� ������, � ��� ���������)
            if (listTextPrintParam.size() > 0){
                cbHeadOne.setSelected(listTextPrintParam.get(0).getBChoise());
                tfHeadOne.setText(listTextPrintParam.get(0).getTextField());
                tfHeadOne.setFont(new Font(listTextPrintParam.get(0).getFontName(),listTextPrintParam.get(0).getFontSize()));
                cbHeadTwo.setSelected(listTextPrintParam.get(1).getBChoise());
                tfHeadTwo.setText(listTextPrintParam.get(1).getTextField());
                tfHeadTwo.setFont(new Font(listTextPrintParam.get(1).getFontName(),listTextPrintParam.get(1).getFontSize()));
                cbHeadThree.setSelected(listTextPrintParam.get(2).getBChoise());
                tfHeadThree.setText(listTextPrintParam.get(2).getTextField());
                tfHeadThree.setFont(new Font(listTextPrintParam.get(2).getFontName(),listTextPrintParam.get(2).getFontSize()));
                cbHeadFour.setSelected(listTextPrintParam.get(3).getBChoise());
                tfHeadFour.setText(listTextPrintParam.get(3).getTextField());
                tfHeadFour.setFont(new Font(listTextPrintParam.get(3).getFontName(),listTextPrintParam.get(3).getFontSize()));
                cbBossOne.setSelected(listTextPrintParam.get(4).getBChoise());
                tfBossOne.setText(listTextPrintParam.get(4).getTextField());
                tfBossOne.setFont(new Font(listTextPrintParam.get(4).getFontName(),listTextPrintParam.get(4).getFontSize()));
                cbBossTwo.setSelected(listTextPrintParam.get(5).getBChoise());
                tfBossTwo.setText(listTextPrintParam.get(5).getTextField());
                tfBossTwo.setFont(new Font(listTextPrintParam.get(5).getFontName(),listTextPrintParam.get(5).getFontSize()));
                cbBossThree.setSelected(listTextPrintParam.get(6).getBChoise());
                tfBossThree.setText(listTextPrintParam.get(6).getTextField());
                tfBossThree.setFont(new Font(listTextPrintParam.get(6).getFontName(),listTextPrintParam.get(6).getFontSize()));
                cbBossFour.setSelected(listTextPrintParam.get(7).getBChoise());
                tfBossFour.setText(listTextPrintParam.get(7).getTextField());
                tfBossFour.setFont(new Font(listTextPrintParam.get(7).getFontName(),listTextPrintParam.get(7).getFontSize()));
                cbWorkerOne.setSelected(listTextPrintParam.get(8).getBChoise());
                tfWorkerOne.setText(listTextPrintParam.get(8).getTextField());
                tfWorkerOne.setFont(new Font(listTextPrintParam.get(8).getFontName(),listTextPrintParam.get(8).getFontSize()));
                cbWorkerTwo.setSelected(listTextPrintParam.get(9).getBChoise());
                tfWorkerTwo.setText(listTextPrintParam.get(9).getTextField());
                tfWorkerTwo.setFont(new Font(listTextPrintParam.get(9).getFontName(),listTextPrintParam.get(9).getFontSize()));
            } else {
                cbHeadOne.setSelected(false);
                tfHeadOne.setText("");
                tfHeadOne.setFont(Font.getDefault());
                cbHeadTwo.setSelected(false);
                tfHeadTwo.setText("");
                tfHeadTwo.setFont(Font.getDefault());
                cbHeadThree.setSelected(false);
                tfHeadThree.setText("");
                tfHeadThree.setFont(Font.getDefault());
                cbHeadFour.setSelected(false);
                tfHeadFour.setText("");
                tfHeadFour.setFont(Font.getDefault());
                cbBossOne.setSelected(false);
                tfBossOne.setText("");
                tfBossOne.setFont(Font.getDefault());
                cbBossTwo.setSelected(false);
                tfBossTwo.setText("");
                tfBossTwo.setFont(Font.getDefault());
                cbBossThree.setSelected(false);
                tfBossThree.setText("");
                tfBossThree.setFont(Font.getDefault());
                cbBossFour.setSelected(false);
                tfBossFour.setText("");
                tfBossFour.setFont(Font.getDefault());
                cbWorkerOne.setSelected(false);
                tfWorkerOne.setText("");
                tfWorkerOne.setFont(Font.getDefault());
                cbWorkerTwo.setSelected(false);
                tfWorkerTwo.setText("");
                tfWorkerTwo.setFont(Font.getDefault());
            }
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
