package com.gmail.alaerof.javafx.dialog.calculatetimeenergy;

import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.CalcModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.TrainThreadModel;
import com.gmail.alaerof.util.FileUtil;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableView;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;

public class ExportSCVHandler {
    private static Logger logger = LogManager.getLogger(ExportSCVHandler.class);

    public static boolean saveToCSV(File file, ResourceBundle bundle,
                                 TreeTableView<CalcModel> distrCalcTreeTable,
                                 TreeTableView<TrainThreadModel> trainThreadTable)
    {
        boolean res = true;
        String fileNn = FilenameUtils.getName(file.getAbsolutePath());
        String filePath = file.getAbsolutePath().replace(fileNn, "");
        FileUtil.lastFileChooserPath = new File(filePath);
        String fileBase = FilenameUtils.getBaseName(file.getAbsolutePath());
        String fileNameD = filePath + fileBase + "_distr.csv";
        String fileNameT = filePath + fileBase + "_train.csv";
        File fileD = new File(fileNameD);
        File fileT = new File(fileNameT);

        try (FileWriter writer = new FileWriter(fileD)) {
            TreeItem<CalcModel> root = distrCalcTreeTable.getRoot();
            List<String> list = ExportExcelHandler.getTabStringDistrCalcModel(bundle);
            String tabStr = getTabString(list);
            writer.write(tabStr);
            writer.write("\n");
            for (TreeItem<CalcModel> item : root.getChildren()) {
                writer.write(item.getValue().getTabValuesString());
                writer.write("\n");
                for (TreeItem<CalcModel> itemc : item.getChildren()) {
                    writer.write(itemc.getValue().getTabValuesString());
                    writer.write("\n");
                }
            }
        } catch (IOException e) {
            logger.error(e.toString(), e);
            res = false;
        }

        try (FileWriter writer = new FileWriter(fileT)) {
            List<String> list = ExportExcelHandler.getTabStringTrainThreadModel(bundle);
            String tabStr = getTabString(list);
            writer.write(tabStr);
            writer.write("\n");
            for (TreeItem<TrainThreadModel> item : trainThreadTable.getRoot().getChildren()) {
                TrainThreadModel model = item.getValue();
                writer.write(model.getTabValuesString());
                writer.write("\n");
                for (TreeItem<TrainThreadModel> itemE : item.getChildren()) {
                    TrainThreadModel modelE = itemE.getValue();
                    writer.write(modelE.getTabValuesString());
                    writer.write("\n");
                }
            }
        } catch (IOException e) {
            logger.error(e.toString(), e);
            res = false;
        }
        return res;
    }

    private  static String getTabString(List<String> titleList){
        StringBuilder sb = new StringBuilder();
        String s = FileUtil.CSV_SEPARATOR;
        for(String title: titleList) {
            sb.append(s).append(title);
        }
        return sb.toString();
    }

}
