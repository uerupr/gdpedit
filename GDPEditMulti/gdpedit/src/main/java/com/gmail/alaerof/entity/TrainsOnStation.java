package com.gmail.alaerof.entity;

import com.gmail.alaerof.dao.dto.TrainCombSt;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import java.util.ArrayList;
import java.util.Collection;

import com.gmail.alaerof.entity.train.TrainStDouble;
import com.gmail.alaerof.entity.train.TrainThread;
import java.util.List;
import java.util.Map;

/**
 */
public class TrainsOnStation {
    /** ��� ������� */
    private int idSt;
    /** ����� ������� �� �������, ���� = IDtrain */
    private Map<Long, TrainThread> trainMap;
    /** ������ "������" ������� �� ������� */
    private List<TrainStDouble> listTrainStDouble;

    public Map<Long, TrainThread> getTrainMap() {
        return trainMap;
    }

    public List<TrainThread> getTrainList() {
        List<TrainThread> list = new ArrayList<>();
        if (trainMap != null) {
            list.addAll(trainMap.values());
        }
        return list;
    }

    public void setTrainMap(Map<Long, TrainThread> trainMap) {
        this.trainMap = trainMap;
    }

    public TrainsOnStation(int idSt){
        this.idSt = idSt;
    }
    
    public boolean isLoaded(){
        return trainMap == null;
    }
    
    public int getIdSt(){
        return idSt;
    }
    
    public void setAllTrainLocation(){
        if (trainMap != null) {
            Collection<TrainThread> list = trainMap.values();
            for (TrainThread tr : list) {
                tr.setTrainLocation();
            }
        }
        if (listTrainStDouble!=null){
            for(int i=0; i< listTrainStDouble.size(); i++){
                TrainStDouble trc =  listTrainStDouble.get(i);
                if(trc!=null){
                    trc.setLineBounds();
                }
            }
        }
    }

    public List<TrainStDouble> getListTrainStDouble() {
        return listTrainStDouble;
    }

    public void setListTrainStDouble(List<TrainStDouble> listTrainStDouble) {
        this.listTrainStDouble = listTrainStDouble;
    }

    public void removeTrainStDouble(TrainStDouble trStD){
        listTrainStDouble.remove(trStD);
        trStD.removeFromTrains();
        trStD.removeFromOwner();
    }

    public Collection<DistrTrain> getDistrTrains() {
        List<TrainThread> list = getTrainList();
        List<DistrTrain> distrTrains = new ArrayList<>();
        for (TrainThread thread : list) {
            distrTrains.add(thread.getDistrTrain());
        }
        return distrTrains;
    }

    public Collection<TrainCombSt> getTrainCombSts() {
        List<TrainCombSt> trainCombSts = new ArrayList<>();
        for (TrainStDouble trainStDouble : listTrainStDouble) {
            trainCombSts.add(trainStDouble.getTrainCombSt());
        }
        return trainCombSts;
    }
}
