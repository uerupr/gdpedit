package com.gmail.alaerof.dialog.linesttrain.model;

import com.gmail.alaerof.dao.dto.gdp.DistrTrain;

public class TRCombST {

    private long idTrainOn;

    private int idDistrOn;

    private String trNameOn;

    private String trCodeOn;

    private long idTrainOff;

    private int idDistrOff;

    private String trNameOff;

    private String trCodeOff;

    private int colorTR;

    private int lineStyle;

    private int widthTR;

    public long getIdTrainOn() {
        return idTrainOn;
    }

    public int getIdDistrOn() {
        return idDistrOn;
    }

    public String getTrNameOn() {
        return trNameOn;
    }

    public String getTrCodeOn() {
        return trCodeOn;
    }

    public long getIdTrainOff() {
        return idTrainOff;
    }

    public int getIdDistrOff() {
        return idDistrOff;
    }

    public String getTrNameOff() {
        return trNameOff;
    }

    public String getTrCodeOff() {
        return trCodeOff;
    }

    public int getColorTR() {
        return colorTR;
    }

    public int getLineStyle() {
        return lineStyle;
    }

    public int getWidthTR() {
        return widthTR;
    }

    public void setIdTrainOn(long idTrainOn) {
        this.idTrainOn = idTrainOn;
    }

    public void setIdDistrOn(int idDistrOn) {
        this.idDistrOn = idDistrOn;
    }

    public void setTrNameOn(String trNameOn) {
        this.trNameOn = trNameOn;
    }

    public void setTrCodeOn(String trCodeOn) {
        this.trCodeOn = trCodeOn;
    }

    public void setIdTrainOff(long idTrainOff) {
        this.idTrainOff = idTrainOff;
    }

    public void setIdDistrOff(int idDistrOff) {
        this.idDistrOff = idDistrOff;
    }

    public void setTrNameOff(String trNameOff) {
        this.trNameOff = trNameOff;
    }

    public void setTrCodeOff(String trCodeOff) {
        this.trCodeOff = trCodeOff;
    }

    public void setColorTR(int colorTR) {
        this.colorTR = colorTR;
    }

    public void setLineStyle(int lineStyle) {
        this.lineStyle = lineStyle;
    }

    public void setWidthTR(int widthTR) {
        this.widthTR = widthTR;
    }
}
