package com.gmail.alaerof.components.mainframe.gdp.action;

import com.gmail.alaerof.components.tab.ButtonTabPane;
import com.gmail.alaerof.components.tab.TitledTab;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.gmail.alaerof.components.mainframe.gdp.action.listspan.GDPActionListSpan;
import com.gmail.alaerof.components.mainframe.gdp.action.listst.GDPActionListSt;
import com.gmail.alaerof.components.mainframe.gdp.action.listtrain.GDPActionListTrain;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * ������ ������� ��� ���������� ��������������� ��� (1 ������ ��� ���� ���)
 * @author Helen Yrofeeva
 * 
 */
public class GDPActionTabPane extends ButtonTabPane {
    private static final long serialVersionUID = 1L;
    private GDPActionListSt actionListSt;
    private GDPActionListSpan actionListSpan;
    private static ResourceBundle bundle;

    public GDPActionTabPane() {
        List<TitledTab> listC = new ArrayList<>();
        actionListSt = new GDPActionListSt();
        listC.add(new TitledTab(bundle.getString("listtrain.schedule"), actionListSt));
        actionListSpan = new GDPActionListSpan();
        listC.add(new TitledTab(bundle.getString("listtrain.stages"), actionListSpan));
        super.initTabs(listC);
        this.setFocusCycleRoot(true);
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.listtrain", LocaleManager.getLocale());
    }

    protected GDPActionListSt getActionListSt() {
        return actionListSt;
    }

    protected GDPActionListSpan getActionListSpan() {
        return actionListSpan;
    }

    /**
     * ��������� �������� �� ������� ������� (����������), ���� �� ���
     */
    protected void addActionListStTab() {
        boolean ins = true;
        for (int i = 0; i < this.getTabCount(); i++) {
            Component item = this.getComponentAt(i);
            if (item instanceof GDPActionListSt) {
                ins = false;
            }
        }
        if (ins) {
            actionListSt = new GDPActionListSt();
            add(bundle.getString("listtrain.schedule"), actionListSt);
            initTabComponent(this.getTabCount() - 1, null);
        }
    }

    /**
     * ��������� �������� �� ������� ���������, ���� �� ���
     */
    protected void addActionListSpanTab() {
        boolean ins = true;
        for (int i = 0; i < this.getTabCount(); i++) {
            Component item = this.getComponentAt(i);
            if (item instanceof GDPActionListSpan) {
                ins = false;
            }
        }
        if (ins) {
            actionListSpan = new GDPActionListSpan();
            add(bundle.getString("listtrain.stages"), actionListSpan);
            initTabComponent(this.getTabCount() - 1, null);
        }
    }
    
    /**
     * �������� ������ ���� action �����
     */
    protected void clearTab(){
        if (actionListSt != null) {
            actionListSt.setListDistrPoints(null);
        }
        if (actionListSpan != null) {
            actionListSpan.setDistrEditEntity(null);
        }
    }

}
