package com.gmail.alaerof.javafx.dialog.distrcomb;

import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.distrcomb.view.DistrCombLayoutController;
import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

public class DistrCombFXDialog extends SwingFXDialogBase {
    private DistrEditEntity distrEditEntity;
    private DistrCombLayoutController controller;
    public DistrCombFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/distrcomb/view/DistrCombLayout.fxml";
        resourceBundleBaseName = "bundles.DistrCombFXDialog";
    }
    public DistrEditEntity getDistrEntity() {
        return distrEditEntity;
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 600, 500};
        return bounds;
    }

    public void setDialogContent(DistrEditEntity distrEditEntity) {
        this.distrEditEntity = distrEditEntity;

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(distrEditEntity);
            }
        });
    }
}
