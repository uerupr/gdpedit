package com.gmail.alaerof.gdpcalculation.model;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.SpTOcc;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.util.TimeConverter;
import java.util.HashMap;
import java.util.Map;

/**
 * �������� ��� DistrTrain ��� ��������������� ���������� ���
 */
public class DistrTrainGDPCalc {
    private LocomotiveType locomotiveType;
    private DistrTrain distrTrain;
    private Category category;
    private boolean fixed;
    private Map<Integer, TimeStCalc> timeStCalcMap = new HashMap<>();
    private Map<Integer, TimeSpCalc> timeSpCalcMap = new HashMap<>();

    public DistrTrainGDPCalc(DistrTrain distrTrain, Category category, boolean fixed){
        this.distrTrain = distrTrain;
        this.category = category;
        this.fixed = fixed;
        buildSt(distrTrain);
        buildSp(distrTrain);
    }

    private void buildSt(DistrTrain distrTrain) {
        for(LineStTrain lineStTrain: distrTrain.getListLineStTrain()){
            TimeStCalc timeStCalc = new TimeStCalc(lineStTrain);
            timeStCalcMap.put(lineStTrain.getIdSt(), timeStCalc);
        }
    }

    private void buildSp(DistrTrain distrTrain) {
        for(SpTOcc spTOcc: distrTrain.getMapSpTOcc().values()){
            TimeSpCalc timeSpCalc = new TimeSpCalc(spTOcc);
            timeSpCalcMap.put(spTOcc.getIdSpan(), timeSpCalc);
        }
    }

    public DistrTrain getDistrTrain() {
        return distrTrain;
    }

    public Category getCategory() {
        return category;
    }

    public boolean isFixed() {
        return fixed;
    }

    public Map<Integer, TimeStCalc> getTimeStCalcMap() {
        return timeStCalcMap;
    }

    public Map<Integer, TimeSpCalc> getTimeSpCalcMap() {
        return timeSpCalcMap;
    }

    public LocomotiveType getLocomotiveType() {
        return locomotiveType;
    }

    public void setLocomotiveType(LocomotiveType locomotiveType) {
        this.locomotiveType = locomotiveType;
    }

    public boolean isForeign(DistrEntity distrEntity) {
        return this.getDistrTrain().getIdDistr() != distrEntity.getIdDistr();
    }

    public int getMass() {
        // todo ���-�� ����� ����� ������
        /*
        �������� ������� � Category �� ��������� ����� � � Distr_Train �������������� �� �������
        ������������� � ���� ������� ��� ��� �� ��� �������� � ��.
         */
        return 0;
    }

    /**
     * ���� �� ��������� �� �������
     * @param idSt
     * @return boolean
     */
    public boolean isStop(int idSt) {
        TimeStCalc timeStCalc = timeStCalcMap.get(idSt);
        if (timeStCalc != null) {
            if (timeStCalc.getmOn() >= 0 && timeStCalc.getmOff() >= 0) {
                return timeStCalc.getmOn() != timeStCalc.getmOff();
            }
        }
        return false;
    }

    /**
     * ������ �� ���� ��������� �� �������
     * @param idSt
     * @return boolean
     */
    public boolean mustStop(int idSt) {
        TimeStCalc timeStCalc = timeStCalcMap.get(idSt);
        if (timeStCalc != null) {
            return timeStCalc.getLineStTrain().getStop()!=0;
        }
        return false;
    }

    /**
     * ����������� ����� ������� � �������
     * @param idSt
     * @return int
     */
    public int getTrainStop(int idSt) {
        TimeStCalc timeStCalc = timeStCalcMap.get(idSt);
        if (timeStCalc != null) {
            return timeStCalc.getLineStTrain().getTstop();
        }
        return 0;
    }

    public void setEndLeave(int idSt, int[] varTime) {
        TimeStCalc timeStCalc = timeStCalcMap.get(idSt);
        if (timeStCalc != null) {
            timeStCalc.setEndLeave(varTime);
        }
    }

    public void trainMustStop(int idSt, int dtAR) {
        LineStTrain lineStTrain = new LineStTrain();
        lineStTrain.setIdSt(idSt);
        lineStTrain.setIdLineSt(0);
        lineStTrain.setIdTrain(distrTrain.getIdTrain());
        lineStTrain.setStop(2);
        lineStTrain.setTstop(dtAR);
        distrTrain.getMapLineStTr().put(idSt, lineStTrain);

        TimeStCalc timeStCalc = new TimeStCalc(lineStTrain);
        timeStCalcMap.put(idSt, timeStCalc);
    }

    public void newOff(int idLineSt, int idSt, int delta, boolean setOff, int[] varTime) {
        TimeStCalc timeStCalc = timeStCalcMap.get(idSt);
        if (timeStCalc != null) {
            int newOff = TimeConverter.addMinutes(timeStCalc.getmOff(), delta);
            if (setOff) {
                timeStCalc.setLeaveSt(newOff, idLineSt, false);
            }
            varTime[0] = newOff;
        }
    }

    public void newArr(int idLineSt, int idSt, int delta, boolean setOn, int[] varTime) {
        TimeStCalc timeStCalc = timeStCalcMap.get(idSt);
        if (timeStCalc != null) {
            int newOn = TimeConverter.addMinutes(timeStCalc.getmOn(), delta);
            if (setOn) {
                timeStCalc.setArriveSt(newOn, idLineSt);
            }
            varTime[0] = newOn;
        }
    }

    public void setLeaveSt(DistrStationGDPCalc st, int idLineSt, int off, boolean calcTStop) {
        int idSt = st.getDistrStation().getIdPoint();
        String tm = TimeConverter.minutesToTime(off);
        TimeStCalc time = timeStCalcMap.get(idSt);
        if (time == null) {
            LineStTrain lineStTrain = new LineStTrain();
            lineStTrain.setIdSt(idSt);
            lineStTrain.setIdLineSt(idLineSt);
            lineStTrain.setIdTrain(distrTrain.getIdTrain());
            lineStTrain.setOccupyOn(tm);
            lineStTrain.setOccupyOff(tm);
            distrTrain.getMapLineStTr().put(idSt, lineStTrain);

            TimeStCalc timeStCalc = new TimeStCalc(lineStTrain);
            timeStCalcMap.put(idSt, timeStCalc);
        }
        if (time != null) {
            LineStTrain lineStTrain = time.getLineStTrain();
            time.setLeaveSt(off, idLineSt, calcTStop);
            if (lineStTrain.getOccupyOn() == null) {
                lineStTrain.setOccupyOn(tm);
            }
        }
        st.removeTrain(this);
        st.onStation(idSt, this);
    }
    /**
     * �������� �������
     * @param distrSpan
     * @param off ����� ����� �� �������
     */
    public void onSpan(DistrSpanGDPCalc distrSpan, int off) {
        int idSpan = distrSpan.getDistrSpan().getSpanScale().getIdSpan();
        SpTOcc spTOcc = new SpTOcc();
        String on = TimeConverter.minutesToTime(off);
        spTOcc.setOccupyOn(on);
        spTOcc.setIdSpan(idSpan);
        spTOcc.setIdTrain(distrTrain.getIdTrain());
        spTOcc.setIdDistr(distrTrain.getIdDistr());
        spTOcc.setOe(distrTrain.getOe().getString());

        distrTrain.getMapSpTOcc().put(idSpan, spTOcc);
        TimeSpCalc timeSpCalc = new TimeSpCalc(spTOcc);
        timeSpCalcMap.put(idSpan, timeSpCalc);
        distrSpan.onSpan(this);
    }
}
