package com.gmail.alaerof.exportexcel;

import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.util.TimeConverter;
import java.util.Comparator;

public class ExportToExcelSortTimeMuveComparator implements Comparator<TrainThread> {
    @Override
    public int compare(TrainThread trainThread1, TrainThread trainThread2) {

        String sch1 = trainThread1.getDistrTrain().getOccupyOff();
        String sch2 = trainThread2.getDistrTrain().getOccupyOff();
        int time1 = TimeConverter.timeToMinutes(sch1);
        int time2 = TimeConverter.timeToMinutes(sch2);
        //int time1 = (Integer.parseInt(sch1.substring(0,1)) * 10 + Integer.parseInt(sch1.substring(1,2))) * 60 + Integer.parseInt(sch1.substring(3,4)) * 10 + Integer.parseInt(sch1.substring(4,5));
        //int time2 = (Integer.parseInt(sch2.substring(0,1)) * 10 + Integer.parseInt(sch2.substring(1,2))) * 60 + Integer.parseInt(sch2.substring(3,4)) * 10 + Integer.parseInt(sch2.substring(4,5));
        if (time1 < time2){
            return -1;
        } else{
            return 0;
        }
    }
}
