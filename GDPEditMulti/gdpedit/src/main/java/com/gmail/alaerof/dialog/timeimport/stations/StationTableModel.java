package com.gmail.alaerof.dialog.timeimport.stations;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * �.�. �.�.������� �� �������
 * @author Helen Yrofeeva
 *
 */
public class StationTableModel extends AbstractTableModel{
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("stations.number"),
            bundle.getString("stations.ecp"),
            bundle.getString("stations.ecp2"),
            bundle.getString("stations.express"),
            bundle.getString("stations.name") };

    private List<Station> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.stations", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            Station el = list.get(row);
            switch (col) {
            case 0:
                return row+1;
            case 1:
                return el.getCodeESR();
            case 2:
                return el.getCodeESR2();
            case 3:
                return el.getCodeExpress();
            case 4:
                return el.getName() ;
            }
        }
        return null;
    }

    public List<Station> getList() {
        return list;
    }

    public void setList(List<Station> list) {
        this.list = list;
    }

}
