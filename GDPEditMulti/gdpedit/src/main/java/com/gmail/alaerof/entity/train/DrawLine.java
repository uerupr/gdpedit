package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import com.gmail.alaerof.util.CalcString;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;

public abstract class DrawLine {
    protected TrainThreadGeneralParams generalParams;
    protected NewLineSelfParams selfParams;

    protected NewLine.Direction direction;

    /** ������� ��������� ������� */
    private NewLine.DrawCaption caption = NewLine.DrawCaption.NoCaption;
    /** ���������� ����� �������� */
    private boolean isTimeA;
    /** ���������� ����� ����������� */
    private boolean isTimeL;
    /** ������� ��������� "��������" ��� ���. ������� */
    private NewLine.DrawTex drawTex = NewLine.DrawTex.NoTex;

    protected int xleft;
    protected int ytop;
    protected Dimension size = new Dimension();
    protected double alphaCaption;
    protected Point captionPointGDP = new Point();
    protected Point captionPointGDP1 = new Point();
    protected Point beginGDP = new Point();
    protected Point endGDP = new Point();
    protected Point pointTimeAGDP = new Point();
    protected Point pointTimeLGDP = new Point();

    public DrawLine(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams) {
        this.generalParams = generalParams;
        this.selfParams = selfParams;
    }

    public void setPosition(int x, int y){
        xleft = x;
        ytop = y;
    }

    protected abstract void calculateAllPoints();

    public boolean isCaption() {
        return caption != NewLine.DrawCaption.NoCaption;
    }

    protected Dimension getCaptionSize() {
        int h = CalcString.getStringH(generalParams.captionSp);
        int w = CalcString.getStringW(generalParams.captionSp, selfParams.getCaptionValue()) + 2;
        return new Dimension(w, h);
    }

    protected Dimension getTimeSize() {
        int h = CalcString.getStringH(generalParams.time);
        int w = CalcString.getStringW(generalParams.time, "0");
        return new Dimension(w, h);
    }

    /**
     * @param g ����� ��� ���������
     */
    public void drawInPicture(Graphics g) {
        calculateAllPoints();

        Color pen = selfParams.color;
        g.setColor(pen);

        int left = xleft + generalParams.xLRightJoint;
        int top = ytop;// + generalParams.yIndTop;

        Graphics2D g2 = (Graphics2D) g;

        if (drawTex == NewLine.DrawTex.TexOn) {
            g2.setStroke(new BasicStroke(2));
            g2.drawLine(beginGDP.x + left, beginGDP.y + top, beginGDP.x - 5 + left, beginGDP.y - 5 + top);
            g2.drawLine(beginGDP.x + left, beginGDP.y + top, beginGDP.x + left, beginGDP.y - 3 + top);
        }
        if (drawTex == NewLine.DrawTex.TexOff) {
            g2.setStroke(new BasicStroke(2));
            g2.drawLine(endGDP.x + left, endGDP.y + top, endGDP.x + 5 + left, endGDP.y - 5 + top);
            g2.drawLine(endGDP.x + 5 + left, endGDP.y - 2 + top, endGDP.x + 5 + left, endGDP.y - 5 + top);
            g2.drawLine(endGDP.x + 2 + left, endGDP.y - 5 + top, endGDP.x + 5 + left, endGDP.y - 5 + top);
        }

        if (selfParams.lineStyle == LineStyle.Solid) {
            g2.setStroke(new BasicStroke(selfParams.lineWidth));
            g2.drawLine(beginGDP.x + left, beginGDP.y + top, endGDP.x + left, endGDP.y + top);
        } else {
            Color bg = g2.getBackground();
            LineStyle.drawLine(g2, beginGDP.x + left, beginGDP.y + top, endGDP.x + left, endGDP.y + top,
                    selfParams.lineStyle, selfParams.lineWidth, bg, pen);
        }
        if (caption == NewLine.DrawCaption.Center) {
            g2.setFont(generalParams.captionSp);
            AffineTransform orig = g2.getTransform();
            g2.rotate(alphaCaption, captionPointGDP.x + left, captionPointGDP.y + top);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP.x + left, captionPointGDP.y + top);
            g2.setTransform(orig);
        }
        if (caption == NewLine.DrawCaption.On) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP1.x + left, captionPointGDP1.y + top);
        }
        if (caption == NewLine.DrawCaption.Off) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP.x + left, captionPointGDP.y + top);
        }

        if (caption == NewLine.DrawCaption.texOn) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP1.x + left, captionPointGDP1.y + top);
            // g2.drawString("'/'", captionPoint.x, captionPoint.y);
        }
        if (caption == NewLine.DrawCaption.texOff) {
            g2.setFont(generalParams.captionSt);
            // g2.drawString(".\\.", polygon.xpoints[1], polygon.ypoints[1]);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP.x + left, captionPointGDP.y + top);
        }

        if (isTimeL) {
            g2.setFont(generalParams.time);
            g2.drawString(selfParams.timeL, pointTimeLGDP.x + left, pointTimeLGDP.y + top);
        }
        if (isTimeA) {
            g2.setFont(generalParams.time);
            g2.drawString(selfParams.timeA, pointTimeAGDP.x + left, pointTimeAGDP.y + top);
        }
    }

    public void setCaption(NewLine.DrawCaption caption) {
        this.caption = caption;
    }

    public void setTimeA(boolean timeA) {
        isTimeA = timeA;
    }

    public void setTimeL(boolean timeL) {
        isTimeL = timeL;
    }

    public void setDrawTex(NewLine.DrawTex drawTex) {
        this.drawTex = drawTex;
    }

    public Dimension getSize() {
        return size;
    }

    public void setSelfParams(NewLineSelfParams selfParams) {
        this.selfParams = selfParams;
    }

    public void setGeneralParams(TrainThreadGeneralParams generalParams) {
        this.generalParams = generalParams;
    }

}
