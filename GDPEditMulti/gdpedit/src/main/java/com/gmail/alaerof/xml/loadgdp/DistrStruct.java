package com.gmail.alaerof.xml.loadgdp;

import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import org.w3c.dom.Element;

/**
 * ����� ��� �������� ������ �� �������� ��������� �������
 */
public class DistrStruct {
    /*
    TfmGRNDataXML
    CheckAllDistrStruct(domDoc, allSt, allLst, allSp, allTwn, allLoc, lst, idDistr);
    CheckAllDistr(domDoc, allDistr, lst, 'DistrComb');
     */
    /** key = ID from xml, value = pair(element xml, element DB) */
    private Map<Integer, Pair<Element, Station>> allSt = new HashMap<>();
    /** key = ID from xml, value = pair(element xml, element DB) */
    private Map<Integer, Pair<Element, LineSt>> allLst = new HashMap<>();
    /** key = ID from xml, value = pair(element xml, element DB) */
    private Map<Integer, Pair<Element, Scale>> allSp = new HashMap<>();
    /** key = ID from xml, value = pair(element xml, element DB) */
    private Map<Integer, Pair<Element, SpanSt>> allTwn = new HashMap<>();
    /** key = ID from xml, value = pair(element xml, element DB) */
    private Map<Integer, Pair<Element, LocomotiveType>> allLoc = new HashMap<>();
    /** key = ID from xml, value = pair(element xml, element DB) */
    private Map<Integer, Pair<Element, Distr>> allDistrComb = new HashMap<>();
    /** key = ID from xml, value = pair(element xml, element DB) */
    private Map<Long, Pair<Element, DistrTrain>> allTrain = new HashMap<>();
    /** ������ ��������� �� ��������� ������� */
    private List<String> warningList = new ArrayList<>();

    public Map<Integer, Pair<Element, Station>> getAllSt() {
        return allSt;
    }

    public Map<Integer, Pair<Element, LineSt>> getAllLst() {
        return allLst;
    }

    public Map<Integer, Pair<Element, Scale>> getAllSp() {
        return allSp;
    }

    public Map<Integer, Pair<Element, SpanSt>> getAllTwn() {
        return allTwn;
    }

    public Map<Integer, Pair<Element, LocomotiveType>> getAllLoc() {
        return allLoc;
    }

    public Map<Integer, Pair<Element, Distr>> getAllDistrComb() {
        return allDistrComb;
    }

    public Map<Long, Pair<Element, DistrTrain>> getAllTrain() {
        return allTrain;
    }

    public List<String> getWarningList() {
        return warningList;
    }

    public Collection<DistrTrain> getDistrTrains() {
        List<DistrTrain> list = new ArrayList<>();
        for (Pair<Element, DistrTrain> pair : allTrain.values()) {
            list.add(pair.getValue());
        }
        return list;
    }
}
