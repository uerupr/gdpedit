package com.gmail.alaerof.history;

import com.gmail.alaerof.manager.LocaleManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.entity.train.TrainXMLHandler;

import java.util.ResourceBundle;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ActionTrainEditTime extends Action {
    private TrainThread train;
    private static ResourceBundle bundle;

    public ActionTrainEditTime(TrainThread train) {
        actionName = "EditTrain";
        this.train = train;
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.history", LocaleManager.getLocale());
    }

    @Override
    protected void buildElement(Document doc, Element action) {
        Element tr = TrainXMLHandler.saveToXML(train, doc);
        action.appendChild(tr);
    }

    @Override
    protected boolean executeAction(int typeAction, Element oldValue, Element newValue) {
        if (typeAction == sRestore && oldValue != null) {
            TrainXMLHandler.loadTimeFromXML(train, oldValue);
            actionState = sRestore;
            GDPEdit.gdpEdit.getGDPActionPanel().getActionListSt().tableRepaint();
            return true;
        }
        if (typeAction == sApply && newValue != null) {
            TrainXMLHandler.loadTimeFromXML(train, newValue);
            actionState = sApply;
            GDPEdit.gdpEdit.getGDPActionPanel().getActionListSt().tableRepaint();
            return true;
        }
        return false;
    }

    @Override
    public String getActionDescription() {
        return "" + train.getCode() + "(" + train.getNameTR() + ") " + bundle.getString("history.change");
    }
}
