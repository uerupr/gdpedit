package com.gmail.alaerof.components.mainframe.gdp.action.listst;

import com.gmail.alaerof.components.mainframe.gdp.action.listtrain.GDPActionListTrain;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.TrainStop;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.dialog.linesttrain.LineStTRDialog;
import com.gmail.alaerof.entity.train.TimeStation;
import com.gmail.alaerof.gdpcalculation.CalculateOverTime;
import com.gmail.alaerof.javafx.dialog.stationinfo.StationInfoFXDialog;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.FileUtil;
import com.gmail.alaerof.util.IconImageUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.gdpcalculation.CalculateGDPHandler;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.util.TableUtils;
import com.ibm.icu.util.Calendar;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Helen Yrofeeva
 */
public class GDPActionListSt extends JPanel {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(GDPActionListSt.class);
    private JLabel trainLabel;
    private List<DistrPoint> listDistrPoints;
    private TrainScheduleTable table;
    private ListStTableModel tableModel = new ListStTableModel();
    private JScrollPane tablePane;
    private JToggleButton modeArrive;
    private JToggleButton modeLeave;
    private JToggleButton modeBoth;
    private JCheckBox checkBoxTrain;
    private JTextField textFieldTrain;
    private boolean useStationInfo = false;
    private boolean useStopPoint = false;
    private static ResourceBundle bundle;

    public GDPActionListSt() {
        super();
        initPanel();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.listst", LocaleManager.getLocale());
    }

    public void initPanel() {
        // ������� ������
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createLineBorder(Color.LIGHT_GRAY)));

        Font tableFont = new Font(Font.SANS_SERIF, 0, 11);

        if (listDistrPoints != null) {
            tableModel.setListDistrPoint(listDistrPoints);
        }
        table = new TrainScheduleTable(tableModel);
        tableModel.setTable(table);
        table.setFont(tableFont);
        table.getTableHeader().setFont(tableFont);
        tablePane = new JScrollPane(table);
        table.setFillsViewportHeight(true);

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSize[] = { 90, 30, 30, 15, 20, 25, 25, 25 };
        TableUtils.setTableColumnSize(table, colSize);
        table.setRowHeight(24);
        table.setShowVerticalLines(true);
        // default table cell editor is JTable$GenericEditor not
        // DefaultCellEditor
        final JTextField textField = new JTextField();
        table.setDefaultEditor(TimeSt.class, new TimeStCellEditor(textField));

        // �������� ���� ������ �� ������� �� �������� ����� � ����������
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                showStationInfoFXDialog(e);
            }
        });

        this.setLayout(new BorderLayout());
        this.add(tablePane, BorderLayout.CENTER);

        JPanel top = new JPanel();
        JPanel topLeft = new JPanel();
        JPanel topRight = new JPanel();
        top.setLayout(new BorderLayout());
        top.add(topLeft, BorderLayout.WEST);
        top.add(topRight, BorderLayout.EAST);
        top.setPreferredSize(new Dimension(0, 35));
        this.add(top, BorderLayout.NORTH);
        trainLabel = new JLabel(bundle.getString("list.st.trainlabel"));
        // trainLabel.setBounds(5, 5, 60, 30);
        topLeft.add(trainLabel);

        final ButtonGroup groupMode = new ButtonGroup();
        modeLeave = new JToggleButton(bundle.getString("list.st.modeleave"));
        modeLeave.setToolTipText(bundle.getString("list.st.modeleave.tooltip"));
        topRight.add(modeLeave);
        // modeLeave.setBounds(65, 5, 60, 25);
        modeArrive = new JToggleButton(bundle.getString("list.st.modearrive"));
        modeArrive.setToolTipText(bundle.getString("list.st.modearrive.tooltip"));
        topRight.add(modeArrive);
        // modeArrive.setBounds(125, 5, 60, 25);
        modeBoth = new JToggleButton(bundle.getString("list.st.modeboth"));
        modeBoth.setToolTipText(bundle.getString("list.st.modeboth.tooltip"));
        topRight.add(modeBoth);
        // modeBoth.setBounds(185, 5, 60, 25);
        modeLeave.setSelected(true);
        groupMode.add(modeLeave);
        groupMode.add(modeArrive);
        groupMode.add(modeBoth);
        modeLeave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                tableModel.setTimeMode(TimeMode.Leave);
                tableModel.setData(false);
                table.repaint();
            }
        });
        modeArrive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tableModel.setTimeMode(TimeMode.Arrive);
                tableModel.setData(false);
                table.repaint();
            }
        });
        modeBoth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tableModel.setTimeMode(TimeMode.Both);
                tableModel.setData(false);
                table.repaint();
            }
        });

        JPanel bottom = new JPanel();
        this.add(bottom, BorderLayout.SOUTH);
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.PAGE_AXIS));
        bottom.setIgnoreRepaint(true);
        bottom.setMinimumSize(new Dimension(10, 10));

        {
            JPanel bottomApply = new JPanel();
            bottom.add(bottomApply);
            bottomApply.setLayout(null);
            bottomApply.setPreferredSize(new Dimension(0, 45));
            JButton applyTimeSt = new JButton(bundle.getString("list.st.apply.timest"));
            bottomApply.add(applyTimeSt);
            applyTimeSt.setBounds(5, 5, 100, 25);

            final JCheckBox stopPoint = new JCheckBox(bundle.getString("list.st.stoppoint"));
            bottomApply.add(stopPoint);
            stopPoint.setBounds(155, 5, 45, 25);
            stopPoint.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    useStopPoint = stopPoint.isSelected();
                    JCheckBox bb = (JCheckBox) e.getSource();
                    tableModel.setTown(bb.isSelected());
                    tableModel.setData(true);
                    tableModel.setData(false);
                    table.repaint();
                }
            });

            final JCheckBox stationInfo = new JCheckBox(bundle.getString("list.st.stationinfo"));
            bottomApply.add(stationInfo);
            stationInfo.setBounds(200, 5, 100, 25);
            stationInfo.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    useStationInfo = stationInfo.isSelected();
                }
            });

            final JPanel contentPane = this;
            applyTimeSt.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    contentPane.setCursor(GDPEdit.waitCursor);
                    try {
                        tableModel.applyData(stopPoint.isSelected());
                    } catch (Exception ex) {
                        logger.error(ex.toString(), ex);
                    } finally {
                        contentPane.setCursor(GDPEdit.defCursor);
                    }
                }
            });
            // ��
            JButton saveDB = new JButton(bundle.getString("list.st.save.bd"));
            bottomApply.add(saveDB);
            saveDB.setBounds(105, 5, 45, 25);
            saveDB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    saveScheduleToDB();
                }
            });
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);
            {
                // ���������
                JButton button = new JButton(bundle.getString("list.st.build.train"));
                // ��������� �� ����������
                button.setToolTipText(bundle.getString("list.st.build.train.tooltip"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        buildTrainBySpanTime();
                    }
                });
            }
            {
                // ���������
                JButton button = new JButton(bundle.getString("listst.calc.gdp.train"));
                // � ������ ���������
                button.setToolTipText(bundle.getString("listst.calc.gdp.train.tooltip"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        buildTrainInGDP();
                    }
                });
            }
            {
                // ��������� ���������� ���������� ������� � ���� (�������)
                JButton button = new JButton();
                panel.add(button);
                button.setIcon(IconImageUtil.getIcon(IconImageUtil.SAVE_FILE));
                button.setToolTipText(bundle.getString("listst.savetheschedule_table"));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        saveTrainToFileTabel();
                    }
                });
            }
            {
                // ��������� ���������� ���������� ������� � ���� (������)
                JButton button = new JButton();
                panel.add(button);
                button.setIcon(IconImageUtil.getIcon(IconImageUtil.SAVE_FILE));
                button.setToolTipText(bundle.getString("listst.savetheschedule_string"));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        saveTrainToFileLine();
                    }
                });
            }
            {
                checkBoxTrain = new JCheckBox();
                panel.add(checkBoxTrain);
                checkBoxTrain.setSelected(true);
                checkBoxTrain.setText(bundle.getString("listst.usetraintextfield"));
            }
            {
                textFieldTrain = new JTextField();
                panel.add(textFieldTrain);
                textFieldTrain.setColumns(4);
                textFieldTrain.setText(bundle.getString("listst.traintextfield"));
            }
            {
                // ������ �������
                JButton button = new JButton();
                panel.add(button);
                button.setIcon(IconImageUtil.getIcon(IconImageUtil.CALC));
                button.setToolTipText(bundle.getString("listst.calc.overtime"));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        calcOverTime();
                    }
                });
            }
            {
                // ���������
                JButton button = new JButton(bundle.getString("listst.verify"));
                // panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                    }
                });
            }
        }
    }

    private void showStationInfoFXDialog(MouseEvent e) {
        if (e.getClickCount() == 2) {
            int row = table.getSelectedRow();
            int col = table.getSelectedColumn();
            if (col == 0) {
                DistrPoint dp = tableModel.getDistrPoint(row);
                if (dp.getTypePoint() == DistrPoint.TypePoint.SeparatePoint) {
                    if (useStationInfo) {
                        DistrStation distrStation = (DistrStation) dp;
                        StationInfoFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getStationInfoFXDialog();
                        dialog.setModal(true);
                        dialog.setDialogContent(distrStation);
                        dialog.setVisible(true);
                    } else {
                        int idSt = dp.getIdPoint();
                        GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
                        editPanel.loadForLineStTRDialog(idSt);
                        LineStTRDialog lineStTRDialog = GDPEdit.gdpEdit.getDialogManager().getLineStTRDialog(
                                dp.getNamePoint()
                        );
                        lineStTRDialog.setDistrStation((DistrStation) dp);
                        lineStTRDialog.clearLabel();
                        lineStTRDialog.setVisible(true);
                    }
                }
            }
        }
    }

    private void buildTrainBySpanTime() {
        try {
            TrainThread train = tableModel.getTrain();
            if (train != null) {
                List<LocomotiveType> locs = train.getDistrEntity().getListDistrLoc();
                List<RouteD> listR = train.getDistrEntity().getListRoute();
                CalcTrainDialog cd = GDPEdit.gdpEdit.getDialogManager().getCalcTrainDialog();
                cd.setListLoc(locs);
                cd.setListRoute(listR);
                cd.setVisible(true);
                if (cd.getResult() == 1) {
                    LocomotiveType loc = cd.getSelectedLoc();
                    RouteD route = cd.getSelectedRoute();
                    boolean town = cd.isTown();
                    CalculateGDPHandler.buildBySpanTime(train, town, loc, route);
                }
            }
        } catch (Exception er) {
            logger.error(er.toString(), er);
        }
    }

    private void buildTrainInGDP() {
        try {
            TrainThread train = tableModel.getTrain();
            if (train != null) {
                ICategoryDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO();
                List<Category> categories = dao.getCategories();
                Collection<DistrTrain> calc = new ArrayList<>();
                calc.add(train.getDistrTrain());
                Collection<DistrTrain> fixed = buildFixedList(train);
                List<String> errorList = new ArrayList<>();
                CalculateGDPHandler.buildConcurrentGDP(train.getDistrEntity(), fixed, calc, categories, errorList);
                if (errorList.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    for (String s : errorList) {
                        sb.append(s).append("\n");
                    }
                    JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                            sb.toString()+ "\n" + bundle.getString("list.st.calculation.error"),
                            bundle.getString("list.st.warning"), JOptionPane.OK_OPTION);
                }
                train.refreshLines();
            }
        } catch (Exception er) {
            logger.error(er.toString(), er);
        }
    }

    private Collection<DistrTrain> buildFixedList(TrainThread train) {
        List<DistrTrain> fixed = new ArrayList<>();
        for (TrainThread trainThread :
                GDPEdit.gdpEdit.getCurrentGDPEditPanel().getDistrEditEntity().getListTrains()) {
            if (trainThread != train) {
                fixed.add(trainThread.getDistrTrain());
            }
        }
        return fixed;
    }

    private void saveScheduleToDB() {
        String message = null;
        if (tableModel.getTrain() != null) {
            int n = JOptionPane.showConfirmDialog(
                    GDPEdit.gdpEdit.getMainFrame(), bundle.getString("list.st.save.train.db"),
                    bundle.getString("list.st.warning"), JOptionPane.YES_NO_OPTION);
            boolean save = n == 0;
            if (save) {
                long dt1 = Calendar.getInstance().getTimeInMillis();
                GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.waitCursor);
                try {
                    tableModel.getTrain().saveGDPtoDB(false);
                } finally {
                    GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.defCursor);
                    long dt2 = Calendar.getInstance().getTimeInMillis();
                    dt2 = (dt2 - dt1) / 1000;
                    message = bundle.getString("list.st.save.result") + " " + dt2;
                }
            }
        } else {
            message = bundle.getString("list.st.select.train.warn");
        }
        JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), message);
    }

    public void setListDistrPoints(List<DistrPoint> listDistrPoints) {
        this.listDistrPoints = listDistrPoints;
        tableModel.setListDistrPoint(this.listDistrPoints);
        table.repaint();
    }

    public void setCurrentTrainThread(TrainThread trainThread) {
        trainLabel.setText(trainThread.getCode() + " (" + trainThread.getNameTR() + ")");
        tableModel.setTrain(trainThread);
        table.repaint();
    }

    public void tableRepaint() {
        tableModel.setData(false);
        table.repaint();
    }

    private void saveTrainToFileTabel() {
        GDPActionListTrain panelTr = GDPEdit.gdpEdit.getGDPActionPanel().getActionListTrain();
        List<TrainThread> listTrain = new ArrayList<>(panelTr.getSelectedTrains());
        if (listTrain.size() > 0) {
            String fileName;
            if (listTrain.size() == 1){
                fileName = listTrain.get(0).getCode() + " (" + listTrain.get(0).getNameTR() + ")_tabel.txt";
            } else {
                fileName = "TrainsSchedule_tabel.txt";
            }
            File file = FileUtil.chooseFile(FileUtil.Mode.save,null, fileName, GDPEdit.gdpEdit.getMainFrame(),
                    new FileNameExtensionFilter("TXT files", "txt"));
            try(FileWriter writer = new FileWriter(file, false))
            {
                for (int j = 0; j < listTrain.size(); j++) {
                    TrainThread train = listTrain.get(j);
                    if (train != null) {
                        //String trainName = bundle.getString("listst.trainnumber") + train.getCode() + " (" + train.getNameTR() + ")";
                        String trainName = bundle.getString("listst.trainnumberlong") + train.getCode();
                        writer.write(trainName + "\r\n");
                        StringBuilder textHead = new StringBuilder("");
                        textHead.append(bundle.getString("listst.station")).append("\t");
                        textHead.append(bundle.getString("list.st.modearrive")).append("\t");
                        textHead.append(bundle.getString("list.st.modeleave")).append("\t");
                        textHead.append(bundle.getString("listst.pt")).append("\t");
                        textHead.append(bundle.getString("listst.min"));
                        writer.write(textHead + "\r\n");
                        if (useStopPoint) {
                            for (int i = 0; i < listDistrPoints.size(); i++){
                                StringBuilder text = new StringBuilder("");
                                DistrPoint distrPoint = listDistrPoints.get(i);
                                if (distrPoint.getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)) {
                                    LineStTrain lst = train.getDistrTrain().getMapLineStTr().get(distrPoint.getIdPoint());
                                    if (lst != null) {
                                        text.append(distrPoint.getNamePoint()).append("\t");
                                        String tOn = lst.getOccupyOn();
                                        String tOff = lst.getOccupyOff();
                                        if (i != listDistrPoints.size() - 1) {
                                            if (tOn.equals(tOff)) {
                                                text.append("\t").append(tOff).append("\t");
                                            } else {
                                                text.append(tOn).append("\t");
                                                text.append(tOff).append("\t");
                                            }
                                        }
                                        else {
                                            text.append(tOn).append("\t").append("\t");
                                        }
                                        text.append(lst.getSk_ps()).append("\t");
                                        text.append(String.valueOf(lst.getTstop())).append("\t");
                                        writer.write(text + "\r\n");
                                    }
                                }
                                if (distrPoint.getTypePoint().equals(DistrPoint.TypePoint.StopPoint)) {
                                    TrainStop trStop = train.getDistrTrain().getMapTrainStop().get(distrPoint.getIdPoint());
                                    if (trStop != null){
                                        text.append(bundle.getString("listst.op")).append(distrPoint.getNamePoint()).append("\t");
                                        String tOn = trStop.getOccupyOn();
                                        String tOff = trStop.getOccupyOff();
                                        if (i != listDistrPoints.size() - 1) {
                                            if (tOn.equals(tOff)) {
                                                text.append("\t").append(tOff).append("\t");
                                            } else {
                                                text.append(tOn).append("\t");
                                                text.append(tOff).append("\t");
                                            }
                                        }
                                        else {
                                            text.append(tOn).append("\t").append("\t");
                                        }
                                        text.append(trStop.getSk_ps()).append("\t");
                                        text.append(String.valueOf(trStop.getTstop())).append("\t");
                                        writer.write(text + "\r\n");
                                    }
                                }
                            }
                        }
                        else {
                            List<TimeStation> timeStationList = train.getListTimeSt();
                            for (int i = 0; i < timeStationList.size(); i++){
                                StringBuilder text = new StringBuilder("");
                                text.append(timeStationList.get(i).getDistrStation().getStation().getName()).append("\t");
                                if (i != timeStationList.size() - 1) {
                                    if (timeStationList.get(i).getLineStTrain().getOccupyOn().equals(timeStationList.get(i).getLineStTrain().getOccupyOff())) {
                                        text.append("\t").append(timeStationList.get(i).getLineStTrain().getOccupyOff()).append("\t");
                                    } else {
                                        text.append(timeStationList.get(i).getLineStTrain().getOccupyOn()).append("\t");
                                        text.append(timeStationList.get(i).getLineStTrain().getOccupyOff()).append("\t");
                                    }
                                }
                                else {
                                    text.append(timeStationList.get(i).getLineStTrain().getOccupyOn()).append("\t").append("\t");
                                }
                                text.append(timeStationList.get(i).getLineStTrain().getSk_ps()).append("\t");
                                text.append(String.valueOf(timeStationList.get(i).getLineStTrain().getTstop())).append("\t");
                                writer.write(text + "\r\n");
                            }
                        }
                        writer.write("\r\n");
                    }
                }
                writer.flush();
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), bundle.getString("listst.ok"));
            } catch (Exception er) {
                logger.error(er.toString(), er);
            }
        }
    }

    private void saveTrainToFileLine() {
        GDPActionListTrain panelTr = GDPEdit.gdpEdit.getGDPActionPanel().getActionListTrain();
        List<TrainThread> listTrain = new ArrayList<>(panelTr.getSelectedTrains());
        if (listTrain.size() > 0) {
            String fileName;
            if (listTrain.size() == 1){
                fileName = listTrain.get(0).getCode() + " (" + listTrain.get(0).getNameTR() + ")_line.txt";
            } else {
                fileName = "TrainsSchedule_line.txt";
            }
            File file = FileUtil.chooseFile(FileUtil.Mode.save,null, fileName, GDPEdit.gdpEdit.getMainFrame(),
                    new FileNameExtensionFilter("TXT files", "txt"));
            try(FileWriter writer = new FileWriter(file, false))
            {
                for (TrainThread train : listTrain) {
                    if (train != null) {
                        //StringBuilder text = new StringBuilder(bundle.getString("listst.trainnumber") + train.getCode() + " (" + train.getNameTR() + "), ");
                        StringBuilder text = new StringBuilder();
                        if (checkBoxTrain.isSelected()) {
                            text.append(textFieldTrain.getText()).append(" ").append(bundle.getString("listst.trainnumber")).append(train.getCode()).append(" ");
                        } else {
                            text.append(bundle.getString("listst.trainnumber")).append(train.getCode()).append(" ");
                        }
                        if (useStopPoint) {
                            for (int i = 0; i < listDistrPoints.size() - 1; i++) {
                                DistrPoint distrPoint = listDistrPoints.get(i);
                                if (distrPoint.getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)) {
                                    LineStTrain lst = train.getDistrTrain().getMapLineStTr().get(distrPoint.getIdPoint());
                                    if (lst != null) {
                                        text.append(distrPoint.getNamePoint()).append(" ");
                                        String tOn = lst.getOccupyOn();
                                        String tOff = lst.getOccupyOff();
                                        if (tOn.equals(tOff)) {
                                            text.append(tOff).append(", ");
                                        } else {
                                            text.append(tOn).append("-").append(tOff);
                                            if (!lst.getSk_ps().equals("")) {
                                                //text.append(" (").append(lst.getSk_ps()).append("), ");
                                                text.append(" (").append(bundle.getString("listst.texst")).append("), ");
                                            } else {
                                                text.append(", ");
                                            }
                                        }
                                    }
                                }
                                if (distrPoint.getTypePoint().equals(DistrPoint.TypePoint.StopPoint)) {
                                    TrainStop trStop = train.getDistrTrain().getMapTrainStop().get(distrPoint.getIdPoint());
                                    if (trStop != null) {
                                        text.append(bundle.getString("listst.op")).append(distrPoint.getNamePoint()).append(" ");
                                        String tOn = trStop.getOccupyOn();
                                        String tOff = trStop.getOccupyOff();
                                        if (tOn.equals(tOff)) {
                                            text.append(tOff).append(", ");
                                        } else {
                                            text.append(tOn).append("-").append(tOff);
                                            if (!trStop.getSk_ps().equals("")) {
                                                //text.append(" (").append(trStop.getSk_ps()).append("), ");
                                                text.append(" (").append(bundle.getString("listst.texst")).append("), ");
                                            } else {
                                                text.append(", ");
                                            }
                                        }
                                    }
                                }
                            }
                            DistrPoint lastDistrPoint = listDistrPoints.get(listDistrPoints.size() - 1);
                            if (lastDistrPoint.getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)) {
                                LineStTrain lastlst = train.getDistrTrain().getMapLineStTr().get(lastDistrPoint.getIdPoint());
                                if (lastlst != null) {
                                    text.append(lastDistrPoint.getNamePoint()).append(" ");
                                    text.append(lastlst.getOccupyOn());
                                } else {
                                    text = new StringBuilder(text.substring(0, text.length() - 2));
                                }
                            }
                            if (lastDistrPoint.getTypePoint().equals(DistrPoint.TypePoint.StopPoint)) {
                                TrainStop lasttrStop = train.getDistrTrain().getMapTrainStop().get(lastDistrPoint.getIdPoint());
                                if (lasttrStop != null) {
                                    text.append(lastDistrPoint.getNamePoint()).append(" ");
                                    text.append(lasttrStop.getOccupyOn());
                                } else {
                                    text = new StringBuilder(text.substring(0, text.length() - 2));
                                }
                            }
                        } else {
                            List<TimeStation> timeStationList = train.getListTimeSt();
                            for (int i = 0; i < timeStationList.size() - 1; i++) {
                                text.append(timeStationList.get(i).getDistrStation().getStation().getName()).append(" ");
                                if (timeStationList.get(i).getLineStTrain().getOccupyOn().equals(timeStationList.get(i).getLineStTrain().getOccupyOff())) {
                                    text.append(timeStationList.get(i).getLineStTrain().getOccupyOff()).append(", ");
                                } else {
                                    text.append(timeStationList.get(i).getLineStTrain().getOccupyOn()).append("-").append(timeStationList.get(i).getLineStTrain().getOccupyOff());
                                    if (!timeStationList.get(i).getLineStTrain().getSk_ps().equals("")) {
                                        //text.append(" (").append(timeStationList.get(i).getLineStTrain().getSk_ps()).append("), ");
                                        text.append(" (").append(bundle.getString("listst.texst")).append("), ");
                                    } else {
                                        text.append(", ");
                                    }
                                }
                            }
                            text.append(timeStationList.get(timeStationList.size() - 1).getDistrStation().getStation().getName()).append(" ");
                            text.append(timeStationList.get(timeStationList.size() - 1).getLineStTrain().getOccupyOn());
                        }
                        writer.write(text.toString() + "\n");
                    }
                }
                writer.flush();
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), bundle.getString("listst.ok"));
            } catch (Exception er) {
                logger.error(er.toString(), er);
            }
        }
    }
    private void calcOverTime() {
        TrainThread train = tableModel.getTrain();
        if (train != null) {
            String report = CalculateOverTime.calculateOverTimeOneTrain(train);
            if (!report.isEmpty()) {
                String mess = bundle.getString("listst.calculationnotes");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            } else {
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), bundle.getString("listst.ok"));
                tableModel.setTrain(train);
                table.repaint();
            }
        } else {
            String mess = bundle.getString("listst.selecttrain");
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
        }

    }
}