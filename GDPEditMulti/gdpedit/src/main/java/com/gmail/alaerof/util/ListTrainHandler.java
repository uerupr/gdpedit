package com.gmail.alaerof.util;

import java.util.List;

import com.gmail.alaerof.entity.train.TrainThread;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ListTrainHandler {
    public static TrainThread searchTrain(String codeTR, List<TrainThread> listTR) {
        TrainThread tr = null;
        if (listTR != null) {
            for (int i = 0; i < listTR.size(); i++) {
                TrainThread train = listTR.get(i);
                if (train.getCode().equals(codeTR)) {
                    tr = train;
                    break;
                }
            }
        }
        return tr;
    }
}
