package com.gmail.alaerof.components.menu;

import com.gmail.alaerof.manager.LocaleManager;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JMenu;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public abstract class LoggerMenu extends JMenu {
    protected Logger logger = LogManager.getLogger(this.getClass());
    protected ResourceBundle bundle = getBundle();

    public LoggerMenu() {
    }

    public LoggerMenu(String s) {
        super(s);
    }

    /**
     * ����� �������� ���� ��������� �� ������ ����� �� �������� �����
     * @return {@link ResourceBundle}
     */
    protected ResourceBundle getBundle() {
        return ResourceBundle.getBundle("bundles.Menu", resolveLocale());
    }

    /**
     * ���������� ������� ������
     * @return {@link Locale}
     */
    private Locale resolveLocale() {
        return LocaleManager.getLocale();
    }
}
