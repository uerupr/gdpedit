package com.gmail.alaerof.javafx.dialog.trainpropertys.view;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrTrainDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.TaskWithCursorExecutor;
import com.gmail.alaerof.javafx.dialog.trainpropertys.TrainModelComparator;
import com.gmail.alaerof.javafx.dialog.trainpropertys.model.TrainModel;
import com.gmail.alaerof.util.IconImageUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import java.util.ResourceBundle;


import static com.gmail.alaerof.util.CategoryUtil.defineCategory;

public class AllTrainListLayoutController implements Initializable {
    protected Logger logger = LogManager.getLogger(AllTrainListLayoutController.class);
    protected ResourceBundle bundle;
    private Scene scene;

    /** ������� ������� */
    private TableView<TrainModel> trainModelTable;
    /** ���� ������� */
    private List<TrainModel> trainModelList = new ArrayList<>();

    @FXML
    private Button findTrainButton;
    @FXML
    private TextField codeTrainTextField;
    @FXML
    private BorderPane rootCenterPane;
    @FXML
    private CheckBox cbPriority;

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        Image image = IconImageUtil.getFXImage(IconImageUtil.SEARCH);
        ImageView imageView = new ImageView(image);
        findTrainButton.setGraphic(imageView);
        rootCenterPane.autosize();
        // train table
        EditableTableViewCreator<TrainModel> builderImp = new EditableTableViewCreator<>();
        trainModelTable = builderImp.createEditableTableView();
        trainModelTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
        rootCenterPane.setCenter(trainModelTable);
        rootCenterPane.getCenter().autosize();
        String title;
        title = bundle.getString("javafx.dialog.trainpropertys.code");
        trainModelTable.getColumns().add(builderImp.createStringColumn(title, TrainModel::codeTrainProperty, false));
        title = bundle.getString("javafx.dialog.trainpropertys.name");
        trainModelTable.getColumns().add(builderImp.createStringColumn(title, TrainModel::nameTrainProperty, false));
        title = bundle.getString("javafx.dialog.trainpropertys.distrname");
        trainModelTable.getColumns().add(builderImp.createStringColumn(title, TrainModel::nameDistrProperty, false));
        title = bundle.getString("javafx.dialog.trainpropertys.priority");
        trainModelTable.getColumns().add(builderImp.createIntegerColumn(title, TrainModel::priorityTrainProperty, true));
        title = bundle.getString("javafx.dialog.trainpropertys.typetr");
        trainModelTable.getColumns().add(builderImp.createStringColumn(title, TrainModel::typeTrainProperty, false));
        title = bundle.getString("javafx.dialog.trainpropertys.categoryname");
        trainModelTable.getColumns().add(builderImp.createStringColumn(title, TrainModel::categoryNameProperty, false));
        title = bundle.getString("javafx.dialog.trainpropertys.categorypriority");
        trainModelTable.getColumns().add(builderImp.createIntegerColumn(title, TrainModel::categoryPriorityProperty, false));
        trainModelTable.getColumns().get(0).setPrefWidth(70);
        trainModelTable.getColumns().get(1).setPrefWidth(70);
        trainModelTable.getColumns().get(2).setPrefWidth(190);
        trainModelTable.getColumns().get(3).setPrefWidth(50);
        trainModelTable.getColumns().get(4).setPrefWidth(128);
        trainModelTable.getColumns().get(5).setPrefWidth(150);
        trainModelTable.getColumns().get(6).setPrefWidth(50);
        for (int i = 0; i < trainModelTable.getColumns().size(); i++){
            trainModelTable.getColumns().get(i).setSortable(false);
        }
    }

    @FXML
    private void findTrain (ActionEvent event) {
        logger.debug("findTrain");
        String codeTrain = codeTrainTextField.getText();
        int row = 0;
        if (codeTrain != null){
            for (int i = trainModelList.size()-1; i > 0; i--) {
                String ss = trainModelList.get(i).codeTrainProperty().get();
                if (ss.equals(codeTrain)) {
                    row = i;
                }
            }
        }
        // ���� ���������� �������� ������� ������
        //TableView.TableViewSelectionModel selectionModel = trainModelTable.getSelectionModel();
        //selectionModel.clearSelection();
        //selectionModel.select(row);
        //trainModelTable.setSelectionModel(selectionModel);
        trainModelTable.scrollTo(row);
    }

    @FXML
    private void delEmptyTrains (ActionEvent event) throws DataManageException {
        logger.debug("delEmptyTrains");
        List<Long> trainIDList = new ArrayList<>();
        for (TrainModel aTrainModelList : trainModelList) {
            if (aTrainModelList.getIDDistr() == 0) {
                trainIDList.add(aTrainModelList.getIDTrain());
            }
        }
        String titleMain = bundle.getString("javafx.dialog.trainpropertys.dialog.attention");;
        String titleMiddle;
        StringBuilder titleSmall;
        if (existChange()) {
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nosavechange1");
            titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nosavechange2"));
            showInformation(titleMain, titleMiddle, titleSmall);
        } else {
            if (trainIDList.size() > 0) {
                deleteTrainsWithoutDistr(titleMain, "1", trainIDList);
            } else {
                titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nodel1");
                titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nodel2"));
                showInformation(titleMain, titleMiddle, titleSmall);
            }
        }
    }

    private boolean existChange(){
        boolean change = false;
        for (TrainModel aTrainModelList : trainModelList) {
            if (aTrainModelList.priorityTrainProperty().get() != aTrainModelList.getOldPriorityTrain()) {
                change = true;
            }
        }
        return change;
    }

    private void deleteTrainsWithoutDistr(String titleMain, String variant, List<Long> trainIDList){
        String titleMiddle;
        StringBuilder titleSmall;
        if (variant.equals("1")){
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.del") + "?";
            titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.delcount") + " " + String.valueOf(trainIDList.size()));
        } else {
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.del2") + "?";
            titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.traincode") + " " + variant);
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        alert.setContentText(titleSmall.toString());
        if (alert.showAndWait().get() == ButtonType.OK) {
            scene.setCursor(Cursor.WAIT);
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.delinfo");
            titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.good"));
            try {
                IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
                dao.deleteTrains(trainIDList);
            } catch (Exception e) {
                logger.error(e.toString(), e);
                titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.bad"));
            } finally {
                scene.setCursor(Cursor.DEFAULT);
                setContent();
                showInformation(titleMain, titleMiddle, titleSmall);
            }
        }
    }

    @FXML
    private void delTrain (ActionEvent event) {
        logger.debug("delTrain");
        List<Long> trainIDList = new ArrayList<>();
        String titleMain = bundle.getString("javafx.dialog.trainpropertys.dialog.attention");
        String titleMiddle;
        StringBuilder titleSmall;
        if (existChange()) {
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nosavechange1");
            titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nosavechange2"));
            showInformation(titleMain, titleMiddle, titleSmall);
        } else {
            int rowImport = trainModelTable.getSelectionModel().getSelectedIndex();
            if (rowImport >= 0) {
                trainIDList.add(trainModelList.get(rowImport).getIDTrain());
                String trainCode = trainModelList.get(rowImport).codeTrainProperty().get() + " "
                        + bundle.getString("javafx.dialog.trainpropertys.dialog.trainname") + " "
                        + trainModelList.get(rowImport).nameTrainProperty().get();
                deleteTrainsWithoutDistr(titleMain, trainCode, trainIDList);
            } else {
                titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nodel3");
                titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nodel4"));
                showInformation(titleMain, titleMiddle, titleSmall);
            }
        }
    }

    private DistrTrain justapositionCategory(List<Category> categoryList, TrainModel trainModel) {
        DistrTrain distrTrain = null;
        long idTrain = trainModel.getIDTrain();
        int codeTrain = Integer.parseInt(trainModel.codeTrainProperty().get());
        int idCat = trainModel.getIDCat();
        Category category = defineCategory(categoryList, codeTrain);
        if (category == null) {
            distrTrain = new DistrTrain();
            distrTrain.setIdTrain(idTrain);
            distrTrain.setIdCat(0);
            distrTrain.setPriority(trainModel.priorityTrainProperty().get());
            // idDistr = 0 ������ ���������� ��������� � �� �������� ���������
            distrTrain.setIdDistr(0);
        }
        if (category != null && category.getIdCat() != idCat) {
            distrTrain = new DistrTrain();
            distrTrain.setIdTrain(idTrain);
            distrTrain.setIdCat(category.getIdCat());
            if (cbPriority.isSelected()){
                distrTrain.setPriority(category.getPriority());
            }else {
                distrTrain.setPriority(trainModel.priorityTrainProperty().get());
            }
            // idDistr = 1 ������ ���������� ��������� � ����� ���������
            distrTrain.setIdDistr(1);
        }
        if (category != null && category.getIdCat() == idCat && cbPriority.isSelected() && category.getPriority() != trainModel.priorityTrainProperty().get()) {
            distrTrain = new DistrTrain();
            distrTrain.setIdTrain(idTrain);
            distrTrain.setIdCat(category.getIdCat());
            distrTrain.setPriority(category.getPriority());
            // idDistr = 2 ������ ���������� ��������� � ������������� �������� ���������
            distrTrain.setIdDistr(2);
        }
        return distrTrain;
    }

    private void showInformation(String titleMain, String titleMiddle, StringBuilder titleSmall) {
        Alert alert;
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        alert.setContentText(titleSmall.toString());
        alert.showAndWait();
    }

    private void categoryToBase(String titleMain, String titleMiddle, StringBuilder titleSmall, List<DistrTrain> trainList) {
        Alert alert;
        alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        alert.setContentText(titleSmall.toString());
        if (alert.showAndWait().get() == ButtonType.OK){
            scene.setCursor(Cursor.WAIT);
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.save");
            titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.good"));
            try {
                IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
                dao.updateTrainCategories(trainList);
            } catch (Exception e) {
                logger.error(e.toString(), e);
                titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.bad"));
            } finally {
                scene.setCursor(Cursor.DEFAULT);
                setContent();
                showInformation(titleMain, titleMiddle, titleSmall);
            }
        }
    }

    @FXML
    private void defineCat (ActionEvent event) throws DataManageException {
        logger.debug("defineCategory");
        String titleMain = bundle.getString("javafx.dialog.trainpropertys.dialog.attention");;
        String titleMiddle;
        StringBuilder titleSmall;
        int rowImport = trainModelTable.getSelectionModel().getSelectedIndex();
        if (rowImport >= 0) {
            if (existChange()) {
                titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nosavechange1");
                titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nosavechange2"));
                showInformation(titleMain, titleMiddle, titleSmall);
            } else {
                List<DistrTrain> trainList = new ArrayList<>();
                List<Category> categoryList = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO().getCategories();
                TrainModel trainModel = trainModelList.get(rowImport);
                DistrTrain distrTrain = justapositionCategory(categoryList, trainModel);
                if (distrTrain != null) {
                    trainList.add(distrTrain);
                }
                if (trainList.size() > 0) {
                    titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.alonenewprioritet");
                    titleSmall = new StringBuilder("");
                    if (distrTrain.getIdDistr() == 1) {
                        titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.alonenewcategory");
                    }
                    if (distrTrain.getIdDistr() == 0) {
                        titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.alonenocategory");
                    }
                    categoryToBase(titleMain, titleMiddle, titleSmall, trainList);
                } else {
                    titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nosave4"));
                    titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nosave5");
                    showInformation(titleMain, titleMiddle, titleSmall);
                }
            }
        } else {
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nodel3");
            titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nochange"));;
            showInformation(titleMain, titleMiddle, titleSmall);
        }
    }

    @FXML
    private void defineAllCat (ActionEvent event) throws DataManageException {
        logger.debug("defineAllCategories");
        String titleMain = bundle.getString("javafx.dialog.trainpropertys.dialog.attention");;
        String titleMiddle;
        StringBuilder titleSmall;
        if (existChange()) {
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nosavechange1");
            titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nosavechange2"));
            showInformation(titleMain, titleMiddle, titleSmall);
        } else {
            List<DistrTrain> trainList = new ArrayList<>();
            List<Category> categoryList = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO().getCategories();
            int badCategoryCount = 0;
            int noCategoryCount = 0;
            List<String> traisCodeName = new ArrayList<>();
            for (TrainModel aTrainModelList : trainModelList) {

                DistrTrain distrTrain = justapositionCategory(categoryList, aTrainModelList);
                if (distrTrain != null) {
                    if (distrTrain.getIdDistr() == 0) {
                        noCategoryCount = noCategoryCount + 1;
                        traisCodeName.add(aTrainModelList.codeTrainProperty().get() + " (" + aTrainModelList.nameTrainProperty().get() + ")");
                    }
                    if (distrTrain.getIdDistr() == 1) {
                        badCategoryCount = badCategoryCount + 1;
                    }
                    trainList.add(distrTrain);
                }
            }
            if (trainList.size() > 0) {
                if (badCategoryCount > 0) {
                    titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.newcategory") + " " + String.valueOf(badCategoryCount);
                } else {
                    titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.newcategoryempty");
                }
                if (noCategoryCount > 0) {
                    titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nocategory") + " (" + String.valueOf(noCategoryCount) + "): ");
                    for (int i = 0; i < traisCodeName.size(); i++){
                        titleSmall.append(traisCodeName.get(i));
                        if (i < traisCodeName.size() - 1){
                            titleSmall.append("; ");
                        }
                    }
                } else {
                    titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nocategoryempty"));
                }
                categoryToBase(titleMain, titleMiddle, titleSmall, trainList);
            } else {
                titleSmall = new StringBuilder(bundle.getString("javafx.dialog.trainpropertys.dialog.nosave4"));
                titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nosave3");
                showInformation(titleMain, titleMiddle, titleSmall);
            }
        }
    }

    @FXML
    private void saveToDB (ActionEvent event) {
        logger.debug("saveToDB");
        List<DistrTrain> trainList = new ArrayList<>();
        for (int i = 0; i < trainModelList.size(); i++) {
            if (trainModelList.get(i).priorityTrainProperty().get() != trainModelList.get(i).getOldPriorityTrain()) {
                DistrTrain distrTrain = new DistrTrain();
                distrTrain.setIdTrain(trainModelList.get(i).getIDTrain());
                distrTrain.setPriority(trainModelList.get(i).priorityTrainProperty().get());
                trainList.add(distrTrain);
            }
        }
        String titleMain = bundle.getString("javafx.dialog.trainpropertys.dialog.attention");;
        String titleMiddle;
        String titleSmall;
        Alert alert;
        if (trainList.size() > 0) {
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.changesave") + "?";
            titleSmall = bundle.getString("javafx.dialog.trainpropertys.dialog.changecount") + " " + String.valueOf(trainList.size());
            alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            if (alert.showAndWait().get() == ButtonType.OK){
                scene.setCursor(Cursor.WAIT);
                titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.save");
                titleSmall = bundle.getString("javafx.dialog.trainpropertys.dialog.good");
                try {
                    IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
                    dao.updateTrainPriorities(trainList);
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                    titleSmall = bundle.getString("javafx.dialog.trainpropertys.dialog.bad");
                } finally {
                    scene.setCursor(Cursor.DEFAULT);
                    setContent();
                    //trainModelTable.scrollTo(0);
                    showInformation(titleMain, titleMiddle, new StringBuilder(titleSmall));
                }
            }
        } else {
            titleMiddle = bundle.getString("javafx.dialog.trainpropertys.dialog.nosave1");
            titleSmall = bundle.getString("javafx.dialog.trainpropertys.dialog.nosave2");
            showInformation(titleMain, titleMiddle, new StringBuilder(titleSmall));
        }
    }

    public void setContent() {
        logger.debug("setContent");
        trainModelTable.getItems().clear();
        Task task = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    trainModelList = buildTrainModelList();
                    trainModelTable.setItems(FXCollections.observableArrayList(trainModelList));
                } catch (DataManageException | ObjectNotFoundException e) {
                    logger.error(e.toString(), e);
                    throw new RuntimeException(e);
                }
                return null;
            }
        };
        TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, () -> {
            logger.debug("setContent - end");
        });
    }

    private List<TrainModel> buildTrainModelList() throws DataManageException, ObjectNotFoundException {
        List<Category> categoryList = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO().getCategories();
        List<TrainModel> itemList = new ArrayList<>();
        IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
        List<DistrTrain> distrTrainList = dao.geTrains();
        for (DistrTrain aDistrTrain : distrTrainList) {
            IDistrDAO dao2 = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO();
            List<Distr> distrList = dao2.getDistrList(0);
            String nameDistr = null;
            for (Distr aDistr : distrList) {
                if (aDistr.getIdDistr() == aDistrTrain.getIdDistr()){
                    nameDistr = aDistr.getName();
                }
            }
            Category category = new Category();
            for (Category aCategoryList : categoryList) {
                if (aCategoryList.getIdCat() == aDistrTrain.getIdCat()) {
                    category = aCategoryList;
                }
            }
            TrainModel trainModel = new TrainModel(aDistrTrain, nameDistr, category);
            itemList.add(trainModel);
        }
        Collections.sort(itemList, new TrainModelComparator());
        return itemList;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
