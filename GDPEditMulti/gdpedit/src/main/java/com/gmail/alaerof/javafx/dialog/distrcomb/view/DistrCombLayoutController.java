package com.gmail.alaerof.javafx.dialog.distrcomb.view;

import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDistrCombDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.distrcomb.DistrCombModelComparator;
import com.gmail.alaerof.javafx.dialog.distrcomb.model.DistrCombModel;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import java.util.ResourceBundle;

public class DistrCombLayoutController implements Initializable {
    protected Logger logger = LogManager.getLogger(DistrCombLayoutController.class);
    protected ResourceBundle bundle;
    private DistrEditEntity distrEditEntity;
    private Scene scene;

    /** ������� ������������ �������� */
    private TableView<DistrCombModel> distrCombTable;
    /** ���� ������������ �������� */
    private List<DistrCombModel> distrCombList = new ArrayList<>();
    /** ���� ��� ��������, ������� ����� ���������� (��� �������, �� ������� ��������� � ��� ������������) */
    private List<DistrCombModel> distrList = new ArrayList<>();

    @FXML
    private BorderPane rootPane;
    @FXML
    private ComboBox<DistrCombModel> distrComboBox;

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        rootPane.autosize();
        // distrcomb table
        EditableTableViewCreator<DistrCombModel> builderImp = new EditableTableViewCreator<>();
        distrCombTable = builderImp.createEditableTableView();
        distrCombTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
        rootPane.setCenter(distrCombTable);
        rootPane.getCenter().autosize();
        String title;
        title = bundle.getString("javafx.dialog.distrcomb.name");
        distrCombTable.getColumns().add(builderImp.createStringColumn(title, DistrCombModel::nameDistrCombProperty, false));
        distrCombTable.getColumns().get(0).setPrefWidth(331);
        distrCombTable.getColumns().get(0).setSortable(false);

        // ��� ����� ���������� �� ��� �����.
        Callback<ListView<DistrCombModel>, ListCell<DistrCombModel>> factory = lv -> new ListCell<DistrCombModel>() {
            //@Override
            protected void updateItem(DistrCombModel distr, boolean empty) {
                super.updateItem(distr, empty);
                setText(empty ? "" : distr.nameDistrCombProperty().get());
            }
        };
        distrComboBox.setCellFactory(factory);
    }

    @FXML
    private void delDistrComb (ActionEvent event) throws DataManageException {
        int rowImport = distrCombTable.getSelectionModel().getSelectedIndex();
        if (rowImport >= 0){
            String distrCombName = distrCombList.get(rowImport).nameDistrCombProperty().get();
            String titleMain = bundle.getString("javafx.dialog.distrcomb.dialog.attention");
            String titleMiddle = bundle.getString("javafx.dialog.distrcomb.dialog.del") + "?";
            String titleSmall = distrCombName;
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            if (alert.showAndWait().get() == ButtonType.OK){
                distrCombList.remove(rowImport);
                distrCombTable.setItems(FXCollections.observableArrayList(distrCombList));
                logger.debug("delDistrComb: " + distrCombName);
            }
            setDistrComboBox(distrEditEntity.getIdDistr());
        }
    }

    @FXML
    private void addDistrComb (ActionEvent event) throws DataManageException {
        DistrCombModel distrCombModel = distrComboBox.getSelectionModel().getSelectedItem();
        logger.debug("addDistrComb: " + distrCombModel.nameDistrCombProperty());
        distrCombList.add(distrCombModel);
        distrCombTable.setItems(FXCollections.observableArrayList(distrCombList));
        setDistrComboBox(distrEditEntity.getIdDistr());
    }

    @FXML
    private void saveToDB (ActionEvent event) {
        logger.debug("saveToDB");
        Alert alert;
        String titleMain = bundle.getString("javafx.dialog.distrcomb.dialog.attention");
        String titleMiddle = bundle.getString("javafx.dialog.distrcomb.dialog.save");
        String titleSmall = bundle.getString("javafx.dialog.distrcomb.dialog.good");
        try {
            IDistrCombDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrCombDAO();
            dao.deleteDistrComb(distrEditEntity.getIdDistr());
            List<Distr> itemList = new ArrayList<>();
            for (DistrCombModel aDistrCombList : distrCombList) {
                itemList.add(aDistrCombList.getDistr());
            }
            dao.addDistrCombList(itemList,distrEditEntity.getIdDistr());
        } catch (Exception e) {
            logger.error(e.toString(), e);
            titleSmall = bundle.getString("javafx.dialog.distrcomb.dialog.bad");
        } finally {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            alert.showAndWait();
        }
    }

    public void setContent(DistrEditEntity distrEditEntity) {
        scene.setCursor(Cursor.WAIT);
        try {
            this.distrEditEntity = distrEditEntity;

            // distrCombList -> distrCombTabel
            distrCombList = buildDistrCombListFromDistr(distrEditEntity.getIdDistr());
            distrCombTable.setItems(FXCollections.observableArrayList(distrCombList));

            // DistrAllList -> distrComboBox
            setDistrComboBox(distrEditEntity.getIdDistr());

        } catch (DataManageException | ObjectNotFoundException e) {
            e.printStackTrace();
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    private void setDistrComboBox (int idDistr) throws DataManageException {
        List<Distr> distrAllList = null;
        IDistrDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO();
        distrAllList = dao.getDistrList(0);
        distrList.clear();
        boolean dopusk;
        for (Distr aDistrAllList : distrAllList) {
            dopusk = true;
            DistrCombModel distrCombModel = new DistrCombModel(aDistrAllList);
            for (DistrCombModel aDistrCombList : distrCombList) {
                if (aDistrCombList.getIDDistrComb() == distrCombModel.getIDDistrComb()) {
                    dopusk = false;
                }
            }
            if (distrCombModel.getIDDistrComb() == idDistr) {
                dopusk = false;
            }
            if (dopusk) {
                distrList.add(distrCombModel);
            }
        }
        Collections.sort(distrList, new DistrCombModelComparator());
        distrComboBox.setItems(FXCollections.observableArrayList(distrList));
        distrComboBox.getSelectionModel().select(0);
    }

    private List<DistrCombModel> buildDistrCombListFromDistr(int idDistr) throws DataManageException, ObjectNotFoundException {
        List<DistrCombModel> itemList = new ArrayList<>();
        List<Integer> distrCombIntList = null;
        IDistrCombDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrCombDAO();
        distrCombIntList = dao.getDistrCombList(idDistr);
        for (Integer aDistrCombIntList : distrCombIntList) {
            List<Distr> distrAllList = null;
            IDistrDAO dao2 = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO();
            distrAllList = dao2.getDistrList(0);
            for (Distr aDistrAllList : distrAllList) {
                if (aDistrAllList.getIdDistr() == aDistrCombIntList) {
                    DistrCombModel distrCombModel = new DistrCombModel(aDistrAllList);
                    itemList.add(distrCombModel);
                }
            }
        }
        return itemList;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
