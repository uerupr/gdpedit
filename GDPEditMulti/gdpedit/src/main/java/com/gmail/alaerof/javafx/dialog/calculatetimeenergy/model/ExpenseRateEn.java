package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.energy.EnergyType;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ExpenseRateEn {
    private EnergyType energyType;

    private StringProperty energyTypeName;
    /** ��������� �������/�������������� �� ��� */
    private SimpleDoubleProperty energyExpRate;

    public ExpenseRateEn(EnergyType energyType) {
        this.energyType = energyType;
        this.energyTypeName = new SimpleStringProperty(energyType.getString());
        this.energyExpRate = new SimpleDoubleProperty(0.0);
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public String getEnergyTypeName() {
        return energyTypeName.get();
    }

    public StringProperty energyTypeNameProperty() {
        return energyTypeName;
    }

    public double getEnergyExpRate() {
        return energyExpRate.get();
    }

    public SimpleDoubleProperty energyExpRateProperty() {
        return energyExpRate;
    }

    public void setEnergyExpRate(double energyExpRate) {
        this.energyExpRate.set(energyExpRate);
    }
}
