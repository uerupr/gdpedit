package com.gmail.alaerof.javafx.dialog.calculategdp.view;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import com.gmail.alaerof.dao.dto.routed.RouteDSpan;
import com.gmail.alaerof.dao.dto.routed.RouteDTrain;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.gdpcalculation.CalculateGDPHandler;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.TaskWithCursorExecutor;
import com.gmail.alaerof.javafx.dialog.calculategdp.model.RouteDModel;
import com.gmail.alaerof.javafx.dialog.calculategdp.model.RouteDSpanModel;
import com.gmail.alaerof.javafx.dialog.calculategdp.model.RouteDTrainModel;
import com.gmail.alaerof.util.IconImageUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CalculateGDPController implements Initializable {
    /** logger */
    protected Logger logger;
    {
        logger = LogManager.getLogger(this.getClass());
    }
    /** Internationalization */
    protected ResourceBundle bundle;
    /** �������� ����� */
    private Scene scene;
    /** ������� � �������� */
    private DistrEditEntity distrEditEntity;

    /** ������� ���������  */
    private TableView<RouteDModel> routeTable;
    /** ������� ���������� �������  */
    private TableView<RouteDTrainModel> routeDTrainTable;
    /** ������� ���������  */
    private TableView<RouteDSpanModel> routeDSpanTable;
    /** ��������� �������  */
    private RouteDModel selectedRouteD;
    /** ������ ��������� ���������  */
    private List<RouteD> deletedRouteD = new ArrayList<>();
    /** ������ ��������� ��������� ��������  */
    private List<RouteDSpan> deletedRouteDSpan = new ArrayList<>();
    /** ������ ��������� ������� ��������  */
    private List<RouteDTrain> deletedRouteDTrain = new ArrayList<>();

    /** ������ ����������� (����� ��������) */
    @FXML
    private ComboBox<LocomotiveType> locTypeComboBox;
    @FXML
    private BorderPane routeListPane;
    @FXML
    private BorderPane trainRangePane;
    @FXML
    private BorderPane spanListPane;
    @FXML
    private TextArea errorLog;
    @FXML
    private Button selectLocButton;
    @FXML
    private Button addRouteButton;
    @FXML
    private Button delRouteButton;
    @FXML
    private Button addRangeButton;
    @FXML
    private Button delRangeButton;
    @FXML
    private Button addSpanButton;
    @FXML
    private Button delSpanButton;

    /**
     * @param scene  �������� �����
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }

    /**
     * ������������� ���������� ��� �������� �� FXML ����������� (FXMLLoader)
     * @param location {@link URL}
     * @param resources {@ResourceBundle} �������������� ��������
     */
    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " + location + " resources = " + resources);
        bundle = resources;

        initRouteTable();
        initRouteDSpanTable();
        initRouteDTrainTable();

        initRouteDButtons();
        initRouteDSpanButtons();
        initRouteDTrainButtons();
    }

    public void setContent(DistrEditEntity distrEditEntity) {
        scene.setCursor(Cursor.WAIT);
        try {
            errorLog.clear();
            deletedRouteD.clear();
            deletedRouteDSpan.clear();
            deletedRouteDTrain.clear();

            this.distrEditEntity = distrEditEntity;
            // LocomotiveType
            List<LocomotiveType> locomotiveTypeList = distrEditEntity.getDistrEntity().getListDistrLoc();
            locTypeComboBox.setItems(FXCollections.observableList(locomotiveTypeList));

            // �������� ��������� ������� (� ����������) ��� ������
            List<RouteD> listR = distrEditEntity.getDistrEntity().getListRoute();
            List<RouteDModel> routeDModelList = buildRouteDModelList(listR);
            routeTable.setItems(FXCollections.observableArrayList(routeDModelList));
            if (routeDModelList.size() > 0) {
                RouteDModel first = routeDModelList.get(0);
                selectRouteDModel(first);
            } else {
                selectedRouteD = null;
                routeDSpanTable.setItems(FXCollections.observableArrayList(new ArrayList<>()));
                routeDTrainTable.setItems(FXCollections.observableArrayList(new ArrayList<>()));
            }
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    private List<RouteDModel> buildRouteDModelList(List<RouteD> listR) {
        List<RouteDModel> list = new ArrayList<>();
        for (RouteD routeD : listR) {
            list.add(new RouteDModel(routeD));
        }
        return list;
    }

    private void initRouteTable(){
        EditableTableViewCreator<RouteDModel> builder = new EditableTableViewCreator<>();
        routeTable = builder.createEditableTableView();
        routeTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        routeListPane.setCenter(routeTable);
        String title;
        //--- columns ---
        title = bundle.getString("routeTable.name");
        TableColumn col = builder.createStringColumn(title, RouteDModel::nameRProperty, true);
        col.setPrefWidth(200);
        routeTable.getColumns().add(col);
        title = bundle.getString("routeTable.type");
        routeTable.getColumns().add(builder.createStringColumn(title, RouteDModel::typeRProperty, false));
        title = bundle.getString("routeTable.tooltip");
        routeTable.setTooltip(new Tooltip(title));
        //--
        routeTable.getSelectionModel().selectedItemProperty().addListener(buildRouteTableChangeListener());
        routeTable.setOnMouseClicked(this::handleOnMouseClicked);
    }

    /**
     * @return ChangeListener
     */
    private ChangeListener<RouteDModel> buildRouteTableChangeListener() {
        return (obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                updateCurrentValues(selectedRouteD);
                selectedRouteD = newSelection;
                selectRouteDModel(selectedRouteD);
            } else {
                selectedRouteD = null;
            }
        };
    }

    /**
     * @param event MouseEvent
     */
    private void handleOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            @SuppressWarnings("rawtypes")
            TablePosition pos = routeTable.getSelectionModel().getSelectedCells().get(0);
            int row = pos.getRow();
            int col = pos.getColumn();
            if (row >= 0 && col == 1) {
                RouteD routeD = routeTable.getItems().get(row).getRouteD();
                boolean def = routeD.getTypeR()==0;
                def = !def;
                String defStr = def ? "*" : "";
                routeD.setTypeRAsString(defStr);
                routeTable.getItems().get(row).setTypeR(defStr);
            }
        }
    }

    private void selectRouteDModel(RouteDModel selectedRouteD) {
        List<RouteDTrainModel> trainRangeModelList = buildRouteDTrainModelList(selectedRouteD);
        List<RouteDSpanModel> spanModelList = buildRouteDSpanDModelList(selectedRouteD);
        routeDTrainTable.setItems(FXCollections.observableArrayList(trainRangeModelList));
        routeDSpanTable.setItems(FXCollections.observableArrayList(spanModelList));
    }

    private List<RouteDSpanModel> buildRouteDSpanDModelList(RouteDModel selectedRouteD) {
        List<RouteDSpanModel> list = new ArrayList<>();
        if (selectedRouteD != null) {
            try {
                for (RouteDSpan dSpan : selectedRouteD.getRouteD().getListSpan()) {
                    DistrSpan distrSpan = distrEditEntity.getDistrEntity().getDistrSpan(dSpan.getIdSpan());
                    list.add(new RouteDSpanModel(dSpan, distrSpan));
                }
            } catch (Exception e) {
                logger.error(e.toString(), e);
                Alert alert = new Alert(Alert.AlertType.ERROR, e.toString());
                alert.showAndWait();
            }
        }
        return list;
    }

    private List<RouteDTrainModel> buildRouteDTrainModelList(RouteDModel selectedRouteD) {
        List<RouteDTrainModel> list = new ArrayList<>();
        if (selectedRouteD != null) {
            for (RouteDTrain dTrain : selectedRouteD.getRouteD().getListTR()) {
                list.add(new RouteDTrainModel(dTrain));
            }
        }
        return list;
    }

    private void updateCurrentValues(List<RouteDModel> routeDList) {
        for(RouteDModel dModel: routeDList){
            updateCurrentValues(dModel);
        }
    }

    private void updateCurrentValues(RouteDModel selectedRouteD) {
        if (selectedRouteD != null) {
            selectedRouteD.getRouteD().setNameR(selectedRouteD.getNameR());
            selectedRouteD.getRouteD().setIdDistr(distrEditEntity.getIdDistr());
            for(RouteDSpanModel spanModel: routeDSpanTable.getItems()){
                spanModel.getRouteDSpan().setNum(spanModel.getNum());
            }
            for(RouteDTrainModel trainModel: routeDTrainTable.getItems()){
                trainModel.getRouteDTrain().setTrMin(trainModel.getMin());
                trainModel.getRouteDTrain().setTrMax(trainModel.getMax());
            }
        }
    }

    private void initRouteDSpanTable(){
        EditableTableViewCreator<RouteDSpanModel> builder = new EditableTableViewCreator<>();
        routeDSpanTable = builder.createEditableTableView();
        routeDSpanTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        spanListPane.setCenter(routeDSpanTable);
        String title;
        //--- columns ---
        title = bundle.getString("routeDSpanTable.num");
        routeDSpanTable.getColumns().add(builder.createIntegerColumn(title, RouteDSpanModel::numProperty, true));
        title = bundle.getString("routeDSpanTable.name");
        TableColumn cal = builder.createStringColumn(title, RouteDSpanModel::nameProperty, false);
        cal.setPrefWidth(260);
        routeDSpanTable.getColumns().add(cal);
    }

    private void initRouteDTrainTable(){
        EditableTableViewCreator<RouteDTrainModel> builder = new EditableTableViewCreator<>();
        routeDTrainTable = builder.createEditableTableView();
        routeDTrainTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        trainRangePane.setCenter(routeDTrainTable);
        String title;
        //--- columns ---
        title = bundle.getString("routeDTrainTable.min");
        routeDTrainTable.getColumns().add(builder.createIntegerColumn(title, RouteDTrainModel::minProperty, true));
        title = bundle.getString("routeDTrainTable.max");
        routeDTrainTable.getColumns().add(builder.createIntegerColumn(title, RouteDTrainModel::maxProperty, true));
    }

    private void initRouteDButtons(){
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.PLUS);
            ImageView imageView = new ImageView(image);
            addRouteButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.MINUS);
            ImageView imageView = new ImageView(image);
            delRouteButton.setGraphic(imageView);
        }
    }

    private void initRouteDSpanButtons(){
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.PLUS);
            ImageView imageView = new ImageView(image);
            addSpanButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.MINUS);
            ImageView imageView = new ImageView(image);
            delSpanButton.setGraphic(imageView);
        }
    }

    private void initRouteDTrainButtons(){
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.PLUS);
            ImageView imageView = new ImageView(image);
            addRangeButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.MINUS);
            ImageView imageView = new ImageView(image);
            delRangeButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.ARR_DOWN);
            ImageView imageView = new ImageView(image);
            selectLocButton.setGraphic(imageView);
        }
    }

    @FXML
    private void handleCalculateGDP(ActionEvent event) {
        try {
            ICategoryDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO();
            List<Category> categories = dao.getCategories();
            Collection<DistrTrain> listDistrTrain = new ArrayList<>(distrEditEntity.getDistrTrains());
            Collection<DistrTrain> fixed = new ArrayList<>();
            List<String> errorList = new ArrayList<>();

            Task task = new Task<Void>() {
                @Override
                protected Void call() {
                    CalculateGDPHandler.buildConcurrentGDP(distrEditEntity.getDistrEntity(), fixed, listDistrTrain, categories, errorList);
                    return null;
                }
            };
            TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, () -> {
                distrEditEntity.refreshTrainThreads();
                if (errorList.size() > 0) {
                    fillErrorLog(errorList);
                }
            });

        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    @FXML
    private void handleSaveToDB(ActionEvent event) {
        // save to DB routes
        String message = bundle.getString("calculategdpfxdialog.ok");
        Alert.AlertType alertType = Alert.AlertType.INFORMATION;
        IDistrDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO();
        updateCurrentValues(routeTable.getItems());
        List<RouteD> list = RouteDModel.extractRouteD(routeTable.getItems());
        try {
            dao.saveRouteDs(list, deletedRouteD);
            distrEditEntity.getDistrEntity().updateRouteDList();
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
            message = e.getMessage();
            alertType = Alert.AlertType.ERROR;
        }
        Alert alert = new Alert(alertType);
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    @FXML
    private void handleRouteAdd(ActionEvent event) {
        RouteD routeD = new RouteD();
        routeTable.getItems().add(new RouteDModel(routeD));
        logger.debug("add new RouteD: " + routeD);
    }

    @FXML
    private void handleRouteDel(ActionEvent event) {
        if (routeTable.getSelectionModel() != null) {
            List<RouteDModel> selected = routeTable.getSelectionModel().getSelectedItems();
            List<RouteD> deleted = RouteDModel.extractRouteD(selected);
            for (RouteD routeD : deleted) {
                if (routeD.getIdRouteD() > 0) {
                    deletedRouteD.add(routeD);
                }
            }
            routeTable.getItems().removeAll(selected);
            selectedRouteD = null;
            selectRouteDModel(selectedRouteD);
            logger.debug("delete RouteD: " + deleted);
        }
    }

    @FXML
    private void handleRangeAdd(ActionEvent event) {
        if (selectedRouteD != null) {
            RouteDTrain routeDTrain = new RouteDTrain();
            routeDTrain.setIdRouteD(selectedRouteD.getRouteD().getIdRouteD());
            routeDTrainTable.getItems().add(new RouteDTrainModel(routeDTrain));
            selectedRouteD.getRouteD().getListTR().add(routeDTrain);
            logger.debug("add new RouteDTrain: " + routeDTrain);
        }
    }

    @FXML
    private void handleRangeDel(ActionEvent event) {
        if (routeDTrainTable.getSelectionModel() != null) {
            List<RouteDTrainModel> selected = routeDTrainTable.getSelectionModel().getSelectedItems();
            List<RouteDTrain> deleted = RouteDTrainModel.extractRouteDTrain(selected);
            for (RouteDTrain routeDTrain : deleted) {
                if (routeDTrain.getIdRouteDTR() > 0) {
                    deletedRouteDTrain.add(routeDTrain);
                }
                selectedRouteD.getRouteD().getListTR().remove(routeDTrain);
            }
            routeDTrainTable.getItems().removeAll(selected);
            logger.debug("delete RouteDTrain: " + deleted);
        }
    }

    @FXML
    private void handleSpanAdd(ActionEvent event) {
        if (selectedRouteD != null) {
            List<DistrSpan> distrSpanList = distrEditEntity.getDistrEntity().getListSp();
            // method reference
            distrSpanList.sort(Comparator.comparingInt(DistrSpan::getNum));
            List<RouteDSpanModel> dSpanModelList = routeDSpanTable.getItems();
            for (DistrSpan distrSpan : distrSpanList) {
                if (!inList(distrSpan, dSpanModelList)) {
                    RouteDSpan routeDSpan = new RouteDSpan();
                    routeDSpan.setIdRouteD(selectedRouteD.getRouteD().getIdRouteD());
                    routeDSpan.setIdSpan(distrSpan.getSpanScale().getIdSpan());
                    routeDSpan.setNum(distrSpan.getSpanScale().getNum());
                    routeDSpanTable.getItems().add(new RouteDSpanModel(routeDSpan, distrSpan));
                    selectedRouteD.getRouteD().getListSpan().add(routeDSpan);
                    logger.debug("add new RouteDSpan: " + routeDSpan);
                }
            }
            // method reference
            routeDSpanTable.getItems().sort(Comparator.comparingInt(RouteDSpanModel::getNum));
        }
    }

    private boolean inList(DistrSpan distrSpan, List<RouteDSpanModel> dSpanModelList) {
        for (RouteDSpanModel spanModel : dSpanModelList) {
            if (spanModel.getRouteDSpan().getIdSpan() == distrSpan.getSpanScale().getIdSpan()) {
                return true;
            }
        }
        return false;
    }

    @FXML
    private void handleSpanDel(ActionEvent event) {
        if (routeDSpanTable.getSelectionModel() != null) {
            List<RouteDSpanModel> selected = routeDSpanTable.getSelectionModel().getSelectedItems();
            List<RouteDSpan> deleted = RouteDSpanModel.extractRouteDSpan(selected);
            for (RouteDSpan routeDSpan : deleted) {
                if (routeDSpan.getIdRouteDS()> 0) {
                    deletedRouteDSpan.add(routeDSpan);
                }
                selectedRouteD.getRouteD().getListSpan().remove(routeDSpan);
            }
            routeDSpanTable.getItems().removeAll(selected);
            logger.debug("delete RouteDSpan: " + deleted);
        }
    }

    @FXML
    private void handleSelectLoc(ActionEvent event) {
    }

    private void fillErrorLog(List<String> errorList) {
        errorLog.setWrapText(false);
        for(String str: errorList){
            errorLog.appendText(str);
            errorLog.appendText("\n");
        }
    }
}
