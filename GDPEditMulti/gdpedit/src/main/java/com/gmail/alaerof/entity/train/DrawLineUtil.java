package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.dao.common.GDPGridConfig;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class DrawLineUtil {
    protected static Logger logger = LogManager.getLogger(DrawLineUtil.class);

    /**
     * ��������� ������� �� �������� �� �����
     * @param grGDP �����
     * @param timeStBegin ��������� ������� ��������
     * @param timeStEnd �������� ������� ��������
     * @param lastSp  ������� ���������� ��������
     * @param trainThread ����� ������
     * @param config ����� �������������� ����������
     */
    public static void drawTrainSp(Graphics grGDP, TimeStation timeStBegin, TimeStation timeStEnd, boolean lastSp,
                                   TrainThread trainThread, GDPGridConfig config) {
        try {
            NewLineSelfParams selfParams = new NewLineSelfParams();
            //TrainThreadGeneralParams
            if (! timeStBegin.hideSpan && !trainThread.isHidden()) {
                List<DistrStation> subListSt = new ArrayList<>();
                NewLine.Direction direction;
                // ����������� ��������
                if (timeStBegin.station.getY0() <= timeStEnd.station.getY0()) {
                    direction = NewLine.Direction.left;
                } else {
                    direction = NewLine.Direction.right;
                }
                List<DrawLine> lines = buildLines(direction, trainThread, timeStBegin, timeStEnd, selfParams, subListSt,
                        config);

                TrainThreadGeneralParams generalParams = trainThread.getGeneralParams();
                boolean viewTimeA = false;
                boolean viewTimeL = false;
                if ((lastSp || (timeStEnd.mOn != timeStEnd.mOff)) && !("0".equals(selfParams.timeA))) {
                    viewTimeA = true;
                }
                int w = 0;

                if (timeStBegin.xOff <= timeStEnd.xOn) {
                    // ��� �������� �� ���� �����
                    w = timeStEnd.xOn - timeStBegin.xOff;
                } else {
                    // ���� ������� �� ���� �����
                    int w1 = generalParams.xRight24 - timeStBegin.xOff;
                    int w2 = timeStEnd.xOn - generalParams.xLeft;
                    w = w1 + w2;
                }
                int h = 0;

                // ������� ������� ����� ������
                for (int i = 0; i < subListSt.size() - 1; i++) {
                    DistrStation st1 = subListSt.get(i);
                    DistrStation st2 = subListSt.get(i + 1);
                    h += st2.getY1() - st1.getYE1();
                }
                float xL = timeStBegin.xOff;
                int indexLine = 0;
                int indexSt = 0;
                int yTop = 0;
                boolean condition = subListSt.size() > 1;
                if (direction == NewLine.Direction.left) {
                    indexSt = 0;
                } else {
                    indexSt = subListSt.size() - 2;
                }
                // ������ ������ ������ �����
                while (condition) {
                    DistrStation st1 = subListSt.get(indexSt);
                    DistrStation st2 = subListSt.get(indexSt + 1);
                    if (direction == NewLine.Direction.left) {
                        indexSt++;
                        condition = indexSt < subListSt.size() - 1;
                    } else {
                        indexSt--;
                        condition = indexSt >= 0;
                    }
                    yTop = st1.getYE1();
                    int hl = st2.getY1() - st1.getYE1();
                    float wl = w * hl / h;
                    int xNext = (int) (xL + wl);
                    if (xNext > (generalParams.xLeft + config.gdpWidth)) {
                        // ��� � ��� ������� �� ���� �����
                        float w1 = 0; //= Math.max(0, generalParams.xRight - xL);
                        float w2 = 0; //= generalParams.xRight24 - generalParams.xRight;
                        float w3 = 0; //= xNext - generalParams.xRight;
                        if (xL < generalParams.xLeft + config.gdpWidth) {
                            w1 = generalParams.xLeft + config.gdpWidth - xL;
                        }
                        if (xNext > generalParams.xRight24) {
                            w2 = generalParams.xRight24 - Math.max(generalParams.xLeft + config.gdpWidth, xL);
                            w3 = xNext - generalParams.xRight24;
                        }else{
                            w2 = xNext - generalParams.xLeft + config.gdpWidth;
                        }

                        int h1 = (int) (w1 * h / w);
                        int h2 = (int) (w2 * h / w);
                        int h3 = (int) (w3 * h / w);

                        // ������ - �� ����������� � ���. ������� �� ����� �������� ���
                        if (w1 > 0) {
                            DrawLine line = lines.get(indexLine);
                            viewTimeL = true;
                            Dimension dim = line.getSize();
                            if (direction == NewLine.Direction.left) {
                                dim.width = (int) w1;
                                dim.height = Math.max(1, h1);
                                line.setPosition((int) xL, yTop);
                            } else {
                                dim.width = (int) w1;
                                dim.height = Math.max(1, h1);
                                line.setPosition((int) xL, (int) (yTop + h2 + h3));
                            }
                            indexLine++;
                        }
                        // ��������� - �� ����� �������� ��� �� ����� ����� (24)
                        if (w2 > 0) {
                            // ������ �� ������, �.�. ��� ������ �� ����� ����������
                        }
                        // ����� - �� ������ �������� ��� �� �������� �� ���. �������
                        if (w3 > 0) {
                            DrawLine line = lines.get(indexLine);
                            Dimension dim = line.getSize();
                            if (direction == NewLine.Direction.left) {
                                dim.width = (int) w3;
                                dim.height = Math.max(1, h3);
                                line.setPosition(generalParams.xLeft, (int) (yTop + h1 + h2));
                            } else {
                                dim.width = (int) w3;
                                dim.height = Math.max(1, h3);
                                line.setPosition(generalParams.xLeft, yTop);
                            }
                            indexLine++;
                        }
                        xL = generalParams.xLeft + w3;
                    } else {
                        // � ��� ���
                        viewTimeL = true;
                        DrawLine line = lines.get(indexLine);
                        Dimension dim = line.getSize();
                        dim.width = (int) wl;
                        dim.height = Math.max(1, hl);
                        line.setPosition((int) xL, yTop);

                        indexLine++;
                        xL += wl;
                    }
                }

                if (lines.size() > 0) {
                    DrawLine line = lines.get(0);
                    line.setTimeL((!("0".equals(selfParams.timeL))) && viewTimeL);
                    line = lines.get(lines.size() - 1);
                    line.setTimeA(viewTimeA);
                }
                for(DrawLine ln: lines){
                    ln.drawInPicture(grGDP);
                }
            }
        } catch (Exception e) {
            logger.error("setLineBounds,  train: " +
                    trainThread.getCode() +
                    "\t" + trainThread.getNameTR() +
                    "\t stB:\t" + timeStBegin.getDistrStation().getNamePoint() +
                    "\t stB:\t" + timeStEnd.getDistrStation().getNamePoint(), e);
        }
    }

    private static List<DrawLine> buildLines(NewLine.Direction direction, TrainThread trainThread,
                                             TimeStation timeStBegin, TimeStation timeStEnd,
                                             NewLineSelfParams selfParams, List<DistrStation> subListSt,
                                             GDPGridConfig config) {
        List<DrawLine> lines = new ArrayList<>();

        try {
            TrainThreadGeneralParams params = trainThread.getGeneralParams();

            selfParams.color = timeStBegin.colorTR;
            String str = trainThread.getCode();

            if (timeStBegin.showCode == 1) {
                str = trainThread.getAInscr();
            }
            if (timeStBegin.showCode == 2) {
                str = timeStBegin.ainscr;
            }
            if (str == null)
                str = "";
            selfParams.setCaptionValue(str);

            //����� * ����� ������� ����������� ������
            LineStTrain lineStTrain = timeStBegin.getLineStTrain();
            if (lineStTrain.getSk_ps() != null) {
                selfParams.timeL = "" + timeStBegin.mOff % 10 + lineStTrain.getSk_ps();
            } else {
                selfParams.timeL = "" + timeStBegin.mOff % 10;
            }
            selfParams.timeA = "" + timeStEnd.mOn % 10;

            // ����� �����
            selfParams.lineStyle = timeStBegin.lineStyleSpan;
            selfParams.lineWidth = trainThread.getWidthTrain();

            // ���������� ���������� �����
            int lineCount = calcLineCount(trainThread, timeStBegin, timeStEnd, subListSt, config);

            int minCount = Math.min(lineCount, lines.size());
            for (int i = 0; i < minCount; i++) {
                lines.get(i).setSelfParams(selfParams);
                lines.get(i).setGeneralParams(params);
            }

            if (lineCount < lines.size()) {
                // ������� ������
                int lc = lines.size() - 1;
                for (int i = lc; i >= lineCount; i--) {
                    lines.remove(i);
                }
            } else {
                // ��������� �����������
                int lc = lineCount - lines.size();
                for (int i = 0; i < lc; i++) {
                    DrawLine ln;
                    if (direction == NewLine.Direction.left) {
                        ln = new DrawLineLeft(params, selfParams);
                    } else {
                        ln = new DrawLineRight(params, selfParams);
                    }
                    lines.add(ln);
                }
            }

            if (lines.size() > 0) {
                // ������� ����������� ������� �� �����
                if (timeStBegin.showCode != -1) {
                    lines.get(0).setCaption(NewLine.DrawCaption.Center);
                } else {
                    lines.get(0).setCaption(NewLine.DrawCaption.NoCaption);
                }
            }
        } catch (Exception e) {
            logger.error("buildLines,  train: " + trainThread.getCode() + "\t" + trainThread.getNameTR(), e);
        }
        return lines;
    }

    private static int calcLineCount(TrainThread trainThread, TimeStation timeStBegin, TimeStation timeStEnd,
                                     List<DistrStation> subListSt, GDPGridConfig config) {
        int res = 1;
        subListSt.clear();
        List<DistrStation> listSt = trainThread.getDistrEntity().getListSt();
        DistrStation st1 = timeStBegin.station;
        DistrStation st2 = timeStEnd.station;
        int indx1 = listSt.indexOf(st1);
        int indx2 = listSt.indexOf(st2);
        if (indx1 > -1 && indx2 > -1) {
            subListSt.addAll(listSt.subList(Math.min(indx1, indx2), Math.max(indx1, indx2)));
            // ������ ��������� ��� ������� 2 �������
            subListSt.add(listSt.get(Math.max(indx1, indx2)));
            res = subListSt.size() - 1; // ���-�� ����� ������ ���-�� ������� �� 1
            TrainThreadGeneralParams generalParams = trainThread.getGeneralParams();
            // ���� ���� ������� �� ���� ����� � "����������" ����� �������� � ������������ �������� ��������
            if (timeStBegin.xOff > timeStEnd.xOn && timeStBegin.xOff < (generalParams.xLeft + config.gdpWidth)) {
                res += 1;
            }
        } else {
            // ��������, ��� ���� �� 1 ������� ������ ��� ����� ������� �� �������,
            // �.�. � �������� �� �����
            res = 0;
        }
        return res;
    }

    /**
     * ��������� ������� �� ������� �� �����
     * @param grGDP �����
     * @param timeSt ������� �������
     * @param timeStBefore ���������� �������
     * @param timeStAfter ��������� �������
     * @param trainThread ����� ������
     * @param config ����� �������������� ����������
     */
    public static void drawTrainSt(Graphics grGDP,
                                   TimeStation timeSt, TimeStation timeStBefore, TimeStation timeStAfter,
                                   TrainThread trainThread, GDPGridConfig config) {

        NewLineSelfParams selfParams = new NewLineSelfParams();
        selfParams.setCaptionValue(trainThread.getCode());

        // ����� �����
        selfParams.lineStyle = timeSt.lineStyleSpan;
        selfParams.lineWidth = trainThread.getWidthTrain();
        selfParams.color = timeSt.colorTR;
        // � �� ������� ���� ����
        // �� ������� ������������ �����, ����� ��� �� ����� ������
        boolean isVertLines = trainThread.isShowInStation() && (timeSt.getLineCount() > 0);
        NewLineSelfParams selfParamsVert = new NewLineSelfParams();
        if (isVertLines) {
            selfParamsVert.setCaptionValue(trainThread.getCode());
            selfParamsVert.lineStyle = LineStyle.Solid;
            selfParamsVert.lineWidth = trainThread.getWidthTrain();
            selfParamsVert.color = timeSt.colorTR;
        }

        if (timeSt.station.isExpanded()) {
            drawTrainStExpanded(grGDP, timeSt, timeStBefore, timeStAfter, trainThread, config, selfParams, selfParamsVert);
        } else {
            drawTrainStNoExpanded(grGDP, timeSt, trainThread, config, selfParams);
        }
    }

    /**
     *
     * @param grGDP
     * @param timeSt
     * @param timeStBefore
     * @param timeStAfter
     * @param trainThread
     * @param config
     * @param selfParams
     * @param selfParamsVert
     */
    private static void drawTrainStExpanded(Graphics grGDP,
                                            TimeStation timeSt, TimeStation timeStBefore, TimeStation timeStAfter,
                                            TrainThread trainThread,
                                            GDPGridConfig config, NewLineSelfParams selfParams,
                                            NewLineSelfParams selfParamsVert) {
        TrainThreadGeneralParams generalParams = trainThread.getGeneralParams();
        DrawLineHor horLine1 = new DrawLineHor(generalParams, selfParams);
        DrawLineHor horLine2 = new DrawLineHor(generalParams, selfParams);
        DrawLineVert vertLineOn = null;
        DrawLineVert vertLineOff = null;

        // ���� ���� ��� �������
        boolean isTexStop = (timeSt.isTexOff() || timeSt.isTexOn());
        // ������� ����������� ������� �� �����
        horLine1.setCaption(NewLine.DrawCaption.NoCaption);
        if (trainThread.isShowInStation() && timeSt.getLineCount() > 0) {
            horLine1.setCaption(NewLine.DrawCaption.On);
            if (timeSt.isTexOff())
                horLine1.setCaption(NewLine.DrawCaption.texOff);
            if (timeSt.isTexOn())
                horLine1.setCaption(NewLine.DrawCaption.texOn);

        }

        // �������� ������������� �������� (���������)
        boolean vertOn = false;
        boolean vertOff = false;
        boolean line1 = false;
        boolean line2 = false;

        int timeStxOn = timeSt.xOn;
        int timeStxOff = timeSt.xOff;
        if (isTexStop) {
            timeStxOn = timeSt.xOnTexStop;
            timeStxOff = timeSt.xOffTexStop;
        }

        if (!timeSt.hideHor && !trainThread.isHidden() && trainThread.isShowInStation()) {
            // ���� ������� ��� �������� �� ���� �����
            if (timeStxOn < timeStxOff && timeStxOn < (generalParams.xLeft + config.gdpWidth) ) {
                line1 = true;
            }
            // ���� ������� � ���� ������� �� ����� �����
            if (timeStxOn > timeStxOff) {
                if (timeStxOn < (generalParams.xLeft + config.gdpWidth)) {
                    line1 = true;
                }
                if (timeStxOff > generalParams.xLeft) {
                    line2 = true;
                }
            }
            boolean s = trainThread.getListTimeSt().size() > 1;
            if (!timeSt.isFirst() && s && timeStxOn < (generalParams.xLeft + config.gdpWidth)) {
                vertOn = true;
                vertLineOn = new DrawLineVert(generalParams, selfParamsVert);

            }
            if (!timeSt.isLast() && s && timeStxOff < (generalParams.xLeft + config.gdpWidth)) {
                vertOff = true;
                vertLineOff = new DrawLineVert(generalParams, selfParamsVert);
            }
        }

        // ���� ���� ���� ���-��, �� ������� ����������
        if (vertOn || vertOff || line1 || line2) {
            // ������ ������������ ���������
            int yTop = 0;
            int yBot = 0;
            int yMiddle = 0;

            int idLineSt = timeSt.lineStTrain.getIdLineSt();
            yMiddle = timeSt.station.getLineY(idLineSt);
            yTop = timeSt.station.getY1();
            yBot = timeSt.station.getYE1();

            if (yMiddle == -1) {
                if (TimeStation.getOnDirection(timeSt, timeStBefore) == TimeStation.DirectionVert.onFromTop) {
                    yMiddle = yTop;
                } else {
                    yMiddle = yBot;
                }
                // � ������, ����� ��� ���������� �������, ������� ���������
                if (yMiddle == -1) {
                    if (TimeStation.getOffDirection(timeSt, timeStAfter) == TimeStation.DirectionVert.offToTop) {
                        yMiddle = yTop;
                    } else {
                        yMiddle = yBot;
                    }
                }
            }

            int wH = LineVert.VERT_WIDTH;
            // ������� ��������
            if (vertOn) {
                int yOn = yMiddle;
                int hOn = yBot - yMiddle;
                if (TimeStation.getOnDirection(timeSt, timeStBefore) == TimeStation.DirectionVert.onFromTop) {
                    yOn = yTop;
                    hOn = yMiddle - yTop;
                }
                if (vertLineOn != null) {
                    vertLineOn.getSize().width = wH;
                    vertLineOn.getSize().height = hOn;
                    vertLineOn.setPosition(timeStxOn, yOn);
                }
            }
            // ������� �����������
            if (vertOff) {
                int yOff = yMiddle;
                int hOff = yBot - yMiddle;
                if (TimeStation.getOffDirection(timeSt, timeStAfter) == TimeStation.DirectionVert.offToTop) {
                    yOff = yTop;
                    hOff = yMiddle - yTop;
                }
                if (vertLineOff != null) {
                    vertLineOff.getSize().width = wH;
                    vertLineOff.getSize().height = hOff;
                    vertLineOff.setPosition(timeStxOff, yOff);
                }
            }
            // ������ �������������� ���������
            if (line1 || line2) {
                int h = timeSt.station.getNameHeight() + 2;
                // ���� ��� �������� ����� �����
                if (timeStxOn < timeStxOff) {
                    int xOff = Math.min(timeStxOff, (generalParams.xLeft + config.gdpWidth));
                    int w1 = Math.max(1, xOff - timeStxOn);
                    Dimension dim = horLine1.getSize();
                    dim.width = w1;
                    dim.height = h;
                    horLine1.setPosition(timeStxOn, yMiddle);
                    horLine1.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                } else {
                    if (line1) {
                        int w1 = (generalParams.xLeft + config.gdpWidth) - timeStxOn;
                        Dimension dim = horLine1.getSize();
                        dim.width = Math.max(1, w1);
                        dim.height = h;
                        horLine1.setPosition(timeStxOn, yMiddle);
                        horLine1.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                    }
                    if (line2) {
                        int w2 = timeStxOff - generalParams.xLeft;
                        Dimension dim = horLine2.getSize();
                        dim.width = Math.max(1, w2);
                        dim.height = h;
                        horLine2.setPosition(generalParams.xLeft, yMiddle);
                        horLine2.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                    }
                }
            }
        }

        horLine1.setDrawTex(NewLine.DrawTex.NoTex);
        horLine2.setDrawTex(NewLine.DrawTex.NoTex);
        if (timeSt.isTexOn()) {
            horLine1.setDrawTex(NewLine.DrawTex.TexOn);
        }
        if (timeSt.isTexOff()) {
            if (line2) {
                horLine2.setDrawTex(NewLine.DrawTex.TexOff);
            } else {
                horLine1.setDrawTex(NewLine.DrawTex.TexOff);
            }
        }

        if (line1) {
            horLine1.drawInPicture(grGDP);
        }
        if (line2) {
            horLine2.drawInPicture(grGDP);
        }
        if (vertOn) {
            vertLineOn.drawInPicture(grGDP);
        }
        if (vertOff) {
            vertLineOff.drawInPicture(grGDP);
        }
    }

    /**
     * ��������� ������� �� ������� �� �����, ������� �� ���������� �� �����
     * @param grGDP �����
     * @param timeSt �������
     * @param trainThread �����
     * @param config ����� �������������� ����������
     * @param selfParams �������������� ��������� �����
     */
    private static void drawTrainStNoExpanded(Graphics grGDP, TimeStation timeSt, TrainThread trainThread,
                                              GDPGridConfig config, NewLineSelfParams selfParams) {

        TrainThreadGeneralParams generalParams = trainThread.getGeneralParams();
        DrawLine drawLine1 = new DrawLineHor(generalParams, selfParams);
        DrawLine drawLine2 = new DrawLineHor(generalParams, selfParams);
        boolean line1 = false;
        boolean line2 = false;

        if (!timeSt.hideHor && !trainThread.isHidden()) {
            // ���� ������� ��� �������� �� ���� �����
            if (timeSt.xOn < timeSt.xOff && timeSt.xOn < (generalParams.xLeft + config.gdpWidth) ) {
                line1 = true;
            }
            // ���� ������� � ���� ������� �� ����� �����
            if (timeSt.xOn > timeSt.xOff) {
                if (timeSt.xOn < (generalParams.xLeft + config.gdpWidth)) {
                    line1 = true;
                }
                if (timeSt.xOff > generalParams.xLeft) {
                    line2 = true;
                }
            }
        }

        if (line1 || line2) {
            // ������ ������������ ���������
            int yMiddle = timeSt.station.getY1();
            // ������ �������������� ���������
            // ���� ��� �������� ����� �����
            if (timeSt.xOn < timeSt.xOff) {
                int timeStxOff = Math.min(timeSt.xOff, (generalParams.xLeft + config.gdpWidth));
                int w1 = Math.max(1, timeStxOff - timeSt.xOn);
                Dimension dim = drawLine1.getSize();
                dim.width = w1;
                dim.height = LineHor.NO_CAPTION_HEIGHT;
                drawLine1.setPosition(timeSt.xOn, yMiddle);
                drawLine1.selfParams.lineWidth = Math.max(1, trainThread.getWidthTrain());
            } else {
                if (line1) {
                    int w1 = (generalParams.xLeft + config.gdpWidth) - timeSt.xOn;
                    Dimension dim = drawLine1.getSize();
                    dim.width = Math.max(1, w1);
                    dim.height = LineHor.NO_CAPTION_HEIGHT;
                    drawLine1.setPosition(timeSt.xOn, yMiddle);
                    drawLine1.selfParams.lineWidth = Math.max(1, trainThread.getWidthTrain());
                }
                if (line2) {
                    int w2 = timeSt.xOff - generalParams.xLeft;
                    Dimension dim = drawLine2.getSize();
                    dim.width = Math.max(1, w2);
                    dim.height = LineHor.NO_CAPTION_HEIGHT;
                    drawLine2.setPosition(generalParams.xLeft, yMiddle);
                    drawLine2.selfParams.lineWidth = Math.max(1, trainThread.getWidthTrain());
                }
            }
        }

        if(line1){
            drawLine1.drawInPicture(grGDP);
        }
        if(line2){
            drawLine2.drawInPicture(grGDP);
        }
    }

    /**
     * ��������� ������ ������� �� ����� ����������� �������
     * @param grGDP �����
     * @param timeSt �������
     * @param trainThread �����
     * @param config ����� �������������� ����������
     * @param selfParams �������������� ��������� �����
     */
    public static void drawTrainStDouble(Graphics grGDP, TimeStation timeSt,
                                         TrainThread trainThread,TrainThread trainThreadOff,
                                         GDPGridConfig config, NewLineSelfParams selfParams){

        TrainThreadGeneralParams generalParams = trainThread.getGeneralParams();
        DrawLine drawLine1 = new DrawLineHor(generalParams, selfParams);
        DrawLine drawLine2 = new DrawLineHor(generalParams, selfParams);

        // ���� ���������� �� �����
        boolean line1 = false;
        boolean line2 = false;
        if ((trainThread.isShowInStation() || trainThreadOff.isShowInStation()) &&
                !trainThread.isHidden() && !trainThreadOff.isHidden()) {
            // ���� ������� ��� �������� �� ���� �����
            if (timeSt.xOn < timeSt.xOff && timeSt.xOn < (generalParams.xLeft + config.gdpWidth) ) {
                line1 = true;
            }
            // ���� ������� � ���� ������� �� ����� �����
            if (timeSt.xOn > timeSt.xOff) {
                if (timeSt.xOn < (generalParams.xLeft + config.gdpWidth)) {
                    line1 = true;
                }
                if (timeSt.xOff > generalParams.xLeft) {
                    line2 = true;
                }
            }
        }

        if (line1 || line2) {

            // ������� ����������� ������� �� �����
            drawLine1.setCaption(NewLine.DrawCaption.NoCaption);
            if ((trainThread.isShowInStation() || trainThreadOff.isShowInStation()) && timeSt.getLineCount() > 0) {
                drawLine1.setCaption(NewLine.DrawCaption.On);
            }

            // ������ ������������ ���������
            int yTop = 0;
            int yMiddle = 0;
            int h = timeSt.station.getNameHeight() + 2;

            int idLineSt = timeSt.getTrainCombSt().idLineSt; // timeSt.lineStTrain.getIdLineSt();
            yMiddle = timeSt.station.getLineY(idLineSt);
            yTop = timeSt.station.getY1();

            if (yMiddle == -1) {
                yMiddle = yTop;
            }

            // ������ �������������� ���������
            // ���� ��� �������� ����� �����
            if (timeSt.xOn < timeSt.xOff) {
                int timeStxOff = Math.min(timeSt.xOff, (generalParams.xLeft + config.gdpWidth));
                int w1 = Math.max(1, timeStxOff - timeSt.xOn);
                Dimension dim = drawLine1.getSize();
                dim.width = w1;
                dim.height = h;
                drawLine1.setPosition(timeSt.xOn, yMiddle);
                drawLine1.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
            } else {
                if (line1) {
                    int w1 = (generalParams.xLeft + config.gdpWidth) - timeSt.xOn;
                    Dimension dim = drawLine1.getSize();
                    dim.width = Math.max(1, w1);
                    dim.height = h;
                    drawLine1.setPosition(timeSt.xOn, yMiddle);
                    drawLine1.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                }
                if (line2) {
                    int w2 = timeSt.xOff - generalParams.xLeft;
                    Dimension dim = drawLine2.getSize();
                    dim.width = Math.max(1, w2);
                    dim.height = h;
                    drawLine2.setPosition(generalParams.xLeft, yMiddle);
                    drawLine2.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                }
            }
        }

        if(line1){
            drawLine1.drawInPicture(grGDP);
        }
        if(line2){
            drawLine2.drawInPicture(grGDP);
        }
    }
}
