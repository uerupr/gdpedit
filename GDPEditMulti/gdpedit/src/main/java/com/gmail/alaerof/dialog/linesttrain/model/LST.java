package com.gmail.alaerof.dialog.linesttrain.model;

public class LST {

    private long idTrain;

    private String codeTR;

    private String nameTR;

    private String tOn;

    private String tOff;

    private int tTex;

    private int IDlineSt;

    private int N;

    private String TRA;

    private int colorLn;

    private int colorTR;

    private int lineStyle;

    private int widthTR;

    public long getIdTrain() {
        return idTrain;
    }

    public String getCodeTR() {
        return codeTR;
    }

    public String getNameTR() {
        return nameTR;
    }

    public String gettOn() {
        return tOn;
    }

    public String gettOff() {
        return tOff;
    }

    public int gettTex() {
        return tTex;
    }

    public int getIDlineSt() {
        return IDlineSt;
    }

    public int getN() {
        return N;
    }

    public String getTRA() {
        return TRA;
    }

    public int getColorLn() {
        return colorLn;
    }

    public int getColorTR() {
        return colorTR;
    }

    public int getLineStyle() {
        return lineStyle;
    }

    public int getWidthTR() {
        return widthTR;
    }

    public void setIdTrain(long idTrain) {
        this.idTrain = idTrain;
    }

    public void setCodeTR(String codeTR) {
        this.codeTR = codeTR;
    }

    public void setNameTR(String nameTR) {
        this.nameTR = nameTR;
    }

    public void settOn(String tOn) {
        this.tOn = tOn;
    }

    public void settOff(String tOff) {
        this.tOff = tOff;
    }

    public void settTex(int tTex) {
        this.tTex = tTex;
    }

    public void setIDlineSt(int IDlineSt) {
        this.IDlineSt = IDlineSt;
    }

    public void setN(int n) {
        N = n;
    }

    public void setTRA(String TRA) {
        this.TRA = TRA;
    }

    public void setColorLn(int colorLn) {
        this.colorLn = colorLn;
    }

    public void setColorTR(int colorTR) {
        this.colorTR = colorTR;
    }

    public void setLineStyle(int lineStyle) {
        this.lineStyle = lineStyle;
    }

    public void setWidthTR(int widthTR) {
        this.widthTR = widthTR;
    }
}
