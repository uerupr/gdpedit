package com.gmail.alaerof.javafx.dialog.distrcomb;

import com.gmail.alaerof.javafx.dialog.distrcomb.model.DistrCombModel;
import java.util.Comparator;

public class DistrCombModelComparator implements Comparator<DistrCombModel> {
    @Override
    public int compare(DistrCombModel distrCombModel1, DistrCombModel distrCombModel2) {
        String s1 = distrCombModel1.nameDistrCombProperty().get();
        String s2 = distrCombModel2.nameDistrCombProperty().get();
        return s1.compareTo(s2);
    }
}
