package com.gmail.alaerof.dialog.spantimetown;

import java.awt.BorderLayout;
import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.gmail.alaerof.entity.DistrEntity;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class SpanTimeTownDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private ListSpanTime listSpanTime;

    public SpanTimeTownDialog() {
        initContent();
    }
    public SpanTimeTownDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }
    
    private void initContent(){
        //super("������� ���� �� �.�.");
        //setIconImage(IconImageUtil.getImage(IconImageUtil.MAIN));
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setOpaque(false);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        setAlwaysOnTop(true);
        listSpanTime = new ListSpanTime();
        contentPane.add(listSpanTime);
        
        //setUndecorated(true);
        //setOpacity(0.5f);
        
    }

    public void setDistrEntity(DistrEntity distrEntity) {
        //if (listSpanTime.getDistrEntity() != distrEntity) {
            distrEntity.reloadSpanStTime();
            listSpanTime.setDistrEntity(distrEntity);
        //}
    }

}
