package com.gmail.alaerof.gdpcalculation.model;

import com.gmail.alaerof.dao.dto.gdp.SpTOcc;
import com.gmail.alaerof.util.TimeConverter;

public class TimeSpCalc implements Cloneable {
    private SpTOcc spTOcc;
    private int mOn;
    private int mOff;

    public TimeSpCalc(SpTOcc  spTOcc){
        this.spTOcc = spTOcc;
        this.mOn = TimeConverter.timeToMinutes(spTOcc.getOccupyOn());
        if (spTOcc.getOccupyOff() == null) {
            this.mOff = mOn;
        } else {
            this.mOff = TimeConverter.timeToMinutes(spTOcc.getOccupyOff());
        }
    }

    public TimeSpCalc(int mOn, int mOff){
        this.mOn = mOn;
        this.mOff = mOff;
    }

    public int getmOn() {
        return mOn;
    }

    public int getmOff() {
        return mOff;
    }

    public void setLeaveSp(int mOff) {
        this.mOff = mOff;
        String off = TimeConverter.minutesToTime(mOff);
        spTOcc.setOccupyOff(off);
        spTOcc.setTmove(TimeConverter.minutesBetween(mOn, mOff));
    }
}
