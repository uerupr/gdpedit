package com.gmail.alaerof.util;

import java.text.DecimalFormat;

import javax.swing.JTable;

/**
 * @author Helen Yrofeeva
 */
public class TableUtils {

    public static void setTableColumnSize(JTable table, int[] colSize) {
        int colCount = table.getColumnModel().getColumnCount();
        for (int i = 0; i < colSize.length; i++) {
            if (i < colCount) {
                table.getColumnModel().getColumn(i).setPreferredWidth((int) (colSize[i] * 1.5));
                table.getColumnModel().getColumn(i).setMinWidth(colSize[i]);
            }
        }
    }
    
    /**
     * ��������� ����� � ������ � ������������ ����� �������
     * @param value �����
     * @param dec ���������� ���� ����� �������
     * @return ����������������� ����� � ���� ������
     */
    public static String dfNumber(Double value, int dec) {
        StringBuilder pattern = new StringBuilder("###.");
        for (int i = 0; i < dec; i++) {
            pattern.append("#");
        }
        DecimalFormat myFormatter = new DecimalFormat(pattern.toString());
        return myFormatter.format(value);
    }

}
