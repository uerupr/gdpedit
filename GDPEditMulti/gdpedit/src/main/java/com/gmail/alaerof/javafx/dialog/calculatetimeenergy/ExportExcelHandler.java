package com.gmail.alaerof.javafx.dialog.calculatetimeenergy;

import com.gmail.alaerof.dao.dto.Dir;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.CalcGroup;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.DirTrainModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.CalcModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.TrainThreadModel;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableView;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class ExportExcelHandler {
    private static Logger logger = LogManager.getLogger(ExportExcelHandler.class);

    public static boolean saveEffectToExcel(File file, ResourceBundle bundle,
                                      TreeTableView<CalcModel> distrCalcTreeTable,
                                      TreeTableView<TrainThreadModel> trainThreadTable,
                                      TableView<DirTrainModel> dirTrainTable,
                                      Map<Dir, List<CalcGroup>> dirCalcMap)
    {
        boolean res = true;
        try {
            // Create a Workbook
            Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
            CreationHelper createHelper = workbook.getCreationHelper();

            // Create a Font for styling header cells
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 10);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Create Cell Style for formatting Date
            CellStyle dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

            sheetTrains(bundle, trainThreadTable, workbook, headerCellStyle);

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(file);
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();
        }catch (IOException e ){
            logger.error(e.toString(), e);
            res = false;
        }
        return res;
    }

    public static boolean saveToExcel(File file, ResourceBundle bundle,
                                      TreeTableView<CalcModel> distrCalcTreeTable,
                                      TreeTableView<TrainThreadModel> trainThreadTable,
                                      TableView<DirTrainModel> dirTrainTable,
                                      Map<Dir, List<CalcGroup>> dirCalcMap)
    {
        boolean res = true;
        try {
            // Create a Workbook
            Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
            CreationHelper createHelper = workbook.getCreationHelper();

            // Create a Font for styling header cells
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 10);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Create Cell Style for formatting Date
            CellStyle dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

            sheetDistr(bundle, distrCalcTreeTable, workbook, headerCellStyle);
            sheetTrains(bundle, trainThreadTable, workbook, headerCellStyle);
            sheetDirTrains(bundle, dirTrainTable, workbook, headerCellStyle);
            sheetDirCalc(bundle, dirCalcMap, workbook, headerCellStyle);


            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(file);
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();
        }catch (IOException e ){
            logger.error(e.toString(), e);
            res = false;
        }
        return res;
    }

    private static void sheetDirCalc(ResourceBundle bundle,
                                     Map<Dir,List<CalcGroup>> dirCalcMap,
                                     Workbook workbook,
                                     CellStyle headerCellStyle)
    {
        // Create a Sheet
        Sheet sheet = workbook.createSheet("DirGroup");
        String titleNOD = bundle.getString("dirTrainTable.nod");
        // Create a Row
        Row headerRow = sheet.createRow(0);
        List<String> columns = getTabStringDirCalcModel(bundle);

        // Create cells
        Cell cell0 = headerRow.createCell(0);
        cell0.setCellValue(titleNOD);
        cell0.setCellStyle(headerCellStyle);

        for (int col = 0; col < columns.size(); col++) {
            Cell cell = headerRow.createCell(col + 1);
            cell.setCellValue(columns.get(col));
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        for (Dir dir : dirCalcMap.keySet()) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(dir.getName());
            List<CalcGroup> list = dirCalcMap.get(dir);
            for (CalcGroup dirCalcGroup : list) {
                Row rowAll = sheet.createRow(rowNum++);
                List<Object> valuesAll = dirCalcGroup.getAllCalc().getDirTabValuesList();
                fillTableRow(rowAll, valuesAll, 1);

                Row rowOdd = sheet.createRow(rowNum++);
                List<Object> valuesOdd = dirCalcGroup.getOddCalc().getDirTabValuesList();
                fillTableRow(rowOdd, valuesOdd, 1);

                Row rowEv = sheet.createRow(rowNum++);
                List<Object> valuesEv = dirCalcGroup.getEvenCalc().getDirTabValuesList();
                fillTableRow(rowEv, valuesEv, 1);
            }
        }

        // Resize all columns to fit the content size
        for (int i = 0; i <= columns.size(); i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private static void sheetDirTrains(ResourceBundle bundle,
                                       TableView<DirTrainModel> dirTrainTable,
                                       Workbook workbook,
                                       CellStyle headerCellStyle)
    {
        // Create a Sheet
        Sheet sheet = workbook.createSheet("DirTrain");

        // Create a Row
        Row headerRow = sheet.createRow(0);
        List<String> columns = getTabStringDirTrain(bundle);
        // Create cells
        for (int i = 0; i < columns.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns.get(i));
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        for (DirTrainModel item : dirTrainTable.getItems()) {
            Row row = sheet.createRow(rowNum++);
            List<Object> values = item.getTabValuesList();
            fillTableRow(row, values, 0);
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.size(); i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private static void sheetTrains(ResourceBundle bundle,
                                    TreeTableView<TrainThreadModel> trainThreadTable,
                                    Workbook workbook,
                                    CellStyle headerCellStyle)
    {
        // Create a Sheet
        Sheet sheet = workbook.createSheet("Trains");

        // Create a Row
        Row headerRow = sheet.createRow(0);
        List<String> columns = getTabStringTrainThreadModel(bundle);
        // Create cells
        for (int i = 0; i < columns.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns.get(i));
            cell.setCellStyle(headerCellStyle);
        }

        final int expStart = 7;
        int rowNum = 1;
        TreeItem<TrainThreadModel> root = trainThreadTable.getRoot();
        for (TreeItem<TrainThreadModel> item : root.getChildren()) {
            Row row = sheet.createRow(rowNum++);
            List<Object> values = item.getValue().getTabValuesList();
            fillTableRow(row, values, 0);
            values = item.getValue().getExpenseTabValuesList();
            fillTableRow(row, values, expStart);


            for (TreeItem<TrainThreadModel> itemc : item.getChildren()) {
                Row rowC = sheet.createRow(rowNum++);
                List<Object> valuesC = itemc.getValue().getTabValuesList();
                fillTableRow(rowC, valuesC, 0);
                valuesC = itemc.getValue().getExpenseTabValuesList();
                fillTableRow(rowC, valuesC, expStart);
            }
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.size(); i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private static void sheetDistr(ResourceBundle bundle,
                                   TreeTableView<CalcModel> distrCalcTreeTable,
                                   Workbook workbook,
                                   CellStyle headerCellStyle)
    {
        // Create a Sheet
        Sheet sheet = workbook.createSheet("Distr");

        // Create a Row
        Row headerRow = sheet.createRow(0);
        List<String> columns = getTabStringDistrCalcModel(bundle);
        // Create cells
        for (int i = 0; i < columns.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns.get(i));
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        TreeItem<CalcModel> root = distrCalcTreeTable.getRoot();
        for (TreeItem<CalcModel> item : root.getChildren()) {
            Row row = sheet.createRow(rowNum++);
            List<Object> values = item.getValue().getDistrTabValuesList();
            fillTableRow(row, values, 0);
            for (TreeItem<CalcModel> itemc : item.getChildren()) {
                Row rowC = sheet.createRow(rowNum++);
                List<Object> valuesC = itemc.getValue().getDistrTabValuesList();
                fillTableRow(rowC, valuesC, 0);
            }
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.size(); i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private static void fillTableRow(Row row, List<Object> values, int expStart) {
        for (int col = 0; col < values.size(); col++) {
            if (values.get(col) instanceof Number) {
                double val = ((Number) values.get(col)).doubleValue();
                row.createCell(col+ expStart).setCellValue(val);
            } else {
                row.createCell(col+ expStart).setCellValue(values.get(col).toString());
            }
        }
    }

    public static List<String> getTabStringTrainThreadModel(ResourceBundle bundle) {
        List<String> list = new ArrayList<>();
        String title;
        title = bundle.getString("trainThreadTable.codeTR");
        list.add(title);
        title = bundle.getString("trainThreadTable.nameTR");
        list.add(title);
        title = bundle.getString("trainThreadTable.len");
        list.add(title);
        title = bundle.getString("trainThreadTable.timeWithStop");
        list.add(title);
        title = bundle.getString("trainThreadTable.timeWithoutStop");
        list.add(title);
        title = bundle.getString("calc.speedWithStop");
        list.add(title);
        title = bundle.getString("calc.speedWithoutStop");
        list.add(title);
        title = bundle.getString("calc.locTypeName");
        list.add(title);
        title = bundle.getString("trainThreadTable.timeNormMove");
        list.add(title);
        title = bundle.getString("calc.speedNorm");
        list.add(title);
        title = bundle.getString("calc.energyWithStop");
        list.add(title);
        title = bundle.getString("calc.energyWithoutStop");
        list.add(title);
        title = bundle.getString("calc.energyMove");
        list.add(title);
        title = bundle.getString("calc.stopCount");
        list.add(title);
        title = bundle.getString("calc.moveExpense");
        list.add(title);
        title = bundle.getString("calc.downUpExpense");
        list.add(title);
        title = bundle.getString("calc.stopExpense");
        list.add(title);
        title = bundle.getString("calc.normExpense");
        list.add(title);
        title = bundle.getString("calc.effect");
        list.add(title);

        return list;
    }

    public static List<String> getTabStringDistrCalcModel(ResourceBundle bundle) {
        List<String> list = new ArrayList<>();
        String title;
        title = bundle.getString("calc.Group");// groupPName
        list.add(title);
        title = bundle.getString("calc.locTypeName"); // locTypeName
        list.add(title);
        title = bundle.getString("calc.trainCount");// trainCount
        list.add(title);
        title = bundle.getString("calc.lenKM"); // lenKM
        list.add(title);
        title = bundle.getString("trainThreadTable.timeWithStop"); // timeWithoutStop
        list.add(title);
        title = bundle.getString("trainThreadTable.timeWithoutStop"); //timeWithoutStop
        list.add(title);
        title = bundle.getString("trainThreadTable.timeNormMove");//timeNormMove
        list.add(title);
        title = bundle.getString("calc.timeWithStopH"); // timeWithoutStopH
        list.add(title);
        title = bundle.getString("calc.timeWithoutStopH"); //timeWithoutStopH
        list.add(title);
        title = bundle.getString("calc.timeNormMoveH");//timeNormMoveH
        list.add(title);
        title = bundle.getString("calc.speedWithStop"); // speedWithStop
        list.add(title);
        title = bundle.getString("calc.speedWithoutStop"); // speedWithoutStop
        list.add(title);
        title = bundle.getString("calc.speedNorm"); // speedNorm
        list.add(title);
        title = bundle.getString("calc.energyWithStop"); // energyMove
        list.add(title);
        title = bundle.getString("calc.energyWithoutStop"); // energyMove
        list.add(title);
        title = bundle.getString("calc.energyMove"); // energyMove
        list.add(title);
        title = bundle.getString("calc.stopCount"); // stopCount
        list.add(title);
        title = bundle.getString("calc.moveExpense"); // moveExpense
        list.add(title);
        title = bundle.getString("calc.downUpExpense");// downUpExpense
        list.add(title);
        title = bundle.getString("calc.stopExpense"); // stopExpense
        list.add(title);
        title = bundle.getString("calc.normExpense"); // normExpense
        list.add(title);
        title =  bundle.getString("calc.effect"); // effect
        list.add(title);
        return list;
    }

    public static List<String> getTabStringDirTrain(ResourceBundle bundle) {
        List<String> list = new ArrayList<>();
        String title;
        title = bundle.getString("dirTrainTable.nod");
        list.add(title);
        title = bundle.getString("trainThreadTable.codeTR");// "CodeTR";
        list.add(title);
        title = bundle.getString("trainThreadTable.nameTR");// "NameTR";
        list.add(title);
        title = bundle.getString("trainThreadTable.len"); // "Len, m";
        list.add(title);
        title = bundle.getString("trainThreadTable.timeWithoutStop"); // "Tt, m";
        list.add(title);
        title = bundle.getString("trainThreadTable.timeWithStop"); //  "Tu, m";
        list.add(title);
        title = bundle.getString("calc.speedWithoutStop"); //  "Vt";
        list.add(title);
        title = bundle.getString("calc.speedWithStop"); //  "Vu";
        list.add(title);

        return list;
    }

    private static List<String> getTabStringDirCalcModel(ResourceBundle bundle) {
        List<String> list = new ArrayList<>();
        String title;
        title = bundle.getString("calc.Group");// groupPName
        list.add(title);
        title = bundle.getString("calc.trainCount");// trainCount
        list.add(title);
        title = bundle.getString("calc.lenKM"); // lenKM
        list.add(title);
        title = bundle.getString("trainThreadTable.timeWithStop"); // timeWithoutStop
        list.add(title);
        title = bundle.getString("trainThreadTable.timeWithoutStop"); //timeWithoutStop
        list.add(title);
        title = bundle.getString("calc.timeWithStopH"); // timeWithoutStopH
        list.add(title);
        title = bundle.getString("calc.timeWithoutStopH"); //timeWithoutStopH
        list.add(title);
        title = bundle.getString("calc.speedWithStop"); // speedWithStop
        list.add(title);
        title = bundle.getString("calc.speedWithoutStop"); // speedWithoutStop
        list.add(title);
        return list;
    }

}
