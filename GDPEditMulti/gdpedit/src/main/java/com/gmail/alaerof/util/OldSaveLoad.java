package com.gmail.alaerof.util;

import java.util.Calendar;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Helen Yrofeeva
 */
public class OldSaveLoad {
    private static Logger logger = LogManager.getLogger(OldSaveLoad.class);
    private static void runApp(String[] args) {
        Runtime r = Runtime.getRuntime();
        // Process p = null;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            sb.append(" ").append(args[i]);
        }

        String cmd = "GraphicXML2.exe" + sb.toString();
        long dt1 = Calendar.getInstance().getTimeInMillis();
        try {
            // p = r.exec(cmd);
            r.exec(cmd);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        } finally {
            //long dt2 = Calendar.getInstance().getTimeInMillis();
            //dt2 = (dt2 - dt1) / 1000;
            //GDPEdit.statusBar.setMessage("����������/�������� ��� ������� �/�� ���� " + dt2 + " ���");
        }

    }


    public static void gdpXMLSaveLoad(boolean load, boolean express) {
        GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
        if (editPanel != null) {
            String open = "0";
            if (load)
                open = "1";
            String expr = "0";
            if (express)
                expr = "1";
            String nameDistr = editPanel.getDistrEditEntity().getDistrEntity().getDistrName();
            String idDistr = "" + editPanel.getDistrEditEntity().getDistrEntity().getIdDistr();
            String[] args = { open, expr, idDistr, "\"" + nameDistr + "\"" };
            runApp(args);
            /*
            File file = chooseFile(load);
            if (file != null) {
                String fileName = file.getAbsolutePath();
                if(fileName.endsWith(".")){
                    fileName = fileName + "xml";
                }
                if(!fileName.endsWith(".xml")){
                    fileName = fileName + ".xml";
                }
                GDPEdit.lastFileChooserPath = file;
                String[] args = { "\"" + fileName + "\"", open, expr, idDistr, "\"" + nameDistr + "\"" };
                runApp(args);
            }
            */
        }
    }

}
