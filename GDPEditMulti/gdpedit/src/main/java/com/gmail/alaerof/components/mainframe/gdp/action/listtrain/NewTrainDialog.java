package com.gmail.alaerof.components.mainframe.gdp.action.listtrain;

import com.gmail.alaerof.manager.LocaleManager;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class NewTrainDialog extends JDialog {

    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTextField textFieldCode;
    private JTextField textFieldName;
    private int result;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.listtrain", LocaleManager.getLocale());
    }

    public int getResult() {
        return result;
    }

    public String getTrainCode() {
        if (textFieldCode != null) {
            return textFieldCode.getText().trim();
        } else {
            return null;
        }
    }

    public String getTrainName() {
        if (textFieldName != null) {
            return textFieldName.getText().trim();
        } else {
            return null;
        }
    }

    public void setTrainName(String name) {
        textFieldName.setText(name);
    }

    /**
     * Create the dialog.
     */
    public NewTrainDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private void initContent() {
        setBounds(100, 100, 250, 150);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                result = 0;

            }
        });

        {
            JPanel panel = new JPanel();
            contentPanel.add(panel);
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            {
                JLabel lblNewLabel = new JLabel(bundle.getString("listtrain.number"));
                panel.add(lblNewLabel);
            }
            {
                JLabel lblNewLabel = new JLabel(bundle.getString("listtrain.namefull"));
                panel.add(lblNewLabel);
            }
        }
        {
            JPanel panel = new JPanel();
            contentPanel.add(panel);
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            {
                textFieldCode = new JTextField();
                panel.add(textFieldCode);
                textFieldCode.setColumns(10);
            }
            {
                textFieldName = new JTextField();
                panel.add(textFieldName);
                textFieldName.setColumns(10);
            }
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog dialog = this;
            {
                JButton okButton = new JButton(bundle.getString("listtrain.apply"));
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        result = 1;
                        dialog.setVisible(false);
                    }
                });
            }
            {
                JButton cancelButton = new JButton(bundle.getString("listtrain.cancel"));
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        result = 0;
                        dialog.setVisible(false);
                    }
                });
            }
        }
    }
}
