package com.gmail.alaerof.gdpcalculation;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.entity.train.TimeStation;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.TimeConverter;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CalculateOverTime {
    protected static Logger logger = LogManager.getLogger(CalculateOverTime.class);
    // ������� �� ������:
    private static final String WARN_NO_SPAN;
    // '�������' ����� �� ������� � ������:
    private static final String WARN_NO_TIME;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("bundles.gdpcalculation", LocaleManager.getLocale());
        WARN_NO_SPAN = bundle.getString("gdpcalculation.ERR_NO_SPAN");
        WARN_NO_TIME = bundle.getString("gdpcalculation.ERR_NO_TIME");
    }

    public static String calculateOverTimeAllTrains(List<TrainThread> listTrain) {
        StringBuilder spanError = new StringBuilder("");
        for (int i = 0; i < listTrain.size(); i++) {
            String report = calculateOverTimeOneTrain(listTrain.get(i));
            if (!report.isEmpty()) {
                spanError.append(report).append("\n");
            }
        }
        return spanError.toString();
    }

    public static String calculateOverTimeOneTrain(TrainThread trainThread) {
        StringBuilder report = new StringBuilder();
        DistrEntity distrEntity = trainThread.getDistrEntity();
        DistrTrain distrTrain = trainThread.getDistrTrain();
        List<LocomotiveType> locs = distrEntity.getListDistrLoc();
        LocomotiveType loc = CalculateUtil.defineLocomotiveType(locs, distrTrain);
        List<TimeStation> timeStationList = trainThread.getListTimeSt();
        int idFirstStation = 0;
        for (int i = 0; i < timeStationList.size() - 1; i++) {
            TimeStation timeStation1 = timeStationList.get(i);
            TimeStation timeStation2 = timeStationList.get(i + 1);
            DistrStation distrStation1 = timeStation1.getDistrStation();
            DistrStation distrStation2 = timeStation2.getDistrStation();
            Station station1 = distrStation1.getStation();
            Station station2 = distrStation2.getStation();
            int idSt1 = station1.getIdSt();
            if (i == 0){
                idFirstStation = idSt1;
            }
            int idSt2 = station2.getIdSt();
            DistrSpan distrSpan = distrEntity.getDistrSpan(idSt1, idSt2);
            boolean timeStopUp = false;
            boolean timeStopDown = false;
            if ((i == 0) || (TimeConverter.minutesBetween(timeStation1.getMOn(), timeStation1.getMOff()) > 0)) {
                timeStopUp = true;
            }
            if ((i == timeStationList.size() - 2) || (TimeConverter.minutesBetween(timeStation2.getMOn(), timeStation2.getMOff()) > 0)) {
                timeStopDown = true;
            }
            int timeMoveReal = TimeConverter.minutesBetween(timeStation1.getMOff(), timeStation2.getMOn());
            float timeMoveNormative = 0;
            if (distrSpan != null) {
                Time time = CalculateUtil.getTrainTime(distrSpan, loc, distrTrain, true);
                if (time == null || time.getTimeMove() == 0) {
                    String warn = WARN_NO_TIME + " 0 : " + trainThread.getCode() + "(" + trainThread.getNameTR() + ") " +
                            loc.getLtype() + " " +
                            station1.getName() + " - " + station2.getName();
                    report = report.append(warn);
                    logger.debug(warn);
                }
                if (time != null) {
                    timeMoveNormative = time.getTimeMove();
                    // ��� ����������� ������� � ���������� �������� � tMove (����� ���� �� ��������)
                    if (TrainType.town != distrTrain.getTypeTR() && timeStopUp) {
                        timeMoveNormative = timeMoveNormative + time.getTimeUp();
                    }
                    if (TrainType.town != distrTrain.getTypeTR() && timeStopDown) {
                        timeMoveNormative = timeMoveNormative + time.getTimeDown();
                    }
                }
            } else {
                String warn = WARN_NO_SPAN + " " + trainThread.getCode() + "(" + trainThread.getNameTR() + ") " +
                        station1.getName() + " - " + station2.getName();
                report = report.append(warn);
                logger.debug(warn);
            }
            int overTime = timeMoveReal - (int) Math.ceil(timeMoveNormative);
            distrTrain.getMapLineStTr().get(idSt2).setOverTime(overTime);
        }
        if (idFirstStation > 0) {
            distrTrain.getMapLineStTr().get(idFirstStation).setOverTime(0);
        }
        return report.toString();
    }
}
