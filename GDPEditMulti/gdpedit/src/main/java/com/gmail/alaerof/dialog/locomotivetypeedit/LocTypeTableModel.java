package com.gmail.alaerof.dialog.locomotivetypeedit;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.manager.LocaleManager;

public class LocTypeTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("locomotivetypeedit.name_"),
            bundle.getString("locomotivetypeedit.color") };
    private List<LocomotiveType> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.locomotivetypeedit", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else
            return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            LocomotiveType el = list.get(row);
            switch (col) {
            case 0:
                return el.getLtype();
            case 1:
                return el.getColorLoc();
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (list != null) {
            LocomotiveType el = list.get(row);
            if (el.isEditable() && col == 0) {
                // ������������� ����� ���� ����� ������ ��������
                return true;
            }
        }
        return false;
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if (col == 0) {
            return String.class;
        }
        return Object.class;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null && row < getRowCount()) {
            LocomotiveType el = list.get(row);
            el.setLtype(aValue.toString());
        }
    }

    public List<LocomotiveType> getList() {
        return list;
    }

    public void setList(List<LocomotiveType> list) {
        this.list = list;
    }

}
