package com.gmail.alaerof.javafx.dialog.energyfromtr;

import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.energyfromtr.view.ImportEnergyFromTRLayoutController;
import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

public class ImportEnergyFromTRFXDialog extends SwingFXDialogBase {
    private DistrEditEntity distrEditEntity;
    private ImportEnergyFromTRLayoutController controller;
    public ImportEnergyFromTRFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/importenergyfromtr/view/ImportEnergyFromTRLayout.fxml";
        resourceBundleBaseName = "bundles.ImportEnergyFromTRFXDialog";
    }
    public DistrEditEntity getDistrEntity() {
        return distrEditEntity;
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 1100, 600};
        return bounds;
    }

    public void setDialogContent(DistrEditEntity distrEditEntity) {
        this.distrEditEntity = distrEditEntity;

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(distrEditEntity);
            }
        });
    }
}
