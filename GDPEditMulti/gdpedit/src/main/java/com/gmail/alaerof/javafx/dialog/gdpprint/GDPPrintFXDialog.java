package com.gmail.alaerof.javafx.dialog.gdpprint;

import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.gdpprint.view.GDPPrintLayoutController;

import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

/**
 * FX ����� ������ ���
 */
public class GDPPrintFXDialog extends SwingFXDialogBase {
    private DistrEditEntity distrEntity;
    private GDPPrintLayoutController controller;

    /**
     * Create the dialog.
     */
    public GDPPrintFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/gdpprint/view/GDPPrintLayout.fxml";
        resourceBundleBaseName = "bundles.GDPPrintFXDialog";
    }

    public DistrEditEntity getDistrEntity() {
        return distrEntity;
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {100, 100, 420, 600};
        return bounds;
    }

    public void setDialogContent(final GDPEditPanel editPanel) {
        this.distrEntity = editPanel.getDistrEditEntity();

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(distrEntity, editPanel);
            }
        });
    }
}
