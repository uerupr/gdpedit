package com.gmail.alaerof.xml;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainShowParam;
import com.gmail.alaerof.dialog.linesttrain.model.LST;
import com.gmail.alaerof.dialog.linesttrain.model.LSTCombST;
import com.gmail.alaerof.dialog.linesttrain.model.TRCombST;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.DataXMLUtil;
import com.gmail.alaerof.util.XMLUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import java.util.ResourceBundle;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javax.swing.JOptionPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.entity.DistrLineSt;
import com.gmail.alaerof.entity.train.TimeStation;
import com.gmail.alaerof.entity.train.TrainStDouble;
import com.gmail.alaerof.entity.train.TrainThread;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Helen Yrofeeva
 */
public class LineTRXMLHandler {
    private static Logger logger = LogManager.getLogger(LineTRXMLHandler.class);
    protected ResourceBundle bundle = ResourceBundle.getBundle("bundles.LineTRXMLHandler", LocaleManager.getLocale());
    private List<Distr> listDistr;

    public boolean saveLineTRxml(File file, List<TrainThread> listTr, List<TrainStDouble> listTrComb,
            int idSt, String nameSt) {
        Document doc = com.gmail.alaerof.util.DataXMLUtil.getNewDocument();
        Element rootElement = doc.createElement("LineSTTR");
        rootElement.setAttribute("IDst", "" + idSt);
        rootElement.setAttribute("nameSt", nameSt);
        doc.appendChild(rootElement);
        Element lsts = doc.createElement("LSTs");
        rootElement.appendChild(lsts);
        for (TrainThread tr : listTr) {
            TimeStation trTime = tr.getTimeSt(idSt);
            if (trTime != null) {
                Element lst = doc.createElement("LST");
                lsts.appendChild(lst);
                DistrTrain dtr = tr.getDistrTrain();
                LineStTrain lstTR = trTime.getLineStTrain();
                String tOn = "";
                if (!trTime.isNullOn())
                    tOn = lstTR.getOccupyOn();
                String tOff = "";
                if (!trTime.isNullOff())
                    tOff = lstTR.getOccupyOn();
                lst.setAttribute("IDtrain", "" + dtr.getIdTrain());
                lst.setAttribute("CodeTR", dtr.getCodeTR());
                lst.setAttribute("NameTR", dtr.getNameTR());
                lst.setAttribute("tOn", tOn);
                lst.setAttribute("tOff", tOff);
                lst.setAttribute("tTex", "" + lstTR.getTexStop());
                int idLineSt = lstTR.getIdLineSt();
                lst.setAttribute("IDlineSt", "" + idLineSt);
                DistrLineSt line = trTime.getLine(idLineSt);
                String n = "";
                String tra = "";
                String colorLn = "";
                if (line != null) {
                    n = "" + line.getLineSt().getN();
                    tra = "" + line.getLineSt().getPs();
                    // ��������������� ���� �� ����� !!!
                    // ColorConverter.convert(clr);
                    colorLn = "" + line.getLineSt().getColorLn();
                }
                lst.setAttribute("N", n);
                lst.setAttribute("TRA", tra);
                lst.setAttribute("colorTR", "" + dtr.getShowParam().getColorTR());
                lst.setAttribute("LineStyle", "" + tr.getLineStyle().getLineStyleCode());
                lst.setAttribute("widthTR", "" + dtr.getShowParam().getWidthTR());
                lst.setAttribute("colorLn", colorLn);
            }
        }
        fillListDistr();
        Element comb = doc.createElement("TRCombSTs");
        rootElement.appendChild(comb);
        for (TrainStDouble trComb : listTrComb) {
            TrainThread trOn = trComb.getTrainThreadOn();
            TrainThread trOff = trComb.getTrainThreadOff();
            Element lst = doc.createElement("TRCombST");
            comb.appendChild(lst);
            lst.setAttribute("IDtrainOn", "" + trOn.getDistrTrain().getIdTrain());
            lst.setAttribute("IDtrainOff", "" + trOff.getDistrTrain().getIdTrain());
            lst.setAttribute("IDdistrOn", "" + trOn.getDistrTrain().getIdDistr());
            lst.setAttribute("IDdistrOff", "" + trOff.getDistrTrain().getIdDistr());
            lst.setAttribute("TrNameOn", "" + trOn.getDistrTrain().getNameTR());
            lst.setAttribute("TrCodeOn", "" + trOn.getDistrTrain().getCodeTR());
            lst.setAttribute("TrNameOff", "" + trOff.getDistrTrain().getNameTR());
            lst.setAttribute("TrCodeOff", "" + trOff.getDistrTrain().getCodeTR());
            lst.setAttribute("DistrNameOn", "" + getDistrName(trOn.getDistrTrain().getIdDistr()));
            lst.setAttribute("DistrNameOff", "" + getDistrName(trOff.getDistrTrain().getIdDistr()));
            lst.setAttribute("colorTR", "" + trOn.getDistrTrain().getShowParam().getColorTR());
            lst.setAttribute("LineStyle", "" + trOn.getLineStyle().getLineStyleCode());
            lst.setAttribute("widthTR", "" + trOn.getDistrTrain().getShowParam().getWidthTR());
        }
        clearListDistr();
        return com.gmail.alaerof.util.DataXMLUtil.saveDocument(file, doc);
    }

    public LSTCombST loadLineTRxml(File file, int idSt, String nameSt){
        LSTCombST lSTCombST = new LSTCombST();
        List<LST> listLST = new ArrayList<>();
        List<TRCombST> listTRCombST = new ArrayList<>();
        try {
            Document doc = DataXMLUtil.loadDocument(file);
            NodeList nodeList;
            Element elITRM;
            Element root = doc.getDocumentElement();
            int idStXML = XMLUtil.getAttributeInt(root,"IDst");
            String nameStXML = XMLUtil.getAttribute(root,"nameSt");
            String titleMain = bundle.getString("xml.linetrxmlhandler.attention");
            String titleMiddle = "";
            String titleSmall = bundle.getString("xml.linetrxmlhandler.doload");
            boolean dopusk = true;
            if (idSt != idStXML || !nameSt.equals(nameStXML)) {
                dopusk = false;
            }
            if (idSt != idStXML) {
                titleMiddle = bundle.getString("xml.linetrxmlhandler.nocode");
            }
            if (!nameSt.equals(nameStXML)) {
                titleMiddle = bundle.getString("xml.linetrxmlhandler.noname");
            }
            if (idSt != idStXML && !nameSt.equals(nameStXML)) {
                titleMiddle = bundle.getString("xml.linetrxmlhandler.nocodename");
            }
            if (!dopusk) {
                if (JOptionPane.showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), titleMiddle + "\n" + titleSmall, titleMain, JOptionPane.YES_NO_OPTION) == 0) {
                    dopusk = true;
                }
            }
            if (dopusk) {
                nodeList = root.getElementsByTagName("LST");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    elITRM = (Element) nodeList.item(i);
                    LST lST = new LST();
                    lST.setIdTrain(XMLUtil.getAttributeLong(elITRM,"IDtrain"));
                    lST.setCodeTR(XMLUtil.getAttribute(elITRM,"CodeTR"));
                    lST.setNameTR(XMLUtil.getAttribute(elITRM,"NameTR"));
                    lST.settOn(XMLUtil.getAttribute(elITRM,"tOn"));
                    lST.settOff(XMLUtil.getAttribute(elITRM,"tOff"));
                    lST.settTex(XMLUtil.getAttributeInt(elITRM,"tTex"));
                    lST.setIDlineSt(XMLUtil.getAttributeInt(elITRM,"IDlineSt"));
                    lST.setN(XMLUtil.getAttributeInt(elITRM,"N"));
                    lST.setTRA(XMLUtil.getAttribute(elITRM,"TRA"));
                    lST.setColorLn(XMLUtil.getAttributeInt(elITRM,"colorLn"));
                    lST.setColorTR(XMLUtil.getAttributeInt(elITRM,"colorTR"));
                    lST.setLineStyle(XMLUtil.getAttributeInt(elITRM,"LineStyle"));
                    lST.setWidthTR(XMLUtil.getAttributeInt(elITRM,"widthTR"));
                    listLST.add(lST);
                }
                nodeList = root.getElementsByTagName("TRCombST");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    elITRM = (Element) nodeList.item(i);
                    TRCombST tRCombST = new TRCombST();
                    tRCombST.setIdTrainOn(XMLUtil.getAttributeLong(elITRM,"IDtrainOn"));
                    tRCombST.setIdDistrOn(XMLUtil.getAttributeInt(elITRM,"IDdistrOn"));
                    tRCombST.setTrNameOn(XMLUtil.getAttribute(elITRM,"TrNameOn"));
                    tRCombST.setTrCodeOn(XMLUtil.getAttribute(elITRM,"TrCodeOn"));
                    tRCombST.setColorTR(XMLUtil.getAttributeInt(elITRM,"colorTR"));
                    tRCombST.setLineStyle(XMLUtil.getAttributeInt(elITRM,"LineStyle"));
                    tRCombST.setWidthTR(XMLUtil.getAttributeInt(elITRM,"widthTR"));
                    tRCombST.setIdTrainOff(XMLUtil.getAttributeLong(elITRM,"IDtrainOff"));
                    tRCombST.setIdDistrOff(XMLUtil.getAttributeInt(elITRM,"IDdistrOff"));
                    tRCombST.setTrNameOff(XMLUtil.getAttribute(elITRM,"TrNameOff"));
                    tRCombST.setTrCodeOff(XMLUtil.getAttribute(elITRM,"TrCodeOff"));
                    listTRCombST.add(tRCombST);
                }
                lSTCombST.setListLST(listLST);
                lSTCombST.setListTRCombST(listTRCombST);
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return lSTCombST;
    }

    private void clearListDistr() {
        listDistr = null;
    }

    private String getDistrName(int idDistr) {
        if (listDistr != null) {
            for(Distr distr: listDistr){
                if(distr.getIdDistr() == idDistr){
                    return distr.getName().trim();
                }
            }
        }
        return "";
    }

    private void fillListDistr() {
        IDistrDAO distrDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO();
        try {
            listDistr = distrDAO.getDistrList(0);
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public static List<TrainStDouble> LoadLineTRFromXML(File file) {
        return null;
    }
}
