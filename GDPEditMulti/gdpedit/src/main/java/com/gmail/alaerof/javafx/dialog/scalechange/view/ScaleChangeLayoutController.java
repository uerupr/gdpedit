package com.gmail.alaerof.javafx.dialog.scalechange.view;

import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IScaleDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.scalechange.model.ScaleChangeModel;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import java.util.ResourceBundle;

public class ScaleChangeLayoutController implements Initializable {
    protected Logger logger = LogManager.getLogger(ScaleChangeLayoutController.class);
    protected ResourceBundle bundle;
    private DistrEditEntity distrEditEntity;
    private Scene scene;

    /** ������� ������������ �������� */
    private TableView<ScaleChangeModel> scaleChangeTable;
    /** ���� ������������ �������� */
    private List<ScaleChangeModel> scaleChangeList = new ArrayList<>();

    @FXML
    private BorderPane rootPane;

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        rootPane.autosize();
        // scalechange table
        EditableTableViewCreator<ScaleChangeModel> builderImp = new EditableTableViewCreator<>();
        scaleChangeTable = builderImp.createEditableTableView();
        scaleChangeTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        rootPane.setCenter(scaleChangeTable);
        rootPane.getCenter().autosize();
        String title;
        title = bundle.getString("javafx.dialog.scalechange.num");
        scaleChangeTable.getColumns().add(builderImp.createIntegerColumn(title, ScaleChangeModel::numProperty, false));
        scaleChangeTable.getColumns().get(0).setPrefWidth(30);
        scaleChangeTable.getColumns().get(0).setSortable(false);
        title = bundle.getString("javafx.dialog.scalechange.distrspan");
        scaleChangeTable.getColumns().add(builderImp.createStringColumn(title, ScaleChangeModel::nameDistrCombProperty, false));
        scaleChangeTable.getColumns().get(1).setPrefWidth(300);
        scaleChangeTable.getColumns().get(1).setSortable(false);
        title = bundle.getString("javafx.dialog.scalechange.fst");
        scaleChangeTable.getColumns().add(builderImp.createStringColumn(title, ScaleChangeModel::fStProperty, false));
        scaleChangeTable.getColumns().get(2).setPrefWidth(40);
        scaleChangeTable.getColumns().get(2).setSortable(false);
        title = bundle.getString("javafx.dialog.scalechange.lst");
        scaleChangeTable.getColumns().add(builderImp.createStringColumn(title, ScaleChangeModel::lStProperty, false));
        scaleChangeTable.getColumns().get(3).setPrefWidth(40);
        scaleChangeTable.getColumns().get(3).setSortable(false);
        title = bundle.getString("javafx.dialog.scalechange.scale");
        scaleChangeTable.getColumns().add(builderImp.createIntegerColumn(title, ScaleChangeModel::scaleProperty, true));
        scaleChangeTable.getColumns().get(4).setPrefWidth(80);
        scaleChangeTable.getColumns().get(4).setSortable(false);

        scaleChangeTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    @SuppressWarnings("rawtypes")
                    TablePosition pos = scaleChangeTable.getSelectionModel().getSelectedCells().get(0);
                    int row = pos.getRow();
                    int col = pos.getColumn();
                    if (col == 2) {
                        ScaleChangeModel scaleChangeModel = scaleChangeTable.getItems().get(row);
                        Boolean fSt = !scaleChangeModel.isFSt();
                        scaleChangeList.get(row).setFSt(fSt);
                        scaleChangeTable.getItems().get(row).setFSt(fSt);
                    }
                    if (col == 3) {
                        ScaleChangeModel scaleChangeModel = scaleChangeTable.getItems().get(row);
                        Boolean lSt = !scaleChangeModel.isLSt();
                        scaleChangeList.get(row).setLSt(lSt);
                        scaleChangeTable.getItems().get(row).setLSt(lSt);
                    }
                }
            }
        });
    }

    @FXML
    private void saveToDB (ActionEvent event) {
        logger.debug("saveToDB");
        Alert alert;
        String titleMain = bundle.getString("javafx.dialog.scalechange.dialog.attention");
        String titleMiddle = bundle.getString("javafx.dialog.scalechange.dialog.save");
        String titleSmall = bundle.getString("javafx.dialog.scalechange.dialog.good");
        try {

            IScaleDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getScaleDAO();
            List<Scale> itemList = new ArrayList<>();
            for (ScaleChangeModel aScaleChangeList : scaleChangeList) {
                Scale scale = new Scale();
                scale.setIdScale(aScaleChangeList.getIDScale());
                scale.setfSt(aScaleChangeList.isFSt());
                scale.setlSt(aScaleChangeList.isLSt());
                scale.setScale(aScaleChangeList.scaleProperty().get());
                itemList.add(scale);
            }
            dao.updateScale(itemList);
            //distrEditEntity.getDistrEntity().reloadFromDB();
        } catch (Exception e) {
            logger.error(e.toString(), e);
            titleSmall = bundle.getString("javafx.dialog.scalechange.dialog.bad");
        } finally {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            alert.showAndWait();
        }
    }

    public void setContent(DistrEditEntity distrEditEntity) {
        scene.setCursor(Cursor.WAIT);
        try {
            this.distrEditEntity = distrEditEntity;

            // scaleChangeList -> scaleChangeTabel
            scaleChangeList = buildDistrCombListFromDistr(distrEditEntity.getIdDistr());
            scaleChangeTable.setItems(FXCollections.observableArrayList(scaleChangeList));

        } catch (DataManageException | ObjectNotFoundException e) {
            e.printStackTrace();
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    private List<ScaleChangeModel> buildDistrCombListFromDistr(int idDistr) throws DataManageException, ObjectNotFoundException {
        List<ScaleChangeModel> itemList = new ArrayList<>();
        IScaleDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getScaleDAO();
        List<Scale> listScale = dao.getListScale(idDistr);
        for (Scale aListScale : listScale) {
            ScaleChangeModel scaleChangeModel = new ScaleChangeModel(aListScale);
            itemList.add(scaleChangeModel);
        }
        return itemList;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
