package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.groupp.GroupRange;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class GroupRangeModel {
    private GroupRange groupRange;
    private IntegerProperty minCode;
    private IntegerProperty maxCode;

    public GroupRangeModel(GroupRange groupRange) {
        this.groupRange = groupRange;
        this.minCode = new SimpleIntegerProperty(groupRange.getMinCode());
        this.maxCode = new SimpleIntegerProperty(groupRange.getMaxCode());
    }

    public int getMinCode() {
        return minCode.get();
    }

    public IntegerProperty minCodeProperty() {
        return minCode;
    }

    public void setMinCode(int minCode) {
        this.minCode.set(minCode);
        this.groupRange.setMinCode(minCode);
    }

    public int getMaxCode() {
        return maxCode.get();
    }

    public IntegerProperty maxCodeProperty() {
        return maxCode;
    }

    public void setMaxCode(int maxCode) {
        this.maxCode.set(maxCode);
        this.groupRange.setMaxCode(maxCode);
    }

    public GroupRange getGroupRange() {
        return groupRange;
    }

    public static List<GroupRange> extractGroupRange(List<GroupRangeModel> selected) {
        List<GroupRange> list = new ArrayList<>();
        for(GroupRangeModel rangeModel: selected){
            list.add(rangeModel.groupRange);
        }
        return list;
    }

    @Override
    public String toString() {
        return "GroupRangeModel{" +
                "groupRange=" + groupRange +
                '}';
    }
}
