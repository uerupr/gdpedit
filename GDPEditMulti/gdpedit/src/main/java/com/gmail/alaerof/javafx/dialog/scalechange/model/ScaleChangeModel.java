package com.gmail.alaerof.javafx.dialog.scalechange.model;

import com.gmail.alaerof.dao.dto.Scale;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ScaleChangeModel {
    //num
    private IntegerProperty num;
    // Name
    private StringProperty nameDistrComb;
    // fSt
    private boolean fSt;
    private StringProperty fStStr;
    // lSt
    private boolean lSt;
    private StringProperty lStStr;
    // Scale
    private IntegerProperty scale;
    // IDscale
    private int idScale;

    public ScaleChangeModel(Scale scale) {
        this.nameDistrComb = new SimpleStringProperty(scale.getName());
        this.num = new SimpleIntegerProperty(scale.getNum());
        this.fStStr = new SimpleStringProperty("-");
        this.lStStr = new SimpleStringProperty("-");
        this.fSt = scale.isfSt();
        if (fSt) {
            this.fStStr.set("*");
        }
        this.lSt = scale.islSt();
        if (lSt) {
            this.lStStr.set("*");
        }
        this.scale = new SimpleIntegerProperty(scale.getScale());
        this.idScale = scale.getIdScale();
    }
    public StringProperty nameDistrCombProperty() {
        return nameDistrComb;
    }

    public StringProperty fStProperty() {
        return fStStr;
    }

    public StringProperty lStProperty() {
        return lStStr;
    }

    public IntegerProperty numProperty() {
        return num;
    }

    public IntegerProperty scaleProperty() {
        return scale;
    }

    public int getIDScale() {return idScale;}

    public boolean isFSt() {return fSt;}

    public boolean isLSt() {return lSt;}

    public void setFSt(boolean fSt) {
        this.fSt = fSt;
        this.fStStr.set("-");
        if (fSt) {
            this.fStStr.set("*");
        }
    }

    public void setLSt(boolean lSt) {
        this.lSt = lSt;
        this.lStStr.set("-");
        if (lSt) {
            this.lStStr.set("*");
        }
    }

    @Override
    public String toString() {
        return nameDistrComb.get();
    }
}
