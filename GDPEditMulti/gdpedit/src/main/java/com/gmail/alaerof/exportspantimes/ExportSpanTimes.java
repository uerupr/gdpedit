package com.gmail.alaerof.exportspantimes;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.util.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import javafx.stage.FileChooser;

import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrSpan;

public class ExportSpanTimes {
    protected static Logger logger = LogManager.getLogger(ExportSpanTimes.class);

    public static void processExport(DistrEntity de) {
        String fileName = de.getDistrName() + " ���������";
        File file = FileUtil.chooseFile(FileUtil.Mode.save,null, fileName, GDPEdit.gdpEdit.getMainFrame(),
                new FileNameExtensionFilter("MSExcel files", "xls","xlsx"));
        if (file != null) {
            try {
                String sampleFileName = "samples//SpanTimeSample.xls";
                String filePath = file.getAbsolutePath();
                String ff = FilenameUtils.removeExtension(filePath) + ".xls";
                file = new File(ff);

                FileInputStream input = new FileInputStream(new File(sampleFileName));
                Workbook wb = WorkbookFactory.create(input);
                try {
                    Sheet sheet = wb.getSheetAt(0);
                    int rowIndx = 0;
                    Row row = sheet.getRow(rowIndx);
                    row.getCell(0).setCellValue(de.getDistrName());

                    int locNameRow = 1;
                    int beginRow = 8;
                    int beginTimeCol = 3;

                    List<LocomotiveType> listLoc = de.getListDistrLoc();
                    List<DistrSpan> listSpan = de.getListSp();
                    // ------ ������ ��������� ����������� � ���������� --------
                    rowIndx = beginRow;
                    for (DistrSpan ds : listSpan) {
                        ds.reloadSpanTime(listLoc); // �������� ��������� �� ��
                        row = getRow(sheet, rowIndx);
                        getCell(row, 0).setCellValue(ds.getSpanScale().getName());
                        getCell(row, 1).setCellValue(ds.getSpanScale().getAbsKM());
                        getCell(row, 2).setCellValue((((double) ds.getSpanScale().getL()) / 1000));
                        rowIndx++;
                    }

                    // ------ ��������� ��� ������� ���������� (��� ��������)--------
                    for (LocomotiveType loc : listLoc) {
                        // ----- �������� ���������� (��� ��������)--------
                        row = sheet.getRow(locNameRow);
                        row.getCell(beginTimeCol).setCellValue(loc.getLtype());
                        // -----------------------------------------------
                        rowIndx = beginRow;
                        for (DistrSpan ds : listSpan) {
                            row = getRow(sheet, rowIndx);
                            Time odd = ds.getSpanTime(loc, OE.odd);
                            Time even = ds.getSpanTime(loc, OE.even);
                            if (odd != null) {
                                getCell(row, beginTimeCol).setCellValue(odd.getTimeUp());
                                getCell(row, beginTimeCol + 1).setCellValue(odd.getTimeMove());
                                if (odd.getTimeTw() > 0) {
                                    getCell(row, beginTimeCol + 2).setCellValue(odd.getTimeTw());
                                }
                                getCell(row, beginTimeCol + 3).setCellValue(odd.getTimeDown());
                            }
                            if (even != null) {
                                getCell(row, beginTimeCol + 4).setCellValue(even.getTimeUp());
                                getCell(row, beginTimeCol + 5).setCellValue(even.getTimeMove());
                                if (even.getTimeTw() > 0) {
                                    getCell(row, beginTimeCol + 6).setCellValue(even.getTimeTw());
                                }
                                getCell(row, beginTimeCol + 7).setCellValue(even.getTimeDown());
                            }
                            rowIndx++;
                        }
                        beginTimeCol += 8;
                    }
                } finally {
                    FileOutputStream fileOut = new FileOutputStream(file);
                    wb.write(fileOut);
                    fileOut.close();
                }
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
    }

    private static Row getRow(Sheet sheet, int indx) {
        Row row = sheet.getRow(indx);
        if (row == null) {
            row = sheet.createRow(indx);
        }
        return row;
    }

    private static Cell getCell(Row row, int indx) {
        Cell cell = row.getCell(indx);
        if (cell == null) {
            cell = row.createCell(indx);
        }
        return cell;
    }
}
