package com.gmail.alaerof.components.mainframe.gdp.action.listspan;

import java.util.Comparator;

import com.gmail.alaerof.entity.DistrSpan;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ComparatorSpanByNum implements Comparator<DistrSpan>{

    @Override
    public int compare(DistrSpan dsp1, DistrSpan dsp2) {
        return dsp1.getSpanScale().getNum() - dsp2.getSpanScale().getNum();
    }

}
