package com.gmail.alaerof.components.menu;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dialog.config.ApplicationConfig;
import com.gmail.alaerof.dialog.config.ApplicationConfigDialog;
import com.gmail.alaerof.dialog.config.ApplicationConfigHandler;
import com.gmail.alaerof.dialog.config.GDPGridConfigDialog;
import com.gmail.alaerof.dialog.locomotivetypeedit.LocomotiveTypeEditDialog;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.javafx.dialog.scalechange.ScaleChangeFXDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 * ���� "���������"
 */
public class MenuViewConfig extends LoggerMenu {
    public MenuViewConfig(){
        super();
        String title = bundle.getString("components.menu.ViewConfig");
        this.setText(title);
        {
            title = bundle.getString("components.menu.viewconfig.Application");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    showApplicationConfigDialog();
                }
            });
        }
        {
            title = bundle.getString("components.menu.viewconfig.GDPPropertys");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    showGDPGridConfigDialog();
                }
            });
        }

        {   // ������� ���������
            title = bundle.getString("components.menu.distr.ScaleChange");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);

            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    scaleChange();
                }
            });
        }
    }

    private void showGDPGridConfigDialog() {
        try {
            GDPGridConfig conf = null;
            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
            if (dee == null) {
                dee = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            GDPGridConfigDialog gdpGridConfigEdit = GDPEdit.gdpEdit.getDialogManager().getGdpGridConfigEditDialog();
            if (dee != null) {
                DistrEntity de = dee.getDistrEntity();
                conf = de.getConfigGDP();
                gdpGridConfigEdit.fillFrameByConfig(conf);
                gdpGridConfigEdit.setTitle(bundle.getString("components.menu.viewconfig.GDPPropertys")
                        + ": " + dee.getDistrEntity().getDistrName());
                gdpGridConfigEdit.setListLoc(de.getListDistrLoc());
                gdpGridConfigEdit.setListSt(de.getListSt());
                gdpGridConfigEdit.setVisible(true);

                if (gdpGridConfigEdit.getResult() > 0) {
                    gdpGridConfigEdit.fillConfigByFrame(conf);
                    dee.getDistrEntity().saveGDPGridConfig();
                }
            } else {
                gdpGridConfigEdit.setTitle(bundle.getString("components.menu.viewconfig.GDPPropertys"));
                gdpGridConfigEdit.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void showApplicationConfigDialog() {
        try {
            ApplicationConfigDialog applicationConfigDialog = GDPEdit.gdpEdit.getDialogManager().getApplicationConfigDialog();
            ApplicationConfig applicationConfig = GDPEdit.gdpEdit.getApplicationConfig();
            applicationConfigDialog.fillFrameByConfig(applicationConfig);
            applicationConfigDialog.setVisible(true);
            if (applicationConfigDialog.getResult() > 0) {
                applicationConfigDialog.fillConfigByFrame(applicationConfig);
                ApplicationConfigHandler.save(applicationConfig);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void scaleChange() {
        try {
            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
            if (dee == null) {
                dee = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            if (dee != null) {
                ScaleChangeFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getScaleChangeFXDialog(
                        dee.getDistrEntity().getDistrName());
                dialog.setModal(true);
                dialog.setDialogContent(dee);
                dialog.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
