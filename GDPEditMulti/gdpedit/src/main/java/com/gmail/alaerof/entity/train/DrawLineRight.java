package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;

public class DrawLineRight extends DrawLine {

    public DrawLineRight(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams) {
        super(generalParams, selfParams);
        direction = NewLine.Direction.right;
    }

    @Override
    protected void calculateAllPoints() {
        int npoints = 10;
        int x[] = new int[npoints];
        int y[] = new int[npoints];
        int maxW = getSize().width; // ������ � ���������
        int maxH = getSize().height; // ������ � ���������
        int w = getSize().width; // ������ ��� ��������
        int h = Math.max(1,getSize().height); // ������ ��� ��������
        double gg = Math.max(1, Math.sqrt(w * w + h * h));// ����������
        //����� ���������� ���� ������� �����������
        int timeShiftL = -13;
        //����� ���������� ���� ������� ��������
        int timeShiftA = 6;
        int indentX = 0; //this.getIndent().width;
        x[0] = indentX; // ������ ���
        y[0] = maxH; // ������ ���

        x[1] = x[0] - indentX;
        y[1] = y[0];

        int step = generalParams.codeStepE;
        if (step + getCaptionSize().width > gg) {
            step = 15;
        }
        double stepX = step * w / h;
        x[2] = (int) (x[1] + stepX);
        y[2] = y[1] - step;

        double chx = 0;
        double chy = 0;
        if (isCaption()) {
            chx = getCaptionSize().getHeight() * h / gg;
            chy = getCaptionSize().getHeight() * w / gg;
        }
        x[3] = (int) (x[2] - chx);
        y[3] = (int) (y[2] - chy);

        double cwx = getCaptionSize().getWidth() * w / gg;
        double cwy = getCaptionSize().getWidth() * h / gg;
        x[4] = (int) (x[3] + cwx);
        y[4] = (int) (y[3] - cwy);

        x[5] = (int) (x[4] + chx);
        y[5] = (int) (y[4] + chy);

        x[6] = maxW - indentX * 2;
        y[6] = 0;

        x[7] = x[6] + indentX; // ����� ���
        y[7] = y[6]; // ����� ���

        x[8] = x[7] + indentX;
        y[8] = y[6];

        x[9] = x[0] + indentX;
        y[9] = y[0];

        alphaCaption = -Math.asin(h / gg);

        captionPointGDP.x = (int) (x[0] + stepX) - indentX;
        captionPointGDP.y = y[0] - step - 1 - maxH + h;
        beginGDP.x = x[0] - indentX;
        beginGDP.y = y[0]- maxH + h;
        endGDP.x = x[7] - indentX;
        endGDP.y = y[7]- maxH + h;

        pointTimeLGDP.x = getTimeSize().width - indentX + timeShiftL;
        pointTimeLGDP.y = y[1] - 3 - (int) (getTimeSize().height / 2)- maxH + h;
        pointTimeAGDP.x = x[8] - getTimeSize().width - indentX + timeShiftA;
        pointTimeAGDP.y = (int) (getTimeSize().height * 3 / 4)- maxH + h;
    }
}
