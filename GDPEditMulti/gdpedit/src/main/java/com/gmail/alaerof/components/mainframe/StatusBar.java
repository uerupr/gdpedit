package com.gmail.alaerof.components.mainframe;

import com.gmail.alaerof.manager.LocaleManager;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class StatusBar extends JLabel {
    private static final long serialVersionUID = 1L;
    private static ResourceBundle bundle;

    /** Creates a new instance of StatusBar */
    public StatusBar() {
        super();
        super.setPreferredSize(new Dimension(100, 19));
        setMessage(bundle.getString("gdp.ready"));
        //this.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        //this.setBorder(BorderFactory.createEtchedBorder());
        this.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.gdp", LocaleManager.getLocale());
    }

    public void setMessage(String message) {
        setText(" " + message);
        this.repaint();
    }
}
