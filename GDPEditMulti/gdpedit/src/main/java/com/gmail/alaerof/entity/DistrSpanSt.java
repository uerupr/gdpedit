package com.gmail.alaerof.entity;

import com.gmail.alaerof.dao.dto.SpanSt;

/**
 * остановочный пункт
 * @author Helen Yrofeeva
 *
 */
public class DistrSpanSt extends DistrPoint {
    private SpanSt spanSt;

    public SpanSt getSpanSt() {
        return spanSt;
    }

    public void setSpanSt(SpanSt spanSt) {
        this.spanSt = spanSt;
    }

    @Override
    public TypePoint getTypePoint() {
        return DistrPoint.TypePoint.StopPoint;
    }

    @Override
    public String getNamePoint() {
        if (spanSt != null) {
            return spanSt.getName();
        } else {
            return "";
        }
    }

    @Override
    public int getIdPoint() {
        if (spanSt != null) {
            return spanSt.getIdSpanst();
        } else
            return 0;
    }

}
