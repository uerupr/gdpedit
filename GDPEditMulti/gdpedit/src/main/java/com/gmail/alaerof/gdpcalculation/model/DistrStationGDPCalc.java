package com.gmail.alaerof.gdpcalculation.model;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.IntGR;
import com.gmail.alaerof.dao.dto.StationIntGR;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IGDPCalcDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.gdpcalculation.CalculateUtil;
import com.gmail.alaerof.util.TimeConverter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * ��������� ����� ������� �������
 */
public class DistrStationGDPCalc {
    protected static Logger logger = LogManager.getLogger(DistrStationGDPCalc.class);
    /** ������� ������� */
    private DistrStation distrStation;
    /** ������ ������� �� ������� ��� ������� ���� �����, �.�. �� ������� ������ ����� 24 �������
     * ��� ����� ������� �� ������� ������� ������� ������� */
    private List<List<DistrTrainGDPCalc>> trainsByHour;
    /** ������ �������������� �������� ���������� ������� */
    private List<StationIntGR> intGRList = new ArrayList<>();

    public DistrStationGDPCalc(DistrStation distrStation) {
        this.distrStation = distrStation;
        initTrainsByHour();
    }

    private void initTrainsByHour() {
        trainsByHour = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            List<DistrTrainGDPCalc> hourSet = new ArrayList<>();
            trainsByHour.add(hourSet);
        }
    }

    public void initFromDB(List<DistrTrainGDPCalc> allTrains, List<Category> categoryList, String trainIDs, int idDistr)throws DataManageException {
        IGDPCalcDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getGDPCalcDAO();
        // ��� ������ �� �������� �� ��
        List<DistrTrain> distrTrains = dao.getStationDistrTrains(distrStation.getIdPoint(), trainIDs, idDistr);

        // ��������� ������ ��, ������� �� ����� �������������, �� ����� ���������, �.�. ��� ����� fixed
        List<DistrTrain> distrTrainsToWrap = new ArrayList<>();
        // fixed trains �� allTrains (� ������� ���� �� ���� �������)
        List<DistrTrainGDPCalc> fixedTrains = new ArrayList<>();
        for (DistrTrain distrTrain : distrTrains) {
            DistrTrainGDPCalc trainGDPCalc = CalculateUtil.findInList(distrTrain, allTrains);
            if (trainGDPCalc == null) {
                distrTrainsToWrap.add(distrTrain);
            } else {
                if (trainGDPCalc.isFixed()) {
                    // � ����� trainGDPCalc.distrTrain.mapLineStTr �� ���� ������ ���� ���������
                    // � mapLineStTr ��� ����� ������� ������ ����...
                    // todo �� ����� ���������
                    fixedTrains.add(trainGDPCalc);
                }
            }
        }
        // ��������� ����� ������ �� �� � ����� fixed ������
        fixedTrains.addAll(CalculateUtil.wrapDistrTrains(distrTrainsToWrap, categoryList, true));

        // ��������� ��������� ������� �� �����
        int idSt = distrStation.getIdPoint();
        for(DistrTrainGDPCalc trainGDPCalc: fixedTrains){
            onStation(idSt, trainGDPCalc);
        }
    }


    public void onStation(int idSt, DistrTrainGDPCalc trainGDPCalc) {
        int hourOn = extractLineStTrHourOn(idSt, trainGDPCalc);
        if (hourOn > -1) {
            trainsByHour.get(hourOn).add(trainGDPCalc);
        }
        int hourOff = extractLineStTrHourOff(idSt, trainGDPCalc);
        if (hourOff > -1 && hourOff != hourOn) {
            trainsByHour.get(hourOff).add(trainGDPCalc);
        }
    }

    public void removeTrain(DistrTrainGDPCalc train) {
        int idSt = distrStation.getIdPoint();
        int hourOn = extractLineStTrHourOn(idSt, train);
        if (hourOn > -1) {
            trainsByHour.get(hourOn).remove(train);
        }
        int hourOff = extractLineStTrHourOn(idSt, train);
        if (hourOff > -1) {
            trainsByHour.get(hourOff).remove(train);
        }
    }

    public DistrStation getDistrStation() {
        return distrStation;
    }

    private int extractLineStTrHourOn(int idSt, DistrTrainGDPCalc distrTrain) {
        TimeStCalc timeStCalc = distrTrain.getTimeStCalcMap().get(idSt);
        if (timeStCalc != null && timeStCalc.getmOn() >= 0) {
            return TimeConverter.extractHour(timeStCalc.getmOn());
        }
        return -1;
    }

    private int extractLineStTrHourOff(int idSt, DistrTrainGDPCalc distrTrain) {
        TimeStCalc timeStCalc = distrTrain.getTimeStCalcMap().get(idSt);
        if (timeStCalc != null && timeStCalc.getmOff() >= 0) {
            return TimeConverter.extractHour(timeStCalc.getmOff());
        }
        return -1;
    }

    public DistrTrainGDPCalc getEarlierTrain(int off, int maxInt, boolean byOn, OE oe) {
        int hh = TimeConverter.extractHour(off);
        // ���� ����� ������ ����� � ���� ����
        DistrTrainGDPCalc earlie = getEarlie(off, maxInt, hh, byOn, oe);
        if (earlie != null) {
            return earlie;
        }

        // ���� ����� ������ ����� � ���������� ����
        int hhBefore = hh - 1;
        if (hhBefore < 0) {
            hhBefore = 23;
            off = 24*60;
        }
        earlie = getEarlie(off, maxInt, hhBefore, byOn, oe);

        return earlie;
    }

    private DistrTrainGDPCalc getEarlie(int tm, int maxInt, int hh, boolean byOn, OE oe) {
        Integer idSt = distrStation.getIdPoint();
        List<DistrTrainGDPCalc> list = trainsByHour.get(hh);
        if (oe != null) {
            list = extractOppositeTrains(list, oe);
        }
        Comparator<DistrTrainGDPCalc> comparator;
        if (byOn) {
            comparator = new DistrTrainStTimeOnComparator(idSt);
        } else {
            comparator = new DistrTrainStTimeOffComparator(idSt);
        }
        Collections.sort(list, comparator);
        Collections.reverse(list);
        DistrTrainGDPCalc earlie = null;
        for (int i = 0; i < list.size(); i++) {
            DistrTrainGDPCalc tr = list.get(i);
            TimeStCalc time = tr.getTimeStCalcMap().get(idSt);
            if (byOn && time.getmOn() <= tm) {
                earlie = tr;
                break;
            }
            if (!byOn && time.getmOff() <= tm) {
                earlie = tr;
                break;
            }
        }
        if (earlie != null && maxInt > 0) {
            TimeStCalc time = earlie.getTimeStCalcMap().get(idSt);
            int interspace = TimeConverter.minutesBetween(time.getmOff(), tm);
            if (interspace <= maxInt) {
                return earlie;
            }
        }
        return earlie;
    }

    public DistrTrainGDPCalc getLaterTrain(int off, int maxInt, boolean byOn, OE oe) {
        int hh = TimeConverter.extractHour(off);
        // ���� ����� ������� ����� � ���� ����
        DistrTrainGDPCalc later = getLater(off, maxInt, hh, byOn, oe);
        if (later != null) {
            return later;
        }

        // ���� ����� ������� ����� � ��������� ����
        int hhAfter = hh + 1;
        if (hhAfter > 23) {
            hhAfter = 0;
            off = 0;
        }
        later = getLater(off, maxInt, hhAfter, byOn, oe);

        return later;
    }

    private DistrTrainGDPCalc getLater(int off, int maxInt, int hh, boolean byOn, OE oe) {
        Integer idSt = distrStation.getIdPoint();
        List<DistrTrainGDPCalc> list = trainsByHour.get(hh);
        if (oe != null) {
            list = extractOppositeTrains(list, oe);
        }
        Comparator<DistrTrainGDPCalc> comparator;
        if (byOn) {
            comparator = new DistrTrainStTimeOnComparator(idSt);
        } else {
            comparator = new DistrTrainStTimeOffComparator(idSt);
        }
        Collections.sort(list, comparator);
        DistrTrainGDPCalc later = null;
        for (int i = 0; i < list.size(); i++) {
            DistrTrainGDPCalc tr = list.get(i);
            TimeStCalc time = tr.getTimeStCalcMap().get(idSt);
            if (byOn && time.getmOn() >= off) {
                later = tr;
                break;
            }
            if (!byOn && time.getmOff() >= off) {
                later = tr;
                break;
            }
        }

        if (later != null && maxInt > 0) {
            TimeStCalc time = later.getTimeStCalcMap().get(idSt);
            int interspace = TimeConverter.minutesBetween(off, time.getmOn());
            if (interspace <= maxInt) {
                return later;
            }
        }
        return later;
    }

    private List<DistrTrainGDPCalc> extractOppositeTrains(List<DistrTrainGDPCalc> distrTrainGDPCalcs, OE oe) {
        List<DistrTrainGDPCalc> list = new ArrayList<>();
        for (DistrTrainGDPCalc train : distrTrainGDPCalcs) {
            if (train.getDistrTrain().getOe() != oe) {
                list.add(train);
            }
        }
        return list;
    }

    /**
     * ��������� ������ �������������� �������� ���������� �������
     * ... ���� ����
     */
    public void initIntGR() throws DataManageException {
        List<StationIntGR> list = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO().getStationIntGR(distrStation.getIdPoint());
        this.intGRList.clear();
        this.intGRList.addAll(list);
    }

    /**
     * ���������� �������� ��������� �� �� ��������� ��� 0 ���� ���
     * @param idIntGR  �� ���������
     * @return �������� ��� 0
     */
    private int getIntGR(int idIntGR){
        for(StationIntGR  intGR: this.intGRList){
            if(intGR.getIdIntGR() == idIntGR){
                return intGR.getValue();
            }
        }
        return 0;
    }

    /**
     * �������� "��������� ��������"
     * @param intGRAllList ������ ����
     * @return ��������
     */
    public int getPassingArrive(List<IntGR> intGRAllList) {
        for (IntGR intGR : intGRAllList) {
            if (IntGR.IntGRTitle.PassingArrive.equals(intGR.getIntGRTitle())) {
                // �������� ����� �������������� ��������
                int value = getIntGR(intGR.getIdIntGR());
                if (value <= 0) {
                    // ���� ��� ���������������, �� ���������� �������� ��-���������
                    value = intGR.getDefaultValue();
                }
                return value;
            }
        }
        // ���� �������, ������ ���� ������ �� �����... �� ������ ���� �� ������
        return IntGR.MAX_INTGR;
    }

    /**
     * @param intGRAllList
     * @return ������������ �������� ��������� ��� ���������� �������
     */
    public int getMaxInt(List<IntGR> intGRAllList) {
        int val = 0;
        for (IntGR intGR : intGRAllList) {
            // �������� ����� �������������� ��������
            int value = getIntGR(intGR.getIdIntGR());
            if (value <= 0) {
                // ���� ��� ���������������, �� ���������� �������� ��-���������
                value = intGR.getDefaultValue();
            }
            if (value > val) {
                val = value;
            }
        }
        return Math.max(val, IntGR.MAX_INTGR);
    }

    /**
     * "���������������� �������� � ��������� �����������"
     * @return �������� ��� 0
     */
    public int checkNonSimultaneousArrivePassingDeparture(
            List<IntGR> intGRAllList, TimeStCalc earlieTime, TimeStCalc time)
    {
        if (time.getmOn() != time.getmOff()) {
            int intGR = getNonSimultaneousArrivePassingDeparture(intGRAllList);
            // time - ��� �������� �� ����� �����
            boolean toCheck = time.getmOn() < time.getmOff() &&
                    time.getmOn() < earlieTime.getmOn() && earlieTime.getmOn() <= time.getmOff();
            // time - ������� �� ����� �����,  earlieTime - �������� � ����� ������
            toCheck = toCheck ||
                    time.getmOn() > time.getmOff() && earlieTime.getmOn() <= time.getmOff();
            // time - ������� �� ����� �����,  earlieTime - �������� � ������ ������
            toCheck = toCheck ||
                    time.getmOn() > time.getmOff() && earlieTime.getmOn() >= time.getmOn();
            if (toCheck) {
                int diff = TimeConverter.minutesBetween(earlieTime.getmOn(), time.getmOff());
                if (diff < intGR) {
                    return intGR;
                }
            }
        }
        return 0;
    }

    /**
     * "���������������� �������� � ��������� �����������"
     * @return ��������
     */
    public int getNonSimultaneousArrivePassingDeparture(List<IntGR> intGRAllList) {
        for (IntGR intGR : intGRAllList) {
            if (IntGR.IntGRTitle.NonSimultaneousArrivePassingDeparture.equals(intGR.getIntGRTitle())) {
                // �������� ����� �������������� ��������
                int value = getIntGR(intGR.getIdIntGR());
                if (value <= 0) {
                    // ���� ��� ���������������, �� ���������� �������� ��-���������
                    value = intGR.getDefaultValue();
                }
                return value;
            }
        }
        // ���� �������, ������ ���� ������ �� �����... �� ������ ���� �� ������
        return IntGR.MAX_INTGR;
    }

    /**
     * �������� "��������� �����������"
     * @return �������� ��� 0
     */
    public int checkPassingDeparture(
            List<IntGR> intGRAllList, DistrTrainGDPCalc earlierTrainOff, DistrTrainGDPCalc trainOff,
            TimeStCalc earlieTime, TimeStCalc time)
    {
        /** ������ ����� */
        TrainType firstType = earlierTrainOff.getDistrTrain().getTypeTR();
        /** ������ ����� */
        TrainType secondType = trainOff.getDistrTrain().getTypeTR();
        /** ������ � ���������� */
        boolean firstStop = earlieTime.getmOn() != earlieTime.getmOff();
        /** ������ � ���������� */
        boolean secondStop = time.getmOn() != time.getmOff();
        int intGR = getPassingDeparture(intGRAllList, firstType, secondType, firstStop, secondStop);

        int diff = TimeConverter.minutesBetween(earlieTime.getmOff(), time.getmOff());
        if (diff < intGR) {
            return intGR;
        }
        return 0;
    }

    /**
     * �������� "��������� �����������"
     * @return ��������
     */
    public int getPassingDeparture(List<IntGR> intGRAllList, TrainType firstType, TrainType secondType,
                                    boolean firstStop, boolean secondStop)
    {
        for (IntGR intGR : intGRAllList) {
            if (IntGR.IntGRTitle.PassingDeparture.equals(intGR.getIntGRTitle()) &&
                    intGR.getFirstType() == firstType && intGR.getSecondType() == secondType &&
                    intGR.isFirstStop() == firstStop && intGR.isSecondStop() == secondStop)
            {
                // �������� ����� �������������� ��������
                int value = getIntGR(intGR.getIdIntGR());
                if (value <= 0) {
                    // ���� ��� ���������������, �� ���������� �������� ��-���������
                    value = intGR.getDefaultValue();
                }
                return value;
            }
        }
        // ���� �������, ������ ���� ������ �� �����... �� ������ ���� �� ������
        return IntGR.MAX_INTGR;
    }

    /**
     * �������� "���������������� ����������� � ��������� ��������"
     * @return �������� ��� 0
     */
    public int checkNonSimultaneousDeparturePassingArrive(
            List<IntGR> intGRAllList, TimeStCalc time, TimeStCalc laterTime)
    {
        int intGR = getNonSimultaneousDeparturePassingArrive(intGRAllList);
        int diff = TimeConverter.minutesBetween(time.getmOff(), laterTime.getmOn());
        if (diff < intGR) {
            return intGR;
        }
        return 0;
    }

    /**
     * �������� "���������������� ����������� � ��������� ��������"
     * @return ��������
     */
    public int getNonSimultaneousDeparturePassingArrive(List<IntGR> intGRAllList) {
        for (IntGR intGR : intGRAllList) {
            if (IntGR.IntGRTitle.NonSimultaneousDeparturePassingArrive.equals(intGR.getIntGRTitle())) {
                // �������� ����� �������������� ��������
                int value = getIntGR(intGR.getIdIntGR());
                if (value <= 0) {
                    // ���� ��� ���������������, �� ���������� �������� ��-���������
                    value = intGR.getDefaultValue();
                }
                return value;
            }
        }
        // ���� �������, ������ ���� ������ �� �����... �� ������ ���� �� ������
        return IntGR.MAX_INTGR;
    }

    /**
     * �������� "���������������� ���������"
     * @return �������� ��� 0
     */
    public int checkNonStopCross(List<IntGR> intGRAllList, TimeStCalc time, TimeStCalc laterTime) {
        int intGR = getNonStopCross(intGRAllList);
        int diff = TimeConverter.minutesBetween(time.getmOff(), laterTime.getmOn());
        if (diff < intGR) {
            return intGR;
        }
        return 0;
    }

    /**
     * �������� "���������������� ���������"
     * @return ��������
     */
    private int getNonStopCross(List<IntGR> intGRAllList) {
        for (IntGR intGR : intGRAllList) {
            if (IntGR.IntGRTitle.NonStopCross.equals(intGR.getIntGRTitle())) {
                // �������� ����� �������������� ��������
                int value = getIntGR(intGR.getIdIntGR());
                if (value <= 0) {
                    // ���� ��� ���������������, �� ���������� �������� ��-���������
                    value = intGR.getDefaultValue();
                }
                return value;
            }
        }
        // ���� �������, ������ ���� ������ �� �����... �� ������ ���� �� ������
        return IntGR.MAX_INTGR;
    }

    /**
     * �������� "���������"
     * @return �������� ��� 0
     */
    public int checkCross(List<IntGR> intGRAllList, TimeStCalc time, TimeStCalc laterTime) {
        int intGR = getCross(intGRAllList);
        int diff = TimeConverter.minutesBetween(time.getmOff(), laterTime.getmOn());
        if (diff < intGR) {
            return intGR;
        }
        return 0;
    }

    /**
     * �������� "���������"
     * @return ��������
     */
    public int getCross(List<IntGR> intGRAllList) {
        for (IntGR intGR : intGRAllList) {
            if (IntGR.IntGRTitle.Cross.equals(intGR.getIntGRTitle())) {
                // �������� ����� �������������� ��������
                int value = getIntGR(intGR.getIdIntGR());
                if (value <= 0) {
                    // ���� ��� ���������������, �� ���������� �������� ��-���������
                    value = intGR.getDefaultValue();
                }
                return value;
            }
        }
        // ���� �������, ������ ���� ������ �� �����... �� ������ ���� �� ������
        return IntGR.MAX_INTGR;
    }

    /**
     * �������� "��������� ��������"
     * @return �������� ��� 0
     */
    public int checkPassingArrive(List<IntGR> intGRList, TimeStCalc earlieTime, TimeStCalc time) {
        int intGR = getPassingArrive(intGRList);
        int diff = TimeConverter.minutesBetween(earlieTime.getmOn(), time.getmOn());
        if (diff < intGR) {
            return intGR;
        }
        return 0;
    }

    /**
     * �������� "���������������� �������� ������� ��������������� �����������"
     * @return �������� ��� 0
     */
    public int checkNonSimultaneousOppositeDirection(List<IntGR> intGRList, TimeStCalc earlieTime, TimeStCalc time) {
        int intGR = getNonSimultaneousOppositeDirection(intGRList);
        int diff = TimeConverter.minutesBetween(earlieTime.getmOn(), time.getmOn());
        if (diff < intGR) {
            return intGR;
        }
        return 0;
    }
    /**
     * �������� "���������������� �������� ������� ��������������� �����������"
     * @return ��������
     */
    public int getNonSimultaneousOppositeDirection(List<IntGR> intGRAllList) {
        for (IntGR intGR : intGRAllList) {
            if (IntGR.IntGRTitle.NonSimultaneousOppositeDirection.equals(intGR.getIntGRTitle())) {
                // �������� ����� �������������� ��������
                int value = getIntGR(intGR.getIdIntGR());
                if (value <= 0) {
                    // ���� ��� ���������������, �� ���������� �������� ��-���������
                    value = intGR.getDefaultValue();
                }
                return value;
            }
        }
        // ���� �������, ������ ���� ������ �� �����... �� ������ ���� �� ������
        return IntGR.MAX_INTGR;
    }

    public void initFromFixed(List<DistrTrainGDPCalc> allTrains) {
        // ��������� ��������� ������� �� �����
        int idSt = distrStation.getIdPoint();
        for(DistrTrainGDPCalc trainGDPCalc: allTrains){
            if (trainGDPCalc.isFixed() && !trainGDPCalc.getDistrTrain().getShowParam().isHidden()) {
                if(trainGDPCalc.getDistrTrain().getCodeTR().equals("2685")){
                    System.out.println("!!!");
                }
                onStation(idSt, trainGDPCalc);
            }
        }
    }
}
