package com.gmail.alaerof.components.menu;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dialog.locomotivetypeedit.LocomotiveTypeEditDialog;
import com.gmail.alaerof.javafx.dialog.categoryoftrains.CategoryOfTrainsFXDialog;
import com.gmail.alaerof.javafx.dialog.trainpropertys.AllTrainListFXDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JMenuItem;

/**
 * ���� "���"
 */
public class MenuCatalog extends LoggerMenu {
    public MenuCatalog() {
        super();
        String title = bundle.getString("components.menu.Catalog");
        this.setText(title);
        {
            title = bundle.getString("components.menu.viewconfig.TypeMove");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    showLocomotiveTypeEditDialog();
                }
            });
        }
        {
            title = bundle.getString("components.menu.CategoryOfTrains");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    showCategoriesOfTrains();
                }
            });
        }
        {   // ������ �������
            title = bundle.getString("components.menu.trains.allTrainList");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);

            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    openAllTrainList();
                }
            });
        }
    }
    private void showCategoriesOfTrains() {
        try {
            List<Category> categoryList = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO().getCategories();
            if (categoryList != null) {
                CategoryOfTrainsFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getCategoryOfTrainsFXDialog();
                dialog.setModal(true);
                dialog.setDialogContent(categoryList);
                dialog.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void openAllTrainList() {
        try {
            AllTrainListFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getAllTrainListFXDialog();
            dialog.setModal(true);
            dialog.setDialogContent();
            dialog.setVisible(true);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void showLocomotiveTypeEditDialog() {
        try {
            LocomotiveTypeEditDialog dialog = GDPEdit.gdpEdit.getDialogManager().getLocomotiveTypeEditDialog();
            dialog.load();
            dialog.setVisible(true);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
