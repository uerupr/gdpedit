package com.gmail.alaerof.entity;

import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.dao.dto.TextPrintParam;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.GDPPrintParam;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.TextPrintPointsOut;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import java.util.Map;
import java.util.ResourceBundle;

import com.gmail.alaerof.manager.LocaleManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.Distr;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.SpanTime;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.factory.IGDPDAOFactory;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.IScaleDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.entity.DistrPoint.TypePoint;
import com.gmail.alaerof.util.CalcString;
import com.gmail.alaerof.util.ColorConverter;


import static jdk.nashorn.internal.objects.NativeString.trim;

/**
 * @author Helen Yrofeeva
 */
public class DistrEntity {
    private static Logger logger = LogManager.getLogger(DistrEntity.class);
    private IGDPDAOFactory dataSource;
    private static ResourceBundle bundle;

    private int idDistr;
    private Distr distr;
    /** ������ ��������� ������� */
    private List<RouteD> listRoute;
    /** ������ ����������� ������� */
    static private List<LocomotiveType> listLoc;
    /** ������ ���� ��������� �������*/
    private List<DistrSpan> listSp;
    /** ������������� ������ ������� ������� (��� ����������� ����� �������) */
    private List<DistrStation> listSt;
    /** ������������� ������ ������� ������� � ������������ ������� (��� ����������) */
    private List<DistrPoint> listDistrPoint;
    /** ��������� ������� �������, ������ ������� � ��. ����� */
    private Map<Integer, DistrStation> mapSt = new HashMap<>();
    private String gdpInfo = "nothing";
    private GDPGridConfig configGDP;
    private XLeft xLeft;
    private XLeft xLeftJoint;
    private XLeft xLeftNoJoint;
    public static final int SEPARATED_GDP = 0;
    public static final int JOINT_GDP = 1;


    public DistrStation getDistrStationFromAll(int idSt) {
        return mapSt.get(idSt);
    }

    public DistrStation getDistrStationFromVisible(int idSt) {
        DistrStation distrStation = mapSt.get(idSt);
        if (!getListSt().contains(distrStation))
            distrStation = null;
        return distrStation;
    }

    public DistrPoint getDistrPoint(int idPoint, TypePoint typePoint) {
        for (DistrPoint dp : listDistrPoint) {
            if (dp.getTypePoint().equals(typePoint) && dp.getIdPoint() == idPoint) {
                return dp;
            }
        }
        return null;
    }

    public XLeft getxLeft() {

        return xLeft;
    }

    public XLeft getxLeftJoint() {

        return xLeftJoint;
    }

    public XLeft getxLeftNoJoint() {
        return xLeftNoJoint;
    }

    public DistrEntity(IGDPDAOFactory dataSource, int idDistr) {
        this.dataSource = dataSource;
        this.idDistr = idDistr;
        IDistrDAO dao = dataSource.getDistrDAO();
        try {
            configGDP = dao.getConfigGDPGrid(idDistr); // ����� ������
            distr = dao.getDistr(idDistr); // ����� ������
        } catch (ObjectNotFoundException | DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.entity", LocaleManager.getLocale());
    }

    private DistrStation getStation(Integer idSt) {
        DistrStation st = mapSt.get(idSt);
        if (st == null) {
            st = new DistrStation(dataSource, idSt);
            mapSt.put(idSt, st);
        }
        return st;
    }

    public DistrSpan getDistrSpan(int idSpan) {
        for (int i = 0; i < listSp.size(); i++) {
            DistrSpan ds = listSp.get(i);
            if (ds.getSpanScale().getIdSpan() == idSpan) {
                return ds;
            }
        }
        return null;
    }

    /**
     * ���������� ������� �� �� ������� (������� ������� �� ����� ���� �� �����)
     * @param idStB �� ��������� �������
     * @param idStE �� �������� �������
     * @return ������� ��� null
     */
    public DistrSpan getDistrSpan(int idStB, int idStE) {
        for (DistrSpan ds : listSp) {
            int idB = ds.getSpanScale().getIdSt();
            int idE = ds.getSpanScale().getIdStE();
            if (idB == idStB && idE == idStE) {
                return ds;
            }
            if (idE == idStB && idB == idStE) {
                return ds;
            }
        }
        return null;
    }

    /**
     * ���������� ������� �� �� ������� (������� ������� ����� !!!)
     * @param idStB �� ��������� �������
     * @param idStE �� �������� �������
     * @return ������� ��� null
     */
    public DistrSpan getExactDistrSpan(int idStB, int idStE) {
        for (DistrSpan ds : listSp) {
            int idB = ds.getSpanScale().getIdSt();
            int idE = ds.getSpanScale().getIdStE();
            if (idB == idStB && idE == idStE) {
                return ds;
            }
        }
        return null;
    }

    private void loadListSp() {
        IScaleDAO sd = dataSource.getScaleDAO();
        listSp = new ArrayList<>();
        List<Scale> listSc;
        DistrSpan sp;
        DistrStation stB, stE;
        Integer idStB, idStE;
        try {
            listSc = sd.getListScale(idDistr);
            for (Scale sc : listSc) {
                // ������� ������� ��� �������������
                idStB = sc.getIdSt();
                idStE = sc.getIdStE();
                stB = getStation(idStB);
                stE = getStation(idStE);
                // ������� �������
                sp = new DistrSpan(dataSource, sc, stB, stE);
                // ���������� �������� ��� �������, �� � ����������� ��� �����
                // ����������
                stB.setSpan(sp);
                stE.setSpan(sp);

                listSp.add(sp);
            }
        } catch ( DataManageException e) {
            //�������� ���������
            logger.error("Loading stages " + e.toString(), e);
        }

    }

    private void createListSt() {
        listSt = new ArrayList<>();
        listDistrPoint = new ArrayList<>();
        DistrStation st = null;
        int num = 1;
        for (DistrSpan sp : getListSp()) {
            // ���� ������ ������� �������� ������������, �� ������� �� � �����
            // ������
            if (sp.getSpanScale().isfSt()) {
                st = sp.getStB();
                st.setNum(num);
                // ������� ������ ����� ������ ��� ��� ���������
                st.setBE(0);
                // ������� ������ ����� ���� ������� (����� ����� ������ ��
                // �������� ����)
                st.setSpan(sp);
                num++;
                listSt.add(st);
                listDistrPoint.add(st);
            }
            // ��� ��� ����� ��������� ��� ������������� ������
            if (sp.getSpanScale().isfSt() || sp.getSpanScale().islSt()) {
                // ������� ��������� ������� ���� �� �������� ����� �.�.!!!
                sp.reloadSpanTime(getListDistrLoc());
                // ����� ���� �� �.�. �� ������� �.�.
                if (st != null) {
                    // ���� st!=null �� ��������� �� ��������� ������� ��������,
                    // (���� �� ��������... �� ������ ��� �� �� ���������)
                    st.setListSpanStTime(sp, 0);
                }
                List<SpanSt> listTw = sp.getSpanScale().getSpanSt();
                for (int i = 0; i < listTw.size(); i++) {
                    SpanSt spanSt = listTw.get(i);
                    DistrSpanSt dp = new DistrSpanSt();
                    dp.setSpanSt(spanSt);
                    dp.setNum(num);
                    num++;
                    listDistrPoint.add(dp);
                    // ����� ���� �� �������� �.�. �� ���������� �.�.
                    // (��� ���������� �.�.)
                    dp.setListSpanStTime(sp, spanSt.getNum());
                }
            }
            // ���� ��������� ������� �������� ������������, �� ������� �� �
            // ����� ������
            if (sp.getSpanScale().islSt()) {
                st = sp.getStE();
                st.setNum(num);
                // ������� ������ ����� ������ ��� ��� ���������
                st.setBE(1);
                // ������� ������ ����� ���� ������� (����� ����� ������ ��
                // �������� ����)
                st.setSpan(sp);
                num++;
                listSt.add(st);
                listDistrPoint.add(st);
            }
        }
    }

    public void updateDistrPointTime() {

    }

    public List<DistrSpan> getListSp() {
        if (listSp == null) {
            loadListSp();
        }
        return listSp;
    }

    public List<DistrStation> getListSt() {
        if (listSt == null) {
            createListSt();
        }
        return listSt;
    }

    public String getDistrName() {
        return distr.getName();
    }

    public int getIdDistr() {
        return idDistr;
    }

    public String getGDPInfo() {
        return gdpInfo;
    }

    public GDPGridConfig getConfigGDP() {
        return configGDP;
    }

    /**
     * ������ ������������������ ������ (�� �����) � ������ (�������� + ����
     * �������) ��� �������
     * 
     * @param hourB  ��������� ��� (0-24) ������� �����������
     * @param hourE  �������� ��� (0-24) ������� �����������
     */
    public void calcGDPSettings(int hourB, int hourE, GDPGridConfig configGDP) {
        if (hourB < 0 || hourB > 24) {
            hourB = 0;
        }
        if (hourE < 0 || hourE > 24) {
            hourE = 24;
        }
        configGDP.hourB = hourB;
        configGDP.hourE = hourE;

        // �������� ��������� �������� � ������������
        // ������� ����� ������� � ������� ���� � ���������� �� ������ ������ �������
        // ����� ������ ������� ������� �� ����������� �������� evenDown
        // (false - �����, true - �������)
        List<DistrStation> lst = getListSt();
        List<DistrPoint> lstP = getListDistrPoint();
        if (!configGDP.evenDown) {
            Collections.sort(lst);
            Collections.sort(lstP);
        } else {
            Collections.sort(lst);
            Collections.reverse(lst);
            Collections.sort(lstP);
            Collections.reverse(lstP);
        }
        float ldistr = 0; // ����� ������� � �������
        int lineH = 0; // ������� � ����� ������� � ��������, �� ����� �������
        int y0 = 0;
        float dl = 0;
        // --- ���� ����� ��� ������� ������ ������� �������� �� ---
        int widthStName = 0;
        // ������ ������ � ��������
        int heightStName = CalcString.getStringH(configGDP.stationFont);
        int maxW = 0;
        for (int i = 0; i < lst.size(); i++) {
            DistrStation dst = lst.get(i);
            // ������� y0 ������������� ��� ����� ������� �� ����� � �.�.
            y0 = Math.round(ldistr * configGDP.picTimeVert) + lineH;
            dst.setY0(y0);
            dst.calcLineStY0(configGDP); // ������ ��������� ����� �������
            // � ���������� �������� ������ ������� � ������ � ����������� �� �� ����������
            // ����������� ������� � ����� �������
            lineH += dst.getExpandHeight();
            // ������ ������ � ��������
            widthStName = CalcString.getStringW(configGDP.stationFont, dst.getStation().getName());
            dst.setNameHeight(heightStName);
            dst.setNameWidht(widthStName);
            maxW = Math.max(maxW, widthStName);
            int mt = 0;
            // ��������� ������ �� ��������� � �����
            if (i < lst.size() - 1) {
                if (configGDP.evenDown) {
                    dst = lst.get(i + 1);
                }
                mt = Math.round(dst.getSpan().getMaxTime(getShortListDistrLoc()));
                // ��� Scale - ��� ������� ������� �������� � ��������� ��
                // ����������� ��������, � �� ����� ������� ��������

                float scale_percent = dst.getScaleSt();
                float scale_koeff = scale_percent / 100;
                float ddl = Math.max(configGDP.minSpanLen, mt);
                dl = scale_koeff * ddl;
                ldistr += dl;
            }
        }
        // ��������� ������ ������� � ���������� ��
        configGDP.stationNameW = maxW + 12;
        // ������ ����� ������� � ��������
        ldistr = Math.round(ldistr * configGDP.picTimeVert) + lineH;
        configGDP.gdpHeight = (int) (ldistr);

        // ������� ������ ���
        float with24 = 24 * 60 * configGDP.picTimeHor;
        float width = 0;
        if (hourE == 24) {
            hourE = 0;
        }
        if (hourB == hourE) {
            width = with24;
        }
        if (hourB < hourE) {
            width = (hourE - hourB) * 60 * configGDP.picTimeHor;
        }
        if (hourB > hourE) {
            width = (hourE + 24 - hourB) * 60 * configGDP.picTimeHor;
        }
        configGDP.gdpWidth = (int) (width);
        configGDP.gdpWidth24 = (int) (with24);

        configGDP.spanTimeCount = getShortListDistrLoc().size();
        // ����� ������ ����� ������
        width = configGDP.spanTimeW * configGDP.spanTimeCount + configGDP.stationNameW + configGDP.scbW + configGDP.lineStW;
        configGDP.stationPanelWidth = (int) (width);
    }

    @Override
    public String toString() {
        return "DistrEntity [distr=" + distr.toString() + "]";
    }

    /**
     * ��������� ����� ������ (������� ���� � �������� �������) � ����� �������
     * @param grLeft ����������� ��������� ����� �����
     * @param grGDP ����������� ��������� ��������� �������
     * @param left ������ ����� (�� ������� ���������������)
     * @param top ������ ������ (�� ������� ���������������)
     * @param bottom ������ �����
     * @param bgColor ����� ���� ����
     * @param jointConst ������� ��������� ����� � �������� ������ �� ����� �������
     */
    public void drawNetGDP(Graphics grLeft, Graphics grGDP, int left, int top, int bottom, Color bgColor, int jointConst,
                           GDPGridConfig configGDP, GDPPrintParam gdpPrintParam, TextPrintPointsOut tPPO) {
        // ������� ������ ��� ���������������� ���������� ������� � ������������ ��� ���������
        if (xLeftNoJoint == null) {
            xLeftNoJoint = new XLeft(0);
        }
        if (xLeftJoint == null) {
            xLeftJoint = new XLeft(1);
        }

        if (jointConst == 0) {
            xLeft = xLeftNoJoint;
            xLeftNoJoint.calcVert();
            xLeftNoJoint.calcHor();
        } else {
            xLeft = xLeftJoint;
            xLeftJoint.indLeft = left;
            xLeftJoint.indTop = top;
            xLeftJoint.calcVert();
            xLeftJoint.calcHor();
        }

        // ���������� ���������
        // --------- ����� ��� --------------
        Graphics2D gGDP = (Graphics2D) grGDP;

        // ���� ����
        gGDP.setColor(bgColor);
        gGDP.fillRect(xLeft.xLeftGDP - configGDP.indentHor, 0, xLeft.xLeftGDP + configGDP.getScaledMainImageWidth() + left,
                (int) xLeft.top + configGDP.getScaledMainImageHeight() + bottom);

        int hCount = 24;
        int hb = configGDP.hourB;
        int he = configGDP.hourE;
        if (hb == he)
            hCount = 24;
        if (hb < he)
            hCount = he - hb;
        if (hb > he)
            hCount = 24 - hb + he;
        // ����� �����
        // ���� �����
        gGDP.setColor(configGDP.gdpGridColor);
        int k = 0;
        Stroke dash = new BasicStroke(1.0f, // Width
                BasicStroke.CAP_SQUARE, // End cap
                BasicStroke.JOIN_MITER, // Join style
                10.0f, // Miter limit
                new float[] { 5.0f, 5.0f }, // Dash pattern
                0.0f); // Dash phase
        Stroke solid = new BasicStroke(1.0f);
        Stroke bold = new BasicStroke(2.0f);
        gGDP.setFont(configGDP.gdpHourFont);
        int heightHour = CalcString.getStringH(configGDP.gdpHourFont);

        while (k <= hCount * 60) {
            int x0 = (int) (xLeft.xLeftGDP + k * configGDP.picTimeHor * configGDP.scale);

            gGDP.setStroke(solid);
            if (k % 30 == 0) {
                gGDP.setStroke(dash);
            }
            if (k % 60 == 0) {
                gGDP.setStroke(bold);
                // ������� ����
                int h = hb + k / 60;
                if (h > 24)
                    h -= 24;
                String tx = "" + h;
                int hw = CalcString.getStringW(configGDP.gdpHourFont, tx);
                // ���� ����� � �������� ����
                gGDP.setColor(configGDP.gdpStHourColor);
                gGDP.drawString(tx, x0 - hw / 2, xLeft.yDistr - heightHour / 3);
                gGDP.drawString(tx, x0 - hw / 2, xLeft.yDistr + xLeft.ldistr + heightHour);
                // ���������� ���� �������
                gGDP.setColor(configGDP.gdpGridColor);
            }
            gGDP.drawLine(x0, xLeft.yDistr, x0, xLeft.yDistr + xLeft.ldistr);
            k += 10;
        }
        gGDP.setStroke(solid);

        // --------- ��������� � ������� --------------
        if (gdpPrintParam != null){
            tPPO.CalcTextXPoints(xLeft.xLeftGDP, configGDP, left);
            for (int i = 0; i < gdpPrintParam.getHeadTextFields().size(); i++){
                drawText(gGDP, gdpPrintParam.getHeadTextFields().get(i), i, tPPO);
            }
            for (int i = 0; i < gdpPrintParam.getBossTextFields().size(); i++){
                drawText(gGDP, gdpPrintParam.getBossTextFields().get(i), i + 4, tPPO);
            }
            for (int i = 0; i < gdpPrintParam.getWorkerTextFields().size(); i++){
                drawText(gGDP, gdpPrintParam.getWorkerTextFields().get(i), i + 8, tPPO);
            }
        }


        // --------- ����� ������ -----------
        Graphics2D gLeft = (Graphics2D) grLeft;
        // ���� ����
        gLeft.setColor(bgColor);
        gLeft.fillRect(0, 0, (int) xLeft.left + configGDP.getScaledLeftWidth(), (int) xLeft.top + configGDP.getScaledMainImageHeight() + bottom);
        // ���� ����� �������
        gLeft.setColor(configGDP.gdpGridColor);

        // ����� �������
        gLeft.setFont(configGDP.headFont);
        int x = xLeft.xLeft;
        int y = xLeft.yTop;
        int w = xLeft.xRight - x;
        int h = ((int) xLeft.headH / 3) * 2;
        gLeft.drawRect(x, y, w, h);
        // ---- ������� ���� ������� ------
        h = ((int) xLeft.headH / 3);
        w = (int) xLeft.allSpanTimeW;
        gLeft.drawRect(x, y, w, h);
        String str = bundle.getString("entity.runningtimes");
        int hStrHead = CalcString.getStringH(configGDP.headFont);
        // int y0 = y + hStrHead + hStrHead / 2;
        int y0 = y + hStrHead;
        gLeft.drawString(str, x + 2, y0);
        int x0 = 0;
        // �������� ����������� + ������� ��� �������
        for (int i = 0; i < xLeft.locType.size(); i++) {
            x = xLeft.xTime.get(i);
            y = (int) (xLeft.yTop + xLeft.headH / 3);
            h = ((int) xLeft.headH / 3);
            w = (int) xLeft.spanTimeW;
            gLeft.drawRect(x, y, w, h);
            y0 = y + hStrHead;// + hStrHead / 2;
            str = xLeft.locType.get(i).getShortLtype();
            Color clrLoc = ColorConverter.convert(xLeft.locType.get(i).getColorLoc());
            gLeft.setColor(clrLoc);

            gLeft.drawString(str, x + 2, y0);
            // ���� ����� �������
            gLeft.setColor(configGDP.gdpGridColor);

            // ������������� � �����
            y = (int) (xLeft.yTop + (xLeft.headH / 3) * 2);
            w = w / 2;
            gLeft.drawRect(x, y, w, h);
            y0 = y + hStrHead;// + hStrHead / 2;
            str = bundle.getString("entity.odd");
            gLeft.setColor(clrLoc);
            gLeft.drawString(str, x + 2, y0);
            gLeft.setColor(configGDP.gdpGridColor);
            // ������ ������������� (� �������)
            gLeft.drawRect(x, xLeft.yDistr, w, xLeft.ldistr);

            x0 = (int) (x + xLeft.spanTimeW / 2);
            gLeft.drawRect(x0, y, w, h);
            str = bundle.getString("entity.even");
            gLeft.setColor(clrLoc);
            gLeft.drawString(str, x0 + 2, y0);
            gLeft.setColor(configGDP.gdpGridColor);
            // ������ ������������� (� �������)
            gLeft.drawRect(x0, xLeft.yDistr, w, xLeft.ldistr);
        }

        // ---- �������� �� (�����) ----
        str = bundle.getString("entity.namerp");
        int wStr = CalcString.getStringW(configGDP.headFont, str);
        x = (int) (xLeft.xStLeft + 2);// + xLeft.stationNameW / 3 - wStr / 2);
        y = (int) (xLeft.top + hStrHead);// xLeft.headH / 3);
        gLeft.drawString(str, x, y);

        // ---- ��� (����� � �������) ----
        y = (int) xLeft.top;
        h = ((int) xLeft.headH / 3) * 2;
        x = xLeft.xStRight;
        w = (int) (xLeft.scbW);
        gLeft.drawRect(x, y, w, h);
        y0 = y + hStrHead;
        str = bundle.getString("entity.sr");
        gLeft.drawString(str, x + 2, y0);
        y0 = y + 2 * hStrHead;
        str = bundle.getString("entity.signal");
        gLeft.drawString(str, x + 2, y0);
        // ������ �������������
        gLeft.drawRect(x, xLeft.yDistr, w, xLeft.ldistr);

        // ---- ���� (����� � �������) ----
        x = xLeft.xLineSt;
        w = (int) (xLeft.lineStW);
        gLeft.drawRect(x, y, w, h);
        y0 = y + hStrHead;
        str = bundle.getString("entity.number");
        gLeft.drawString(str, x + 2, y0);
        y0 = y + 2 * hStrHead;
        str = bundle.getString("entity.tracks");
        gLeft.drawString(str, x + 2, y0);
        // ������ �������������
        gLeft.drawRect(x, xLeft.yDistr, w, xLeft.ldistr);

        // -------- ��������� ������ ��������� -----------
        List<DistrStation> lst = getListSt();
        int y1 = 0;
        int hStr = CalcString.getStringH(configGDP.stationFont);
        int hUpDown = CalcString.getStringH(configGDP.updownFont);
        //int hMove = CalcString.getStringH(configGDP.moveFont);
        for (int j = 0; j < lst.size(); j++) {
            DistrStation st = lst.get(j);
            // ����� ������� �� ����� ������
            y1 = st.getY1();
            Stroke stLine = new BasicStroke((float) st.getWidthLineSt());
            gLeft.setStroke(stLine);
            gLeft.drawLine(xLeft.xLeft, y1, xLeft.xStLeft, y1);
            gLeft.drawLine(xLeft.xStRight, y1, xLeft.xRight, y1);
            gLeft.setStroke(solid);

            // ����� ������� �� ���
            gGDP.setStroke(stLine);
            gGDP.drawLine(xLeft.xLeftGDP, y1, (int) (xLeft.xLeftGDP + configGDP.gdpWidth * configGDP.scale), y1);
            gGDP.setStroke(solid);

            // �������� �������
            gLeft.setFont(configGDP.stationFont);
            str = st.getStation().getName();
            wStr = st.getNameWidht();
            x0 = xLeft.xStRight - wStr - 10;
            // ���� �������� �������
            gLeft.setColor(configGDP.gdpStHourColor);
            gLeft.drawString(str, x0, y1 + hStr / 3);
            // ���������� ���� �������
            gLeft.setColor(configGDP.gdpGridColor);
            // ���� ������� ��������, �� ������ � ������ �����
            if (st.isExpanded()) {
                // ����� ������� �� ����� ������
                y1 = st.getYE1();
                gLeft.setStroke(stLine);
                gGDP.setStroke(stLine);
                gLeft.drawLine(xLeft.xLeft, y1, xLeft.xStLeft, y1);
                gLeft.drawLine(xLeft.xStRight, y1, xLeft.xRight, y1);

                y0 = st.getY1();
                // ����������� ����� ������� �� ����� ����� �������
                if (!configGDP.listSelectedSt.contains(st.getNamePoint().trim())) {
                    gGDP.setColor(bgColor);
                    gGDP.fillRect(xLeft.xLeftGDP, y0, xLeft.xRightGDP - xLeft.xLeftGDP, y1 - y0);
                    gGDP.setColor(configGDP.gdpGridColor);
                }
                // ������������ ����� �������
                // ����� ������� �� ���
                //gGDP.drawLine(xLeft.xLeftGDP, y0, xLeft.xLeftGDP + configGDP.gdpWidth, y0);
                //gGDP.drawLine(xLeft.xLeftGDP, y1, xLeft.xLeftGDP + configGDP.gdpWidth, y1);
                // ������� �������� � �������� ����� �������� ������� ��� ���������������
                gGDP.drawLine(xLeft.xLeftGDP, y0, (int) (xLeft.xLeftGDP + configGDP.gdpWidth * configGDP.scale), y0);
                gGDP.drawLine(xLeft.xLeftGDP, y1, (int) (xLeft.xLeftGDP + configGDP.gdpWidth * configGDP.scale), y1);
                gLeft.setStroke(solid);
                gGDP.setStroke(solid);

                // �������� �������
                str = st.getStation().getName();
                wStr = st.getNameWidht();
                x0 = xLeft.xStRight - wStr - 10;
                // ���� �������� ������� ��� �� ��������
                gLeft.setColor(configGDP.gdpStHourColor);
                gLeft.drawString(str, x0, y1 + hStr / 3);
                // ���������� ���� �������
                gLeft.setColor(configGDP.gdpGridColor);
                // � ����� ������ ���� � �� �������� ��������������� ������
                List<DistrLineSt> listLST = st.getVisibleLines();
                gLeft.setFont(configGDP.stationLineFont);
                for (DistrLineSt dlst : listLST) {
                    y0 = dlst.getY1();
                    // ���� ���� �������
                    Color clrL = ColorConverter.convert(dlst.getLineSt().getColorLn());
                    gLeft.setColor(clrL);
                    gLeft.drawLine(xLeft.xStLeft, y0, xLeft.xStRight, y0);
                    gGDP.setColor(clrL);
                    //gGDP.drawLine(xLeft.xLeftGDP, y0, xLeft.xLeftGDP + configGDP.gdpWidth, y0);
                    // ������� �������� � �������� ����� �������� ������� ��� ���������������
                    gGDP.drawLine(xLeft.xLeftGDP, y0, (int) (xLeft.xLeftGDP + configGDP.gdpWidth * configGDP.scale), y0);
                    // �������� ����
                    str = dlst.getLineSt().getPs();
                    wStr = CalcString.getStringW(configGDP.stationFont, str);
                    x0 = xLeft.xStRight + 5;
                    gLeft.drawString(str, x0, y0 + hStr / 3);
                }
                // ���� ����� �������
                gLeft.setColor(configGDP.gdpGridColor);
                gGDP.setColor(configGDP.gdpGridColor);
            }

            if (j < lst.size() - 1) {
                DistrStation nextSt = lst.get(j + 1);
                // ������� ����
                // � ������ �������� ����� �������, �� ����� ����� ��� � ������������
                y = nextSt.getY1();
                int x1 = 0;
                String move = "";
                String up = "";
                String down = "";
                String tw = "";
                for (int i = 0; i < xLeft.locType.size(); i++) {
                    LocomotiveType lt = xLeft.locType.get(i);
                    Color clrLoc = ColorConverter.convert(lt.getColorLoc());
                    gLeft.setColor(clrLoc);

                    SpanTime spTime = null;
                    if (!configGDP.evenDown) {
                        DistrSpan ds = st.getSpan();
                        spTime = ds.getSpanTime(getShortListDistrLoc());
                    } else {
                        spTime = nextSt.getSpan().getSpanTime(getShortListDistrLoc());
                    }

                    x = xLeft.xTime.get(i); // �����
                    x0 = x + xLeft.spanTimeW / 2; // ���
                    x1 = x + xLeft.spanTimeW;
                    // ����� ��������
                    Time tm = spTime.getLocTimeO().get(lt);
                    if (tm != null) {
                        float fff = tm.getTimeMove();
                        move = CalcString.formatFloat(fff, 2, 1);
                        fff = tm.getTimeUp();
                        up = CalcString.formatFloat(fff, 2, 1);
                        fff = tm.getTimeDown();
                        down = CalcString.formatFloat(fff, 2, 1);
                        fff = tm.getTimeTw();
                        tw = CalcString.formatFloat(fff, 2, 1);
                        tw = "".equals(tw) ? "" : "+" + tw;
                    } else {
                        move = "";
                        up = "";
                        down = "";
                        tw = "";
                    }
                    gLeft.setFont(configGDP.moveFont);
                    y0 = y1 + (y - y1) / 2;// + hMove / 3;
                    gLeft.drawString(move, x + 2, y0);
                    gLeft.setFont(configGDP.updownFont);
                    gLeft.drawString(tw, x + 2, y0 + hUpDown);
                    if (!configGDP.evenDown) {
                        y0 = y1 + hUpDown - 2;
                        wStr = CalcString.getStringW(configGDP.updownFont, up);
                        gLeft.drawString(up, x0 - wStr - 3, y0);
                        y0 = y - 2;
                        wStr = CalcString.getStringW(configGDP.updownFont, down);
                        gLeft.drawString(down, x0 - wStr - 3, y0);
                    } else {
                        y0 = y1 + hUpDown - 2;
                        wStr = CalcString.getStringW(configGDP.updownFont, down);
                        gLeft.drawString(down, x0 - wStr - 3, y0);
                        y0 = y - 2;
                        wStr = CalcString.getStringW(configGDP.updownFont, up);
                        gLeft.drawString(up, x0 - wStr - 3, y0);
                    }

                    // ����� ������
                    tm = spTime.getLocTimeE().get(lt);
                    if (tm != null) {
                        float fff = tm.getTimeMove();
                        move = CalcString.formatFloat(fff, 2, 1);
                        fff = tm.getTimeUp();
                        up = CalcString.formatFloat(fff, 2, 1);
                        fff = tm.getTimeDown();
                        down = CalcString.formatFloat(fff, 2, 1);
                        fff = tm.getTimeTw();
                        tw = CalcString.formatFloat(fff, 2, 1);
                        tw = "".equals(tw) ? "" : "+" + tw;
                    } else {
                        move = "";
                        up = "";
                        down = "";
                        tw = "";
                    }
                    gLeft.setFont(configGDP.moveFont);
                    y0 = y1 + (y - y1) / 2;// + hMove / 3;
                    gLeft.drawString(move, x0 + 2, y0);
                    gLeft.setFont(configGDP.updownFont);
                    gLeft.drawString(tw, x0 + 2, y0 + hUpDown);
                    if (configGDP.evenDown) {
                        y0 = y1 + hUpDown - 2;
                        wStr = CalcString.getStringW(configGDP.updownFont, up);
                        gLeft.drawString(up, x1 - wStr - 3, y0);
                        y0 = y - 2;
                        wStr = CalcString.getStringW(configGDP.updownFont, down);
                        gLeft.drawString(down, x1 - wStr - 3, y0);
                    } else {
                        y0 = y1 + hUpDown - 2;
                        wStr = CalcString.getStringW(configGDP.updownFont, down);
                        gLeft.drawString(down, x1 - wStr - 3, y0);
                        y0 = y - 2;
                        wStr = CalcString.getStringW(configGDP.updownFont, up);
                        gLeft.drawString(up, x1 - wStr - 3, y0);
                    }
                }
                // ���� ����� �������
                gLeft.setColor(configGDP.gdpGridColor);

                // �������� �����
                gLeft.setFont(configGDP.headFont);
                String scb = "";
                if (!configGDP.evenDown) {
                    scb = st.getSpan().getSpanScale().getScb();
                } else {
                    scb = nextSt.getSpan().getSpanScale().getScb();
                }
                x0 = xLeft.xStRight + 3;
                y0 = y1 + (y - y1) / 2;
                if (scb == null)
                    scb = "";
                gLeft.drawString(scb, x0, y0);

                // ���������� ����� �� �������� + �� + ������-���� ���� �������
                int countLine = 0;
                int passBuild = nextSt.getStation().getPassBld();
                int lineO = nextSt.getStation().getCountLineO();
                int lineE = nextSt.getStation().getCountLineE();
                if (!configGDP.evenDown) {
                    countLine = st.getSpan().getSpanScale().getLine();
                } else {
                    countLine = nextSt.getSpan().getSpanScale().getLine();
                }
                x0 = xLeft.xLineSt + (xLeft.xRight - xLeft.xLineSt) / 2;
                if (countLine == 1) {
                    gLeft.drawLine(x0, y1, x0, y);
                }
                if (countLine == 2) {
                    gLeft.drawLine(x0 - 2, y1, x0 - 2, y);
                    gLeft.drawLine(x0 + 2, y1, x0 + 2, y);
                }
                y0 = y - configGDP.passBuildHeight;

                if (passBuild == 1) { // �����
                    gLeft.fillRect(xLeft.xLineSt, y0, configGDP.passBuildWidth, configGDP.passBuildHeight);
                }
                if (passBuild == 2) { // ���
                    gLeft.fillRect(xLeft.xRight - configGDP.passBuildWidth, y0, configGDP.passBuildWidth, configGDP.passBuildHeight);
                }
                if (passBuild == 3) { // ������
                    gLeft.fillRect(x0 - configGDP.passBuildWidth / 2, y0, configGDP.passBuildWidth, configGDP.passBuildHeight);
                }
                if (lineO > 0) {
                    gLeft.drawString("" + lineO, xLeft.xLineSt + 5, y - hStr / 2);
                }
                if (lineE > 0) {
                    gLeft.drawString("" + lineE, xLeft.xRight - 10, y - hStr / 2);
                }
            }
        }
    }

    private void drawText(Graphics2D gGDP, TextPrintParam tpp, int code, TextPrintPointsOut tPPO){
        if (tpp.getBChoise()){
            String strtext = tpp.getTextField();
            strtext = trim(strtext);
            String fName = tpp.getFontName();
            int fStyle = tpp.getFontStyleAwt();
            int fSize = tpp.getFontSizeAwt();
            java.awt.Font strfont = new java.awt.Font(fName, fStyle, fSize);
            gGDP.setFont(strfont);
            gGDP.drawString(strtext, tPPO.getXPoint(code), tPPO.getYPoint(code));
        }
    }

    public void drawNetGDPHour(Graphics grHour, int left, int top, Color bgColor) {
        // ���������� ���������
        // --------- ����� ��� --------------
        Graphics2D gGDP = (Graphics2D) grHour;

        // ���� ����
        gGDP.setColor(bgColor);
        gGDP.fillRect(xLeft.xLeftGDP - configGDP.indentHor, (int) xLeft.top, xLeft.xLeftGDP + configGDP.getScaledMainImageWidth(),
                (int) xLeft.top + configGDP.getScaledHourHeight());
        int hCount = 24;
        int hb = configGDP.hourB;
        int he = configGDP.hourE;
        if (hb == he)
            hCount = 24;
        if (hb < he)
            hCount = he - hb;
        if (hb > he)
            hCount = 24 - hb + he;
        // ����� �����
        // ���� �����
        // ���� ����� � ��������� ������� ������
        //gGDP.setColor(configGDP.gdpGridColor);
        gGDP.setColor(configGDP.gdpStHourColor);
        int k = 0;
        gGDP.setFont(configGDP.gdpHourFont);
        int heightHour = CalcString.getStringH(configGDP.gdpHourFont);
        while (k <= hCount * 60) {
            int x0 = (int) (xLeft.xLeftGDP + k * configGDP.picTimeHor * configGDP.scale);
            if (k % 60 == 0) {
                // ������� ����
                int h = hb + k / 60;
                if (h > 24)
                    h -= 24;
                String tx = String.valueOf(h);
                int hw = CalcString.getStringW(configGDP.gdpHourFont, tx);
                gGDP.drawString(tx, x0 - hw / 2, heightHour);
            }
            k += 10;
        }
    }

    private List<LocomotiveType> getShortListDistrLoc() {
        List<LocomotiveType> list = new ArrayList<>();
        List<LocomotiveType> fullList = getListDistrLoc();
        for (int i = 0; i < fullList.size(); i++) {
            LocomotiveType lt = fullList.get(i);
            boolean selected = configGDP.listSelectedLoc.contains(lt.getLtype().trim());
            if (selected) {
                list.add(lt);
            }
            // if (lt.isShowInGDP()) {
            // list.add(lt);
            // }
        }

        return list;
    }

    public List<LocomotiveType> getListDistrLoc() {
        if (listLoc == null) {
            try {
                listLoc = new ArrayList<>(dataSource.getDistrDAO().getListLocomotiveType());
            } catch ( DataManageException e) {
                logger.error(e.toString(), e);
            }
        }
        return listLoc;
    }

    public List<DistrPoint> getListDistrPoint() {
        if (listDistrPoint == null) {
            createListSt();
        }
        return listDistrPoint;
    }

    public void saveGDPGridConfig() {
        IDistrDAO dao = dataSource.getDistrDAO();
        dao.saveConfigGDPGrid(idDistr, distr.getName(), configGDP);
    }

    /**
     * ������ ������� �� ������� ������� (������)
     * @return double
     */
    public double getStopEnergy(LocomotiveType lt) {
        return 1.0;
    }

    public class XLeft {
        /** ������ ����������� �� ������� */
        List<LocomotiveType> locType;
        // �� ���������������� ���������� �� � ���������� (�� ���� ��
        // ��������������!!!)
        /** ������ ������� ������� */
        ArrayList<Integer> xTime = new ArrayList<>();
        /** ����� ���� ����� ����� */
        int xLeft;
        /** ������ ������� �������� ������� */
        int xStLeft; //
        /** ����� ������� �������� ������� */
        int xStRight; //
        /** ������ ������� "���-�� ����� �������" */
        int xLineSt; //
        /** ������ ���� ����� ����� */
        int xRight; //
        // ������ ������� (�� ���� �� �������������� �� ������, �.�. ����� ��
        // ��������������)
        /** ������ 1 ������� ������ ���� */
        int spanTimeW;
        /** ��������� ������ ������� ������ ���� */
        int allSpanTimeW;
        /** ������ ������� �������� ������� */
        int stationNameW;
        /** ������ ������� ��� */
        int scbW;
        /** ������ ������� "����" */
        int lineStW;

        /** ���������������� ������ ����� ������� */
        float headH;

        /** ���������������� ������ �����, ���������� ����� */
        float left;
        /** ���������������� ������ ������, ���������� ����� */
        float top;
        // ���������������� ���������� �� y ������������
        /** ������ ����� */
        int yTop;
        /** ������ ������� */
        int yDistr;
        /** ���������������� ����� ������� (������ ������� ��� ������ ��������) */
        int ldistr;

        /** ������������������ ������ �����, ���������� ����� */
        int indLeft;
        /** ������������������ ������ ������, ���������� ����� */
        int indTop;

        /** ����� ���� ��� */
        int xLeftGDP;
        /** ������ ���� ��� */
        int xRightGDP;
        /** ������ ���� ��� �� ����� */
        int xRightGDP24;

        int jointGDP;

        XLeft(int jointGDP) {
            this.jointGDP = jointGDP;
        }

        /**
         * ������ ���������������� ������������ ��������� �����, ��� ���� ��
         * ������� � ��� ����� � y1 ����� �������
         */
        void calcVert() {
            this.top = indTop * configGDP.scale;
            //headH = configGDP.gdpTop * configGDP.scale;
            headH = configGDP.gdpTop;

            float y = this.top;
            yTop = (int) y;
            y = this.top + headH + configGDP.indentDistr * configGDP.scale;
            // y = headH + configGDP.indentDistr * configGDP.scale;
            yDistr = (int) y;

            // ������ ���������������� ��������� y1 ����� �������
            List<DistrStation> lst = getListSt();
            for (DistrStation st : lst) {
                float y1 = y + st.getY0() * configGDP.scale;
                st.setY1((int) y1);
                float yE1 = y + st.getYE0() * configGDP.scale;
                st.setYE1((int) yE1);
                List<DistrLineSt> listLines = st.getVisibleLines();
                // � ����� �������
                for (DistrLineSt dlst : listLines) {
                    float yy = y + dlst.getY0() * configGDP.scale;
                    dlst.setY1((int) yy);
                }
            }
            ldistr = (int) (configGDP.gdpHeight * configGDP.scale);
        }

        void calcHor() {
            locType = getShortListDistrLoc();
            this.left = indLeft * configGDP.scale;
            xLeft = (int) this.left;
            spanTimeW = configGDP.spanTimeW;
            stationNameW = configGDP.stationNameW;
            scbW = configGDP.scbW;
            lineStW = configGDP.lineStW;
            float w = this.left;
            xTime.clear();
            for (int i = 0; i < locType.size(); i++) {
                xTime.add((int) w);
                w += spanTimeW;
            }
            allSpanTimeW = spanTimeW * locType.size();
            float x = (this.left + allSpanTimeW);
            xStLeft = (int) x;
            x += stationNameW;
            xStRight = (int) x;
            x += scbW;
            xLineSt = (int) x;
            x += lineStW;
            xRight = (int) x;

            // ������ ����� � ������������ �������
            xLeftGDP = (int) (configGDP.indentHor * configGDP.scale);
            if (jointGDP == DistrEntity.JOINT_GDP) {
                // � ������������ ��� ���� ������ �� ����� ����� !!!
                xLeftGDP += xRight;
            }
            xRightGDP = (int) (xLeftGDP + configGDP.gdpWidth * configGDP.scale);
            xRightGDP24 = (int) (xLeftGDP + configGDP.gdpWidth24 * configGDP.scale);
        }

        public int getxStRight() {
            return xStRight;
        }
    }

    public void updateSpanTime() {
        List<LocomotiveType> lt = getListDistrLoc();
        for (int i = 0; i < listSp.size(); i++) {
            DistrSpan sp = listSp.get(i);
            sp.reloadSpanTime(lt);
            sp.updateSpanScale();
        }

    }

    public List<RouteD> getListRoute() {
        if (listRoute == null) {
            loadListRoute();
        }
        return listRoute;
    }

    private void loadListRoute() {
        IDistrDAO dao = dataSource.getDistrDAO();
        try {
            listRoute = dao.getRouteDList(getIdDistr());
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public void updateRouteDList(){
        loadListRoute();
    }

    public void setNoJoint() {
        xLeftNoJoint.calcVert();
        xLeftNoJoint.calcHor();
        xLeft = xLeftNoJoint;
    }

    /**
     * ��������� ������ � �������� ���� ����� �.�. ���������
     */
    public void reloadSpanStTime() {
        for (DistrSpan sp : getListSp()) {
            sp.reloadSpanStTime();
        }
    }

    public String getDistrINDX() {
        return distr.getIndx();
    }

    public Map<Integer, DistrStation> getMapSt() {
        return mapSt;
    }

    /**
     * �������� ��� ������ ������� �� ��
     */
    public void reloadFromDB(){
        IDistrDAO dao = dataSource.getDistrDAO();
        try {
            configGDP = dao.getConfigGDPGrid(idDistr); // ����� ������
            distr = dao.getDistr(idDistr); // ����� ������
            listLoc = new ArrayList<>(dataSource.getDistrDAO().getListLocomotiveType());
            loadListSp();
            createListSt();
            loadListRoute();
        } catch (ObjectNotFoundException | DataManageException e) {
            logger.error(e.toString(), e);
        }

    }
}
