package com.gmail.alaerof.entity.train;

import java.util.Comparator;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class TrainStDoubleComparatorByCode implements Comparator<TrainStDouble> {

    @Override
    public int compare(TrainStDouble tr1, TrainStDouble tr2) {
        int t1 = tr1.getTrainThreadOn().getCodeInt();
        int t2 = tr2.getTrainThreadOn().getCodeInt();
        return t1 - t2;
    }

}
