package com.gmail.alaerof.entity.train;

import javax.swing.JCheckBox;

import com.gmail.alaerof.entity.DistrStation;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class StationExpandCheckBox extends JCheckBox {
    private static final long serialVersionUID = 1L;
    private DistrStation station;
    private int posX;

    public StationExpandCheckBox(DistrStation station, int posX) {
        this.station = station;
        this.posX = posX;
    }

    public DistrStation getStation() {
        return station;
    }

    public void setCurrentPosition() {
        //this.setBounds(10, 10, 20, 20);
        this.setBounds(posX, station.getY1()-10, 20, 20);
    }
}
