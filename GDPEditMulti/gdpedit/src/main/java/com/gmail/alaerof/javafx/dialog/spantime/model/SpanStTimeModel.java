package com.gmail.alaerof.javafx.dialog.spantime.model;

import com.gmail.alaerof.dao.dto.SpanStTime;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class SpanStTimeModel {
    private SpanStTime stTime;
    private int idScale;
    private int idLType;
    private IntegerProperty num;
    private DoubleProperty tMoveO;
    private DoubleProperty tMoveE;

    public SpanStTimeModel(SpanStTime stTime, int idScale, int idLType, int num, double tMoveO, double tMoveE) {
        this.stTime = stTime;
        this.idScale = idScale;
        this.idLType = idLType;
        this.num = new SimpleIntegerProperty(num);
        this.tMoveO = new SimpleDoubleProperty(tMoveO);
        this.tMoveE = new SimpleDoubleProperty(tMoveE);
    }

    public int getIdScale() {
        return idScale;
    }

    public int getIdLType() {
        return idLType;
    }

    public int getNum() {
        return num.get();
    }

    public IntegerProperty numProperty() {
        return num;
    }

    public void setNum(int num) {
        this.num.set(num);
    }

    public double getTMoveO() {
        return tMoveO.get();
    }

    public DoubleProperty tMoveOProperty() {
        return tMoveO;
    }

    public void setTMoveO(double tMoveO) {
        this.tMoveO.set(tMoveO);
        this.stTime.setTmoveO(tMoveO);
    }

    public double getTMoveE() {
        return tMoveE.get();
    }

    public DoubleProperty tMoveEProperty() {
        return tMoveE;
    }

    public void setTMoveE(double tMoveE) {
        this.tMoveE.set(tMoveE);
        this.stTime.setTmoveE(tMoveE);
    }

    public SpanStTime getStTime() {
        return stTime;
    }
}
