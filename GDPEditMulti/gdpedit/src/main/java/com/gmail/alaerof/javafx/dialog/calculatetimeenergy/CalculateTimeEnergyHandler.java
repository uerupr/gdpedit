package com.gmail.alaerof.javafx.dialog.calculatetimeenergy;

import com.gmail.alaerof.dao.dto.Dir;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.energy.Energy;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.dto.energy.EnergyType;
import com.gmail.alaerof.dao.dto.gdp.DirTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainExpense;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.dao.dto.groupp.GroupP;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.entity.train.TimeStation;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.gdpcalculation.CalculateUtil;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.CalcModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.CalcGroup;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.ExpenseRateEn;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.ExpenseRateModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.LocTypeModel;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.TimeConverter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CalculateTimeEnergyHandler {
    protected static Logger logger = LogManager.getLogger(CalculateTimeEnergyHandler.class);
    // ������� �� ������:
    private static final String ERR_NO_SPAN;
    // ��� �������� �� ���������:
    private static final String ERR_NO_LOCOMOTIVE_TYPE;
    // ��� �������� �� ���������:
    private static final String ERR_NO_ENERGY;
    // '�������' ����� �� ������� � ������:
    private static final String WARN_TRAIN_HIDDEN;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("bundles.gdpcalculation", LocaleManager.getLocale());
        ERR_NO_SPAN = bundle.getString("gdpcalculation.ERR_NO_SPAN");
        ERR_NO_LOCOMOTIVE_TYPE = bundle.getString("gdpcalculation.ERR_NO_LOCOMOTIVE_TYPE");
        ERR_NO_ENERGY = bundle.getString("gdpcalculation.ERR_NO_ENERGY");
        WARN_TRAIN_HIDDEN = bundle.getString("gdpcalculation.WARN_TRAIN_HIDDEN");
    }

    /**
     * ������ ��������� � �������������� ����� ��� �������
     * @param distrEditEntity ������� � ��������
     * @param dirList ������ ���
     * @param dirCalcMap ���������� ������� �� ����� �� ��� (����������� � �������� �������)
     * @param distrCalcGroups ���������� ������� �� �������
     * @param expenseRateEnMap  ��������� ������ �������
     * @param expenseRateList ��������� ������ ������/���������� ����
     * @param onlyRB � ������ ������ ��
     * @param noTechSt �� ��������� ���. �������
     * @param gdpEditWarnings ������ ������ ��� ������� ������ ��� ���������� � �������� ������
     */
    public static void calculateTimeEnergy(
            DistrEditEntity distrEditEntity,
            List<Dir> dirList,
            Map<Dir, List<CalcGroup>>  dirCalcMap,
            List<CalcGroup> distrCalcGroups,
            Map<EnergyType, ExpenseRateEn> expenseRateEnMap,
            List<ExpenseRateModel> expenseRateList, boolean onlyRB, boolean noTechSt,
            List<String> gdpEditWarnings)
    {
        logger.debug("calculateTimeEnergy - start: " + distrEditEntity.toString());
        List<TrainThread> trainThreadList = distrEditEntity.getListTrains();

        // �������� ������� ������� �� ��
        for (DistrSpan distrSpan : distrEditEntity.getDistrEntity().getListSp()) {
            distrSpan.updateSpanEnergy(distrEditEntity.getDistrEntity().getListDistrLoc());
        }
        Map<Integer, Dir> dirMap = new HashMap<>();
        for (Dir dir : dirList) {
            dirMap.put(dir.getIdDir(), dir);
        }
        // ��� ����� ����������������
        // fillTestEnergyValues(distrEditEntity.getDistrEntity());
        // ------------
        for (TrainThread trainThread : trainThreadList) {
            if (!trainThread.isForeign()) {
                trainThread.getDistrTrain().clearPreviousCalc();
                // ������ ������� � ���������� �� ���
                calculateGDPTimeLen(trainThread, distrEditEntity.getDistrEntity(), onlyRB, noTechSt);

                List<CalcGroup> distrCalcGroupTr = recognizeDistrGroup(trainThread, distrCalcGroups);
                matchExpensesToGroups(trainThread.getDistrTrain().getTypeTR(), distrCalcGroupTr,expenseRateEnMap, expenseRateList);

                for (CalcGroup distrCalcGroup : distrCalcGroupTr) {
                    LocTypeModel locTypeModel = distrCalcGroup.getAllCalc().getLocType();
                    if (locTypeModel == null) {
                        String message = ERR_NO_LOCOMOTIVE_TYPE + trainThread.getCode() +
                                "(" + trainThread.getNameTR() + ")";
                        logger.error(message);
                        gdpEditWarnings.add(message);
                    }
                    // ����������� ����� � ������
                    calculateNormTimeEnergy(trainThread, distrEditEntity.getDistrEntity(), locTypeModel, onlyRB,
                            gdpEditWarnings);
                    // ���������� ������� �� ������ distrCalcGroup
                    calculateCroup(trainThread, distrCalcGroup, gdpEditWarnings);
                    // ���������� ������� �� ��� �� ������ distrCalcGroup
                    calculateNODGroup(trainThread, distrCalcGroup, dirMap, dirCalcMap);
                }
            }
        }

        logger.debug("calculateTimeEnergy - end: " + distrEditEntity.toString());
        logger.debug("warning count = " + gdpEditWarnings.size());
    }

    private static void matchExpensesToGroups(TrainType typeTR, List<CalcGroup> distrCalcGroupTr,
                                              Map<EnergyType, ExpenseRateEn> expenseRateEnMap,
                                              List<ExpenseRateModel> expenseRateList) {
        for (CalcGroup distrCalcGroup : distrCalcGroupTr) {
            LocTypeModel locType = distrCalcGroup.getAllCalc().getLocType();
            ExpenseRateEn rateEn = expenseRateEnMap.get(locType.getEnergyType());
            ExpenseRateModel rate = findExpenseRateModel(typeTR, locType.getEnergyType(), expenseRateList);
            distrCalcGroup.setExpenseRate(rate);
            distrCalcGroup.setExpenseRateEn(rateEn);
        }
    }

    private static ExpenseRateModel findExpenseRateModel(TrainType typeTR, EnergyType energyType,
                                                         List<ExpenseRateModel> expenseRateList) {
        for (ExpenseRateModel rateModel : expenseRateList) {
            if (rateModel.getTrainType().equals(typeTR) && rateModel.getEnergyType().equals(energyType)) {
                return rateModel;
            }
        }
        return null;
    }

    private static void calculateCroup(TrainThread trainThread, CalcGroup distrCalcGroup,
                                       List<String> gdpEditWarnings) {
        DistrTrain train =  trainThread.getDistrTrain();
        LocTypeModel locTypeModel = distrCalcGroup.getAllCalc().getLocType();
        String key = locTypeModel.getExpenseKeyString();
        Map<String, DistrTrainExpense> expenseMap = train.getDistrTrainExpenseMap();

        DistrTrainExpense trainExpense = expenseMap.get(key);
        ExpenseRateModel expRate = distrCalcGroup.getExpenseRate();
        ExpenseRateEn expRateEn = distrCalcGroup.getExpenseRateEn();

        double km = train.getLen()/1000;
        double tT = train.getTimeWithoutStop()/60;
        double tU = train.getTimeWithStop()/60;
        double tN = trainExpense.getTimeNormMove()/60;
        double vN = 0;
        if (tN > 0) {
            vN = km / tN;
        }

        // ������� �� ����� �������, ���
        /** ������� �� ������-���� � �������� */
        double moveExpense = expRate.getMoveExpRate() * (Math.abs(tT-tN) + tN) * vN;
        /** ������� �� ������-���� ������� */
        double stopExpense = (tU - tT) * expRate.getStopExpRate();
        /** ������� �� ������ � ���������� */
        double downUpExpense = train.getStopCount() * locTypeModel.getDownupExp();// * expRateEn.getEnergyExpRate(); //15.04.20 ����������� ���
        /** ����������� ������� */
        double normExpense =  km * expRate.getMoveExpRate();

        trainExpense.setDownUpExpense(downUpExpense);
        trainExpense.setMoveExpense(moveExpense);
        trainExpense.setStopExpense(stopExpense);
        trainExpense.setNormExpense(normExpense);

        if (!trainThread.getDistrTrain().getShowParam().isHidden()) {
            if (trainThread.getOe() == OE.odd) {
                addToResult(distrCalcGroup.getOddCalc(), train, trainExpense);
            } else {
                addToResult(distrCalcGroup.getEvenCalc(), train, trainExpense);
            }
            addToResult(distrCalcGroup.getAllCalc(), train, trainExpense);
        } else{
            String message = WARN_TRAIN_HIDDEN + " " + trainThread.getDistrTrain().getCodeTR() +
                    "(" + trainThread.getDistrTrain().getNameTR() + ")";
            logger.error(message);
            gdpEditWarnings.add(message);
        }
    }

    private static List<CalcGroup> recognizeDistrGroup(TrainThread trainThread, List<CalcGroup> distrCalcGroups) {
        List<CalcGroup> list = new ArrayList<>();
        trainThread.getCodeInt();
        for (CalcGroup group : distrCalcGroups) {
            GroupP groupP = group.getAllCalc().getGroupP();
            if(groupP.isByColor() && groupP.isTrainInGroupByColor(trainThread.getColor())){
                list.add(group);
            }
            if (!groupP.isByColor() && groupP.isTrainInGroup(trainThread.getCodeInt())) {
                list.add(group);
            }
        }
        return list;
    }

    /**
     * ������: ���������� + ����� ��� ����� ������
     * @param trainThread ����� ������
     * @param distrEntity �������
     * @param onlyRB � ������ ������ ��
     * @param noTechSt �� ��������� ���. �������
     */
    private static void calculateGDPTimeLen(TrainThread trainThread, DistrEntity distrEntity, boolean onlyRB, boolean noTechSt) {
        List<TimeStation> timeStations = trainThread.getListTimeSt();
        double sumT = 0;  // ���������� �� ������� ��� �������
        double sumTS = 0; // ��������� ������ �� �������
        double len = 0;   // ����������, ���������� �� �������, �
        int stopCount = 0;  // ���-�� ������� ����� ��� � ���

        // �������������� ������� ���������� ������� �� ��� ��� ������
        trainThread.getDistrTrain().getDirTrainList().clear();

        for (int i = 0; i < timeStations.size() - 1; i++) {
            TimeStation timeStationB = timeStations.get(i);
            TimeStation timeStationE = timeStations.get(i + 1);
            // ����� ����� ���������
            int tm1 = timeStationB.getMOff();
            int tm2 = timeStationE.getMOn();
            int dt = TimeConverter.minutesBetween(tm1, tm2);
            DistrSpan distrSpan = distrEntity.getDistrSpan(timeStationB.getDistrStation().getIdPoint(), timeStationE.getDistrStation().getIdPoint());
            if (distrSpan != null && checkForRB(onlyRB, distrSpan)) {
                sumT += dt;
                len += distrSpan.getSpanScale().getL();
                // ���������� �� ���
                int idDir = distrSpan.getSpanScale().getIdDir();
                DirTrain dirTrain = findDirTrain(trainThread, idDir);
                dirTrain.setLen(dirTrain.getLen() + distrSpan.getSpanScale().getL());
                dirTrain.setTt(dirTrain.getTt() + dt);
                dirTrain.setTu(dirTrain.getTu() + dt);
            }
            // ������� �� ������� (��� ����� ������ � ���������)
            if (i > 0 && checkForTech(noTechSt, timeStationB) && checkForRB(onlyRB, timeStationB)) {
                tm1 = timeStationB.getMOn();
                tm2 = timeStationB.getMOff();
                dt = TimeConverter.minutesBetween(tm1, tm2);
                if (dt > 0) {
                    sumTS += dt;
                    stopCount++;

                    // ���������� �� ���
                    int idDir = timeStationB.getDistrStation().getStation().getIdDir();
                    DirTrain dirTrain = findDirTrain(trainThread, idDir);
                    dirTrain.setTu(dirTrain.getTu() + dt);
                    dirTrain.setStopCount(dirTrain.getStopCount() + 1);
                }
            }
        }
        trainThread.getDistrTrain().setLen(len);
        trainThread.getDistrTrain().setTimeWithoutStop(sumT);
        trainThread.getDistrTrain().setTimeWithStop(sumT + sumTS);
        trainThread.getDistrTrain().setStopCount(stopCount);
    }

    private static DirTrain findDirTrain(TrainThread trainThread, int idDir) {
        DirTrain dirTrain = trainThread.getDistrTrain().getDirTrain(idDir);
        if(dirTrain == null){
            dirTrain = new DirTrain();
            dirTrain.setIdDir(idDir);
            dirTrain.setIdTrain(trainThread.getIdTrain());
            dirTrain.setIdDistr(trainThread.getIdDistr());
            trainThread.getDistrTrain().getDirTrainList().add(dirTrain);
        }
        return dirTrain;
    }

    /**
     * ������ �������������� ������ ��� ����� ������
     * @param trainThread ����� ������
     * @param locTypeModel
     * @param onlyRB � ������ ������ ��
     * @param gdpEditWarnings ������ ������ ��� ������� ������ ��� ���������� � �������� ������
     */
    private static void calculateNormTimeEnergy(TrainThread trainThread, DistrEntity distrEntity,
                                                LocTypeModel locTypeModel, boolean onlyRB,
                                                List<String> gdpEditWarnings) {

        List<TimeStation> timeStations = trainThread.getListTimeSt();
        double sumTNorm = 0;  // ����� ��� �������
        double sumTSNorm = 0; // � ��������� � ������������ ��� �������
        double sumEM = 0;  // ������� �� ��������, ��� �������� � ����������
        double sumE = 0;  // ������� �� ��������, � ��������� � ������������
        double sumES = 0; // ������� ������ �� �������
        double eStop = 0; // ������ ������� � ������� �������
        LocomotiveType lt = null;
        if (locTypeModel != null) {
            lt = locTypeModel.getLocomotiveType();
        }
        if (lt != null) {
            eStop = distrEntity.getStopEnergy(lt);
        }
        for (int i = 0; i < timeStations.size() - 1; i++) {
            TimeStation timeStationB = timeStations.get(i);
            TimeStation timeStationE = timeStations.get(i + 1);
            DistrSpan distrSpan = distrEntity.getDistrSpan(timeStationB.getDistrStation().getIdPoint(), timeStationE.getDistrStation().getIdPoint());
            if (distrSpan != null && checkForRB(onlyRB, distrSpan)) {
                if (lt != null) {
                    boolean isStopB = i == 0 || timeStationB.getMOn() < timeStationB.getMOff();
                    boolean isStopE = (i == timeStations.size() - 1) || timeStationE.getMOn() < timeStationE.getMOff();
                    Energy energy = distrSpan.getSpanEnergy(lt, trainThread.getOe());
                    if (energy != null) {
                        sumEM += energy.getEnMove();
                        sumE += energy.getEnMove();
                        if (isStopB) {
                            sumE += energy.getEnUp();
                        }
                        if (isStopE) {
                            sumE += energy.getEnDown();
                        }
                    } else {
                        String message = ERR_NO_ENERGY + distrSpan.getSpanScale().getName() +
                                "(" + lt.getLtype() + ")";
                        logger.error(message);
                        gdpEditWarnings.add(message);
                    }
                    Time trainTime = CalculateUtil.getTrainTime(distrSpan, lt, trainThread.getDistrTrain(), false);
                    if (trainTime != null) {
                        sumTNorm += trainTime.getTimeMove();
                        sumTSNorm += trainTime.getTimeMove();
                        if (isStopB) {
                            sumTSNorm += trainTime.getTimeUp();
                        }
                        if (isStopE) {
                            sumTSNorm += trainTime.getTimeDown();
                        }
                    }
                }
            } else {
                String message = trainThread.getCode() + "(" + trainThread.getNameTR() + ") - " +
                        ERR_NO_SPAN +
                        timeStationB.getDistrStation().getNamePoint() +
                        " - " +
                        timeStationE.getDistrStation().getNamePoint();
                logger.error(message);
                gdpEditWarnings.add(message);
            }
        }
        double sumTS = trainThread.getDistrTrain().getTimeWithStop() - trainThread.getDistrTrain().getTimeWithoutStop();
        sumES = eStop * sumTS;

        Map<String, DistrTrainExpense> expenseMap = trainThread.getDistrTrain().getDistrTrainExpenseMap();
        DistrTrainExpense trainExpense = new DistrTrainExpense(locTypeModel.getLocomotiveType(), locTypeModel.getEnergyType());
        String key = trainExpense.getExpenseKeyString();
        expenseMap.put(key, trainExpense);
        trainExpense.setTimeNormMove(sumTNorm);
        trainExpense.setTimeNormAll(sumTSNorm);
        trainExpense.setEnergyWithoutStop(sumE);
        trainExpense.setEnergyWithStop(sumE + sumES);
        trainExpense.setEnergyMove(sumEM);
    }

    private static boolean checkForRB(boolean onlyRB, DistrSpan distrSpan) {
        if (onlyRB) {
            return distrSpan.getSpanScale().isBr();
        }
        return true;
    }

    private static boolean checkForRB(boolean onlyRB, TimeStation timeStation) {
        if (onlyRB) {
            return timeStation.getDistrStation().getStation().isBr();
        }
        return true;
    }

    private static boolean checkForTech(boolean noTechSt, TimeStation timeStation) {
        if (noTechSt) {
            String typeStB = timeStation.getDistrStation().getStation().getType();
            return !Station.TYPE_TECH.equals(typeStB);
        }
        return true;
    }

    private static void addToResult(CalcModel calc, DistrTrain distrTrain, DistrTrainExpense trainExpense) {
        calc.setTrainCount(calc.getTrainCount() + 1);
        calc.setLenM(calc.getLenM() + distrTrain.getLen());
        calc.setTimeWithoutStop(calc.getTimeWithoutStop() + distrTrain.getTimeWithoutStop());
        calc.setTimeWithStop(calc.getTimeWithStop() + distrTrain.getTimeWithStop());
        calc.setStopCount(calc.getStopCount() + distrTrain.getStopCount());

        calc.setTimeNormMove(calc.getTimeNormMove() + trainExpense.getTimeNormMove());
        calc.setEnergyMove(calc.getEnergyMove() + trainExpense.getEnergyMove());
        calc.setEnergyWithoutStop(calc.getEnergyWithoutStop() + trainExpense.getEnergyWithoutStop());
        calc.setEnergyWithStop(calc.getEnergyWithStop() + trainExpense.getEnergyWithStop());

        calc.setMoveExpense(calc.getMoveExpense() + trainExpense.getMoveExpense());
        calc.setDownUpExpense(calc.getDownUpExpense() + trainExpense.getDownUpExpense());
        calc.setStopExpense(calc.getStopExpense() + trainExpense.getStopExpense());
        calc.setNormExpense(calc.getNormExpense() + trainExpense.getNormExpense());
    }

    /**
     * ��������� dirCalcMap ������������ ������� (���,������ += �����)
     * @param trainThread  ����� � ������������ ������� �� ���  (DirTrainList)
     * @param distrCalcGroup ������ � ������� ������ �����
     * @param dirMap ������ ���
     * @param dirCalcMap map �������� ��� �� ������� (��� ����������)
     */
    private static void calculateNODGroup(TrainThread trainThread,
                                          CalcGroup distrCalcGroup,
                                          Map<Integer, Dir> dirMap,
                                          Map<Dir, List<CalcGroup>> dirCalcMap)
    {
        if (!trainThread.getDistrTrain().getShowParam().isHidden()) {
            for (DirTrain dirTrain : trainThread.getDistrTrain().getDirTrainList()) {
                Dir dir = dirMap.get(dirTrain.getIdDir());
                if (dir != null) {
                    CalcGroup dirGroup = getDistrCalcGroup(dirCalcMap, dir, distrCalcGroup);
                    if (trainThread.getOe() == OE.odd) {
                        addToDirResult(dirGroup.getOddCalc(), dirTrain);
                    } else {
                        addToDirResult(dirGroup.getEvenCalc(), dirTrain);
                    }
                    addToDirResult(dirGroup.getAllCalc(), dirTrain);
                }
            }
        }
    }

    /**
     * @param dirCalcMap
     * @param dir
     * @param sample
     * @return
     */
    private static CalcGroup getDistrCalcGroup(
            Map<Dir, List<CalcGroup>> dirCalcMap,
            Dir dir,
            CalcGroup sample)
    {
        List<CalcGroup> dirCalcGroupList = dirCalcMap.get(dir);
        if (dirCalcGroupList == null) {
            dirCalcGroupList = new ArrayList<>();
            dirCalcMap.put(dir, dirCalcGroupList);
        } else {
            for (CalcGroup dirCalcGroup : dirCalcGroupList) {
                if (dirCalcGroup.getAllCalc().getGroupP() == sample.getAllCalc().getGroupP()) {
                    return dirCalcGroup;
                }
            }
        }
        GroupP groupP = sample.getAllCalc().getGroupP();
        CalcModel allCalc = new CalcModel(groupP, groupP.getName(), CalcModel.LAYER_GROUP);
        CalcGroup dirCalcGroup = new CalcGroup(allCalc);
        dirCalcGroupList.add(dirCalcGroup);

        CalcModel oddModel = new CalcModel(groupP, OE.odd.getString(), CalcModel.LAYER_VALUE);
        oddModel.setLocType(sample.getAllCalc().getLocType());
        dirCalcGroup.setOddCalc(oddModel);
        CalcModel evenModel = new CalcModel(groupP, OE.even.getString(), CalcModel.LAYER_VALUE);
        evenModel.setLocType(sample.getAllCalc().getLocType());
        dirCalcGroup.setEvenCalc(evenModel);

        return dirCalcGroup;
    }

    /**
     * @param calc
     * @param dirTrain
     */
    private static void addToDirResult(CalcModel calc, DirTrain dirTrain) {
        calc.setTrainCount(calc.getTrainCount() + 1);
        calc.setLenM(calc.getLenM() + dirTrain.getLen());
        calc.setTimeWithoutStop(calc.getTimeWithoutStop() + dirTrain.getTt());
        calc.setTimeWithStop(calc.getTimeWithStop() + dirTrain.getTu());
        calc.setStopCount(calc.getStopCount() + dirTrain.getStopCount());
    }
}
