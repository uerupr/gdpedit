package com.gmail.alaerof.gdpcalculation.model;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IGDPCalcDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.gdpcalculation.CalculateUtil;
import com.gmail.alaerof.util.TimeConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * ��������� �������� �������
 */
public class DistrSpanGDPCalc {
    protected static Logger logger = LogManager.getLogger(DistrSpanGDPCalc.class);
    /** ������� ������� */
    private DistrSpan distrSpan;
    /** ������ ������� �� �������� ��� ������� ���� �����, �.�. �� ������� ������ ����� 24 �������
     * ��� ����� ������� �� ������� ������� �������� ������� */
    private List<List<DistrTrainGDPCalc>> trainsByHour;

    public DistrSpanGDPCalc(DistrSpan distrSpan) {
        this.distrSpan = distrSpan;
        initTrainsByHour();
    }

    private void initTrainsByHour() {
        trainsByHour = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            List<DistrTrainGDPCalc> hourList = new ArrayList<>();
            trainsByHour.add(hourList);
        }
    }

    public void initFromDB(List<DistrTrainGDPCalc> allTrains, List<Category> categoryList, String trainIDs, int idDistr) throws DataManageException {
        IGDPCalcDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getGDPCalcDAO();
        // ��� ������ �� �������� �� ��
        List<DistrTrain> distrTrains = dao.getSpanDistrTrains(distrSpan.getSpanScale().getIdSpan(), trainIDs, idDistr);
        // ��������� ������ ��, ������� �� ����� �������������, �� ����� ���������, �.�. ��� ����� fixed
        List<DistrTrain> distrTrainsToWrap = new ArrayList<>();
        // fixed trains �� allTrains (� ������� ���� �� ���� ��������)
        List<DistrTrainGDPCalc> fixedTrains = new ArrayList<>();
        for (DistrTrain distrTrain : distrTrains) {
            DistrTrainGDPCalc trainGDPCalc = CalculateUtil.findInList(distrTrain, allTrains);
            if (trainGDPCalc == null) {
                distrTrainsToWrap.add(distrTrain);
            } else {
                if (trainGDPCalc.isFixed()) {
                    // � ����� trainGDPCalc.distrTrain.mapSpTOcc �� ���� ������ ���� ���������
                    // � SpTOcc ��� ������ �������� distrSpan ������ ����...
                    // todo �� ����� ���������
                    fixedTrains.add(trainGDPCalc);
                }
            }
        }
        // ��������� ����� ������ �� �� � ����� fixed ������
        fixedTrains.addAll(CalculateUtil.wrapDistrTrains(distrTrainsToWrap, categoryList, true));

        // ��������� ��������� �������� �� �����
        for(DistrTrainGDPCalc trainGDPCalc: fixedTrains){
            onSpan(trainGDPCalc);
        }
    }

    public void onSpan(DistrTrainGDPCalc trainGDPCalc){
        int idSpan = distrSpan.getSpanScale().getIdSpan();
        int hourOn = extractSpTOccHourOn(idSpan, trainGDPCalc);
        if (hourOn > -1) {
            trainsByHour.get(hourOn).add(trainGDPCalc);
        }
        int hourOff = extractSpTOccHourOff(idSpan, trainGDPCalc);
        if (hourOff > -1 && hourOff != hourOn) {
            trainsByHour.get(hourOff).add(trainGDPCalc);
        }
    }

    public void removeTrain(DistrTrainGDPCalc train) {
        int idSpan = distrSpan.getSpanScale().getIdSpan();
        int hourOn = extractSpTOccHourOn(idSpan, train);
        if (hourOn > -1) {
            trainsByHour.get(hourOn).remove(train);
        }
        int hourOff = extractSpTOccHourOff(idSpan, train);
        if (hourOff > -1) {
            trainsByHour.get(hourOff).remove(train);
        }
    }

    private int extractSpTOccHourOn(int idSpan, DistrTrainGDPCalc distrTrain) {
        TimeSpCalc timeSpCalc = distrTrain.getTimeSpCalcMap().get(idSpan);
        if (timeSpCalc != null) {
            return TimeConverter.extractHour(timeSpCalc.getmOn());
        }
        return -1;
    }

    private int extractSpTOccHourOff(int idSpan, DistrTrainGDPCalc distrTrain) {
        TimeSpCalc timeSpCalc = distrTrain.getTimeSpCalcMap().get(idSpan);
        if (timeSpCalc != null) {
            return TimeConverter.extractHour(timeSpCalc.getmOff());
        }
        return -1;
    }

    public DistrSpan getDistrSpan() {
        return distrSpan;
    }

    /**
     * ���������� ���-�� ��������� ����� � ��������� ���������� ������� � ������ "����"
     * � varTime ������������ ��������� ��������� ����� ����������� � ������ ����������� �����
     *
     * @param mOn
     * @param mOff
     * @param varTime ����� ����, ���� ���� ����
     * @return
     */
    public int getLineCount(int mOn, int mOff, int[] varTime){
        return distrSpan.getSpanScale().getLine();
    }

    public void initFromFixed(List<DistrTrainGDPCalc> allTrains) {
        for (DistrTrainGDPCalc trainGDPCalc : allTrains) {
            if (trainGDPCalc.isFixed() && !trainGDPCalc.getDistrTrain().getShowParam().isHidden()) {
                onSpan(trainGDPCalc);
            }
        }
    }
}
