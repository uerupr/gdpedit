package com.gmail.alaerof.dialog.timeimport;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.VariantCalcTime;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.TableUtils;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */

public class VarCalcTimeTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("timeimport.number"),
            bundle.getString("timeimport.type"),
            bundle.getString("timeimport.name"),
            bundle.getString("timeimport.ecp"),
            bundle.getString("timeimport.l"),
            bundle.getString("timeimport.running"),
            bundle.getString("timeimport.slowdown"),
            bundle.getString("timeimport.racing"),
            bundle.getString("timeimport.addition"),
            bundle.getString("timeimport.km") };
    private List<VariantCalcTime> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.timeimport", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            VariantCalcTime el = list.get(row);
            switch (col) {
            case 0:
                return el.getNum();
            case 1:
                return el.getType();
            case 2:
                return el.getName();
            case 3:
                return el.getCodeESR();
            case 4:
                return TableUtils.dfNumber(el.getL() / 1000, 2);
            case 5:
                return TableUtils.dfNumber(el.getT1(), 1);
            case 6:
                return TableUtils.dfNumber(el.getTdown(), 1);
            case 7:
                return TableUtils.dfNumber(el.getTup(), 1);
            case 8:
                return TableUtils.dfNumber(el.getDt(), 1);
            case 9:
                return TableUtils.dfNumber(el.getAbs(), 1);
            }
        }
        return null;
    }

    public List<VariantCalcTime> getList() {
        return list;
    }

    public void setList(List<VariantCalcTime> list) {
        this.list = list;
    }

}
