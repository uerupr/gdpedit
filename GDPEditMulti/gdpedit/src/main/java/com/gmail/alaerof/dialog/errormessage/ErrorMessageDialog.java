package com.gmail.alaerof.dialog.errormessage;

import com.gmail.alaerof.util.IconImageUtil;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JTextArea;

public class ErrorMessageDialog extends JDialog {

    public ErrorMessageDialog(String message) {
        setIconImage(IconImageUtil.getImage(IconImageUtil.MAIN));
        setBounds(100, 100, 500, 200);
        getContentPane().setLayout(new BorderLayout());
        JTextArea text = new JTextArea(message);
        text.setLineWrap(true);
        getContentPane().add(text, BorderLayout.CENTER);
    }

}
