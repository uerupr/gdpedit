package com.gmail.alaerof.components.mainframe.gdp.action.listst;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public enum TimeMode {
    Leave, Arrive, Both
}