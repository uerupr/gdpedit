package com.gmail.alaerof.javafx.dialog.stationinfo.model;

import com.gmail.alaerof.manager.LocaleManager;

import java.util.ResourceBundle;

public class PassBldModel {
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.StationInfoFXDialog", LocaleManager.getLocale());
    }

    public enum PassBld{
        Non,
        Odd,
        Even,
        Island;

        public String getString(){
            switch (this) {
                case Non:
                    return "";
                case Odd:
                    return bundle.getString("stationinfo.odd");
                case Even:
                    return bundle.getString("stationinfo.even");
                case Island:
                    return bundle.getString("stationinfo.island");
                default:
                    return null;
            }
        }

        public static PassBld getValue(String name) {
            switch (name) {
                case "":
                    return Non;
                case "�����":
                    return Odd;
                case "���":
                    return Even;
                case "������":
                    return Island;
                default:
                    throw new IllegalArgumentException("name: " + name);
            }
        }

        @Override
        public String toString() {
            return this.getString();
        }
    }
}
