package com.gmail.alaerof.dialog.spantime;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.TableUtils;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.util.ResourceBundle;
import javax.swing.SwingConstants;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class SpanStPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private DistrSpan distrSpan;

    private LocomotiveType currentLoc;
    private LocomotiveTypeTableModel locTableModel;
    private JTable tableLoc;
    private SpanStTableModel spanStTableModel;
    private JTable tableSpanSt;
    private SpanStTimeTableModel spanStTimeTableModel;
    private JTable tableSpanStTime;
    private Window window;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spantime", LocaleManager.getLocale());
    }

    public SpanStPanel(LocomotiveTypeTableModel locTableModel, Window window) {
        setLayout(new BorderLayout(0, 0));
        this.locTableModel = locTableModel;
        this.window = window;
        initContent();
    }

    private void initContent() {
        Font tableFont = new Font(Font.SANS_SERIF, 0, 11);
        {
            JLabel label = new JLabel(bundle.getString("spantime.stoppingpoints"));
            label.setFont(new Font("Tahoma", Font.BOLD, 11));
            label.setHorizontalAlignment(SwingConstants.CENTER);
            add(label, BorderLayout.NORTH);

            JPanel panelSt = new JPanel();
            this.add(panelSt, BorderLayout.CENTER);
            panelSt.setLayout(new BorderLayout(0, 0));
            {
                JPanel panelS = new JPanel();
                panelSt.add(panelS, BorderLayout.SOUTH);
            }
            JScrollPane scrollPane = new JScrollPane();
            panelSt.add(scrollPane, BorderLayout.CENTER);

            spanStTableModel = new SpanStTableModel();
            tableSpanSt = new JTable(spanStTableModel);
            tableSpanSt.setFont(tableFont);
            tableSpanSt.getTableHeader().setFont(tableFont);
            tableSpanSt.setFillsViewportHeight(true);
            tableSpanSt.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 20, 60, 40, 30, 90, 40, 60 };
            TableUtils.setTableColumnSize(tableSpanSt, colSize);
            scrollPane.setViewportView(tableSpanSt);
        }
        {
            JPanel panelStTime = new JPanel();
            this.add(panelStTime, BorderLayout.EAST);
            panelStTime.setPreferredSize(new Dimension(240, 10));
            panelStTime.setLayout(new BorderLayout(0, 0));

            {
                JPanel panelLoc = new JPanel();
                panelLoc.setPreferredSize(new Dimension(10, 120));
                panelStTime.add(panelLoc, BorderLayout.NORTH);
                panelLoc.setLayout(new BorderLayout(0, 0));

                {
                    JPanel panelS = new JPanel();
                    panelLoc.add(panelS, BorderLayout.SOUTH);
                }

                JScrollPane scrollPane = new JScrollPane();
                panelLoc.add(scrollPane, BorderLayout.CENTER);

                tableLoc = new JTable(locTableModel);
                tableLoc.setFont(tableFont);
                tableLoc.getTableHeader().setFont(tableFont);
                tableLoc.setFillsViewportHeight(true);
                tableLoc.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                int colSizeLoc[] = { 120 };
                TableUtils.setTableColumnSize(tableLoc, colSizeLoc);
                scrollPane.setViewportView(tableLoc);

                ListSelectionModel selectionModel = tableLoc.getSelectionModel();
                selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

                selectionModel.addListSelectionListener(new ListSelectionListener() {
                    public void valueChanged(ListSelectionEvent e) {
                        int[] selectedRow = tableLoc.getSelectedRows();
                        if (selectedRow.length >= 1) {
                            int row = selectedRow[0];
                            currentLoc = locTableModel.getList().get(row);
                            spanStTimeTableModel.setLoc(currentLoc);
                            tableSpanStTime.repaint();
                        }
                    }

                });

            }
            {
                JPanel panelTime = new JPanel();
                panelStTime.add(panelTime, BorderLayout.CENTER);
                panelTime.setLayout(new BorderLayout(0, 0));
                {
                    JPanel panelS = new JPanel();
                    FlowLayout flowLayout = (FlowLayout) panelS.getLayout();
                    flowLayout.setAlignment(FlowLayout.LEADING);
                    panelTime.add(panelS, BorderLayout.SOUTH);
                    JButton but = new JButton(bundle.getString("spantime.createall"));
                    panelS.add(but);
                    but.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (currentLoc != null) {
                                distrSpan.buildNewSpanStTime(currentLoc);
                                spanStTimeTableModel.setAllList(distrSpan.getListSpanStTime());
                                spanStTimeTableModel.setLoc(currentLoc);
                                tableSpanStTime.repaint();
                            }
                        }
                    });

                    JButton butSave = new JButton(bundle.getString("spantime.db"));
                    butSave.setToolTipText(bundle.getString("spantime.saverunningtimesindb"));
                    panelS.add(butSave);
                    butSave.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent arg0) {
                            try {
                                window.setCursor(GDPEdit.waitCursor);
                                distrSpan.saveSpanStTime();
                            } finally {
                                window.setCursor(GDPEdit.defCursor);

                            }
                        }
                    });
                }
                {
                    JLabel title = new JLabel(bundle.getString("spantime.runningtimes"));
                    panelTime.add(title, BorderLayout.NORTH);
                }

                JScrollPane scrollPane = new JScrollPane();
                panelTime.add(scrollPane, BorderLayout.CENTER);

                spanStTimeTableModel = new SpanStTimeTableModel();
                tableSpanStTime = new JTable(spanStTimeTableModel);
                tableSpanStTime.setFont(tableFont);
                tableSpanStTime.getTableHeader().setFont(tableFont);
                tableSpanStTime.setFillsViewportHeight(true);
                tableSpanStTime.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                int colSizeLoc[] = { 20, 50, 50 };
                TableUtils.setTableColumnSize(tableSpanStTime, colSizeLoc);
                scrollPane.setViewportView(tableSpanStTime);
            }

        }
    }

    public void setDistrSpan(DistrSpan distrSpan) {
        this.distrSpan = distrSpan;
        tableLoc.changeSelection(2, 0, false, false);
        spanStTableModel.setList(distrSpan.getSpanScale().getSpanSt());
        tableSpanSt.repaint();
        spanStTimeTableModel.setAllList(distrSpan.getListSpanStTime());
        spanStTimeTableModel.setLoc(currentLoc);
        tableSpanStTime.repaint();
    }

}
