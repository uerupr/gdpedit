package com.gmail.alaerof.javafx.dialog.spantime.view;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.entity.DistrSpan;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public abstract class CommonDistrSpanController implements Initializable {
    /** logger */
    protected Logger logger;
    /** Internationalization */
    protected ResourceBundle bundle;
    /** ������� DistrSpan ��� ������*/
    protected DistrSpan distrSpan;
    /** ������� DistrSpan ��� ������*/
    protected List<LocomotiveType> listLoc;
    protected Scene scene;

    {
        logger = LogManager.getLogger(this.getClass());
    }

    /**
     * ������������� ���������� ��� �������� �� FXML ����������� (FXMLLoader)
     * @param location {@link URL}
     * @param resources {@ResourceBundle} �������������� ��������
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        initialize();
    }

    /**
     * ������������� ������-�����������.
     * ���������� �� ������������� ������ initialize(URL location, ResourceBundle resources),
     * ������� � ���� ������� ���������� ����������� ��� �������� �� FXML
     */
    protected abstract void initialize();

    /**
     * ������������� ������� ������� � ������ ����� �������� (�����������) ��� �����������
     * @param distrSpan {@link DistrSpan} ������� �������
     * @param listLoc {@link LocomotiveType} list ������ ����� �����������
     */
    public void setContent(DistrSpan distrSpan, List<LocomotiveType> listLoc) {
        logger.debug("setContent");
        this.distrSpan = distrSpan;
        this.listLoc = listLoc;
        updateModelWithContent();
    }

    /**
     * ��������� ������ ���������� ��� �����������
     */
    protected abstract void updateModelWithContent();

    /**
     * @param scene Scene
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
