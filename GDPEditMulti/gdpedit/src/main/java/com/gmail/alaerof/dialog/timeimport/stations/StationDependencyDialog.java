package com.gmail.alaerof.dialog.timeimport.stations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.IconImageUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.VariantCalc;
import com.gmail.alaerof.dao.dto.VariantCalcTime;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dialog.timeimport.spans.TimeConfirmDialog;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrPoint.TypePoint;
import com.gmail.alaerof.entity.DistrSpanSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.util.TableUtils;
import com.gmail.alaerof.xml.StationDependencyXML;

/**
 * ����� ��� ����������� ������������ ������� �� ������� ��������
 * �������� �.�. ������� �� �� �������
 * @author Helen Yrofeeva
 */
public class StationDependencyDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(StationDependencyDialog.class);;
    private final JPanel contentPanel = new JPanel();
    private StationHolderTableModel shTableModel = new StationHolderTableModel();
    private StationTableModel sdTableModel = new StationTableModel();
    private SpanStTableModel sptTableModel = new SpanStTableModel();
    private JTable shTable = new JTable(shTableModel);
    private JTable sdTable = new JTable(sdTableModel);
    private JTable sptTable = new JTable(sptTableModel);
    private List<VariantCalcTime> list;
    private int distrID;
    private DistrEntity distr;
    private LocomotiveType loc;
    private int oe;
    protected TimeConfirmDialog timeConfirmDialog;
    private VariantCalc varCalc;
    private static ResourceBundle bundle;

    public StationDependencyDialog() {
        initContent();
    }

    public StationDependencyDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.stations", LocaleManager.getLocale());
    }

    private void initContent() {
        setTitle(bundle.getString("stations.matchingpoints"));
        try {
            this.setIconImage(IconImageUtil.getImage(IconImageUtil.ISPT64));
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        timeConfirmDialog = new TimeConfirmDialog(null, bundle.getString("stations.stagerunningtimes"), true);

        setBounds(0, 0, 960, 600);
        setLocationRelativeTo(null);

        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[] { 0, 0, 0, 0 };
        gbl_contentPanel.rowHeights = new int[] { 0, 0 };
        gbl_contentPanel.columnWeights = new double[] { 10.0, 1.0, 6.0, Double.MIN_NORMAL };// Double.MIN_VALUE
        gbl_contentPanel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
        contentPanel.setLayout(gbl_contentPanel);
        {
            JPanel panel = new JPanel();
            GridBagConstraints gbc_panel = new GridBagConstraints();
            gbc_panel.insets = new Insets(0, 0, 0, 5);
            gbc_panel.fill = GridBagConstraints.BOTH;
            gbc_panel.gridx = 0;
            gbc_panel.gridy = 0;
            contentPanel.add(panel, gbc_panel);
            panel.setLayout(new BorderLayout(0, 0));
            {
                JLabel label = new JLabel(bundle.getString("stations.fromtraction"));
                panel.add(label, BorderLayout.NORTH);
            }
            {
                JScrollPane scrollPane = new JScrollPane();
                panel.add(scrollPane, BorderLayout.CENTER);
                {
                    // shTable = new JTable();
                    shTable.setFillsViewportHeight(true);
                    shTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    int colSize[] = { 25, 40, 60, 25, 40, 25, 40, 60 };
                    TableUtils.setTableColumnSize(shTable, colSize);
                    shTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

                    scrollPane.setViewportView(shTable);
                }
            }
        }
        {
            JPanel panel = new JPanel();
            GridBagConstraints gbc_panel = new GridBagConstraints();
            gbc_panel.insets = new Insets(0, 0, 0, 5);
            gbc_panel.gridx = 1;
            gbc_panel.gridy = 0;
            contentPanel.add(panel, gbc_panel);
            GridBagLayout gbl_panel = new GridBagLayout();
            gbl_panel.columnWidths = new int[] { 0, 0 };
            gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
            gbl_panel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
            gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
            panel.setLayout(gbl_panel);
            {
                JButton button = new JButton(bundle.getString("stations.twoarrowleft"));
                GridBagConstraints gbc_button = new GridBagConstraints();
                gbc_button.insets = new Insets(0, 0, 5, 0);
                gbc_button.gridx = 0;
                gbc_button.gridy = 0;
                panel.add(button, gbc_button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        int row = sdTable.getSelectedRow();
                        if (row > -1) {
                            Station st = sdTableModel.getList().get(row);
                            row = shTable.getSelectedRow();
                            if (row > -1) {
                                shTableModel.getList().get(row).distrSt = st;
                                shTableModel.getList().get(row).spanSt = null;
                                shTable.repaint();
                            }
                        }
                    }
                });
            }
            {
                JButton button = new JButton(bundle.getString("stations.twoarrowright"));
                GridBagConstraints gbc_button = new GridBagConstraints();
                gbc_button.insets = new Insets(0, 0, 5, 0);
                gbc_button.gridx = 0;
                gbc_button.gridy = 1;
                panel.add(button, gbc_button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        int row = shTable.getSelectedRow();
                        if (row > -1) {
                            shTableModel.getList().get(row).distrSt = null;
                            shTableModel.getList().get(row).spanSt = null;
                            shTable.repaint();
                        }
                    }
                });
            }
            {
                JButton button = new JButton(bundle.getString("stations.twoarrowleft"));
                GridBagConstraints gbc_button = new GridBagConstraints();
                gbc_button.gridx = 0;
                gbc_button.gridy = 3;
                panel.add(button, gbc_button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        int row = sptTable.getSelectedRow();
                        if (row > -1) {
                            SpanSt st = sptTableModel.getList().get(row);
                            row = shTable.getSelectedRow();
                            if (row > -1) {
                                shTableModel.getList().get(row).spanSt = st;
                                shTableModel.getList().get(row).distrSt = null;
                                shTable.repaint();
                            }
                        }
                    }
                });
            }

        }
        {
            JPanel panel = new JPanel();
            GridBagConstraints gbc_panel = new GridBagConstraints();
            gbc_panel.fill = GridBagConstraints.BOTH;
            gbc_panel.gridx = 2;
            gbc_panel.gridy = 0;
            contentPanel.add(panel, gbc_panel);
            panel.setLayout(new BorderLayout(0, 0));
            {
                JLabel label = new JLabel(bundle.getString("stations.fromasgraphist"));
                panel.add(label, BorderLayout.NORTH);
            }
            {
                JScrollPane scrollPane = new JScrollPane();
                panel.add(scrollPane, BorderLayout.CENTER);
                {
                    // sdTable = new JTable();
                    sdTable.setFillsViewportHeight(true);
                    sdTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    int colSize[] = { 25, 40, 25, 40, 60 };
                    TableUtils.setTableColumnSize(sdTable, colSize);
                    sdTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                    scrollPane.setViewportView(sdTable);
                }
            }
            {
                JScrollPane scrollPane = new JScrollPane();
                JPanel townPanel = new JPanel();
                townPanel.setLayout(new BorderLayout());
                townPanel.setPreferredSize(new Dimension(0, 250));
                townPanel.add(scrollPane, BorderLayout.CENTER);
                panel.add(townPanel, BorderLayout.SOUTH);
                {
                    // sdTable = new JTable();
                    sptTable.setFillsViewportHeight(true);
                    sptTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    int colSize[] = { 25, 40, 25, 40, 60 };
                    TableUtils.setTableColumnSize(sptTable, colSize);
                    sptTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                    scrollPane.setViewportView(sptTable);
                }

            }
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton(bundle.getString("stations.ok"));
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        if (shTableModel.getList() != null) {
                            StationDependencyXML.saveStationHolderList(distrID, distr.getDistrName(), shTableModel.getList(), varCalc.getOe());
                            // ������ ����� ��� ������� ����� � ��������� ��������
                            timeConfirmDialog.fillListSpans(distr, shTableModel.getList(), loc, oe, varCalc);
                            timeConfirmDialog.setVisible(true);
                        }
                    }
                });
            }
            {
                JButton cancelButton = new JButton(bundle.getString("stations.cancel"));
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        StationDependencyDialog.this.setVisible(false);
                    }
                });

            }
        }
    }

    /**
     * ����� ������������ ��� ������� �� ������� ��������
     * @param list ������ ������� �� ������� �������
     * @param distrID �� �.�. �������
     */
    public void searchStationsBeforeOpen(List<VariantCalcTime> list, int distrID, LocomotiveType loc, int oe, VariantCalc varCalc) {
        try {
            this.loc = loc;
            this.oe = oe;
            this.varCalc = varCalc;
            setList(list);
            setDistrID(distrID);
            distr = new DistrEntity(GDPDAOFactoryCreater.getGDPDAOFactory(), distrID);

            fillListStationHolder();
            fillListStation();
            fillListSpanSt();

            // ����� ������������ ��� �������
            List<StationHolder> listH = shTableModel.getList();
            List<StationHolder> loaded = StationDependencyXML.loadStationHolderList(distrID, distr.getDistrName(), varCalc.getOe());
            if (loaded.size() > 0) {
                // ��������� ���������� �� �����
                for (StationHolder sth : loaded) {
                    // ������������, ���� ��� ����
                    if (sth.distrSt != null) {
                        for (StationHolder sthh : listH) {
                            if (sth.varSt.equals(sthh.varSt)) {
                                DistrStation dst = distr.getDistrStationFromAll(sth.distrSt.getIdSt());
                                sthh.distrSt = dst.getStation();
                            }
                        }
                    }
                    if (sth.spanSt != null) {
                        for (StationHolder sthh : listH) {
                            if (sth.varSt.equals(sthh.varSt)) {
                                sthh.spanSt = ((DistrSpanSt) distr.getDistrPoint(sth.spanSt.getIdSpanst(), TypePoint.StopPoint)).getSpanSt();
                            }
                        }
                    }
                }
            }
            // ��� ��� � ���� ��� �������� - �������� � �������
            for (StationHolder sth : listH) {
                if (sth.varSt.isTown() && sth.spanSt == null) {
                    sth.spanSt = searchSpanSt(sth.varSt);
                }
                if (sth.distrSt == null) {
                    sth.distrSt = searchDistrSt(sth.varSt);
                }
            }
            shTable.repaint();
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }

    /** ����� ������������ � �������� �.�. ������� �� ���� ��� */
    private Station searchDistrSt(VariantCalcTime varSt) {
        List<DistrStation> listSt = distr.getListSt();
        for (DistrStation dst : listSt) {
            if (dst.getStation().getCodeESR().equals(varSt.getCodeESR())) {
                return dst.getStation();
            }
        }
        return null;
    }

    /** ����� ������������ � �.�. �.�. ������� �� ���� ��� */
    private SpanSt searchSpanSt(VariantCalcTime varSt) {
        List<SpanSt> listSt = sptTableModel.getList();
        for (SpanSt dst : listSt) {
            if (dst.getCodeESR().equals(varSt.getCodeESR())) {
                return dst;
            }
        }
        return null;
    }

    /**
     * ������� � ��������� ������
     * �������� ��� ������������� ������� �� ������� �������� �������� �� �� �������
     * � ������� ������������
     */
    private void fillListStationHolder() {
        List<StationHolder> listH = new ArrayList<>();
        for (VariantCalcTime vct : list) {
            StationHolder sh = new StationHolder();
            sh.varSt = vct;
            listH.add(sh);
        }
        shTableModel.setList(listH);
    }

    /**
     * ������� � ��������� ������ ������� �� �� �������
     * � �������
     */
    private void fillListStation() {
        List<Station> listSt = new ArrayList<>();
        List<DistrStation> listDSt = distr.getListSt();
        for (DistrStation dst : listDSt) {
            listSt.add(dst.getStation());
        }
        sdTableModel.setList(listSt);
    }

    /**
     * ������� � ��������� ������ �.�. �� �� �������
     * � �������
     */
    private void fillListSpanSt() {
        List<SpanSt> listSt = new ArrayList<>();
        List<DistrPoint> listDSP = distr.getListDistrPoint();
        for (DistrPoint dsp : listDSP) {
            if (dsp instanceof DistrSpanSt) {
                listSt.add(((DistrSpanSt) dsp).getSpanSt());
            }
        }
        sptTableModel.setList(listSt);
    }

    public List<VariantCalcTime> getList() {
        return list;
    }

    public void setList(List<VariantCalcTime> list) {
        this.list = list;
    }

    public int getDistrID() {
        return distrID;
    }

    public void setDistrID(int distrID) {
        this.distrID = distrID;
    }

}
