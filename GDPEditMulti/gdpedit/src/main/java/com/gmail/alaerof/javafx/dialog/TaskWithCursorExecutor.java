package com.gmail.alaerof.javafx.dialog;

import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.concurrent.Task;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class TaskWithCursorExecutor {
    /** logger */
    private static Logger logger = LogManager.getLogger(TaskWithCursorExecutor.class);

    static class TimeInfo{
        long tm1;
        long tm2;
    }

    /*
        Task task = new Task<Void>() {
            @Override
            protected Void call() {
                GDPXMLHandler.saveGDPToXML(new File(NAME_TEMP_FILE), distrEntity.getIdDistr());
                return null;
            }
        };

     */

    public static void executeTaskWithCursor(Task task, Scene scene) {
        task.setOnScheduled((v) -> scene.setCursor(Cursor.WAIT));
        task.setOnSucceeded((v) -> scene.setCursor(Cursor.DEFAULT));
        task.setOnFailed((v) -> scene.setCursor(Cursor.DEFAULT));

        ExecutorService service = Executors.newFixedThreadPool(1);
        service.execute(task);
    }

    public static void executeTaskWithCursorAndAlert(Task task, Scene scene, OnTaskFinished taskFinished) {
        final TimeInfo timeInfo = new TimeInfo();
        task.setOnScheduled((v) -> {
            timeInfo.tm1 = Calendar.getInstance().getTimeInMillis();
            scene.setCursor(Cursor.WAIT);
        });

        task.setOnSucceeded((v) -> {
            scene.setCursor(Cursor.DEFAULT);

            timeInfo.tm2 = Calendar.getInstance().getTimeInMillis();
            double sec = (double) (timeInfo.tm2 - timeInfo.tm1) / 1000.0;
            String status = "All done: " + sec + " sec";
            logger.debug(status);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("");
            alert.setHeaderText(status);
            alert.showAndWait();
            if (taskFinished != null) {
                taskFinished.doOnTaskFinished();
            }
        });

        task.setOnFailed((v) -> {
            scene.setCursor(Cursor.DEFAULT);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("");
            alert.setHeaderText("ERROR: see logs/gdpedit.log");
            alert.showAndWait();
            if (taskFinished != null) {
                taskFinished.doOnTaskFinished();
            }
        });

        ExecutorService service = Executors.newFixedThreadPool(1);
        service.execute(task);
    }

}
