package com.gmail.alaerof.dialog.spantimetown;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.SpanStTime;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
class DistrPointTime {
    int num;
    double timeO;
    double timeE;
}
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class SpanTimeTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("spantimetown.station"),
            bundle.getString("spantimetown.odd"),
            bundle.getString("spantimetown.even") };
    private DistrEntity distrEntity;
    private LocomotiveType locType;
    private List<DistrPointTime> listTime;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spantimetown", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (distrEntity != null) {
            return distrEntity.getListDistrPoint().size();
        }
        return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (distrEntity != null) {
            DistrPoint dp = distrEntity.getListDistrPoint().get(row);
            DistrPointTime dpt = null;
            if (listTime != null) {
                dpt = listTime.get(row);
            }
            switch (col) {
            case 0:
                String name = bundle.getString("spantimetown.dotspaces");
                if (dp.getTypePoint() == DistrPoint.TypePoint.StopPoint) {
                    name = bundle.getString("spantimetown.op");
                }
                return name + dp.getNamePoint();
            case 1:
                if (dpt != null)
                    return dpt.timeO;
                else
                    return 0;
            case 2:
                if (dpt != null)
                    return dpt.timeE;
                else
                    return 0;
            default:
                return null;
            }
        }
        return null;
    }

    public LocomotiveType getLocType() {
        return locType;
    }

    public void setLocType(LocomotiveType locType) {
        this.locType = locType;
        fillDistrPointTime();
    }

    public DistrEntity getDistrEntity() {
        return distrEntity;
    }

    public void setDistrEntity(DistrEntity distrEntity) {
        this.distrEntity = distrEntity;
    }

    private void fillDistrPointTime() {
        listTime = new ArrayList<>();
        double timeO = 0;
        if (distrEntity != null) {
            List<DistrPoint> listPoint = distrEntity.getListDistrPoint();
            for (int i = 0; i < listPoint.size(); i++) {
                DistrPoint dp = listPoint.get(i);
                DistrPointTime dpt = new DistrPointTime();
                dpt.num = dp.getNum();
                SpanStTime spanStTime = dp.getSpanStTime(locType);
                dpt.timeO = timeO / 100;
                if (spanStTime != null) {
                    double x = 0;
                    x = Math.round(spanStTime.getTmoveE() * 100);
                    dpt.timeE = x / 100;
                    timeO = Math.round(spanStTime.getTmoveO() * 100);
                } else {
                    timeO = 0;
                }
                listTime.add(dpt);
            }
        }
    }
}
