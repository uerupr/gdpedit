package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.Dimension;
import java.awt.Polygon;

import com.gmail.alaerof.util.CalcString;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class LineHor extends NewLine {
    private static final long serialVersionUID = 1L;
    public static final int NO_CAPTION_HEIGHT = 10;

    public LineHor(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams,
                   TrainThreadElement trainThreadElement) {
        super(generalParams, selfParams, trainThreadElement);
        direction = Direction.hor;
    }

    @Override
    protected Polygon getPolygon() {
        int npoints = 10;
        int x[] = new int[npoints];
        int y[] = new int[npoints];
        int ph = this.getIndent().height;
        int pw = this.getIndent().width;
        int w = getOriginalSize().width;// getSize().width;
        int h = getSize().height;
        x[0] = pw;
        y[0] = h - ph;

        x[1] = pw;
        y[1] = y[0] - ph;

        // x[2] = x[3] = x[4] = x[5] = w - this.getCaptionSize().width;
        // y[2] = y[3] = y[4] = y[5] = y[1];

        x[6] = x[0] + w;
        y[6] = y[1];

        x[7] = x[6];
        y[7] = y[0];

        x[8] = x[6];
        y[8] = h;

        x[9] = x[0];
        y[9] = y[8];

        x[2] = x[1];
        y[2] = y[1] - this.getCaptionSize().height;

        x[3] = x[1] + this.getCaptionSize().width;
        y[3] = y[1];

        x[4] = x[6] - this.getCaptionSize().width;
        y[4] = y[1];

        x[5] = x[6];
        y[5] = y[6] - this.getCaptionSize().height;

        captionPoint.x = x[4];
        captionPoint.y = y[4];
        begin.x = x[0];
        begin.y = y[0];
        end.x = x[7];
        end.y = y[7];

        captionPointGDP1.x = x[1] - pw;
        captionPointGDP1.y = y[1] - (h - ph);
        captionPointGDP.x = x[4] - pw;
        captionPointGDP.y = y[4] - (h - ph);
        beginGDP.x = x[0] - pw;
        beginGDP.y = y[0] - (h - ph);
        endGDP.x = x[7] - pw;
        endGDP.y = y[7] - (h - ph);

        return new Polygon(x, y, npoints);
    }

    @Override
    public Dimension getIndent() {
        int h = (int) (ADD_WIDTH + selfParams.lineWidth);
        int w = 5;
        return new Dimension(w, h);
    }

    @Override
    protected Dimension getCaptionSize() {
        int h = CalcString.getStringH(generalParams.captionSt);
        int w = CalcString.getStringW(generalParams.captionSt, selfParams.getCaptionValue()) + 2;
        return new Dimension(w, h);
    }

    @Override
    public void setPosition(int x, int y) {
        xleft = x;
        ytop = y;
        int ph = getIndent().height;
        int pw = getIndent().width;
        int w = Math.max(getOriginalSize().width, getCaptionSize().width);
        int h = getOriginalSize().height - ph;
        this.setBounds(x - pw , y - h, w + 2 * pw, h + ph);

    }
}
