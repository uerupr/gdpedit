package com.gmail.alaerof.javafx.dialog.calculatetimeenergy;

import com.gmail.alaerof.dao.dto.energy.EnergyType;
import com.gmail.alaerof.dao.dto.energy.ExpenseRate;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateEnergy;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateNormative;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.ExpenseRateTransfer;
import com.gmail.alaerof.util.DataXMLUtil;
import com.gmail.alaerof.util.XMLUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class CalculateTimeEnergyXML {
    private static Logger logger = LogManager.getLogger(CalculateTimeEnergyXML.class);

    public static ExpenseRateTransfer loadExpRateXML(File file){
        ExpenseRateTransfer expenseRateTransfer = new ExpenseRateTransfer();
        try {
            Document doc = DataXMLUtil.loadDocument(file);
            Element root = doc.getDocumentElement();
            expenseRateTransfer.setDbMode(XMLUtil.getAttribute(root,"dbMode"));
            NodeList nodeList = doc.getElementsByTagName("Energy");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element el = (Element) nodeList.item(i);
                EnergyType energyType = EnergyType.getType(el.getAttribute("EnergyType"));
                float v = Float.valueOf(el.getAttribute("ExpRateEnValue"));
                ExpenseRateEnergy expenseRateEnergy = new ExpenseRateEnergy(energyType, v);
                if (energyType.equals(EnergyType.teplo)) {
                    expenseRateTransfer.setTeplo(expenseRateEnergy);
                }
                if (energyType.equals(EnergyType.electro)) {
                    expenseRateTransfer.setElectro(expenseRateEnergy);
                }
            }
            nodeList = doc.getElementsByTagName("ExpRate");
            List<ExpenseRate> expenseRateList = new ArrayList<>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element el = (Element) nodeList.item(i);
                float moveExpRate = Float.valueOf(el.getAttribute("MoveExpRate"));
                float stopExpRate = Float.valueOf(el.getAttribute("StopExpRate"));
                TrainType trainType = TrainType.getType(el.getAttribute("TrainType"));
                EnergyType energyType = EnergyType.getType(el.getAttribute("EnergyType"));
                ExpenseRate expenseRate = new ExpenseRate(moveExpRate, stopExpRate, trainType, energyType);
                expenseRateList.add(expenseRate);
            }
            expenseRateTransfer.setExpenseRateList(expenseRateList);
            nodeList = doc.getElementsByTagName("Normative");
            List<ExpenseRateNormative> expenseRateNormativeList = new ArrayList<>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element el = (Element) nodeList.item(i);
                int idLtype = Integer.parseInt(el.getAttribute("IDltype"));
                String lType = el.getAttribute("Ltype");
                EnergyType energyType = EnergyType.getType(el.getAttribute("EnergyType"));
                float downUpExp = Float.valueOf(el.getAttribute("DownUpExp"));
                ExpenseRateNormative expenseRateNormative = new ExpenseRateNormative(idLtype, lType, energyType, downUpExp);
                expenseRateNormativeList.add(expenseRateNormative);
            }
            expenseRateTransfer.setExpenseRateNormativeList(expenseRateNormativeList);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return expenseRateTransfer;
    }

    public static void saveExpRateXML(File file, ExpenseRateTransfer expenseRateTransfer){
        try {
            Document doc = DataXMLUtil.getNewDocument();
            Element rootElement = doc.createElement("ExpenseRates");
            rootElement.setAttribute("dbMode", "" + expenseRateTransfer.getDbMode());
            doc.appendChild(rootElement);
            Element energy = doc.createElement("Energys");
            rootElement.appendChild(energy);
            Element el = doc.createElement("Energy");
            energy.appendChild(el);
            el.setAttribute("EnergyType", "" + expenseRateTransfer.getTeplo().getEnergyType().getString());
            el.setAttribute("ExpRateEnValue", "" + expenseRateTransfer.getTeplo().getExpRateEnValue());
            el = doc.createElement("Energy");
            energy.appendChild(el);
            el.setAttribute("EnergyType", "" + expenseRateTransfer.getElectro().getEnergyType().getString());
            el.setAttribute("ExpRateEnValue", "" + expenseRateTransfer.getElectro().getExpRateEnValue());
            Element expRate = doc.createElement("ExpRates");
            rootElement.appendChild(expRate);
            for (ExpenseRate expenseRate : expenseRateTransfer.getExpenseRateList()) {
                el = doc.createElement("ExpRate");
                expRate.appendChild(el);
                el.setAttribute("MoveExpRate", "" + expenseRate.getMoveExpRate());
                el.setAttribute("StopExpRate", "" + expenseRate.getStopExpRate());
                el.setAttribute("TrainType", "" + expenseRate.getTrainType().getString());
                el.setAttribute("EnergyType", "" + expenseRate.getEnergyType().getString());
            }
            Element normative = doc.createElement("Normatives");
            rootElement.appendChild(normative);
            for (ExpenseRateNormative expenseRateNormative : expenseRateTransfer.getExpenseRateNormativeList()) {
                el = doc.createElement("Normative");
                normative.appendChild(el);
                el.setAttribute("IDltype", "" + expenseRateNormative.getIdLType());
                el.setAttribute("Ltype", "" + expenseRateNormative.getLtype());
                el.setAttribute("EnergyType", "" + expenseRateNormative.getEnergyType().getString());
                el.setAttribute("DownUpExp", "" + expenseRateNormative.getDownUpExp());
            }
            DataXMLUtil.saveDocument(file, doc);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }
}
