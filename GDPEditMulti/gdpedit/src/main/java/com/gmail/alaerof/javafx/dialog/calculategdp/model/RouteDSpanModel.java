package com.gmail.alaerof.javafx.dialog.calculategdp.model;

import com.gmail.alaerof.dao.dto.routed.RouteDSpan;
import com.gmail.alaerof.entity.DistrSpan;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class RouteDSpanModel {
    private RouteDSpan routeDSpan;
    private DistrSpan distrSpan;
    private IntegerProperty num;
    private StringProperty name;

    public RouteDSpanModel(RouteDSpan routeDSpan, DistrSpan distrSpan) {
        this.routeDSpan = routeDSpan;
        this.distrSpan = distrSpan;
        num = new SimpleIntegerProperty(routeDSpan.getNum());
        name = new SimpleStringProperty(distrSpan.getSpanScale().getName());
    }

    public RouteDSpan getRouteDSpan() {
        return routeDSpan;
    }

    public int getNum() {
        return num.get();
    }

    public IntegerProperty numProperty() {
        return num;
    }

    public void setNum(int num) {
        this.num.set(num);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public static List<RouteDSpan> extractRouteDSpan(List<RouteDSpanModel> selected) {
        List<RouteDSpan> list = new ArrayList<>();
        for (RouteDSpanModel spanModel : selected) {
            list.add(spanModel.getRouteDSpan());
        }
        return list;
    }
}
