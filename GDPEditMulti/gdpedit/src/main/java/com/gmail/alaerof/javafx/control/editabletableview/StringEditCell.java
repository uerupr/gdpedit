package com.gmail.alaerof.javafx.control.editabletableview;

import java.util.function.Function;
import java.util.regex.Pattern;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ContentDisplay;

/**
 * {@link StringProperty} editable table cell
 * @param <T> Entity with property-fields to be edited in TableView
 */
public class StringEditCell<T> extends EditCell<T, String, StringProperty> {
    /**
     * {@inheritDoc}
     */
    public StringEditCell(Function<T, StringProperty> property) {
        super(property);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void bindItemProperty() {
        textProperty().bind(itemProperty());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String parseText(String text) {
        return text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Pattern getSimpleTypePattern() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void commitEdit(String value) {
        super.commitEdit(value);
        T entity = getTableView().getItems().get(getIndex()) ;
        StringProperty cellProperty = property.apply(entity);
        cellProperty.set(value);
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }
}
