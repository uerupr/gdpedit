package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.groupp.GroupP;
import com.gmail.alaerof.javafx.control.RoundedDoubleProperty;
import com.gmail.alaerof.util.FileUtil;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

public class CalcModel {
    public static int LAYER_ROOT = 0;
    public static int LAYER_GROUP = 1;
    public static int LAYER_VALUE = 2;
    private static int MIN_HOUR = 60;
    private static int M_KM = 1000;

    private GroupP groupP;
    private LocTypeModel locType;
    /** ������� ������������� ������ ������: root, group, value
     * ������������ � ApplyLocomotiveType ������ �� ������ group*/
    private int layer;

    private StringProperty groupPName;
    private StringProperty locTypeName;

    // �������
    private IntegerProperty trainCount;
    // ������-��, ��
    private DoubleProperty lenM;
    private DoubleProperty lenKM;
    // Tu ����� ���������� ������ �� �������, �
    private DoubleProperty timeWithStop;
    private DoubleProperty timeWithStopH;
    // Tt ����� ���������� ������ �� ������� ��� ����� ������� �������, ���
    private DoubleProperty timeWithoutStop;
    private DoubleProperty timeWithoutStopH;
    /** ����������� (�� ���������) ����� ���������� ������ �� ������� ����� ��� �������� � ����������, ��� */
    private DoubleProperty timeNormMove;
    private DoubleProperty timeNormMoveH;
    // ���������� ��������, ��/�
    private DoubleProperty speedWithStop;
    // ����������� ��������, ��/�
    private DoubleProperty speedWithoutStop;
    // ����������� ����������� ��������, ��/�
    private DoubleProperty speedNorm;

    // ���������� ��������� �� ������� ������ � �����
    private IntegerProperty stopCount;

    // Distr_Train_Energy
    // Eu ������� ������� � ������ �������, ��/� (���/�) �������� � ���, �� ��� �� �����
    private DoubleProperty energyWithStop;
    // Et ������� ������� ��� ����� ������� (�� � ������ �������� � ����������), ��/� (���/�)
    private DoubleProperty energyWithoutStop;
    // Et ������� ������� ��� ����� �������, �������� � ����������,  ��/� (���/�)
    private DoubleProperty energyMove;

    // ����. �� ����./������.
    private DoubleProperty downUpExpense;
    // ����. �� ��������
    private DoubleProperty moveExpense;
    // ����. �� ������� (�������)
    private DoubleProperty stopExpense;
    // ����. ������
    private DoubleProperty normExpense;
    // ����������� �������������
    private DoubleProperty effect;
    // Vuch/Vtex
    private DoubleProperty vv;

    public CalcModel(GroupP groupP, String name, int layer) {
        this.groupP = groupP;
        this.layer = layer;
        this.groupPName = new SimpleStringProperty(name);

        this.locTypeName = new SimpleStringProperty("");
        this.trainCount = new SimpleIntegerProperty(0);
        this.lenM = new RoundedDoubleProperty(0.0);
        this.lenKM = new RoundedDoubleProperty(0.0);
        this.timeWithStopH = new RoundedDoubleProperty(0.0);
        this.timeWithoutStopH = new RoundedDoubleProperty(0.0);
        this.timeNormMoveH = new RoundedDoubleProperty(0.0);
        this.timeWithStop = new RoundedDoubleProperty(0.0);
        this.timeWithoutStop = new RoundedDoubleProperty(0.0);
        this.timeNormMove = new RoundedDoubleProperty(0.0);
        this.speedNorm = new RoundedDoubleProperty(0.0);
        this.speedWithoutStop = new RoundedDoubleProperty(0.0);
        this.speedWithStop = new RoundedDoubleProperty(0.0);
        this.energyMove = new RoundedDoubleProperty(0.0);
        this.energyWithoutStop = new RoundedDoubleProperty(0.0);
        this.energyWithStop = new RoundedDoubleProperty(0.0);
        this.stopCount = new SimpleIntegerProperty(0);

        this.downUpExpense = new RoundedDoubleProperty(0);
        this.moveExpense = new RoundedDoubleProperty(0);
        this.stopExpense = new RoundedDoubleProperty(0);
        this.normExpense = new RoundedDoubleProperty(0);

        this.effect = new RoundedDoubleProperty(0);
        this.vv = new RoundedDoubleProperty(0);
    }

    public double getVv() {
        return vv.get();
    }

    public DoubleProperty vvProperty() {
        return vv;
    }

    public void setVv(double vv) {
        this.vv.set(vv);
    }

    public int getLayer() {
        return layer;
    }

    public GroupP getGroupP() {
        return groupP;
    }

    public String getGroupPName() {
        return groupPName.get();
    }

    public StringProperty groupPNameProperty() {
        return groupPName;
    }

    public void setGroupPName(String groupPName) {
        this.groupPName.set(groupPName);
    }

    public LocTypeModel getLocType() {
        return locType;
    }

    public void setLocType(LocTypeModel locType) {
        this.locType = locType;
        this.locTypeName.set(locType.getLocName());
    }

    public double getLenM() {
        return lenM.get();
    }

    public DoubleProperty lenMProperty() {
        return lenM;
    }

    public void setLenM(double lenM) {
        this.lenM.set(lenM);
    }

    public double getTimeWithStop() {
        return timeWithStop.get();
    }

    public DoubleProperty timeWithStopProperty() {
        return timeWithStop;
    }

    public void setTimeWithStop(double timeWithStop) {
        this.timeWithStop.set(timeWithStop);
    }

    public double getTimeWithoutStop() {
        return timeWithoutStop.get();
    }

    public DoubleProperty timeWithoutStopProperty() {
        return timeWithoutStop;
    }

    public void setTimeWithoutStop(double timeWithoutStop) {
        this.timeWithoutStop.set(timeWithoutStop);
    }

    public double getTimeNormMove() {
        return timeNormMove.get();
    }

    public DoubleProperty timeNormMoveProperty() {
        return timeNormMove;
    }

    public void setTimeNormMove(double timeNormMove) {
        this.timeNormMove.set(timeNormMove);
    }

    public double getEffect() {
        double value = (getMoveExpense() + getStopExpense() + getDownUpExpense());

        if (value > 0) {
            value = getNormExpense() / value;
        }
        return value;
    }

    public DoubleProperty effectProperty() {
        effect.set(getEffect());
        return effect;
    }

    public void calcV() {
        this.lenKM.set(this.getLenM()/M_KM);
        this.timeWithStopH.set(this.getTimeWithStop()/MIN_HOUR);
        this.timeWithoutStopH.set(this.getTimeWithoutStop()/MIN_HOUR);
        this.timeNormMoveH.set(this.getTimeNormMove()/MIN_HOUR);
        double v;
        if (this.timeWithStopH.doubleValue() > 0) {
            v = this.lenKM.doubleValue() / this.timeWithStopH.doubleValue();
            this.speedWithStop.set(v);
        }
        if (this.timeWithoutStopH.doubleValue() > 0) {
            v = this.lenKM.doubleValue() / this.timeWithoutStopH.doubleValue();
            this.speedWithoutStop.set(v);
        }
        if (this.timeNormMoveH.doubleValue() > 0) {
            v = this.lenKM.doubleValue() / this.timeNormMoveH.doubleValue();
            this.speedNorm.set(v);
        }
        // Vu/Vtex
        if (this.speedWithoutStop.doubleValue() > 0) {
            v = this.speedWithStop.doubleValue()/ this.speedWithoutStop.doubleValue();
            this.vv.set(v);
        }
    }

    public int getStopCount() {
        return stopCount.get();
    }

    public IntegerProperty stopCountProperty() {
        return stopCount;
    }

    public void setStopCount(int stopCount) {
        this.stopCount.set(stopCount);
    }

    public double getDownUpExpense() {
        return downUpExpense.get();
    }

    public DoubleProperty downUpExpenseProperty() {
        return downUpExpense;
    }

    public void setDownUpExpense(double downUpExpense) {
        this.downUpExpense.set(downUpExpense);
    }

    public double getMoveExpense() {
        return moveExpense.get();
    }

    public DoubleProperty moveExpenseProperty() {
        return moveExpense;
    }

    public void setMoveExpense(double moveExpense) {
        this.moveExpense.set(moveExpense);
    }

    public double getStopExpense() {
        return stopExpense.get();
    }

    public DoubleProperty stopExpenseProperty() {
        return stopExpense;
    }

    public void setStopExpense(double stopExpense) {
        this.stopExpense.set(stopExpense);
    }

    public double getNormExpense() {
        return normExpense.get();
    }

    public DoubleProperty normExpenseProperty() {
        return normExpense;
    }

    public void setNormExpense(double normExpense) {
        this.normExpense.set(normExpense);
    }

    public String getLocTypeName() {
        return locTypeName.get();
    }

    public StringProperty locTypeNameProperty() {
        return locTypeName;
    }

    public void setLocTypeName(String locTypeName) {
        this.locTypeName.set(locTypeName);
    }

    public int getTrainCount() {
        return trainCount.get();
    }

    public IntegerProperty trainCountProperty() {
        return trainCount;
    }

    public void setTrainCount(int trainCount) {
        this.trainCount.set(trainCount);
    }

    public double getLenKM() {
        return lenKM.get();
    }

    public DoubleProperty lenKMProperty() {
        return lenKM;
    }

    public void setLenKM(double lenKM) {
        this.lenKM.set(lenKM);
    }

    public double getTimeWithStopH() {
        return timeWithStopH.get();
    }

    public DoubleProperty timeWithStopHProperty() {
        return timeWithStopH;
    }

    public void setTimeWithStopH(double timeWithStopH) {
        this.timeWithStopH.set(timeWithStopH);
    }

    public double getTimeWithoutStopH() {
        return timeWithoutStopH.get();
    }

    public DoubleProperty timeWithoutStopHProperty() {
        return timeWithoutStopH;
    }

    public void setTimeWithoutStopH(double timeWithoutStopH) {
        this.timeWithoutStopH.set(timeWithoutStopH);
    }

    public double getTimeNormMoveH() {
        return timeNormMoveH.get();
    }

    public DoubleProperty timeNormMoveHProperty() {
        return timeNormMoveH;
    }

    public void setTimeNormMoveH(double timeNormMoveH) {
        this.timeNormMoveH.set(timeNormMoveH);
    }

    public double getSpeedWithStop() {
        return speedWithStop.get();
    }

    public DoubleProperty speedWithStopProperty() {
        return speedWithStop;
    }

    public void setSpeedWithStop(double speedWithStop) {
        this.speedWithStop.set(speedWithStop);
    }

    public double getSpeedWithoutStop() {
        return speedWithoutStop.get();
    }

    public DoubleProperty speedWithoutStopProperty() {
        return speedWithoutStop;
    }

    public void setSpeedWithoutStop(double speedWithoutStop) {
        this.speedWithoutStop.set(speedWithoutStop);
    }

    public double getSpeedNorm() {
        return speedNorm.get();
    }

    public DoubleProperty speedNormProperty() {
        return speedNorm;
    }

    public void setSpeedNorm(double speedNorm) {
        this.speedNorm.set(speedNorm);
    }

    public double getEnergyWithStop() {
        return energyWithStop.get();
    }

    public DoubleProperty energyWithStopProperty() {
        return energyWithStop;
    }

    public void setEnergyWithStop(double energyWithStop) {
        this.energyWithStop.set(energyWithStop);
    }

    public double getEnergyWithoutStop() {
        return energyWithoutStop.get();
    }

    public DoubleProperty energyWithoutStopProperty() {
        return energyWithoutStop;
    }

    public void setEnergyWithoutStop(double energyWithoutStop) {
        this.energyWithoutStop.set(energyWithoutStop);
    }

    public double getEnergyMove() {
        return energyMove.get();
    }

    public DoubleProperty energyMoveProperty() {
        return energyMove;
    }

    public void setEnergyMove(double energyMove) {
        this.energyMove.set(energyMove);
    }

    @Override
    public String toString() {
        return "CalcModel{" +
                "groupP=" + groupP +
                ", locType=" + locType +
                ", layer=" + layer +
                ", groupPName=" + groupPName +
                ", locTypeName=" + locTypeName +
                ", trainCount=" + trainCount +
                ", lenKM=" + lenKM +
                ", timeWithStopH=" + timeWithStopH +
                ", timeWithoutStopH=" + timeWithoutStopH +
                ", timeNormMoveH=" + timeNormMoveH +
                ", speedWithStop=" + speedWithStop +
                ", speedWithoutStop=" + speedWithoutStop +
                ", speedNorm=" + speedNorm +
                ", energyWithStop=" + energyWithStop +
                ", energyWithoutStop=" + energyWithoutStop +
                ", energyMove=" + energyMove +
                ", stopCount=" + stopCount +
                ", moveExpense=" + moveExpense +
                ", downUpExpense=" + downUpExpense +
                ", stopExpense=" + stopExpense +
                ", normExpense=" + normExpense +
                '}';
    }

    public void addValues(CalcModel model){
        this.setTrainCount(this.getTrainCount() + model.getTrainCount());
        this.setLenM(this.getLenM() + model.getLenM());
        this.setTimeWithoutStop(this.getTimeWithoutStop() + model.getTimeWithoutStop());
        this.setTimeWithStop(this.getTimeWithStop() + model.getTimeWithStop());
        this.setTimeNormMove(this.getTimeNormMove() + model.getTimeNormMove());
        this.setStopCount(this.getStopCount() + model.getStopCount());
        this.setEnergyMove(this.getEnergyMove() + model.getEnergyMove());
        this.setEnergyWithoutStop(this.getEnergyWithoutStop() + model.getEnergyWithoutStop());
        this.setEnergyWithStop(this.getEnergyWithStop() + model.getEnergyWithStop());

        this.calcV();

        this.setNormExpense(this.getNormExpense() + model.getNormExpense());
        this.setStopExpense(this.getStopExpense() + model.getStopExpense());
        this.setMoveExpense(this.getMoveExpense() + model.getMoveExpense());
        this.setDownUpExpense(this.getDownUpExpense() + model.getDownUpExpense());

    }

    public String getTabValuesString() {
        String s = FileUtil.CSV_SEPARATOR;
        String loc = locType != null ? locType.getLocName() : "";
        String str =
                s + groupPName.getValue() +
                s + loc +
                s + trainCount.getValue() +
                s + lenKM.getValue() +
                s + timeWithStop.getValue() +
                s + timeWithoutStop.getValue() +
                s + timeNormMove.getValue() +
                s + timeWithStopH.getValue() +
                s + timeWithoutStopH.getValue() +
                s + timeNormMoveH.getValue() +
                s + speedWithStop.getValue() +
                s + speedWithoutStop.getValue() +
                s + speedNorm.getValue() +
                s + energyWithStop.getValue() +
                s + energyWithoutStop.getValue() +
                s + energyMove.getValue() +
                s + stopCount.getValue() +
                s + moveExpense.getValue() +
                s + downUpExpense.getValue() +
                s + stopExpense.getValue() +
                s + normExpense.getValue() +
                s + getEffect()+
                s + vv.getValue();
        str = str.replaceAll("\\.",",");
        return str;
    }

    public List<Object> getDistrTabValuesList(){
        List<Object> list = new ArrayList<>();
        String loc = locType != null ? locType.getLocName() : "";
        list.add(groupPName.getValue());
        list.add(loc);
        list.add(trainCount.getValue());
        list.add(lenKM.getValue());
        list.add(timeWithStop.getValue());
        list.add(timeWithoutStop.getValue());
        list.add(timeNormMove.getValue());
        list.add(timeWithStopH.getValue());
        list.add(timeWithoutStopH.getValue());
        list.add(timeNormMoveH.getValue());
        list.add(speedWithStop.getValue());
        list.add(speedWithoutStop.getValue());
        list.add(speedNorm.getValue());
        list.add(energyWithStop.getValue());
        list.add(energyWithoutStop.getValue());
        list.add(energyMove.getValue());
        list.add(stopCount.getValue());
        list.add(moveExpense.getValue());
        list.add(downUpExpense.getValue());
        list.add(stopExpense.getValue());
        list.add(normExpense.getValue());
        list.add(getEffect());
        list.add(vv.getValue());
        return list;
    }

    public List<Object> getDirTabValuesList(){
        List<Object> list = new ArrayList<>();
        list.add(groupPName.getValue());
        list.add(trainCount.getValue());
        list.add(lenKM.getValue());
        list.add(timeWithStop.getValue());
        list.add(timeWithoutStop.getValue());
        list.add(timeWithStopH.getValue());
        list.add(timeWithoutStopH.getValue());
        list.add(speedWithStop.getValue());
        list.add(speedWithoutStop.getValue());
        list.add(vv.getValue());
        return list;
    }

    /**
     * ������� ���������� ���������� ��������
     */
    public void clearCalc() {
        trainCount.set(0);
        lenKM.set(0);
        lenM.set(0);
        timeWithStop.set(0);
        timeWithoutStop.set(0);
        timeNormMove.set(0);
        timeWithStopH.set(0);
        timeWithoutStopH.set(0);
        timeNormMoveH.set(0);
        speedWithStop.set(0);
        speedWithoutStop.set(0);
        speedNorm.set(0);
        energyWithStop.set(0);
        energyWithoutStop.set(0);
        energyMove.set(0);
        stopCount.set(0);
        moveExpense.set(0);
        downUpExpense.set(0);
        stopExpense.set(0);
        normExpense.set(0);
        effect.set(0);
        vv.setValue(0);
    }
}
