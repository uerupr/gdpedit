package com.gmail.alaerof.javafx.dialog.energyfromtr.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ImportTRModel {
    // Connect
    private StringProperty connect;
    // Name
    private StringProperty nameSt;
    // CodeESR
    private StringProperty codeESR;
    // T1Oil
    private DoubleProperty t1Oil;
    // T2Oil
    private DoubleProperty t2Oil;
    // TupOil
    private DoubleProperty tUpOil;
    // TdownOil
    private DoubleProperty tDownOil;
    // NewName
    private StringProperty newNameSt;
    // NewCodeESR
    private StringProperty newCodeESR;
    // CodeESR2
    private StringProperty codeESR2;
    // IDstation
    private IntegerProperty idStation;
    // IDspanSt
    private IntegerProperty idSpanSt;
    // StationOrSpanSt
    private IntegerProperty stationOrSpanSt; // 0 - ��� �� ����������, 1 - station, 2 - spanSt

    public ImportTRModel() {
        this.connect = new SimpleStringProperty();
        this.nameSt = new SimpleStringProperty();
        this.codeESR = new SimpleStringProperty();
        this.t1Oil = new SimpleDoubleProperty();
        this.t2Oil = new SimpleDoubleProperty();
        this.tUpOil = new SimpleDoubleProperty();
        this.tDownOil = new SimpleDoubleProperty();
        this.newNameSt = new SimpleStringProperty();
        this.newCodeESR = new SimpleStringProperty();
        this.codeESR2 = new SimpleStringProperty();
        this.idStation = new SimpleIntegerProperty();
        this.idSpanSt = new SimpleIntegerProperty();
        this.stationOrSpanSt = new SimpleIntegerProperty();
    }
    public StringProperty connectProperty() {return connect;}
    public StringProperty nameStProperty() {
        return nameSt;
    }
    public StringProperty codeESRProperty() {
        return codeESR;
    }
    public DoubleProperty t1OilProperty() {return t1Oil;}
    public DoubleProperty t2OilProperty() {return t2Oil;}
    public DoubleProperty tUpOilProperty() {return tUpOil;}
    public DoubleProperty tDownOilProperty() {return tDownOil;}
    public StringProperty newNameStProperty() {
        return newNameSt;
    }
    public StringProperty newCodeESRProperty() {
        return newCodeESR;
    }
    public StringProperty codeESR2Property() {
        return codeESR2;
    }
    public IntegerProperty idStationProperty() {return idStation;}
    public IntegerProperty idSpanStProperty() {return idSpanSt;}
    public IntegerProperty stationOrSpanStProperty() {return stationOrSpanSt;}
    public void setNewParamStation (StringProperty newNameSt, StringProperty newCodeESR, StringProperty codeESR2, IntegerProperty idStation){
        this.newNameSt = newNameSt;
        this.newCodeESR = newCodeESR;
        this.codeESR2 = codeESR2;
        this.idStation = idStation;
        this.stationOrSpanSt.set(1);
        this.connect.set("Ok");
    }
    public void setNewParamSpanSt (StringProperty newNameSt, StringProperty newCodeESR, StringProperty codeESR2, IntegerProperty idSpanSt){
        this.newNameSt = newNameSt;
        this.newCodeESR = newCodeESR;
        this.codeESR2 = codeESR2;
        this.idSpanSt = idSpanSt;
        this.stationOrSpanSt.set(2);
        this.connect.set("Ok");
    }

    public void setT1Oil(double t1Oil) {
        this.t1Oil.set(t1Oil);
    }

    public void setT2Oil(double t2Oil) {
        this.t2Oil.set(t2Oil);
    }

    public void setTUpOil(double tUpOil) {
        this.tUpOil.set(tUpOil);
    }

    public void setTDownOil(double tDownOil) {
        this.tDownOil.set(tDownOil);
    }
}
