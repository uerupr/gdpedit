package com.gmail.alaerof.gdpcalculation;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.gdp.SpanTrain;
import com.gmail.alaerof.dao.dto.groupp.GroupP;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.gdpcalculation.model.DistrTrainGDPCalc;
import com.gmail.alaerof.util.CategoryUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CalculateUtil {
    /**
     * ���������� ����� ����,�������,���������� �� �������� ��� ������:
     * ���� ���� � spanTrainEnapled=true, �� �������������� ����� ���� ������,
     * ���� ���, �� ��� ��������� ����������� � ���� �������� loc
     *
     * @param distrSpan ������� �������
     * @param loc ��� ��������
     * @param distrTrain �����
     * @param spanTrainEnabled ������ �������������� ������� ���� ���� true
     * @return Time
     */
    public static Time getTrainTime(DistrSpan distrSpan, LocomotiveType loc, DistrTrain distrTrain,
                                    boolean spanTrainEnabled) {
        int idSpan = distrSpan.getSpanScale().getIdSpan();
        SpanTrain spanTrain = null;
        if (spanTrainEnabled) {
            spanTrain = distrTrain.getMapSpanTrain().get(idSpan);
        }
        Time time;
        if (spanTrain != null) {
            time = spanTrain.getTime();
        } else {
            OE oe = distrTrain.getOe();
            time = distrSpan.getSpanTime(loc, oe);
        }
        return time;
    }

    /**
     *
     * @param locomotiveTypeList
     * @param categories
     * @param groupP
     * @return
     */
    public static LocomotiveType defineLocomotiveType(List<LocomotiveType> locomotiveTypeList, List<Category> categories, GroupP groupP) {
        if (groupP.getIdLtype() == 0) {
            int codeTR = groupP.getRangeList().get(0).getMinCode();
            Category cat = CategoryUtil.defineCategory(categories, codeTR);
            for (LocomotiveType lt : locomotiveTypeList) {
                if (cat.getType().getString().equals(lt.getLtype())) {
                    groupP.setIdLtype(lt.getIdLType());
                    return lt;
                }
            }
        } else {
            for (LocomotiveType lt : locomotiveTypeList) {
                if (groupP.getIdLtype() == lt.getIdLType()) {
                    return lt;
                }
            }
        }
        return null;
    }

    public static LocomotiveType defineLocomotiveType(List<LocomotiveType> locomotiveTypeList, DistrTrainGDPCalc trainGDPCalc) {
        if (trainGDPCalc.getLocomotiveType() != null) {
            return trainGDPCalc.getLocomotiveType();
        }
        String typeTR = trainGDPCalc.getDistrTrain().getTypeTR().getString();

        for (LocomotiveType lt : locomotiveTypeList) {
            if (lt.getLtype().equals(typeTR)) {
                trainGDPCalc.setLocomotiveType(lt);
                return lt;
            }
        }
        return null;
    }

    public static LocomotiveType defineLocomotiveType(List<LocomotiveType> locomotiveTypeList, DistrTrain distrTrain) {
        if (distrTrain.getIdLtype() != 0) {
            for (LocomotiveType lt : locomotiveTypeList) {
                if (lt.getIdLType() == distrTrain.getIdLtype()) {
                    return lt;
                }
            }
        }
        String typeTR = distrTrain.getTypeTR().getString();
        for (LocomotiveType lt : locomotiveTypeList) {
            if (lt.getLtype().equals(typeTR)) {
                distrTrain.setIdLtype(lt.getIdLType());
                return lt;
            }
        }
        return null;
    }

    /**
     *
     * @param distrTrains
     * @param categoryList
     * @return
     */
    public static List<DistrTrainGDPCalc> wrapDistrTrains(Collection<DistrTrain> distrTrains, List<Category> categoryList, boolean fixed) {
        List<DistrTrainGDPCalc> list = new ArrayList<>();
        for (DistrTrain distrTrain : distrTrains) {
            Category cat = resolveCategory(categoryList, distrTrain.getCodeTRInt());
            DistrTrainGDPCalc gdpCalc = new DistrTrainGDPCalc(distrTrain, cat, fixed);
            list.add(gdpCalc);
        }
        return list;
    }

    private static Category resolveCategory(List<Category> categoryList, int codeTR) {
        for (Category category : categoryList) {
            if (codeTR >= category.getTrMin() && codeTR <= category.getTrMax()) {
                return category;
            }
        }
        return null;
    }

    public static DistrTrainGDPCalc findInList(DistrTrain distrTrain, List<DistrTrainGDPCalc> allTrains) {
        for (DistrTrainGDPCalc trainGDPCalc : allTrains) {
            if (trainGDPCalc.getDistrTrain().equalsByID(distrTrain)) {
                return trainGDPCalc;
            }
        }
        return null;
    }

    public static String buildTrainIDs(Collection<DistrTrain> calculateList) {
        StringBuilder sb = new StringBuilder();
        for(DistrTrain distrTrain: calculateList){
            sb.append(distrTrain.getIdTrain()).append(", ");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }
}
