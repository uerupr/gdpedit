package com.gmail.alaerof.entity.train;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrLineSt;
import com.gmail.alaerof.entity.DistrStation;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class PopupMenuLineHor extends JPopupMenu {
    private static final long serialVersionUID = 1L;
    private NewLine line;

    private ActionListener lineStListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
            String nameLine = null;
            if (item.isSelected()) {
                nameLine = item.getText();
            }
            if (line != null && item.isSelected()) {
                TrainSt trStD = (TrainSt) line.getTrainThreadElement();
                DistrStation dSt = trStD.getStationBegin().station;
                int idLine = dSt.getLineID(nameLine);
                trStD.setIdLineSt(idLine);
                GDPEdit.gdpEdit.getGDPActionPanel().getActionListSt().tableRepaint();
            }

        }
    };

    public PopupMenuLineHor() {
        initContent();
    }

    private void initContent() {
        this.removeAll();
        if (line != null) {

            TrainSt trStD = (TrainSt) line.getTrainThreadElement();
            DistrStation dSt = trStD.getStationBegin().station;
            int idLine = trStD.getIdLineSt();
            List<DistrLineSt> listLines = dSt.getListLines();
            final ButtonGroup linesCheckBox = new ButtonGroup();
            for (int i = 0; i < listLines.size(); i++) {
                DistrLineSt lineSt = listLines.get(i);
                if (!lineSt.getLineSt().isCloseInGDP()) {
                    String lineName = lineSt.getLineSt().getPs();
                    boolean selected = lineSt.getLineSt().getIdLineSt() == idLine;
                    JCheckBoxMenuItem item = new JCheckBoxMenuItem(lineName, selected);
                    item.addActionListener(lineStListener);
                    linesCheckBox.add(item);
                    this.add(item);
                }
            }
        }
    }

    public NewLine getLine() {
        return line;
    }

    public void setLine(NewLine line) {
        boolean init = true;
        if (line != null && this.line != null) {
            TrainSt trSt_N = (TrainSt) line.getTrainThreadElement();
            TrainSt trSt_O = (TrainSt) this.line.getTrainThreadElement();
            int idO = trSt_O.getIdLineSt();
            int idN = trSt_N.getIdLineSt();
            init = idO != idN;
        }

        this.line = line;
        if (init)
            initContent();
    }
}

