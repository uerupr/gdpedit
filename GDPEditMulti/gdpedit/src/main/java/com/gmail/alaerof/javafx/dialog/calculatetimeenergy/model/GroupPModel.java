package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.groupp.GroupP;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GroupPModel {
    private GroupP groupP;
    private IntegerProperty ord;
    private StringProperty name;
    private StringProperty byColor;

    public GroupPModel(GroupP groupP) {
        this.groupP = groupP;
        ord = new SimpleIntegerProperty(groupP.getOrd());
        name = new SimpleStringProperty(groupP.getName());
        byColor = new SimpleStringProperty(groupP.getColorString());
    }

    public GroupP getGroupP() {
        return groupP;
    }

    public int getOrd() {
        return ord.get();
    }

    public IntegerProperty ordProperty() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord.set(ord);
        this.groupP.setOrd(ord);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
        this.groupP.setName(name);
    }

    public String getByColor() {
        return byColor.get();
    }

    public StringProperty byColorProperty() {
        return byColor;
    }

    public void setByColor(String byColor) {
        this.byColor.set(byColor);
        this.getGroupP().setColorString(byColor);
    }

    public static List<GroupP> extractGroupP(List<GroupPModel> groupPModels) {
        List<GroupP> list = new ArrayList<>();
        for (GroupPModel model : groupPModels) {
            list.add(model.groupP);
        }
        return list;
    }

    @Override
    public String toString() {
        return "GroupPModel{" +
                "groupP=" + groupP +
                '}';
    }
}
