package com.gmail.alaerof.dialog.linesttrain;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
class ListTrainStCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int col) {
        if (value != null)
            setText(value.toString());

        Color background;
        if (row % 2 == 0) {
            background = Color.WHITE;
        } else {
            background = new Color(242, 242, 242);
        }
        if (isSelected) {
            background = Color.LIGHT_GRAY;
        }

        Font oldFont = getFont();
        if (col == 0) {
            Font newFont = new java.awt.Font(oldFont.getName(), java.awt.Font.BOLD, oldFont.getSize());
            setFont(newFont);
        } else {
            Font newFont = new java.awt.Font(oldFont.getName(), 0, oldFont.getSize());
            setFont(newFont);
        }

        setBackground(background);

        return this;
    }
}