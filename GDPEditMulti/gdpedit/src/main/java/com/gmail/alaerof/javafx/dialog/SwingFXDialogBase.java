package com.gmail.alaerof.javafx.dialog;

import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.IconImageUtil;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javax.swing.JDialog;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public abstract class SwingFXDialogBase extends JDialog {
    /** logger */
    protected Logger logger;
    /** root FX pane (layout) */
    protected Pane rootPaneFX;
    /** ���� � ��������� fxml, ������ ���� ������������������� � ���������� */
    protected String rootFXMLPath;
    /** ���� � property �����, ������ ���� ������������������� � ���������� */
    protected String resourceBundleBaseName;
    /** �������� ����� fx */
    protected Scene scene;
    /** �������� Swing frame */
    protected Frame owner;

    {
        logger = LogManager.getLogger(this.getClass());
    }

    public SwingFXDialogBase(Frame owner, String title, boolean modal){
        super(owner, title, modal);
        this.owner = owner;
        setIconImage(IconImageUtil.getImage(IconImageUtil.MAIN));
        initContent();
    }

    /**
     * ���������� ������������ Swing ����������� ����
     */
    private void initContent() {
        // x, y, with, height
        int[] initialBounds = getInitialFrameBounds();
        int screenSizeW = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenSizeH = Toolkit.getDefaultToolkit().getScreenSize().height;
        int w = initialBounds[2] < screenSizeW ? initialBounds[2] : screenSizeW - 10;
        int h = initialBounds[3] < screenSizeH ? initialBounds[3] : screenSizeH - 10;
        this.setSize(w, h);

        if (initialBounds[0] == 0 && initialBounds[1] == 0) {
            this.setLocation(
                    screenSizeW / 2 - this.getWidth() / 2,
                    screenSizeH / 2 - this.getHeight() / 2);
        } else {
            this.setLocation(initialBounds[0], initialBounds[1]);
        }

        getContentPane().setLayout(new BorderLayout());
        final JFXPanel jfxPanel = new JFXPanel();
        getContentPane().add(jfxPanel, BorderLayout.CENTER);

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initFX(jfxPanel);
            }
        });
    }

    /**
     * Returns x, y, with, height
     * @return x, y, with, height
     */
    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 800, 600};
        return bounds;
    }

    /**
     * �������������� ���������� FX ���������� ����������� ����
     * @param jfxPanel JFXPanel
     */
    protected void initFX(JFXPanel jfxPanel) {
        try {
            // ��������� �������� ����� �� fxml �����.
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(getBundle());
            URL url = this.getClass().getClassLoader().getResource(rootFXMLPath);
            loader.setLocation(url);
            // �������� FX layout
            rootPaneFX = loader.load();

            // ���������� �����, ���������� �������� �����
            scene = new Scene(rootPaneFX);
            jfxPanel.setScene(scene);

            // ����������
            setController(loader);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * ����� �������� ���� ��������� �� ������ ����� �� �������� �����
     * @return {@link ResourceBundle}
     */
    protected ResourceBundle getBundle(){
        return ResourceBundle.getBundle(resourceBundleBaseName, resolveLocale());
    }

    /**
     * ���������� ������� ������
     * @return {@link Locale}
     */
    protected Locale resolveLocale() {
        return LocaleManager.getLocale();
    }

    /**
     * ����� � ������� ����������� ��� �������� FX ������� � ����������� �����������
     * @param loader FXMLLoader
     */
    protected abstract void setController(FXMLLoader loader);
}
