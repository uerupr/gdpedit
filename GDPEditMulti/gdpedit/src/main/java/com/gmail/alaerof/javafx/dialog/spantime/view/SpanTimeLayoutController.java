package com.gmail.alaerof.javafx.dialog.spantime.view;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.energy.Energy;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.spantime.model.SpanTimeModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

/**
 *
 */
public class SpanTimeLayoutController extends CommonDistrSpanController {
    @FXML
    private BorderPane tablePane;
    /** ������� ���������� ������ ���� */
    private TableView<SpanTimeModel> spanTimeTable;

    private String okMessage;
    private String errMessage;

    /**
     * ������������� ������-�����������.
     * ���������� �� ������������� ������ initialize(URL location, ResourceBundle resources),
     * ������� � ���� ������� ���������� ����������� ��� �������� �� FXML
     */
    @FXML
    @Override
    protected void initialize() {
        logger.debug("@FXML initialize");
        okMessage = bundle.getString("javafx.dialog.spanst.ok");
        errMessage = bundle.getString("javafx.dialog.spanst.err");

        EditableTableViewCreator<SpanTimeModel> builder = new EditableTableViewCreator<>();
        spanTimeTable = builder.createEditableTableView();

        tablePane.setCenter(spanTimeTable);

        String title = bundle.getString("javafx.dialog.spantime.table.loc");
        spanTimeTable.getColumns().add(builder.createStringColumn(title, SpanTimeModel::locTypeProperty, false));

        title = bundle.getString("javafx.dialog.spantime.table.oe");
        spanTimeTable.getColumns().add(builder.createStringColumn(title, SpanTimeModel::oeProperty, false));

        title = bundle.getString("javafx.dialog.spantime.table.move");
        spanTimeTable.getColumns().add(builder.createDoubleColumn(title, SpanTimeModel::tmoveProperty, true));

        title = bundle.getString("javafx.dialog.spantime.table.up");
        spanTimeTable.getColumns().add(builder.createDoubleColumn(title, SpanTimeModel::tupProperty, true));

        title = bundle.getString("javafx.dialog.spantime.table.down");
        spanTimeTable.getColumns().add(builder.createDoubleColumn(title, SpanTimeModel::tdownProperty, true));

        title = bundle.getString("javafx.dialog.spantime.table.tw");
        spanTimeTable.getColumns().add(builder.createDoubleColumn(title, SpanTimeModel::twProperty, true));

        title = bundle.getString("javafx.dialog.spantime.table.t1oil");
        spanTimeTable.getColumns().add(builder.createDoubleColumn(title, SpanTimeModel::t1oilProperty, true));

        title = bundle.getString("javafx.dialog.spantime.table.tupoil");
        spanTimeTable.getColumns().add(builder.createDoubleColumn(title, SpanTimeModel::tupoilProperty, true));

        title = bundle.getString("javafx.dialog.spantime.table.tdownoil");
        spanTimeTable.getColumns().add(builder.createDoubleColumn(title, SpanTimeModel::tdownoilProperty, true));
    }

    /**
     * ���������� ����������� SpanTimeLayout � ������������ � ��������� ��������� �������.
     */
    @Override
    protected void updateModelWithContent() {
        logger.debug("updateModelWithContent");
        List<SpanTimeModel> itemList = buildSpanTimeModelListFromDistrSpan(distrSpan, listLoc);
        spanTimeTable.setItems(FXCollections.observableArrayList(itemList));
    }

    /**
     * ������� ������ ���������� ������ ����, ��������� ��� ����������� � �������, ��� ����
     * ��������� � distrSpan ������� ������� ���� ��� ���� LocomotiveType �� ������ listLoc, ��� ����������
     * @return list {@link SpanTimeModel}
     */
    private List<SpanTimeModel> buildSpanTimeModelListFromDistrSpan(DistrSpan distrSpan, List<LocomotiveType> listLoc) {
        List<SpanTimeModel> itemList = new ArrayList<>();
        if (distrSpan != null && listLoc != null) {
            OE oev[] = OE.values();
            distrSpan.updateSpanEnergy(listLoc);
            for (LocomotiveType ltype : listLoc) {
                for (OE oe : oev) {
                    Time spanTime = distrSpan.getSpanTime(ltype, oe);
                    Energy energy = distrSpan.getSpanEnergy(ltype, oe);
                    if(spanTime == null){
                        // ��� ��� �� ������� ����� ������ � ���������� ��� � distrSpan
                        // ���� ����� ������ ������ "���������" ��� ��������� � ��
                        spanTime = new Time();
                        distrSpan.setSpanTime(spanTime, ltype, oe);
                    }
                    if (energy == null){
                        energy = new Energy();
                        distrSpan.setSpanEnergy(energy, ltype, oe);
                    }
                    int idSpan = distrSpan.getSpanScale().getIdSpan();
                    SpanTimeModel spanTimeModel = new SpanTimeModel(idSpan, ltype.getIdLType(), ltype.getLtype(), oe, spanTime, energy);
                    itemList.add(spanTimeModel);
                }
            }
            Collections.sort(itemList);
        }
        return itemList;
    }

    /**
     * ���������� ������ ���� � ��
     * @param event {@link ActionEvent} ������� (������� �� ������)
     */
    @FXML
    private void handleSaveDistrSpan(ActionEvent event){
        logger.debug("handleSaveDistrSpan");
        String message = okMessage;
        try {
            // ������� �������� ������ �� ������� � distrSpan
            updateDistrSpanFromSpanTimeModelList(spanTimeTable.getItems());
            // ����� ��������� � ��
            distrSpan.saveSpanTime();
            distrSpan.saveSpanEnergy();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = errMessage + ": " + e.getMessage();
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(distrSpan.getSpanScale().getName());
        alert.setHeaderText(message);
        alert.show();
    }

    /**
     * ��� ������ ������ ������� ��������� ����� {@Time} spantime, ������� ����������� �������� �������
     * �.�. ��� ��������� ������ ������� DistrSpan
     * @param spanTimeModelList ������ ����� �������
     */
    private void updateDistrSpanFromSpanTimeModelList(ObservableList<SpanTimeModel> spanTimeModelList) {
        logger.debug("updateDistrSpanFromSpanTimeModelList");
        for (SpanTimeModel model : spanTimeModelList) {
            Time spanTime = model.getSpanTime();
            spanTime.setTimeMove((float) model.getTmove());
            spanTime.setTimeUp((float) model.getTup());
            spanTime.setTimeDown((float) model.getTdown());
            spanTime.setTimeTw((float) model.getTw());
            Energy energy = model.getEnergy();
            energy.setEnMove((float) model.getT1oil());
            energy.setEnUp((float) model.getTupoil());
            energy.setEnDown((float) model.getTdownoil());
        }
    }

}
