package com.gmail.alaerof.javafx.dialog.scheduletoexcel;

import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.scheduletoexcel.view.ScheduleToExcelLayoutController;

import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

/**
 * FX ����� ������ ���
 */
public class ScheduleToExcelFXDialog extends SwingFXDialogBase {
    private DistrEditEntity distrEntity;
    private ScheduleToExcelLayoutController controller;

    /**
     * Create the dialog.
     */
    public ScheduleToExcelFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/scheduletoexcel/view/ScheduleToExcelLayout.fxml";
        resourceBundleBaseName = "bundles.ScheduleToExcelFXDialog";
    }

    public DistrEditEntity getDistrEntity() {
        return distrEntity;
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 530, 480};
        return bounds;
    }

     public void setDialogContent(final GDPEditPanel editPanel) {
        this.distrEntity = editPanel.getDistrEditEntity();

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(distrEntity, editPanel);
            }
        });
    }
}
