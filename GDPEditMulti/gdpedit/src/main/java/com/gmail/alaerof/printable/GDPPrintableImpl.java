package com.gmail.alaerof.printable;

import com.gmail.alaerof.javafx.dialog.gdpprint.model.GDPPrintParam;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrEntity;

/**
 * ����� ��� ������ ��� � ���� ��� �� �������
 * @author Helen Yrofeeva
 *
 */
public class GDPPrintableImpl implements Printable{
    /** ���� */
    //public static final int PAPER_FIELDS = 40;
    private DistrEditEntity distrEditEntity;
    private GDPPrintParam gdpPrintParam;

    public GDPPrintableImpl(DistrEditEntity distrEditEntity, GDPPrintParam gdpPrintParam) {
        this.distrEditEntity = distrEditEntity;
        this.gdpPrintParam = gdpPrintParam;
    }


    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex > 0) {
            return NO_SUCH_PAGE;
        }
        Graphics2D gGDP = (Graphics2D) graphics;
        gGDP.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        GDPPrintSize printSize = new GDPPrintSize(distrEditEntity, gdpPrintParam);
        printSize.calcPrintSize(pageFormat.getWidth(), pageFormat.getHeight());

        gGDP.scale(printSize.getCoeffx(), printSize.getCoeffy());

        distrEditEntity.getDistrEntity().drawNetGDP(gGDP, gGDP, GDPPrintSize.PAPER_FIELDS,
                printSize.getHeadGap(), printSize.getBottomGap(), Color.white,
                DistrEntity.JOINT_GDP, printSize.getCf(), gdpPrintParam, printSize.gettPPO());

        //distrEditEntity.paintInPictureAllTrains(gGDP, cf);
        distrEditEntity.paintInPictureAllTrains(gGDP, printSize.getCf());

        return PAGE_EXISTS;
    }

}
