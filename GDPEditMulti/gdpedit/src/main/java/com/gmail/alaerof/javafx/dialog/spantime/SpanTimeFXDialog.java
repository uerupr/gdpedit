package com.gmail.alaerof.javafx.dialog.spantime;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.spantime.view.CommonDistrSpanController;
import java.awt.Frame;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

/**
 * FX ������ ��� com.gmail.alaerof.dialog.spantime.SpanTimeDialog
 */
public class SpanTimeFXDialog extends SwingFXDialogBase {
    private List<CommonDistrSpanController> commonDistrSpanControllerList = new ArrayList<>();

    /**
     * Create the dialog.
     */
    public SpanTimeFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/spantime/view/SpanTimeFXRootLayout.fxml";
        resourceBundleBaseName = "bundles.SpanTimeFXDialog";
    }

    /**
     * �������������� ���������� FX ���������� ����������� ����
     * @param jfxPanel JFXPanel
     */
    @Override
    protected void initFX(JFXPanel jfxPanel) {
        Scene scene = initRootLayout();

        // "������� ���� �� ��������"
        Pane spanTimePane = initPaneLayout("com/gmail/alaerof/javafx/dialog/spantime/view/SpanTimeLayout.fxml");
        // "������������ ������ �������� + ������� ���� ����� �.�."
        Pane spanStPane =  initPaneLayout("com/gmail/alaerof/javafx/dialog/spantime/view/SpanStLayout.fxml");

        SplitPane splitPane = (SplitPane) ((BorderPane) rootPaneFX).getCenter();
        splitPane.getItems().clear();
        splitPane.getItems().addAll(spanTimePane, spanStPane);
        jfxPanel.setScene(scene);
    }

    /**
     * ��������� �������� ������ �� FXML � ������ �����������
     * ������� �������� ����� ��� ����������� ������
     * @return {@Scene} �������� �����
     */
     private Scene initRootLayout() {
        try {
            // ��������� �������� ����� �� fxml �����
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(getBundle());
            URL url = this.getClass().getClassLoader().getResource(rootFXMLPath);
            loader.setLocation(url);
            rootPaneFX = (BorderPane) loader.load();

            // ������� �����, ���������� �������� �����.
            Scene scene = new Scene(rootPaneFX);

            // �������� ���������� ������ � ������
            CommonDistrSpanController controller = (CommonDistrSpanController) loader.getController();
            controller.setScene(scene);
            commonDistrSpanControllerList.add(controller);

            return scene;
        } catch (IOException e) {
            logger.error(e);
        }
        return null;
    }

    /**
     * ��������� ������ �� FXML � ������ �����������
     * @param fxml �������� FXML
     * @return {@Pane} ����������� ������ ��� null
     */
    private Pane initPaneLayout(String fxml) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(getBundle());
            URL url = this.getClass().getClassLoader().getResource(fxml);
            loader.setLocation(url);
            Pane pane = loader.load();

            // �������� ���������� ������ � ������
            CommonDistrSpanController controller = loader.getController();
            commonDistrSpanControllerList.add(controller);

            return pane;
        } catch (IOException e) {
            logger.error(e);
        }
        return null;
    }

    @Override
    protected void setController(FXMLLoader loader) {

    }

    /**
     * ������ ���������� ������ ��� ��� �������� ���� ��� ����������� ��������
     * @param distrSpan {@link DistrSpan} ���� ���������� ������� �� �������
     * @param listLoc {@link LocomotiveType} ������ ����� �������� (�����������)
     */
    public void setDialogContent(DistrSpan distrSpan, List<LocomotiveType> listLoc) {
        this.setTitle(distrSpan.getSpanScale().getName());

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for (CommonDistrSpanController controller : commonDistrSpanControllerList) {
                    controller.setContent(distrSpan, listLoc);
                }
            }
        });
    }
}
