package com.gmail.alaerof.components.mainframe.gdp.action.listst;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class TimeStCellEditor extends DefaultCellEditor {
    protected static Logger logger = LogManager.getLogger(TimeStCellEditor.class);
    private TimeSt cellValue;

    public TimeStCellEditor(JTextField textField) {
        super(textField);
        textField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (textField.getText().length() == 4) {
                    stopCellEditing();
                }
                if (textField.getText().length() > 5) {
                    String ss = String.valueOf(e.getKeyChar());
                    textField.setText(ss);
                }
            }
        });
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
            }
        });
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        // remember the value
        if(value==null){
            cellValue = null;
        }else{
            try {
                cellValue = new TimeSt(value.toString());
            } catch (TimeStException e) {
                cellValue = null;
            }
        }
        JTextField textField  = (JTextField) super.editorComponent;
        if (cellValue != null) {
            textField.setText(cellValue.toString());
        } else {
            textField.setText("");
        }
        return textField;
    }

    @Override
    public Object getCellEditorValue() {
        return cellValue;
    }

    @Override
    public boolean stopCellEditing() {
        // change the value
        JTextField textField  = (JTextField) super.editorComponent;
        String time = textField.getText();
        try {
            //�������� �� null
            if(time != null){
                time = time.trim();
            }
            int res = TimeSt.checkTime(time);
            if (res == 1){
                cellValue = new TimeSt(time);
            }
            if (res == 2){
                cellValue = null;
            }
        } catch (TimeStException e) {
            logger.error(e.toString(), e);
        }
        return super.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        super.cancelCellEditing();
    }

    @Override
    public boolean isCellEditable(EventObject ev) {
        return true;
    }
    
    @Override
    public boolean shouldSelectCell(EventObject ev) {
        return true;
    }

    public JTextField getTextField() {
        JTextField textField  = (JTextField) super.editorComponent;
        return textField;
    }

}
