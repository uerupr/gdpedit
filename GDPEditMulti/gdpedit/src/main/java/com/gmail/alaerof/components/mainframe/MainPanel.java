package com.gmail.alaerof.components.mainframe;

import com.gmail.alaerof.components.mainframe.gdp.action.GDPActionPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditTabPane;
/**
 * MainPanel:[ [DistrTree] | [GDPEditTabPane | GDPActionPanel] ]
 * @author Helen Yrofeeva
 */
public class MainPanel extends JPanel {
	private static final long serialVersionUID = 1L;
    private DistrTree distrTree;
    private GDPActionPanel actionPanel;
    private GDPEditTabPane editTabPane;

	public MainPanel() {
	    // ������ ��� DistrTree
        JPanel paneDistr = new JPanel();
        distrTree = new DistrTree();
        JScrollPane sl = distrTree.getDistrTreePanel();
        paneDistr.setLayout(new BorderLayout());
        paneDistr.add(sl, BorderLayout.CENTER);

        // ������ ��� GDP
        JPanel paneRight = new JPanel();
        actionPanel = new GDPActionPanel();
        editTabPane = new GDPEditTabPane(actionPanel);

        JSplitPane splitPaneRight = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, editTabPane,
				actionPanel);

        splitPaneRight.setOneTouchExpandable(true);
        splitPaneRight.setDividerLocation(2050);
        splitPaneRight.setPreferredSize(new Dimension(400, 200));
        paneRight.setLayout(new BorderLayout());
        paneRight.add(splitPaneRight, BorderLayout.CENTER);

		// ������ ��� ������
        JSplitPane splitPaneMain = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, paneDistr,
				paneRight);
		this.add(splitPaneMain);

		splitPaneMain.setOneTouchExpandable(true);
		splitPaneMain.getLeftComponent().setMinimumSize(new Dimension(0, 0));
		splitPaneMain.setDividerLocation(100);
		splitPaneMain.setPreferredSize(new Dimension(400, 200));

		this.setLayout(new BorderLayout());
		this.add(splitPaneMain, BorderLayout.CENTER);
	}

	public GDPActionPanel getGDPActionPanel() {
		return actionPanel;
	}
	
	public GDPEditTabPane getGDPEditTabPane(){
		return editTabPane;
	}

	public DistrTree getDistrTree(){
		return distrTree;
	}
	
}
