package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.energy.EnergyType;
import com.gmail.alaerof.dao.dto.energy.ExpenseRate;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * ��������� ������
 */
public class ExpenseRateModel {
    private TrainType trainType;
    private EnergyType energyType;

    private StringProperty trainTypeName;
    private StringProperty energyTypeName;

    /** ��������� ������-�� */
    private SimpleDoubleProperty moveExpRate;
    /** ��������� ������-���� ������� */
    private SimpleDoubleProperty stopExpRate;

    public ExpenseRateModel(TrainType trainType, EnergyType energyType) {
        this.trainType = trainType;
        this.energyType = energyType;
        this.trainTypeName = new SimpleStringProperty(trainType.getString());
        this.energyTypeName = new SimpleStringProperty(energyType.getString());
        this.moveExpRate = new SimpleDoubleProperty(0.0);
        this.stopExpRate = new SimpleDoubleProperty(0.0);
    }

    public ExpenseRateModel(ExpenseRate expenseRate) {
        this.trainType = expenseRate.getTrainType();
        this.energyType = expenseRate.getEnergyType();
        this.trainTypeName = new SimpleStringProperty(trainType.getString());
        this.energyTypeName = new SimpleStringProperty(energyType.getString());
        this.moveExpRate = new SimpleDoubleProperty(expenseRate.getMoveExpRate());
        this.stopExpRate = new SimpleDoubleProperty(expenseRate.getStopExpRate());
    }

    public TrainType getTrainType() {
        return trainType;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public String getTrainTypeName() {
        return trainTypeName.get();
    }

    public StringProperty trainTypeNameProperty() {
        return trainTypeName;
    }

    public String getEnergyTypeName() {
        return energyTypeName.get();
    }

    public StringProperty energyTypeNameProperty() {
        return energyTypeName;
    }

    public double getMoveExpRate() {
        return moveExpRate.get();
    }

    public SimpleDoubleProperty moveExpRateProperty() {
        return moveExpRate;
    }

    public void setMoveExpRate(double moveExpRate) {
        this.moveExpRate.set(moveExpRate);
    }

    public double getStopExpRate() {
        return stopExpRate.get();
    }

    public SimpleDoubleProperty stopExpRateProperty() {
        return stopExpRate;
    }

    public void setStopExpRate(double stopExpRate) {
        this.stopExpRate.set(stopExpRate);
    }
}
