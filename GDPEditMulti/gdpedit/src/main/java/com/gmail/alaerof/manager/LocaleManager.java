package com.gmail.alaerof.manager;

import java.util.Locale;

public class LocaleManager {
    private static Locale locale = new Locale.Builder().setLanguage("ru").setRegion("RU").build();

    public static Locale getLocale() {
        return locale;
    }
}
