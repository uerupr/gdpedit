package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.energy.ExpenseRate;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateEnergy;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateNormative;
import java.util.List;

public class ExpenseRateTransfer {
    private ExpenseRateEnergy teplo;
    private ExpenseRateEnergy electro;
    private List<ExpenseRate> expenseRateList;
    private List<ExpenseRateNormative> expenseRateNormativeList;
    // ����� ������
    private String dbMode;

    public ExpenseRateEnergy getTeplo() {
        return teplo;
    }

    public ExpenseRateEnergy getElectro() {
        return electro;
    }

    public List<ExpenseRate> getExpenseRateList() {
        return expenseRateList;
    }

    public List<ExpenseRateNormative> getExpenseRateNormativeList() {
        return expenseRateNormativeList;
    }

    public String getDbMode() {
        return dbMode;
    }

    public void setTeplo(ExpenseRateEnergy teplo) {
        this.teplo = teplo;
    }

    public void setElectro(ExpenseRateEnergy electro) {
        this.electro = electro;
    }

    public void setExpenseRateList(List<ExpenseRate> expenseRateList) {
        this.expenseRateList = expenseRateList;
    }

    public void setExpenseRateNormativeList(List<ExpenseRateNormative> expenseRateNormativeList) {
        this.expenseRateNormativeList = expenseRateNormativeList;
    }

    public void setDbMode(String dbMode) {
        this.dbMode = dbMode;
    }
}
