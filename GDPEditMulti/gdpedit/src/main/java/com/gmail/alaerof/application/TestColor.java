package com.gmail.alaerof.application;

import java.awt.Color;

import com.gmail.alaerof.dao.dto.gdp.TrainLineStyleSpan;
import com.gmail.alaerof.util.ColorConverter;

public class TestColor {
	public static void main(String[] args) {
		
		int c = 32896;//255;
		Color clr = ColorConverter.convert(c);
		System.out.println(clr);
		c = ColorConverter.convert(clr);
		System.out.println(c);
		clr = ColorConverter.convert(c);
		System.out.println(clr);
		
		TrainLineStyleSpan ln = new TrainLineStyleSpan();
		ln.setShape("span;hor");
        System.out.println(ln.isHor());
        System.out.println(ln.isSpan());

//		int size = 10;
//		String value = "sdf";
//		String newValue = StringUtils.rightPad(value, size);
//		newValue += ".";
//		System.out.println(newValue);
	}

}
