package com.gmail.alaerof.util;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;

public class IconImageUtil {
    public static Logger logger = LogManager.getLogger(IconImageUtil.class);
    private static final String picPath = "pic/";

    public static final String MAIN = "GR4.GIF";
    public static final String PLUS = "plus.gif";
    public static final String MINUS = "minus.gif";
    public static final String SEARCH = "search.gif";
    public static final String CALC = "calc.gif";
    public static final String CALCGDP = "calcGDP.gif";
    public static final String APPLY = "apply.gif";
    public static final String RESTORE = "restore.GIF";
    public static final String LOAD_FILE = "loadFile.gif";
    public static final String SAVE_FILE = "saveFile.gif";
    public static final String SAVE_DB = "saveDB.gif";
    public static final String ISPT64 = "ISPT64.gif";
    public static final String ARR_LEFT = "arrLeft.gif";
    public static final String ARR_RIGHT = "arrRight.gif";
    public static final String ARR_UP = "arrUp.gif";
    public static final String ARR_DOWN = "arrDown.gif";
    public static final String OPEN = "open.gif";
    public static final String MODE00 = "movemode00.gif";
    public static final String MODE01 = "movemode01.gif";
    public static final String MODE02 = "movemode02.gif";
    public static final String MODE03 = "movemode03.gif";
    public static final String MODE04 = "movemode04.gif";
    public static final String MODE05 = "movemode05.gif";

    public static Image getImage(String iconName) {
        String path = picPath + iconName;
        Image image = null;
        if (new File(path).exists()) {
            try {
                image = Toolkit.getDefaultToolkit().getImage(path);
            } catch (Exception e) {
                logger.error(e);
            }
        }
        return image;
    }

    public static Icon getIcon(String iconName){
        String path = picPath + iconName;
        Icon icon = null;
        if (new File(path).exists()) {
            try {
                icon = new ImageIcon(path);
            } catch (Exception e) {
                logger.error(e);
            }
        }
        return icon;
    }

    public static javafx.scene.image.Image getFXImage(String iconName){
        javafx.scene.image.Image image = null;
        String path = picPath + iconName;
        try {
            FileInputStream inputstream = new FileInputStream(path);
            image = new javafx.scene.image.Image(inputstream);
        } catch (Exception e) {
            logger.error(e);
        }
        return image;
    }
}
