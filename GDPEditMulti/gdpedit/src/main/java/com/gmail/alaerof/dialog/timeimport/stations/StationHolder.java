package com.gmail.alaerof.dialog.timeimport.stations;

import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.VariantCalcTime;
/**
 * ������ ��� ������������� ������� �� ������� �������� �������� �� �� �������
 * @author Helen Yrofeeva
 *
 */
public class StationHolder {
    /** �� ������� �������� (����� ���� ��� �.�., ��� � �.�.)*/
    public  VariantCalcTime varSt;
    /** �� �� ��������  �.�. */
    public Station distrSt;
    /** �� �� ��������  �.�. */
    public SpanSt spanSt;
}
