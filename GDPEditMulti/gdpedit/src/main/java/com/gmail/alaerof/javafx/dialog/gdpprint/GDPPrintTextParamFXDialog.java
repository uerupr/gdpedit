package com.gmail.alaerof.javafx.dialog.gdpprint;

import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.gdpprint.view.GDPPrintTextParamLayoutController;
import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

/**
 * Created by Paul M. Bui on 02.05.2018.
 */
public class GDPPrintTextParamFXDialog extends SwingFXDialogBase {
    private GDPEditPanel editPanel;
    private DistrEditEntity distrEntity;
    private GDPPrintTextParamLayoutController controller;

    /**
     * Create the dialog.
     */
    public GDPPrintTextParamFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/gdpprint/view/GDPPrintTextParamLayout.fxml";
        resourceBundleBaseName = "bundles.GDPPrintTextParamFXDialog";
    }

    public DistrEditEntity getDistrEntity() {
        return distrEntity;
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 580, 520};
        return bounds;
    }

    public void setDialogContent(GDPEditPanel editPanel) {
        this.distrEntity = editPanel.getDistrEditEntity();
        this.editPanel = editPanel;

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(distrEntity, editPanel);
            }
        });
    }
}
