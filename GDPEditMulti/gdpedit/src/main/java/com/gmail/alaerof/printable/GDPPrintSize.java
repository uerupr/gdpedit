package com.gmail.alaerof.printable;

import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.GDPPrintParam;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.TextPrintPointsOut;

public class GDPPrintSize {
    public final static int PAPER_FIELDS = 40;
    private final static double HELP_COEFF = 0.95;
    private DistrEditEntity distrEditEntity;
    private GDPPrintParam gdpPrintParam;
    private GDPGridConfig cf;
    private TextPrintPointsOut tPPO;
    private int headGap;
    private int bottomGap;
    private long xSize;
    private long ySize;
    private double coeffx;
    private double coeffy;
    // �������� ������ ����� ��� � ��������, ������
    private int imageSizeW;
    // �������� ������ ����� ��� � ��������, ������
    private int imageSizeH;

    public GDPPrintSize(DistrEditEntity distrEditEntity, GDPPrintParam gdpPrintParam) {
        this.distrEditEntity = distrEditEntity;
        this.gdpPrintParam = gdpPrintParam;
    }

    /**
     * @param pageWidth ������ �������� ������ � ������ (INCH)
     * @param pageHeight ������ �������� ������ � ������ (INCH)
     */
    public void calcPrintSize(double pageWidth, double pageHeight) {
        // �������� ���������� ���������
        // � DistrEntity.configGDP ��������������� hourB, hourE � ��.
        GDPGridConfig config = distrEditEntity.getDistrEntity().getConfigGDP();
        cf = new GDPGridConfig(config);
        distrEditEntity.getDistrEntity().calcGDPSettings(gdpPrintParam.getHourBeg(), gdpPrintParam.getHourEnd(), cf);

        int hGDP = cf.getScaledMainImageHeight();
        int wLeft = cf.getScaledLeftWidth();
        int wGDP = cf.getScaledMainImageWidth();

        tPPO = new TextPrintPointsOut(gdpPrintParam);
        headGap = tPPO.getHeadGap();
        bottomGap = tPPO.getBottomGap();

        double wIm = wLeft + wGDP + PAPER_FIELDS*2;
        double hIm = hGDP + headGap;

        imageSizeW = (int) wIm;
        imageSizeH = (int) hIm;

        // ������ ������������� � �������� � ��
        double wSize = 0;
        double hSize = 0;
        double scaleKoeff = 0;
        double wPage = pageWidth;
        double hPage = pageHeight;
        // ��������������� �� �������� ��������
        if (gdpPrintParam.getScallingType() == 1) {
            // ������: 5 - �0; 4 - �1; 3 - �2; 2 - �3; 1 - �4; 0 - ����
            if (gdpPrintParam.getFormat() == null) {
                scaleKoeff = Math.min(wPage/wIm, hPage/hIm);
                wSize = wIm * scaleKoeff;
                hSize = hIm * scaleKoeff;
            } else {
                double pageSize[] = gdpPrintParam.getPageSize(wPage < hPage);
                double wFormat = pageSize[0];
                double hFormat = pageSize[1];
                if (gdpPrintParam.getSaveProportionl()) {
                    scaleKoeff = Math.min(wFormat/wIm, hFormat/hIm);
                    wSize = wIm * scaleKoeff;
                    hSize = hIm * scaleKoeff;
                } else {
                    wSize = wFormat;
                    hSize = hFormat;
                }
            }
        }
        // �������������� ���������������
        if (gdpPrintParam.getScallingType() == 2) {
            wSize = wIm * gdpPrintParam.getScale() / 100;
            hSize = hIm * gdpPrintParam.getScale() / 100;
        }
        // ������������ ���������������
        if (gdpPrintParam.getScallingType() == 3) {
            double monolithWidth = gdpPrintParam.getWidth() * 72 / 25.4;
            double monolithHeight = gdpPrintParam.getHeight() * 72 / 25.4;
            if (gdpPrintParam.getSaveProportionl()) {
                scaleKoeff = Math.min(monolithWidth/wIm, monolithHeight/hIm);
                wSize = wIm * scaleKoeff;
                hSize = hIm * scaleKoeff;
            } else {
                wSize = monolithWidth;
                hSize = monolithHeight;
            }
        }
        coeffx = wSize*HELP_COEFF/wIm;
        coeffy = hSize*HELP_COEFF/hIm;
        xSize = Math.round(25.4 * wSize / 72);
        ySize = Math.round(25.4 * hSize / 72);
    }

    public GDPGridConfig getCf() {
        return cf;
    }

    public TextPrintPointsOut gettPPO() {
        return tPPO;
    }

    public int getHeadGap() {
        return headGap;
    }

    public int getBottomGap() {
        return bottomGap;
    }

    public long getxSize() {
        return xSize;
    }

    public long getySize() {
        return ySize;
    }

    public double getCoeffx() {
        return coeffx;
    }

    public double getCoeffy() {
        return coeffy;
    }

    public int getImageSizeW() {
        return imageSizeW;
    }

    public int getImageSizeH() {
        return imageSizeH;
    }
}
