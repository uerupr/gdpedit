package com.gmail.alaerof.dialog.spantime;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.Time;

public class SpanTimeHolder implements Comparable<SpanTimeHolder>{
    private Time time;
    private LocomotiveType locType;
    private OE oe;

    public SpanTimeHolder() {
    }

    public SpanTimeHolder(Time time, LocomotiveType locType, OE oe) {
        this.time = time;
        this.locType = locType;
        this.oe = oe;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public LocomotiveType getLocType() {
        return locType;
    }

    public void setLocType(LocomotiveType locType) {
        this.locType = locType;
    }

    public OE getOe() {
        return oe;
    }

    public void setOe(OE oe) {
        this.oe = oe;
    }

    @Override
    public int compareTo(SpanTimeHolder o) {
        int type = locType.getIdLType() - o.locType.getIdLType();
        if(type == 0){
            if (this.oe == o.oe) return 0;
            if(this.oe.equals(OE.odd)) return -1;
            return 1;
        }
        return type;
    }
    
}
