package com.gmail.alaerof.manager;

import com.gmail.alaerof.components.mainframe.gdp.action.listst.CalcTrainDialog;
import com.gmail.alaerof.components.mainframe.gdp.action.listtrain.NewTrainDialog;
import com.gmail.alaerof.dialog.linesttrain.LineStTRDialog;
import com.gmail.alaerof.dialog.config.ApplicationConfigDialog;
import com.gmail.alaerof.dialog.config.GDPGridConfigDialog;
import com.gmail.alaerof.dialog.locomotivetypeedit.LocomotiveTypeEditDialog;
import com.gmail.alaerof.dialog.spantimetown.SpanTimeTownDialog;
import com.gmail.alaerof.dialog.timeimport.ImportSPTimeFrame;
import com.gmail.alaerof.history.HistoryViewDialog;
import com.gmail.alaerof.javafx.dialog.actualgdp.ActualFXDialog;
import com.gmail.alaerof.javafx.dialog.calculategdp.CalculateGDPFXDialog;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.CalculateTimeEnergyFXDialog;
import com.gmail.alaerof.javafx.dialog.categoryoftrains.CategoryOfTrainsFXDialog;
import com.gmail.alaerof.javafx.dialog.distrcomb.DistrCombFXDialog;
import com.gmail.alaerof.javafx.dialog.scalechange.ScaleChangeFXDialog;
import com.gmail.alaerof.javafx.dialog.trainpropertys.AllTrainListFXDialog;
import com.gmail.alaerof.javafx.dialog.stationinfo.StationInfoFXDialog;
import com.gmail.alaerof.javafx.dialog.energyfromtr.ImportEnergyFromTRFXDialog;
import com.gmail.alaerof.javafx.dialog.gdpprint.GDPPrintFXDialog;
import com.gmail.alaerof.javafx.dialog.gdpprint.GDPPrintTextParamFXDialog;
import com.gmail.alaerof.javafx.dialog.loadfromxml.LoadFromXMLFXDialog;
import com.gmail.alaerof.javafx.dialog.scheduletoexcel.ScheduleToExcelFXDialog;
import com.gmail.alaerof.javafx.dialog.spantime.SpanTimeFXDialog;
import com.gmail.alaerof.dialog.ColorDialog;
import java.util.ResourceBundle;
import javax.swing.JFrame;

public class DialogManager {
    protected ResourceBundle bundle;
    private JFrame mainFrame;
    private HistoryViewDialog historyViewDialog;
    private GDPGridConfigDialog gdpGridConfigEditDialog;
    private ColorDialog colorDialog;
    private NewTrainDialog newTrainDialog;
    private CalcTrainDialog calcTrainDialog;
    private ImportSPTimeFrame importSPTimeDialog;
    private LineStTRDialog lineStTRDialog;
    private SpanTimeTownDialog spanTimeTownDialog;
    private ApplicationConfigDialog applicationConfigDialog;
    private LocomotiveTypeEditDialog locomotiveTypeEditDialog;
    private GDPPrintFXDialog gdpPrintFXDialog;
    private ActualFXDialog actualFXDialog;
    private GDPPrintTextParamFXDialog gdpPrintTextParamFXDialog;
    private ScheduleToExcelFXDialog scheduleToExcelFXDialog;
    private ImportEnergyFromTRFXDialog importEnergyFromTFXDialog;
    private DistrCombFXDialog distrCombFXDialog;
    private ScaleChangeFXDialog scaleChangeFXDialog;
    private AllTrainListFXDialog allTrainListFXDialog;
    private StationInfoFXDialog stationInfoFXDialog;
    private CalculateTimeEnergyFXDialog calculateTimeEnergyFXDialog;
    private SpanTimeFXDialog spanTimeFXDialog;
    private LoadFromXMLFXDialog loadFromXMLFXDialog;
    private CalculateGDPFXDialog calculateGDPFXDialog;
    private CategoryOfTrainsFXDialog categoryOfTrainsFXDialog;

    /**
     * @param mainFrame {@link JFrame} ������������ ���� �������
     */
    public DialogManager(JFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.bundle = ResourceBundle.getBundle("bundles.DialogManager", LocaleManager.getLocale());
    }

    //������� ��������
    public HistoryViewDialog getHistoryViewDialog() {
        if (historyViewDialog == null) {
            String title = bundle.getString("manager.HistoryView");
            historyViewDialog = new HistoryViewDialog(mainFrame, title, true);
        }
        return historyViewDialog;
    }

    //��������� ���
    public GDPGridConfigDialog getGdpGridConfigEditDialog() {
        if (gdpGridConfigEditDialog == null) {
            String title = bundle.getString("manager.GDPGridConfig");
            gdpGridConfigEditDialog = new GDPGridConfigDialog(mainFrame, title, true);
        }
        return gdpGridConfigEditDialog;
    }

    //����� �����
    public ColorDialog getColorDialog() {
        if (colorDialog == null) {
            String title = bundle.getString("manager.Color");
            colorDialog = new ColorDialog(mainFrame, title, true);
        }
        return colorDialog;
    }

    //����� ����� ������
    public NewTrainDialog getNewTrainDialog() {
        if (newTrainDialog == null) {
            String title = bundle.getString("manager.NewTrain");
            newTrainDialog = new NewTrainDialog(mainFrame, title, true);
        }
        return newTrainDialog;
    }

    //��������� �������
    public CalcTrainDialog getCalcTrainDialog() {
        if (calcTrainDialog == null) {
            String title = bundle.getString("manager.CalcTrain");
            calcTrainDialog = new CalcTrainDialog(mainFrame, title, true);
        }
        return calcTrainDialog;
    }

    //������ ������ ����
    public ImportSPTimeFrame getImportSPTimeDialog(String distrName) {
        if (importSPTimeDialog == null) {
            String title = bundle.getString("manager.ImportSPTime") + ": " + distrName;
            importSPTimeDialog = new ImportSPTimeFrame(title);
        }
        return importSPTimeDialog;
    }

    //������ �������
    public LineStTRDialog getLineStTRDialog(String stName) {
        if (lineStTRDialog == null) {
            String title = bundle.getString("manager.LineStTR") + ": " + stName;
            lineStTRDialog = new LineStTRDialog(mainFrame, title, true);
        }
        return lineStTRDialog;
    }

    //������� ���� �� �.�.
    public SpanTimeTownDialog getSpanTimeTownDialog(String distrName) {
        if (spanTimeTownDialog == null) {
            String title = bundle.getString("manager.SpanTimeTown") + ": " + distrName;
            spanTimeTownDialog = new SpanTimeTownDialog(mainFrame, title, false);
        }
        return spanTimeTownDialog;
    }

    //��������� ����������
    public ApplicationConfigDialog getApplicationConfigDialog() {
        if (applicationConfigDialog == null) {
            String title = bundle.getString("manager.ApplicationConfig");
            applicationConfigDialog = new ApplicationConfigDialog(mainFrame, title, true);
        }
        return applicationConfigDialog;
    }

    //��� ��������
    public LocomotiveTypeEditDialog getLocomotiveTypeEditDialog() {
        if (locomotiveTypeEditDialog == null) {
            String title = bundle.getString("manager.LocomotiveTypeEdit");
            locomotiveTypeEditDialog = new LocomotiveTypeEditDialog(mainFrame, title, true);
        }
        return locomotiveTypeEditDialog;
    }

    //��������� ������
    public GDPPrintFXDialog getGDPPrintFXDialog() {
        if (gdpPrintFXDialog == null) {
            String title = bundle.getString("manager.GDPPrintFX");
            gdpPrintFXDialog = new GDPPrintFXDialog(mainFrame, title, true);
        }
        return gdpPrintFXDialog;
    }

    //Actual Graph
    public ActualFXDialog getActualFXDialog() {
        if (actualFXDialog == null) {
            String title = bundle.getString("manager.Server");
            actualFXDialog = new ActualFXDialog(mainFrame, title, true);
        }
        return actualFXDialog;
    }

    //��������� ���� ������� ������
    public GDPPrintTextParamFXDialog getGDPPrintTextParamFXDialog() {
        if (gdpPrintTextParamFXDialog == null) {
            String title = bundle.getString("manager.GDPPrintTextParamFX");
            gdpPrintTextParamFXDialog = new GDPPrintTextParamFXDialog(mainFrame, title, true);
        }
        return gdpPrintTextParamFXDialog;
    }

    //�������� �����
    public ScheduleToExcelFXDialog getScheduleToExcelFXDialog() {
        if (scheduleToExcelFXDialog == null) {
            String title = bundle.getString("manager.ScheduleToExcelFX");
            scheduleToExcelFXDialog = new ScheduleToExcelFXDialog(mainFrame, title, true);
        }
        return scheduleToExcelFXDialog;
    }

    //������ �������������� ������ �� ������� ��������
    public ImportEnergyFromTRFXDialog getImportEnergyFromTRFXDialog(String distrName) {
        if (importEnergyFromTFXDialog == null) {
            String title = bundle.getString("manager.ImportEnergyFromTRFX") + ": " + distrName;
            importEnergyFromTFXDialog = new ImportEnergyFromTRFXDialog(mainFrame, title, true);
        }
        return importEnergyFromTFXDialog;
    }

    //������������ �������
    public DistrCombFXDialog getDistrCombFXDialog(String distrName) {
        if (distrCombFXDialog == null) {
            String title = bundle.getString("manager.DistrCombFX") + ": " + distrName;
            distrCombFXDialog = new DistrCombFXDialog(mainFrame, title, true);
        }
        return distrCombFXDialog;
    }

    //��������
    public ScaleChangeFXDialog getScaleChangeFXDialog(String distrName) {
        if (scaleChangeFXDialog == null) {
            String title = bundle.getString("manager.ScaleChangeFX") + ": " + distrName;
            scaleChangeFXDialog = new ScaleChangeFXDialog(mainFrame, title, true);
        }
        return scaleChangeFXDialog;
    }

    //��������� �������
    public AllTrainListFXDialog getAllTrainListFXDialog() {
        if (allTrainListFXDialog == null) {
            String title = bundle.getString("manager.TrainPropertysFX");
            allTrainListFXDialog = new AllTrainListFXDialog(mainFrame, title, true);
        }
        return allTrainListFXDialog;
    }

    //�������
    public StationInfoFXDialog getStationInfoFXDialog() {
        if (stationInfoFXDialog == null) {
            String title = bundle.getString("manager.StationInfoFX");
            stationInfoFXDialog = new StationInfoFXDialog(mainFrame, title, true);
        }
        return stationInfoFXDialog;
    }

    //������ ��������� � �������������� ������
    public CalculateTimeEnergyFXDialog getCalculateTimeEnergyFXDialog() {
        if (calculateTimeEnergyFXDialog == null) {
            String title = bundle.getString("manager.CalculateTimeEnergyFX");
            calculateTimeEnergyFXDialog = new CalculateTimeEnergyFXDialog(mainFrame, title, true);
        }
        return calculateTimeEnergyFXDialog;
    }

    //��������� �������
    public CategoryOfTrainsFXDialog getCategoryOfTrainsFXDialog() {
        if (categoryOfTrainsFXDialog == null) {
            String title = bundle.getString("manager.CategoryOfTrainsFX");
            categoryOfTrainsFXDialog = new CategoryOfTrainsFXDialog(mainFrame, title, true);
        }
        return categoryOfTrainsFXDialog;
    }


    //���������� ������� ����
    public SpanTimeFXDialog getSpanTimeFXDialog() {
        //private SpanTimeDialog spanTimeDialog = new SpanTimeDialog();
        //private SpanTimeFXDialog spanTimeDialog = new SpanTimeFXDialog();
        if (spanTimeFXDialog == null) {
            String title = bundle.getString("manager.SpanTimeFX");
            spanTimeFXDialog = new SpanTimeFXDialog(mainFrame, title, true);
        }
        return spanTimeFXDialog;
    }

    // �������� ��� �� xml
    public LoadFromXMLFXDialog getLoadFromXMLFXDialog() {
        if (loadFromXMLFXDialog == null) {
            String title = bundle.getString("manager.LoadFromXML");
            loadFromXMLFXDialog = new LoadFromXMLFXDialog(mainFrame, title, true);
        }
        return loadFromXMLFXDialog;
    }

    // ������ ���
    public CalculateGDPFXDialog getCalculateGDPFXDialog() {
        if (calculateGDPFXDialog == null) {
            String title = bundle.getString("manager.calcGDP");
            calculateGDPFXDialog = new CalculateGDPFXDialog(mainFrame, title, true);
        }
        return calculateGDPFXDialog;
    }
}
