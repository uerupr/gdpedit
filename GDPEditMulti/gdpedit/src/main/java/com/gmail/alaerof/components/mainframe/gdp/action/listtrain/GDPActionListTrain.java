package com.gmail.alaerof.components.mainframe.gdp.action.listtrain;

import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.dialog.ColorDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.entity.train.TrainXMLHandler;
import com.gmail.alaerof.history.ActionTrainAdd;
import com.gmail.alaerof.history.ActionTrainCopy;
import com.gmail.alaerof.history.ActionTrainDelete;
import com.gmail.alaerof.history.ActionTrainSaveAs;
import com.gmail.alaerof.history.History;
import com.gmail.alaerof.history.Action.TypeValue;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.BoxLayout;

import java.awt.FlowLayout;

import javax.swing.JComboBox;
import javax.swing.Box;

/**
 * ������ ������� � �������� � ���������� ������
 * @author Helen Yrofeeva
 */
public class GDPActionListTrain extends JPanel {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(GDPActionListTrain.class);
    private static final int DEL_KEY_CODE = 127;
    private static final int TRAIN_ADD = 0;
    private static final int TRAIN_SAVE_AS = 1;
    private static final int TRAIN_COPY = 2;
    private ListTrainTableModel tableModel;
    private JTable table;
    private JScrollPane tablePane;
    private DistrEditEntity distrEditEntity;

    private JCheckBox cbRange;
    private JTextField neRange1;
    private JTextField neRange2;
    private JCheckBox cbPattern;
    private JTextField mePattern;
    private JCheckBox cbSt;
    private JComboBox<DistrStation> cbStB;
    private JComboBox<DistrStation> cbStE;
    private JCheckBox cbBO;
    private JCheckBox cbBE;
    private JTextField edWTR;
    private JTextField searchTrainTextField;
    private static ResourceBundle bundle;

    public GDPActionListTrain() {
        initPanel();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.listtrain", LocaleManager.getLocale());
    }

    private void initPanel() {
        // ������� ������
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createLineBorder(Color.LIGHT_GRAY)));
        this.setLayout(new BorderLayout());

        Font tableFont = new Font(Font.SANS_SERIF, 0, 11);
        tableModel = new ListTrainTableModel();

        table = new JTable(tableModel);
        tablePane = new JScrollPane(table);
        table.setFont(tableFont);
        table.getTableHeader().setFont(tableFont);
        table.setFillsViewportHeight(true);

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSize[] = { 40, 30, 30, 50 };
        TableUtils.setTableColumnSize(table, colSize);

        table.setDefaultRenderer(Object.class, new ListTrainCellRenderer());

        table.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    performTrainTableMouseClick(e);
                } catch (Exception ex) {
                    logger.error(e.toString(), ex);
                }
            }
        });

        table.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                try {
                    if (e.getKeyCode() == DEL_KEY_CODE) {
                        trainDeleteAllSelected();
                    }
                } catch (Exception ex) {
                    logger.error(ex.toString(), ex);
                }
            }
        });

        this.add(tablePane, BorderLayout.CENTER);

        JPanel bottom = new JPanel();
        this.add(bottom, BorderLayout.SOUTH);
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.PAGE_AXIS));
        bottom.setIgnoreRepaint(true);
        bottom.setMinimumSize(new Dimension(10, 10));
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);

            searchTrainTextField = new JTextField();
            panel.add(searchTrainTextField);
            searchTrainTextField.setColumns(7);
            {
                JButton button = new JButton();
                button.setIcon(IconImageUtil.getIcon(IconImageUtil.SEARCH));
                button.setToolTipText(bundle.getString("listtrain.search"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        trainSearch();
                    }
                });
            }
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);
            {
                JButton button = new JButton();
                panel.add(button);
                button.setIcon(IconImageUtil.getIcon(IconImageUtil.PLUS));
                button.setToolTipText(bundle.getString("listtrain.addnew"));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        processTrainAction(TRAIN_ADD);
                    }
                });
            }
            {
                JButton button = new JButton();
                panel.add(button);
                button.setIcon(IconImageUtil.getIcon(IconImageUtil.MINUS));
                button.setToolTipText(bundle.getString("list.train.delete.selected"));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        trainDeleteAllSelected();
                    }
                });
            }

            {
                // ����. ���
                JButton button = new JButton(bundle.getString("listtrain.saveas"));
                panel.add(button);
                button.setToolTipText(bundle.getString("listtrain.rename"));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        processTrainAction(TRAIN_SAVE_AS);
                    }
                });
            }
            {
                // �����.
                JButton button = new JButton(bundle.getString("listtrain.copybriefly"));
                panel.add(button);
                button.setToolTipText(bundle.getString("listtrain.copy"));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        processTrainAction(TRAIN_COPY);
                    }
                });
            }
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);
            {
                JLabel lDisplay = new JLabel(bundle.getString("listtrain.display"));
                panel.add(lDisplay);
            }
            {
                JCheckBox cbGr = new JCheckBox(bundle.getString("listtrain.gr"));
                cbGr.setSelected(true);
                panel.add(cbGr);
                cbGr.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        hideTrains(TrainType.gr, !cbGr.isSelected());
                    }
                });
            }
            {
                JCheckBox cbPass = new JCheckBox(bundle.getString("listtrain.pass"));
                cbPass.setSelected(true);
                panel.add(cbPass);
                cbPass.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        hideTrains(TrainType.pass, !cbPass.isSelected());
                    }
                });
            }
            {
                JCheckBox cbTown = new JCheckBox(bundle.getString("listtrain.town"));
                cbTown.setSelected(true);
                panel.add(cbTown);
                cbTown.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        hideTrains(TrainType.town, !cbTown.isSelected());
                    }
                });
            }
        }

        final ButtonGroup groupRange = new ButtonGroup();
        MouseListener groupRangeListener = new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    groupRange.clearSelection();
                }
            }
        };
        {
            JPanel panel = new JPanel();
            bottom.add(panel);
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            {
                cbRange = new JCheckBox(bundle.getString("listtrain.range"));
                panel.add(cbRange);
                groupRange.add(cbRange);
                cbRange.addMouseListener(groupRangeListener);
            }
            {
                neRange1 = new JTextField();
                panel.add(neRange1);
                neRange1.setColumns(4);
            }
            {
                neRange2 = new JTextField();
                panel.add(neRange2);
                neRange2.setColumns(4);
            }
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);
            {
                cbPattern = new JCheckBox(bundle.getString("listtrain.template"));
                panel.add(cbPattern);
                groupRange.add(cbPattern);
                cbPattern.addMouseListener(groupRangeListener);
            }
            {
                mePattern = new JTextField();
                mePattern.setText("");
                panel.add(mePattern);
                mePattern.setColumns(4);
                // * �������� ����� ����� �� ���� �����,
                // ���������� ��������� �������� �������� ���������� ���� � ������ ������.
                mePattern.setToolTipText(bundle.getString("listtrain.meansanydigit"));
            }
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);
            {
                cbBO = new JCheckBox(bundle.getString("listtrain.noteven"));
                panel.add(cbBO);
            }
            {
                cbBE = new JCheckBox(bundle.getString("listtrain.even"));
                panel.add(cbBE);
            }
        }
        {
            JPanel panel = new JPanel();
            bottom.add(panel);
            panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
            // panel.setAlignmentX(Component.RIGHT_ALIGNMENT);
            // panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            {
                Component horizontalStrut = Box.createHorizontalStrut(5);
                panel.add(horizontalStrut);
            }
            {

                cbSt = new JCheckBox(bundle.getString("listtrain.stations"));
                panel.add(cbSt);
            }
            {
                Component horizontalStrut = Box.createHorizontalStrut(10);
                panel.add(horizontalStrut);
            }

            {
                JPanel panel_1 = new JPanel();
                panel.add(panel_1);
                panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
                {
                    cbStB = new JComboBox<>();
                    panel_1.add(cbStB);
                }
                {
                    cbStE = new JComboBox<>();
                    panel_1.add(cbStE);
                }
            }
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);
            {
                JButton button = new JButton(bundle.getString("listtrain.color"));
                button.setToolTipText(bundle.getString("listtrain.changecolorselected"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ColorDialog cd = GDPEdit.gdpEdit.getDialogManager().getColorDialog();
                        cd.setVisible(true);
                        if (cd.getResult() > 0) {
                            applyTrainAction(1, cd.getColor(), 0);
                        }
                    }
                });
            }
            {
                JButton button = new JButton(bundle.getString("listtrain.thickness"));
                button.setToolTipText(bundle.getString("listtrain.changethicknessselected"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            int w = Integer.parseInt(edWTR.getText());
                            applyTrainAction(2, null, w);
                        } catch (Exception err) {
                            logger.error(err.toString(), err);
                        }
                    }
                });
            }
            {
                edWTR = new JTextField();
                panel.add(edWTR);
                edWTR.setColumns(4);
            }
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);
            {
                JButton button = new JButton(bundle.getString("listtrain.hide"));
                button.setToolTipText(bundle.getString("listtrain.hideselected"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        applyTrainAction(3, null, 0);
                    }
                });
            }
            {
                JButton button = new JButton(bundle.getString("listtrain.show"));
                button.setToolTipText(bundle.getString("listtrain.showselected"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        applyTrainAction(4, null, 0);
                    }
                });
            }
        }
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
            bottom.add(panel);
            {
                JButton button = new JButton(bundle.getString("listtrain.plusnumber"));
                button.setToolTipText(bundle.getString("listtrain.showtrainnumber"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        applyTrainAction(5, null, 0);
                    }
                });
            }
            {
                JButton button = new JButton(bundle.getString("listtrain.minusnumber"));
                button.setToolTipText(bundle.getString("listtrain.hidetrainnumber"));
                panel.add(button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        applyTrainAction(6, null, 0);
                    }
                });
            }
        }
    }

    protected void performTrainTableMouseClick(MouseEvent e) {
        int row = table.getSelectedRow();
        TrainThread train = tableModel.getTrain(row);
        if (train != null) {
            switch (e.getClickCount()) {
            case 1:
                GDPEditPanel ep = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
                setTrainWidth(train);
                if (ep != null) {
                    ep.setCurrentTrain(train);
                    ep.setCurrentTrainListSt();
                    ep.updateHorScrollPosition();
                } else{
                    GDPEdit.gdpEdit.getGDPActionPanel().getActionListSt().setCurrentTrainThread(train);
                }
                break;
            case 2:
                boolean h = train.isHidden();
                train.setHidden(!h);
                table.repaint();
                train.setTrainLocation();
                break;
            default:
                break;
            }
        }
    }

    /**
     * ����� ������ �� ������ � ������ ������� � ����� ��� � ������ �� ������
     */
    protected void trainSearch() {
        String codeTR = searchTrainTextField.getText();
        List<TrainThread> listTR = tableModel.getListTrain();
        TrainThread res = ListTrainHandler.searchTrain(codeTR, listTR);
        if (res != null) {
            GDPEditPanel ep = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (ep != null) {
                ep.setCurrentTrain(res);
                ep.setCurrentTrainListSt();
                ep.setCurrentTrainListTrain();
            }
        }
    }

    /**
     * ���������� �������� � ������ ������:
     *  0 - ����������
     *  1 - ��������� ���
     *  2 - �����������
     * @param action  0 - TRAIN_ADD, 1 -  TRAIN_SAVE_AS, 2 - TRAIN_COPY
     */
    private void processTrainAction(int action) {
        String codeStr = null;
        String name = null;
        TrainThread train = null;
        String trName = null;
        if (distrEditEntity != null) {
            trName = new String(distrEditEntity.getDistrEntity().getDistrName().substring(0, 2));
        }
        // Get the size of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        // Determine the new location of the gdpEdit
        int x = (dim.width) / 2;
        int y = (dim.height) / 2;
        NewTrainDialog dialog = GDPEdit.gdpEdit.getDialogManager().getNewTrainDialog();
        dialog.setLocation(x, y);
        dialog.setTrainName(trName);
        dialog.setVisible(true);
        int result = dialog.getResult();
        if (result == 1) {
            codeStr = dialog.getTrainCode();
            name = dialog.getTrainName();
        }
        if (action != TRAIN_ADD) {
            int index = table.getSelectedRow();
            if (index > -1) {
                train = tableModel.getTrain(index);
            }
        }
        if (codeStr != null && name != null) {
            int code = 0;
            try {
                code = Integer.parseInt(codeStr);
            } catch (Exception e) {
                code = 0;
                // �������� ���������
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                        bundle.getString("listtrain.integeronly"),
                        bundle.getString("listtrain.warning"),
                        JOptionPane.CANCEL_OPTION);

                logger.error(e.toString(), e);
            }
            if (code > 0) {
                switch (action) {
                case TRAIN_ADD:
                    trainAdd(code, name);
                    break;
                case TRAIN_SAVE_AS:
                    trainSaveAs(code, name, train);
                    break;
                case TRAIN_COPY:
                    trainCopy(code, name, train);
                    break;
                default:
                    break;
                }
            }
        }
    }

    public void fillListSt(List<DistrStation> listSt) {
        cbStB.removeAllItems();
        cbStE.removeAllItems();
        if (listSt != null) {
            for (int i = 0; i < listSt.size(); i++) {
                DistrStation st = listSt.get(i);
                cbStB.addItem(st);
                cbStE.addItem(st);
            }
        }
    }

    public List<TrainThread> getListTrain() {
        return tableModel.getListTrain();
    }

    public void setDistrEditEntity(DistrEditEntity distrEditEntity) {
        this.distrEditEntity = distrEditEntity;
        if (distrEditEntity != null) {
            fillListSt(distrEditEntity.getDistrEntity().getListSt());
        } else {
            fillListSt(null);
        }
        updateListTrain();
    }

    public void updateListTrain() {
        List<TrainThread> listTrain = null;
        if (distrEditEntity != null) {
            listTrain = this.distrEditEntity.getListTrains();
        }
        tableModel.setListTrain(listTrain);
        table.repaint();
    }

    public void setCurrentTrainThread(TrainThread train) {
        int index = tableModel.getTrainIndex(train);
        if (index > -1) {
            table.changeSelection(index, 0, false, false);
        }
        GDPEdit.gdpEdit.getGDPActionPanel().getActionListSt().setCurrentTrainThread(train);
        GDPEditPanel ep = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
        ep.setCurrentTrain(train);
    }

    public void setTrainWidth(TrainThread train) {
        int iTrainWidth = train.getWidthTrain();
        String sTrainWidth = String.valueOf(iTrainWidth);
        edWTR.setText(sTrainWidth);
    }

    public TrainThread getCurrent() {
        int index = table.getSelectedRow();
        TrainThread train = null;
        if (index > -1) {
            train = tableModel.getTrain(index);
        }
        return train;
    }

    private void trainSaveAs(int code, String name, TrainThread train) {
        TrainThread newTrain = distrEditEntity.createTrain(code, name);
        if (newTrain == null) {
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                    bundle.getString("listtrain.traincouldnotbecreated") + code + "(" + name + ")",
                    bundle.getString("listtrain.warning"),
                    JOptionPane.ERROR_MESSAGE);
        }
        if (newTrain != null && train != null) {
            String o = train.getCode() + "(" + train.getNameTR() + ")";
            String n = "" + code + "(" + name + ")";
            // ���������� � ������� �������� ����������� ��������
            History history = train.getHistory();
            ActionTrainSaveAs action = new ActionTrainSaveAs(train, o, n);
            history.addAction(action, TypeValue.oldValue);

            // ��������� ��������
            train.saveAs(newTrain.getIdTrain(), newTrain.getCode(), newTrain.getNameTR());

            // ���������� � ������� �������� ������ ��������
            history.addAction(action, TypeValue.newValue);

            // ----------
            train.setTrainLocation();
            updateListTrain();
            setCurrentTrainThread(train);
            train.repaint();
        }
    }

    private void trainCopy(int code, String name, TrainThread train) {
        TrainThread newTrain = distrEditEntity.createTrain(code, name);
        if (newTrain == null) {
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                    bundle.getString("listtrain.traincouldnotbecreated") + code + "(" + name + ")",
                    bundle.getString("listtrain.warning"),
                    JOptionPane.ERROR_MESSAGE);
        }

        if (newTrain != null && train != null) {
            String oldCodeTR = train.getCode() + "(" + train.getNameTR() + ")";
            String newCodeTR = "" + code + "(" + name + ")";

            org.w3c.dom.Document doc = TrainXMLHandler.getDocument();
            Element root = doc.getDocumentElement();
            Element el = TrainXMLHandler.saveToXML(train, doc);
            root.appendChild(el);
            TrainXMLHandler.loadAllFromXML(newTrain, root, false);
            newTrain.getDistrTrain().setCodeTR("" + code);
            newTrain.getDistrTrain().setNameTR(name);
            newTrain.updateShowParamTimeSt();
            distrEditEntity.addTrain(newTrain);
            newTrain.setTrainLocation();
            updateListTrain();
            setCurrentTrainThread(newTrain);
            newTrain.repaint();
            // ���������� � ������� �������� ��������
            History history = train.getHistory();
            ActionTrainCopy action = new ActionTrainCopy(newTrain, distrEditEntity, oldCodeTR, newCodeTR);
            history.addAction(action, TypeValue.newValue);
        }
    }

    private void trainAdd(int code, String name) {
        TrainThread train = distrEditEntity.createTrain(code, name);
        if (train != null) {
            distrEditEntity.addTrain(train);
            updateListTrain();
            setCurrentTrainThread(train);
            // ���������� � ������� �������� ��������
            History history = train.getHistory();
            ActionTrainAdd action = new ActionTrainAdd(train, distrEditEntity);
            history.addAction(action, TypeValue.newValue);
        } else {
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                    bundle.getString("listtrain.traincouldnotbecreated") + code + "(" + name + ")",
                    bundle.getString("listtrain.warning"),
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private void trainDeleteAllSelected() {
        int[] indexes = table.getSelectedRows();
        setCursor(GDPEdit.waitCursor);
        try {
            StringBuilder trainsName = new StringBuilder();
            int countYourTrain = 0;
            for (int index : indexes) {
                TrainThread train = tableModel.getTrain(index);
                if(!train.isForeign()) {
                    trainsName.append(train.getCode() + " (" + train.getNameTR() + "), ");
                    countYourTrain = countYourTrain + 1;
                }
            }
            String tn = "";
            if (countYourTrain == 0) {
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                        bundle.getString("listtrain.allselectedtoremove"));
            } else {
                if (countYourTrain == 1) {
                    tn = bundle.getString("listtrain.removetrain") + trainsName.substring(0, trainsName.length() - 2);
                } else {
                    tn = bundle.getString("listtrain.removetrains") + trainsName.substring(0, trainsName.length() - 2);
                }
            }
            if (indexes.length > 0 && countYourTrain > 0) {
                int result = JOptionPane.showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(),
                        tn, bundle.getString("listtrain.warning"),
                        JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    for (int index : indexes) {
                        TrainThread train = tableModel.getTrain(index);
                        if(!train.isForeign()) {
                            trainDeleteFromDistrEditEntity(train);
                        }
                    }
                    updateListTrain();
                    if (indexes[0] <= table.getRowCount() - 1) {
                        table.changeSelection(indexes[0], 0, false, false);
                    }
                    JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                            bundle.getString("listtrain.ok"));
                }
            }
        } finally {
            setCursor(GDPEdit.defCursor);
        }
    }

    private void trainDeleteFromDistrEditEntity(TrainThread train) {
        distrEditEntity.removeTrain(train);
        // ���������� � ������� �������� ��������
        History history = train.getHistory();
        ActionTrainDelete action = new ActionTrainDelete(train, distrEditEntity);
        history.addAction(action, TypeValue.oldValue);
    }

    private List<TrainThread> filterListTrain(List<TrainThread> listTrain) {
        List<TrainThread> filtered = new ArrayList<>(listTrain);
        boolean useFilter = false;
        if (cbRange.isSelected()) {
            filtered = filterByRange(filtered);
            useFilter = true;
        }
        if (cbPattern.isSelected()) {
            filtered = filterByPattern(filtered);
            useFilter = true;
        }
        if (cbBE.isSelected() || cbBO.isSelected()) {
            filtered = filterByOE(filtered);
            useFilter = true;
        }
        if (useFilter)
            return filtered;
        else
            return new ArrayList<>();
    }

    private List<TrainThread> filterByOE(List<TrainThread> listTrain) {
        List<TrainThread> filtered = new ArrayList<>();
        boolean odd = cbBO.isSelected();
        boolean even = cbBE.isSelected();
        if (odd && even) {
            filtered = listTrain;
        } else {
            if (odd) {
                for (int i = 0; i < listTrain.size(); i++) {
                    TrainThread tr = listTrain.get(i);
                    if (tr.getOe() == OE.odd) {
                        filtered.add(tr);
                    }
                }
            } else {
                if (even) {
                    for (int i = 0; i < listTrain.size(); i++) {
                        TrainThread tr = listTrain.get(i);
                        if (tr.getOe() == OE.even) {
                            filtered.add(tr);
                        }
                    }
                }
            }

        }
        return filtered;
    }

    private List<TrainThread> filterByPattern(List<TrainThread> listTrain) {
        List<TrainThread> filtered = new ArrayList<>();
        StringBuilder regex = new StringBuilder("[\\d\\*]+");
        String value = "";
        value = mePattern.getText();

        if (Pattern.matches(regex.toString(), value)) {
            regex = new StringBuilder();
            for (int i = 0; i < value.length(); i++) {
                if (value.charAt(i) == '*') {
                    regex.append("\\d");
                } else {
                    regex.append(value.charAt(i));
                }
            }
            for (int i = 0; i < listTrain.size(); i++) {
                TrainThread tr = listTrain.get(i);
                String code = tr.getCode();
                if (Pattern.matches(regex.toString(), code)) {
                    filtered.add(tr);
                }
            }
        } else {
            // �������� ���������
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                    bundle.getString("listtrain.templatecanbeentered"),
                    bundle.getString("listtrain.warning"),
                    JOptionPane.CANCEL_OPTION);
        }

        return filtered;
    }

    private List<TrainThread> filterByRange(List<TrainThread> listTrain) {
        List<TrainThread> filtered = new ArrayList<>();
        int begin = 0;
        int end = 0;
        try {
            begin = Integer.parseInt(neRange1.getText());
            end = Integer.parseInt(neRange2.getText());

            // ����������, ���� ��� ������
            for (int i = 0; i < listTrain.size(); i++) {
                TrainThread tr = listTrain.get(i);
                int code = tr.getCodeInt();
                if (code >= begin && code <= end) {
                    filtered.add(tr);
                }
            }
        } catch (NumberFormatException e) {
            // �������� ���������
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                    bundle.getString("listtrain.rangecanbeentered"),
                    bundle.getString("listtrain.warning"),
                    JOptionPane.CANCEL_OPTION);
            logger.error(bundle.getString("listtrain.rangeoftrain") + e.toString(), e);
        }
        return filtered;
    }

    /**
     *
     * @param action 1-����, 2- �������, 3- ������, 4-��������, 5-�������� ���, 6-������ ���
     * @param color ����
     * @param width �������
     */
    private void applyTrainAction(int action, Color color, int width) {
        List<TrainThread> list = getListTrain();
        if (list != null) {
            list = filterListTrain(list);
            int idStB = -1;
            int idStE = -1;
            if (cbSt.isSelected()) {
                DistrStation st = (DistrStation) cbStB.getSelectedItem();
                if (st != null) {
                    idStB = st.getIdPoint();
                }
                st = (DistrStation) cbStE.getSelectedItem();
                if (st != null) {
                    idStE = st.getIdPoint();
                }
            }
            if (list.size() == 0) {
                list.addAll(getSelectedTrains());
            }

            for (int i = 0; i < list.size(); i++) {
                TrainThread tr = list.get(i);
                switch (action) {
                case 1:
                    tr.setColor(color, idStB, idStE);
                    tr.setTrainLocation();
                    break;

                case 2:
                    tr.setWidth(width);
                    tr.setTrainLocation();
                    break;

                case 3:
                    tr.setHidden(true, idStB, idStE);
                    tr.setTrainLocation();
                    break;

                case 4:
                    tr.setHidden(false, idStB, idStE);
                    tr.setTrainLocation();
                    break;

                case 5:
                    tr.setViewCode(true, idStB);
                    tr.setTrainLocation();
                    break;
                case 6:
                    tr.setViewCode(false, idStB);
                    tr.setTrainLocation();
                    break;

                default:
                    break;
                }
            }
            table.repaint();
        }

    }

    /**
     *
     * @param trainType ��� ������
     */
    private void hideTrains(TrainType trainType, boolean hidden) {
        List<TrainThread> list = getListTrain();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                TrainThread tr = list.get(i);
                DistrTrain distrTrain = tr.getDistrTrain();
                if (distrTrain.getTypeTR() == trainType) {
                    tr.setHidden(hidden);
                    tr.setTrainLocation();
                }
            }
            table.repaint();
        }
    }

    /**
     * @return  Collection selected TrainThreads
     */
    public Collection<? extends TrainThread> getSelectedTrains() {
        List<TrainThread> selectedTrains = new ArrayList<>();
        int[] indexes = table.getSelectedRows();
        for (int index : indexes) {
            TrainThread train = tableModel.getTrain(index);
            selectedTrains.add(train);
        }
        return selectedTrains;
    }
}

class ListTrainCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int col) {
        if (value != null)
            setText(value.toString());
        List<TrainThread> listTrain = ((ListTrainTableModel) table.getModel()).getListTrain();
        TrainThread train = listTrain.get(row);

        Color background;
        Color foreground = Color.BLACK;
        if (row % 2 == 0) {
            background = Color.WHITE;
        } else {
            background = new Color(242, 242, 242);
        }
        if (isSelected) {
            background = Color.LIGHT_GRAY;
        }

        Font oldFont = getFont();
        if (col == 0) {
            Font newFont = new java.awt.Font(oldFont.getName(), java.awt.Font.BOLD, oldFont.getSize());
            setFont(newFont);
            setForeground(ColorConverter.convert(train.getColor()));
        } else {
            Font newFont = new java.awt.Font(oldFont.getName(), 0, oldFont.getSize());
            setFont(newFont);
            setForeground(foreground);
        }

        setBackground(background);

        return this;
    }
}
