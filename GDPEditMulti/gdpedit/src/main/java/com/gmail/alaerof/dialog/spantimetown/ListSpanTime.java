package com.gmail.alaerof.dialog.spantimetown;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.util.TableUtils;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
public class ListSpanTime extends JPanel {
    private static final long serialVersionUID = 1L;
    private SpanTimeTableModel tableModel;
    private JTable table;
    private DistrEntity distrEntity;
    private JComboBox<LocomotiveType> comboBoxLoc;

    public ListSpanTime() {
        initPanel();
    }

    private void initPanel() {
        setLayout(new BorderLayout(0, 0));
        {
            JPanel panel = new JPanel();
            add(panel, BorderLayout.NORTH);

            comboBoxLoc = new JComboBox<>();
            fillListLoc();
            panel.add(comboBoxLoc);
            comboBoxLoc.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    tableModel.setLocType((LocomotiveType) comboBoxLoc.getSelectedItem());
                    table.repaint();
                }
            });
        }
        {
            Font tableFont = new Font(Font.SANS_SERIF, 0, 11);
            tableModel = new SpanTimeTableModel();
            table = new JTable(tableModel);
            JScrollPane tablePane = new JScrollPane(table);
            table.setFont(tableFont);
            table.getTableHeader().setFont(tableFont);
            table.setFillsViewportHeight(true);

            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 100, 50, 50 };
            TableUtils.setTableColumnSize(table, colSize);
            this.add(tablePane, BorderLayout.CENTER);

        }

    }

    public DistrEntity getDistrEntity() {
        return distrEntity;
    }

    public void setDistrEntity(DistrEntity distrEntity) {
        this.distrEntity = distrEntity;
        tableModel.setDistrEntity(distrEntity);
        fillListLoc();
    }

    private void fillListLoc() {
        comboBoxLoc.removeAllItems();
        if (distrEntity != null) {
            List<LocomotiveType> list = distrEntity.getListDistrLoc();
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    comboBoxLoc.addItem(list.get(i));
                }
            }
            if (list.size()>2) {
                comboBoxLoc.setSelectedIndex(2);
                tableModel.setLocType((LocomotiveType) comboBoxLoc.getSelectedItem());
                table.repaint();
            }
        }
    }

}
