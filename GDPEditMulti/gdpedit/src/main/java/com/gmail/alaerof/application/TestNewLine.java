package com.gmail.alaerof.application;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.gmail.alaerof.entity.train.LineHor;
import com.gmail.alaerof.entity.train.LineLeft;
import com.gmail.alaerof.entity.train.LineRight;
import com.gmail.alaerof.entity.train.LineStyle;
import com.gmail.alaerof.entity.train.LineVert;
import com.gmail.alaerof.entity.train.NewLine;
import com.gmail.alaerof.entity.train.NewLineSelfParams;

public class TestNewLine {

    private JFrame frame;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    TestNewLine window = new TestNewLine();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    JButton but;

    public TestNewLine() {
        // ������ ��������
        // ��� ���� ������ ��������
        frame = new JFrame();
        frame.setBounds(100, 100, 750, 600);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        but = new JButton("hoho");
        but.setBounds(200, 2, 80, 20);
        frame.add(but);

        initialize2();
    }

    /**
     * Initialize the contents of the frame.
     */
    @SuppressWarnings("unused")
    private void initialize() {
        final JLabel lb = new JLabel("---");
        lb.setBounds(2, 2, 100, 20);
        frame.add(lb);

        TrainThreadGeneralParams params = new TrainThreadGeneralParams();
        int h = 60;
        int w = 40;
        int x = 0;
        int y = 40;
        for (int i = 0; i < 10; i++) {
            x = 0;
            for (int j = 0; j < 60; j++) {
                NewLineSelfParams selfParams = new NewLineSelfParams();
                final NewLine nl = new LineLeft(params, selfParams, null);
                nl.setCaption(NewLine.DrawCaption.Center);
                nl.setOriginalSize(new Dimension(w - 6, h - 4));
                frame.add(nl);
                nl.setCaptionValue("line " + i + " " + j);
                final String line = nl.getCaptionValue();
                nl.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        lb.setText(line);
                        java.awt.Rectangle r = nl.getBounds();
                        r.height += 3;
                        nl.setBounds(r);
                    }
                });
                nl.setBounds(x, y, w, h);
                x += w + 2;
            }
            y += h + 2;
        }

    }

    private void initialize2() {
        /** ��������/����������� ������, ����� */
        Font trainTimeFont = new Font(Font.SANS_SERIF, 0, 10);
        /** ������� ������ (�������), ����� */
        Font trainCaptionSpFont = new Font("Times New Roman", Font.BOLD, 14);
        /** ������� ������ (�������), ����� */
        Font trainCaptionStFont = new Font("Times New Roman", 0, 10);

        TrainThreadGeneralParams params = new TrainThreadGeneralParams();
        params.captionSp = trainCaptionSpFont;
        params.captionSt = trainCaptionStFont;
        params.time = trainTimeFont;

        NewLineSelfParams vertParams = new NewLineSelfParams();
        NewLineSelfParams horParams = new NewLineSelfParams();
        NewLineSelfParams leftParams = new NewLineSelfParams();
        NewLineSelfParams rightParams = new NewLineSelfParams();

        rightParams.lineStyle = LineStyle.Double;
        leftParams.lineStyle = LineStyle.Double;
        horParams.lineStyle = LineStyle.Double;
        vertParams.lineStyle = LineStyle.Wave;
        
        NewLine lineVert = new LineVert(params, vertParams, null);
        NewLine lineHor = new LineHor(params, horParams, null);
        final NewLine lineLeft = new LineLeft(params, leftParams, null);
        NewLine lineRight = new LineRight(params, rightParams, null);
        
        lineVert.getOriginalSize().width = 2;
        lineVert.getOriginalSize().height = 100;
        lineHor.getOriginalSize().width = 100;
        lineHor.getOriginalSize().height = 40;
        lineLeft.getOriginalSize().width = 300;
        lineLeft.getOriginalSize().height = 100;
        lineRight.getOriginalSize().width = 300;
        lineRight.getOriginalSize().height = 100;
        
        lineHor.setCaption(NewLine.DrawCaption.texOff);
        lineLeft.setCaption(NewLine.DrawCaption.Center);
        lineRight.setCaption(NewLine.DrawCaption.Center);

        lineLeft.setTimeA(true);
        lineLeft.setTimeL(true);

        lineRight.setTimeA(true);
        lineRight.setTimeL(true);

        frame.add(lineVert);
        frame.add(lineHor);
        frame.add(lineLeft);
        frame.add(lineRight);

        int w = lineVert.getIndent().width * 2 + lineVert.getOriginalSize().width;
        int h = lineVert.getIndent().height * 2 + lineVert.getOriginalSize().height;
        lineVert.setBounds(10, 10, w, h);

        w = lineHor.getIndent().width * 2 + lineHor.getOriginalSize().width;
        h = lineHor.getIndent().height * 2 + lineHor.getOriginalSize().height;
        lineHor.setBounds(30, 10, w, h);

        w = lineLeft.getIndent().width * 2 + lineLeft.getOriginalSize().width;
        h = lineLeft.getIndent().height * 2 + lineLeft.getOriginalSize().height;
        lineLeft.setBounds(40 + w, 60, w, h);

        w = lineRight.getIndent().width * 2 + lineRight.getOriginalSize().width;
        h = lineRight.getIndent().height * 2 + lineRight.getOriginalSize().height;
        lineRight.setBounds(40, 60, w, h);

        but.addActionListener(new ActionListener() {
            boolean x;

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                lineLeft.setVisible(x);
                x = !x;
            }
        });

    }

}
