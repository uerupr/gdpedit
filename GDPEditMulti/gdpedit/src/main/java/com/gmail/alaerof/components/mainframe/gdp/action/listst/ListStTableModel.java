package com.gmail.alaerof.components.mainframe.gdp.action.listst;

import com.gmail.alaerof.entity.train.TrainStTableData;

import java.util.ArrayList;

import java.util.List;
import java.util.ResourceBundle;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.TrainStop;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrSpanSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.entity.train.TimeStation;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.history.ActionTrainEditTime;
import com.gmail.alaerof.history.Action.TypeValue;
import com.gmail.alaerof.history.History;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ListStTableModel extends AbstractTableModel implements TableModelListener {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = {bundle.getString("listst.station"),
            bundle.getString("list.st.modearrive"),
            bundle.getString("list.st.modeleave"),
            bundle.getString("listst.pt"),
            bundle.getString("listst.min"),
            bundle.getString("listst.way"),
            bundle.getString("listst.tech"),
            bundle.getString("listst.stock")};
    private List<DistrPoint> listDistrPoint;
    private TrainThread train;
    private boolean town;
    private Object[][] data;
    private boolean dataChanged;
    private TrainScheduleTable table;
    private int sRow = -1;
    private int sCol = -1;
    private static ResourceBundle bundle;

    private TimeMode timeMode = TimeMode.Leave;

    static {
        bundle = ResourceBundle.getBundle("bundles.listst", LocaleManager.getLocale());
    }

    {
        this.addTableModelListener(this);
    }

    private class St {
        String name;
        int id;
        boolean town = false;

        public String toString() {
            return name;
        }
    }

    private DistrPoint getDPbyID(St st) {
        DistrPoint dp = null;
        for (DistrPoint d : listDistrPoint) {
            if (d.getIdPoint() == st.id
                    && ((d.getTypePoint() == DistrPoint.TypePoint.SeparatePoint && !st.town) || (d
                            .getTypePoint() == DistrPoint.TypePoint.StopPoint && st.town))) {
                dp = d;
            }
        }
        return dp;
    }

    /**
     * ���������� ������� �������
     */
    public void setData(boolean fillSt) {
        if (listDistrPoint != null) {
            int rowCount = getRowCount();
            if (fillSt) {
                data = new Object[rowCount][getColumnCount()];
                int j = 0;
                for (int i = 0; i < listDistrPoint.size(); i++) {
                    DistrPoint dp = listDistrPoint.get(i);
                    St st = new St();
                    st.id = dp.getIdPoint();
                    if (!town) {
                        if (dp.getTypePoint() == DistrPoint.TypePoint.SeparatePoint) {
                            st.name = dp.getNamePoint();
                            data[j][0] = st;
                            j++;
                        }
                    } else {
                        if (dp.getTypePoint() == DistrPoint.TypePoint.SeparatePoint) {
                            st.name = ".     " + dp.getNamePoint();
                            data[i][0] = st;
                        } else {
                            st.name = bundle.getString("list.st.stoppoint") + dp.getNamePoint();
                            st.town = true;
                            data[i][0] = st;
                        }
                    }
                }
            } else {
                for (int i = 0; i < rowCount; i++) {
                    for (int j = 1; j < 8; j++)
                        data[i][j] = null;
                    if (train != null) {
                        St st = (St) data[i][0];
                        DistrPoint dp = getDPbyID(st);
                        if (dp != null) {
                            if (dp.getTypePoint() == DistrPoint.TypePoint.SeparatePoint) {
                                DistrStation ds = (DistrStation) dp;
                                TimeStation timeSt = train.getTimeSt(ds.getStation().getIdSt());
                                if (timeSt != null) {
                                    // 0"�������", 1"����.", 2"����.", 3"��",
                                    // 4"���", 5"����", 6"���.", 7"�����"
                                    LineStTrain ln = timeSt.getLineStTrain();
                                    String tOn = ln.getOccupyOn();
                                    String tOff = ln.getOccupyOff();
                                    if (tOn.equals(tOff)) {
                                        if(timeSt.isFirst()||timeSt.isLast()){
                                            if(timeSt.isNullOn()){
                                              tOn = "";
                                            }
                                            if(timeSt.isNullOff()){
                                              tOff = "";
                                            }
                                        } else {
                                            if (timeMode == TimeMode.Leave) {
                                                tOn = "";
                                            } else if (timeMode == TimeMode.Arrive) {
                                                tOff = "";
                                            }
                                        }
                                    }
                                    data[i][1] = tOn;
                                    data[i][2] = tOff;
                                    data[i][3] = ln.getSk_ps();
                                    if (ln.getTstop() != 0)
                                        data[i][4] = ln.getTstop();
                                    data[i][5] = timeSt.getLineName(ln.getIdLineSt());
                                    if (ln.getTexStop() != 0)
                                        data[i][6] = ln.getTexStop();
                                    if (ln.getOverTime() != 0)
                                        data[i][7] = ln.getOverTime();
                                }
                            } else {
                                DistrSpanSt ds = (DistrSpanSt) dp;
                                TrainStop ts = train.getTrainStopByID(ds.getSpanSt().getIdSpanst());
                                if (ts != null) {
                                    // 0"�������", 1"����.", 2"����.", 3"��",
                                    // 4"���", 5"����", 6"���.", 7"�����"
                                    data[i][1] = ts.getOccupyOn();
                                    data[i][2] = ts.getOccupyOff();
                                    data[i][3] = ts.getSk_ps();
                                    if (ts.getTstop() != 0)
                                        data[i][4] = ts.getTstop();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * ��������� ���������� ������, � �����. � ��������
     * 
     * @param town
     *            �.�. "�������"
     */
    public void applyData(boolean town) {
        ArrayList<TrainStTableData> listStTableData = new ArrayList<>();
        if (listDistrPoint != null && train != null) {
            int rowCount = getRowCount();
            for (int i = 0; i < rowCount; i++) {
                St st = (St) data[i][0];
                // 0"�������", 1"����.", 2"����.", 3"��",
                // 4"���", 5"����", 6"���.", 7"�����"
                String tOn = "";
                String tOff = "";
                if (data[i][1] != null) {
                    tOn = (data[i][1]).toString();
                }
                if (data[i][2] != null) {
                    tOff = (data[i][2]).toString();
                }
                String sk_ps = (String) data[i][3];
                String lineSt = (String) data[i][5];
                Integer texStop = (Integer) data[i][6];
                DistrPoint dp = getDPbyID(st);
                if (tOn.length() == 0) {
                    tOn = null;
                }
                if (tOff.length() == 0) {
                    tOff = null;
                }
                if ((tOn != null) || tOff != null) {
                    TrainStTableData std = new TrainStTableData();
                    std.tOn = tOn;
                    std.tOff = tOff;
                    std.sk_ps = sk_ps;
                    std.lineSt = lineSt;
                    std.texStop = texStop;
                    std.distrpoint = dp;
                    listStTableData.add(std);
                }
            }

            ActionTrainEditTime action = new ActionTrainEditTime(train);
            // ���������� � ������� �������� ����������� ��������
            History history = train.getHistory();
            history.addAction(action, TypeValue.oldValue);
            // ��������� ��������
            train.applyThreadByTable(listStTableData, town);
            // ���������� � ������� �������� ������ ��������
            history.addAction(action, TypeValue.newValue);

        }
        dataChanged = false;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        if (listDistrPoint == null) {
            return 0;
        }
        if (town) {
            return listDistrPoint.size();
        } else {
            int count = 0;
            for (DistrPoint dp : listDistrPoint) {
                if (dp.getTypePoint() == DistrPoint.TypePoint.SeparatePoint) {
                    count++;
                }
            }
            return count;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (data == null || row >= getRowCount() || col >= getColumnCount()) {
            return null;
        } else {
            return data[row][col];
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (data != null && row < getRowCount() && col < getColumnCount()) {
            data[row][col] = aValue;
            dataChanged = true;
            fireTableCellUpdated(row, col);
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return St.class;
        case 1:
        case 2:
            return TimeSt.class;
        case 6:
            return Integer.class;
        default:
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 0 || columnIndex == 4 || columnIndex == 7) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    public TrainThread getTrain() {
        return train;
    }

    public void setTrain(TrainThread train) {
        this.train = train;
        setData(false);
    }

    public List<DistrPoint> getListDistrPoint() {
        return listDistrPoint;
    }

    public void setListDistrPoint(List<DistrPoint> listDistrPoint) {
        this.listDistrPoint = listDistrPoint;
        setData(true);
    }

    public boolean isTown() {
        return town;
    }

    public void setTown(boolean town) {
        if (this.town != town) {
            setData(true);
            setData(false);
        }
        this.town = town;
    }

    public boolean isDataChanged() {
        return dataChanged;
    }

    public TimeMode getTimeMode() {
        return timeMode;
    }

    public void setTimeMode(TimeMode timeMode) {
        this.timeMode = timeMode;
    }

    public DistrPoint getDistrPoint(int row) {
        DistrPoint dp = null;
        St st = (St) getValueAt(row, 0);
        for (int i = 0; i < listDistrPoint.size(); i++) {
            DistrPoint dpp = listDistrPoint.get(i);
            if (st.id == dpp.getIdPoint()) {
                if (st.town && dpp.getTypePoint() == DistrPoint.TypePoint.StopPoint) {
                    dp = dpp;
                }
                if (!st.town && dpp.getTypePoint() == DistrPoint.TypePoint.SeparatePoint) {
                    dp = dpp;
                }
            }
        }
        return dp;
    }

    @Override
    public void tableChanged(TableModelEvent evt) {
        int col = evt.getColumn();
        int row = evt.getFirstRow();
        if (sCol != col || sRow != row) {
            sCol = col;
            sRow = row;
            int[] pos = getNextPosition(row, col);
            table.moveFocus(pos[0], pos[1]);
//            JTextField textField = ((TimeStCellEditor)table.getCellEditor(pos[0], pos[1])).getTextField();
        }
    }

    public int[] getNextPosition(int selectedRow, int selectedCol) {
        int nextRow = selectedRow;
        int nextCol = selectedCol;

        if (train != null && (selectedCol == 1 || selectedCol == 2)) {
            nextRow = selectedRow;
            if (OE.odd.equals(train.getOe()) && selectedRow < (this.getRowCount() - 1)) {
                nextRow = selectedRow + 1;
                System.out.println("~1");
            }
            if (OE.even.equals(train.getOe()) && selectedRow > 0) {
                nextRow = selectedRow - 1;
                System.out.println("~2");
            }
            if (selectedCol == 1 && (timeMode != TimeMode.Arrive)) {
                nextRow = selectedRow;
                nextCol = 2;
                System.out.println("~3 ");
            }
            if (selectedCol == 1 && (timeMode == TimeMode.Arrive)) {
                nextCol = 1;
                System.out.println("~4");
            }
            if (selectedCol == 2 && (timeMode != TimeMode.Leave)) {
                nextCol = 1;
                System.out.println("~5");
            }
            if (selectedCol == 2 && (timeMode == TimeMode.Leave)) {
                nextCol = 2;
                System.out.println("~6");
            }
        }
        System.out.println(" sr=" + selectedRow + " sc=" + selectedCol +
        " nr=" + nextRow + " nc=" + nextCol);
        int[] pos = { nextRow, nextCol };
        return pos;
    }

    public TrainScheduleTable getTable() {
        return table;
    }

    public void setTable(TrainScheduleTable table) {
        this.table = table;
    }

}
