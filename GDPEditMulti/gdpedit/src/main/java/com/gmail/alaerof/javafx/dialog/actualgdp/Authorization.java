package com.gmail.alaerof.javafx.dialog.actualgdp;

import com.gmail.alaerof.dao.dto.gdp.User;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IUsersDAO;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.manager.LocaleManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ResourceBundle;

import static com.gmail.alaerof.application.GDPEdit.authorUserName;
import static javax.swing.JOptionPane.QUESTION_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;

public class Authorization {
    private static Logger logger = LogManager.getLogger(Authorization.class);

    private static final int[] CRPT = {4, 3, 5, 2, 1, 4, 2, 4, 3, 1, 5, 2, 4, 3, 1};

    public static boolean check() {
        ResourceBundle bundle = ResourceBundle.getBundle("bundles.ActualFXDialog", LocaleManager.getLocale());
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        JPanel label = new JPanel(new GridLayout(0, 1, 2, 2));
        label.add(new JLabel(bundle.getString("authorization.login"), SwingConstants.RIGHT));
        label.add(new JLabel(bundle.getString("authorization.password"), SwingConstants.RIGHT));
        panel.add(label, BorderLayout.WEST);

        JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
        JTextField aUserLogin = new JTextField();
        controls.add(aUserLogin);
        JPasswordField aUserPass = new JPasswordField();
        controls.add(aUserPass);
        panel.add(controls, BorderLayout.CENTER);

        showMessageDialog(null, panel, bundle.getString("authorization.title"), QUESTION_MESSAGE);
        if ((aUserLogin.getText().length() != 0) && (aUserPass.getText().length() != 0)) {
            String userLogin = encriptStr(aUserLogin.getText());
            IUsersDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getUsersDAO();
            User user = null;
            try {
                user = dao.getUser(userLogin);
            } catch (PoolManagerException e) {
                logger.error(e.toString(), e);
            }
            //graf 111
            if (user.getUserLogin() == null) return false;
            String userPass = encriptStr(aUserPass.getText());
            if (userPass.equals(user.getUserPass())) {
                authorUserName = descriptStr(user.getName());
                return true;
            }
        }
        return false;
    }

    private static String encriptStr(String str) {
        String result = "";
        str.length();
        for (int i = 0; i < str.length(); i++)
            result = result + "." + String.valueOf((byte) str.charAt(i) + CRPT[i]);
        return result.substring(1);
    }

    private static String descriptStr(String str) {
        String result = "";
        String ss = "";
        int l = str.length();
        int p, i = 0;

        while (l > 0) {
            p = str.indexOf(".");
            if (p > 0) {
                ss = str.substring(0, p);
                str = str.substring(p + 1);
            } else {
                ss = str;
                str = "";
            }
            result = result + (char) (Integer.valueOf(ss) - CRPT[i] + 848); // "848" is code for decode in Windows-1251
            i++;
            l = str.length();
        }
        return result;
    }
}
