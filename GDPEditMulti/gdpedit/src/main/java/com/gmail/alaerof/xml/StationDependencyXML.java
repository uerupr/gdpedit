package com.gmail.alaerof.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.VariantCalcTime;
import com.gmail.alaerof.dialog.timeimport.stations.StationHolder;
import com.gmail.alaerof.util.DataXMLUtil;

public class StationDependencyXML {
    private static Logger logger = LogManager.getLogger(StationDependencyXML.class);
    private static final String savePath = "import";

    /**
     * ��������� ��������� ������������ ������� �� ������� �������� �������� �� �� �������
     * ��� ����������� �.�.�������
     * @param distrID �� �������
     * @param distrName �������� �������
     * @param list ������ ������������
     */
    public static void saveStationHolderList(int distrID, String distrName, List<StationHolder> list, int oe) {
        try {
            String fileName = savePath + File.separator + "distr" + distrID + "_" + oe + ".xml";
            Document doc = DataXMLUtil.getNewDocument();
            Element root = doc.createElement("importdependency");
            doc.appendChild(root);

            for (StationHolder sth : list) {
                if (sth.distrSt != null || sth.spanSt != null) {
                    Element st = doc.createElement("stationholder");
                    Element var = doc.createElement("variantcalctime");
                    st.appendChild(var);
                    var.setAttribute("esr", sth.varSt.getCodeESR());
                    var.setAttribute("name", sth.varSt.getName());
                    if (sth.distrSt != null) {
                        Element std = doc.createElement("station");
                        std.setAttribute("id", "" + sth.distrSt.getIdSt());
                        std.setAttribute("esr", "" + sth.distrSt.getCodeESR());
                        std.setAttribute("esr2", "" + sth.distrSt.getCodeESR2());
                        std.setAttribute("express", "" + sth.distrSt.getCodeExpress());
                        std.setAttribute("name", "" + sth.distrSt.getName());
                        st.appendChild(std);
                    }
                    if (sth.spanSt != null) {
                        Element std = doc.createElement("spanst");
                        std.setAttribute("id", "" + sth.spanSt.getIdSpanst());
                        std.setAttribute("esr", "" + sth.spanSt.getCodeESR());
                        std.setAttribute("esr2", "" + sth.spanSt.getCodeESR2());
                        std.setAttribute("express", "" + sth.spanSt.getCodeExpress());
                        std.setAttribute("name", "" + sth.spanSt.getName());
                        st.appendChild(std);
                    }
                    root.appendChild(st);
                }
            }
            DataXMLUtil.saveDocument(new File(fileName), doc);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }

    /**
     * ���������� ������ ������������ (������ �������� ����) �� ����� ������������ ����� ��� �������
     * ���� ��� �����, ���������� ������ ������
     * @param distrID ��� �������
     * @param distrName �������� �������
     * @return ������ ������������
     */
    public static List<StationHolder> loadStationHolderList(int distrID, String distrName, int oe) {
        List<StationHolder> list = new ArrayList<>();
        try {
            String fileName = savePath + File.separator + "distr" + distrID + "_" + oe + ".xml";
            Document doc = DataXMLUtil.loadDocument(new File(fileName));
            NodeList nodeList = doc.getElementsByTagName("stationholder");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element elsth = (Element) nodeList.item(i);
                StationHolder sth = new StationHolder();
                list.add(sth);
                NodeList chlds = elsth.getElementsByTagName("variantcalctime");
                if (chlds.getLength() > 0) {
                    Element el = (Element) chlds.item(0);
                    VariantCalcTime var = new VariantCalcTime();
                    sth.varSt = var;
                    String codeESR = el.getAttribute("esr");
                    String name = el.getAttribute("name");
                    var.setCodeESR(codeESR);
                    var.setName(name);
                }
                chlds = elsth.getElementsByTagName("station");
                if (chlds.getLength() > 0) {
                    Element el = (Element) chlds.item(0);
                    Station st = new Station();
                    sth.distrSt = st;
                    int id = Integer.parseInt(el.getAttribute("id"));
                    String codeESR = el.getAttribute("esr");
                    String codeESR2 = el.getAttribute("esr2");
                    String codeExpress = el.getAttribute("express");
                    String name = el.getAttribute("name");
                    st.setIdSt(id);
                    st.setCodeESR(codeESR);
                    st.setCodeESR2(codeESR2);
                    st.setCodeExpress(codeExpress);
                    st.setName(name);
                }
                chlds = elsth.getElementsByTagName("spanst");
                if (chlds.getLength() > 0) {
                    Element el = (Element) chlds.item(0);
                    SpanSt st = new SpanSt();
                    sth.spanSt = st;
                    int id = Integer.parseInt(el.getAttribute("id"));
                    String codeESR = el.getAttribute("esr");
                    String codeESR2 = el.getAttribute("esr2");
                    String codeExpress = el.getAttribute("express");
                    String name = el.getAttribute("name");
                    st.setIdSpanst(id);
                    st.setCodeESR(codeESR);
                    st.setCodeESR2(codeESR2);
                    st.setCodeExpress(codeExpress);
                    st.setName(name);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return list;
    }

}
