package com.gmail.alaerof.history;

import com.gmail.alaerof.manager.LocaleManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.entity.train.TrainXMLHandler;

import java.util.ResourceBundle;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ActionTrainDelete extends Action {
    private TrainThread train;
    private DistrEditEntity distrEditEntity;
    private static ResourceBundle bundle;

    public ActionTrainDelete(TrainThread train, DistrEditEntity distrEditEntity) {
        actionName = "DeleteTrain";
        this.train = train;
        this.distrEditEntity = distrEditEntity;
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.history", LocaleManager.getLocale());
    }


    @Override
    protected void buildElement(Document doc, Element action) {
        Element tr = TrainXMLHandler.saveToXML(train, doc);
        action.appendChild(tr);
    }

    @Override
    protected boolean executeAction(int typeAction, Element oldValue, Element newValue) {
        // ��� newValue ������ ���� ������ Null
        // ������ �������� �������� ����������� ���������� ������ � ������ �������
        if (typeAction == sRestore && oldValue != null) {
            distrEditEntity.addTrain(train);
            GDPEdit.gdpEdit.getGDPActionPanel().getActionListTrain().updateListTrain();
            actionState = sRestore;
            return true;
        }

        // ���������� �������� �������� �������� ������ �� ������
        if (typeAction == sApply) {
            distrEditEntity.removeTrain(train);
            GDPEdit.gdpEdit.getGDPActionPanel().getActionListTrain().updateListTrain();
            actionState = sApply;
            return true;
        }

        return false;
    }

    @Override
    public String getActionDescription() {
        return "" + train.getCode() + "(" + train.getNameTR() + ") " + bundle.getString("history.deletion");
    }

}
