package com.gmail.alaerof.javafx.dialog.energyfromtr.view;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.dto.energy.Energy;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.energy.SpanEnergyTract;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IScaleDAO;
import com.gmail.alaerof.dao.interfacedao.ISpanEnergyDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrSpanSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.energyfromtr.model.ImportTRModel;
import com.gmail.alaerof.javafx.dialog.energyfromtr.model.SpanStExModel;
import com.gmail.alaerof.javafx.dialog.energyfromtr.model.StationExModel;
import com.gmail.alaerof.util.FileUtil;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import java.util.ResourceBundle;


import com.gmail.alaerof.javafx.dialog.energyfromtr.ImportEnergyFromTRXML;//.loadEnergyTRList;

public class ImportEnergyFromTRLayoutController implements Initializable {
    protected Logger logger = LogManager.getLogger(ImportEnergyFromTRLayoutController.class);
    protected ResourceBundle bundle;
    private DistrEditEntity distrEditEntity;
    private Scene scene;

    /** ������� ��������� ������ */
    private TableView<ImportTRModel> importTRTable;

    /** ������� ������� */
    private TableView<StationExModel> stationExTable;

    /** ������� �.�. */
    private TableView<SpanStExModel> spanStExTable;

    private List<ImportTRModel> importTRList = new ArrayList<>();
    private List<StationExModel> stationExList = new ArrayList<>();
    private List<SpanStExModel> spanStExList = new ArrayList<>();

    @FXML
    private BorderPane rootLeftPane;
    @FXML
    private BorderPane rootRightTopPane;
    @FXML
    private BorderPane rootRightBottomPane;
    @FXML
    private Pane rootCenterPane;
    @FXML
    private ComboBox<LocomotiveType> locomotiveTypeComboBox;

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        rootLeftPane.autosize();
        rootRightTopPane.autosize();
        rootRightBottomPane.autosize();
        rootCenterPane.autosize();
        // import table
        EditableTableViewCreator<ImportTRModel> builderImp = new EditableTableViewCreator<>();
        importTRTable = builderImp.createEditableTableView();
        importTRTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
        rootLeftPane.setCenter(importTRTable);
        rootLeftPane.getCenter().autosize();
        String title;
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.connect");
        importTRTable.getColumns().add(builderImp.createStringColumn(title, ImportTRModel::connectProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.nameSt");
        importTRTable.getColumns().add(builderImp.createStringColumn(title, ImportTRModel::nameStProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.codeESR");
        importTRTable.getColumns().add(builderImp.createStringColumn(title, ImportTRModel::codeESRProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.newnameSt");
        importTRTable.getColumns().add(builderImp.createStringColumn(title, ImportTRModel::newNameStProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.newcodeESR");
        importTRTable.getColumns().add(builderImp.createStringColumn(title, ImportTRModel::newCodeESRProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.codeESR2");
        importTRTable.getColumns().add(builderImp.createStringColumn(title, ImportTRModel::codeESR2Property, false));
        for (int i = 0; i < importTRTable.getColumns().size(); i++){
            importTRTable.getColumns().get(i).setSortable(false);
        }

        // station table
        EditableTableViewCreator<StationExModel> builderSt = new EditableTableViewCreator<>();
        stationExTable = builderSt.createEditableTableView();
        stationExTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
        //stationExTable.setMaxHeight(250);
        rootRightTopPane.setCenter(stationExTable);
        rootRightTopPane.getCenter().autosize();
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.nameSt");
        stationExTable.getColumns().add(builderSt.createStringColumn(title, StationExModel::nameStProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.codeESR");
        stationExTable.getColumns().add(builderSt.createStringColumn(title, StationExModel::codeESRProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.codeESR2");
        stationExTable.getColumns().add(builderSt.createStringColumn(title, StationExModel::codeESR2Property, false));
        for (int i = 0; i < stationExTable.getColumns().size(); i++){
            stationExTable.getColumns().get(i).setSortable(false);
        }

        // spanst table
        EditableTableViewCreator<SpanStExModel> builderSpSt = new EditableTableViewCreator<>();
        spanStExTable = builderSpSt.createEditableTableView();
        spanStExTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
        //spanStExTable.setMaxHeight(250);
        rootRightBottomPane.setCenter(spanStExTable);
        rootRightBottomPane.getCenter().autosize();
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.nameSt");
        spanStExTable.getColumns().add(builderSpSt.createStringColumn(title, SpanStExModel::nameStProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.codeESR");
        spanStExTable.getColumns().add(builderSpSt.createStringColumn(title, SpanStExModel::codeESRProperty, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.station.codeESR2");
        spanStExTable.getColumns().add(builderSpSt.createStringColumn(title, SpanStExModel::codeESR2Property, false));
        title = bundle.getString("javafx.dialog.importenergyfromtr.span");
        spanStExTable.getColumns().add(builderSpSt.createStringColumn(title, SpanStExModel::spanNameProperty, false));
        for (int i = 0; i < spanStExTable.getColumns().size(); i++){
            spanStExTable.getColumns().get(i).setSortable(false);
        }

        // ��� ����� ���������� �� ��� �����.
        Callback<ListView<LocomotiveType>, ListCell<LocomotiveType>> factory = lv -> new ListCell<LocomotiveType>() {
            @Override
            protected void updateItem(LocomotiveType item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getLtype());
            }
        };
        locomotiveTypeComboBox.setCellFactory(factory);
    }

    @FXML
    private void openXML (ActionEvent event) {
        logger.debug("openXML");
        String fileName = ".xml";
        File file = FileUtil.chooseFile(FileUtil.Mode.open,null, fileName, null,
                new FileChooser.ExtensionFilter("XML files", "*.xml"));
        // ����� ���� null, ��������, ���� ��� ������ ����� ������ cancel
        if (file != null) {
            importTRList = ImportEnergyFromTRXML.loadEnergyTRList(file, distrEditEntity);
            importTRTable.setItems(FXCollections.observableArrayList(importTRList));
        }
    }

    @FXML
    private void delRow (ActionEvent event) {
        logger.debug("delStation (delRow)");
        int rowImport = importTRTable.getSelectionModel().getSelectedIndex();
        if (rowImport >= 0){
            String stName = importTRList.get(rowImport).newNameStProperty().get();
            if (stName.equals("")){
                stName = importTRList.get(rowImport).nameStProperty().get();
            }
            String titleMain = bundle.getString("javafx.dialog.importenergyfromtr.attention");
            String titleMiddle = bundle.getString("javafx.dialog.importenergyfromtr.del") + "?";
            String titleSmall = stName;
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            if (alert.showAndWait().get() == ButtonType.OK){
                if (rowImport < importTRTable.getItems().size()) {
                    importTRList.get(rowImport + 1).setT1Oil(importTRList.get(rowImport + 1).t1OilProperty().get() + importTRList.get(rowImport).t1OilProperty().get());
                    importTRList.get(rowImport + 1).setT2Oil(importTRList.get(rowImport + 1).t2OilProperty().get() + importTRList.get(rowImport).t2OilProperty().get());
                    importTRList.get(rowImport + 1).setTUpOil(importTRList.get(rowImport + 1).tUpOilProperty().get());
                }
                importTRList.remove(rowImport);
                importTRTable.setItems(FXCollections.observableArrayList(importTRList));
            }
        }
    }

    @FXML
    private void saveToDB (ActionEvent event) {
        logger.debug("saveToDB");
        boolean start = true;
        if (importTRList.size() == 0){
            start = false;
        }
        for (int i = 0; i < importTRList.size(); i++){
            String ss = importTRList.get(i).connectProperty().get();
            if (!ss.equals("Ok")){
                start = false;
            }
        }
        if (start) {
            String spanError = "";
            try {
                LocomotiveType locomotiveType = locomotiveTypeComboBox.getSelectionModel().getSelectedItem();
                int lineFirst = 0;
                int lineSecond = 1;
                for (int i = 1; i < importTRList.size(); i++){
                    if (importTRList.get(i).stationOrSpanStProperty().get() == 1){
                        SpanEnergyTract spanEnergyTract = findSpan(lineFirst, lineSecond, locomotiveType);
                        if (spanEnergyTract.getIdScale() > 0) {
                            ISpanEnergyDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getSpanEnergyDAO();
                            dao.saveSpanEnergyTract(spanEnergyTract);
                            logger.debug("Save to DB: " + lineFirst + " - " + lineSecond + " " +spanEnergyTract.toString());
                        } else {
                            String stName1 = importTRList.get(lineFirst).newNameStProperty().get();
                            if (stName1.equals("")){
                                stName1 = importTRList.get(lineFirst).nameStProperty().get();
                            }
                            String stName2 = importTRList.get(lineSecond).newNameStProperty().get();
                            if (stName2.equals("")){
                                stName2 = importTRList.get(lineSecond).nameStProperty().get();
                            }
                            spanError = spanError + stName1 + " - " + stName2 + ";";
                        }
                        lineFirst = i;
                        if (i < importTRList.size()){
                            lineSecond = i+1;
                        }
                    }
                    if (importTRList.get(i).stationOrSpanStProperty().get() != 1){
                        lineSecond = i + 1;
                    }
                }
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
            Alert alert;
            if (spanError.equals("")){
                String titleMain = bundle.getString("javafx.dialog.importenergyfromtr.attention");
                String titleMiddle = bundle.getString("javafx.dialog.importenergyfromtr.station.verification");
                String titleSmall = bundle.getString("javafx.dialog.importenergyfromtr.import.good");
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(titleMain);
                alert.setHeaderText(titleMiddle);
                alert.setContentText(titleSmall);
            }
            else {
                String titleMain = bundle.getString("javafx.dialog.importenergyfromtr.attention");
                String titleMiddle = bundle.getString("javafx.dialog.importenergyfromtr.span.verification");
                String titleSmall = spanError;
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(titleMain);
                alert.setHeaderText(titleMiddle);
                alert.setContentText(titleSmall);
            }
            alert.showAndWait();
        }
        else {
            String titleMain = bundle.getString("javafx.dialog.importenergyfromtr.attention");
            String titleMiddle = bundle.getString("javafx.dialog.importenergyfromtr.station.verification.err");
            String titleSmall = bundle.getString("javafx.dialog.importenergyfromtr.import.bad");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            alert.showAndWait();
        }
    }

    private SpanEnergyTract findSpan(int lineFirst, int lineSecond, LocomotiveType locomotiveType) throws DataManageException, PoolManagerException {
        SpanEnergyTract spanEnergyTract = new SpanEnergyTract();
        spanEnergyTract.setLocomotiveType(locomotiveType);
        IScaleDAO sd = GDPDAOFactoryCreater.getGDPDAOFactory().getScaleDAO();
        int idDistr = distrEditEntity.getIdDistr();
        List<Scale> listSc = sd.getListScale(idDistr);
        int idStationFirst = importTRList.get(lineFirst).idStationProperty().get();
        int idStationSecond = importTRList.get(lineSecond).idStationProperty().get();
        Energy energy = calcEnergy(lineFirst, lineSecond, locomotiveType);
        spanEnergyTract.setEnergy(energy);
        for (int i = 0; i < listSc.size(); i++){
            Scale scale = listSc.get(i);
            if ((scale.getIdSt() == idStationFirst) && (scale.getIdStE() == idStationSecond)){
                // �������� �������
                spanEnergyTract.setIdScale(scale.getIdScale());
                spanEnergyTract.setOe(OE.odd);
            }
            if ((listSc.get(i).getIdStE() == idStationFirst) && (listSc.get(i).getIdSt() == idStationSecond)){
                // ������ �������
                spanEnergyTract.setIdScale(scale.getIdScale());
                spanEnergyTract.setOe(OE.even);
            }
        }
        return spanEnergyTract;
    }

    private Energy calcEnergy(int lineFirst, int lineSecond, LocomotiveType locomotiveType) {
        Energy energy = new Energy();
        double enMove = 0;
        double enUp = 0;
        double enDown = 0;
        if ((locomotiveType.getIdLType() == 1) || (locomotiveType.getIdLType() == 2)){
            for (int i = lineFirst + 1; i <= lineSecond; i++){
                enMove = enMove + importTRList.get(i).t1OilProperty().get();
                enUp = enUp + importTRList.get(i).tUpOilProperty().get();
                enDown = enDown + importTRList.get(i).tDownOilProperty().get();
            }
        }
        if (locomotiveType.getIdLType() == 3){
            for (int i = lineFirst; i <= lineSecond; i++){
                enMove = enMove + importTRList.get(i).t2OilProperty().get();
            }
        }
        energy.setEnMove((float) enMove);
        energy.setEnUp((float) enUp);
        energy.setEnDown((float) enDown);
        return energy;
    }

    @FXML
    private void stationToLeft (ActionEvent event) {
        logger.debug("stationToLeft");
        int rowStation = stationExTable.getSelectionModel().getSelectedIndex();
        int rowImport = importTRTable.getSelectionModel().getSelectedIndex();
        if ((rowImport >= 0) && (rowStation >= 0)){
            importTRList.get(rowImport).setNewParamStation(stationExList.get(rowStation).nameStProperty(),
                                                           stationExList.get(rowStation).codeESRProperty(),
                                                           stationExList.get(rowStation).codeESR2Property(),
                                                           stationExList.get(rowStation).idStationProperty());
            importTRTable.refresh();
            logger.debug(rowImport + " < " + rowStation);
        }
    }

    @FXML
    private void spanStToLeft (ActionEvent event) {
        logger.debug("spanStToLeft");
        int rowSpanSt = spanStExTable.getSelectionModel().getSelectedIndex();
        int rowImport = importTRTable.getSelectionModel().getSelectedIndex();
        if ((rowImport >= 0) && (rowSpanSt >= 0)) {
            importTRList.get(rowImport).setNewParamSpanSt(spanStExList.get(rowSpanSt).nameStProperty(),
                                                          spanStExList.get(rowSpanSt).codeESRProperty(),
                                                          spanStExList.get(rowSpanSt).codeESR2Property(),
                                                          spanStExList.get(rowSpanSt).idSpanStProperty());
            importTRTable.refresh();
            logger.debug(rowImport + " < " + rowSpanSt);
        }
    }

    public void setContent(DistrEditEntity distrEditEntity) {
        scene.setCursor(Cursor.WAIT);
        try {
            this.distrEditEntity = distrEditEntity;
            logger.debug("updateModelWithContent");

            // LocomotiveType
            List<LocomotiveType> locomotiveTypeList = distrEditEntity.getDistrEntity().getListDistrLoc();
            locomotiveTypeComboBox.setItems(FXCollections.observableArrayList(locomotiveTypeList));
            // �� ��������� ������ ���� ������ "��������"
            for (LocomotiveType type : locomotiveTypeList) {
                if (type.getIdLType() == 1) {
                    locomotiveTypeComboBox.getSelectionModel().select(type);
                    break;
                }
            }
            // ImportTrModel List
            importTRList.clear();
            importTRTable.setItems(FXCollections.observableArrayList(importTRList));
            // StationExModel List
            stationExList = buildStationModelListFromDistrEditEntity(distrEditEntity);
            stationExTable.setItems(FXCollections.observableArrayList(stationExList));

            // SpanStExModel List
            spanStExList = buildSpanStExModelListFromDistrEditEntity(distrEditEntity);
            spanStExTable.setItems(FXCollections.observableArrayList(spanStExList));
        } catch (DataManageException e) {
            e.printStackTrace();
        } catch (ObjectNotFoundException e) {
            e.printStackTrace();
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    private List<StationExModel> buildStationModelListFromDistrEditEntity(DistrEditEntity distrEditEntity) {
        List<StationExModel> itemList = new ArrayList<>();
        List<DistrStation> stationExList = new ArrayList<>();
        int pointCount = distrEditEntity.getDistrEntity().getListDistrPoint().size();
        for (int i = 0; i < pointCount; i++){
            if (distrEditEntity.getDistrEntity().getListDistrPoint().get(i).getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)){
                DistrStation ds = (DistrStation) distrEditEntity.getDistrEntity().getListDistrPoint().get(i);
                stationExList.add(ds);
            }
        }
        //List<DistrStation> list = distrEditEntity.getDistrEntity().getListSt();
        //Collections.sort(list, new TrainComparatorByCode());
        for (DistrStation distrStation : stationExList) {
            StationExModel stationModel = new StationExModel(distrStation);
            itemList.add(stationModel);
        }

        return itemList;
    }

    private List<SpanStExModel> buildSpanStExModelListFromDistrEditEntity(DistrEditEntity distrEditEntity) throws DataManageException, ObjectNotFoundException {
        List<SpanStExModel> itemList = new ArrayList<>();
        List<DistrSpanSt> spanStExlist = new ArrayList<>();
        int pointCount = distrEditEntity.getDistrEntity().getListDistrPoint().size();
        for (int i = 0; i < pointCount; i++){
            if (distrEditEntity.getDistrEntity().getListDistrPoint().get(i).getTypePoint().equals(DistrPoint.TypePoint.StopPoint)){
                DistrSpanSt dss = (DistrSpanSt) distrEditEntity.getDistrEntity().getListDistrPoint().get(i);
                spanStExlist.add(dss);
            }
        }
        //List<DistrStation> list = distrEditEntity.getDistrEntity().getListSt();
        //Collections.sort(list, new TrainComparatorByCode());
        for (DistrSpanSt distrSpanSt : spanStExlist) {
            SpanStExModel spanStExModel = new SpanStExModel(distrSpanSt, distrEditEntity);
            itemList.add(spanStExModel);
        }

        return itemList;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
