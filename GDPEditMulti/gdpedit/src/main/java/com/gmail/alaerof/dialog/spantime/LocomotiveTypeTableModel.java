package com.gmail.alaerof.dialog.spantime;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class LocomotiveTypeTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("spantime.typeofmove") };
    private List<LocomotiveType> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spantime", LocaleManager.getLocale());
    }


    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else
            return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            LocomotiveType lt = list.get(row);
            switch (col) {
            case 0:
                return lt.getLtype().trim();
            default:
                break;
            }
        }
        return null;
    }

    public List<LocomotiveType> getList() {
        return list;
    }

    public void setList(List<LocomotiveType> list) {
        this.list = list;
    }

}
