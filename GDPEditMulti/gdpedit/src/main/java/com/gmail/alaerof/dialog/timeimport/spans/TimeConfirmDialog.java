package com.gmail.alaerof.dialog.timeimport.spans;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.IconImageUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.dto.VariantCalc;
import com.gmail.alaerof.dialog.timeimport.stations.StationHolder;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.util.TableUtils;
import javax.swing.JLabel;
import javax.swing.JCheckBox;

/**
 * ������������� ������������� ������ ���� �� ���������
 * @author Helen Yrofeeva
 */
public class TimeConfirmDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(TimeConfirmDialog.class);
    private SpanTimeTableModel tableModel = new SpanTimeTableModel();
    private JTable table = new JTable(tableModel);
    private List<StationHolder> listStHolder;
    private List<StationHolder> listSpanStHolder;
    private DistrEntity distr;
    private LocomotiveType loc;
    private int oe;
    private VariantCalc varCalc;
    private JLabel lblAddTime;
    private JCheckBox chbUpDownFromAdd;
    private JCheckBox isSaveSpanStTime;
    private static ResourceBundle bundle;

    public TimeConfirmDialog() {
        initContent();
    }

    public TimeConfirmDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.spans", LocaleManager.getLocale());
    }

    private void initContent() {
        try {
            this.setIconImage(IconImageUtil.getImage(IconImageUtil.ISPT64));
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        setBounds(0, 0, 850, 600);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());

        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSizeO[] = { 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40 };
        TableUtils.setTableColumnSize(table, colSizeO);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(table);
        getContentPane().add(scroll, BorderLayout.CENTER);

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                isSaveSpanStTime = new JCheckBox(bundle.getString("spans.savetimes"));
                buttonPane.add(isSaveSpanStTime);
            }
            {
                lblAddTime = new JLabel(bundle.getString("spans.line"));
                buttonPane.add(lblAddTime);
            }
            {
                chbUpDownFromAdd = new JCheckBox(bundle.getString("spans.additionracingandslowdown"));
                chbUpDownFromAdd.setToolTipText(bundle.getString("spans.setracingandslowdown"));
                // buttonPane.add(chbUpDownFromAdd);
            }
            {
                JButton okButton = new JButton(bundle.getString("spans.ok"));
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                // ��������� ��������� � ��������� ��
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            // saveAllSpanTime(chbUpDownFromAdd.isSelected());
                            saveAllSpanTime(true);
                            if (isSaveSpanStTime.isSelected()) {
                                // ��� ��� �������� ����� ���� �� �.�. � (����� �� !!!) �������� ��� ���������� � ��
                                serachAndSaveSpanStTime();
                            }
                            table.repaint();
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });

            }
            {
                JButton cancelButton = new JButton(bundle.getString("spans.cancel"));
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        TimeConfirmDialog.this.setVisible(false);
                    }
                });
            }
        }
    }

    /**
     * ��������� ��������� � ��������� ��
     * @param isUpDownFromAdd ����� ������� � ���������� �� ���. �������
     */
    protected void saveAllSpanTime(boolean isUpDownFromAdd) {
        List<SpanTimeHolder> list = tableModel.getList();
        for (SpanTimeHolder spth : list) {
            if (spth.getTime() != null) {
                // ���� ���. ����� ����, �� ����� �������� ��������� �������� � Time �� ���.
                if (varCalc.hasAdd()) {
                    spth.updateTimeToAdd(isUpDownFromAdd);
                }
                // � ��������� ��� � ��
                spth.getSpan().setSpanTime(spth.getTime(), loc, spth.getOe());
                spth.getSpan().saveSpanTime();
            }
        }
    }

    /**
     * ����� � ��������� ������ ��������� � �����. �� ������� �������
     * @param distr
     * @param listStHolder
     */
    public void fillListSpans(DistrEntity distr, List<StationHolder> listStHolder, LocomotiveType loc, int oe, VariantCalc varCalc) {
        this.distr = distr;
        this.loc = loc;
        this.oe = oe;
        this.varCalc = varCalc;
        // ������� ���������� ������� � �������������
        modifyListStHolderSum(listStHolder);
        // ��� ���������� ���� �������� � ��������� ���������� ����� ����
        searchSpans();
        lblAddTime.setText(bundle.getString("spans.line"));
        if (varCalc.hasAdd()) {
            lblAddTime.setText(bundle.getString("spans.haveextratime"));
        }
    }

    private int getIndexInListSpanStHolder(StationHolder st) {
        for (int i = 0; i < listSpanStHolder.size(); i++) {
            if (listSpanStHolder.get(i) == st) {
                return i;
            }
        }
        return -1;
    }

    /** ��� ��� �������� ����� ���� �� �.�. � (����� �� !!!) �������� ��� ���������� � �� */
    private void serachAndSaveSpanStTime() {
        for (int i = 0; i < listStHolder.size() - 1; i++) {
            StationHolder stB = listStHolder.get(i);
            StationHolder stE = listStHolder.get(i + 1);
            DistrSpan sp = distr.getDistrSpan(stB.distrSt.getIdSt(), stE.distrSt.getIdSt());
            if (sp != null) {
                // ������� ������, ������ ����� ��������� ��� ����� �.�.
                int indexStB = getIndexInListSpanStHolder(stB);
                int indexStE = getIndexInListSpanStHolder(stE);
                if (varCalc.getOe() == VariantCalc.EVEN) {
                    int num = 0;
                    for (int j = indexStB; j < indexStE; j++) {
                        StationHolder st = listSpanStHolder.get(j);
                        int locID = loc.getIdLType();
                        int scaleID = sp.getSpanScale().getIdScale();
                        if (j == indexStB) {
                            num = 0;
                        } else {
                            num = st.spanSt.getNum();
                        }
                        double tm = st.varSt.getTownT1();
                        sp.setSpanStTime(tm, num, scaleID, locID, OE.even);
                    }
                } else {
                    int num = 0;
                    for (int j = indexStB + 1; j <= indexStE; j++) {
                        StationHolder st = listSpanStHolder.get(j);
                        int locID = loc.getIdLType();
                        int scaleID = sp.getSpanScale().getIdScale();
                        if (j == indexStB + 1) {
                            num = 0;
                        } else {
                            if (j < indexStE) {
                                num = st.spanSt.getNum();
                            } else {
                                num = num + 1;
                            }
                        }
                        double tm = st.varSt.getTownT1();
                        sp.setSpanStTime(tm, num, scaleID, locID, OE.odd);
                    }
                }
                // ��������� � �� !!!
                sp.saveSpanStTime();
            }
        }

    }

    /** ������� ���������� ������� � ������������� */
    protected void modifyListStHolderSum(List<StationHolder> listStHolder) {
        // ������� ����� ����� ������ ���������� �� ���������� ����
        // ����� ������� ����� ���� ������������ (� ���. ����� ����)
        // p.s. ������� � ���������� �� �������, �.�. ��� ��������� �� ����� ������

        // ������� ��� ��������� ��� �.�. � ��������� ������ � ��������� ������
        double sumT = 0;
        double sumDT = 0;
        double sumL = 0;
        this.listSpanStHolder = new ArrayList<>();
        for (StationHolder sth : listStHolder) {
            // if (!sth.varSt.isTown() && sth.distrSt != null) {
            if (sth.distrSt != null || sth.spanSt != null) {
                // ���������� ���������
                sumL += sth.varSt.getL();
                sth.varSt.setTownL(sumL);
                // ���������� ����� ��������
                sumT += sth.varSt.getT1();
                sth.varSt.setTownT1(sumT);
                sth.varSt.setTownDt(sumDT);
                this.listSpanStHolder.add(sth);
                sumL = 0;
                sumT = 0;
                sumDT = 0;
            } else {
                sumL += sth.varSt.getL();
                sumT += sth.varSt.getT1();
                sumDT += sth.varSt.getDt();
            }
        }

        // ����� ��������� ��� ��� �.�. � ��������� � �������� ������
        sumT = 0;
        sumDT = 0;
        sumL = 0;
        this.listStHolder = new ArrayList<>();
        for (StationHolder sth : listStHolder) {
            // if (!sth.varSt.isTown() && sth.distrSt != null) {
            if (sth.distrSt != null) {
                // ���������� ���������
                sumL += sth.varSt.getL();
                sth.varSt.setL(sumL);
                // ���������� ����� ��������
                sumT += sth.varSt.getT1();
                sth.varSt.setT1(sumT);
                sth.varSt.setDt(sumDT);
                this.listStHolder.add(sth);
                sumL = 0;
                sumT = 0;
                sumDT = 0;
            } else {
                sumL += sth.varSt.getL();
                sumT += sth.varSt.getT1();
                sumDT += sth.varSt.getDt();
            }
        }
    }

    /** ��� ���������� ���� �������� � ��������� ���������� ����� ���� */
    private void searchSpans() {
        List<SpanTimeHolder> list = new ArrayList<>();
        // ��������� ������ ��������� �� �������
        for (DistrSpan sp : distr.getListSp()) {
            SpanTimeHolder spth = new SpanTimeHolder();
            spth.setSpan(sp);
            spth.setOe(oe);
            spth.setLocType(loc);
            list.add(spth);
        }
        tableModel.setList(list);
        for (int i = 0; i < listStHolder.size() - 1; i++) {
            StationHolder stB = listStHolder.get(i);
            StationHolder stE = listStHolder.get(i + 1);
            DistrSpan sp = distr.getDistrSpan(stB.distrSt.getIdSt(), stE.distrSt.getIdSt());
            if (sp != null) {
                // ������� ������, ������ ����� ��������� ��� ����� � �������� ��� ����� � SpanTimeHolder
                Time tm = new Time();
                tm.setTimeUp((float) stB.varSt.getTup());
                tm.setTimeDown((float) stE.varSt.getTdown());
                tm.setTimeMove((float) stE.varSt.getT1());
                // ���. ����� �� ���. �������
                Time tmAdd = new Time();
                tmAdd.setTimeUp((float) stB.varSt.getTupAdd());
                tmAdd.setTimeDown((float) stE.varSt.getTdownAdd());
                tmAdd.setTimeMove((float) stE.varSt.getDt());

                SpanTimeHolder spth = getSpanTimeHolder(sp.getSpanScale().getIdSpan());
                if (spth != null) {
                    spth.setTime(tm);
                    spth.setTimeAdd(tmAdd);
                }
            }
        }
        table.repaint();
    }

    /**
     * ���������� SpanTimeHolder �� ������ ������� �� �� ��������
     */
    private SpanTimeHolder getSpanTimeHolder(int idSpan) {
        List<SpanTimeHolder> list = tableModel.getList();
        for (SpanTimeHolder spth : list) {
            if (spth.getSpan().getSpanScale().getIdSpan() == idSpan) {
                return spth;
            }
        }
        return null;
    }

    public List<StationHolder> getListSpanStHolder() {
        return listSpanStHolder;
    }

    public void setListSpanStHolder(List<StationHolder> listSpanStHolder) {
        this.listSpanStHolder = listSpanStHolder;
    }
}
