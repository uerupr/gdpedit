package com.gmail.alaerof.javafx.dialog.spantime.model;

import com.gmail.alaerof.dao.dto.energy.Energy;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.Time;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SpanTimeModel implements Comparable<SpanTimeModel>{
    private int idSpan;
    private int idLtype;
    private Time spanTime;
    private Energy energy;
    private StringProperty locType; // SpanTime.type = locType.ltype
    private StringProperty oe;
    private DoubleProperty tmove;
    private DoubleProperty tdown;
    private DoubleProperty tup;
    private DoubleProperty tw;
    private DoubleProperty t1oil;
    private DoubleProperty tupoil;
    private DoubleProperty tdownoil;

    public SpanTimeModel(int idSpan, int idLtype, String locType, OE oe, Time spanTime, Energy energy) {
        this.idSpan = idSpan;
        this.idLtype = idLtype;
        this.spanTime = spanTime;
        this.energy = energy;
        this.locType = new SimpleStringProperty(locType);
        this.oe = new SimpleStringProperty(oe.getString());
        this.tmove = new SimpleDoubleProperty(spanTime.getTimeMove());
        this.tup = new SimpleDoubleProperty(spanTime.getTimeUp());
        this.tdown = new SimpleDoubleProperty(spanTime.getTimeDown());
        this.tw = new SimpleDoubleProperty(spanTime.getTimeTw());
        this.t1oil = new SimpleDoubleProperty(energy.getEnMove());
        this.tupoil = new SimpleDoubleProperty(energy.getEnUp());
        this.tdownoil = new SimpleDoubleProperty(energy.getEnDown());
    }

    public int getIdSpan() {
        return idSpan;
    }

    public void setIdSpan(int idSpan) {
        this.idSpan = idSpan;
    }

    public int getIdLtype() {
        return idLtype;
    }

    public void setIdLtype(int idLtype) {
        this.idLtype = idLtype;
    }

    public String getLocType() {
        return locType.get();
    }

    public StringProperty locTypeProperty() {
        return locType;
    }

    public void setLocType(String locType) {
        this.locType.set(locType);
    }

    public String getOe() {
        return oe.get();
    }

    public StringProperty oeProperty() {
        return oe;
    }

    public void setOe(String oe) {
        this.oe.set(oe);
    }

    public double getTmove() {
        return tmove.get();
    }

    public DoubleProperty tmoveProperty() {
        return tmove;
    }

    public void setTmove(double tmove) {
        this.tmove.set(tmove);
    }

    public double getTdown() {
        return tdown.get();
    }

    public DoubleProperty tdownProperty() {
        return tdown;
    }

    public void setTdown(double tdown) {
        this.tdown.set(tdown);
    }

    public double getTup() {
        return tup.get();
    }

    public DoubleProperty tupProperty() {
        return tup;
    }

    public void setTup(double tup) {
        this.tup.set(tup);
    }

    public double getTw() {
        return tw.get();
    }

    public DoubleProperty twProperty() {
        return tw;
    }

    public void setTw(double tw) {
        this.tw.set(tw);
    }

    public double getT1oil() { return t1oil.get(); }

    public DoubleProperty t1oilProperty() { return t1oil; }

    public void setT1oil(double t1oil) {
        this.t1oil.set(t1oil);
    }

    public double getTupoil() { return tupoil.get(); }

    public DoubleProperty tupoilProperty() { return tupoil; }

    public void setTupoil(double tupoil) {
        this.tupoil.set(tupoil);
    }

    public double getTdownoil() { return tdownoil.get(); }

    public DoubleProperty tdownoilProperty() { return tdownoil; }

    public void setTdownoil(double tdownoil) {
        this.tdownoil.set(tdownoil);
    }

    public Time getSpanTime() {
        return spanTime;
    }

    public Energy getEnergy() {
        return energy;
    }

    @Override
    public int compareTo(SpanTimeModel o) {
        int locType = idLtype - o.idLtype;
        if (locType == 0) {
            if (this.oe == o.oe) return 0;
            if (this.oe.equals(OE.odd)) return -1;
            return 1;
        }
        return locType;
    }
}
