package com.gmail.alaerof.dialog.linesttrain;

import java.util.List;
import java.util.ResourceBundle;
import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.entity.train.TrainStDouble;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * @author Helen Yrofeeva
 */
public class ListTrainCombStTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("linesttrain.numberarrival"),
            bundle.getString("linesttrain.name"),
            bundle.getString("linesttrain.numberdeparture"),
            bundle.getString("linesttrain.name") };
    private List<TrainStDouble> listTrainStDouble;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.linesttrain", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (listTrainStDouble == null) {
            return 0;
        } else {
            return listTrainStDouble.size();
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (listTrainStDouble != null) {
            TrainStDouble trStD = listTrainStDouble.get(row);
            if (trStD != null) {
                TrainThread trOn = trStD.getTrainThreadOn();
                TrainThread trOff = trStD.getTrainThreadOff();
                switch (col) {
                case 0:
                    return trOn.getCode();
                case 1:
                    return trOn.getNameTR();
                case 2:
                    return trOff.getCode();
                case 3:
                    return trOff.getNameTR();
                default:
                    return null;
                }
            }
        }
        return null;
    }

    /**
     * ���������� ������ ������ �� �������
     * @return ������ ������
     */
    public List<TrainStDouble> getListTrainStDouble() {
        return listTrainStDouble;
    }

    public void setListTrainStDouble(List<TrainStDouble> listTrainStDouble) {
        this.listTrainStDouble = listTrainStDouble;
    }

    /**
     * ���������� ������ TrainStDouble �� id ������� ��� Null
     * @param idTrainOn id ����� ��������
     * @param idTrainOff id ����� �����������
     * @return ���������� ������ TrainStDouble ��� Null
     */
    public TrainStDouble getTrainStDouble(long idTrainOn, long idTrainOff) {
        TrainStDouble trStD = null;
        int i = 0;
        while (trStD == null && i < listTrainStDouble.size()) {
            TrainStDouble st = listTrainStDouble.get(i);
            if (st.getTrainThreadOn().getIdTrain() == idTrainOn
                    && st.getTrainThreadOff().getIdTrain() == idTrainOff) {
                trStD = st;
            }
            i++;
        }
        return trStD;
    }

}
