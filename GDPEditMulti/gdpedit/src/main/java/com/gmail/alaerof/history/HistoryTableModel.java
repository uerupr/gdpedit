package com.gmail.alaerof.history;

import com.gmail.alaerof.manager.LocaleManager;

import javax.swing.table.AbstractTableModel;
import java.util.ResourceBundle;

/**
 * @author Helen Yrofeeva
 */
public class HistoryTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private History history;
    private static ResourceBundle bundle;
    private String[] columnNames = { bundle.getString("history.dash"),
            bundle.getString("history.number"),
            bundle.getString("history.act"),
            bundle.getString("history.time") };

    static {
        bundle = ResourceBundle.getBundle("bundles.history", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        if (history != null)
            return history.getActionsCount();
        return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        String s = "";
        if (row == history.getCurrentIndex())
            s = bundle.getString("history.sign");
        if (history != null) {
            Action action = history.getActions().get(row);
            switch (col) {
            case 0:
                return s;
            case 1:
                return String.valueOf(action.getActionIndex());
            case 2:
                return action.getActionDescription();
            case 3:
                return action.getActionTime();
            default:
                return null;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 1:
            return Integer.class;
        default:
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
        fireTableDataChanged();
    }
    
}
