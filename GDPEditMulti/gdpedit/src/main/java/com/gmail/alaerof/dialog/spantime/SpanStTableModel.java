package com.gmail.alaerof.dialog.spantime;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class SpanStTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("spantime.number"),
            bundle.getString("spantime.express"),
            bundle.getString("spantime.ecp"),
            bundle.getString("spantime.ecp2"),
            bundle.getString("spantime.name"),
            bundle.getString("spantime.km"),
            bundle.getString("spantime.stay") };
    private List<SpanSt> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spantime", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else
            return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if(list!=null){
            SpanSt st = list.get(row);
            switch (col) {
            case 0:
                return st.getNum();
            case 1:
                return st.getCodeExpress();
            case 2:
                return st.getCodeESR();
            case 3:
                return st.getCodeESR2();
            case 4:
                return st.getName();
            case 5:
                return st.getAbsKM();
            case 6:
                return st.getTstop();
            default:
                break;
            }
        }
        return null;
    }

    public List<SpanSt> getList() {
        return list;
    }

    public void setList(List<SpanSt> list) {
        this.list = list;
    }

}
