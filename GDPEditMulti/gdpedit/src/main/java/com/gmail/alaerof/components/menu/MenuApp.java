package com.gmail.alaerof.components.menu;

import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.sql.SelectDB;
import com.gmail.alaerof.application.GDPEdit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenuItem;

public class MenuApp extends LoggerMenu {
    public MenuApp() {
        super();
        this.setText(bundle.getString("components.menu.application"));
        {
            JMenuItem menuItem = new JMenuItem(bundle.getString("components.menu.application.selectawp"));
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    selectARMGDPDB();
                }
            });
            // this.add(menuItem);
            // ��� HSQLDB ���� ������� ������
            if (GDPDAOFactoryCreater.getGDPDAOFactory().isHSQLDB()) {
                menuItem.setEnabled(false);
            }
        }
        {
            JMenuItem menuItem = new JMenuItem(bundle.getString("components.menu.application.close"));
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    GDPEdit.gdpEdit.mainFrameCloseOperation();
                }
            });
            this.add(menuItem);
        }
    }

    private void selectARMGDPDB() {
        JFrame mainFrame = GDPEdit.gdpEdit.getMainFrame();
        try {
            if (SelectDB.selectDB(mainFrame)) {
                mainFrame.setVisible(false);
                GDPDAOFactoryCreater.initGDPDAOFactory();
                GDPEdit gdpEdit = new GDPEdit();
                GDPEdit.setGDPEdit(gdpEdit);
                mainFrame.setTitle(GDPEdit.getGDPEditTitle());
                mainFrame.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
