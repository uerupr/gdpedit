package com.gmail.alaerof.history;

import java.text.DateFormat;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
public abstract class Action {
    public enum TypeValue {
        oldValue, newValue
    }

    private static int count;
    private final int actionIndex = count++; // ����� �������� (ID)

    public static final int sAction = 1;
    public static final int sRestore = 2;
    public static final int sApply = 3;
    protected int actionState; // ���������: 1- Action; 2- Restore; 3- Apply

    protected String actionName;
    private String actionTime;

    private String getTime() {
        if (actionTime == null) {
            Date d = new Date();
            DateFormat df = DateFormat.getTimeInstance();
            actionTime = df.format(d);
        }
        return actionTime;
    }

    /**
     * ��������� ���������� �������� ��� ���������� � �������
     * @param doc ����� ��� �������� ����� ��������� ��� ����������
     * @param action ���� ������� ������ ���� �������� ���������� � ������
     */
    protected abstract void buildElement(Document doc, Element action);

    /**
     * ��������� � ��������� �������� ������� �������� ��������
     * @param doc ������� ��������
     * @param typeValue ��� ������������ �������� (oldValue, newValue)
     */
    public void saveAction(Document doc, TypeValue typeValue) {
        Element root = doc.getDocumentElement();
        Element action = doc.createElement("Action");
        action.setAttribute("ActionNum", String.valueOf(actionIndex));
        action.setAttribute("ActionName", actionName);
        action.setAttribute("ValueState", typeValue.toString());
        action.setAttribute("Time", getTime());
        // ���������� �������� ��������
        buildElement(doc, action);

        root.appendChild(action);
        actionState = sAction;
    }

    /**
     * ���������� �������� (��������/���������)
     * @param typeAction ��� ��������: ��������(restore)/���������(apply)
     * @param oldValue ������ �������� ����������� �� ������� (�� ����������)
     * @param newValue ����� �������� ����������� �� ������� (����� ����������)
     * @return
     */
    protected abstract boolean executeAction(int typeAction, Element oldValue, Element newValue);

    /**
     * �������� �������� �� �������
     * 
     * @param doc
     * @param typeAction
     * @return
     */
    private boolean loadAction(Document doc, int typeAction) {
        NodeList list = doc.getElementsByTagName("Action");
        int find = 0;
        int i = list.getLength() - 1;
        Element oldValue = null;
        Element newValue = null;
        while (i >= 0 && find < 2) {
            Element action = (Element) list.item(i);
            int index = Integer.parseInt(action.getAttribute("ActionNum"));
            if (index == actionIndex) {
                find++;
                String tv = action.getAttribute("ValueState");
                if (TypeValue.oldValue.equals(TypeValue.valueOf(tv))) {
                    oldValue = action;
                }
                if (TypeValue.newValue.equals(TypeValue.valueOf(tv))) {
                    newValue = action;
                }
            }
            i--;
        }
        if (oldValue != null || newValue != null) {
            return executeAction(typeAction, oldValue, newValue);
        } else
            return false;

    }

    /**
     * �������� ��������
     * 
     * @param doc ������� ��������
     * @return ������� �� ��������
     */
    public boolean restoreAction(Document doc) {
        return loadAction(doc, sRestore);
    }

    /**
     * ����� ��������� �������� (�������� ������)
     * 
     * @param doc ������� ��������
     * @return ������� �� ���������
     */
    public boolean applyAction(Document doc) {
        return loadAction(doc, sApply);
    }

    public abstract String getActionDescription();

    public int getActionIndex() {
        return actionIndex;
    }

    public String getActionName() {
        return actionName;
    }

    public int getActionState() {
        return actionState;
    }

    public String getActionTime() {
        return actionTime;
    }

}
