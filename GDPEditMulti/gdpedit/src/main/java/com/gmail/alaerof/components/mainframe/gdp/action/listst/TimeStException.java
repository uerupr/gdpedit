package com.gmail.alaerof.components.mainframe.gdp.action.listst;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class TimeStException extends Exception {
    private static final long serialVersionUID = 1L;

    public TimeStException() {

    }

    public TimeStException(String mess) {
        super(mess);
    }

    public TimeStException(String mess, Throwable t) {
        super(mess, t);
    }

    public TimeStException(Throwable t) {
        super(t);
    }
}
