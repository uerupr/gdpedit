package com.gmail.alaerof.exportexcel;

import com.gmail.alaerof.entity.DistrPoint;

public class StationToExcel {
    //private Station station;
    private int id;
    private DistrPoint.TypePoint typePoint;
    private String name;
    private String codeESR;
    private String codeExpress;
    private String km;

    public StationToExcel(int id, DistrPoint.TypePoint typePoint, String name, String codeESR, String codeExpress, String km) {
        //this.station = station;
        this.id = id;
        this.typePoint = typePoint;
        this.name = name;
        this.codeESR = codeESR;
        this.codeExpress = codeExpress;
        this.km = km;
    }

    /*public Station getDistrStation() {
        return station;
    }*/

    public String getKm() {
        return km;
    }

    public int getId() {
        return id;
    }

    public DistrPoint.TypePoint getTypePoint() {
        return typePoint;
    }

    public String getName() {
        return name;
    }

    public String getCodeESR() {
        return codeESR;
    }

    public String getCodeExpress() {
        return codeExpress;
    }
}
