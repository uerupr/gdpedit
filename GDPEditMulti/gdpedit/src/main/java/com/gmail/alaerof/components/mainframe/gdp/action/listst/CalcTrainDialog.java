package com.gmail.alaerof.components.mainframe.gdp.action.listst;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JCheckBox;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * ������ ��� "��������� ����� ������ �� ���������� �������� ����"
 * @author Helen Yrofeeva
 *
 */
public class CalcTrainDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JComboBox<RouteD> cbRoute;
    private JComboBox<LocomotiveType> cbLoc;
    private JCheckBox chbTown;
    private int result = 0;
    private static ResourceBundle bundle;

    /**
     *
     * @param frame ������������ ����
     * @param string title
     * @param modal  ������� �����������
     */
    public CalcTrainDialog(JFrame frame, String string, boolean modal) {
        super(frame, string, modal);
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.listst", LocaleManager.getLocale());
    }

    private void initContent() {
        setBounds(100, 100, 400, 150);
        getContentPane().setLayout(new BorderLayout());
        FlowLayout fl_contentPanel = new FlowLayout();
        contentPanel.setLayout(fl_contentPanel);
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        {
            JLabel label = new JLabel(bundle.getString("list.st.route"));
            contentPanel.add(label);
        }
        {
            cbRoute = new JComboBox<>();
            contentPanel.add(cbRoute);
        }
        {
            JLabel label = new JLabel(bundle.getString("listst.typeofmovement"));
            contentPanel.add(label);
        }
        {
            cbLoc = new JComboBox<>();
            contentPanel.add(cbLoc);
        }
        {
            chbTown = new JCheckBox(bundle.getString("listst.calculation"));
            contentPanel.add(chbTown);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog d = this;
            {
                String okString = bundle.getString("listst.ok");
                JButton okButton = new JButton(okString);
                okButton.setActionCommand(okString);
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        result = 1;
                        d.setVisible(false);
                    }
                });
            }
            {
                String cancelString = bundle.getString("listst.cancel");
                JButton cancelButton = new JButton(cancelString);
                cancelButton.setActionCommand(cancelString);
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        result = 0;
                        d.setVisible(false);
                    }
                });
            }
        }
    }

    public void setListLoc(List<LocomotiveType> listLoc) {
        cbLoc.removeAllItems();
        if (listLoc != null)
            for (int i = 0; i < listLoc.size(); i++) {
                cbLoc.addItem(listLoc.get(i));
            }
    }

    public void setListRoute(List<RouteD> listRoute) {
        cbRoute.removeAllItems();
        if (listRoute != null) {
            for (int i = 0; i < listRoute.size(); i++) {
                cbRoute.addItem(listRoute.get(i));
            }
        }
    }

    public RouteD getSelectedRoute() {
        return (RouteD)cbRoute.getSelectedItem();
    }
    
    public LocomotiveType getSelectedLoc(){
        return (LocomotiveType)cbLoc.getSelectedItem();
    }
    
    public boolean isTown(){
        return chbTown.isSelected();
    }

    public int getResult() {
        return result;
    }

}
