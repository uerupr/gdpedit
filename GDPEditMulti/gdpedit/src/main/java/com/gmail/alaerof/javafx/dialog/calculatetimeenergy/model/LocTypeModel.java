package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.energy.EnergyType;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateNormative;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDistrDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;

import java.io.Serializable;
import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LocTypeModel implements Comparable<LocTypeModel>, Serializable {
    private LocomotiveType locomotiveType;
    private EnergyType energyType;
    // �������� - ��� ����������� ����������� energyType
    private StringProperty locName;
    // �������� ������� �� ������/���������� ��� ������� LocTypeModel (������� �� 1 ���+����)
    private SimpleDoubleProperty downupExp;

    public LocTypeModel(LocomotiveType locomotiveType, EnergyType energyType) {
        this.locomotiveType = locomotiveType;
        this.energyType = energyType;
        String name = locomotiveType.getLtype();
        if (energyType != null) {
            name = name + "-" + energyType.getString();
        }
        this.locName = new SimpleStringProperty(name);

        this.downupExp = new SimpleDoubleProperty(0.0);
    }

    public LocTypeModel(ExpenseRateNormative expenseRateNormative) throws DataManageException {
        IDistrDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO();
        List<LocomotiveType> locomotiveTypeList = dao.getListLocomotiveType();
        for (int i = 0; i < locomotiveTypeList.size(); i++) {
            if (locomotiveTypeList.get(i).getIdLType() == expenseRateNormative.getIdLType()) {
                this.locomotiveType = locomotiveTypeList.get(i);
                this.energyType = expenseRateNormative.getEnergyType();
                String name = locomotiveType.getLtype();
                if (energyType != null) {
                    name = name + "-" + energyType.getString();
                }
                this.locName = new SimpleStringProperty(name);
                this.downupExp = new SimpleDoubleProperty(expenseRateNormative.getDownUpExp());
            }
        }
    }

    public LocomotiveType getLocomotiveType() {
        return locomotiveType;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public String getLocName() {
        return locName.get();
    }

    public StringProperty locNameProperty() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName.set(locName);
    }

    public double getDownupExp() {
        return downupExp.get();
    }

    public SimpleDoubleProperty downupExpProperty() {
        return downupExp;
    }

    public void setDownupExp(double downupExp) {
        this.downupExp.set(downupExp);
    }

    @Override
    public int compareTo(LocTypeModel o) {
        if (this.locomotiveType.getIdLType() == o.locomotiveType.getIdLType() &&
                this.energyType != null && o.energyType != null) {
            return this.energyType.compareTo(o.energyType);
        } else
            return this.locomotiveType.getIdLType() - o.locomotiveType.getIdLType();
    }

    @Override
    public String toString() {
        return locName.getValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocTypeModel that = (LocTypeModel) o;

        if (!locomotiveType.equals(that.locomotiveType)) return false;
        return energyType == that.energyType;
    }

    @Override
    public int hashCode() {
        int result = locomotiveType.hashCode();
        result = 31 * result + energyType.hashCode();
        return result;
    }

    public String getExpenseKeyString(){
        return locomotiveType.getLtype() + "-" + energyType.getString();
    }
}
