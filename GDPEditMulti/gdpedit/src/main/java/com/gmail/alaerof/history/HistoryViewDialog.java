package com.gmail.alaerof.history;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
//import java.awt.Image;
//import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

//import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.IconImageUtil;
import com.gmail.alaerof.util.TableUtils;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
class HistoryCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int col) {
        if (value != null)
            setText(value.toString());
        History history = ((HistoryTableModel) table.getModel()).getHistory();
        Action action = history.getActions().get(row);

        // if(isSelected){
        // setBackground(Color.BLUE);
        // }

        if (action.getActionState() == Action.sRestore) {
            setForeground(Color.GRAY);
        } else {
            setForeground(Color.BLACK);
        }

        Font oldFont = getFont();
        Font newFont = new java.awt.Font(oldFont.getName(), java.awt.Font.BOLD, oldFont.getSize());
        setFont(newFont);

        return this;
    }

}

public class HistoryViewDialog extends JDialog {

    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTable table;
    private HistoryTableModel tableModel;
    private static ResourceBundle bundle;

    /**
     * Create the dialog.
     */
    public HistoryViewDialog() {
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.history", LocaleManager.getLocale());
    }

    private void initContent() {
        setBounds(100, 100, 270, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.NORTH);
        contentPanel.setLayout(new BorderLayout(0, 0));
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton restoreButton = new JButton();
                restoreButton.setIcon(IconImageUtil.getIcon(IconImageUtil.RESTORE));

                buttonPane.add(restoreButton);
                getRootPane().setDefaultButton(restoreButton);
                restoreButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        int selRow = table.getSelectedRow();
                        if (selRow > -1)
                            tableModel.getHistory().restoreAction(selRow);
                        else
                            tableModel.getHistory().restoreAction();
                        tableModel.fireTableDataChanged();
                    }
                });
            }
            {
                JButton applyButton = new JButton();
                applyButton.setIcon(IconImageUtil.getIcon(IconImageUtil.APPLY));
                buttonPane.add(applyButton);
                applyButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int selRow = table.getSelectedRow();
                        if (selRow > -1)
                            tableModel.getHistory().applyAction(selRow);
                        else
                            tableModel.getHistory().applyAction();
                        tableModel.fireTableDataChanged();
                    }
                });
            }

            {
                JButton clearButton = new JButton(bundle.getString("history.clearhistory"));
                buttonPane.add(clearButton);
                clearButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        tableModel.getHistory().clear();
                        tableModel.fireTableDataChanged();
                    }
                });
            }

        }
        
        {
            tableModel = new HistoryTableModel();
            table = new JTable(tableModel);
            JScrollPane scrollPane = new JScrollPane(table);
            getContentPane().add(scrollPane, BorderLayout.CENTER);

            // table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 15, 19, 90 };
            TableUtils.setTableColumnSize(table, colSize);

            table.setDefaultRenderer(String.class, new HistoryCellRenderer());
        }

    }

    public HistoryViewDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    public History getHistory() {
        return tableModel.getHistory();
    }

    public void setHistory(History history) {
        tableModel.setHistory(history);
        table.repaint();
    }

}
