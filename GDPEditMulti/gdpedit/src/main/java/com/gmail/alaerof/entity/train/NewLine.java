package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;

import javax.swing.JButton;

import com.gmail.alaerof.util.CalcString;

/***
 * ������� ����� ������������ �������� ����� ������
 * 
 * @author Helen Yrofeeva
 * 
 */
public abstract class NewLine extends JButton {
    /** texOn - ����� ������ � �����, texOff - ����� ����� � ������������ */
    public enum DrawCaption {
        NoCaption, Center, On, Off, texOn, texOff
    }

    public enum Direction {
        right, left, vert, hor
    }
    /** TexOn - ����� ������ � �����, TexOff - ����� ����� � ������������ */
    public enum DrawTex {
        NoTex, TexOn, TexOff
    }

    /** ������� �����: 0 - ������; 1 - ����� */
    private int clickPosition = 0;

    private static final long serialVersionUID = 1L;
    protected Polygon polygon;
    private Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);

    /** ������� ��������� ������� */
    private DrawCaption caption = DrawCaption.NoCaption;
    /** ���������� ����� �������� */
    private boolean isTimeA;
    /** ���������� ����� ����������� */
    private boolean isTimeL;
    /** �������� ������ ��� �������� */
    private Dimension originalSize = new Dimension(1, 1);
    protected Direction direction;

    protected static final int ADD_WIDTH = 1;
    protected TrainThreadGeneralParams generalParams;
    protected NewLineSelfParams selfParams;
    protected double alphaCaption;
    protected Point captionPoint = new Point();
    protected Point begin = new Point();
    protected Point end = new Point();
    protected Point pointTimeA = new Point();
    protected Point pointTimeL = new Point();

    protected int xleft;
    protected int ytop;
    protected Point captionPointGDP = new Point();
    protected Point captionPointGDP1 = new Point();
    protected Point beginGDP = new Point();
    protected Point endGDP = new Point();
    protected Point pointTimeAGDP = new Point();
    protected Point pointTimeLGDP = new Point();
    /** ������� ��������� "��������" ��� ���. ������� */
    private DrawTex drawTex = DrawTex.NoTex;

    protected TrainThreadElement trainThreadElement;

    protected static PopupMenuNewLine linePopupMenu = new PopupMenuNewLine();
    protected static PopupMenuStDouble doublePopupMenu = new PopupMenuStDouble();
    protected static PopupMenuLineHor lineHorPopupMenu = new PopupMenuLineHor();

    public NewLine(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams,
            final TrainThreadElement trainThreadElement) {
        this.setCursor(handCursor);
        this.setGeneralParams(generalParams);
        this.setSelfParams(selfParams);
        this.trainThreadElement = trainThreadElement;
        this.setToolTipText(this.selfParams.getCaptionValue());

        // This call causes the JButton not to paint the background.
        // This allows us to paint a round background.
        setContentAreaFilled(false);
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                double gbegin = Math.pow(e.getX() - begin.getX(), 2) + Math.pow(e.getY() - begin.getY(), 2);
                double gend = Math.pow(e.getX() - end.getX(), 2) + Math.pow(e.getY() - end.getY(), 2);
                clickPosition = 0;
                if (gbegin > gend) {
                    clickPosition = 1;
                }
            }
        });
        addMouseListener(new PopupListener());
    }

    @Override
    protected void paintComponent(Graphics g) {

        Color pen = selfParams.color;
        if (trainThreadElement != null && trainThreadElement.trainThread.isSelected()) {
            pen = getSelectedTrainColor();
        }

        if (getModel().isArmed()) {
            pen = Color.lightGray;
        }
        g.setColor(pen);

        if (polygon == null || !polygon.getBounds().equals(getBounds())) {
            polygon = getPolygon();
        }

        Graphics2D g2 = (Graphics2D) g;

        if (drawTex == DrawTex.TexOff) {
            g2.setStroke(new BasicStroke(2));
            g2.drawLine(begin.x, begin.y, begin.x - 5, begin.y - 5);
            g2.drawLine(begin.x, begin.y, begin.x, begin.y - 3);
        }
        if (drawTex == DrawTex.TexOn) {
            g2.setStroke(new BasicStroke(2));
            g2.drawLine(end.x, end.y, end.x + 5, end.y - 5);
            g2.drawLine(end.x + 5, end.y - 2, end.x + 5, end.y - 5);
            g2.drawLine(end.x + 2, end.y - 5, end.x + 5, end.y - 5);
        }

        if (selfParams.lineStyle == LineStyle.Solid) {
            g2.setStroke(new BasicStroke(selfParams.lineWidth));
            g2.drawLine(begin.x, begin.y, end.x, end.y);
        } else {
            Color bg = g2.getBackground();
            LineStyle.drawLine(g2, begin.x, begin.y, end.x, end.y, selfParams.lineStyle,
                    selfParams.lineWidth, bg, pen);
        }
        if (caption == DrawCaption.Center) {
            g2.setFont(generalParams.captionSp);
            AffineTransform orig = g2.getTransform();
            g2.rotate(alphaCaption, captionPoint.x, captionPoint.y);
            g2.drawString(selfParams.getCaptionValue(), captionPoint.x, captionPoint.y);
            g2.setTransform(orig);
        }
        if (caption == DrawCaption.On) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), polygon.xpoints[1], polygon.ypoints[1]);
        }
        if (caption == DrawCaption.Off) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), captionPoint.x, captionPoint.y);
        }

        if (caption == DrawCaption.texOn) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), polygon.xpoints[1], polygon.ypoints[1]);
            // g2.drawString("'/'", captionPoint.x, captionPoint.y);
        }
        if (caption == DrawCaption.texOff) {
            g2.setFont(generalParams.captionSt);
            // g2.drawString(".\\.", polygon.xpoints[1], polygon.ypoints[1]);
            g2.drawString(selfParams.getCaptionValue(), captionPoint.x, captionPoint.y);
        }

        if (isTimeL) {
            g2.setFont(generalParams.time);
            g2.drawString(selfParams.timeL, pointTimeL.x, pointTimeL.y);
        }
        if (isTimeA) {
            g2.setFont(generalParams.time);
            g2.drawString(selfParams.timeA, pointTimeA.x, pointTimeA.y);
        }

        super.paintComponent(g);
    }

    public void drawInPicture(Graphics g) {
        Color pen = selfParams.color;
        g.setColor(pen);

        polygon = getPolygon();

        int left = xleft + generalParams.xLRightJoint;
        int top = ytop + generalParams.yIndTop;

        Graphics2D g2 = (Graphics2D) g;

        if (drawTex == DrawTex.TexOn) {
            g2.setStroke(new BasicStroke(2));
            g2.drawLine(beginGDP.x + left, beginGDP.y + top, beginGDP.x - 5 + left, beginGDP.y - 5 + top);
            g2.drawLine(beginGDP.x + left, beginGDP.y + top, beginGDP.x + left, beginGDP.y - 3 + top);
        }
        if (drawTex == DrawTex.TexOff) {
            g2.setStroke(new BasicStroke(2));
            g2.drawLine(endGDP.x + left, endGDP.y + top, endGDP.x + 5 + left, endGDP.y - 5 + top);
            g2.drawLine(endGDP.x + 5 + left, endGDP.y - 2 + top, endGDP.x + 5 + left, endGDP.y - 5 + top);
            g2.drawLine(endGDP.x + 2 + left, endGDP.y - 5 + top, endGDP.x + 5 + left, endGDP.y - 5 + top);
        }

        if (selfParams.lineStyle == LineStyle.Solid) {
            g2.setStroke(new BasicStroke(selfParams.lineWidth));
            g2.drawLine(beginGDP.x + left, beginGDP.y + top, endGDP.x + left, endGDP.y + top);
        } else {
            Color bg = g2.getBackground();
            LineStyle.drawLine(g2, beginGDP.x + left, beginGDP.y + top, endGDP.x + left, endGDP.y + top,
                    selfParams.lineStyle, selfParams.lineWidth, bg, pen);
        }
        if (caption == DrawCaption.Center) {
            g2.setFont(generalParams.captionSp);
            AffineTransform orig = g2.getTransform();
            g2.rotate(alphaCaption, captionPointGDP.x + left, captionPointGDP.y + top);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP.x + left, captionPointGDP.y + top);
            g2.setTransform(orig);
        }
        if (caption == DrawCaption.On) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP1.x + left, captionPointGDP1.y + top);
        }
        if (caption == DrawCaption.Off) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP.x + left, captionPointGDP.y + top);
        }

        if (caption == DrawCaption.texOn) {
            g2.setFont(generalParams.captionSt);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP1.x + left, captionPointGDP1.y + top);
            // g2.drawString("'/'", captionPoint.x, captionPoint.y);
        }
        if (caption == DrawCaption.texOff) {
            g2.setFont(generalParams.captionSt);
            // g2.drawString(".\\.", polygon.xpoints[1], polygon.ypoints[1]);
            g2.drawString(selfParams.getCaptionValue(), captionPointGDP.x + left, captionPointGDP.y + top);
        }

        if (isTimeL) {
            g2.setFont(generalParams.time);
            g2.drawString(selfParams.timeL, pointTimeLGDP.x + left, pointTimeLGDP.y + top);
        }
        if (isTimeA) {
            g2.setFont(generalParams.time);
            g2.drawString(selfParams.timeA, pointTimeAGDP.x + left, pointTimeAGDP.y + top);
        }
        polygon = null;
    }

    // Paint the border of the button using a simple stroke.
    protected void paintBorder(Graphics g) {
        g.setColor(getBackground());
        if (polygon == null || !polygon.getBounds().equals(getBounds())) {
            polygon = getPolygon();
        }
        // g.drawOval(0, 0, getSize().width - 1, getSize().height - 1);
        // g.drawPolygon(polygon);
    }

    // Hit detection.
    public boolean contains(int x, int y) {
        // If the button has changed size, make a new shape object.
        if (polygon == null || !polygon.getBounds().equals(getBounds())) {
            polygon = getPolygon();
        }
        return polygon.contains(x, y);
    }

    /** ��������� ����� = ��������� ����� */
    protected abstract Polygon getPolygon();

    public boolean isCaption() {
        return caption != DrawCaption.NoCaption;
    }

    public void setCaption(DrawCaption caption) {
        this.caption = caption;
    }

    public TrainThreadGeneralParams getGeneralParams() {
        return generalParams;
    }

    public void setGeneralParams(TrainThreadGeneralParams generalParams) {
        this.generalParams = generalParams;
    }

    public NewLineSelfParams getSelfParams() {
        return selfParams;
    }

    public void setSelfParams(NewLineSelfParams selfParams) {
        setBackground(selfParams.color);
        this.selfParams = selfParams;
        this.setToolTipText(this.selfParams.getCaptionValue());
    }

    protected Dimension getCaptionSize() {
        int h = CalcString.getStringH(generalParams.captionSp);
        int w = CalcString.getStringW(generalParams.captionSp, selfParams.getCaptionValue()) + 2;
        return new Dimension(w, h);
    }

    public void setCaptionValue(String captionValue) {
        selfParams.setCaptionValue(captionValue);
    }

    public String getCaptionValue() {
        return selfParams.getCaptionValue();
    }

    /**
     * ������ ������ ����� �� ����� �� ����������� (��� ����� � ������), ��
     * ������ ���� (��� ������������), �� ������� ���� (��� ��������������)
     * 
     * @return ������� ������ ����� �� ����� �������
     */
    public abstract Dimension getIndent();

    protected Dimension getTimeSize() {
        int h = CalcString.getStringH(generalParams.time);
        int w = CalcString.getStringW(generalParams.time, "0");
        return new Dimension(w, h);
    }

    public void setTimeA(boolean isTimeA) {
        this.isTimeA = isTimeA;
    }

    public void setTimeL(boolean isTimeL) {
        this.isTimeL = isTimeL;
    }

    public void setPosition(int x, int y) {
        xleft = x;
        ytop = y;
        int w = this.getIndent().width * 2 + originalSize.width;
        int h = this.getIndent().height * 2 + originalSize.height;
        this.setBounds(x - this.getIndent().width, y - this.getIndent().height, w, h);
    }

    public Dimension getOriginalSize() {
        return originalSize;
    }

    public void setOriginalSize(Dimension originalSize) {
        this.originalSize = originalSize;
    }

    public TrainThread getTrainThread() {
        if (trainThreadElement != null) {
            return trainThreadElement.getTrainThread();
        } else {
            return null;
        }
    }

    public TrainThreadElement getTrainThreadElement() {
        return trainThreadElement;
    }

    @Override
    public String toString() {
        return "NewLine [" + selfParams.getCaptionValue() + "]";
    }

    public void updateLineListener() {
        // ��������� ����� ������������� ������� ������� ������
        TrainThread train = getTrainThread();
        if (train != null) {
            addActionListener(train.getLineListener());
        }
    }

    public Direction getDirection() {
        return direction;
    }

    public void removeAllActionListeners() {
        ActionListener[] list = this.getActionListeners();
        for (ActionListener al : list) {
            this.removeActionListener(al);
        }
    }

    public int getClickPosition() {
        return clickPosition;
    }

    public DrawTex getDrawTex() {
        return drawTex;
    }

    public void setDrawTex(DrawTex drawTex) {
        this.drawTex = drawTex;
    }

    private Color getSelectedTrainColor() {
        Color selectedTrainColor = Color.ORANGE;
        if (GDPEdit.gdpEdit.getApplicationConfig() != null &&
                GDPEdit.gdpEdit.getApplicationConfig().getSelectTrainColor() != null) {
            selectedTrainColor = GDPEdit.gdpEdit.getApplicationConfig().getSelectTrainColor();
        }
        return selectedTrainColor;
    }



    private final NewLine newLine = this;
    class PopupListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            Direction direct = getDirection();
            if (direct != Direction.vert) {
                if (e.isPopupTrigger()) {
                    TrainThreadElement element = getTrainThreadElement();
                    if (element instanceof TrainStDouble) {
                        doublePopupMenu.setLine(newLine);
                        doublePopupMenu.show(e.getComponent(), e.getX(), e.getY());
                    } else {
                        if (element instanceof TrainSt) {
                            if(element.getStationBegin().station.isExpanded()){
                                lineHorPopupMenu.setLine(newLine);
                                lineHorPopupMenu.show(e.getComponent(), e.getX(), e.getY());
                            }else{
                                linePopupMenu.setLine(newLine);
                                linePopupMenu.show(e.getComponent(), e.getX(), e.getY());
                            }
                        }else{
                            linePopupMenu.setLine(newLine);
                            linePopupMenu.show(e.getComponent(), e.getX(), e.getY());
                        }
                    }
                }
            }
        }
    }
}
