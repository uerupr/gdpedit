package com.gmail.alaerof.dialog.linesttrain;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.entity.DistrLineSt;
import com.gmail.alaerof.entity.train.TimeStation;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.manager.LocaleManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Helen Yrofeeva
 */
public class ListTrainStTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(ListTrainStTableModel.class);
    private String[] columnNames = { bundle.getString("linesttrain.numberoftrain"),
            bundle.getString("linesttrain.name"),
            bundle.getString("linesttrain.arriv"),
            bundle.getString("linesttrain.depart"),
            bundle.getString("linesttrain.tech"),
            bundle.getString("linesttrain.numberoftheway_"),
            bundle.getString("linesttrain.hide") };
    private List<TrainThread> listTrainSt;
    private int idSt;
    private JTable table;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.linesttrain", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (listTrainSt != null)
            return listTrainSt.size();
        else
            return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (listTrainSt != null) {
            TrainThread train = listTrainSt.get(row);
            if (train != null) {
                TimeStation st = train.getTimeSt(idSt);
                LineStTrain lstTr = st.getLineStTrain();
                switch (col) {
                case 0:
                    return train.getCode();
                case 1:
                    return train.getNameTR();
                case 2:
                    String tOn = "";
                    if (!st.isNullOn())
                        tOn = lstTr.getOccupyOn();
                    return tOn;
                case 3:
                    String tOff = "";
                    if (!st.isNullOff())
                        tOff = lstTr.getOccupyOff();
                    return tOff;
                case 4:
                    return lstTr.getTexStop();
                case 5:
                    int idLineSt = lstTr.getIdLineSt();
                    DistrLineSt lineSt = st.getLine(idLineSt);
                    return lineSt;
                case 6:
                    String ss = "";
                    if (train.isHidden())
                        ss = bundle.getString("linesttrain.star");
                    return ss;
                default:
                    return null;
                }
            }
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (listTrainSt != null)
            switch (col) {
            case 4:
                int tTex = 0;
                try {
                    tTex = Integer.parseInt(aValue.toString());
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                    tTex = 0;
                }
                if (tTex >= 0) {
                    TrainThread train = listTrainSt.get(row);
                    if (train != null) {
                        TimeStation st = train.getTimeSt(idSt);
                        LineStTrain lstTr = st.getLineStTrain();
                        lstTr.setTexStop(tTex);
                        st.updatePositionTexStop(train.getGeneralParams(), train.getDistrEntity()
                                .getConfigGDP());
                        train.setTrainLocation();
                    }
                }
                break;
            case 5:
                DistrLineSt lineSt = (DistrLineSt) aValue;
                if (lineSt != null) {
                    TrainThread train = listTrainSt.get(row);
                    TimeStation st = train.getTimeSt(idSt);
                    LineStTrain lstTr = st.getLineStTrain();
                    int idLineSt = lineSt.getLineSt().getIdLineSt();

                    train.setLineStTr(lstTr.getIdSt(), idLineSt);
                    // ���������� ��� ������
                    train.updateLineForDouble(idLineSt, lstTr.getIdSt());
                    table.repaint();
                }
                break;
            default:
                break;
            }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 4 || columnIndex == 5) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 4:
            return Integer.class;
        case 5:
            return DistrLineSt.class;
        default:
            return String.class;
        }
    }

    public List<TrainThread> getListTrainSt() {
        return listTrainSt;
    }

    public void setListTrainSt(List<TrainThread> listTrainSt) {
        this.listTrainSt = listTrainSt;
    }

    public int getIdSt() {
        return idSt;
    }

    public void setIdSt(int idSt) {
        this.idSt = idSt;
    }

    public void setTable(JTable table) {
        this.table = table;
    }
}
