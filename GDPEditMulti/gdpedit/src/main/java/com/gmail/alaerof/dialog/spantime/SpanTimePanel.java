package com.gmail.alaerof.dialog.spantime;

import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.TableUtils;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
public class SpanTimePanel extends JPanel {
    protected static Logger logger = LogManager.getLogger(SpanTimePanel.class);
    private static final long serialVersionUID = 1L;
    private DistrSpan distrSpan;
    private List<LocomotiveType> listLoc;
    private SpanTimeHolderTableModel timeTableModel;
    private JTable timeTable;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spantime", LocaleManager.getLocale());
    }

    /**
     * Create the panel.
     */
    public SpanTimePanel() {
        setLayout(new BorderLayout(0, 0));
        try {
            this.listLoc = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO().getListLocomotiveType();
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
        initContent();
    }

    private void initContent() {
        {
            setPreferredSize(new Dimension(400, 250));

            JLabel label = new JLabel(bundle.getString("spantime.stagerunningtimes"));
            label.setFont(new Font("Tahoma", Font.BOLD, 11));
            label.setHorizontalAlignment(SwingConstants.CENTER);
            add(label, BorderLayout.NORTH);

            JPanel panel = new JPanel();
            add(panel, BorderLayout.CENTER);
            panel.setLayout(new BorderLayout(0, 0));

            JPanel panelS = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panelS.getLayout();
            flowLayout.setAlignment(FlowLayout.TRAILING);
            panel.add(panelS, BorderLayout.SOUTH);

            JScrollPane scrollPane = new JScrollPane();
            panel.add(scrollPane, BorderLayout.CENTER);

            Font tableFont = new Font(Font.SANS_SERIF, 0, 11);
            timeTableModel = new SpanTimeHolderTableModel();
            timeTable = new JTable(timeTableModel);
            timeTable.setFont(tableFont);
            timeTable.getTableHeader().setFont(tableFont);
            timeTable.setFillsViewportHeight(true);
            timeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 80, 45, 45, 45, 45, 45 };
            TableUtils.setTableColumnSize(timeTable, colSize);
            scrollPane.setViewportView(timeTable);

            JButton saveButton = new JButton(bundle.getString("spantime.db"));
            saveButton.setToolTipText(bundle.getString("spantime.savestagerunningtimesindb"));
            panelS.add(saveButton);
            final JPanel spanTimePanel = this;
            saveButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    spanTimePanel.setCursor(GDPEdit.waitCursor);
                    try {
                        distrSpan.saveSpanTime();
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                    }finally{
                        spanTimePanel.setCursor(GDPEdit.defCursor);
                    }
                }
            });
        }
    }

    public void setDistrSpan(DistrSpan distrSpan) {
        this.distrSpan = distrSpan;
        fillTimeTable();
    }

    private void fillTimeTable() {
        List<SpanTimeHolder> list = null;
        if (distrSpan != null && listLoc != null) {
            list = new ArrayList<>();
            OE oev[] = OE.values();
            for (LocomotiveType ltype : listLoc) {
                for (OE oe : oev) {
                    Time time = distrSpan.getSpanTime(ltype, oe);
                    if(time == null){
                        // ��� ��� �� ������� ����� ������ � ���������� ��� � distrSpan
                        // ���� ����� ������ ������ "���������" ��� ��������� � �� 
                        time = new Time(); 
                        distrSpan.setSpanTime(time, ltype, oe);        
                    }
                    SpanTimeHolder sth = new SpanTimeHolder(time, ltype, oe);
                    list.add(sth);
                }
            }
            Collections.sort(list);
            timeTableModel.setList(list);
        }
        timeTableModel.setList(list);
        timeTable.repaint();
    }

}
