package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.Dimension;
import java.awt.Polygon;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class LineVert extends NewLine {
    private static final long serialVersionUID = 1L;
    public static final int VERT_WIDTH = 10;

    public LineVert(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams,
                    TrainThreadElement trainThreadElement) {
        super(generalParams, selfParams, trainThreadElement);
        direction = Direction.vert;
    }

    @Override
    protected Polygon getPolygon() {
        final int npoints = 10;
        int x[] = new int[npoints];
        int y[] = new int[npoints];
        int pw = this.getIndent().width;
        int h = getSize().height;
        x[0] = pw;
        y[0] = 0;

        x[1] = x[0] + pw;
        y[1] = y[0];

        x[5] = x[4] = x[3] = x[2] = x[1];
        y[5] = y[4] = y[3] = y[2] = generalParams.codeStepO;

        x[6] = x[1];
        y[6] = h;

        x[7] = x[0];
        y[7] = y[6];

        x[8] = 0;
        y[8] = y[6];

        x[9] = x[8];
        y[9] = y[0];

        captionPoint.x = x[2];
        captionPoint.y = y[2];
        begin.x = x[0];
        begin.y = y[0];
        end.x = x[7];
        end.y = y[7];

        captionPointGDP.x = x[2]-pw;
        captionPointGDP.y = y[2];
        beginGDP.x = x[0]-pw;
        beginGDP.y = y[0];
        endGDP.x = x[7]-pw;
        endGDP.y = y[7];

        return new Polygon(x, y, npoints);
    }

    @Override
    public void setCaption(DrawCaption caption) {
        super.setCaption(DrawCaption.NoCaption);
    }

    @Override
    public Dimension getIndent() {
        return new Dimension((int) (ADD_WIDTH + selfParams.lineWidth), 0);
    }

    @Override
    public void setPosition(int x, int y) {
        xleft = x;
        ytop= y;
        int pw = getIndent().width;
        int w = getOriginalSize().width;
        int h = getOriginalSize().height;
        this.setBounds(x - pw, y, w + pw, h);
    }
}
