package com.gmail.alaerof.gdpcalculation;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.IntGR;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.SpTOcc;
import com.gmail.alaerof.dao.dto.routed.RouteDTrain;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.entity.train.TrainStTableData;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.gdpcalculation.model.DistrSpanGDPCalc;
import com.gmail.alaerof.gdpcalculation.model.DistrStationGDPCalc;
import com.gmail.alaerof.gdpcalculation.model.DistrTrainGDPCalc;
import com.gmail.alaerof.gdpcalculation.model.DistrTrainPriorityComparator;
import com.gmail.alaerof.gdpcalculation.model.TimeSpCalc;
import com.gmail.alaerof.gdpcalculation.model.TimeStCalc;
import com.gmail.alaerof.manager.LocaleManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import com.gmail.alaerof.dao.dto.routed.RouteDSpan;
import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.SpanStTime;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.entity.DistrPoint.TypePoint;
import com.gmail.alaerof.history.ActionTrainEditTime;
import com.gmail.alaerof.history.History;
import com.gmail.alaerof.history.Action.TypeValue;
import com.gmail.alaerof.util.TimeConverter;
import java.util.Map;
import java.util.ResourceBundle;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 */
public class CalculateGDPHandler {
    protected static Logger logger = LogManager.getLogger(CalculateGDPHandler.class);
    // M������ �� ����������� �� ���������:
    private static final String ERR_NO_SPANS;
    // �.�. �� ������
    private static final String ERR_NO_SPANST;
    // ����� ���� �� �.�. �� �������:
    private static final String ERR_NO_SPANSTTIME;
    /** ������� ������� */
    public static final int CALC_DEPTH = 15;
    /**
     * �� ������� ��� ��������� �������� (��� = 0) ��� �� ������ ������,
     * ������� ������������ �� �������� ��� ���� �������, �� ������� ������������ ��������
     */
    private static final String ERR_DEFAULT_ROUTE;
    /** ������������������ ������� �������� �������� */
    private static final String ERR_ANY_ROUTE;
    /** ��� ������� ��� ������� ��������� ������ */
    private static final String ERR_GDP_CALC;
    /** �� ������� ��������� �����: */
    private static final String ERR_GDP_CALC_TRAIN;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("bundles.gdpcalculation", LocaleManager.getLocale());
        ERR_NO_SPANS = bundle.getString("gdpcalculation.ERR_NO_SPANS");
        ERR_NO_SPANST = bundle.getString("gdpcalculation.ERR_NO_SPANST");
        ERR_NO_SPANSTTIME = bundle.getString("gdpcalculation.ERR_NO_SPANSTTIME");
        ERR_DEFAULT_ROUTE = bundle.getString("gdpcalculation.ERR_DEFAULT_ROUTE");
        ERR_ANY_ROUTE = bundle.getString("gdpcalculation.ERR_ANY_ROUTE");
        ERR_GDP_CALC = bundle.getString("gdpcalculation.ERR_GDP_CALC");
        ERR_GDP_CALC_TRAIN = bundle.getString("gdpcalculation.ERR_GDP_CALC_TRAIN");
    }

    private static class SpanMove{
        boolean stop1;
        boolean stop2;
        int on2;
    }

    /**
     * ���������� ������ ������� ������ �� ���� �������� �� ��������, �.�. StB ��� ����� � StE ��� ���
     *
     * @param distrTrain ����� �� �������
     * @param distrSpan  ������� �������
     * @return ������� �������
     */
    private static DistrStation getStB(DistrTrain distrTrain, DistrSpan distrSpan) {
        DistrStation st = distrSpan.getStB();
        if (distrTrain.getOe() == OE.even) {
            st = distrSpan.getStE();
        }
        return st;
    }

    /**
     * ���������� ������ ������� ��������� �� ���� �������� �� ��������, �.�. StE ��� ����� � StB ��� ���
     *
     * @param distrTrain ����� �� �������
     * @param distrSpan  ������� �������
     * @return ������� �������
     */
    private static DistrStation getStE(DistrTrain distrTrain, DistrSpan distrSpan) {
        DistrStation st = distrSpan.getStE();
        if (distrTrain.getOe() == OE.even) {
            st = distrSpan.getStB();
        }
        return st;
    }

    /**
     * ���������� ����� ���� � ������� �� ��������
     * � ������ ������������� ��������� �� ��������� �������� ��������
     *
     * @param time  ���������� ����� ���� ������
     * @param stopB ��������� �� ��������� ������� �� ���� ��������
     * @param stopE ��������� �� �������� ������� �� ���� ��������
     * @return ����� � �������
     */
    private static float getTimeMove(Time time, boolean stopB, boolean stopE) {
        float timeM = 0;
        if (time != null) {
            timeM = time.getTimeMove();
            if (stopB) {
                timeM += time.getTimeUp();
            }
            if (stopE) {
                timeM += time.getTimeDown();
            }
        }
        return timeM;
    }

    /**
     * ��������� ����� �� ����������
     */
    public static void buildBySpanTime(TrainThread train, boolean town, LocomotiveType loc, RouteD route) {
        ArrayList<TrainStTableData> listStTableData = new ArrayList<>();
        List<RouteDSpan> listSp = route.getListSpan();
        if (listSp.size() > 0) {
            DistrTrain distrTrain = train.getDistrTrain();
            if (distrTrain.getOe() == OE.odd) {
                // ������� ��� �������� � ������ �������
                Collections.sort(listSp);
            } else {
                // ������� ��� ������ � �������� �������
                Collections.sort(listSp);
                Collections.reverse(listSp);
            }
            DistrEntity distrEntity = train.getDistrEntity();
            String tmB = train.getDistrTrain().getOccupyOff();
            RouteDSpan rsp = listSp.get(0);
            DistrSpan distrSpan = distrEntity.getDistrSpan(rsp.getIdSpan());
            DistrStation distrSt = getStB(distrTrain, distrSpan);
            TrainStTableData stdB = new TrainStTableData();
            stdB.distrpoint = distrSt;
            stdB.tOn = tmB;
            stdB.tOff = tmB;
            listStTableData.add(stdB);
            int minB = TimeConverter.timeToMinutes(tmB);
            String tmE = null;
            float tMove = 0;
            for (int i = 0; i < listSp.size(); i++) {
                rsp = listSp.get(i);
                distrSpan = distrEntity.getDistrSpan(rsp.getIdSpan());
                // ������� �� �.�.
                if (town) {
                    calcTown(distrEntity, distrSpan, distrTrain, loc, listStTableData, minB);
                    // -----
                }
                // �.�. - ����� ��������
                distrSt = getStE(distrTrain, distrSpan);
                Time trainTime = CalculateUtil.getTrainTime(distrSpan, loc, distrTrain, true);
                tMove = getTimeMove(trainTime, i == 0, i == (listSp.size() - 1));
                int minE = TimeConverter.addMinutes(minB, (int) tMove);
                tmE = TimeConverter.minutesToTime(minE);
                TrainStTableData std = new TrainStTableData();
                std.distrpoint = distrSt;
                std.tOn = tmE;
                std.tOff = tmE;
                listStTableData.add(std);

                minB = minE;
            }
            ActionTrainEditTime action = new ActionTrainEditTime(train);
            // ���������� � ������� �������� ����������� ��������
            History history = train.getHistory();
            history.addAction(action, TypeValue.oldValue);
            // ��������� ��������
            train.applyThreadByTable(listStTableData, town);
            // ���������� � ������� �������� ������ ��������
            history.addAction(action, TypeValue.newValue);
        } else {
            // todo message box
            logger.error(ERR_NO_SPANS + " " + route.getNameR());
        }
    }

    /**
     * ������ �������� �� �.�. ��� ������������ ������
     *
     * @param distrEntity     {@link DistrEntity}
     * @param distrSpan       {@link DistrSpan}
     * @param distrTrain      {@link DistrTrain}
     * @param loc             {@link LocomotiveType}
     * @param listStTableData {@link TrainStTableData} ������ �������� (�� ��� �� + ��������/�����������)
     *                        �� �.�. � �.�. �� ���� ��������,
     *                        ���� ��������� ������� �� �.�. �������� distrSpan
     * @param minB            ����� ����� �� ������� (����������� � ��������� ������� ��������)
     */
    private static void calcTown(DistrEntity distrEntity, DistrSpan distrSpan, DistrTrain distrTrain,
                                 LocomotiveType loc, ArrayList<TrainStTableData> listStTableData, int minB) {
        List<SpanSt> listSpanSt = new ArrayList<>(distrSpan.getSpanScale().getSpanSt());
        if (distrTrain.getOe() == OE.odd) {
            // ������� ��� �������� � ������ �������
            Collections.sort(listSpanSt);
        } else {
            // ������� ��� ������ � �������� �������
            Collections.sort(listSpanSt);
            Collections.reverse(listSpanSt);
        }
        int minTB = minB;
        for (int j = 0; j < listSpanSt.size(); j++) {
            SpanSt spanSt = listSpanSt.get(j);
            DistrPoint ds = distrEntity.getDistrPoint(spanSt.getIdSpanst(), TypePoint.StopPoint);
            if (ds != null) {

                int num = spanSt.getNum();
                if (distrTrain.getOe() == OE.odd) {
                    num--;
                }
                int idScale = distrSpan.getSpanScale().getIdScale();
                int idLoc = loc.getIdLType();
                SpanStTime stTime = distrSpan.getSpanStTime(idScale, idLoc, num);
                if (stTime != null) {
                    float tMove = (float) stTime.getTmoveO();
                    if (distrTrain.getOe() == OE.even) {
                        tMove = (float) stTime.getTmoveE();
                    }
                    int minTE = TimeConverter.addMinutes(minTB, (int) tMove);
                    String tmE = TimeConverter.minutesToTime(minTE);
                    TrainStTableData std = new TrainStTableData();
                    std.distrpoint = ds;
                    std.tOn = tmE;
                    minTE = TimeConverter.addMinutes(minTE, spanSt.getTstop());
                    tmE = TimeConverter.minutesToTime(minTE);
                    std.tOff = tmE;
                    listStTableData.add(std);

                    minTB = minTE;
                } else {
                    // todo message box
                    logger.error(ERR_NO_SPANSTTIME + " " + distrSpan.getSpanScale().getName() +
                            " " + loc.getLtype() + " N=" + num);
                }
            } else {
                // todo message box
                logger.error(ERR_NO_SPANST + " " + spanSt.toString() + " - " + distrEntity.toString());
            }
        }
    }

    /**
     * ������ ��� ��� ������� �� calculateList �� ������� distrEntity � ������ ��� ����������� fixedList
     *
     * @param distrEntity   ������� ��� ������� �������
     * @param fixedList     ������ �������, ���������� ������� �� ����� ��������
     * @param calculateList ������ �������, ������� ����� ��������� �� �������
     * @param errorList ������ ������ ��� ������� ��� ��� ���������� � �������� ������
     */
    public static void buildConcurrentGDP(DistrEntity distrEntity, Collection<DistrTrain> fixedList,
                                          Collection<DistrTrain> calculateList, List<Category> categoryList,
                                          List<String> errorList) {

        // �������� ��������� ������� (� ����������) ��� ������
        List<RouteD> listR = distrEntity.getListRoute();
        // �������� ������� ����������� ��������
        if (!checkForDefaultRoute(listR)) {
            logger.error(ERR_DEFAULT_ROUTE);
            errorList.add(ERR_DEFAULT_ROUTE);
            return;
        }
        for (RouteD routeD : listR) {
            if (!checkRouteConsistency(routeD, distrEntity)) {
                logger.error(ERR_ANY_ROUTE + ": " + routeD.getNameR());
                errorList.add(ERR_ANY_ROUTE + ": " + routeD.getNameR());
            }
        }
        // �� ����, ����� �������� ��� null ���� �� ����� �������
        RouteD defRoute = getDefaultRoute(listR);

        // �������� ������� ���������� �������� �� ���� ������� (�� �� ��, � ������ �� ������)
        clearGDPData(calculateList);

        // ---- ���������� ������ ��� ������� (BeginCalc) -------
        // ���������� ������ �� ����������� � �� ������� ����� �� �������
        List<DistrTrainGDPCalc> allTrains = new ArrayList<>();
        allTrains.addAll(CalculateUtil.wrapDistrTrains(fixedList, categoryList, true));
        allTrains.addAll(CalculateUtil.wrapDistrTrains(calculateList, categoryList, false));
        Collections.sort(allTrains, new DistrTrainPriorityComparator());

        updateSpTOcc(allTrains, distrEntity);

        // ������ ����� �������� (�����������)
        List<LocomotiveType> locs = distrEntity.getListDistrLoc();

        //String trainIDs = CalculateUtil.buildTrainIDs(calculateList);
        // ��������� ��������� �������
        Map<Integer, DistrSpanGDPCalc> spanGDPCalcMap = new HashMap<>();
        try {
            for (DistrSpan distrSpan : distrEntity.getListSp()) {
                DistrSpanGDPCalc spanGDPCalc = new DistrSpanGDPCalc(distrSpan);
                //spanGDPCalc.initFromDB(allTrains, categoryList, trainIDs, distrEntity.getIdDistr());
                spanGDPCalc.initFromFixed(allTrains);
                spanGDPCalcMap.put(distrSpan.getSpanScale().getIdSpan(), spanGDPCalc);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
            errorList.add(ERR_GDP_CALC);
            return;
        }

        // ��������� ������� ������� + ���������
        List<IntGR> intGRList;
        Map<Integer, DistrStationGDPCalc> stationGDPCalcMap = new HashMap<>();
        try {
            intGRList = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO().getAllIntGR();
            int maxIntCommon = getMaxInt(intGRList);
            IntGR.MAX_INTGR = maxIntCommon;
            for (DistrStation distrSt : distrEntity.getMapSt().values()) {
                DistrStationGDPCalc stGDPCalc = new DistrStationGDPCalc(distrSt);
                //stGDPCalc.initFromDB(allTrains, categoryList, trainIDs, distrEntity.getIdDistr());
                stGDPCalc.initFromFixed(allTrains);
                stationGDPCalcMap.put(distrSt.getIdPoint(), stGDPCalc);
                stGDPCalc.initIntGR();
            }
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
            errorList.add(ERR_GDP_CALC);
            return;
        }

        // �������������� ������� ��������� �� �.�. ��� ���� (��� ������� ����������� ������� ���� ����. ������)
        // �������������� ������� ��������� �� �.�. ��� ������� ������ (��� ������� ����������� ������� ���� ����. ������)

        // ���� �� ������� ... �� ���� ���

        // ---- ������ -------
        // ��� ������� ������
        for (DistrTrainGDPCalc trainGDPCalc : allTrains) {
            if (!trainGDPCalc.isFixed() && !trainGDPCalc.isForeign(distrEntity)) {
                logger.debug("---- calc train: " + trainGDPCalc.getDistrTrain().getCodeTR());
                OE trainOE = trainGDPCalc.getDistrTrain().getOe();
                // todo ����� LocomotiveType �� DistrTrainGDPCalc
                LocomotiveType loc = CalculateUtil.defineLocomotiveType(locs, trainGDPCalc);
                if (loc == null) {
                    logger.error("LocomotiveType undefined for train: " + trainGDPCalc.getDistrTrain());
                    errorList.add(ERR_GDP_CALC);
                    return;
                }
                int off1 = TimeConverter.timeToMinutes(trainGDPCalc.getDistrTrain().getOccupyOff());
                // ��������� ������� ��� ������ ���������� ��������
                RouteD trRoute = getTrainRoute(listR, trainGDPCalc);
                RouteD route = defRoute;
                if (trRoute != null) {
                    route = trRoute;
                }
                // ������ ��������� ��������
                List<RouteDSpan> routeDSpans = route.getListSpan();
                Collections.sort(routeDSpans);
                // ���� ����� ������, ����� ��������������� ������ � ��������� �������
                if (OE.even == trainOE) {
                    Collections.reverse(routeDSpans);
                }
                String tr = trainGDPCalc.getDistrTrain().getCodeTR() + " (" + trainGDPCalc.getDistrTrain().getNameTR() + ")";
                int maxIteration = routeDSpans.size() * CALC_DEPTH + 1;
                logger.debug("-START- train: " + tr + " maxIt = " + maxIteration);
                int indxSp = 0;
                boolean beginCalc = true; // ������� ������ ������� ������
                boolean endSt = false; // ������� ��������� ������� ������
                while (!endSt && maxIteration > 0) {
                    maxIteration--;
                    // ������� ������� �� ��
                    RouteDSpan routeDSpan = routeDSpans.get(indxSp);
                    DistrSpanGDPCalc distrSpanGDP = spanGDPCalcMap.get(routeDSpan.getIdSpan());
                    // ������� �������� �������� ��
                    // st1 - �� (������� �����������)
                    DistrStation st1 = getStationFirstOnTheGo(trainOE, distrSpanGDP.getDistrSpan());
                    DistrStationGDPCalc st1GDP = stationGDPCalcMap.get(st1.getIdPoint());
                    // st2 - �� (������� ��������)
                    DistrStation st2 = getStationLastOnTheGo(trainOE, distrSpanGDP.getDistrSpan());
                    DistrStationGDPCalc st2GDP = stationGDPCalcMap.get(st2.getIdPoint());

                    logger.debug("train: " + tr + " maxIt = " + maxIteration + " indxSp = " + indxSp +
                    " distrSpan = " + distrSpanGDP.getDistrSpan().getSpanScale().getName());

                    int[] varTime = new int[1];
//                    boolean canLeave = true;
//                    if (beginCalc) {
//                        // ���� ���������� ����������� � �� �� ����� off1 ����� ������� ��������� ��������� �����
//                        // �������� ������� � ������� ����� ������� �� ���� �������� ������
                    boolean canLeave = canTrainLeave(intGRList, distrSpanGDP, st1GDP, st2GDP, loc, trainGDPCalc, off1, varTime);
//                    }
                    if (!canLeave) {
                        // ��� ��� ������ ����������� �� �������
                        off1 = varTime[0];
                    } else {
                        // ������ ��������, ��������� ������
                        beginCalc = false;

                        // todo ����������� � ������
                        int idLineSt = 0;
                        // ������� ����� ����������� ������ � �� c �������� ������� �������
                        trainGDPCalc.setLeaveSt(st1GDP, idLineSt, off1, indxSp != 0);
                        // �������� �������
                        trainGDPCalc.onSpan(distrSpanGDP, off1);

                        // ����� �������� �� ��������
                        SpanMove spanMove = calculateSpanMove(distrSpanGDP.getDistrSpan(), loc, trainGDPCalc, st1, st2, off1);
                        boolean stp2 = spanMove.stop2; // ������ �� ���� ��������� �� ��
                        int on2 = spanMove.on2; // ����� �������� �� ��

                        int[] shiftAL = new int[1];
                        int[] shiftAR = new int[1];
                        // �������� ������� � ������� ����� �������� �� ���� �������� ������
                        boolean canArr = canTrainArrive(intGRList,st2GDP, trainGDPCalc, on2, shiftAL, shiftAR);
                        int dtAL = shiftAL[0]; // ����� �������� �� ����� ������ ����� (����)
                        int dtAR = shiftAR[0]; // ����� �������� �� ����� ������� ����� (�����)

                        boolean canStop;
                        int off2 = on2;
                        // ���������� ������� �������
                        int indxSpCurr = indxSp;
                        if (canArr) {
                            // ��������� �� ��������� ������� �� (����� ��������� �������)
                            indxSp++;
                            if (indxSp < routeDSpans.size() && indxSp >= 0) {
                                //lineCount = FieldByName('line').AsInteger; // ����� ����� �� ��������
                                RouteDSpan routeDSpanNext = routeDSpans.get(indxSp);
                                DistrSpanGDPCalc distrSpanGDPNext = spanGDPCalcMap.get(routeDSpanNext.getIdSpan());
                                DistrStation st3 = getStationLastOnTheGo(trainOE, distrSpanGDPNext.getDistrSpan());
                                DistrStationGDPCalc st3GDP = stationGDPCalcMap.get(st3.getIdPoint());
                                // ���� �� �� ���� ������������� ���������, �� ������������ ����� ����������� � ��
                                if (stp2) {
                                    int stopTm = trainGDPCalc.getTrainStop(st2.getIdPoint()); // ����� ������� �� ��
                                    off2 = TimeConverter.addMinutes(on2, stopTm);
                                }
                                // ����������� ����������� �� ������� ��������
                                // �������� ������� � ������� ����� ������� �� ���� �������� ������
                                canLeave = canTrainLeave(intGRList, distrSpanGDPNext, st2GDP, st3GDP, loc, trainGDPCalc, off2, varTime);
                                off2 = varTime[0];
                                endSt = false;
                            } else {
                                canLeave = true;
                                endSt = true;
                            }
                        } else {
                            canLeave = false;
                        }
                        // ����������� ��������� �� �� �� �����
                        canStop = canTrainStopMass(st2GDP, trainGDPCalc);

                        // ��������� �������� �� �������� �� �� � ���������� ���� �������������
                        if (canArr && (canStop || canLeave)) {
                            // ����������� �������
                            offSpan(distrSpanGDP, trainGDPCalc, on2);
                            // ������� ����� �������� �� ��
                            setArriveSt(st2GDP, idLineSt, trainGDPCalc, on2);
                            off1 = off2;
                        } else {
                            boolean needStopSt; // fl
                            // ������� �������� ��������� ����� ����������� � ��������� ������� �������� ��������
                            if (!canArr && dtAL > 0) {
                                endSt = false;
                                do {
                                    indxSp--;
                                    routeDSpan = routeDSpans.get(indxSp);
                                    distrSpanGDP = spanGDPCalcMap.get(routeDSpan.getIdSpan());
                                    st1 = getStationFirstOnTheGo(trainOE, distrSpanGDP.getDistrSpan());
                                    st1GDP = stationGDPCalcMap.get(st1.getIdPoint());
                                    st2 = getStationLastOnTheGo(trainOE, distrSpanGDP.getDistrSpan());
                                    st2GDP = stationGDPCalcMap.get(st2.getIdPoint());

                                    trainGDPCalc.newArr(idLineSt, st1.getIdPoint(), dtAL, false, varTime);
                                    on2 = varTime[0];
                                    trainGDPCalc.newOff(idLineSt, st1.getIdPoint(), dtAL, false, varTime);
                                    off1 = varTime[0];

                                    canArr = canTrainArrive(intGRList, st1GDP, trainGDPCalc, on2, shiftAL, shiftAR);
                                    canLeave = canTrainLeave(intGRList, distrSpanGDP, st1GDP, st2GDP, loc, trainGDPCalc, off1, varTime);
                                } while (indxSp > 0 && canArr && canLeave);

                                // ���� �� ������ ������� ��������� �������� ����� ����������,
                                // �� ���������� ���� ��� ����� ���������
                                if (indxSp > 0) {
                                    needStopSt = true;
                                } else {
                                    // ����� �� ����� ������ ������� ������� ����� ����������� (���������� ��������)
                                    // indxSp �� ���� = 0
                                    needStopSt = false;
                                    trainGDPCalc.newArr(idLineSt, st1.getIdPoint(), dtAL, true, varTime);
                                    trainGDPCalc.newOff(idLineSt, st1.getIdPoint(), dtAL, true, varTime);
                                    off1 = varTime[0];

                                    // ������ ��������� ��������� � ������� (����� �����������)
                                    clearGDPData(trainGDPCalc, spanGDPCalcMap, stationGDPCalcMap);
                                }
                            } else {
                                needStopSt = true;
                            }

                            // �������� ������� ��������� �� ����� �� ���������� �������
                            if (needStopSt) {
                                endSt = false;
                                // ����� ������ (��������� ) = dtAR ��� = diff(on2,off2)
                                int diff;
                                if (!canArr) {
                                    diff = dtAR;
                                } else {
                                    diff = TimeConverter.minutesBetween(on2, off2);
                                }

                                // ������������ �� "������� �������"
                                indxSp = indxSpCurr;
                                routeDSpan = routeDSpans.get(indxSp);
                                distrSpanGDP = spanGDPCalcMap.get(routeDSpan.getIdSpan());
                                st1 = getStationFirstOnTheGo(trainOE, distrSpanGDP.getDistrSpan());
                                st1GDP = stationGDPCalcMap.get(st1.getIdPoint());
                                // ������� ��������� ��������
                                clearSpan(distrSpanGDP, trainGDPCalc);
                                // �������� ��������� �� ������ ������� ��������
                                canStop = canTrainStopMass(st1GDP, trainGDPCalc);

                                while (indxSp > 0 && !canStop) {
                                    // ������� ��������� �������
                                    clearLSTT(st1GDP, trainGDPCalc);
                                    // ��������� �� ���������� �������
                                    indxSp--;
                                    routeDSpan = routeDSpans.get(indxSp);
                                    distrSpanGDP = spanGDPCalcMap.get(routeDSpan.getIdSpan());
                                    st1 = getStationFirstOnTheGo(trainOE, distrSpanGDP.getDistrSpan());
                                    st1GDP = stationGDPCalcMap.get(st1.getIdPoint());
                                    // ������� ��������� ��������
                                    clearSpan(distrSpanGDP, trainGDPCalc);
                                    // �������� ��������� �� ������ ������� ��������
                                    canStop = canTrainStopMass(st1GDP, trainGDPCalc);
                                }

                                if (indxSp > 0) {
                                    // ���������� ������������� ��������� � ������������ �� ����. ��
                                    trainGDPCalc.trainMustStop(st1GDP.getDistrStation().getIdPoint(), diff);

                                    indxSp--;
                                    routeDSpan = routeDSpans.get(indxSp);
                                    distrSpanGDP = spanGDPCalcMap.get(routeDSpan.getIdSpan());
                                    // ������� ��������� ��������
                                    clearSpan(distrSpanGDP, trainGDPCalc);

                                    // ������� ����� ����� ����������� �� ��
                                    st1 = getStationFirstOnTheGo(trainOE, distrSpanGDP.getDistrSpan());
                                    trainGDPCalc.newOff(idLineSt, st1.getIdPoint(), 0, false, varTime);
                                    off1 = varTime[0];
                                } else {
                                    // ����� �� ����� ������ ������� ������� ����� ����������� (���������� �������)
                                    // indxSp ��-���� = 0
                                    trainGDPCalc.newArr(idLineSt, st1.getIdPoint(), diff, true, varTime);
                                    trainGDPCalc.newOff(idLineSt, st1.getIdPoint(), diff, true, varTime);
                                    off1 = varTime[0];
                                }
                                beginCalc = true;
                            }
                        }
                        trainGDPCalc.setEndLeave(st2.getIdPoint(), varTime);
                        //off1 = varTime[0];
                    }
                }
                logger.debug("-END- train: " + tr + " maxIt = " + maxIteration);
                if(!endSt) {
                    logger.error("-SKIP CALC- train: " + tr + " maxIt = " + maxIteration);
                    errorList.add(ERR_GDP_CALC_TRAIN + " " + tr);
                }
            }
        }
    }

    /**
     * �������� ������������������ ������� ��������
     * @param routeD �������
     * @param distrEntity �������
     * @return boolean
     */
    private static boolean checkRouteConsistency(RouteD routeD, DistrEntity distrEntity) {
        for (int i = 0; i < routeD.getListSpan().size() - 2; i++) {
            RouteDSpan routeDSpan = routeD.getListSpan().get(i);
            RouteDSpan routeDSpanNext = routeD.getListSpan().get(i + 1);
            DistrSpan distrSpan = distrEntity.getDistrSpan(routeDSpan.getIdSpan());
            DistrSpan distrSpanNext = distrEntity.getDistrSpan(routeDSpanNext.getIdSpan());
            if (distrSpan.getStE().getIdPoint() != distrSpanNext.getStB().getIdPoint()) {
                return false;
            }
        }
        return true;
    }

    /**
     * �������� ��������� ��������� ��� ������� ������
     * @param allTrains
     * @param distrEntity
     */
    private static void updateSpTOcc(List<DistrTrainGDPCalc> allTrains, DistrEntity distrEntity) {
        for (DistrTrainGDPCalc trainGDPCalc : allTrains) {
            DistrTrain distrTrain = trainGDPCalc.getDistrTrain();
            Map<Integer, SpTOcc> spTOccMap = distrTrain.getMapSpTOcc();
            //spTOccMap.clear();

            for (DistrSpan distrSpan : distrEntity.getListSp()) {
                int idStB = distrSpan.getSpanScale().getIdSt();
                int idStE = distrSpan.getSpanScale().getIdStE();
                LineStTrain lstB = distrTrain.getMapLineStTr().get(idStB);
                LineStTrain lstE = distrTrain.getMapLineStTr().get(idStE);
                if (lstB != null && lstE != null) {
                    String tOff1 = lstB.getOccupyOff();
                    String tOn2 = lstE.getOccupyOn();
                    if (tOff1 != null && tOn2 != null && !tOff1.isEmpty() && !tOn2.isEmpty()) {
                        int mOff1 = TimeConverter.timeToMinutes(tOff1);
                        int mOn2 = TimeConverter.timeToMinutes(tOn2);

                        int idSpan = distrSpan.getSpanScale().getIdSpan();
                        SpTOcc spTOcc = new SpTOcc();

                        spTOcc.setIdSpan(idSpan);
                        spTOcc.setIdTrain(distrTrain.getIdTrain());
                        spTOcc.setIdDistr(distrTrain.getIdDistr());
                        spTOcc.setOe(distrTrain.getOe().getString());
                        spTOcc.setOccupyOn(tOff1);
                        spTOcc.setOccupyOff(tOn2);

                        // NextDay 0 ���� ��� �������� �� ����� ����� �� ���� ��������, 1 ���� ����
                        int nextDay = mOff1 < mOn2 ? 0 : 1;
                        spTOcc.setNextDay(nextDay);
                        int tmove = TimeConverter.minutesBetween(mOff1, mOn2);
                        spTOcc.setTmove(tmove);

                        spTOccMap.put(idSpan, spTOcc);
                    }
                }
            }
        }
    }

    private static int getMaxInt(List<IntGR> intGRList) {
        int val = 0;
        for (IntGR intGR : intGRList) {
            if (intGR.getDefaultValue() > val) {
                val = intGR.getDefaultValue();
            }
        }
        return val;
    }

    /**
     * ����� �������� �� �������� � ������ ���������
     * @return SpanMove
     */
    private static SpanMove calculateSpanMove(DistrSpan distrSpan, LocomotiveType loc, DistrTrainGDPCalc trainGDPCalc,
                                              DistrStation st1, DistrStation st2, int off1)
    {
        // ����� ����� ����, �������, ���������� �� �������� ��� ������
        Time trainTime = CalculateUtil.getTrainTime(distrSpan, loc, trainGDPCalc.getDistrTrain(), true);
        boolean stp1 = trainGDPCalc.isStop(st1.getIdPoint()); // ���� �� ��������� �� ��
        boolean stp2 = trainGDPCalc.mustStop(st2.getIdPoint()); // ������ �� ���� ��������� �� ��
        float tMove = getTimeMove(trainTime, stp1, stp2);
        // todo: ��� ����������� ������� �������� ����� ������� �� �.�.
        // �������� ����� �������� �� ��
        int on2 = TimeConverter.addMinutes(off1, (int) tMove);

        SpanMove spanMove = new SpanMove();
        spanMove.on2 = on2;
        spanMove.stop1 = stp1;
        spanMove.stop2 = stp2;
        return spanMove;
    }

    private static DistrStation getStationFirstOnTheGo(OE trainOE, DistrSpan distrSpan) {
        if (OE.odd == trainOE) {
            return distrSpan.getStB();
        } else {
            return distrSpan.getStE();
        }
    }

    private static DistrStation getStationLastOnTheGo(OE trainOE, DistrSpan distrSpan) {
        if (OE.odd == trainOE) {
            return distrSpan.getStE();
        } else {
            return distrSpan.getStB();
        }
    }

    private static void clearLSTT(DistrStationGDPCalc st, DistrTrainGDPCalc train) {
        st.removeTrain(train);
        int idSt = st.getDistrStation().getIdPoint();
        train.getTimeStCalcMap().remove(idSt);
        LineStTrain lineStTrain = train.getDistrTrain().getMapLineStTr().get(idSt);
        if (lineStTrain.getStop() != 1) {
            train.getDistrTrain().getMapLineStTr().remove(idSt);
        } else {
            lineStTrain.setOccupyOn(null);
            lineStTrain.setOccupyOff(null);
        }
    }

    private static void clearSpan(DistrSpanGDPCalc span, DistrTrainGDPCalc train) {
        span.removeTrain(train);
        int idSpan = span.getDistrSpan().getSpanScale().getIdSpan();
        train.getTimeSpCalcMap().remove(idSpan);
        train.getDistrTrain().getMapSpTOcc().remove(idSpan);
    }

    private static void setArriveSt(DistrStationGDPCalc st, int idLineSt, DistrTrainGDPCalc trainGDPCalc, int on) {
        int idSt = st.getDistrStation().getIdPoint();
        LineStTrain lineStTrain = new LineStTrain();
        lineStTrain.setIdSt(idSt);
        lineStTrain.setIdLineSt(idLineSt);
        lineStTrain.setIdTrain(trainGDPCalc.getDistrTrain().getIdTrain());
        lineStTrain.setOccupyOn(TimeConverter.minutesToTime(on));
        trainGDPCalc.getDistrTrain().getMapLineStTr().put(idSt, lineStTrain);

        TimeStCalc timeStCalc = new TimeStCalc(lineStTrain);
        trainGDPCalc.getTimeStCalcMap().put(idSt, timeStCalc);

        st.onStation(idSt, trainGDPCalc);
    }

    private static void offSpan(DistrSpanGDPCalc distrSpan, DistrTrainGDPCalc trainGDPCalc, int tm) {
        int idSpan = distrSpan.getDistrSpan().getSpanScale().getIdSpan();
        TimeSpCalc timeSpCalc = trainGDPCalc.getTimeSpCalcMap().get(idSpan);
        timeSpCalc.setLeaveSp(tm);
    }

    private static boolean canTrainStopMass(DistrStationGDPCalc st, DistrTrainGDPCalc trainGDPCalc) {
        int mass = trainGDPCalc.getMass();
        int maxMass;
        if (OE.odd == trainGDPCalc.getDistrTrain().getOe()) {
            maxMass = st.getDistrStation().getStation().getMaxMassO();
        } else {
            maxMass = st.getDistrStation().getStation().getMaxMassE();
        }
        return mass <= maxMass;
    }

    /**
     * �������� �� �������� trainGDPCalc �� st2
     */
    private static boolean canTrainArrive(List<IntGR> intGRList, DistrStationGDPCalc st2,
                                          DistrTrainGDPCalc trainGDPCalc, int on2, int[] shiftAL, int[] shiftAR) {
        shiftAL[0] = 0; // ����� �����/�����
        shiftAR[0] = 0; // ����� �������/������

        int[] varTime = new int[1];

        // ---------- �������� ���������� ------------------
        // ����� ����� ������������� �� ��������� ����������
        int maxInt = st2.getMaxInt(intGRList);
        // ������� ��������� ��������� ����� �����, ���������� � ������������ �������� �� ������� ��������
        DistrTrainGDPCalc earlierTrainOn = st2.getEarlierTrain(on2, maxInt, true, null);
        if (earlierTrainOn != null && earlierTrainOn != trainGDPCalc) {
            // �������� �����
            if (trainGDPCalc.getDistrTrain().getOe() == earlierTrainOn.getDistrTrain().getOe()) {
                // �������� "��������� ��������"
                int idSt = st2.getDistrStation().getIdPoint();
                TimeStCalc earlieTime = earlierTrainOn.getTimeStCalcMap().get(idSt);
                TimeStCalc timeCopy = new TimeStCalc(on2, on2);
                int intGR = st2.checkPassingArrive(intGRList, earlieTime, timeCopy);
                if (intGR > 0) {
                    int diff = TimeConverter.minutesBetween(earlieTime.getmOn(), timeCopy.getmOn());
                    shiftAR[0] = intGR - diff;
                    logger.debug("station = " + st2.getDistrStation().getNamePoint());
                    //�������� '��������� ��������'
                    logger.debug("interval of 'passing arrival'");
                    logger.debug("conflict arrive: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                            " - earlierTrainOn:" + earlierTrainOn.getDistrTrain().getCodeTR());
                    logger.debug("shiftAR = " + shiftAR[0]);
                    return false;
                }
            } else {
                // �������� "���������������� �������� ������� ��������������� �����������"
                int idSt = st2.getDistrStation().getIdPoint();
                TimeStCalc earlieTime = earlierTrainOn.getTimeStCalcMap().get(idSt);
                TimeStCalc timeCopy = new TimeStCalc(on2, on2);
                int intGR = st2.checkNonSimultaneousOppositeDirection(intGRList, earlieTime, timeCopy);
                if (intGR > 0) {
                    int diff = TimeConverter.minutesBetween(earlieTime.getmOn(), timeCopy.getmOn());
                    shiftAR[0] = intGR - diff;
                    logger.debug("station = " + st2.getDistrStation().getNamePoint());
                    //�������� '���������������� �������� ������� ��������������� �����������'
                    logger.debug("interval of 'non-simultaneous arrival of trains of opposite directions'");
                    logger.debug("conflict arrive: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                            " - earlierTrainOn:" + earlierTrainOn.getDistrTrain().getCodeTR());
                    logger.debug("shiftAR = " + shiftAR[0]);
                    return false;
                }
            }
        }

        // ������� ��������� ������������� ����� �����, ���������� � ������������ �������� �� ������� ��������
        DistrTrainGDPCalc earlierTrainOff = st2.getEarlierTrain(on2, maxInt, false, null);
        if (earlierTrainOff != null && trainGDPCalc!= earlierTrainOff &&
                trainGDPCalc.getDistrTrain().getOe() == earlierTrainOff.getDistrTrain().getOe())
        {
            // "���������������� ����������� � ��������� ��������"
            int idSt = st2.getDistrStation().getIdPoint();
            TimeStCalc earlieTime = earlierTrainOff.getTimeStCalcMap().get(idSt);
            TimeStCalc timeCopy = new TimeStCalc(on2, on2);
            int intGR = st2.checkNonSimultaneousDeparturePassingArrive(intGRList, earlieTime, timeCopy);
            if (intGR > 0) {
                int diff = TimeConverter.minutesBetween(earlieTime.getmOn(), timeCopy.getmOn());
                shiftAR[0] = intGR - diff;
                logger.debug("station = " + st2.getDistrStation().getNamePoint());
                //�������� '���������������� ����������� � ��������� ��������'
                logger.debug("interval of 'non-simultaneous departure and passing arrival'");
                logger.debug("conflict arrive: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                        " - earlierTrainOff:" + earlierTrainOff.getDistrTrain().getCodeTR());
                logger.debug("shiftAR = " + shiftAR[0]);
                return false;
            }
        }

        // ������� ��������� ��������� ������� �����, ���������� � ������������ �������� �� ������� ��������
        DistrTrainGDPCalc laterTrainOn = st2.getLaterTrain(on2, maxInt, true, null);
        if (laterTrainOn != null && laterTrainOn != trainGDPCalc) {
            // �������� �����
            if (trainGDPCalc.getDistrTrain().getOe() == laterTrainOn.getDistrTrain().getOe()) {
                // �������� "��������� ��������"
                int idSt = st2.getDistrStation().getIdPoint();
                TimeStCalc laterTime = laterTrainOn.getTimeStCalcMap().get(idSt);
                TimeStCalc timeCopy = new TimeStCalc(on2, on2);
                int intGR = st2.checkPassingArrive(intGRList, timeCopy, laterTime);
                if (intGR > 0) {
                    int diff = TimeConverter.minutesBetween(timeCopy.getmOn(), laterTime.getmOn());
                    intGR = st2.getPassingArrive(intGRList);
                    shiftAR[0] = diff + intGR;
                    logger.debug("station = " + st2.getDistrStation().getNamePoint());
                    //�������� '��������� ��������'
                    logger.debug("interval of 'passing arrival'");
                    logger.debug("conflict arrive: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                            " - laterTrainOn:" + laterTrainOn.getDistrTrain().getCodeTR());
                    logger.debug("shiftAR = " + shiftAR[0]);
                    return false;
                }
            } else {
                // �������� "���������������� �������� ������� ��������������� �����������"
                int idSt2 = st2.getDistrStation().getIdPoint();
                TimeStCalc laterTime2 = laterTrainOn.getTimeStCalcMap().get(idSt2);
                TimeStCalc timeCopy = new TimeStCalc(on2, on2);
                int intGR = st2.checkNonSimultaneousOppositeDirection(intGRList, timeCopy, laterTime2);
                if (intGR > 0) {
                    int diff = TimeConverter.minutesBetween(timeCopy.getmOn(), laterTime2.getmOn());
                    intGR = st2.getNonSimultaneousOppositeDirection(intGRList);
                    shiftAR[0] = diff + intGR;
                    logger.debug("station = " + st2.getDistrStation().getNamePoint());
                    //�������� '���������������� �������� ������� ��������������� �����������'
                    logger.debug("interval of 'non-simultaneous arrival of trains of opposite directions'");
                    logger.debug("conflict arrive: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                            " - laterTrainOn:" + laterTrainOn.getDistrTrain().getCodeTR());
                    logger.debug("shiftAR = " + shiftAR[0]);
                    return false;
                }
            }
        }

        // ������� ��������� ������������� ������� �����, ���������� � ������������ �������� �� ������� ��������
        DistrTrainGDPCalc laterTrainOff = st2.getLaterTrain(on2, maxInt, false, null);
        if (laterTrainOff != null && trainGDPCalc!= laterTrainOff &&
                trainGDPCalc.getDistrTrain().getOe() == laterTrainOff.getDistrTrain().getOe())
        {
            // "���������������� �������� � ��������� �����������"
            int idSt = st2.getDistrStation().getIdPoint();
            TimeStCalc laterTime = laterTrainOff.getTimeStCalcMap().get(idSt);
            TimeStCalc timeCopy = new TimeStCalc(on2, on2);
            int intGR = st2.checkNonSimultaneousArrivePassingDeparture(intGRList, timeCopy, laterTime);
            if (intGR > 0) {
                int diff = TimeConverter.minutesBetween(timeCopy.getmOn(), laterTime.getmOff());
                intGR = st2.getNonSimultaneousDeparturePassingArrive(intGRList);
                shiftAR[0] = diff + intGR;
                logger.debug("station = " + st2.getDistrStation().getNamePoint());
                //�������� '���������������� �������� � ��������� �����������'
                logger.debug("interval of 'non-simultaneous arrival and passing departure'");
                logger.debug("conflict arrive: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                        " - laterTrainOff:" + laterTrainOff.getDistrTrain().getCodeTR());
                logger.debug("shiftAR = " + shiftAR[0]);
                return false;
            }
        }
        return true;
    }

    /**
     * �������� ����������� ����������� �� �������
     * @param  varTime  ����� ����� �����������
     * @return boolean
     */
    private static boolean canTrainLeave(
            List<IntGR> intGRList, DistrSpanGDPCalc spanGDPCalc,
            DistrStationGDPCalc st1, DistrStationGDPCalc st2, LocomotiveType loc,
            DistrTrainGDPCalc trainGDPCalc, int off, int[] varTime)
    {
        varTime[0] = off;
        SpanMove spanMove = calculateSpanMove(spanGDPCalc.getDistrSpan(), loc, trainGDPCalc,
                st1.getDistrStation(), st2.getDistrStation(), off);

        // ---------- �������� ��������� �������� ----------
        int lineCount = spanGDPCalc.getLineCount(off, spanMove.on2, varTime); // ���������� ���-�� ��������� �����, � ������ "����"
        if (lineCount <= 0) {
            // ���� �� �������� ��� ����� ��� ����������, �� ������������ ������, � varTime ������������ ����� ����
            logger.debug("lineCount=0 " + spanGDPCalc.getDistrSpan().getSpanScale().getName() +
                    " train " + trainGDPCalc.getDistrTrain().getCodeTR());
            return false;
        }

        // ---------- ���� ���� ------------------
        // ����� �������� �� �� - spanMove.on2

        // ---------- �������� ��������� ������� �� ��������� -----------
        if (lineCount == 1) {
            // ��������� ����� ������������� �� ������� c s2 �� �������� �������� ������ �� s2
            DistrTrainGDPCalc earlierTrainOff = st2.getEarlierTrain(spanMove.on2, 0,false, trainGDPCalc.getDistrTrain().getOe());
            if (earlierTrainOff != null){
                int tr1St1Off = off;
                int tr1St2On = spanMove.on2;
                int tr2St2Off = earlierTrainOff.getTimeStCalcMap().get(st2.getDistrStation().getIdPoint()).getmOff();
                int tr2St1On = earlierTrainOff.getTimeStCalcMap().get(st1.getDistrStation().getIdPoint()).getmOn();
                if (!checkCross�ounter(tr1St1Off, tr1St2On, tr2St2Off, tr2St1On)) {
                    earlierTrainOff = null;
                }
            }
            if (earlierTrainOff != null && trainGDPCalc != earlierTrainOff ) {
                // ���� �� ���������� �������� � ��������� ���������� ���� ��� �����, �� ������������ ������,
                // � varTime ������������ ��������� ����� ����������� = ����� ������������ �������� + ��������
                int idSt = st1.getDistrStation().getIdPoint();
                TimeStCalc timeStCalc = earlierTrainOff.getTimeStCalcMap().get(idSt);
                // �������� "���������"
                int intGR = st1.getCross(intGRList);
                varTime[0] = timeStCalc.getmOn() + intGR;
                logger.debug("station = " + st1.getDistrStation().getNamePoint());
                //�������� '���������'
                logger.debug("interval 'crossing'");
                logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                        " - conflictTrain:" + earlierTrainOff.getDistrTrain().getCodeTR());
                logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                return false;
            }
        }

        // ---------- �������� ���������� ------------------
        // ����� ����� ������������� �� ��������� ����������
        int maxInt = st1.getMaxInt(intGRList);
        // ������� ��������� ��������� ����� �����, ���������� � ������������ �������� �� ������� �����������
        DistrTrainGDPCalc earlierTrainOn = st1.getEarlierTrain(off, maxInt, true, null);
        // ���� ��� �������� �����
        if (earlierTrainOn != null && trainGDPCalc != earlierTrainOn &&
                earlierTrainOn.getDistrTrain().getOe().equals(trainGDPCalc.getDistrTrain().getOe()))
        {
            // �������� "���������������� �������� � ��������� �����������"
            int idSt = st1.getDistrStation().getIdPoint();
            TimeStCalc earlieTime = earlierTrainOn.getTimeStCalcMap().get(idSt);
            TimeStCalc time = trainGDPCalc.getTimeStCalcMap().get(idSt);
            TimeStCalc timeCopy;
            if (time != null) {
                timeCopy = new TimeStCalc(time.getmOn(), off);
            } else {
                timeCopy = new TimeStCalc(off, off);
            }
            int intGR = st1.checkNonSimultaneousArrivePassingDeparture(intGRList, earlieTime, timeCopy);
            if (intGR > 0) {
                int diff = TimeConverter.minutesBetween(earlieTime.getmOn(), timeCopy.getmOff());
                varTime[0] = TimeConverter.addMinutes(off, intGR - diff);
                logger.debug("station = " + st1.getDistrStation().getNamePoint());
                //�������� '���������������� �������� � ��������� �����������'
                logger.debug("interval of 'non-simultaneous arrival and passing departure'");
                logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                        " - earlierTrainOn:" + earlierTrainOn.getDistrTrain().getCodeTR());
                logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                return false;
            }
        }

        // ������� ��������� ������������� ����� �����, ���������� � ������������ �������� �� ������� �����������
        DistrTrainGDPCalc earlierTrainOff = st1.getEarlierTrain(off, maxInt, false, null);
        // ���� ��� �������� �����
        if (earlierTrainOff != null && trainGDPCalc != earlierTrainOff &&
                earlierTrainOff.getDistrTrain().getOe().equals(trainGDPCalc.getDistrTrain().getOe()))
        {
            // ���������, ��� ��� �� ������������
            // (��� ��� ����� ���������� � st1 ����� � �������� �� st2)
            int idSpan = spanGDPCalc.getDistrSpan().getSpanScale().getIdSpan();
            TimeSpCalc timeSpCalc = earlierTrainOff.getTimeSpCalcMap().get(idSpan);
            if (timeSpCalc != null) {
                int off1E = timeSpCalc.getmOn();
                int on2E = timeSpCalc.getmOff();
                boolean cross = checkCrossPassing(off1E, on2E, off, spanMove.on2);
                if (cross) {
                    // ����� ����� ����������� = ������� �������� + ��������
                    int intGR = st2.getPassingArrive(intGRList);
                    int diff = TimeConverter.minutesBetween(spanMove.on2, on2E);
                    varTime[0] = TimeConverter.addMinutes(off, diff + intGR);
                    logger.debug("station1 = " + st1.getDistrStation().getNamePoint());
                    //������� ������������
                    logger.debug("are in passing crossed");
                    logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                            " - conflictTrain:" + earlierTrainOff.getDistrTrain().getCodeTR());
                    logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                    return false;
                }
            }

            // �������� "��������� �����������"
            int idSt = st1.getDistrStation().getIdPoint();
            TimeStCalc earlieTime = earlierTrainOff.getTimeStCalcMap().get(idSt);
            TimeStCalc time = trainGDPCalc.getTimeStCalcMap().get(idSt);
            TimeStCalc timeCopy;
            if (time != null) {
                timeCopy = new TimeStCalc(time.getmOn(), off);
            } else {
                timeCopy = new TimeStCalc(off, off);
            }
            int intGR = st1.checkPassingDeparture(intGRList, earlierTrainOff, trainGDPCalc, earlieTime, timeCopy);
            if (intGR > 0) {
                int diff = TimeConverter.minutesBetween(earlieTime.getmOff(), timeCopy.getmOff());
                varTime[0] = TimeConverter.addMinutes(off, intGR - diff);
                logger.debug("station = " + st1.getDistrStation().getNamePoint());
                //�������� '��������� �����������'
                logger.debug("interval of 'passing departure'");
                logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                        " - earlierTrainOff:" + earlierTrainOff.getDistrTrain().getCodeTR());
                logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                return false;
            }
        }

        // ������� ��������� ��������� ������� �����, ���������� � ������������ �������� �� ������� �����������
        DistrTrainGDPCalc laterTrainOn = st1.getLaterTrain(off, maxInt, true, null);
        if (laterTrainOn != null && trainGDPCalc != laterTrainOn) {
            int idSt = st1.getDistrStation().getIdPoint();
            TimeStCalc laterTime = laterTrainOn.getTimeStCalcMap().get(idSt);
            TimeStCalc time = trainGDPCalc.getTimeStCalcMap().get(idSt);
            TimeStCalc timeCopy;
            if (time != null) {
                timeCopy = new TimeStCalc(time.getmOn(), off);
            } else {
                timeCopy = new TimeStCalc(off, off);
            }
            // ���� ��� �������� �����
            if (laterTrainOn.getDistrTrain().getOe().equals(trainGDPCalc.getDistrTrain().getOe())) {
                // "���������������� ����������� � ��������� ��������"
                int intGR = st1.checkNonSimultaneousDeparturePassingArrive(intGRList, timeCopy, laterTime);
                if (intGR > 0) {
                    // ����� ����� ����������� = ����� �������� laterTrainOn + ��������
                    // �������� "���������������� �������� � ��������� �����������"
                    intGR = st1.getNonSimultaneousArrivePassingDeparture(intGRList);
                    varTime[0] = TimeConverter.addMinutes(laterTime.getmOff(), intGR);
                    logger.debug("station = " + st1.getDistrStation().getNamePoint());
                    //�������� '���������������� ����������� � ��������� ��������'
                    logger.debug("interval of 'non-simultaneous departure and passing arrival'");
                    logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                            " - laterTrainOn:" + laterTrainOn.getDistrTrain().getCodeTR());
                    logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                    return false;
                }
            } else {
                if (lineCount == 1) {
                    if (timeCopy.getmOn() != off && laterTime.getmOn() != laterTime.getmOff()) {
                        // �������� �� �������� "���������������� ���������"
                        int intGR = st1.checkNonStopCross(intGRList, timeCopy, laterTime);
                        if (intGR > 0) {
                            // ����� ����� ����������� = laterTime.getmOff()
                            varTime[0] = laterTime.getmOff();
                            logger.debug("station = " + st1.getDistrStation().getNamePoint());
                            //�������� '���������������� ���������'
                            logger.debug("interval of 'unceasing(nonstop) crossing'");
                            logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                                    " - laterTrainOn:" + laterTrainOn.getDistrTrain().getCodeTR());
                            logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                            return false;
                        }
                    } else {
                        // ��� "���������"
                        int intGR = st1.checkCross(intGRList, timeCopy, laterTime);
                        if (intGR > 0) {
                            // ����� ����� ����������� = laterTime.getmOff()
                            varTime[0] = laterTime.getmOff();
                            logger.debug("station = " + st1.getDistrStation().getNamePoint());
                            //�������� '���������'
                            logger.debug("interval of 'crossing'");
                            logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                                    " - laterTrainOn:" + laterTrainOn.getDistrTrain().getCodeTR());
                            logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                            return false;
                        }
                    }
                }
            }
        }

        // ������� ��������� ������������� ������� �����, ���������� � ������������ �������� �� ������� �����������
        DistrTrainGDPCalc laterTrainOff = st1.getLaterTrain(off, maxInt, false, null);
        // ���� ��� �������� �����
        if (laterTrainOff != null && trainGDPCalc!= laterTrainOff &&
                laterTrainOff.getDistrTrain().getOe().equals(trainGDPCalc.getDistrTrain().getOe()))
        {
            // ���������, ��� ��� �� ������������
            // (��� ��� ������� ���������� � st1 ������� � �������� �� st2)
            int idSpan = spanGDPCalc.getDistrSpan().getSpanScale().getIdSpan();
            TimeSpCalc timeSpCalc = laterTrainOff.getTimeSpCalcMap().get(idSpan);
            if (timeSpCalc != null) {
                int off1L = timeSpCalc.getmOn();
                int on2L = timeSpCalc.getmOff();
                boolean cross = checkCrossPassing(off, spanMove.on2, off1L, on2L);
                if (cross) {
                    // ����� ����� ����������� = ����� ����������� ����� �������� + ��������
                    // �������� "��������� �����������"
                    int idSt = st1.getDistrStation().getIdPoint();
                    TimeStCalc laterTime = laterTrainOff.getTimeStCalcMap().get(idSt);
                    TimeStCalc time = trainGDPCalc.getTimeStCalcMap().get(idSt);
                    TimeStCalc timeCopy;
                    if (time != null) {
                        timeCopy = new TimeStCalc(time.getmOn(), off);
                    } else {
                        timeCopy = new TimeStCalc(off, off);
                    }
                    int intGR = st1.getPassingDeparture(
                            intGRList,
                            trainGDPCalc.getDistrTrain().getTypeTR(),
                            laterTrainOff.getDistrTrain().getTypeTR(),
                            timeCopy.getmOn() != timeCopy.getmOff(),
                            laterTime.getmOn() != laterTime.getmOff());

                    varTime[0] = TimeConverter.addMinutes(off1L, intGR);
                    logger.debug("station1 = " + st1.getDistrStation().getNamePoint());
                    //������� ������������
                    logger.debug("are in passing crossed");
                    logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                            " - laterTrainOff:" + laterTrainOff.getDistrTrain().getCodeTR());
                    logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                    return false;
                }
            }

            // �������� "��������� �����������"
            int idSt = st1.getDistrStation().getIdPoint();
            TimeStCalc laterTime = laterTrainOff.getTimeStCalcMap().get(idSt);
            TimeStCalc time = trainGDPCalc.getTimeStCalcMap().get(idSt);
            TimeStCalc timeCopy;
            if (time != null) {
                timeCopy = new TimeStCalc(time.getmOn(), off);
            } else {
                timeCopy = new TimeStCalc(off, off);
            }
            int intGR = st1.checkPassingDeparture(intGRList, trainGDPCalc, laterTrainOff, timeCopy, laterTime);
            if (intGR > 0) {
                varTime[0] = TimeConverter.addMinutes(laterTime.getmOff(), intGR);
                logger.debug("station = " + st1.getDistrStation().getNamePoint());
                //�������� '��������� �����������'
                logger.debug("interval of 'passing departure'");
                logger.debug("conflict leave: " + trainGDPCalc.getDistrTrain().getCodeTR() +
                        " - laterTrainOff:" + laterTrainOff.getDistrTrain().getCodeTR());
                logger.debug("new off = " + TimeConverter.minutesToTime(varTime[0]));
                return false;
            }
        }
        return true;
    }

    private static boolean checkCross�ounter(int tr1St1Off, int tr1St2On, int tr2St2Off, int tr2St1On) {
        // ����� ���������, ��� tr1St1Off < tr2St1On
        // ��� �� ��������� �� ����. �����
        if (tr1St1Off <= tr1St2On && tr2St2Off <= tr2St1On) {
            return tr1St1Off <= tr2St1On && tr1St2On >= tr2St2Off;
        }
        // ���� ��� ��������� �� ����. �����
        if (tr1St1Off > tr1St2On && tr2St2Off > tr2St1On) {
            return true;
        }
        // ���� ������ ��������� �� ����. �����, � ������ ���
        if (tr1St1Off > tr1St2On && tr2St2Off <= tr2St1On) {
            if (tr1St1Off <= tr2St1On && tr2St2Off > tr1St2On) {
                return true;
            }
            if (tr1St1Off > tr2St1On && tr2St2Off <= tr1St2On) {
                return true;
            }
        }
        // ���� ������ ��������� �� ����. �����, � ������ ���
        if (tr1St1Off <= tr1St2On && tr2St2Off > tr2St1On) {
            if (tr1St1Off > tr2St1On && tr2St2Off <= tr1St2On) {
                return true;
            }
            if (tr1St1Off <= tr2St1On && tr2St2Off > tr1St2On) {
                return true;
            }
        }
        return false;
    }


    /**
     * �������� ��� ������������ �������� ������
     * @param off1E ����������� �� ������� ����� ������� ������
     * @param on2E  �������� � �������� ����� ������� ������
     * @param off1 ����������� �� ������� ����� �������� ������
     * @param on2 �������� � �������� ����� �������� ������
     * @return true ���� ������������
     */
    private static boolean checkCrossPassing(int off1E, int on2E, int off1, int on2) {
        // 1  - ���� ��� �� ��������� �� ���� �����
        // ���
        // 2  - ���� ��� ��������� �� ���� �����
        if (on2E > off1E && on2 > off1 || on2E > off1E && on2 > off1) {
            return off1 >= off1E && on2 <= on2E;
        }
        // 3 - ������ ����� ������ ��������� �� ���� �����
        if (on2E < off1E && on2 > off1) {
            return off1 >= off1E;
        }
        // 4 - ������ ����� ������� ��������� �� ���� �����
        if (on2E > off1E && on2 < off1) {
            return on2 <= on2E;
        }
        return false;
    }

    private static RouteD getTrainRoute(List<RouteD> listR, DistrTrainGDPCalc trainGDPCalc) {
        int codeTR = trainGDPCalc.getDistrTrain().getCodeTRInt();
        for (RouteD routeD : listR) {
            for (RouteDTrain routeDTrain : routeD.getListTR()) {
                if (codeTR <= routeDTrain.getTrMax() && codeTR >= routeDTrain.getTrMin()) {
                    return routeD;
                }
            }
        }
        return null;
    }

    private static RouteD getDefaultRoute(List<RouteD> listR) {
        for (RouteD routeD : listR) {
            if (routeD.getTypeR() == 0) {
                return routeD;
            }
        }
        return null;
    }

    /**
     * �� ������� ������ ���� ������ 1 �������� ������� (TypeR = 0)
     *
     * @param listR ������ RouteD
     * @return boolean
     */
    private static boolean checkForDefaultRoute(List<RouteD> listR) {
        int countDefault = 0;
        for (RouteD routeD : listR) {
            if (routeD.getTypeR() == 0) {
                countDefault++;
            }
        }
        return countDefault == 1;
    }

    /**
     * @param distrTrains
     */
    private static void clearGDPData(Collection<DistrTrain> distrTrains) {
        Iterator<DistrTrain> it = distrTrains.iterator();
        while (it.hasNext()) {
            DistrTrain distrTrain = it.next();
            if (!distrTrain.getShowParam().isHidden()) {
                clearGDPData(distrTrain);
            } else {
                it.remove();
            }
        }
    }

    private static void clearGDPData(DistrTrain distrTrain) {
        // ������� ��������� ��������� �� �������
        distrTrain.getMapSpTOcc().clear();
        // ������� ������� �� �.�.
        distrTrain.getMapTrainStop().clear();
        // ������� ��������� ����������� ����� �� �������
        List<Integer> stIDToClear = new ArrayList<>();
        Map<Integer, LineStTrain> lineStTrainMap = distrTrain.getMapLineStTr();
        for (Map.Entry<Integer, LineStTrain> entry : lineStTrainMap.entrySet()) {
            LineStTrain lineStTrain = entry.getValue();
            // ��� �������� �������
            if (lineStTrain.getTstopPL() > 0) {
                lineStTrain.setStop(1);
                lineStTrain.setIdLineSt(0);
                lineStTrain.setOccupyOn(null);
                lineStTrain.setOccupyOff(null);
                lineStTrain.setTstop(lineStTrain.getTstopPL());
            } else {
                stIDToClear.add(entry.getKey());
            }
        }
        for (Integer key : stIDToClear) {
            lineStTrainMap.remove(key);
        }
    }

    private static void clearGDPData(DistrTrainGDPCalc trainGDPCalc, Map<Integer, DistrSpanGDPCalc> spanGDPCalcMap, Map<Integer, DistrStationGDPCalc> stationGDPCalcMap) {
    }

}
