package com.gmail.alaerof.components.mainframe.gdp.action.listst;

import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.TableModel;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class TrainScheduleTable extends JTable {
    private static final long serialVersionUID = 1L;

    public TrainScheduleTable() {
        super();
    }

    public TrainScheduleTable(TableModel dm) {
        super(dm);
        // �������������� � ������� �� ������ �����
        for (int i = 0; i < this.getColumnModel().getColumnCount(); i++) {
            DefaultCellEditor defaultEditor = (DefaultCellEditor) this.getDefaultEditor(this
                    .getColumnClass(i));
            defaultEditor.setClickCountToStart(1);
        }
    }
    

    /**
     * ��������� ������ � ������ � ������ ��������������
     */
    private void selectCellText(int row, int col) {
        TimeStCellEditor ed = (TimeStCellEditor) this.getCellEditor(row, col);
        if (ed != null) {
            final JTextField tf = ed.getTextField();
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    tf.grabFocus();
                    tf.selectAll();
                }
            });
        }
    }

    @Override
    public boolean editCellAt(int row, int col, EventObject e) {
        boolean res = super.editCellAt(row, col, e);
        if (res) {
            selectCellText(row, col);
        }
        return res;
    }

    @Override
    public boolean editCellAt(int row, int col) {
        boolean res = super.editCellAt(row, col);
        if (res) {
            selectCellText(row, col);
        }
        return res;
    }

    public void moveFocus(int row, int col) {
        changeSelection(row, col, false, false);
        editCellAt(row, col);
    }

}
