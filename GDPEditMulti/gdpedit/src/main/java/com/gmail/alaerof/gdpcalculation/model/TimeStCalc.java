package com.gmail.alaerof.gdpcalculation.model;

import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.util.TimeConverter;

public class TimeStCalc implements Cloneable {
    private LineStTrain lineStTrain;
    private int mOn;
    private int mOff;

    public TimeStCalc(LineStTrain lineStTrain) {
        this.lineStTrain = lineStTrain;
        if (lineStTrain.getOccupyOn() == null) {
            this.mOn = -1;
        } else {
            this.mOn = TimeConverter.timeToMinutes(lineStTrain.getOccupyOn());
        }
        if (lineStTrain.getOccupyOff() == null) {
            this.mOff = mOn;
        } else {
            this.mOff = TimeConverter.timeToMinutes(lineStTrain.getOccupyOff());
        }
    }

    public  TimeStCalc (int mOn, int mOff){
        this.mOn = mOn;
        this.mOff = mOff;
    }

    public int getmOn() {
        return mOn;
    }

    public int getmOff() {
        return mOff;
    }

    public void setLeaveSt(int mOff, int idLineSt, boolean calcTStop) {
        this.mOff = mOff;
        String off = TimeConverter.minutesToTime(mOff);
        lineStTrain.setOccupyOff(off);
        lineStTrain.setIdLineSt(idLineSt);
        if (calcTStop) {
            lineStTrain.setTstop(TimeConverter.minutesBetween(mOn, mOff));
        }
    }

    public void setArriveSt(int mOn, int idLineSt) {
        this.mOn = mOn;
        String on = TimeConverter.minutesToTime(mOn);
        lineStTrain.setOccupyOn(on);
        lineStTrain.setIdLineSt(idLineSt);
    }

    public LineStTrain getLineStTrain() {
        return lineStTrain;
    }

    public void setEndLeave(int[] varTime) {
        if (lineStTrain.getTstop() > 0) {
            lineStTrain.setStop(1);
            mOff = TimeConverter.addMinutes(mOn, lineStTrain.getTstop());
        } else {
            mOff = mOn;
        }
        varTime[0] = mOff;
        lineStTrain.setOccupyOff(TimeConverter.minutesToTime(mOff));
    }

}
