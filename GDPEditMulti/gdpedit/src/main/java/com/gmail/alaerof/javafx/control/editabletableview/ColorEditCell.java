package com.gmail.alaerof.javafx.control.editabletableview;

import com.gmail.alaerof.util.ColorConverter;
import java.util.function.Function;
import java.util.regex.Pattern;
import javafx.beans.property.IntegerProperty;
import javafx.scene.paint.Color;

public class ColorEditCell<T> extends EditCell<T, Integer, IntegerProperty> {
    /**
     * {@inheritDoc}
     */
    public ColorEditCell(Function<T, IntegerProperty> property) {
        super(property);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateItem(Integer item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {
            setText(null);
            setStyle("");
        } else {
            Color clrFX = getColor(item);
            String hex = ColorConverter.getHEXString(clrFX);
            setText(hex);
            setStyle("-fx-background-color: " + hex);
            setGraphic(null);
        }
    }

    private Color getColor(int clr) {
        java.awt.Color clrAWT = ColorConverter.convert(clr);
        Color clrFX = ColorConverter.colorAWTtoFX(clrAWT);
        return clrFX;
    }

    @Override
    protected void bindItemProperty() {

    }

    @Override
    protected Integer parseText(String text) {
        return null;
    }

    @Override
    protected Pattern getSimpleTypePattern() {
        return null;
    }
}
