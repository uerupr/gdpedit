package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.gdp.DirTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.javafx.control.RoundedDoubleProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

public class DirTrainModel {
    private DirTrain dirTrain;
    private DistrTrain distrTrain;
    private String nameDir;

    private StringProperty dirName;
    // code
    private StringProperty codeTR;
    // Name
    private StringProperty nameTR;
    // L ����������, ���������� ������� �� �������, �
    private DoubleProperty len;
    // Tt ����� ���������� ������ �� ������� ��� ����� ������� �������, ���
    private DoubleProperty tt;
    // Tu ����� ���������� ������ �� �������, ���
    private DoubleProperty tu;
    // Vt, ��� �������� ��/�
    private DoubleProperty vt;
    // Vu, �� �������� ��/�
    private DoubleProperty vu;


    public DirTrainModel(DirTrain dirTrain, DistrTrain distrTrain, String nameDir) {
        this.dirTrain = dirTrain;
        this.distrTrain = distrTrain;
        this.nameDir = nameDir;

        dirName = new SimpleStringProperty(nameDir);
        codeTR = new SimpleStringProperty(distrTrain.getCodeTR());
        nameTR = new SimpleStringProperty(distrTrain.getNameTR());
        len = new RoundedDoubleProperty(dirTrain.getLen());
        tt = new RoundedDoubleProperty(dirTrain.getTt());
        tu = new RoundedDoubleProperty(dirTrain.getTu());
        vt = new RoundedDoubleProperty(dirTrain.getVt());
        vu = new RoundedDoubleProperty(dirTrain.getVu());
    }

    public DirTrain getDirTrain() {
        return dirTrain;
    }

    public DistrTrain getDistrTrain() {
        return distrTrain;
    }

    public String getNameDir() {
        return nameDir;
    }

    public String getDirName() {
        return dirName.get();
    }

    public StringProperty dirNameProperty() {
        return dirName;
    }

    public String getCodeTR() {
        return codeTR.get();
    }

    public StringProperty codeTRProperty() {
        return codeTR;
    }

    public String getNameTR() {
        return nameTR.get();
    }

    public StringProperty nameTRProperty() {
        return nameTR;
    }

    public double getLen() {
        return len.get();
    }

    public DoubleProperty lenProperty() {
        return len;
    }

    public double getTt() {
        return tt.get();
    }

    public DoubleProperty ttProperty() {
        return tt;
    }

    public double getTu() {
        return tu.get();
    }

    public DoubleProperty tuProperty() {
        return tu;
    }

    public double getVt() {
        return vt.get();
    }

    public DoubleProperty vtProperty() {
        return vt;
    }

    public double getVu() {
        return vu.get();
    }

    public DoubleProperty vuProperty() {
        return vu;
    }

    public List<Object> getTabValuesList() {
        List<Object> list = new ArrayList<>();
        list.add(dirName.getValue());
        list.add(codeTR.getValue());
        list.add(nameTR.getValue());
        list.add(len.getValue());
        list.add(tt.getValue());
        list.add(tu.getValue());
        list.add(vt.getValue());
        list.add(vu.getValue());
        return list;
    }
}
