package com.gmail.alaerof.javafx.dialog.stationinfo.model;

import com.gmail.alaerof.dao.dto.LineSt;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


import static org.apache.commons.lang.StringUtils.trim;

public class LineStModel {
    // LineSt
    private LineSt lineSt;
    // N
    private IntegerProperty n;
    // PS
    private StringProperty ps;

    public LineStModel(LineSt lineSt) {
        this.lineSt = lineSt;
        this.n = new SimpleIntegerProperty(lineSt.getN());
        this.ps = new SimpleStringProperty(trim(lineSt.getPs()));
    }

    public LineSt getLineSt() {
        return lineSt;
    }

    public int getN() {
        return n.get();
    }

    public IntegerProperty nProperty() {
        return n;
    }

    public String getPs() {
        return ps.get();
    }

    public StringProperty psProperty() {
        return ps;
    }

    public void setLineSt(LineSt lineSt) {
        this.lineSt = lineSt;
    }

    public void setN(int n) {
        this.n.set(n);
    }

    public void setPs(String ps) {
        this.ps.set(ps);
    }
}
