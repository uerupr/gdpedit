package com.gmail.alaerof.gdpcalculation.model;

import java.util.Comparator;

public class DistrTrainStTimeOffComparator implements Comparator<DistrTrainGDPCalc> {
    private int idSt;
    public DistrTrainStTimeOffComparator(int idSt) {
        this.idSt = idSt;
    }

    @Override
    public int compare(DistrTrainGDPCalc tr1, DistrTrainGDPCalc tr2) {
        TimeStCalc timeStCalc1 = tr1.getTimeStCalcMap().get(idSt);
        TimeStCalc timeStCalc2 = tr2.getTimeStCalcMap().get(idSt);
        return timeStCalc1.getmOff() - timeStCalc2.getmOff();
    }
}
