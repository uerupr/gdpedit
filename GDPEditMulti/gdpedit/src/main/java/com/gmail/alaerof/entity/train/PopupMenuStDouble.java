package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.components.mainframe.gdp.action.GDPActionPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import java.util.List;
import java.util.ResourceBundle;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrLineSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class PopupMenuStDouble extends JPopupMenu {
    private static final long serialVersionUID = 1L;
    private NewLine line;
    private static ResourceBundle bundle;

    private ActionListener lineStListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
            GDPActionPanel actionTabPane = GDPEdit.gdpEdit.getGDPActionPanel();
            String nameLine = null;
            if (item.isSelected()) {
                nameLine = item.getText();
            }
            if (line != null && item.isSelected()) {
                TrainStDouble trStD = (TrainStDouble) line.getTrainThreadElement();
                DistrStation dSt = trStD.getStationBegin().station;
                int idLine = dSt.getLineID(nameLine);
                trStD.setIdLineSt(idLine);
                actionTabPane.getActionListSt().tableRepaint();
            }

        }
    };

    public PopupMenuStDouble() {
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.train", LocaleManager.getLocale());
    }

    private void initContent() {
        this.removeAll();
        JMenuItem item1 = new JMenuItem(bundle.getString("train.removelink"));
        this.add(item1);
        item1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DistrEditEntity distrEE = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
                if (distrEE != null) {
                    // ������� ����� �� ������
                    TrainStDouble trStD = (TrainStDouble) line.getTrainThreadElement();
                    distrEE.removeTrainStDouble(trStD);
                }
            }
        });
        
        if (line != null) {

            TrainStDouble trStD = (TrainStDouble) line.getTrainThreadElement();
            DistrStation dSt = trStD.getStationBegin().station;
            int idLine = trStD.getIdLineSt();
            List<DistrLineSt> listLines = dSt.getListLines();
            final ButtonGroup linesCheckBox = new ButtonGroup();
            for (int i = 0; i < listLines.size(); i++) {
                DistrLineSt lineSt = listLines.get(i);
                if (!lineSt.getLineSt().isCloseInGDP()) {
                    String lineName = lineSt.getLineSt().getPs();
                    boolean selected = lineSt.getLineSt().getIdLineSt() == idLine;
                    JCheckBoxMenuItem item = new JCheckBoxMenuItem(lineName, selected);
                    item.addActionListener(lineStListener);
                    linesCheckBox.add(item);
                    this.add(item);
                }
            }
        }
    }

    public NewLine getLine() {
        return line;
    }

    public void setLine(NewLine line) {
        boolean init = true;
        if (line != null && this.line != null) {
            TrainStDouble trStD_N = (TrainStDouble) line.getTrainThreadElement();
            TrainStDouble trStD_O = (TrainStDouble) this.line.getTrainThreadElement();
            int idO = trStD_O.getIdLineSt();
            int idN = trStD_N.getIdLineSt();
            init = idO != idN;
        }

        this.line = line;
        if (init)
            initContent();
    }
}
