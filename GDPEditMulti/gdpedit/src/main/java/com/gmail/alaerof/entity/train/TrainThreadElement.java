package com.gmail.alaerof.entity.train;

import java.awt.Graphics;

import javax.swing.JComponent;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * ������� ����� �������� ����� ������
 * 
 * @author Helen Yrofeeva
 * 
 */
public abstract class TrainThreadElement {
    protected static Logger logger = LogManager.getLogger(TrainThread.class);
    protected JComponent owner;
    /** ����� ������, ������� ����������� ������� */
    protected TrainThread trainThread;
    /** ���������� ���������, ��������� ������ */
    protected NewLine clickedLine;

    public TrainThreadElement(JComponent owner, TrainThread trainThread) {
        this.owner = owner;
        this.trainThread = trainThread;
    }

    public TrainThread getTrainThread() {
        return trainThread;
    }
    
    /** ��������� �������� �� ��������� ����������� */
    @Deprecated
    public abstract void drawInPicture(Graphics g);
    
    /** ��������� �������� � ��������� ����� � ����������� �� timeSt1 � timeSt2 */
    public abstract void setLineBounds();

    public abstract void updateLineListener();
    
    public abstract void repaint();
    
    public abstract void removeFromOwner();

    public abstract void addToOwner(); 
    
    public abstract TimeStation getClickedTimeSt();
    
    public abstract TimeStation getStationBegin();
    
    public NewLine getClickedLine(){
        return clickedLine;
    }
    
    public void setClickedLine(NewLine line){
        clickedLine = line;
    }


}
