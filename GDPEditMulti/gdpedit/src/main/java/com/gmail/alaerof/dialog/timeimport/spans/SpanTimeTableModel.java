package com.gmail.alaerof.dialog.timeimport.spans;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */

public class SpanTimeTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("spans.number"),
            bundle.getString("spans.name"),
            bundle.getString("spans.typeofmove"),
            bundle.getString("spans.directionofmove"),
            bundle.getString("spans.running"),
            bundle.getString("spans.racing"),
            bundle.getString("spans.slowdown"),
            bundle.getString("spans.addition"),
            bundle.getString("spans.newrunning"),
            bundle.getString("spans.newracing"),
            bundle.getString("spans.newslowdown"),
            bundle.getString("spans.newaddition"),
            bundle.getString("spans.additionracing"),
            bundle.getString("spans.additionslowdown") };
    private List<SpanTimeHolder> list;
    private boolean editable = true;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spans", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            SpanTimeHolder el = list.get(row);
          
            Time tmOld = el.getSpan().getSpanTime(el.getLocType(), el.getOe());
            switch (col) {
            case 0:
                return el.getSpan().getSpanScale().getNum();
            case 1:
                return el.getSpan().getSpanScale().getName();
            case 2:
                return el.getLocType().getLtype();
            case 3:
                return el.getOe().getString();
            case 4:
                if (tmOld != null) {
                    return tmOld.getTimeMove();
                }
                return null;
            case 5:
                if (tmOld != null) {
                    return tmOld.getTimeUp();
                }
                return null;
            case 6:
                if (tmOld != null) {
                    return tmOld.getTimeDown();
                }
                return null;
            case 7:
                if (tmOld != null) {
                    return tmOld.getTimeTw();
                }
                return null;
            case 8:
                if (el.getTime() != null) {
                    return el.getTime().getTimeMove();
                }
                return null;
            case 9:
                if (el.getTime() != null) {
                    return el.getTime().getTimeUp();
                }
                return null;
            case 10:
                if (el.getTime() != null) {
                    return el.getTime().getTimeDown();
                }
                return null;
            case 11:
                if (el.getTimeAdd() != null) {
                    return el.getTimeAdd().getTimeMove();
                }
                return null;
            case 12:
                if (el.getTimeAdd() != null) {
                    return el.getTimeAdd().getTimeUp();
                }
                return null;
            case 13:
                if (el.getTimeAdd() != null) {
                    return el.getTimeAdd().getTimeDown();
                }
                return null;
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (isEditable() && col > 7) {
            return true;
        }
        return false;
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if (col > 3) {
            return Float.class;
        }
        return Object.class;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (isEditable() && list != null && aValue != null && row < getRowCount() && col < getColumnCount()) {
            SpanTimeHolder htm = list.get(row);
            Time tm = htm.getTime();
            if (tm != null) {
                switch (col) {
                case 8:
                    tm.setTimeMove((Float)aValue);
                    break;
                case 9:
                    tm.setTimeUp((Float)aValue);
                    break;
                case 10:
                    tm.setTimeDown((Float)aValue);
                    break;
                case 11:
                    tm.setTimeTw((Float)aValue);
                    break;
                default:
                    break;
                }
            }
        }
    }

    public List<SpanTimeHolder> getList() {
        return list;
    }

    public void setList(List<SpanTimeHolder> list) {
        this.list = list;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

}
