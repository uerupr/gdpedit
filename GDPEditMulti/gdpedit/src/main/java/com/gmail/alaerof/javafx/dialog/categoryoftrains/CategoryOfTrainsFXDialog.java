package com.gmail.alaerof.javafx.dialog.categoryoftrains;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.categoryoftrains.view.CategoryOfTrainsLayoutController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class CategoryOfTrainsFXDialog  extends SwingFXDialogBase {
    /**FX controller */
    private CategoryOfTrainsLayoutController controller;
    private List<Category> categoryList;

    /**
     * Create the dialog.
     */
    public CategoryOfTrainsFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/categoryoftrains/view/CategoryOfTrainsLayout.fxml";
        resourceBundleBaseName = "bundles.CategoryOfTrainsFXDialog";

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if (categoryList != null) {
                    categoryList.clear();
                    controller.getDeletedCategories().clear();
                }
            }
        });
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
        controller.setDialogFrame(this);
    }

    public void setDialogContent(List<Category> categoryList) {
        this.categoryList = categoryList;
        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(categoryList);
            }
        });
    }

    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 1200, 620};
        return bounds;
    }
}

