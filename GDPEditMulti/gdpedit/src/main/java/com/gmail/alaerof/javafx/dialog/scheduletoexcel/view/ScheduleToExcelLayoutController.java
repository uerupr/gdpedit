package com.gmail.alaerof.javafx.dialog.scheduletoexcel.view;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.exportexcel.ExportToExcel;
import com.gmail.alaerof.exportexcel.ExportToExcelParam;
import com.gmail.alaerof.dialog.ColorDialog;
import com.gmail.alaerof.util.ColorConverter;
import java.awt.Frame;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Paul M. Bui on 29.06.2018.
 */
public class ScheduleToExcelLayoutController extends Frame implements Initializable {
    protected Logger logger = LogManager.getLogger(ScheduleToExcelLayoutController.class);
    protected ResourceBundle bundle;
    private GDPEditPanel editPanel;
    private DistrEditEntity distrEditEntity;
    private Scene scene;

    @FXML
    private RadioButton rbGr4;
    @FXML
    private RadioButton rbGr8;
    @FXML
    private RadioButton rbPr4;
    @FXML
    private RadioButton rbPr8;
    @FXML
    private CheckBox cbGrESR;
    @FXML
    private CheckBox cbGrExpress;
    @FXML
    private CheckBox cbGrRoundKm;
    @FXML
    private CheckBox cbGrsortTimeMuve;
    @FXML
    private CheckBox cbGrdopParam;
    @FXML
    private CheckBox cbGrusldl;
    @FXML
    private CheckBox cbPrESR;
    @FXML
    private CheckBox cbPrExpress;
    @FXML
    private CheckBox cbPrRoundKm;
    @FXML
    private CheckBox cbPrRp;
    @FXML
    private CheckBox cbPrPohodu;
    @FXML
    private CheckBox cbPrObgon;
    @FXML
    private CheckBox cbPrAutoObgon;
    @FXML
    private CheckBox cbPrEdinNomer;
    @FXML
    private CheckBox cbPrdopParam;
    @FXML
    private TextField tfPt;
    @FXML
    private TextField tfPtW;
    @FXML
    private Circle circleColor;
    @FXML
    private Button bFirstButton;
    @FXML
    private Button bSecondButton;
    @FXML
    private Button bChooseColor;

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        tfPt.setText(bundle.getString("javafx.dialog.scheduletoexcel.overtakingbytrain"));
        tfPtW.setText(bundle.getString("javafx.dialog.scheduletoexcel.presenceinthecoursetrain"));
    }

    @FXML
    private void exportToExcel(ActionEvent event){
        logger.debug("exportToExcelGr");
        scene.setCursor(Cursor.WAIT);
        try {
            Pattern pIDButton = Pattern.compile("id=(.*?),");
            Matcher mIDButton = pIDButton.matcher(event.getSource().toString());
            if (mIDButton.find()) {
                String buttonName = mIDButton.group().subSequence(3, mIDButton.group().length() - 1).toString();
                ExportToExcelParam exportToExcelParam = new ExportToExcelParam();
                switch (buttonName) {
                    case "bFirstButton": {
                        if (rbGr4.isSelected()) {
                            exportToExcelParam.setTrainTCount(4);
                        }
                        if (rbGr8.isSelected()) {
                            exportToExcelParam.setTrainTCount(8);
                        }
                        exportToExcelParam.setTrainType(bundle.getString("javafx.dialog.scheduletoexcel.cargo"));
                        exportToExcelParam.setRoundKm(cbGrRoundKm.isSelected());
                        exportToExcelParam.setUslDl(cbGrusldl.isSelected());
                        exportToExcelParam.setESR(cbGrESR.isSelected());
                        exportToExcelParam.setExpress(cbGrExpress.isSelected());
                        exportToExcelParam.setSortTimeMuve(cbGrsortTimeMuve.isSelected());
                        exportToExcelParam.setDopParam(cbGrdopParam.isSelected());
                        ExportToExcel exportToExcel = new ExportToExcel(exportToExcelParam, distrEditEntity);
                    }
                    break;
                    case "bSecondButton": {
                        if (rbPr4.isSelected()) {
                            exportToExcelParam.setTrainTCount(4);
                        }
                        if (rbPr8.isSelected()) {
                            exportToExcelParam.setTrainTCount(8);
                        }
                        exportToExcelParam.setTrainType(bundle.getString("javafx.dialog.scheduletoexcel.town"));
                        exportToExcelParam.setRoundKm(cbPrRoundKm.isSelected());
                        exportToExcelParam.setESR(cbPrESR.isSelected());
                        exportToExcelParam.setExpress(cbPrExpress.isSelected());
                        exportToExcelParam.setEdinNomer(cbPrEdinNomer.isSelected());
                        exportToExcelParam.setObgon(cbPrObgon.isSelected());
                        exportToExcelParam.setPohode(cbPrPohodu.isSelected());
                        exportToExcelParam.setRp(cbPrRp.isSelected());
                        exportToExcelParam.setDopParam(cbPrdopParam.isSelected());
                        exportToExcelParam.setAutoobgon(cbPrAutoObgon.isSelected());
                        exportToExcelParam.setTextPt(tfPt.getText());
                        exportToExcelParam.setTextPtW(tfPtW.getText());
                        Color clrFX = (Color) circleColor.fillProperty().get();
                        java.awt.Color clrAWT = ColorConverter.colorFXtoAWT(clrFX);
                        exportToExcelParam.setHolidayTrainColor(clrAWT);
                        ExportToExcel exportToExcel = new ExportToExcel(exportToExcelParam, distrEditEntity);
                    }
                    break;
                }
            }
        }finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    @FXML
    private void chooseColor(ActionEvent event){
        ColorDialog cd = GDPEdit.gdpEdit.getDialogManager().getColorDialog();
        cd.setVisible(true);
        if (cd.getResult() > 0) {
            java.awt.Color clrAWT = cd.getColor();
            Color clrFX = ColorConverter.colorAWTtoFX(clrAWT);
            circleColor.setFill(clrFX);
        }
    }

    public void setContent(DistrEditEntity distrEditEntity, GDPEditPanel editPanel) {
        logger.debug("setContent");
        this.distrEditEntity = distrEditEntity;
        this.editPanel = editPanel;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
