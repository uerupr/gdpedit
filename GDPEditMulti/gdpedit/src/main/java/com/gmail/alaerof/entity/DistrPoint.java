package com.gmail.alaerof.entity;

import java.util.List;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.SpanStTime;

/**
 * ���������� ��� ������������ ����� �.�. �������
 * @author Helen Yrofeeva
 */
public abstract class DistrPoint implements Comparable<DistrPoint> {
    public enum TypePoint {
        SeparatePoint, StopPoint
    }

    /** ��������� ���������� ����� ������� ��� �.�. */
    private int num;

    /** ������ ������ ���� ����� �.�. - �.�. - �.�. ��� ���� ����� �������� */
    private DistrSpan distrSpan;;
    private int numOnSpan;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public int compareTo(DistrPoint distrPoint) {
        return this.num - distrPoint.num;
    }

    public abstract TypePoint getTypePoint();

    public abstract String getNamePoint();

    public abstract int getIdPoint();

    public List<SpanStTime> getListSpanStTime() {
        List<SpanStTime> listSpanStTime = null;
        if (distrSpan != null) {
            listSpanStTime = distrSpan.getSpanStTime(numOnSpan);
        }
        return listSpanStTime;
    }

    public void setListSpanStTime(DistrSpan distrSpan, int numOnSpan) {
        this.distrSpan = distrSpan;
        this.numOnSpan = numOnSpan;
    }

    public SpanStTime getSpanStTime(LocomotiveType locType) {
        SpanStTime spanStTime = null;
        List<SpanStTime> listSpanStTime = null;
        if (distrSpan != null) {
            listSpanStTime = distrSpan.getSpanStTime(numOnSpan);
        }
        if (locType != null && listSpanStTime != null) {
            for (int i = 0; i < listSpanStTime.size(); i++) {
                spanStTime = listSpanStTime.get(i);
                if (spanStTime.getIdLType() == locType.getIdLType()) {
                    return spanStTime;
                }
            }
        }
        return null;
    }
}
