package com.gmail.alaerof.javafx.dialog.calculategdp;

import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.calculategdp.view.CalculateGDPController;
import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

public class CalculateGDPFXDialog extends SwingFXDialogBase {
    /**FX controller */
    private CalculateGDPController controller;

    public CalculateGDPFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/calculategdp/view/CalculateGDPLayout.fxml";
        resourceBundleBaseName = "bundles.CalculateGDPFXDialog";
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    public void setDialogContent(DistrEditEntity distrEditEntity) {
        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(distrEditEntity);
            }
        });
    }

    /** {@inheritDoc} */
    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 1000, 700};
        return bounds;
    }
}
