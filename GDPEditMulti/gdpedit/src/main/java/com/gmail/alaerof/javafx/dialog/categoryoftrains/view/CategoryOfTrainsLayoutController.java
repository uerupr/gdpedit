package com.gmail.alaerof.javafx.dialog.categoryoftrains.view;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dialog.ColorDialog;
import com.gmail.alaerof.javafx.control.editabletableview.IntegerEditCell;
import com.gmail.alaerof.javafx.control.editabletableview.StringEditCell;
import com.gmail.alaerof.javafx.dialog.categoryoftrains.model.CategoryOfTrainsModel;
import com.gmail.alaerof.util.ColorConverter;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.converter.IntegerStringConverter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

public class CategoryOfTrainsLayoutController implements Initializable {
    /** logger */
    protected Logger logger;
    {
        logger = LogManager.getLogger(this.getClass());
    }
    /** Internationalization */
    protected ResourceBundle bundle;

    /** ������ ��� ����������� */
    ObservableList<CategoryOfTrainsModel> categoryOfTrainsModelList;

    /** ������ ��������� ��������� */
    private List<CategoryOfTrainsModel> deletedCategories = new ArrayList<>();

    private Scene scene;

    protected JDialog dialogFrame;

    public List<CategoryOfTrainsModel> getDeletedCategories() {
        return deletedCategories;
    }

    /** ������� ��������� ������� */
    @FXML
    private TableView<CategoryOfTrainsModel> categoryOfTrainsTable;

    @FXML
    private TableColumn<CategoryOfTrainsModel, Integer> idColumn;

    @FXML
    private TableColumn<CategoryOfTrainsModel, String> name;

    @FXML
    private TableColumn<CategoryOfTrainsModel, Integer> priority;

    @FXML
    private TableColumn<CategoryOfTrainsModel, Integer> withNumber;

    @FXML
    private TableColumn<CategoryOfTrainsModel, Integer> beforeNumber;

    @FXML
    private TableColumn<CategoryOfTrainsModel, String> typeTrain;

    @FXML
    private TableColumn<CategoryOfTrainsModel, Color> color;

    /** ������ */
    @FXML
    private VBox rootPane;

    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button refreshButton;

    @FXML
    private Button saveDBButton;

    @FXML
    private Button closeButton;

    /**
     * ������������� ���������� ��� �������� �� FXML ����������� (FXMLLoader)
     * @param location {@link URL}
     * @param resources {@ResourceBundle} �������������� ��������
     */
    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        categoryOfTrainsTable.getSelectionModel().setCellSelectionEnabled(true);
        categoryOfTrainsTable.setEditable(true);
//        categoryOfTrainsTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);

        categoryOfTrainsTable.setOnKeyPressed(event -> {
            TablePosition<CategoryOfTrainsModel, ?> pos = categoryOfTrainsTable.getFocusModel().getFocusedCell() ;
            if (pos != null) {
                categoryOfTrainsTable.edit(pos.getRow(), pos.getTableColumn());
            }
        });
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;

        idColumn.setCellValueFactory(new PropertyValueFactory<CategoryOfTrainsModel, Integer>("idCat"));

        name.setCellValueFactory(new PropertyValueFactory<CategoryOfTrainsModel, String>("name"));
//        name.setCellFactory(TextFieldTableCell.<CategoryOfTrainsModel>forTableColumn());
//        name.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        Function<CategoryOfTrainsModel, StringProperty> propertyName = CategoryOfTrainsModel::nameProperty;
        name.setCellFactory(column -> new StringEditCell(propertyName));


        priority.setCellValueFactory(new PropertyValueFactory<CategoryOfTrainsModel, Integer>("priority"));
//        priority.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        Function<CategoryOfTrainsModel, IntegerProperty> propertyPriority = CategoryOfTrainsModel::priorityProperty;
        priority.setCellFactory(column -> new IntegerEditCell(propertyPriority));

        withNumber.setCellValueFactory(new PropertyValueFactory<CategoryOfTrainsModel, Integer>("trMin"));
//        withNumber.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        Function<CategoryOfTrainsModel, IntegerProperty> propertyTrMin = CategoryOfTrainsModel::trMinProperty;
        withNumber.setCellFactory(column -> new IntegerEditCell(propertyTrMin));

        beforeNumber.setCellValueFactory(new PropertyValueFactory<CategoryOfTrainsModel, Integer>("trMax"));
//        beforeNumber.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        Function<CategoryOfTrainsModel, IntegerProperty> propertyTrMax = CategoryOfTrainsModel::trMaxProperty;
        beforeNumber.setCellFactory(column -> new IntegerEditCell(propertyTrMax));

        color.setCellValueFactory(new PropertyValueFactory<CategoryOfTrainsModel, java.awt.Color>("color"));
        color.setCellFactory(column -> {
            return new TableCell<CategoryOfTrainsModel, java.awt.Color>() {
                @Override
                protected void updateItem(java.awt.Color item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
//                        javafx.scene.paint.Color clrFX = ColorConverter.colorFXtoAWT(item);
                        javafx.scene.paint.Color clrFX = ColorConverter.colorAWTtoFX(item);
                        String hex = ColorConverter.getHEXString(clrFX);
                        setTextFill(javafx.scene.paint.Color.WHITE);
                        setText(hex);
                        setStyle("-fx-background-color: " + hex);
                        setGraphic(null);
                    }
                }
            };
        });

        categoryOfTrainsTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    TablePosition pos = categoryOfTrainsTable.getSelectionModel().getSelectedCells().get(0);
                    int row = pos.getRow();
                    int col = pos.getColumn();
                    if (pos.getTableColumn() == color){
                        ColorDialog cd = GDPEdit.gdpEdit.getDialogManager().getColorDialog();
                        cd.setVisible(true);
                        if (cd.getResult() > 0) {
                            java.awt.Color clrAWT = cd.getColor();
                            categoryOfTrainsModelList.get(row).setColor(clrAWT);
                        }

                    }
                }
            }
        });

        typeTrain.setCellValueFactory(new PropertyValueFactory<CategoryOfTrainsModel, String>("type"));
        typeTrain.setCellFactory(ComboBoxTableCell.forTableColumn(valuesTrainType()));
    }

    private ObservableList<String> valuesTrainType() {
        ObservableList<String> listTrainType = FXCollections.observableArrayList();
        for (TrainType trainType : TrainType.values()) {
            listTrainType.add(trainType.getString());
        }
        return listTrainType;
    }

    /**
     * @param categoryList {@link CategoryOfTrainsModel} ������ ��� ������� � �����������
     */
    public void setContent(List<Category> categoryList) {
        scene.setCursor(Cursor.WAIT);
        try {
            logger.debug("updateModelWithContent");
            // TrainThreadModel List
            this.categoryOfTrainsModelList = FXCollections.observableArrayList(categoryListToCategoryOfTrainsModelList(categoryList));
            categoryOfTrainsTable.setItems(this.categoryOfTrainsModelList);
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    private List<CategoryOfTrainsModel> categoryListToCategoryOfTrainsModelList(List<Category> categoryList) {
        List<CategoryOfTrainsModel> categoryOfTrainsModelList = new ArrayList<>();
        for (Category category : categoryList) {
            categoryOfTrainsModelList.add(new CategoryOfTrainsModel(category));
        }
        return categoryOfTrainsModelList;
    }

    private List<Category> categoryOfTrainsModelListToCategoryList(List<CategoryOfTrainsModel> categoryOfTrainsModelList) {
        List<Category> categoryList = new ArrayList<>();
        for (CategoryOfTrainsModel categoryOfTrainsModel : categoryOfTrainsModelList) {
            if (categoryOfTrainsModel.getName() != null && ! "".equals(categoryOfTrainsModel.getName().trim())) {
                Category category = new Category();
                category.setIdCat(categoryOfTrainsModel.getIdCat());
                category.setName(categoryOfTrainsModel.getName().trim());
                category.setPriority(categoryOfTrainsModel.getPriority());
                category.setTrMin(categoryOfTrainsModel.getTrMin());
                category.setTrMax(categoryOfTrainsModel.getTrMax());
                category.setColor((java.awt.Color)categoryOfTrainsModel.getColor());
                category.setType(categoryOfTrainsModel.getType());
                categoryList.add(category);
            }
        }
        return categoryList;
    }

    @FXML
    private void handleAddRecord(ActionEvent actionEvent) {
        //��������� ������
        CategoryOfTrainsModel category = new CategoryOfTrainsModel(new Category());
        categoryOfTrainsModelList.add(category);
        categoryOfTrainsTable.scrollTo(categoryOfTrainsModelList.size());
        categoryOfTrainsTable.getSelectionModel().selectLast();
    }

    @FXML
    private void handleDeleteRecord(ActionEvent actionEvent) {
        //������� ������
        Alert alert;
        int selectedIndex = categoryOfTrainsTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            alert = new Alert(Alert.AlertType.CONFIRMATION,
                    bundle.getString("javafx.dialog.categoryoftrains.alert.delrecord"),
                    ButtonType.YES, ButtonType.NO);
            alert.setTitle(bundle.getString("javafx.dialog.categoryoftrains.alert.confirm"));
            alert.setHeaderText(null);
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {
                categoryOfTrainsTable.scrollTo(selectedIndex);
                CategoryOfTrainsModel categoryOfTrainsModel = categoryOfTrainsTable.getItems().get(selectedIndex);
                if (categoryOfTrainsModel.getIdCat() > 0) {
                    deletedCategories.add(categoryOfTrainsModel);
                }
                categoryOfTrainsTable.getItems().remove(selectedIndex);
            }
        } else {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(bundle.getString("javafx.dialog.categoryoftrains.alert.information"));
            alert.setHeaderText(null);
            alert.setContentText(bundle.getString("javafx.dialog.categoryoftrains.alert.selectrecord"));
            alert.showAndWait();
        }
    }

    @FXML
    private void handleSaveToDB(ActionEvent actionEvent) {
        //��������� � ��
        scene.setCursor(Cursor.WAIT);
        Alert.AlertType alertType = Alert.AlertType.INFORMATION;
        String message = bundle.getString("javafx.dialog.categoryoftrains.alert.savedtodb");
        try {
            ICategoryDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO();
            try {
                List<Category> categoryList = categoryOfTrainsModelListToCategoryList(categoryOfTrainsTable.getItems());
                List<Category> deletedCategoryList = categoryOfTrainsModelListToCategoryList(deletedCategories);
                dao.saveToDB(categoryList, deletedCategoryList);
            } catch (DataManageException e) {
                logger.error(e.toString(), e);
                alertType = Alert.AlertType.ERROR;
                message = e.toString();
            }
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
        Alert alert = new Alert(alertType, message);
        alert.setTitle(bundle.getString("javafx.dialog.categoryoftrains.alert.information"));
        alert.setHeaderText(null);
        alert.showAndWait();
    }

    public void setDialogFrame(JDialog dialogFrame) {
        this.dialogFrame = dialogFrame;
    }

    private void closeFrame() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                dialogFrame.setVisible(false);
            }
        });
    }

    @FXML
    public void handleCloseStage(ActionEvent actionEvent) {
//        deletedCategories.clear();
        closeFrame();
    }

    @FXML
    public void handleRefreshCategory(ActionEvent actionEvent) {
        //�������� ��������� ������� � ��
        scene.setCursor(Cursor.WAIT);
        Alert.AlertType alertType = Alert.AlertType.INFORMATION;
        String message = bundle.getString("javafx.dialog.categoryoftrains.alert.refreshfordb");
        try {
            ICategoryDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO();
            try {
                List<Category> categoryList = categoryOfTrainsModelListToCategoryList(categoryOfTrainsTable.getItems());
                List<Category> deletedCategoryList = categoryOfTrainsModelListToCategoryList(deletedCategories);
                dao.saveToDB(categoryList, deletedCategoryList);
                dao.refreshToDB();
            } catch (DataManageException e) {
                logger.error(e.toString(), e);
                alertType = Alert.AlertType.ERROR;
                message = e.toString();
            }
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
        Alert alert = new Alert(alertType, message);
        alert.setTitle(bundle.getString("javafx.dialog.categoryoftrains.alert.information"));
        alert.setHeaderText(null);
        alert.showAndWait();
    }
}
