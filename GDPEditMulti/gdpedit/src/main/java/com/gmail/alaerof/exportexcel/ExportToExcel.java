package com.gmail.alaerof.exportexcel;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrSpanSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.ColorConverter;
import com.gmail.alaerof.util.FileUtil;
import com.gmail.alaerof.util.TimeConverter;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import static java.lang.Math.ceil;

public class ExportToExcel {
    protected Logger logger = LogManager.getLogger(ExportToExcel.class);
    private ExportToExcelParam exportToExcelParam;
    private DistrEditEntity distrEditEntity;
    private Cell cell;
    private HSSFFont mainFont;
    private HSSFFont boldFont;
    private HSSFFont trainFont;
    private HSSFFont usldlFont;
    private HSSFFont commentFont;
    private HSSFCellStyle mainStyle;
    private HSSFCellStyle boldStyle;
    private HSSFCellStyle trainStyle;
    private HSSFCellStyle usldlStyle;
    private HSSFCellStyle commentStyle;
    private int cellCount; // ���������� �������� � ����������� �� �������� ����������
    private int commentRowCount = 0; // ���������� �������������� ����� �� ����������
    private List<String> localComments = new ArrayList<String>();
    private Map<Integer, String> localSpSt = new HashMap<Integer, String>();
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.exportexcel", LocaleManager.getLocale());
    }

    public ExportToExcel(ExportToExcelParam exportToExcelParam, DistrEditEntity distrEditEntity) {
        this.exportToExcelParam = exportToExcelParam;
        this.distrEditEntity = distrEditEntity;
        // �������� ������ excel ����� � ������
        HSSFWorkbook workbook = new HSSFWorkbook();
        // �������� ����� � ��������� "������ ����"
        HSSFSheet sheet = workbook.createSheet(bundle.getString("exportexcel.schedule"));
        // ��������� ������
        // ������� ������
        mainFont = workbook.createFont();
        mainFont.setFontName("Arial Cyr");
        mainFont.setFontHeightInPoints((short) 14);
        boldFont = workbook.createFont();
        boldFont.setFontName("Arial Cyr");
        boldFont.setFontHeightInPoints((short) 14);
        boldFont.setBold(true);
        trainFont = workbook.createFont();
        trainFont.setFontName("Arial Cyr");
        trainFont.setFontHeightInPoints((short) 12);
        usldlFont = workbook.createFont();
        usldlFont.setFontName("Arial Cyr");
        usldlFont.setFontHeightInPoints((short) 10);
        commentFont = workbook.createFont();
        commentFont.setFontName("Arial Cyr");
        commentFont.setFontHeightInPoints((short) 10);
        // ������� ����� ��� ������
        mainStyle = workbook.createCellStyle();
        mainStyle.setFont(mainFont);
        mainStyle.setAlignment(mainStyle.ALIGN_CENTER);
        mainStyle.setVerticalAlignment(mainStyle.VERTICAL_CENTER);
        mainStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        mainStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        mainStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        mainStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        boldStyle = workbook.createCellStyle();
        boldStyle.setFont(boldFont);
        boldStyle.setAlignment(boldStyle.ALIGN_CENTER);
        boldStyle.setVerticalAlignment(boldStyle.VERTICAL_CENTER);
        boldStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        boldStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        boldStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        boldStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        trainStyle = workbook.createCellStyle();
        trainStyle.setFont(trainFont);
        trainStyle.setAlignment(trainStyle.ALIGN_CENTER);
        trainStyle.setVerticalAlignment(trainStyle.VERTICAL_CENTER);
        trainStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        trainStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        trainStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        trainStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        usldlStyle = workbook.createCellStyle();
        usldlStyle.setFont(usldlFont);
        usldlStyle.setAlignment(usldlStyle.ALIGN_CENTER);
        usldlStyle.setVerticalAlignment(usldlStyle.VERTICAL_CENTER);
        usldlStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        usldlStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        usldlStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        usldlStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        commentStyle = workbook.createCellStyle();
        commentStyle.setFont(commentFont);
        commentStyle.setAlignment(commentStyle.ALIGN_LEFT);
        commentStyle.setVerticalAlignment(commentStyle.VERTICAL_CENTER);

        cellCount = 11;
        if (exportToExcelParam.isESR()){
            cellCount++;
        }
        if (exportToExcelParam.isExpress()){
            cellCount++;
        }
        // stationsCount - ���������� ������� � �.�. � ������ ����������� �������
        int stationsCount = 0;
        if (exportToExcelParam.getTrainType().equals(TrainType.gr.getString())) {
            stationsCount = distrEditEntity.getDistrEntity().getListSt().size();
        }
        //!!!!!!!! �.�.
        if (exportToExcelParam.getTrainType().equals(TrainType.town.getString())) {
            stationsCount = distrEditEntity.getDistrEntity().getListDistrPoint().size();
        }
        List<StationToExcel> stationsToExcel = new ArrayList<>();
        List<StationToExcel> reStationsToExcel = new ArrayList<>();
        for (int i = 0; i < stationsCount; i++){
            String stationKm = "";
            if (exportToExcelParam.getTrainType().equals(TrainType.gr.getString())) {
                for (int j = 0; j < distrEditEntity.getDistrEntity().getListSp().size(); j++){
                    if (distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getIdSt() == distrEditEntity.getDistrEntity().getListSt().get(i).getStation().getIdSt()){
                        stationKm = String.valueOf(distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getAbsKM());
                    }
                    if (distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getIdStE() == distrEditEntity.getDistrEntity().getListSt().get(i).getStation().getIdSt()){
                        stationKm = String.valueOf(distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getAbsKM_E());
                    }
                }
                Station st = distrEditEntity.getDistrEntity().getListSt().get(i).getStation();
                stationsToExcel.add(new StationToExcel(st.getIdSt(), DistrPoint.TypePoint.SeparatePoint, st.getName(), st.getCodeESR(), st.getCodeExpress(), stationKm));
            }
            //!!!!!!!! �.�.
            if (exportToExcelParam.getTrainType().equals(TrainType.town.getString())) {
                int id = distrEditEntity.getDistrEntity().getListDistrPoint().get(i).getIdPoint();
                if (distrEditEntity.getDistrEntity().getListDistrPoint().get(i).getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)){
                    DistrStation ds = (DistrStation) distrEditEntity.getDistrEntity().getListDistrPoint().get(i);
                    Station st = ds.getStation();
                    for (int j = 0; j < distrEditEntity.getDistrEntity().getListSp().size(); j++){
                        if (distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getIdSt() == st.getIdSt()){
                            stationKm = String.valueOf(distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getAbsKM());
                        }
                        if (distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getIdStE() == st.getIdSt()){
                            stationKm = String.valueOf(distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getAbsKM_E());
                        }
                    }
                    stationsToExcel.add(new StationToExcel(st.getIdSt(), DistrPoint.TypePoint.SeparatePoint, st.getName(), st.getCodeESR(), st.getCodeExpress(), stationKm));
                }
                if (distrEditEntity.getDistrEntity().getListDistrPoint().get(i).getTypePoint().equals(DistrPoint.TypePoint.StopPoint)){
                    DistrSpanSt dss = (DistrSpanSt) distrEditEntity.getDistrEntity().getListDistrPoint().get(i);
                    stationsToExcel.add(new StationToExcel(dss.getSpanSt().getIdSpanst(), DistrPoint.TypePoint.StopPoint, dss.getSpanSt().getName(), dss.getSpanSt().getCodeESR(), dss.getSpanSt().getCodeExpress(), String.valueOf(dss.getSpanSt().getAbsKM())));
                }
            }
        }
        for (int i = stationsCount-1; i >=0 ; i--){
            String stationKm = "";
            if (exportToExcelParam.getTrainType().equals(TrainType.gr.getString())) {
                for (int j = 0; j < distrEditEntity.getDistrEntity().getListSp().size(); j++){
                    if (distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getIdSt() == distrEditEntity.getDistrEntity().getListSt().get(i).getStation().getIdSt()){
                        stationKm = String.valueOf(distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getAbsKM());
                    }
                    if (distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getIdStE() == distrEditEntity.getDistrEntity().getListSt().get(i).getStation().getIdSt()){
                        stationKm = String.valueOf(distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getAbsKM_E());
                    }
                }
                Station st = distrEditEntity.getDistrEntity().getListSt().get(i).getStation();
                reStationsToExcel.add(new StationToExcel(st.getIdSt(), DistrPoint.TypePoint.SeparatePoint, st.getName(), st.getCodeESR(), st.getCodeExpress(), stationKm));
            }
            //!!!!!!!! �.�.
            if (exportToExcelParam.getTrainType().equals(TrainType.town.getString())) {
                int id = distrEditEntity.getDistrEntity().getListDistrPoint().get(i).getIdPoint();
                if (distrEditEntity.getDistrEntity().getListDistrPoint().get(i).getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)){
                    DistrStation ds = (DistrStation) distrEditEntity.getDistrEntity().getListDistrPoint().get(i);
                    Station st = ds.getStation();
                    for (int j = 0; j < distrEditEntity.getDistrEntity().getListSp().size(); j++){
                        if (distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getIdSt() == st.getIdSt()){
                            stationKm = String.valueOf(distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getAbsKM());
                        }
                        if (distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getIdStE() == st.getIdSt()){
                            stationKm = String.valueOf(distrEditEntity.getDistrEntity().getListSp().get(j).getSpanScale().getAbsKM_E());
                        }
                    }
                    reStationsToExcel.add(new StationToExcel(st.getIdSt(), DistrPoint.TypePoint.SeparatePoint, st.getName(), st.getCodeESR(), st.getCodeExpress(), stationKm));
                }
                if (distrEditEntity.getDistrEntity().getListDistrPoint().get(i).getTypePoint().equals(DistrPoint.TypePoint.StopPoint)){
                    DistrSpanSt dss = (DistrSpanSt) distrEditEntity.getDistrEntity().getListDistrPoint().get(i);
                    reStationsToExcel.add(new StationToExcel(dss.getSpanSt().getIdSpanst(), DistrPoint.TypePoint.StopPoint, dss.getSpanSt().getName(), dss.getSpanSt().getCodeESR(), dss.getSpanSt().getCodeExpress(), String.valueOf(dss.getSpanSt().getAbsKM())));
                }
            }
        }
        // �������� ���������� ������� (�������� � ����������� � ������� �������) ������������ �����������
        // trainsCount - ���������� �������
        // trainsToExcel - ����� ������� ��� ������ � ����� �������
        List<TrainThread> nechTrainsToExcel = new ArrayList<>();
        List<TrainThread> chTrainsToExcel = new ArrayList<>();
        List<TrainThread> trainsToExcel = new ArrayList<>();
        for (int i = 0; i < distrEditEntity.getListTrains().size(); i++){
            if ((!distrEditEntity.getListTrains().get(i).isForeign()) && (distrEditEntity.getListTrains().get(i).getDistrTrain().getTypeTR().getString().equals(exportToExcelParam.getTrainType()))){
                if (distrEditEntity.getListTrains().get(i).getDistrTrain().getOe().getString().equals(OE.odd.getString())){
                    nechTrainsToExcel.add(distrEditEntity.getListTrains().get(i));
                }
                if (distrEditEntity.getListTrains().get(i).getDistrTrain().getOe().getString().equals(OE.even.getString())){
                    chTrainsToExcel.add(distrEditEntity.getListTrains().get(i));
                }
            }
        }
        //����������
        if ((exportToExcelParam.getTrainType().equals(TrainType.gr)) && (exportToExcelParam.isSortTimeMuve())){
            Collections.sort(nechTrainsToExcel, new ExportToExcelSortTimeMuveComparator());
            Collections.sort(chTrainsToExcel, new ExportToExcelSortTimeMuveComparator());
        }
        if ((exportToExcelParam.getTrainType().equals(TrainType.town)) && (exportToExcelParam.isPohode())){
            Collections.sort(nechTrainsToExcel, new ExportToExcelSortTimeMuveComparator());
            Collections.sort(chTrainsToExcel, new ExportToExcelSortTimeMuveComparator());
        }

        // rowNumber - ����� ������, � ������� ���������� �������
        int rowNumber;
        // �������� ������
        int trainsCount = nechTrainsToExcel.size();
        // tabCount - ���������� ������
        double nechTabCount = ceil((double) trainsCount/exportToExcelParam.getTrainTCount());
        for (int i = 1; i <= nechTabCount; i++){
            trainsToExcel.clear();
            for (int j = 1; j <= exportToExcelParam.getTrainTCount(); j++){
                if ((i - 1) * exportToExcelParam.getTrainTCount() + j <= trainsCount){
                    // ������� �������� �������!!!
                    trainsToExcel.add(nechTrainsToExcel.get((i - 1) * exportToExcelParam.getTrainTCount() + j - 1));
                }
            }
            //������� ��� ������ ������ ��� ������ ��������
            //rowNumber = (4 + stationsCount + 3) * (i - 1) + commentRowCount;
            rowNumber = (4 + stationsCount + 2) * (i - 1) + commentRowCount;
            tabCreate(sheet, stationsToExcel, trainsToExcel, i, rowNumber);
        }
        // ������ ������
        trainsCount = chTrainsToExcel.size();
        double chTabCount = ceil((double) trainsCount/exportToExcelParam.getTrainTCount());
        for (int i = 1; i <= chTabCount; i++){
            trainsToExcel.clear();
            for (int j = 1; j <= exportToExcelParam.getTrainTCount(); j++){
                if ((i - 1) * exportToExcelParam.getTrainTCount() + j <= trainsCount){
                    // ������� �������� �������!!!
                    trainsToExcel.add(chTrainsToExcel.get((i - 1) * exportToExcelParam.getTrainTCount() + j - 1));
                }
            }
            //������� ��� ������ ������ ��� ������ ��������
            //rowNumber = (4 + stationsCount + 3) * (i - 1 + (int) nechTabCount) + commentRowCount;
            rowNumber = (4 + stationsCount + 2) * (i - 1 + (int) nechTabCount) + commentRowCount;
            tabCreate(sheet, reStationsToExcel, trainsToExcel, i + (int) nechTabCount, rowNumber);
        }
        // ������ � ����
        String distrName = distrEditEntity.getDistrEntity().getDistrName();
        String fileName = distrName + ".xls";
        if (exportToExcelParam.getTrainType().equals(TrainType.gr.getString())) {
            fileName = distrName + "(��������).xls";
        }
        if (exportToExcelParam.getTrainType().equals(TrainType.town.getString())) {
            fileName = distrName + "(�����������).xls";
        }
        File file = FileUtil.chooseFile(FileUtil.Mode.save,null, fileName, null,
                new FileChooser.ExtensionFilter("MSExcel files", "*.xls","*.xlsx"));
        if (file != null) {
            try (FileOutputStream out = new FileOutputStream(file)) {
                workbook.write(out);
            } catch (IOException e) {
                logger.error(e.toString(), e);
            }
            logger.debug("Export to Excel: Excel-file successfully created!");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(bundle.getString("exportexcel.attention"));
            alert.setHeaderText(bundle.getString("exportexcel.trainbook"));
            alert.setContentText(bundle.getString("exportexcel.ok"));
            alert.showAndWait();
        }
    }

    private void tabCreate(HSSFSheet sheet, List<StationToExcel> stationsToExcel, List<TrainThread> trainsToExcels, int tabNumber, int rowNumber){
        // ���������� �����
        localComments.clear();
        localSpSt.clear();
        Row row;
        // ������ 1
        row = sheet.createRow(rowNumber);
        drawCell(row,0,distrEditEntity.getDistrEntity().getDistrName(),distrEditEntity.getDistrEntity().getDistrName(),mainStyle);
        for (int j = 1; j < cellCount-2; j++){
            drawCell(row,j,null,null,mainStyle);
        }
        drawCell(row,cellCount-2,bundle.getString("exportexcel.table")+ tabNumber,"1",mainStyle);
        drawCell(row,cellCount-1,null,null,mainStyle);
        // ������ 2
        row = sheet.createRow(rowNumber + 1);
        if (exportToExcelParam.getTrainTCount() == 4) {
            drawTrainCode4(row, trainsToExcels, 0, 0);
            drawTrainCode4(row, trainsToExcels, 2, 1);
        }
        if (exportToExcelParam.getTrainTCount() == 8) {
            drawTrainCode8(row, trainsToExcels, 0, 0);
            drawTrainCode8(row, trainsToExcels, 1, 1);
            drawTrainCode8(row, trainsToExcels, 2, 2);
            drawTrainCode8(row, trainsToExcels, 3, 3);
        }
        drawCell(row, 4, bundle.getString("exportexcel.separateitems"), "1", mainStyle);
        drawCell(row, 5, null, null, mainStyle);
        drawCell(row, 6, null, null, mainStyle);
        if (cellCount > 11) {
            drawCell(row, 6, null, null, mainStyle);
        }
        if (cellCount == 13) {
            drawCell(row, 7, null, null, mainStyle);
        }
        if (exportToExcelParam.getTrainTCount() == 4) {
            drawTrainCode4(row, trainsToExcels, cellCount - 4, 2);
            drawTrainCode4(row, trainsToExcels, cellCount - 2, 3);
        }
        if (exportToExcelParam.getTrainTCount() == 8) {
            drawTrainCode8(row, trainsToExcels, cellCount-4, 4);
            drawTrainCode8(row, trainsToExcels, cellCount-3, 5);
            drawTrainCode8(row, trainsToExcels, cellCount-2, 6);
            drawTrainCode8(row, trainsToExcels, cellCount-1, 7);
        }
        // ������ 3 � ����� �����
        // ������ 4
        row = sheet.createRow(rowNumber + 3);
        if (exportToExcelParam.getTrainTCount() == 4) {
            drawCell(row, 0, bundle.getString("exportexcel.arrival"), "1", mainStyle);
            drawCell(row, 1, bundle.getString("exportexcel.departure"), "1", mainStyle);
            drawCell(row, 2, bundle.getString("exportexcel.arrival"), "1", mainStyle);
            drawCell(row, 3, bundle.getString("exportexcel.departure"), "1", mainStyle);
        }
        if (exportToExcelParam.getTrainTCount() == 8) {
            for (int j = 0; j < 4; j++){
                drawCell(row, j, bundle.getString("exportexcel.departure"), "1", mainStyle);
            }
        }
        drawCell(row,4,null,null,mainStyle);
        drawCell(row,5,null,null,mainStyle);
        if (exportToExcelParam.isESR()){
            drawCell(row,6,bundle.getString("exportexcel.esrcode"),"1",mainStyle);
            if (exportToExcelParam.isExpress()){
                drawCell(row,7,bundle.getString("exportexcel.expresscode"),"1",mainStyle);
                drawCell(row,8,bundle.getString("exportexcel.km"),"1",mainStyle);
            }
            else {
                drawCell(row,7,bundle.getString("exportexcel.km"),"1",mainStyle);
            }
        }
        else {
            if (exportToExcelParam.isExpress()){
                drawCell(row,6,bundle.getString("exportexcel.expresscode"),"1",mainStyle);
                drawCell(row,7,bundle.getString("exportexcel.km"),"1",mainStyle);
            }
            else {
                drawCell(row,6,bundle.getString("exportexcel.km"),"1",mainStyle);
            }
        }
        if (exportToExcelParam.getTrainTCount() == 4) {
            drawCell(row, cellCount - 4, bundle.getString("exportexcel.arrival"), "1", mainStyle);
            drawCell(row, cellCount - 3, bundle.getString("exportexcel.departure"), "1", mainStyle);
            drawCell(row, cellCount - 2, bundle.getString("exportexcel.arrival"), "1", mainStyle);
            drawCell(row, cellCount - 1, bundle.getString("exportexcel.departure"), "1", mainStyle);
        }
        if (exportToExcelParam.getTrainTCount() == 8) {
            for (int j = cellCount - 4; j < cellCount; j++){
                drawCell(row, j, bundle.getString("exportexcel.departure"), "1", mainStyle);
            }
        }
        // ��������� ������
        for (int j = 0; j < stationsToExcel.size(); j++){
            row = sheet.createRow(rowNumber + 4 + j);
            HSSFCellStyle useStyle = mainStyle;
            // ��������� �.�. ������ ������
            if ((exportToExcelParam.getTrainType().equals(TrainType.town.getString())) && (exportToExcelParam.isRp()) && (stationsToExcel.get(j).getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint))){
                useStyle = boldStyle;
            }
            if (exportToExcelParam.getTrainTCount() == 4) {
                for (int k = 0; k < 4; k++){
                    if (k < 2){
                        drawTrain4(row, stationsToExcel.get(j), trainsToExcels, k * 2, useStyle, k);
                    } else {
                        drawTrain4(row, stationsToExcel.get(j), trainsToExcels, cellCount + (4 - k) * -2, useStyle, k);
                    }
                }
            }
            if (exportToExcelParam.getTrainTCount() == 8) {
                for (int k = 0; k < 8; k++){
                    if (k < 4){
                        drawTrain8(row, stationsToExcel.get(j), trainsToExcels, k, useStyle, k);
                    } else {
                        drawTrain8(row, stationsToExcel.get(j), trainsToExcels, cellCount + (k - 8), useStyle, k);
                    }
                }
            }
            drawCell(row,4,stationsToExcel.get(j).getName(),stationsToExcel.get(j).getName(),useStyle);
            drawCell(row,5,null,null,useStyle);
            if (exportToExcelParam.isESR()){
                drawCell(row,6,stationsToExcel.get(j).getCodeESR(),stationsToExcel.get(j).getCodeESR(),useStyle);
                if (exportToExcelParam.isExpress()){
                    drawCell(row,7,stationsToExcel.get(j).getCodeExpress(),stationsToExcel.get(j).getCodeExpress(),useStyle);
                    drawCell(row,8,stationsToExcel.get(j).getKm(),stationsToExcel.get(j).getKm(),useStyle);
                }
                else {
                    drawCell(row,7,stationsToExcel.get(j).getKm(),stationsToExcel.get(j).getKm(),useStyle);
                }
            }
            else {
                if (exportToExcelParam.isExpress()){
                    drawCell(row,6,stationsToExcel.get(j).getCodeExpress(),stationsToExcel.get(j).getCodeExpress(),useStyle);
                    drawCell(row,7,stationsToExcel.get(j).getKm(),stationsToExcel.get(j).getKm(),useStyle);
                }
                else {
                    drawCell(row,6,stationsToExcel.get(j).getKm(),stationsToExcel.get(j).getKm(),useStyle);
                }
            }
        }
        // ��������� ������. � ����� ��� ����������� ������ ������ �� ������� �� ������� ���������. ��� ����������� ���������� ������ ����������� � ��������� ������� �������)
        /*row = sheet.createRow(rowNumber + 4 + stationsToExcel.size());
        for (int j = 0; j < cellCount; j++){
            drawCell(row,j,null,null,mainStyle);
        }*/
        // ������ 3
        row = sheet.createRow(rowNumber + 2);
        // �������� �����
        for (int j = 0; j < cellCount; j++){
            drawCell(row,j,null,null,mainStyle);
        }
        //��������� ������ w, ���� �� = w
        if (exportToExcelParam.getTrainTCount() == 4) {
            for (int k = 0; k < 4; k++){
                String team;
                try {
                    team = localSpSt.get(k);
                } catch (Exception ex){
                    team = null;
                }
                if (k < 2){
                    drawCell(row, k * 2, team, team, usldlStyle);
                } else {
                    drawCell(row, cellCount + (4 - k) * -2, team, team, usldlStyle);
                }
            }
        }
        if (exportToExcelParam.getTrainTCount() == 8) {
            for (int k = 0; k < 8; k++){
                String team;
                try {
                    team = localSpSt.get(k);
                } catch (Exception ex){
                    team = null;
                }
                if (k < 4){
                    drawCell(row, k, team, team, usldlStyle);
                } else {
                    drawCell(row, cellCount + (k - 8), team, team, usldlStyle);
                }
            }
        }
        // ����������� �����
        sheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber,0,cellCount-3));
        sheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber,cellCount-2,cellCount-1));
        if (exportToExcelParam.getTrainTCount() == 4) {
            for (int j = rowNumber + 1; j <= rowNumber + 2; j++) {
                sheet.addMergedRegion(new CellRangeAddress(j, j, 0, 1));
                sheet.addMergedRegion(new CellRangeAddress(j, j, 2, 3));
                sheet.addMergedRegion(new CellRangeAddress(j, j, cellCount - 4, cellCount - 3));
                sheet.addMergedRegion(new CellRangeAddress(j, j, cellCount - 2, cellCount - 1));
            }
        }
        sheet.addMergedRegion(new CellRangeAddress(rowNumber + 1, rowNumber + 3, 4, 5));
        for (int j = rowNumber + 4; j < rowNumber + 4 + stationsToExcel.size(); j++){
            sheet.addMergedRegion(new CellRangeAddress(j, j,4,5));
        }
        // ������������ �������� �������
        for (int j = rowNumber; j < rowNumber + 5 + stationsToExcel.size(); j++){
            sheet.autoSizeColumn(j, true);
        }

        // �����������
        for (int i = 0; i < localComments.size(); i++){
            row = sheet.createRow(rowNumber + 6 + stationsToExcel.size() + i);
            drawCell(row,0,localComments.get(i),localComments.get(i),commentStyle);
        }
        commentRowCount += localComments.size();
    }

    private void drawTrain4(Row row, StationToExcel stationToExcel, List<TrainThread> trainsToExcels, int coll, HSSFCellStyle style, int train){
        if ((train + 1 <= trainsToExcels.size()) && (trainsToExcels.get(train).getCode() != null)) {
            try {
                String occOn = "";
                String occOff = "";
                String skSp = "";
                boolean isNullOn = false;
                boolean isNullOff = false;
                if (stationToExcel.getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)) {
                    occOn = trainsToExcels.get(train).getDistrTrain().getMapLineStTr().get(stationToExcel.getId()).getOccupyOn();
                    occOff = trainsToExcels.get(train).getDistrTrain().getMapLineStTr().get(stationToExcel.getId()).getOccupyOff();
                    skSp = trainsToExcels.get(train).getDistrTrain().getMapLineStTr().get(stationToExcel.getId()).getSk_ps();
                    if (skSp != null) skSp = skSp.trim();
                    isNullOff = trainsToExcels.get(train).getTimeSt(stationToExcel.getId()).isNullOff();
                    isNullOn = trainsToExcels.get(train).getTimeSt(stationToExcel.getId()).isNullOn();
                }
                //!!!!!!!! �.�.
                if (stationToExcel.getTypePoint().equals(DistrPoint.TypePoint.StopPoint)){
                    occOn = trainsToExcels.get(train).getDistrTrain().getMapTrainStop().get(stationToExcel.getId()).getOccupyOn();
                    occOff = trainsToExcels.get(train).getDistrTrain().getMapTrainStop().get(stationToExcel.getId()).getOccupyOff();
                    skSp = trainsToExcels.get(train).getDistrTrain().getMapTrainStop().get(stationToExcel.getId()).getSk_ps();
                }
                try {
                    // �������� �� ��������� ������� �������� � �� �� ������ ������� �����������
                    if ((!isNullOn) && ((!occOn.equals(occOff)) || (isNullOff))) {
                        drawCell(row, coll, occOn, trainsToExcels.get(train).getCode(), style);
                    }
                    else {
                        drawCell(row, coll, null, null, style);
                    }
                }
                catch (Exception ex){
                    drawCell(row, coll, null, null, style);
                }
                try {
                    // �������� �� �� ��������� ������� ��������
                    if (!isNullOff) {
                        boolean dopusk = true;
                        //if ((exportToExcelParam.isObgon()) && ((skSp.equals("*")) || ((exportToExcelParam.isAutoobgon()) && (deltaTime(occOn,occOff) > 3)))) {
                        if ((exportToExcelParam.isObgon()) && ((skSp.equals("*")) || ((exportToExcelParam.isAutoobgon()) && (TimeConverter.minutesBetween(occOn,occOff) > 3)))) {
                            localComments.add(bundle.getString("exportexcel.trainnumber_s") + trainsToExcels.get(train).getCode() +
                                    bundle.getString("exportexcel.bystation") + stationToExcel.getName() + " " + occOn + "-" + occOff + " " + exportToExcelParam.getTextPt());
                            drawCell(row, coll + 1, occOff + "*", trainsToExcels.get(train).getCode(), style);
                            dopusk = false;
                        }
                        if (skSp.equals("w")){
                            localComments.add(bundle.getString("exportexcel.trainnumber_w") + trainsToExcels.get(train).getCode() +
                                    " " + exportToExcelParam.getTextPtW() + " " + stationToExcel.getName() + " " + occOn + "-" + occOff);
                            localSpSt.put(train,"w");
                            drawCell(row, coll + 1, occOff + "w", trainsToExcels.get(train).getCode(), style);
                            dopusk = false;
                        }
                        if (dopusk) {
                            drawCell(row, coll + 1, occOff, trainsToExcels.get(train).getCode(), style);
                        }
                    } else {
                        drawCell(row, coll + 1, null, null, style);
                    }
                }
                catch (Exception ex){
                    drawCell(row, coll + 1, null, null, style);
                }
            } catch (Exception ex){
                drawCell(row, coll, null, null, style);
                drawCell(row, coll + 1, null, null, style);
            }
        }
        else {
            drawCell(row, coll, null, null, style);
            drawCell(row, coll + 1, null, null, style);
        }
    }

    private void drawTrain8 (Row row, StationToExcel stationToExcel, List<TrainThread> trainsToExcels, int coll, HSSFCellStyle style, int train){
        if ((train + 1 <= trainsToExcels.size()) && (trainsToExcels.get(train).getCode() != null)) {
            try {
                String occOn = "";
                String occOff = "";
                String skSp = "";
                if (stationToExcel.getTypePoint().equals(DistrPoint.TypePoint.SeparatePoint)) {
                    occOn = trainsToExcels.get(train).getDistrTrain().getMapLineStTr().get(stationToExcel.getId()).getOccupyOn();
                    occOff = trainsToExcels.get(train).getDistrTrain().getMapLineStTr().get(stationToExcel.getId()).getOccupyOff();
                    skSp = trainsToExcels.get(train).getDistrTrain().getMapLineStTr().get(stationToExcel.getId()).getSk_ps();
                    if (skSp != null) skSp = skSp.trim();
                }
                //!!!!!!!! �.�.
                if (stationToExcel.getTypePoint().equals(DistrPoint.TypePoint.StopPoint)){
                    occOn = trainsToExcels.get(train).getDistrTrain().getMapTrainStop().get(stationToExcel.getId()).getOccupyOn();
                    occOff = trainsToExcels.get(train).getDistrTrain().getMapTrainStop().get(stationToExcel.getId()).getOccupyOff();
                    skSp = trainsToExcels.get(train).getDistrTrain().getMapTrainStop().get(stationToExcel.getId()).getSk_ps();
                }
                if (skSp.equals("w")){
                    localComments.add(bundle.getString("exportexcel.trainnumber_w") + trainsToExcels.get(train).getCode() +
                            exportToExcelParam.getTextPtW() + stationToExcel.getName() + " " + occOn + "-" + occOff);
                    localSpSt.put(train,"w");
                    drawCell(row, coll, occOff + "w", trainsToExcels.get(train).getCode(), style);
                } else {
                    drawCell(row, coll, occOff, trainsToExcels.get(train).getCode(), style);
                }

            }
            catch (Exception ex){
                drawCell(row, coll, null, null, style);
            }
        } else {
            drawCell(row, coll, null, null, style);
        }
    }

    private void drawTrainCode4(Row row, List<TrainThread> trainsToExcels, int coll, int train){
        try {
            if (exportToExcelParam.getTrainType().equals(TrainType.town.getString())){
                String trainCode;
                int icolorTr = trainsToExcels.get(train).getColor();
                Color colorTr = ColorConverter.convert(icolorTr);
                if ((colorTr.getRed() == exportToExcelParam.getHolidayTrainColor().getRed()) && (colorTr.getGreen() == exportToExcelParam.getHolidayTrainColor().getGreen()) && (colorTr.getBlue() == exportToExcelParam.getHolidayTrainColor().getBlue())){
                    trainCode = trainsToExcels.get(train).getCode() + "*";
                }
                else {
                    trainCode = trainsToExcels.get(train).getCode();
                }
                drawCell(row, coll, trainCode, trainCode, trainStyle);
            }
            else {
                drawCell(row, coll, trainsToExcels.get(train).getCode(), trainsToExcels.get(train).getCode(), trainStyle);
            }
        }
        catch (Exception ex){
            drawCell(row, coll, null, null, trainStyle);
        }
        drawCell(row, coll + 1, null, null, trainStyle);
    }

    private void drawTrainCode8(Row row, List<TrainThread> trainsToExcels, int coll, int train){
        try {
            if (exportToExcelParam.getTrainType().equals(TrainType.town.getString())){
                String trainCode;
                int icolorTr = trainsToExcels.get(train).getColor();
                Color colorTr = ColorConverter.convert(icolorTr);
                if ((colorTr.getRed() == exportToExcelParam.getHolidayTrainColor().getRed()) && (colorTr.getGreen() == exportToExcelParam.getHolidayTrainColor().getGreen()) && (colorTr.getBlue() == exportToExcelParam.getHolidayTrainColor().getBlue())){
                    trainCode = trainsToExcels.get(train).getCode() + "*";
                }
                else {
                    trainCode = trainsToExcels.get(train).getCode();
                }
                drawCell(row, coll, trainCode, trainCode, trainStyle);
            }
            else {
                drawCell(row, coll, trainsToExcels.get(train).getCode(), trainsToExcels.get(train).getCode(), trainStyle);
            }
        }
        catch (Exception ex){
            drawCell(row, coll, null, null, trainStyle);
        }
    }

    private int deltaTime(String timeFirst, String timeSecond){
        int time1 = (Integer.parseInt(timeFirst.substring(0,1)) * 10 + Integer.parseInt(timeFirst.substring(1,2))) * 60 + Integer.parseInt(timeFirst.substring(3,4)) * 10 + Integer.parseInt(timeFirst.substring(4,5));
        int time2 = (Integer.parseInt(timeSecond.substring(0,1)) * 10 + Integer.parseInt(timeSecond.substring(1,2))) * 60 + Integer.parseInt(timeSecond.substring(3,4)) * 10 + Integer.parseInt(timeSecond.substring(4,5));
        return time2 - time1;
    }

    private void drawCell(Row row, int cellNum, String text, String condition, HSSFCellStyle style){
        cell = row.createCell(cellNum);
        if ((text != null) && (condition != null)) {
            cell.setCellValue(text);
        }
        cell.setCellStyle(style);
    }
}
