package com.gmail.alaerof.components.menu;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.gdpcalculation.CalculateOverTime;
import com.gmail.alaerof.javafx.dialog.actualgdp.ActualFXDialog;

import com.gmail.alaerof.javafx.dialog.actualgdp.Authorization;
import com.gmail.alaerof.javafx.dialog.calculategdp.CalculateGDPFXDialog;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.CalculateTimeEnergyFXDialog;
import com.gmail.alaerof.javafx.dialog.gdpprint.GDPPrintFXDialog;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.GDPPrintParam;
import com.gmail.alaerof.javafx.dialog.loadfromxml.LoadFromXMLFXDialog;
import com.gmail.alaerof.util.DataXMLUtil;
import com.gmail.alaerof.util.FileUtil;
import com.gmail.alaerof.xml.GDPXMLHandler;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Calendar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.w3c.dom.Document;

import static com.gmail.alaerof.application.GDPEdit.authorUserName;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

public class MenuGDP extends LoggerMenu {
    public MenuGDP() {
        super();
        String title = bundle.getString("components.menu.GDP");
        this.setText(title);
        {
            // ������ ��� (����������)
            title = bundle.getString("components.menu.GDP.calcGDP");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    calculateGDP();
                }
            });
        }
        {
            // ������ ����������� ���
            title = bundle.getString("components.menu.GDP.calcGDPTimeEnergy");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    calculateGDPTimeEnergy();
                }
            });
        }
        {
            // ������ �������
            title = bundle.getString("components.menu.GDP.calcOverTime");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    calculateOverTime();
                }
            });
        }
        {
            // ��������� � ����
            title = bundle.getString("components.menu.GDP.SaveGDPToXML");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    handleSaveGDPToXML();
                }
            });
        }
        {
            // ��������� �� �����
            title = bundle.getString("components.menu.GDP.LoadGDPFromXML");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    handleLoadGDPFromXML();
                }
            });
        }
        {
            // ������ (����) old
            title = bundle.getString("components.menu.GDP.printFileOld");
            JMenuItem menuItem = new JMenuItem(title);
            //this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    printFile();
                }
            });
        }
        {
            // ������ (�������) old
            title = bundle.getString("components.menu.GDP.printOld");
            JMenuItem menuItem = new JMenuItem(title);
            //this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    print();
                }
            });
        }
        // Paul M. Bui
        {
            // ������ �� �������/� ����
            title = bundle.getString("components.menu.GDP.printNew");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    newPrint();
                }
            });
        }
        {
            // Actual graph
            title = bundle.getString("components.menu.GDP.server");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    actualGDP();
                }
            });
        }
    }

    /**
     * ������ (����������) ���
     */
    private void calculateGDP() {
        try {
            DistrEditEntity distrEditEntity = null;
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                distrEditEntity = editPanel.getDistrEditEntity();
            } else {
                distrEditEntity = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            if (distrEditEntity != null) {
                GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.waitCursor);
                // ���� ������ �� ���������, �� ����������� �� �� � ������ �����������, �.�. ��� ���������� �����������
                if (!distrEditEntity.listTrainLoaded()) {
                    distrEditEntity.loadListDistrTrain(0, 24, null, false);

                }
                GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.defCursor);
                CalculateGDPFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getCalculateGDPFXDialog();
                dialog.setModal(true);
                dialog.setDialogContent(distrEditEntity);
                dialog.setVisible(true);
            } else {
                String mess = bundle.getString("components.menu.GDP.selectDistr");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ������� ��������� � �������������� ����������� ���
     */
    private void calculateGDPTimeEnergy() {
        try {
            DistrEditEntity distrEditEntity = null;
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                distrEditEntity = editPanel.getDistrEditEntity();
            } else {
                distrEditEntity = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            if (distrEditEntity != null) {
                CalculateTimeEnergyFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getCalculateTimeEnergyFXDialog();
                dialog.setModal(true);
                dialog.setDialogContent(distrEditEntity);
                dialog.setVisible(true);
            } else {
                String mess = bundle.getString("components.menu.GDP.selectDistr");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ������ �������
     */
    private void calculateOverTime() {
        try {
            DistrEditEntity distrEditEntity = null;
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                distrEditEntity = editPanel.getDistrEditEntity();
            } else {
                String mess = bundle.getString("components.menu.GDP.openEditPanel");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            }
            if (distrEditEntity != null) {
                if (distrEditEntity.getListTrains().size() > 0) {
                    GDPEdit.gdpEdit.setWaitCursor();
                    logger.debug("calculateOverTimeAllTrains - " + distrEditEntity.getDistrEntity().getDistrName());
                    String warn = CalculateOverTime.calculateOverTimeAllTrains(distrEditEntity.getListTrains());
                    GDPEdit.gdpEdit.setDefaultCursor();

                    if (!warn.isEmpty()) {
                        String mess = bundle.getString("components.menu.GDP.calc.overtime.warn");
                        JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
                    } else {
                        JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                                bundle.getString("components.menu.GDP.ok"));
                    }
                } else {
                    String mess = bundle.getString("components.menu.GDP.loadTrainFromDB");
                    JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
                }
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        } finally {
            GDPEdit.gdpEdit.setDefaultCursor();
        }
    }

    // Paul M. Bui
    private void newPrint() {
        try {
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                GDPPrintFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getGDPPrintFXDialog();
                dialog.setModal(true);
                dialog.setDialogContent(editPanel);
                dialog.setVisible(true);
            } else {
                String mess = bundle.getString("components.menu.GDP.openEditPanel");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void actualGDP() {
        try {
            DistrEditEntity distrEditEntity = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            if (distrEditEntity != null) {
                if (authorUserName.length() == 0) {
                    if (!Authorization.check()) {
                        String mess = bundle.getString("components.menu.GDP.authorError");
                        String title = bundle.getString("components.menu.GDP.authorTitle");
                        JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess, title, INFORMATION_MESSAGE);
                        return;
                    }
                }
                ActualFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getActualFXDialog();
                dialog.setSize(1000, 600);
                dialog.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width) / 2 - dialog.getWidth() / 2,
                        (Toolkit.getDefaultToolkit().getScreenSize().height) / 2 - dialog.getHeight() / 2);
                dialog.setModal(true);
                dialog.setDialogContent(distrEditEntity);
                dialog.setVisible(true);
            } else {
                String mess = bundle.getString("components.menu.GDP.selectDistr");
                String title = bundle.getString("components.menu.GDP.server");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess, title, INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void handleSaveGDPToXML() {
        DistrEditEntity distrEditEntity = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
        if (distrEditEntity != null) {
            long tm1 = Calendar.getInstance().getTimeInMillis();
            String mess = bundle.getString("components.menu.GDP.ok");
            try {
                String message = bundle.getString("components.menu.GDP.saveGDPToDBBeforeXML");
                String title = bundle.getString("components.menu.GDP.warning");
                int confirmResult = JOptionPane.showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), message,
                        title, JOptionPane.YES_NO_OPTION);
                boolean save = confirmResult == 0;
                if (save) {
                    String distrName = distrEditEntity.getDistrEntity().getDistrName();
                    String fileName = distrName + "_GDP.xml";
                    File file = FileUtil.chooseFile(FileUtil.Mode.save, null, fileName, GDPEdit.gdpEdit.getMainFrame(),
                            new FileNameExtensionFilter("XML files", "xml"));
                    if (file != null && file.exists()) {
                        message = bundle.getString("components.menu.GDP.saveGDPFileExists");
                        title = bundle.getString("components.menu.GDP.warning");
                        confirmResult = JOptionPane.showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), message,
                                title, JOptionPane.YES_NO_OPTION);
                        save = confirmResult == 0;
                    }
                    if (file != null && save) {
                        GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.waitCursor);
                        try {
                            GDPXMLHandler.saveGDPToXML(file, distrEditEntity.getIdDistr());
                        } finally {
                            GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.defCursor);
                        }
                    }
                }
            } catch (Exception e) {
                mess = bundle.getString("components.menu.GDP.saveError");
                logger.error(e.toString(), e);
            }
            long tm2 = Calendar.getInstance().getTimeInMillis();
            double sec = (double) (tm2 - tm1) / 1000.0;
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess
                    + bundle.getString("components.menu.GDP.spend") + sec);
        } else {
            String mess = bundle.getString("components.menu.GDP.selectDistr");
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
        }
    }

    private void handleLoadGDPFromXML() {
        try {
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                DistrEditEntity distrEditEntity = editPanel.getDistrEditEntity();

                File file = FileUtil.chooseFile(FileUtil.Mode.open, null, "", GDPEdit.gdpEdit.getMainFrame(),
                        new FileNameExtensionFilter("XML files", "xml"));
                if (file != null && file.exists()) {
                    Document doc = DataXMLUtil.loadDocument(file);
                    if (GDPXMLHandler.firstCheck4Format(doc)) {
                        String distrName = GDPXMLHandler.firstCheck4Distr(doc, distrEditEntity);
                        boolean load = true;
                        if (distrName.length() > 0) {
                            //��������������
                            String title = bundle.getString("components.menu.GDP.warning");
                            String warn1 = bundle.getString("components.menu.GDP.warning.distrmatch1");
                            String warn2 = bundle.getString("components.menu.GDP.warning.distrmatch2");
                            String message = warn1 + " '" + distrName + "' " +
                                    warn2 + " '" + distrEditEntity.getDistrEntity().getDistrName() + "' ?";
                            int n = JOptionPane.showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), message, title,
                                    JOptionPane.YES_NO_OPTION);
                            load = n == 0;
                        }
                        if (load) {
                            logger.debug("load GDP from file: " + file);
                            LoadFromXMLFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getLoadFromXMLFXDialog();
                            dialog.setModal(true);
                            dialog.setDialogContent(doc, distrEditEntity, file.toString());
                            dialog.setVisible(true);
                        }
                    } else {
                        String mess = bundle.getString("components.menu.GDP.NotDistrGDP.Error");
                        JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
                    }
                }
            } else {
                String mess = bundle.getString("components.menu.GDP.openEditPanel");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            }
        } catch (Exception e) {
            String mess = bundle.getString("components.menu.GDP.loadError");
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            logger.error(e.toString(), e);
        }
    }

    private void print() {
        try {
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                DistrEditEntity distrEditEntity = editPanel.getDistrEditEntity();
                try {
                    GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.waitCursor);
                    int hourB = 23;
                    int hourE = 18;
                    //������ ��� ��� ������� ��� ������ ���� �����: editPanel.printGDP(gdpPrintParam);
                    //editPanel.printGDP(hourB, hourE);
                    distrEditEntity.setNoJoint();
                } finally {
                    GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.defCursor);
                }
            } else {
                String mess = bundle.getString("components.menu.GDP.openEditPanel");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void printFile() {
        try {
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                DistrEditEntity distrEditEntity = editPanel.getDistrEditEntity();
                String fileName = distrEditEntity.getDistrEntity().getDistrName() + ".png";
                File file = FileUtil.chooseFile(FileUtil.Mode.save, null, fileName, GDPEdit.gdpEdit.getMainFrame(),
                        new FileNameExtensionFilter("PNG files", "png"));
                if (file != null) {
                    try {
                        GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.gdpEdit.waitCursor);
                        GDPPrintParam printParam = new GDPPrintParam();
                        printParam.setHourBeg(17);
                        printParam.setHourEnd(10);
                        editPanel.printGDP(file, printParam);
                        distrEditEntity.setNoJoint();
                    } finally {
                        GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.gdpEdit.defCursor);
                    }
                }
            } else {
                String mess = bundle.getString("components.menu.GDP.openEditPanel");
                JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
