package com.gmail.alaerof.javafx.dialog.energyfromtr.model;

import com.gmail.alaerof.entity.DistrStation;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StationExModel {

    private DistrStation distrStation;
    // Name
    private StringProperty nameSt;
    // CodeESR
    private StringProperty codeESR;
    // CodeESR2
    private StringProperty codeESR2;
    // IDstation
    private IntegerProperty idStation;

    public StationExModel(DistrStation distrStation) {
        this.distrStation = distrStation;
        this.nameSt = new SimpleStringProperty(distrStation.getStation().getName());
        this.codeESR = new SimpleStringProperty(distrStation.getStation().getCodeESR());
        this.codeESR2 = new SimpleStringProperty(distrStation.getStation().getCodeESR2());
        this.idStation = new SimpleIntegerProperty(distrStation.getStation().getIdSt());
    }


    public StringProperty nameStProperty() {
        return nameSt;
    }
    public StringProperty codeESRProperty() {
        return codeESR;
    }
    public StringProperty codeESR2Property() {
        return codeESR2;
    }
    public IntegerProperty idStationProperty() {return idStation;}
}
