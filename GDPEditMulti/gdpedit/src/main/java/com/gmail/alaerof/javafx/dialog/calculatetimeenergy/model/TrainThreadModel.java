package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainExpense;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.javafx.control.RoundedDoubleProperty;
import com.gmail.alaerof.util.FileUtil;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

public class TrainThreadModel {
    private TrainThread trainThread;
    // code
    private IntegerProperty codeTR;
    // Name
    private StringProperty nameTR;
    // L ����������, ���������� ������� �� �������, �
    private DoubleProperty len;
    // Tu ����� ���������� ������ �� �������, ���
    private DoubleProperty timeWithStop;
    // Tt ����� ���������� ������ �� ������� ��� ����� ������� �������, ���
    private DoubleProperty timeWithoutStop;
    // �� �������� ��/�
    private DoubleProperty speedWithStop;
    // ��� �������� ��/�
    private DoubleProperty speedWithoutStop;

    // ������� �� ���������� ���
    DistrTrainExpense  expense;
    private StringProperty expenseName;
    /** ����������� (�� ���������) ����� ���������� ������ �� ������� � ��������� � ������������, ��� */
    private DoubleProperty timeNormAll;
    /** ����������� (�� ���������) ����� ���������� ������ �� ������� ����� ��� �������� � ����������, ��� */
    private DoubleProperty timeNormMove;
    /***/
    private IntegerProperty stopCount;
    // ����������� �������� ��/�
    private DoubleProperty speedNormMove;
    /** Eu ������� ������� � ������ �������, */
    private DoubleProperty energyWithStop;
    /** Et ������� ������� ��� ����� ������� (�� � ������ �������� � ����������),  */
    private DoubleProperty energyWithoutStop;
    /** Et ������� ������� ��� ����� �������, �������� � ����������,  */
    private DoubleProperty energyMove;

    // ������� �� ����� �������, ���
    /** ������� �� ������-���� � �������� */
    private DoubleProperty moveExpense;
    /** ������� �� ������-���� ������� */
    private DoubleProperty stopExpense;
    /** ������� �� ������ � ���������� */
    private DoubleProperty downUpExpense;
    /** ����������� ������� */
    private DoubleProperty normExpense;
    /** ����������� ������������� */
    private DoubleProperty effect;
    /** Vtex/Vuch */
    private DoubleProperty vv;

    public TrainThreadModel(){
        this.codeTR = new SimpleIntegerProperty();
        this.nameTR = new SimpleStringProperty();
        this.timeWithStop = new RoundedDoubleProperty();
        this.timeWithoutStop = new RoundedDoubleProperty();
        this.stopCount = new SimpleIntegerProperty();
        this.len = new RoundedDoubleProperty();
        this.speedWithStop = new RoundedDoubleProperty(0);
        this.speedWithoutStop = new RoundedDoubleProperty(0);

        expenseName = new SimpleStringProperty();

        timeNormAll = new RoundedDoubleProperty();
        timeNormMove = new RoundedDoubleProperty();
        speedNormMove = new RoundedDoubleProperty();
        energyWithStop = new RoundedDoubleProperty();
        energyWithoutStop = new RoundedDoubleProperty();
        energyMove = new RoundedDoubleProperty();

        moveExpense = new RoundedDoubleProperty();
        stopExpense = new RoundedDoubleProperty();
        downUpExpense = new RoundedDoubleProperty();
        normExpense = new RoundedDoubleProperty();
        effect = new RoundedDoubleProperty();
        vv = new RoundedDoubleProperty();
    }

    public TrainThreadModel(TrainThread trainThread) {
        this();
        this.trainThread = trainThread;
        DistrTrain distrTrain = trainThread.getDistrTrain();
        this.codeTR.set(trainThread.getCodeInt());
        this.nameTR.set(distrTrain.getNameTR());
        this.timeWithStop.set(distrTrain.getTimeWithStop());
        this.timeWithoutStop.set(distrTrain.getTimeWithoutStop());
        this.stopCount.set(distrTrain.getStopCount());
        this.len.set(distrTrain.getLen());
        calculateSpeedWithStop();
        calculateSpeedWithoutStop();
        calcVV();
    }

    private void calcVV() {
        if (this.speedWithStop.doubleValue() > 0) {
            double v = this.speedWithoutStop.doubleValue() / this.speedWithStop.doubleValue();
            this.vv.set(v);
        }
    }

    public int getStopCount() {
        return stopCount.get();
    }

    public IntegerProperty stopCountProperty() {
        return stopCount;
    }

    public DistrTrainExpense getExpense() {
        return expense;
    }

    public void setExpense(DistrTrainExpense expense) {
        this.expense = expense;

        timeNormAll.set(expense.getTimeNormAll());
        timeNormMove.set(expense.getTimeNormMove());
        energyWithStop.set(expense.getEnergyWithStop());
        energyWithoutStop.set(expense.getEnergyWithoutStop());
        energyMove.set(expense.getEnergyMove());

        expenseName.set(expense.getExpenseKeyString());

        moveExpense.set(expense.getMoveExpense());
        stopExpense.set(expense.getStopExpense());
        downUpExpense.set(expense.getDownUpExpense());
        normExpense.set(expense.getNormExpense());

        effect.set(getEffect());
        calculateSpeeNormMove();
    }

    public long getIdTrain() {
        return trainThread.getIdTrain();
    }

    public int getCodeTR() {
        return codeTR.get();
    }

    public IntegerProperty codeTRProperty() {
        return codeTR;
    }

    public void setCodeTR(int codeTR) {
        this.codeTR.set(codeTR);
    }

    public String getNameTR() {
        return nameTR.get();
    }

    public StringProperty nameTRProperty() {
        return nameTR;
    }

    public void setNameTR(String nameTR) {
        this.nameTR.set(nameTR);
    }

    public double getTimeWithStop() {
        return timeWithStop.get();
    }

    public DoubleProperty timeWithStopProperty() {
        return timeWithStop;
    }

    public void setTimeWithStop(double timeWithStop) {
        this.timeWithStop.set(timeWithStop);
        calculateSpeedWithStop();
        calcVV();
    }

    public double getTimeWithoutStop() {
        return timeWithoutStop.get();
    }

    public DoubleProperty timeWithoutStopProperty() {
        return timeWithoutStop;
    }

    public void setTimeWithoutStop(double timeWithoutStop) {
        this.timeWithoutStop.set(timeWithoutStop);
        calculateSpeedWithoutStop();
        calcVV();
    }

    public double getLen() {
        return len.get();
    }

    public DoubleProperty lenProperty() {
        return len;
    }

    public void setLen(double len) {
        this.len.set(len);
        calculateSpeedWithStop();
        calculateSpeedWithoutStop();
        calcVV();
    }

    public double getSpeedWithStop() {
        return speedWithStop.get();
    }

    public DoubleProperty speedWithStopProperty() {
        return speedWithStop;
    }

    public double getSpeedWithoutStop() {
        return speedWithoutStop.get();
    }

    public DoubleProperty speedWithoutStopProperty() {
        return speedWithoutStop;
    }

    public double getTimeNormAll() {
        return timeNormAll.get();
    }

    public DoubleProperty timeNormAllProperty() {
        return timeNormAll;
    }

    public double getTimeNormMove() {
        return timeNormMove.get();
    }

    public DoubleProperty timeNormMoveProperty() {
        return timeNormMove;
    }

    public String getExpenseName() {
        return expenseName.get();
    }

    public StringProperty expenseNameProperty() {
        return expenseName;
    }

    public double getSpeedNormMove() {
        return speedNormMove.get();
    }

    public DoubleProperty speedNormMoveProperty() {
        return speedNormMove;
    }

    public double getEnergyWithStop() {
        return energyWithStop.get();
    }

    public DoubleProperty energyWithStopProperty() {
        return energyWithStop;
    }

    public double getEnergyWithoutStop() {
        return energyWithoutStop.get();
    }

    public DoubleProperty energyWithoutStopProperty() {
        return energyWithoutStop;
    }

    public double getEnergyMove() {
        return energyMove.get();
    }

    public DoubleProperty energyMoveProperty() {
        return energyMove;
    }

    public double getMoveExpense() {
        return moveExpense.get();
    }

    public DoubleProperty moveExpenseProperty() {
        return moveExpense;
    }

    public double getStopExpense() {
        return stopExpense.get();
    }

    public DoubleProperty stopExpenseProperty() {
        return stopExpense;
    }

    public double getDownUpExpense() {
        return downUpExpense.get();
    }

    public DoubleProperty downUpExpenseProperty() {
        return downUpExpense;
    }

    public double getNormExpense() {
        return normExpense.get();
    }

    public DoubleProperty normExpenseProperty() {
        return normExpense;
    }

    public double getEffect() {
        double value = (getMoveExpense() + getStopExpense() + getDownUpExpense());

        if (value > 0) {
            value = getNormExpense() / value;
        }
        return value;
    }

    public DoubleProperty effectProperty() {
        effect.set(getEffect());
        return effect;
    }

    private void calculateSpeedWithStop() {
        if (getTimeWithStop() > 0) {
            double h = getTimeWithStop()/60.0;
            double km = getLen()/1000.0;
            double sp = km/h;
            speedWithStop.set(sp);
        } else {
            speedWithStop.set(0);
        }
    }
    private void calculateSpeedWithoutStop() {
        if (getTimeWithoutStop() > 0) {
            double h = getTimeWithoutStop()/60.0;
            double km = getLen()/1000.0;
            double sp = km/h;
            speedWithoutStop.set(sp);
        } else {
            speedWithoutStop.set(0);
        }
    }
    private void calculateSpeeNormMove() {
        if (getTimeNormMove() > 0) {
            double h = getTimeNormMove()/60.0;
            double km = getLen()/1000.0;
            double sp = km/h;
            speedNormMove.set(sp);
        } else {
            speedNormMove.set(0);
        }
    }

    public String getTabValuesString() {
        String s = FileUtil.CSV_SEPARATOR;
        String str = "";
        if (this.expense == null) {
            str = str +
                    s + codeTR.get() +
                    s + nameTR.get() +
                    s + len.get() +
                    s + timeWithStop.get() +
                    s + timeWithoutStop.get() +
                    s + speedWithStop.get() +
                    s + speedWithoutStop.get();
        }
        if (this.expense != null) {
            str = str + s + s + s + s + s + s + s +
                    s + expenseName.get() +
                    s + timeNormMove.get() +
                    s + speedNormMove.get() +
                    s + energyWithStop.get() +
                    s + energyWithoutStop.get() +
                    s + energyMove.get() +
                    s + stopCount.get() +
                    s + moveExpense.get() +
                    s + stopExpense.get() +
                    s + downUpExpense.get() +
                    s + normExpense.get() +
                    s + getEffect();
        }
        str = str.replaceAll("\\.",",");
        return str;
    }

    public List<Object> getTabValuesList() {
        List<Object> list = new ArrayList<>();
       // if (this.expense == null) {  // �������� ��� ������ ��������� ������ ������ � ������������������� �������.
            list.add(codeTR.getValue());
            list.add(nameTR.getValue());
            list.add(len.getValue());
            list.add(timeWithStop.getValue());
            list.add(timeWithoutStop.getValue());
            list.add(speedWithStop.getValue());
            list.add(speedWithoutStop.getValue());
     //   }
        return list;
    }

    public List<Object> getExpenseTabValuesList() {
        List<Object> list = new ArrayList<>();
        if (this.expense != null) {
            list.add(expenseName.getValue());
            list.add(timeNormMove.getValue());
            list.add(speedNormMove.getValue());
            list.add(energyWithStop.getValue());
            list.add(energyWithoutStop.getValue());
            list.add(energyMove.getValue());
            list.add(stopCount.getValue());
            list.add(moveExpense.getValue());
            list.add(downUpExpense.getValue());
            list.add(stopExpense.getValue());
            list.add(normExpense.getValue());
            list.add(getEffect());
        }
        return list;
    }

    @Override
    public String toString() {
        return "TrainThreadModel{" +
                "trainThread=" + trainThread +
                ", codeTR=" + codeTR +
                ", nameTR=" + nameTR +
                ", len=" + len +
                ", timeWithStop=" + timeWithStop +
                ", timeWithoutStop=" + timeWithoutStop +
                ", speedWithStop=" + speedWithStop +
                ", speedWithoutStop=" + speedWithoutStop +
                ", expense=" + expense +
                ", expenseName=" + expenseName +
                ", timeNormAll=" + timeNormAll +
                ", timeNormMove=" + timeNormMove +
                ", stopCount=" + stopCount +
                ", speedNormMove=" + speedNormMove +
                ", energyWithStop=" + energyWithStop +
                ", energyWithoutStop=" + energyWithoutStop +
                ", energyMove=" + energyMove +
                ", moveExpense=" + moveExpense +
                ", stopExpense=" + stopExpense +
                ", downUpExpense=" + downUpExpense +
                ", normExpense=" + normExpense +
                ", effect=" + effect +
                ", vv=" + vv +
                '}';
    }
}
