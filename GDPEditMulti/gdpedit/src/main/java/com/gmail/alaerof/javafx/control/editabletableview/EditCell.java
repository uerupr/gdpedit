package com.gmail.alaerof.javafx.control.editabletableview;

import java.util.function.Function;
import java.util.regex.Pattern;
import javafx.beans.value.WritableValue;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Parent class for any editable table cell
 * @param <T> Entity with property-fields to be edited in TableView
 * @param <V> Certain Simple type
 * @param <P> Certain Property type
 */
public abstract class EditCell<T, V, P> extends TableCell<T, V> {
    /** logger */
    protected Logger logger;
    /**
     * {@link TextField} to edit property-field
     */
    protected final TextField textField = new TextField();
    /**
     * Property to be able to edit in cell
     */
    protected final Function<T, P> property;

    {
        logger = LogManager.getLogger(this.getClass());
    }
    /**
     * Constructor
     * @param property {@link Function<T, P>} property to be able to edit in cell
     */
    public EditCell(Function<T, P> property) {
        this.property = property;

        bindItemProperty();

        setGraphic(textField);
        setContentDisplay(ContentDisplay.TEXT_ONLY);

        textField.setOnAction(evt -> {
            processEdit();
        });

        textField.focusedProperty().addListener((obs, wasFocused, isNowFocused) -> {
            if (! isNowFocused) {
                processEdit();
            }
        });
    }

    /**
     * Process edit if the text in text field is correct
     */
    private void processEdit() {
        String text = textField.getText();
        if (isCorrectInput(text)){
            commitEdit(parseText(text));
        } else {
            cancelEdit();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startEdit() {
        super.startEdit();
        Object value = getItem();
        if (value != null) {
            textField.setText(value.toString());
        }
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        textField.requestFocus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    /**
     * {@inheritDoc}
     * @param value input value
     */
    @Override
    public void commitEdit(V value) {
        super.commitEdit(value);
        T entity = getTableView().getItems().get(getIndex());
        P cellProperty = property.apply(entity);
        ((WritableValue<V>) cellProperty).setValue(value);
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    /**
     * Checks if input string matches certain simple type pattern
     * @param text input string
     * @return true or false
     */
    private boolean isCorrectInput(String text) {
        Pattern patternV = getSimpleTypePattern();
        if (patternV == null) {
            return true; // in case String
        }
        return patternV.matcher(text).matches();
    }

    /**
     * Binds item property
     */
    protected abstract void bindItemProperty();

    /**
     * Parses string to V
     * @param text string to be parsed to V value
     * @return V value
     */
    protected abstract V parseText(String text);

    /**
     * Returns pattern for certain simple type V
     * @return {@link Pattern} pattern for certain simple type V
     */
    protected abstract Pattern getSimpleTypePattern();
}
