package com.gmail.alaerof.javafx.dialog.actualgdp.view;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.common.LoadGDPXMLConfig;
import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.gdp.GDParm;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.ICategoryDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrGDPXMLDAO;
import com.gmail.alaerof.dao.interfacedao.IGDParmDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.sql.PoolManagerException;
import com.gmail.alaerof.dao.sql.impl.SQLQueryGDParm;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.util.DataXMLUtil;
import com.gmail.alaerof.xml.GDPXMLHandler;
import com.gmail.alaerof.xml.loadgdp.DistrStruct;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.gmail.alaerof.application.GDPEdit.authorUserName;
import static com.gmail.alaerof.dao.sql.impl.GDParmDAOSQL.GDP;
import static com.gmail.alaerof.javafx.dialog.TaskWithCursorExecutor.executeTaskWithCursor;
import static com.gmail.alaerof.javafx.dialog.TaskWithCursorExecutor.executeTaskWithCursorAndAlert;
import static javax.swing.JOptionPane.showConfirmDialog;

public class ActualLayoutController implements Initializable {
  public static final int ARCH = 1;
  public static final int ACTUAL = 0;
  private final String NAME_TEMP_FILE = "temp.xml";
  protected Logger logger = LogManager.getLogger(ActualLayoutController.class);
  protected ResourceBundle bundle;
  private Scene scene;
  private DistrEditEntity distrEntity;

  @FXML
  private Label labelTitle;

  @FXML
  private TableView<GDParm> tableView;

  @FXML
  private TableColumn<GDParm, String> normColumn;

  @FXML
  private TableColumn<GDParm, String> descriptionColumn;
  @FXML
  private TableColumn<GDParm, String> dateBeginColumn;
  @FXML
  private TableColumn<GDParm, String> dateEndColumn;
  @FXML
  private TableColumn<GDParm, String> dateOnColumn;
  @FXML
  private TableColumn<GDParm, String> authorColumn;
  @FXML
  private TableColumn<GDParm, String> dateKTCColumn;
  @FXML
  private TextField descriptionText;
  @FXML
  private TextField dateBeginText;
  @FXML
  private TextField dateEndText;
  @FXML
  private TextField dateOnText;
  @FXML
  private TextField authorText;
  @FXML
  private TextField yearText;
  @FXML
  private RadioButton radioButton1;
  @FXML
  private RadioButton radioButton2;
  @FXML
  private RadioButton radioButton3;
  @FXML
  private RadioButton radioButton4;
  @FXML
  private RadioButton radioButton5;
  @FXML
  private CheckBox normative;
  @FXML
  private CheckBox sendktc;
  @FXML
  private TextArea noteText;
  
  ToggleGroup group1 = new ToggleGroup();

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    logger.debug("initialize: location = " + location + " resources = " + resources);
    bundle = resources;
    tableView.getSelectionModel().selectedIndexProperty().addListener(new RowSelectChangeListener());
    initRadioButton();
  }

  private void initRadioButton() {
    radioButton1.setToggleGroup(group1);
    radioButton1.setUserData(GDP.ACTUAL);
    radioButton2.setToggleGroup(group1);
    radioButton2.setUserData(GDP.ARCHIVE);
    radioButton3.setToggleGroup(group1);
    radioButton3.setUserData(GDP.NORMATIVE);
    radioButton4.setToggleGroup(group1);
    radioButton4.setUserData(GDP.VARIANT);
    radioButton5.setToggleGroup(group1);
    radioButton5.setUserData(GDP.ALL);
    radioButton5.setSelected(true);
    group1.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
      if (new_toggle != null) {
        tableView.setItems(FXCollections.observableArrayList(getGDP(distrEntity.getIdDistr(), (GDP) new_toggle.getUserData())));
      }
    });
    sendktc.selectedProperty().addListener(new ChangeListener<Boolean>() {
      @Override
      public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
        yearText.setDisable(!newValue);
        noteText.setDisable(!newValue);
      }
    });
  }


  private class RowSelectChangeListener implements ChangeListener {
    @Override
    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
      GDParm selectedItem = tableView.getSelectionModel().getSelectedItem();
      if (selectedItem == null) return;
      descriptionText.setText(selectedItem.getDescription());
      dateBeginText.setText(selectedItem.getDateBegin());
      dateEndText.setText(selectedItem.getDateEnd());
      dateOnText.setText(selectedItem.getDateOn());
      //authorText.setText(selectedItem.getAuthor());
    }
  }

  @FXML
  private void sendXML(ActionEvent event) {
    Task task = new Task<Void>() {
      @Override
      protected Void call() {
        GDPXMLHandler.saveGDPToXML(new File(NAME_TEMP_FILE), distrEntity.getIdDistr());
        try {
          addTagKTCtoXML(NAME_TEMP_FILE);
        } catch (ParserConfigurationException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        } catch (SAXException e) {
          e.printStackTrace();
        } catch (TransformerException e) {
          e.printStackTrace();
        }
        return null;
      }
    };
    executeTaskWithCursorAndAlert(task, scene, null);
  }

  private void addTagKTCtoXML(String name_temp_file) throws ParserConfigurationException, IOException, SAXException, TransformerException {
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    Document document = documentBuilder.parse(name_temp_file);

    NodeList tagsFileInfo = document.getElementsByTagName("file_info");
    Node tagFileInfo;
    if (tagsFileInfo.getLength() != 0) {
      tagFileInfo = tagsFileInfo.item(0);
    } else {
      tagFileInfo = document.createElement("file_info");
      document.getFirstChild().appendChild(tagFileInfo);
    }

    Element tagLogin  = document.createElement("login");
    tagLogin.setAttribute("name", "graf");
    tagFileInfo.appendChild(tagLogin);
    Element tagKTC = document.createElement("ktc");
    tagKTC.setAttribute("yearGDP", "2019");
    tagKTC.setAttribute("commit", "");
    tagKTC.setAttribute("tip_change", "add_only");
    tagFileInfo.appendChild(tagKTC);

    DOMSource source = new DOMSource(document);

    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    StreamResult result = new StreamResult(name_temp_file);
    transformer.transform(source, result);
  }

  @FXML
  private void replaceXML(ActionEvent event) {
    GDParm selectedGDP = tableView.getSelectionModel().getSelectedItem();
    if (selectedGDP == null || selectedGDP.getGdpXML() == null) showMessageDontSelectGDP();
    Task task = new Task<Void>() {
      @Override
      protected Void call() {
        try {
          saveGDPToXML(selectedGDP.getGdpXML());
          loadGDPFromXML();
        } catch (PoolManagerException e) {
          e.printStackTrace();
        } catch (DataManageException e) {
          e.printStackTrace();
        }
        return null;
      }
    };
    executeTaskWithCursorAndAlert(task, scene, null);
  }

  private void showMessageDontSelectGDP() {
    String title = bundle.getString("javafx.dialog.actualgdp.warning");
    String message = bundle.getString("javafx.dialog.actualgdp.requiredchoosegdp");
    showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), message, title,
            JOptionPane.DEFAULT_OPTION);
  }

  @FXML
  private void deleteGDP(ActionEvent event) {
    String title = bundle.getString("javafx.dialog.actualgdp.warning");
    String warn1 = bundle.getString("javafx.dialog.actualgdp.youwantremovegdp");
    String message = warn1 + " ?";
    int n = showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), message, title,
            JOptionPane.YES_NO_OPTION);
    boolean isOk = n == 0;
    if (isOk) {
      updateGDP(ARCH);
    }
  }

  @FXML
  private void restoreGDP(ActionEvent event) {
    updateGDP(ACTUAL);
  }

  @FXML
  private void saveGDP() {
    String title = bundle.getString("javafx.dialog.actualgdp.warning");
    String warn1 = bundle.getString("javafx.dialog.actualgdp.youwantsavegdpserver");
    String message = warn1 + " ?";
    int n = showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), message, title,
            JOptionPane.YES_NO_OPTION);
    boolean save = n == 0;
    if (save) {
      try {
        Task task = new Task<Void>() {
          @Override
          protected Void call() {
            saveGDPServer();
            return null;
          }
        };
        executeTaskWithCursorAndAlert(task, scene, null);
      } finally {
        logger.debug("save GDP to server done");
      }
    }
  }

  private void saveGDPServer() {
    GDParm selectedGDP = tableView.getSelectionModel().getSelectedItem();
    if (selectedGDP == null) return;

    Long idDistr = Long.valueOf(distrEntity.getIdDistr());
    boolean ext = true;
    if (descriptionText.getText().length() == 0) {
      ext = false;
    }
    if (dateBeginText.getText().length() == 0) {
      ext = false;
    }
    if (dateEndText.getText().length() == 0) {
      ext = false;
    }
    if (dateOnText.getText().length() == 0) {
      ext = false;
    }
    if (authorText.getText().length() == 0) {
      ext = false;
    }
    if (!ext) return;
    SQLQueryGDParm SQLQueryGDParm = GDPDAOFactoryCreater.getGDPDAOFactory().getSQLGDParm();
    try {
      if (SQLQueryGDParm.selectGDParm(distrEntity.getIdDistr(), dateBeginText.getText(), dateEndText.getText())) {
        showMessageGDPExists();
        return;
      }
    } catch (PoolManagerException e) {
      e.printStackTrace();
    }
    GDParm gdParm = new GDParm();
    gdParm.setDateBegin(dateBeginText.getText());
    gdParm.setDateEnd(dateEndText.getText());
    gdParm.setDateOn(dateOnText.getText());
    gdParm.setDescription(descriptionText.getText());
    gdParm.setNorm(normative.isSelected());

    String src = descriptionText.getText() + "; " +
            bundle.getString("javafx.dialog.actualgdp.begin") + dateBeginText.getText() + "; " +
            bundle.getString("javafx.dialog.actualgdp.end") + dateEndText.getText() + "; " +
            bundle.getString("javafx.dialog.actualgdp.save") + dateOnText.getText() + "; " +
            authorText.getText() + "; " +
            bundle.getString("javafx.dialog.actualgdp.normative");//�������� ����� ���������� � FX

    GDPXMLHandler.saveGDPToXML(new File(NAME_TEMP_FILE), distrEntity.getIdDistr());
    String gdpXML = "";
    try {
      //windows-1251
      gdpXML = new String(Files.readAllBytes(Paths.get(NAME_TEMP_FILE)), "windows-1251");
    } catch (IOException e) {
      logger.error(e.toString(), e);
    }

    GDParm aGDParm = new GDParm();
    aGDParm.setIdDistr(idDistr);
    aGDParm.setDateBegin(dateBeginText.getText());
    aGDParm.setDateEnd(dateEndText.getText());
    aGDParm.setGdpXML(gdpXML);
    aGDParm.setDateOn(dateOnText.getText());
    aGDParm.setAuthor(authorText.getText());
    aGDParm.setDescription(descriptionText.getText());
    aGDParm.setNorm(normative.isSelected());// ��������!

    int maxId = 0;
    IGDParmDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getGDParmDAO();
    try {
      maxId = dao.insertGDParm(aGDParm);
    } catch (PoolManagerException e) {
      logger.error(e.toString(), e);
    }
    logger.info("max(last) IdGDParm " + maxId);
    SaveGDPToServer(Integer.valueOf(distrEntity.getIdDistr()), maxId);
  }//end

  private void showMessageGDPExists() {
    String title = bundle.getString("javafx.dialog.actualgdp.warning");
    String message = bundle.getString("javafx.dialog.actualgdp.thisgraphexists");
    showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), message, title,
            JOptionPane.DEFAULT_OPTION);
  }

  private void SaveGDPToServer(int idDistr, int maxId) {
    //
    SQLQueryGDParm SQLQueryGDParm = GDPDAOFactoryCreater.getGDPDAOFactory().getSQLGDParm();
    try {
      SQLQueryGDParm.deleteXDistrTrain(maxId);
      SQLQueryGDParm.deleteXWind(maxId);
      SQLQueryGDParm.deleteXxSpanTime(maxId);
      SQLQueryGDParm.deleteXGroupVal(maxId);
      SQLQueryGDParm.deleteXGroupValDir(maxId);
    } catch (PoolManagerException e) {
      logger.error(e.toString(), e);
    }
//    try { // � Delphi �������� ��� �� ����
//      SQLQueryGDParm.deleteStationDistr();
//      SQLQueryGDParm.insertStationDistr(idDistr);
//      SQLQueryGDParm.updateStationDistr(idDistr);
//    } catch (PoolManagerException e) {
//      logger.error(e.toString(), e);
//    }
    try {
      SQLQueryGDParm.saveGDP(idDistr, maxId);
    } catch (PoolManagerException e) {
      logger.error(e.toString(), e);
    }
  }


  private void updateGDP(int arch) {
    scene.setCursor(Cursor.WAIT);
    try {

      GDParm selectedGDP = tableView.getSelectionModel().getSelectedItem();
      if (selectedGDP == null) return;
      IGDParmDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getGDParmDAO();
      try {
        dao.updateGDParm(selectedGDP.getIdGDParm(), arch);
      } catch (PoolManagerException e) {
        logger.error(e.toString(), e);
      }
    } finally {
      scene.setCursor(Cursor.DEFAULT);
      tableView.setItems(FXCollections.observableArrayList(getGDP(distrEntity.getIdDistr(),
              (GDP) group1.getSelectedToggle().getUserData())));
    }
  }

  private void saveGDPToXML(String gdpXML) {
    BufferedWriter bufferedWriter = null;
    try {
      File file = new File(NAME_TEMP_FILE + "w");
      if (!file.exists()) {
        file.createNewFile();
      }
      Writer writer = new FileWriter(file);
      bufferedWriter = new BufferedWriter(writer);
      FileUtils.writeByteArrayToFile(new File(NAME_TEMP_FILE), gdpXML.getBytes("windows-1251"));
      //bufferedWriter.write(gdpXML);
    } catch (IOException e) {
      logger.error(e.toString(), e);
    } finally {
      try {
        if (bufferedWriter != null) bufferedWriter.close();
      } catch (Exception ex) {
        logger.error(ex.toString(), ex);
      }
    }
  }

  private void clearGDP() {
    logger.debug("clearGDP");
    long tm1 = Calendar.getInstance().getTimeInMillis();
    try {
      distrEntity.clearDistrGDPFromDB(false);
    } finally {
      long tm2 = Calendar.getInstance().getTimeInMillis();
      double sec = (double) (tm2 - tm1) / 1000.0;
      logger.debug("clear GDP done: " + sec + " sec");
    }
  }

  private void loadGDPFromXML() throws DataManageException, PoolManagerException {
    File file = new File(NAME_TEMP_FILE);
    if (file != null && file.exists()) {
      Document doc = DataXMLUtil.loadDocument(file);
      String title = bundle.getString("javafx.dialog.actualgdp.warning");
      String warn1 = bundle.getString("javafx.dialog.actualgdp.youwantloadgdp");
      String message = warn1 + " ?";
      int n = showConfirmDialog(GDPEdit.gdpEdit.getMainFrame(), message, title,
              JOptionPane.YES_NO_OPTION);
      boolean load = n == 0;
      if (load) {
        try {
          clearGDP();
          DistrStruct distrStruct = GDPXMLHandler.checkAllDistrStruct(doc, distrEntity, true);
          ICategoryDAO categoryDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getCategoryDAO();
          List<Category> categoryList = categoryDAO.getCategories();
          IDistrGDPXMLDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrGDPXMLDAO();
          dao.checkAllTrain(doc, "Distr_Train", distrStruct.getAllTrain(), categoryList, distrStruct.getWarningList());
          dao.checkAllTrain(doc, "ShowParam", distrStruct.getAllTrain(), categoryList, distrStruct.getWarningList());
          LoadGDPXMLConfig loadGDPXMLConfig = createLoadGDPXMLConfig();
          GDPXMLHandler.loadGDPFromXML(doc, distrEntity, distrStruct, loadGDPXMLConfig);
          distrEntity.clearTrainThreadMap();
        //  distrEntity.getDistrEntity().reloadFromDB();
        } finally {
          logger.debug("loadGDPFromXML done");
        }
      }
    }
  }

  private LoadGDPXMLConfig createLoadGDPXMLConfig() {
    LoadGDPXMLConfig config = new LoadGDPXMLConfig();
    config.loadRoutes = true;
    config.loadSpanStTime = false;
    config.loadSpanTime = true;
    config.loadDistrAppearance = false;
    config.loadSpanTrain = false;
    config.loadWind = false;
    config.sourceGDP = NAME_TEMP_FILE;
    return config;
  }

  public void setContent(DistrEditEntity distrEntity) {
    logger.debug("setContent");
    this.distrEntity = distrEntity;
    labelTitle.setText(distrEntity.getDistrEntity().getDistrName());
    setColumnValue();
    clearInputText();
    authorText.setText(authorUserName);
    authorText.setEditable(false);
    tableView.setItems(FXCollections.observableArrayList(getGDP(distrEntity.getIdDistr(), GDP.ALL)));
  }

  private void setColumnValue() {
    normColumn.setCellValueFactory(new PropertyValueFactory<GDParm, String>("normStr"));
    descriptionColumn.setCellValueFactory(new PropertyValueFactory<GDParm, String>("description"));
    dateBeginColumn.setCellValueFactory(new PropertyValueFactory<GDParm, String>("dateBegin"));
    dateEndColumn.setCellValueFactory(new PropertyValueFactory<GDParm, String>("dateEnd"));
    dateOnColumn.setCellValueFactory(new PropertyValueFactory<GDParm, String>("dateOn"));
    authorColumn.setCellValueFactory(new PropertyValueFactory<GDParm, String>("author"));
  }

  private void clearInputText() {
    descriptionText.clear();
    dateBeginText.clear();
    dateEndText.clear();
    dateOnText.clear();
  }


  public void setScene(Scene scene) {
    this.scene = scene;
  }

  private List<GDParm> getGDP(int idDistr, GDP gdp) {
    IGDParmDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getGDParmDAO();
    List<GDParm> listGDParm = null;
    try {
      listGDParm = dao.getListGDParm(idDistr, gdp);
    } catch (PoolManagerException e) {
      logger.error(e.toString(), e);
    }
    return listGDParm;
  }
}
