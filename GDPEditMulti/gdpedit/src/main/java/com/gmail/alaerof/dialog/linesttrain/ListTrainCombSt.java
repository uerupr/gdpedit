package com.gmail.alaerof.dialog.linesttrain;

import com.gmail.alaerof.dao.dto.TrainCombSt;
import com.gmail.alaerof.dao.interfacedao.IStationDAO;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDistrTrainDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.train.TrainStDouble;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.TableUtils;
import javax.swing.BoxLayout;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
class ListTrainCombStCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        if (value != null)
            setText(value.toString());

        Color background;
        if (row % 2 == 0) {
            background = Color.WHITE;
        } else {
            background = new Color(242, 242, 242);
        }
        if (isSelected) {
            background = Color.LIGHT_GRAY;
        }

        Font oldFont = getFont();
        if (col == 0 || col == 2) {
            Font newFont = new java.awt.Font(oldFont.getName(), java.awt.Font.BOLD, oldFont.getSize());
            setFont(newFont);
        } else {
            Font newFont = new java.awt.Font(oldFont.getName(), 0, oldFont.getSize());
            setFont(newFont);
        }

        setBackground(background);

        return this;
    }
}

public class ListTrainCombSt extends JPanel {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(ListTrainCombSt.class);
    private ListTrainCombStTableModel tableModel;
    private JTable table;
    private int idSt;
    private LineStTRDialog lineStTR;
    private String trainCombSource = "";
    private static ResourceBundle bundle;


    JLabel lblLine;
    JLabel lblTimeOn;
    JLabel lblDistrOn;
    JLabel lblTimeOff;
    JLabel lblDistrOff;

    public void clearLabel() {
        lblLine.setText(bundle.getString("linesttrain.line"));
        lblTimeOn.setText(bundle.getString("linesttrain.line"));
        lblTimeOff.setText(bundle.getString("linesttrain.line"));
        // lblDistrOn.setText("---");
        // lblDistrOff.setText("---");
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.linesttrain", LocaleManager.getLocale());
    }

    public ListTrainCombSt() {
        setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
        setLayout(new BorderLayout(0, 0));

        Font tableFont = new Font(Font.SANS_SERIF, 0, 11);
        tableModel = new ListTrainCombStTableModel();
        table = new JTable(tableModel);
        JScrollPane tablePane = new JScrollPane(table);
        table.setFont(tableFont);
        table.getTableHeader().setFont(tableFont);
        table.setFillsViewportHeight(true);
        table.setDefaultRenderer(Object.class, new ListTrainCombStCellRenderer());

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSize[] = { 40, 30, 40, 30 };
        TableUtils.setTableColumnSize(table, colSize);
        this.add(tablePane, BorderLayout.CENTER);
        tablePane.setPreferredSize(new Dimension(220, 100));

        table.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int row = table.getSelectedRow();
                    int col = table.getSelectedColumn();
                    if (col == 0 || col == 1) {
                        TrainThread train = tableModel.getListTrainStDouble().get(row).getTrainThreadOn();
                        lineStTR.selectTrain(train);
                    }
                    if (col == 2 || col == 3) {
                        TrainThread train = tableModel.getListTrainStDouble().get(row).getTrainThreadOff();
                        lineStTR.selectTrain(train);
                    }
                }
            }
        });

        ListSelectionModel cellSelectionModel = table.getSelectionModel();
        cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {

                int[] selectedRow = table.getSelectedRows();
                int row = selectedRow[0];
                // lblTimeOn.setText(text)
                TrainStDouble trStD = tableModel.getListTrainStDouble().get(row);
                if (trStD != null) {

                    lblTimeOn.setText(trStD.getTrainCombSt().occupyOn);
                    lblTimeOff.setText(trStD.getTrainCombSt().occupyOff);
                    // int idSt =
                    // trStD.getStationBegin().getDistrStation().getIdPoint();
                    int idLine = trStD.getIdLineSt();
                    String nameLine = trStD.getStationBegin().getLineName(idLine);
                    lblLine.setText(nameLine);
                }
            }

        });

        {
            JPanel panelTop = new JPanel();
            panelTop.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
            add(panelTop, BorderLayout.NORTH);
            panelTop.setLayout(new BorderLayout(0, 0));

            JLabel label = new JLabel(bundle.getString("linesttrain.listallgartersbystation"));
            label.setFont(new Font("SansSerif", Font.BOLD, 12));
            label.setHorizontalAlignment(SwingConstants.CENTER);
            panelTop.add(label, BorderLayout.NORTH);
        }

        {
            JPanel panelBottom = new JPanel();
            FlowLayout fl_panelBottom = (FlowLayout) panelBottom.getLayout();
            fl_panelBottom.setAlignment(FlowLayout.LEADING);
            panelBottom.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
            add(panelBottom, BorderLayout.SOUTH);

            JButton buttonDelete = new JButton(bundle.getString("linesttrain.removelink"));
            buttonDelete.setFont(new Font("SansSerif", Font.PLAIN, 12));
            panelBottom.add(buttonDelete);
            buttonDelete.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    int row = table.getSelectedRow();
                    if (row > -1) {
                        TrainStDouble trStD = tableModel.getListTrainStDouble().get(row);
                        DistrEditEntity distrEE = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
                        if (distrEE != null) {
                            // ������� ����� �� ������
                            distrEE.removeTrainStDouble(trStD);
                            table.repaint();
                        }
                    }
                }
            });

            JButton buttonDeleteAll = new JButton(bundle.getString("linesttrain.removeall"));
            buttonDeleteAll.setFont(new Font("SansSerif", Font.PLAIN, 12));
            panelBottom.add(buttonDeleteAll);

            JButton saveToDB = new JButton(bundle.getString("linesttrain.db"));
            saveToDB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
                    List<TrainStDouble> listDouble = tableModel.getListTrainStDouble();
                    List<TrainCombSt> trainCombSts = new ArrayList<>();
                    // ������ ���� � ��
                    IStationDAO daoSt = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO();
                    for(TrainStDouble trainStDouble: listDouble){
                        trainCombSts.add(trainStDouble.getTrainCombSt());
                    }
                    try {
                        dao.saveAllTrainStDouble(idSt, trainCombSts);
                        daoSt.saveTrainCombSource(idSt,trainCombSource);
                    } catch (DataManageException e) {
                        logger.error(e.toString(), e);
                    }
                }
            });

            panelBottom.add(saveToDB);
            buttonDeleteAll.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    DistrEditEntity distrEE = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
                    ArrayList<TrainStDouble> list = new ArrayList<>(tableModel.getListTrainStDouble());
                    if (distrEE != null && list != null) {
                        for (int i = 0; i < list.size(); i++) {
                            TrainStDouble trStD = list.get(i);
                            // ������� ����� �� ������
                            distrEE.removeTrainStDouble(trStD);
                        }
                        table.repaint();
                    }

                }
            });
        }

        {
            JPanel panelRight = new JPanel();
            panelRight.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
            add(panelRight, BorderLayout.EAST);
            panelRight.setPreferredSize(new Dimension(140, 200));
            panelRight.setLayout(new BorderLayout(0, 0));
            JPanel panelRightTop = new JPanel();
            panelRightTop.setLayout(new BoxLayout(panelRightTop, BoxLayout.Y_AXIS));
            panelRight.add(panelRightTop, BorderLayout.NORTH);

            {
                JPanel panel = new JPanel();
                panelRightTop.add(panel);
                JLabel la = new JLabel(bundle.getString("linesttrain.numberoftheway"));
                panel.add(la);
                lblLine = new JLabel(bundle.getString("linesttrain.zero"));
                lblLine.setFont(new Font("Tahoma", Font.BOLD, 11));
                panel.add(lblLine);
            }
            {
                JPanel panel = new JPanel();
                panelRightTop.add(panel);
                JLabel la = new JLabel(bundle.getString("linesttrain.arrival_"));
                panel.add(la);
                lblTimeOn = new JLabel(bundle.getString("linesttrain.time"));
                lblTimeOn.setFont(new Font("Tahoma", Font.BOLD, 11));
                panel.add(lblTimeOn);
            }

            {
                JPanel panel = new JPanel();
                panelRightTop.add(panel);
                JLabel la = new JLabel(bundle.getString("linesttrain.departure_"));
                panel.add(la);
                lblTimeOff = new JLabel(bundle.getString("linesttrain.time"));
                lblTimeOff.setFont(new Font("Tahoma", Font.BOLD, 11));
                panel.add(lblTimeOff);
            }

        }
    }

    /**
     * ���������� ������ ������ �� �������
     * @return ������ ������ �� �������
     */
    public List<TrainStDouble> getListTrainStDouble() {
        return tableModel.getListTrainStDouble();
    }

    public void setListTrainStDouble(List<TrainStDouble> listTrainStDouble) {
        tableModel.setListTrainStDouble(listTrainStDouble);
    }

    public void repaintTable() {
        table.repaint();
    }

    public TrainStDouble getTrainStDouble(long idTrainOn, long idTrainOff) {
        return tableModel.getTrainStDouble(idTrainOn, idTrainOff);
    }

    public int getIdSt() {
        return idSt;
    }

    public void setIdSt(int idSt) {
        this.idSt = idSt;
    }

    public void setTrainCombSource(String trainCombSource) {
        this.trainCombSource = trainCombSource;
    }

    public void selectTrain(TrainThread train) {
        try {
            List<TrainStDouble> list = tableModel.getListTrainStDouble();
            int row = -1;
            int i = 0;
            while (row == -1 && i < list.size()) {
                if (train.equalsByCodeName(list.get(i).getTrainThreadOn()) || train.equalsByCodeName(list.get(i).getTrainThreadOff())) {
                    row = i;
                }
                i++;
            }
            if (row > -1) {
                table.changeSelection(row, 0, false, false);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    public void setLineStTR(LineStTRDialog lineStTR) {
        this.lineStTR = lineStTR;
    }

}
