package com.gmail.alaerof.dialog.timeimport.stations;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * ������������ ������� �� ������� �������� ������� �� �� �������
 * @author Helen Yrofeeva
 * 
 */

public class StationHolderTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("stations.number"),
            bundle.getString("stations.ecp"),
            bundle.getString("stations.name"),
            bundle.getString("stations.type"),
            bundle.getString("stations.ecp"),
            bundle.getString("stations.ecp2"),
            bundle.getString("stations.express"),
            bundle.getString("stations.name") };
    private List<StationHolder> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.stations", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            StationHolder elH = list.get(row);
            Station el = elH.distrSt;
            SpanSt es = elH.spanSt;
            switch (col) {
            case 0:
                return elH.varSt.getNum();
            case 1:
                return elH.varSt.getCodeESR();
            case 2:
                return elH.varSt.getName();
            case 3:
                return elH.varSt.getType();
            case 4:
                if (el != null) {
                    return el.getCodeESR();
                } else {
                    if (es != null) {
                        return es.getCodeESR();
                    } else
                        return null;
                }
            case 5:
                if (el != null) {
                    return el.getCodeESR2();
                } else {
                    if (es != null) {
                        return es.getCodeESR2();
                    } else
                        return null;
                }
            case 6:
                if (el != null) {
                    return el.getCodeExpress();
                } else {
                    if (es != null) {
                        return es.getCodeExpress();
                    } else
                        return null;
                }
            case 7:
                if (el != null) {
                    return el.getName();
                } else {
                    if (es != null) {
                        return es.getName();
                    } else
                        return null;
                }
            }
        }
        return null;
    }

    public List<StationHolder> getList() {
        return list;
    }

    public void setList(List<StationHolder> list) {
        this.list = list;
    }

}
