package com.gmail.alaerof.application;

import java.awt.EventQueue;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.gmail.alaerof.manager.LocaleManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import java.util.ResourceBundle;

import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dialog.timeimport.ImportSPTimeFrame;

/**
 * ��������� ������ ������� ������� ������� ������ ���� � ������� � ��������� ��
 * �� ������� ��� ������ �� ��������� ������� ��������
 * @author Helen Yrofeeva
 * 
 */
public class ImportSPTime {
    public static Logger logger = LogManager.getLogger(ImportSPTime.class);
    private static ResourceBundle bundle;
    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
        bundle = ResourceBundle.getBundle("bundles.ImportSPTime", LocaleManager.getLocale());
    }

    private static void setLookAndFeel() {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    UIManager.put("swing.boldMetal", Boolean.FALSE);
                    break;
                }
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    public static void main(String[] args) {
        int id = -1;
        if (args.length > 0) {
            try {
                id = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                id = -1;
                logger.error(e.toString() + " =" + args[0], e);
            }
        }
        final int idDistr = id;
        EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                    setLookAndFeel();
                    GDPDAOFactoryCreater.initGDPDAOFactory();
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                }
                String pathDB = "";
                String title = "";
                if (GDPDAOFactoryCreater.getGDPDAOFactory() != null) {
                    pathDB = GDPDAOFactoryCreater.getGDPDAOFactory().getPath();
                }
                try {
                    title = bundle.getString("importsptime.title");
                    ImportSPTimeFrame dialog = new ImportSPTimeFrame();
                    dialog.setTitle(title + pathDB);
                    dialog.setDistrID(idDistr);
                    dialog.setVisible(true);
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                }
            }
        });
    }
}
