package com.gmail.alaerof.components.mainframe.gdp.edit;

import com.gmail.alaerof.components.mainframe.gdp.action.GDPActionPanel;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.GDPPrintParam;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.TextPrintPointsOut;
import com.gmail.alaerof.printable.GDPPrintService;
import com.gmail.alaerof.printable.GDPPrintableImpl;
import com.gmail.alaerof.dialog.config.ApplicationConfig;
import com.gmail.alaerof.entity.train.StationExpandCheckBox;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;

import java.net.URISyntaxException;
import java.util.List;
import javax.imageio.ImageIO;
import javax.print.PrintException;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.action.listst.GDPActionListSt;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.entity.train.NewLine;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.entity.train.TrainThreadElement;

/**
 * ������ ��� ��������� ��� ������ ������� (DistrEditEntity) � �������������� ����� ��� GDPActionTabPane
 * @author Helen Yrofeeva
 * 
 */
public class GDPEditPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(GDPActionListSt.class);
    private DistrEditEntity distrEditEntity;
    private JScrollPane contentLeft;
    private JScrollPane contentGDP;
    private JScrollPane contentGDPHour;
    private JPanel contentGDPpanel;
    private JSplitPane split;
    private JPanel panelLeft = new JPanel();
    private JPanel panelGDPHour = new JPanel();
    private JLabel imageLabelLeft;
    private JLabel imageLabelGDP;
    private JLabel imageLabelGDPHour;
    private JScrollBar vertScrollBar;
    private JScrollBar horScrollBar;
    private boolean isStationScroll;
    private boolean isLeftStationScroll;
    private double scrollBarPosition = 0;
    private int scrollBarGDPPosition = 0;
    //private int scrollBarStPosition = 0;

    private GDPActionPanel actionTabPane;
    private TrainThread currentTrain;
    /** ������ ������� (�����) - ��� ����������� � GDPEditPanel */
    public final int HOUR_BEGIN = 0;
    /** �����  ������� (�����) - ��� ����������� � GDPEditPanel */
    public final int HOUR_END = 24;
    /***/
    private NewLineActionListener listener = new NewLineActionListener();
    /** ������ "����" ��� ������� (����������� ��� �������� �� ��) */
    private int trainWindowHourBegin = 0;
    /** �����  "����" ��� ������� (����������� ��� �������� �� ��) */
    private int trainWindowHourEnd = 24;

    private ActionListener actionOnClose = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            removeAllLines();
            if (distrEditEntity != null) {
                distrEditEntity.removeListDistrTrain();
                distrEditEntity.clearHistory();
            }
            GDPEdit.gdpEdit.getGDPActionPanel().clearTab();
        }
    };

    /**
     * ��������� �������� � ����� ����� � ������� �����
     */
    private void fillImages() {
        GDPGridConfig cf = distrEditEntity.getDistrEntity().getConfigGDP();

        int h = cf.getScaledMainImageHeight();
        int hHour = cf.getScaledHourHeight();
        int wLeft = cf.getScaledLeftWidth();
        int wGDP = cf.getScaledMainImageWidth();
        imageLabelLeft = null;
        imageLabelGDP = null;
        imageLabelGDPHour = null;

        BufferedImage buffImageLeft = new BufferedImage(wLeft, h, BufferedImage.TYPE_USHORT_555_RGB);
        BufferedImage buffImageGDP = new BufferedImage(wGDP, h, BufferedImage.TYPE_USHORT_555_RGB); // TYPE_INT_ARGB
        BufferedImage buffImageGDPHour = new BufferedImage(wGDP, hHour, BufferedImage.TYPE_USHORT_555_RGB); // TYPE_INT_ARGB

        Graphics gLeft = buffImageLeft.createGraphics();
        Graphics gGDP = buffImageGDP.createGraphics();
        Graphics gGDPHour = buffImageGDPHour.createGraphics();

        Color backgroundColor = getBackgroundColor();
        GDPGridConfig config = distrEditEntity.getDistrEntity().getConfigGDP();
        distrEditEntity.getDistrEntity().drawNetGDP(gLeft, gGDP, 0, 0, 0, backgroundColor,
                DistrEntity.SEPARATED_GDP, config, null, null);
        distrEditEntity.getDistrEntity().drawNetGDPHour(gGDPHour, 0, 0, backgroundColor);

        ImageIcon imageIconLeft = new ImageIcon(buffImageLeft);
        imageLabelLeft = new JLabel(imageIconLeft);
        imageLabelLeft.setPreferredSize(new Dimension(wLeft, h));

        panelLeft.setBounds(0, 0, wLeft, h);
        panelLeft.setLayout(null);
        panelLeft.setPreferredSize(new Dimension(wLeft, h));
        panelLeft.setBackground(backgroundColor);

        panelLeft.add(imageLabelLeft);
        imageLabelLeft.setBounds(0, 0, wLeft, h);
        imageLabelLeft.setLayout(null);

        ImageIcon imageIconGDP = new ImageIcon(buffImageGDP);
        imageLabelGDP = new JLabel(imageIconGDP);
        imageLabelGDP.setPreferredSize(new Dimension(wGDP, h));

        this.setBounds(0, 0, wGDP, h);
        this.setLayout(null);
        this.setPreferredSize(new Dimension(wGDP, h));
        this.setBackground(backgroundColor);

        this.add(imageLabelGDP);
        imageLabelGDP.setBounds(0, 0, wGDP, h);
        imageLabelGDP.setLayout(null);

        int wHour = wGDP + 20;
        ImageIcon imageIconGDPHour = new ImageIcon(buffImageGDPHour);
        imageLabelGDPHour = new JLabel(imageIconGDPHour);
        imageLabelGDPHour.setPreferredSize(new Dimension(wGDP, hHour));

        panelGDPHour.setBounds(0, 0, wHour, hHour);
        panelGDPHour.setLayout(null);
        panelGDPHour.setPreferredSize(new Dimension(wHour, hHour));
        panelGDPHour.setBackground(backgroundColor);

        panelGDPHour.add(imageLabelGDPHour);
        imageLabelGDPHour.setBounds(0, 0, wGDP, hHour);
        imageLabelGDPHour.setLayout(null);
    }

    /**
     * ��������� ����� �������, ������� � ���������� ������ ���� �� �����. �������.
     * ���������� ��������� ��� ����������� "��������"/"��������" ������� (���� �������)
     */
    private void fillNetGDP() {
        GDPGridConfig cf = distrEditEntity.getDistrEntity().getConfigGDP();
        distrEditEntity.getDistrEntity().calcGDPSettings(HOUR_BEGIN, HOUR_END, cf);
        fillImages();

        List<DistrStation> listSt = distrEditEntity.getDistrEntity().getListSt();
        StationExpandActionListener listener = new StationExpandActionListener();
        int x = distrEditEntity.getDistrEntity().getxLeft().getxStRight();
        for (DistrStation st : listSt) {
            StationExpandCheckBox checkExp = new StationExpandCheckBox(st, x);
            checkExp.setSelected(st.isExpanded());
            panelLeft.add(checkExp, 0);
            checkExp.setCurrentPosition();
            checkExp.addActionListener(listener);
        }

        panelLeft.repaint();
        this.repaint();
    }

    // ����� ����� ��� Scale();
    public void scaleGDP(float scale) {
        // �������� � ������ ������ ��������
        panelLeft.remove(imageLabelLeft);
        panelGDPHour.remove(imageLabelGDPHour);
        contentGDPpanel.remove(imageLabelGDP);
        // ���������������
        distrEditEntity.getDistrEntity().getConfigGDP().scale = scale;
        // ��������� �����
        GDPGridConfig cf = distrEditEntity.getDistrEntity().getConfigGDP();
        distrEditEntity.getDistrEntity().calcGDPSettings(HOUR_BEGIN, HOUR_END, cf);
        fillImages();

        List<DistrStation> listSt = distrEditEntity.getDistrEntity().getListSt();
        StationExpandActionListener listener = new StationExpandActionListener();
        int x = distrEditEntity.getDistrEntity().getxLeft().getxStRight();
        for (DistrStation st : listSt) {
            StationExpandCheckBox checkExp = new StationExpandCheckBox(st, x);
            checkExp.setSelected(st.isExpanded());
            panelLeft.add(checkExp, 0);
            checkExp.setCurrentPosition();
            checkExp.addActionListener(listener);
        }

        panelLeft.repaint();
        this.repaint();
        // ����� ���������������� ������ ��� �������
        moveStationExpandCheckBox();
        // ��������� �������
        loadListDistrTrain();
    }

    /**
     * ���������������� ������ (���������) ��� �������
     */
    private void moveStationExpandCheckBox() {
        Component[] listComp = panelLeft.getComponents();
        for (Component c : listComp) {
            if (c instanceof StationExpandCheckBox) {
                StationExpandCheckBox st = (StationExpandCheckBox) c;
                st.setCurrentPosition();
            }
        }
    }

    public void loadForLineStTRDialog(int idSt) {
        int extTR = distrEditEntity.existTrainsOnStation(idSt);
        if (extTR == 0) {
            distrEditEntity.loadListDistrTrainOnStation(trainWindowHourBegin, trainWindowHourEnd, listener, idSt, true);
            distrEditEntity.loadTrainStDouble(trainWindowHourBegin, trainWindowHourEnd, idSt);
        }
    }

    /**
     * ��������� ������� "���������" �������
     */
    private class StationExpandActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                StationExpandCheckBox st = (StationExpandCheckBox) ev.getSource();
                // ��������� �������� ������������� ����� ��� �������
                DistrStation dst = st.getStation();
                dst.setExpanded(st.isSelected());
                // �������� � ������ ������ ��������
                panelLeft.remove(imageLabelLeft);
                panelGDPHour.remove(imageLabelGDPHour);
                remove(imageLabelGDP);
                // �������� ���������� ���������
                GDPGridConfig cf = distrEditEntity.getDistrEntity().getConfigGDP();
                distrEditEntity.getDistrEntity().calcGDPSettings(HOUR_BEGIN, HOUR_END, cf);
                // ��������� ����� �������� (����� ���)
                fillImages();
                // ����� ���������������� ������ ��� �������
                moveStationExpandCheckBox();

                // ��������, ��� ������������� ������� ��� �������� �������
                if (st.isSelected()) {
                    int idSt = dst.getIdPoint();
                    int extTR = distrEditEntity.existTrainsOnStation(idSt);
                    if (extTR == 0) {
                        distrEditEntity.loadListDistrTrainOnStation(trainWindowHourBegin, trainWindowHourEnd, listener, idSt, true);
                        distrEditEntity.loadTrainStDouble(trainWindowHourBegin, trainWindowHourEnd, idSt);
                    }
                }

                // ���������� ����� ���
                distrEditEntity.setAllTrainLocation();

                isStationScroll = true;
                isLeftStationScroll = true;
                scrollBarPosition = vertScrollBar.getValue();
                //scrollBarStPosition = contentLeft.getHorizontalScrollBar().getValue();
                scrollBarGDPPosition = contentGDP.getHorizontalScrollBar().getValue();

            } catch (Exception e) {
                logger.error(e.toString(), e);
            }

        }
    }

    /**
     * �����������
     * @param distrEE {@link DistrEditEntity} ������� + ���
     * @param actionTabPane ������ ��� �������������� ��� ()
     */
    public GDPEditPanel(DistrEditEntity distrEE, GDPActionPanel actionTabPane) {
        this.distrEditEntity = distrEE;
        this.actionTabPane = actionTabPane;
        contentGDPpanel = this;
        distrEditEntity.setComponentOwner(contentGDPpanel);
        fillNetGDP();
        contentGDP = new JScrollPane(this);
        contentLeft = new JScrollPane(panelLeft);
        contentGDPHour = new JScrollPane(panelGDPHour);

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        JPanel panelLeft = new JPanel();
        panelLeft.setLayout(new BorderLayout());
        JPanel panelGDP = new JPanel();
        panelGDP.setLayout(new BorderLayout());

        JPanel panelLeftTop = new JPanel();
        panelLeftTop.setPreferredSize(new Dimension(0, panelGDPHour.getPreferredSize().height + 5));

        panelLeft.add(panelLeftTop, BorderLayout.NORTH);
        panelLeft.add(contentLeft, BorderLayout.CENTER);

        panelGDP.add(contentGDPHour, BorderLayout.NORTH);
        panelGDP.add(contentGDP, BorderLayout.CENTER);
        split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panelLeft, panelGDP);

        horScrollBar = contentGDPHour.getHorizontalScrollBar();
        contentGDP.setHorizontalScrollBar(horScrollBar);
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        // mainframe = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, contentLeft,
        // contentGDP);
        split.setOneTouchExpandable(true);
        split.setDividerLocation(150);
        contentLeft.getHorizontalScrollBar().setValue(140);
        vertScrollBar = contentLeft.getVerticalScrollBar();
        contentGDP.setVerticalScrollBar(vertScrollBar);
        vertScrollBar.setUnitIncrement(20);

        vertScrollBar.getModel().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent arg0) {
                if (isStationScroll) {
                    vertScrollBar.setValue((int) (scrollBarPosition));
                    contentGDP.getHorizontalScrollBar().setValue(scrollBarGDPPosition);
                    isStationScroll = false;
                }
            }
        });

//        JScrollBar leftHor = contentLeft.getHorizontalScrollBar();
//
//        leftHor.getModel().addChangeListener(new ChangeListener() {
//            @Override
//            public void stateChanged(ChangeEvent e) {
//                if (isLeftStationScroll) {
//                    contentLeft.getHorizontalScrollBar().setValue(scrollBarStPosition);
//                    isLeftStationScroll = false;
//                }
//            }
//        });
    }

    public int getIdDistr() {
        return distrEditEntity.getDistrEntity().getIdDistr();
    }

    public JComponent getContent() {
        return split;
    }

    public DistrEditEntity getDistrEditEntity() {
        return distrEditEntity;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + getIdDistr();
        return result;
    }

    public ActionListener getLineListener() {
        return listener;
    }

    /**
     * �������� ��� �� �� + ������������ ���� ���������� �����
     */
    public void loadListDistrTrain() {
        removeAllLines();
        distrEditEntity.loadListDistrTrain(trainWindowHourBegin, trainWindowHourEnd, listener, true);
        actionTabPane.getActionListTrain().updateListTrain();
    }

    public boolean listTrainLoaded() {
        return distrEditEntity.listTrainLoaded();
    }

    /**
     * ��������� ��� � ��
     * @param onlyShowParam  ������ ��������� �����������
     */
    public void saveListDistrTrain(boolean onlyShowParam) {
        distrEditEntity.saveAllDistrTrainDB(onlyShowParam);
        distrEditEntity.clearHistory();
    }

    /**
     * ������� ��� �� ��
     * @param onlyShowParam ������ ��������� �����������
     */
    public void clearListDistrTrain(boolean onlyShowParam) {
        distrEditEntity.clearDistrGDPFromDB(onlyShowParam);
    }

    private class NewLineActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                NewLine line = (NewLine) e.getSource();
                TrainThreadElement element = line.getTrainThreadElement();
                element.setClickedLine(line);
                TrainThread train = line.getTrainThread();
                train.setCurrentElement(element);
                setCurrentTrain(train);
                setCurrentTrainListSt();
                setCurrentTrainListTrain();
                setTrainWidth();
            } catch (Exception err) {
                logger.error(err.toString(), err);
            }
        }

    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GDPEditPanel other = (GDPEditPanel) obj;
        if (getIdDistr() != other.getIdDistr())
            return false;
        return true;
    }

    public JPanel getPanelLeft() {
        return panelLeft;
    }

    public TrainThread getCurrentTrain() {
        return currentTrain;
    }

    public void setCurrentTrain(TrainThread currentTrain) {
        TrainThread prevTr = this.currentTrain;
        this.currentTrain = currentTrain;
        if (currentTrain != prevTr) {
            if (prevTr != null) {
                prevTr.setSelected(false);
                prevTr.setCurrentElement(null);
            }
            currentTrain.setSelected(true);
        }
    }

    public void setCurrentTrainListSt() {
        actionTabPane.getActionListSt().setCurrentTrainThread(currentTrain);
    }

    public void setCurrentTrainListTrain() {
        actionTabPane.getActionListTrain().setCurrentTrainThread(currentTrain);
    }

    public void setTrainWidth() {
        actionTabPane.getActionListTrain().setTrainWidth(currentTrain);
    }

    private void removeAllLines() {
        for (int i = (this.getComponentCount() - 1); i >= 0; i--) {
            Component jc = this.getComponent(i);
            if (jc instanceof NewLine) {
                ((NewLine) jc).removeAllActionListeners();
                this.remove(jc);
            }
        }
        this.repaint();
    }

    public ActionListener getActionOnClose() {
        return actionOnClose;
    }

    public boolean historyRestore() {
        return distrEditEntity.getHistory().restoreAction();
    }

    public boolean historyApply() {
        return distrEditEntity.getHistory().applyAction();
    }

    public void clearHistory() {
        distrEditEntity.clearHistory();
    }

    public void updateHorScrollPosition() {
        if (currentTrain != null) {
            if (currentTrain.getListTimeSt().size() > 0) {
                int x = currentTrain.getListTimeSt().get(0).getXOn();
                contentGDP.getHorizontalScrollBar().setValue(x);
            }
        }
    }

    /** ������ ��� � ���� */
    public void printGDP(File file, GDPPrintParam gdpPrintParam) {
        // �������� ���������� ���������
        // � DistrEntity.configGDP ��������������� hourB, hourE � ��.
        GDPGridConfig config = distrEditEntity.getDistrEntity().getConfigGDP();
        GDPGridConfig cf = new GDPGridConfig(config);// distrEditEntity.getDistrEntity().getConfigGDP();//
        distrEditEntity.getDistrEntity().calcGDPSettings(gdpPrintParam.getHourBeg(), gdpPrintParam.getHourEnd(), cf);

        int h = cf.getScaledMainImageHeight();
        int wLeft = cf.getScaledLeftWidth();
        int wGDP = cf.getScaledMainImageWidth();

        TextPrintPointsOut tPPO = new TextPrintPointsOut(gdpPrintParam);
        //int headGap = 100;
        //int bottomGap = 100;
        int headGap = tPPO.getHeadGap();
        int bottomGap = tPPO.getBottomGap();
        int horGap = 40;

        BufferedImage buffImageGDP = new BufferedImage(wLeft + wGDP + horGap*2, h + headGap + bottomGap,
                BufferedImage.TYPE_USHORT_555_RGB); // TYPE_INT_ARGB

        Graphics gGDP = buffImageGDP.createGraphics();
        // ��� ���������� ����� � ����� ���������� ������������ ���������� ���
        // �������. �.�. ������������ ���������� ��� � ���������
        distrEditEntity.getDistrEntity().drawNetGDP(gGDP, gGDP, horGap, headGap, bottomGap, Color.white,
                DistrEntity.JOINT_GDP, cf, gdpPrintParam, tPPO);
        
        distrEditEntity.paintInPictureAllTrains(gGDP, cf);

        try {
            String ff = file.getAbsolutePath();
            if(!ff.endsWith(".png")){ff = ff + ".png";}
            File outputfile = new File(ff);
           
            ImageIO.write((RenderedImage) buffImageGDP, "png", outputfile);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }

    }

    /** ������ ��� �� ������� ����� ������ */
    public void printGDP(GDPPrintParam gdpPrintParam) throws Exception {
        GDPPrintService.print(distrEditEntity, gdpPrintParam);
    }

    private Color getBackgroundColor() {
        Color backgroundColor = Color.WHITE;
        ApplicationConfig applicationConfig = GDPEdit.gdpEdit.getApplicationConfig();
        if (applicationConfig != null && applicationConfig.getBackgroundColor() != null) {
            backgroundColor = applicationConfig.getBackgroundColor();
        }
        return backgroundColor;
    }

    public JPanel getContentGDPpanel() {
        return contentGDPpanel;
    }
}
