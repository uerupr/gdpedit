package com.gmail.alaerof.javafx.control.editabletableview;

import java.util.function.Function;
import java.util.regex.Pattern;
import javafx.beans.property.IntegerProperty;
import javafx.util.converter.IntegerStringConverter;

/**
 * {@link IntegerProperty} editable table cell
 * @param <T> Entity with property-fields to be edited in TableView
 */
public class IntegerEditCell<T> extends EditCell<T, Integer, IntegerProperty> {
    /**
     * Integer pattern
     */
    private final Pattern intPattern = Pattern.compile("-?\\d+");

    /**
     * {@inheritDoc}
     */
    public IntegerEditCell(Function<T, IntegerProperty> property) {
        super(property);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void bindItemProperty() {
        textProperty().bindBidirectional(itemProperty(), new IntegerStringConverter());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer parseText(String text) {
        return Integer.parseInt(text);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Pattern getSimpleTypePattern() {
        return intPattern;
    }
}
