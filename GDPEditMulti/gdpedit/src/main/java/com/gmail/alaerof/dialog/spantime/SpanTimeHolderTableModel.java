package com.gmail.alaerof.dialog.spantime;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
public class SpanTimeHolderTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("spantime.typeofmove"),
            bundle.getString("spantime.direction"),
            bundle.getString("spantime.running"),
            bundle.getString("spantime.racing"),
            bundle.getString("spantime.slowdown"),
            bundle.getString("spantime.addition") };
    private List<SpanTimeHolder> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spantime", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else
            return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            SpanTimeHolder el = list.get(row);
            switch (col) {
            case 0:
                return el.getLocType().getLtype().trim();
            case 1:
                return el.getOe().getString().trim();
            case 2:
                if (el.getTime() != null) {
                    return el.getTime().getTimeMove();
                } else
                    return null;
            case 3:
                if (el.getTime() != null) {
                    return el.getTime().getTimeUp();
                } else
                    return null;
                
            case 4:
                if (el.getTime() != null) {
                    return el.getTime().getTimeDown();
                } else
                    return null;
                
            case 5:
                if (el.getTime() != null) {
                   return el.getTime().getTimeTw(); 
                } else
                    return null;
                
            default:
                break;
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col > 1) {
            return true;
        }
        return false;
    }

    @Override
    public Class<?> getColumnClass(int col) {
        if (col > 1) {
            return Float.class;
        }
        return Object.class;
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null && row < getRowCount() && col < getColumnCount()) {
            SpanTimeHolder htm = list.get(row);
            Time tm = htm.getTime();
            if (tm != null) {
                switch (col) {
                case 2:
                    tm.setTimeMove((Float) aValue);
                    break;
                case 3:
                    tm.setTimeUp((Float) aValue);
                    break;
                case 4:
                    tm.setTimeDown((Float) aValue);
                    break;
                case 5:
                    tm.setTimeTw((Float) aValue);
                    break;
                default:
                    break;
                }
            }
        }
    }

    public List<SpanTimeHolder> getList() {
        return list;
    }

    public void setList(List<SpanTimeHolder> list) {
        this.list = list;
    }

}
