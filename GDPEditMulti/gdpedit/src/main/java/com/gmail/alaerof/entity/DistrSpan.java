package com.gmail.alaerof.entity;

import com.gmail.alaerof.dao.dto.energy.Energy;
import com.gmail.alaerof.dao.dto.energy.SpanEnergy;
import com.gmail.alaerof.dao.interfacedao.ISpanEnergyDAO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.SpanStTime;
import com.gmail.alaerof.dao.dto.SpanTime;
import com.gmail.alaerof.dao.dto.Time;
import com.gmail.alaerof.dao.factory.IGDPDAOFactory;
import com.gmail.alaerof.dao.interfacedao.ISpanTimeDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;

/**
 * @author Helen Yrofeeva
 */
public class DistrSpan {
    private static Logger logger = LogManager.getLogger(DistrSpan.class);
    private IGDPDAOFactory dataSource;
    private Scale spanScale;
    private SpanTime spanTime;
    private SpanEnergy spanEnergy;
    private DistrStation stB;
    private DistrStation stE;
    /** ����� ���� ����� �.�. ��� ���� ����������� */
    private List<SpanStTime> listSpanStTime;

    public DistrSpan(IGDPDAOFactory dataSource, Scale sc, DistrStation stB, DistrStation stE) {
        this.dataSource = dataSource;
        this.spanScale = sc;
        this.stB = stB;
        this.stE = stE;
    }

    public Scale getSpanScale() {
        return spanScale;
    }

    public SpanTime getSpanTime(List<LocomotiveType> lt) {
        if (spanTime == null) {
            loadSpanTime(lt);
        }
        return spanTime;
    }

    /**
     * ���������� ��������� ����� ���� ��� ��������� ���� � ����������� ��������
     * @param lt ��� ��������
     * @param oe ����������� ��������
     * @return ��������� ����� ����
     */
    public Time getSpanTime(LocomotiveType lt, OE oe) {
        Time time = null;

        if (spanTime != null) {
            if (oe == OE.odd) {
                time = spanTime.getLocTimeO().get(lt);
            }
            if (oe == OE.even) {
                time = spanTime.getLocTimeE().get(lt);
            }
        }
        return time;
    }

    /**
     * ������������� ��������� ����� ���� ��� ��������� ���� � ����������� ��������
     * @param tm ��������� ����� ����
     * @param lt ��� ��������
     * @param oe ����������� ��������
     */
    public void setSpanTime(Time tm, LocomotiveType lt, OE oe) {
        if (spanTime != null) {
            if (oe == OE.odd) {
                spanTime.getLocTimeO().put(lt, tm);
            }
            if (oe == OE.even) {
                spanTime.getLocTimeE().put(lt, tm);
            }
        }
    }

    public DistrStation getStB() {
        return stB;
    }

    public DistrStation getStE() {
        return stE;
    }

    @Override
    public String toString() {
        return "DistrSpan [scale=" + spanScale + ", stB=" + stB + ", stE=" + stE + "]";
    }

    private void loadSpanTime(List<LocomotiveType> lt) {
        try {
            ISpanTimeDAO tmDAO = dataSource.getSpanTimeDAO();
            spanTime = tmDAO.getSpanTime(spanScale.getIdSpan(), lt);
            listSpanStTime = tmDAO.getSpanStTime(spanScale.getIdScale());
        } catch ( DataManageException  e) {
            logger.error("load span time " + spanScale + " " + e.toString(), e);
        }
    }

    public void saveSpanStTime() {
        try {
            ISpanTimeDAO tmDAO = dataSource.getSpanTimeDAO();
            tmDAO.saveSpanStTime(listSpanStTime);
        } catch (DataManageException  e) {
            logger.error("save span st time " + spanScale + " " + e.toString(), e);
        }
    }

    public void saveSpanTime() {
        if (spanTime != null) {
            try {
                ISpanTimeDAO tmDAO = dataSource.getSpanTimeDAO();
                tmDAO.saveSpanTime(spanScale.getIdSpan(), spanTime);
            } catch (DataManageException  e) {
                logger.error("save span time " + spanScale + " " + e.toString(), e);
            }
        }
    }

    public float getMaxTime(List<LocomotiveType> lt) {
        SpanTime spt = getSpanTime(lt);
        if (spt != null) {
            return Math.max(spt.getMaxO(), spt.getMaxE());
        } else
            return 0;
    }

    /** �������� ������ ������ ���� �� �������� ��� ������ ����������� */
    public void reloadSpanTime(List<LocomotiveType> lt) {
        loadSpanTime(lt);
    }

    public void updateSpanScale() {
        try {
            Scale sc = dataSource.getScaleDAO().getScale(spanScale.getIdDistr(), spanScale.getIdSpan());
            spanScale = sc;
        } catch (ObjectNotFoundException | DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public List<SpanStTime> getListSpanStTime() {
        if (listSpanStTime == null) {
            try {
                listSpanStTime = dataSource.getSpanTimeDAO().getSpanStTime(spanScale.getIdScale());
            } catch (DataManageException  e) {
                logger.error(e.toString(), e);
            }
        }
        return listSpanStTime;
    }

    /**
     * ������ ������ ���� �� �.�. ������ ��� 1 ���� ��������
     * @param locomotiveType {@link LocomotiveType}
     * @return list {@link SpanStTime}
     */
    public List<SpanStTime> getListSpanStTime(LocomotiveType locomotiveType) {
        List<SpanStTime> allList = getListSpanStTime();
        List<SpanStTime> list = new ArrayList<>();
        for (SpanStTime stTime : allList) {
            if (stTime.getIdLType() == locomotiveType.getIdLType()) {
                list.add(stTime);
            }
        }
        return list;
    }

    /**
     * ��������� �� �� ������� ���� ����� �.�.
     */
    public void reloadSpanStTime() {
        try {
            listSpanStTime = dataSource.getSpanTimeDAO().getSpanStTime(spanScale.getIdScale());
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public SpanStTime getSpanStTime(int idScale, int idLType, int num) {
        if (listSpanStTime == null) {
            reloadSpanStTime();
        }
        for (int i = 0; i < listSpanStTime.size(); i++) {
            SpanStTime spStT = listSpanStTime.get(i);
            if (spStT.getIdScale() == idScale && spStT.getIdLType() == idLType && spStT.getNum() == num) {
                return spStT;
            }
        }
        return null;
    }

    private void removeSpanStTime(LocomotiveType loc) {
        if (loc != null && listSpanStTime != null) {
            Iterator<SpanStTime> iter = listSpanStTime.iterator();
            while (iter.hasNext()) {
                SpanStTime tm = iter.next();
                if (tm.getIdLType() == loc.getIdLType()) {
                    iter.remove();
                }
            }
        }
    }

    public void buildNewSpanStTime(LocomotiveType loc) {
        if (loc != null && listSpanStTime != null) {
            removeSpanStTime(loc);
            Scale scale = getSpanScale();
            float len = scale.getL() / 1000;
            if (len > 0) {
                Time tmO = getSpanTime(loc, OE.odd);
                Time tmE = getSpanTime(loc, OE.even);
                float moveO = 0;
                float moveE = 0;
                if (tmO != null) {
                    moveO = tmO.getTimeMove();
                }
                if (tmE != null) {
                    moveE = tmE.getTimeMove();
                }
                List<SpanSt> list = scale.getSpanSt();
                float km1 = scale.getAbsKM();
                float l = 0;
                double tm = 0;
                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        SpanSt st = list.get(i);
                        SpanStTime sstm = new SpanStTime();
                        sstm.setIdScale(scale.getIdScale());
                        sstm.setIdLType(loc.getIdLType());
                        sstm.setNum(i);
                        listSpanStTime.add(sstm);
                        float km2 = st.getAbsKM();
                        l = Math.abs(km2 - km1);
                        tm = l * moveO / len;
                        sstm.setTmoveO(tm);
                        tm = l * moveE / len;
                        sstm.setTmoveE(tm);
                        km1 = km2;

                        if (i == list.size() - 1) {
                            // ����� �� ���������� �.�. �� �.�.
                            sstm = new SpanStTime();
                            sstm.setIdScale(scale.getIdScale());
                            sstm.setIdLType(loc.getIdLType());
                            sstm.setNum(i + 1);
                            listSpanStTime.add(sstm);

                            km2 = scale.getAbsKM_E();
                            l = Math.abs(km2 - km1);
                            tm = l * moveO / len;
                            sstm.setTmoveO(tm);
                            tm = l * moveE / len;
                            sstm.setTmoveE(tm);
                        }
                    }
                    if (list.size() == 0) {
                        // ����� �� �.�. �� �.�.
                        SpanStTime sstm = new SpanStTime();
                        sstm.setIdScale(scale.getIdScale());
                        sstm.setIdLType(loc.getIdLType());
                        sstm.setNum(0);
                        listSpanStTime.add(sstm);
                        tm = moveO;
                        sstm.setTmoveO(tm);
                        tm = moveE;
                        sstm.setTmoveE(tm);
                    }
                }
            }
        }
    }

    public List<SpanStTime> getSpanStTime(int num) {
        List<SpanStTime> list = new ArrayList<>();
        if (listSpanStTime != null) {
            for (int i = 0; i < listSpanStTime.size(); i++) {
                SpanStTime st = listSpanStTime.get(i);
                if (num == st.getNum()) {
                    list.add(st);
                }
            }
        }
        return list;
    }

    public List<LocomotiveType> getListLocomotiveType() {
        return new ArrayList<>(spanTime.getLocTimeO().keySet());
    }

    public void setSpanStTime(double tm, int num, int scaleID, int locID, OE oe) {
        SpanStTime spt = getSpanStTime(scaleID, locID, num);
        if (spt == null) {
            spt = new SpanStTime();
            spt.setIdLType(locID);
            spt.setIdScale(scaleID);
            spt.setNum(num);
            listSpanStTime.add(spt);
        }
        if (oe == OE.even) {
            spt.setTmoveE(tm);
        } else {
            spt.setTmoveO(tm);
        }
    }

    /**
     * @param lt ������ ����� ��������
     * @return SpanEnergy
     */
    public SpanEnergy getSpanEnergy(List<LocomotiveType> lt) {
        if (spanEnergy == null) {
            loadSpanEnergy(lt);
        }
        return spanEnergy;
    }

    private void loadSpanEnergy(List<LocomotiveType> lt) {
        try {
            ISpanEnergyDAO dao = dataSource.getSpanEnergyDAO();
            spanEnergy = dao.getSpanEnergy(spanScale.getIdScale(), lt);
        } catch (DataManageException e) {
            logger.error("load span energy " + spanEnergy + " " + e.toString(), e);
        }
    }

    /**
     * ���������� ��������������� ������� ��� ��������� ���� � ����������� ��������
     * @param lt ��� ��������
     * @param oe ����������� ��������
     * @return ��������� ����� ����
     */
    public Energy getSpanEnergy(LocomotiveType lt, OE oe) {
        Energy energy = null;
        if (spanEnergy != null) {
            if (oe == OE.odd) {
                energy = spanEnergy.getLocEnergyO().get(lt);
            }
            if (oe == OE.even) {
                energy = spanEnergy.getLocEnergyE().get(lt);
            }
        }
        return energy;
    }

    /**
     * ��������� SpanEnergy � ��
     */
    public void saveSpanEnergy() {
        if (spanEnergy != null) {
            try {
                ISpanEnergyDAO dao = dataSource.getSpanEnergyDAO();
                dao.saveSpanEnergy(spanScale.getIdScale(), spanEnergy);
            } catch (DataManageException e) {
                logger.error("save span energy " + spanEnergy + " " + e.toString(), e);
            }
        }
    }

    /**
     * ������������� ��������� ������ ��� ��������� ���� � ����������� ��������
     * @param energy ��������� ������
     * @param lt ��� ��������
     * @param oe ����������� ��������
     */
    public void setSpanEnergy(Energy energy, LocomotiveType lt, OE oe) {
        if (oe == OE.odd) {
            spanEnergy.getLocEnergyO().put(lt, energy);
        }
        if (oe == OE.even) {
            spanEnergy.getLocEnergyE().put(lt, energy);
        }
    }

    /** �������� �������������� ������� �� �������� ��� ������ ����������� */
    public void updateSpanEnergy(List<LocomotiveType> lt) {
        loadSpanEnergy(lt);
    }

    public int getNum(){
        return getSpanScale().getNum();
    }
}
