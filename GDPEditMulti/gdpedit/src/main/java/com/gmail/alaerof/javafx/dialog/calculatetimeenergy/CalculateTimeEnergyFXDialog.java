package com.gmail.alaerof.javafx.dialog.calculatetimeenergy;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.view.CalculateTimeEnergyLayoutController;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

public class CalculateTimeEnergyFXDialog  extends SwingFXDialogBase {
    /**FX controller */
    private CalculateTimeEnergyLayoutController controller;
    /***/
    private DistrEditEntity distrEditEntity;
    /**
     * Create the dialog.
     */
    public CalculateTimeEnergyFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/calculatetimeenergy/view/CalculateTimeEnergyLayout.fxml";
        resourceBundleBaseName = "bundles.CalculateTimeEnergyFXDialog";

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if(distrEditEntity!=null){
                    distrEditEntity.clearTrainThreadMap();
                }
            }
        });
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    /**
     * ������ ���������� ������ ��� ��� �������� ���� ��� ����������� ��������
     * @param distrEditEntity {@link DistrEditEntity} ������� ������� � �������� ��� ��� :)
     */
    public void setDialogContent(DistrEditEntity distrEditEntity){
        this.distrEditEntity = distrEditEntity;
        owner.setCursor(GDPEdit.waitCursor);
        try {
            // ���� ������ �� ���������, �� ����������� �� �� � ������ �����������, �.�. ��� ���������� �����������
            if (!distrEditEntity.listTrainLoaded()) {
                distrEditEntity.loadListDistrTrain(0, 24, null, false);
            }
        }finally {
            owner.setCursor(GDPEdit.defCursor);
        }
        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(distrEditEntity);
            }
        });
    }

    /** {@inheritDoc} */
    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {100, 50, 1100, 620};
        return bounds;
    }
}
