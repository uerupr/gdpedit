package com.gmail.alaerof.components.mainframe.gdp.action;

import com.gmail.alaerof.components.mainframe.gdp.action.listspan.GDPActionListSpan;
import com.gmail.alaerof.components.mainframe.gdp.action.listst.GDPActionListSt;
import com.gmail.alaerof.components.mainframe.gdp.action.listtrain.GDPActionListTrain;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

/**
 *  ������ ���������� ��������������� ��� (1 ������ ��� ���� ���)
 *  ������ ������ ������� + ��������
 *  [GDPActionListTrain | GDPActionTabPane]
 */
public class GDPActionPanel extends JPanel {
    private GDPActionTabPane actionTabPane;
    private GDPActionListTrain actionListTrain;

    public GDPActionPanel(){
        actionTabPane = new GDPActionTabPane();
        actionListTrain = new GDPActionListTrain();

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, actionListTrain,
                actionTabPane);
        actionTabPane.setMinimumSize(new Dimension(407,0));
        actionListTrain.setMinimumSize(new Dimension(290, 0));
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(290);
        //splitPane.setMinimumSize(new Dimension(560, 0));
        splitPane.setPreferredSize(new Dimension(660, 0));
        this.setLayout(new BorderLayout());
        this.add(splitPane, BorderLayout.CENTER);
    }

    public GDPActionListTrain getActionListTrain() {
        return actionListTrain;
    }
    public GDPActionListSt getActionListSt() {
        return actionTabPane.getActionListSt();
    }

    public GDPActionListSpan getActionListSpan() {
        return actionTabPane.getActionListSpan();
    }

    /**
     * ��������� �������� �� ������� ������� (����������), ���� �� ���
     */
    public void addActionListStTab() {
        actionTabPane.addActionListStTab();
    }

    /**
     * ��������� �������� �� ������� ���������, ���� �� ���
     */
    public void addActionListSpanTab() {
        actionTabPane.addActionListSpanTab();
    }

    /**
     * �������� ������ ���� action �����
     */
    public void clearTab(){
        if (actionListTrain != null) {
            actionListTrain.setDistrEditEntity(null);
        }
        actionTabPane.clearTab();
    }
}
