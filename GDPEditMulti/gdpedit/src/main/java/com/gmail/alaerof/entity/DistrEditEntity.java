package com.gmail.alaerof.entity;

import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.gmail.alaerof.manager.LocaleManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.TrainCombSt;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDistrTrainDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.train.TimeStation;
import com.gmail.alaerof.entity.train.TrainComparatorByCode;
import com.gmail.alaerof.entity.train.TrainStDouble;
import com.gmail.alaerof.entity.train.TrainStDoubleComparatorByCode;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.history.History;
import com.gmail.alaerof.util.TimeConverter;

/**
 * ��� �� ������� � ��� ���.
 * ������������ ��� ������ ������� ����� ������� ������ ���� ��� !!!
 *
 * @author Helen Yrofeeva
 */
public class DistrEditEntity {
    protected static Logger logger = LogManager.getLogger(DistrEditEntity.class);
    /** ��������� ����� ��� ���� ������� ������� */
    private TrainThreadGeneralParams params;
    /** ���, ��� ���� ����� �� ������� (�� ���� �� �������) */
    private DistrEntity distrEntity;
    /** �������� ��� */
    private String sourceGDP = "";
    /** ����� ������� �� ������� (� ����� � "�����"), ���� = IDtrain */
    private Map<Long, TrainThread> listTrain = new HashMap<>();
    /** ����� ������� ������� �� �������� ������� + ������, ���� = IDst */
    // ��� ����� ������ �� �������, � ������� ���������� ������������ ����� > 0
    private Map<Integer, TrainsOnStation> trainsOnStations = new HashMap<>();
    /** ������� �������� � ���*/
    private History history;
    /** GDPEditPanel extends JPanel - ������ - ��������� ���� ����� */
    private JComponent owner;
    /** ���� ������������� ����� ���� ����� ���� ������� */
    private ActionListener lineListener;
    private static ResourceBundle bundle;

    public DistrEditEntity(int idDistr) {
        distrEntity = new DistrEntity(GDPDAOFactoryCreater.getGDPDAOFactory(), idDistr);
        history = new History(String.valueOf(idDistr));
        IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
        try {
            sourceGDP = dao.getSourceGDP(idDistr);
        } catch (DataManageException e) {
            sourceGDP = "";
            logger.error(e.toString(), e);
        }
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.entity", LocaleManager.getLocale());
    }

    public void setComponentOwner(JComponent owner) {
        this.owner = owner;
    }

    public TrainThread getTrainThreadFromListByCodeName(int code, String name) {
        TrainThread train = null;
        ArrayList<TrainThread> list = new ArrayList<>(listTrain.values());
        int i = 0;
        while (train == null && i < list.size()) {
            if (list.get(i).equalsByCodeName(code, name)) {
                train = list.get(i);
            }
            i++;
        }
        return train;
    }

    public TrainThread getTrainThreadFromStByCodeName(int idSt, int code, String name) {
        TrainThread train = null;
        TrainsOnStation trOnSt = trainsOnStations.get(idSt);
        if (trOnSt != null) {
            ArrayList<TrainThread> list = new ArrayList<>(trOnSt.getTrainMap().values());
            int i = 0;
            while (train == null && i < list.size()) {
                if (list.get(i).equalsByCodeName(code, name)) {
                    train = list.get(i);
                }
                i++;
            }
        }
        return train;
    }

    /**
     * ������� ����� ����� � ���������� ���
     *
     * @param code �����
     * @param name ��������
     * @return ����� �����, ������� ��������, � ������ null, ���� ����� �����
     * ��� ���� �� �������
     */
    public TrainThread createTrain(int code, String name) {
        TrainThread train = null;
        if (lineListener != null) {
            // ���� ��������� ���� �� ����� ����� ��� ���,
            // � ������ �� �������
            train = getTrainThreadFromListByCodeName(code, name);
            if (train == null) {
                // ���� � ������ ���, �� �������� ����� �� �� ��� ������� �����
                IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
                try {
                    DistrTrain tr = dao.newTrain(code, name);
                    if (tr != null) {
                        tr.setIdDistr(distrEntity.getIdDistr());
                        train = new TrainThread(tr, distrEntity, owner, params,
                                history, true);
                        train.setLineListener(lineListener);
                    } else {
                        logger.error("Something went wrong: can't create train " + code + " - " + name);
                    }
                } catch (DataManageException e) {
                    logger.error(e.toString(), e);
                }
            }
        }
        return train;
    }

    public TrainThread addTrain(TrainThread train) {
        if (train != null) {
            train.addToOwner();
            return listTrain.put(train.getIdTrain(), train);
        } else
            return null;
    }

    public TrainThread removeTrain(TrainThread train) {
        if (train != null) {
            train.removeFromOwner();
            return listTrain.remove(train.getIdTrain());
        } else
            return null;
    }

    public boolean listTrainLoaded() {
        return !(listTrain.isEmpty());
    }

    public DistrEntity getDistrEntity() {
        return distrEntity;
    }

    @Override
    public String toString() {
        return distrEntity.getDistrName();
    }

    public int getIdDistr() {
        return distrEntity.getIdDistr();
    }

    /**
     * �������� ��� ������� �� ��
     * @param onlyShowParam ���� true, �� ��������� ������ ��������� ����������� ����� �������
     */
    public void clearDistrGDPFromDB(boolean onlyShowParam) {
        IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
        try {
            // �������� ��� �������
            dao.clearDistrGDP_ShowParam(distrEntity.getIdDistr(), 0);
            if (!onlyShowParam) {
                Collection<DistrTrain> trains = getDistrTrains();
                dao.clearDistrGDP(distrEntity.getIdDistr(), trains,0, false, false);
            }
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ��������� ��� � ��������� ������� � ��
     *
     * @param onlyShowParam ������� ���������� ������ ���������� ���������� ���
     */
    public void saveAllDistrTrainDB(boolean onlyShowParam) {
        IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
        // -- ��������� ������������ ���������� ���� !!!
        // ���������� �������� ����������� �������
        distrEntity.saveGDPGridConfig();
        // ���������� ��� ������� (���� + ������������)
        Collection<TrainThread> trainThreads = listTrain.values();
        Collection<DistrTrain> distrTrains = getDistrTrains();
        // for (TrainThread train : list) {
        try {
            if (!onlyShowParam) {
                // ���������� ��������� ��������� SpTOcc � ������������ � ��������� �� ��������
                updateAllTrainSpTOcc(trainThreads);
                dao.saveListTrainGDP(distrTrains, getIdDistr());
            }

            dao.saveListTrainGDP_ShowParam(distrTrains, getIdDistr());

        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
        // }
        // ���������� ��� ������� (�� ��� �� ��������)
        // ����� ������ ������������ �������
        List<DistrStation> listSt = distrEntity.getListSt();
        for (DistrStation st : listSt) {
            // ��� �������
            int idSt = st.getIdPoint();
            // ������, ���������� ��������� ������� �������
            // (�� ����. ����� � ������������)
            TrainsOnStation trainsOnStation = trainsOnStations.get(idSt);
            if (trainsOnStation != null) {
                // ��������� ������� �������
                Collection<DistrTrain>  distrTrainsSt = trainsOnStation.getDistrTrains();
                try {
                    dao.saveListTrainGDP(distrTrainsSt, getIdDistr());
                } catch (DataManageException  e) {
                    logger.error(e.toString(), e);
                }
                // ������ ������ �� �������
                Collection<TrainCombSt> trainCombSts = trainsOnStation.getTrainCombSts();
                try {
                    dao.saveAllTrainStDouble(idSt, trainCombSts);
                } catch (DataManageException  e) {
                    logger.error(e.toString(), e);
                }
            }
        }

        // ���������� � �� ��������� ���
        try {
            dao.updateSourceGDP(getIdDistr(), getSoureGDP());
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * �������� ��������� ��������� � ���� "�����" ������� � ������������
     * @param trainThreads
     */
    private void updateAllTrainSpTOcc(Collection<TrainThread> trainThreads) {
        for (TrainThread trainThread : trainThreads) {
            if (trainThread.getDistrTrain().getIdDistr() == getIdDistr()) {
                trainThread.updateSpTOcc();
            }
        }
    }

    /**
     * �������� �� �� ���������� ���� ������� � ����������
     * HashMap<Integer,TrainThread> listTrain
     *
     * @param hourBegin    ����� ������ "����" �� �������
     * @param hourEnd      ����� ��������� "����" �� �������
     * @param lineListener ActionListener ���� ������������� ����� ���� ����� ����� ������
     */
    public void loadListDistrTrain(int hourBegin, int hourEnd, ActionListener lineListener,
                                   boolean isTrainThreadVisible) {
        try {
            this.lineListener = lineListener;
            TrainThreadGeneralParams params = getTrainParams();
            IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
            List<DistrTrain> listDistrTrain = dao.getDistrTrains(distrEntity.getIdDistr());

            trainsOnStations = new HashMap<>(); // ������ ��� ���� ����� ��������� ��� �� ��������
            listTrain = new HashMap<>();
            sourceGDP = dao.getSourceGDP(this.getIdDistr());

            TrainThread thread = null;
            for (DistrTrain dtr : listDistrTrain) {
                String tmOff = dtr.getOccupyOff();
                String[] ss = tmOff.split(":");
                int h = Integer.parseInt(ss[0]);
                if (hourBegin <= h && h <= hourEnd) {
                    thread = new TrainThread(dtr, distrEntity, owner, params, history, isTrainThreadVisible);
                    thread.setLineListener(lineListener);
                    listTrain.put(thread.getIdTrain(), thread);
                }
            }

            // ���� ��������� ������, ���� ���� �� ������� �������� �������
            List<DistrStation> list = distrEntity.getListSt();
            for (int i = 0; i < list.size(); i++) {
                DistrStation st = list.get(i);
                if (st.isExpanded()) {
                    int idSt = st.getIdPoint();
                    loadListDistrTrainOnStation(hourBegin, hourEnd, lineListener, idSt, isTrainThreadVisible);
                    loadTrainStDouble(hourBegin, hourEnd, idSt);
                }
            }
        } catch ( DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    private TrainThread searchTrain(long idTrain, int idSt) {
        TrainThread train = null;
        train = listTrain.get(idTrain);
        if (train == null) {
            TrainsOnStation listTr = trainsOnStations.get(idSt);
            train = listTr.getTrainMap().get(idTrain);
        }
        return train;
    }

    /**
     * �������� ������ �� ��, ��������� �� ���. ������� � �������� ����������
     * �������� ������
     *
     * @param hourBegin
     * @param hourEnd
     * @param idSt
     */
    public void loadTrainStDouble(int hourBegin, int hourEnd, int idSt) {
        try {
            TrainsOnStation trOnSt = trainsOnStations.get(idSt);
            if (trOnSt == null) {
                trOnSt = new TrainsOnStation(idSt);
            }
            List<TrainCombSt> trainCombStList = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO().getListTrainCombSt(idSt);
            ArrayList<TrainStDouble> listTrainStDouble = new ArrayList<>();
            trOnSt.setListTrainStDouble(listTrainStDouble);
            TrainThread trainThreadOn = null;
            TrainThread trainThreadOff = null;
            TrainStDouble trD = null;
            for (int i = 0; i < trainCombStList.size(); i++) {
                TrainCombSt trainCombSt = trainCombStList.get(i);
                // ������� ������� �������
                DistrStation station = distrEntity.getDistrStationFromVisible(idSt);
                // ������� ����� ��������, ����� ���� ������� �������
                trainThreadOn = searchTrain(trainCombSt.IDtrainOn, idSt);
                // ������� ����� �����������, ����� ���� ������� �������
                trainThreadOff = searchTrain(trainCombSt.IDtrainOff, idSt);
                // ��������� ������������ ��������� �������
                if (trainThreadOn != null && trainThreadOff != null) {
                    if (trainThreadOn.getIdDistr() != trainCombSt.IDdistrOn) {
                        trainThreadOn = null;
                    }
                    if (trainThreadOff.getIdDistr() != trainCombSt.IDdistrOff) {
                        trainThreadOff = null;
                    }
                } else {
                    logger.debug("not found one of the trains, trainCombSt: " + trainCombSt);
                }
                if (station != null && trainThreadOn != null && trainThreadOff != null) {
                    // ��������� ����� ��������/�����������
                    TimeStation stOn = trainThreadOn.getTimeSt(idSt);
                    TimeStation stOff = trainThreadOff.getTimeSt(idSt);
                    if (stOn != null && stOff != null) {
                        trainCombSt.occupyOn = stOn.getLineStTrain().getOccupyOn();
                        trainCombSt.occupyOff = stOff.getLineStTrain().getOccupyOff();
                        trainCombSt.idLineSt = stOn.getLineStTrain().getIdLineSt();

                        if (trainCombSt.occupyOn != null && trainCombSt.occupyOff != null) {
                            trainCombSt.tstop = TimeConverter.minutesBetween(trainCombSt.occupyOn,
                                    trainCombSt.occupyOff);

                            trD = new TrainStDouble(owner, trainThreadOn, trainThreadOff, station,
                                    trainCombSt);

                            listTrainStDouble.add(trD);

                            trainThreadOn.addTrainStDouble(trD);
                            trainThreadOff.addTrainStDouble(trD);
                        }
                    }
                }
            }

        } catch ( DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public void addTrainStDouble(TrainThread trainThreadOn, TrainThread trainThreadOff, DistrStation station, List<String> warnList) {
        int idSt = station.getIdPoint();
        if (station != null && trainThreadOn != null && trainThreadOff != null) {
            TrainsOnStation trOnSt = trainsOnStations.get(idSt);
            TrainStDouble trD = createTrainStDouble(trainThreadOn, trainThreadOff, station, warnList);
            if(trD!=null){
                    trOnSt.getListTrainStDouble().add(trD);
                    Collections.sort(trOnSt.getListTrainStDouble(), new TrainStDoubleComparatorByCode());

                    trainThreadOn.addTrainStDouble(trD);
                    trainThreadOff.addTrainStDouble(trD);
            }
        }
    }

    public TrainStDouble createTrainStDouble(TrainThread trainThreadOn, TrainThread trainThreadOff, DistrStation station,
                                             List<String> warnList) {
        TrainStDouble trD = null;
        int idSt = station.getIdPoint();
        if (station != null && trainThreadOn != null && trainThreadOff != null) {
            TrainCombSt trainCombSt = new TrainCombSt();
            // ��������� ����� ��������/�����������
            TimeStation stOn = trainThreadOn.getTimeSt(idSt);
            TimeStation stOff = trainThreadOff.getTimeSt(idSt);
            trainCombSt.IDst = idSt;
            trainCombSt.occupyOn = stOn.getLineStTrain().getOccupyOn();
            trainCombSt.occupyOff = stOff.getLineStTrain().getOccupyOff();
            int idLineOn = stOn.getLineStTrain().getIdLineSt();
            int idLineOff = stOff.getLineStTrain().getIdLineSt();
            //������ ����� ������ �������� � ������ ����������� �� ���������:
            if (idLineOn != idLineOff) {
                String warn = bundle.getString("The numbers of tracks of the arrival train and the departure train do not match: ") +
                        trainThreadOn.getCode() + "(" + trainThreadOn.getNameTR()+") - " +
                        trainThreadOff.getCode() + "(" + trainThreadOff.getNameTR()+")";
                logger.warn(warn);
                warnList.add(warn);
                trainThreadOff.setLineStTr(stOff.getDistrStation().getIdPoint(), idLineOn);
            }
            trainCombSt.idLineSt = stOn.getLineStTrain().getIdLineSt();
            trainCombSt.IDtrainOn = trainThreadOn.getIdTrain();
            trainCombSt.IDtrainOff = trainThreadOff.getIdTrain();
            trainCombSt.IDdistrOn = trainThreadOn.getIdDistr();
            trainCombSt.IDdistrOff = trainThreadOff.getIdDistr();
            if (trainCombSt.occupyOn != null && trainCombSt.occupyOff != null) {
                trainCombSt.tstop = TimeConverter.minutesBetween(trainCombSt.occupyOn, trainCombSt.occupyOff);
                trD = new TrainStDouble(owner, trainThreadOn, trainThreadOff, station, trainCombSt);
            }
        }
        return trD;
    }

    /**
     * �������� �� �� ���������� ���� ������� �� ������� � ����������
     * HashMap<Integer,TrainThread> listTrain ��� �������
     */
    public void loadListDistrTrainOnStation(int hourBegin, int hourEnd, ActionListener lineListener, int idSt,
                                            boolean isTrainThreadVisible) {
        try {
            TrainThreadGeneralParams params = getTrainParams();

            List<DistrTrain> listDistrTrain = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO()
                    .getDistrTrainsOnStation(idSt);
            TrainsOnStation trOnSt = trainsOnStations.get(idSt);
            if (trOnSt == null) {
                trOnSt = new TrainsOnStation(idSt);
            }
            HashMap<Long, TrainThread> listTrains = new HashMap<>();

            TrainThread thread = null;
            for (DistrTrain dtr : listDistrTrain) {
                TrainThread trainTR = listTrain.get(dtr.getIdTrain());
                // ���� ����� ������ ��� � ��� ��������� "�����" �
                // "������������ = �����"
                // �� ����� ��������� ���� ����� � ������ �� �������
                if (trainTR == null) {
                    String tmOff = dtr.getTimeOnStation(idSt);
                    if (tmOff != null) {
                        String[] ss = tmOff.split(":");
                        int h = Integer.parseInt(ss[0]);
                        if (hourBegin <= h && h <= hourEnd) {
                            thread = new TrainThread(dtr, distrEntity, owner, params, history, isTrainThreadVisible);
                            thread.getDistrTrain().setOnlyStation(true);
                            thread.setLineListener(lineListener);
                            listTrains.put(thread.getIdTrain(), thread);
                        }
                    }
                }
            }
            trOnSt.setTrainMap(listTrains);
            trainsOnStations.put(idSt, trOnSt);
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public void removeListDistrTrain() {
        listTrain = null;
        listTrain = new HashMap<>();
        trainsOnStations = null;
        trainsOnStations = new HashMap<>();
        params = null;
    }

    public void setAllTrainLocation() {
        // ��������� ����� � ������������
        if (listTrain != null) {
            Collection<TrainThread> list = listTrain.values();
            for (TrainThread tr : list) {
                tr.setTrainLocation();
            }
        }
        // ��������� �� ����� �������
        List<DistrStation> listSt = distrEntity.getListSt();
        for (DistrStation st : listSt) {
            int idSt = st.getIdPoint();
            TrainsOnStation trainsOnStation = trainsOnStations.get(idSt);
            if (trainsOnStation != null) {
                trainsOnStation.setAllTrainLocation();
            }
        }
    }

    private void updateTrainParams() {
        if (params != null) {
            GDPGridConfig conf = distrEntity.getConfigGDP();
            params.captionSp = conf.trainCaptionSpFont;
            params.captionSt = conf.trainCaptionStFont;
            params.time = conf.trainTimeFont;
            params.codeStepO = conf.codeStepO;
            params.codeStepE = conf.codeStepE;
            params.scale = conf.scale;
            if (distrEntity.getxLeftNoJoint() != null) {
                params.xLeft = distrEntity.getxLeftNoJoint().xLeftGDP;
                params.xRight = distrEntity.getxLeftNoJoint().xRightGDP;
                params.xRight24 = distrEntity.getxLeftNoJoint().xRightGDP24;
            }
            if (distrEntity.getxLeftJoint() != null) {
                params.xLRightJoint = distrEntity.getxLeftJoint().xRight;
                params.yIndTop = distrEntity.getxLeftJoint().indTop;
            }
        }
    }

    private TrainThreadGeneralParams getTrainParams() {
        if (params == null) {
            params = new TrainThreadGeneralParams();
        }
        updateTrainParams();
        return params;
    }

    /**
     * ���������� ������� ������� ������� �� �������
     * -1 �� ��������� � ��������
     * 0 �� �����������
     * 1 �����������
     *
     * @param idSt �� �������
     * @return -1 or 0 or 1
     */
    public int existTrainsOnStation(int idSt) {
        int res = -1;
        DistrStation ds = distrEntity.getDistrStationFromVisible(idSt);
        if (ds.getListLines().size() > 0) {
            TrainsOnStation listTR = trainsOnStations.get(idSt);
            if (listTR == null) {
                res = 0;
            } else {
                res = 1;
            }
        }
        return res;
    }

    public History getHistory() {
        return history;
    }

    public void clearHistory() {
        history.clear();
    }

    public List<TrainThread> getListTrains() {
        List<TrainThread> list = new ArrayList<>();
        if (listTrain != null) {
            list.addAll(listTrain.values());
        }
        return list;
    }

    public String getSoureGDP() {
        return sourceGDP;
    }

    public void setSourceGDP(String sourceGDP) {
        this.sourceGDP = sourceGDP;
    }

    public void removeTrainStDouble(TrainStDouble trStD) {
        Integer idSt = trStD.getStationBegin().getDistrStation().getIdPoint();
        TrainsOnStation trOnSt = trainsOnStations.get(idSt);
        trOnSt.removeTrainStDouble(trStD);
    }

    public List<TrainStDouble> getListTrainStDouble(int idSt) {
        List<TrainStDouble> list = null;
        TrainsOnStation tr = trainsOnStations.get(idSt);
        if (tr != null) {
            list = tr.getListTrainStDouble();
        }
        return list;
    }

    public List<TrainThread> getListTrainSt(int idSt) {
        List<TrainThread> list = null;
        TrainsOnStation tr = trainsOnStations.get(idSt);
        if (tr != null) {
            list = new ArrayList<>(tr.getTrainMap().values());
            list.addAll(getSelfTrainsOnSt(idSt));
            Collections.sort(list, new TrainComparatorByCode());
        }
        return list;
    }

    private List<TrainThread> getSelfTrainsOnSt(int idSt) {
        List<TrainThread> list = new ArrayList<>();
        List<TrainThread> listTr = new ArrayList<>(listTrain.values());
        for (int i = 0; i < listTr.size(); i++) {
            TrainThread tr = listTr.get(i);
            TimeStation st = tr.getTimeSt(idSt);
            if (st != null) {
                list.add(tr);
            }
        }
        return list;
    }

    public void updateSpanTime() {
        distrEntity.updateSpanTime();
    }

    public void setNoJoint() {
        distrEntity.setNoJoint();
    }

    /**
     * �������� ��������� ���� ����� ������� � ������ �� �������� �� �������
     *
     * @param grGDP ����� ��� ���������
     */
    public void paintInPictureAllTrains(Graphics grGDP, GDPGridConfig config) {
        updateTrainParams();
        List<TrainThread> listTr = new ArrayList<>(listTrain.values());
        for (int i = 0; i < listTr.size(); i++) {
            TrainThread tr = listTr.get(i);
//            tr.drawInPicture(grGDP);
            tr.drawInPicture(grGDP, config);
        }
        Collection<TrainsOnStation> cl = trainsOnStations.values();
        Iterator<TrainsOnStation> it = cl.iterator();
        while (it.hasNext()) {
            TrainsOnStation trSt = it.next();
            List<TrainStDouble> listTrStD = trSt.getListTrainStDouble();
            for (TrainStDouble trStD : listTrStD) {
//                trStD.drawInPicture(grGDP);
                trStD.drawInPicture(grGDP, config);
            }
            List<TrainThread> listTrain = trSt.getTrainList();
            for (TrainThread tr : listTrain) {
//                tr.drawInPicture(grGDP);
                tr.drawInPicture(grGDP, config);
            }
        }
    }

    public void clearTrainThreadMap() {
        listTrain.clear();
        trainsOnStations.clear();
    }

    public Collection<DistrTrain> getDistrTrains() {
        List<DistrTrain> list = new ArrayList<>();
        if (listTrain != null) {
            for (TrainThread trainThread : listTrain.values()) {
                list.add(trainThread.getDistrTrain());
            }
        }
        return list;
    }

    /**
     * �� ������, ����� ������ ���� ��������� ��� ����������� �� ���� ���
     */
    public void makeTrainsVisible(ActionListener lineListener, JComponent owner) {
        this.lineListener = lineListener;
        this.owner = owner;
        updateTrainParams();
        for(TrainThread trainThread: listTrain.values()){
            trainThread.setOwner(owner);
            trainThread.makeVisible();
            trainThread.setLineListener(lineListener);
        }
    }

    public void refreshTrainThreads() {
        if (listTrain != null) {
            for (TrainThread trainThread : listTrain.values()) {
                refreshTrainThread(trainThread);
            }
        }
    }

    public void refreshTrainThread(TrainThread trainThread) {
        if (!trainThread.isForeign() && !trainThread.isHidden()) {
            trainThread.refreshLines();
            trainThread.setLineListener(lineListener);
        }
    }
}
