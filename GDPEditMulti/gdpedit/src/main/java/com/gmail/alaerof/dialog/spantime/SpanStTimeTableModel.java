package com.gmail.alaerof.dialog.spantime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.SpanStTime;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class SpanStTimeTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("spantime.number"),
            bundle.getString("spantime.odd"),
            bundle.getString("spantime.even") };
    private List<SpanStTime> list;
    private List<SpanStTime> allList;
    private LocomotiveType loc;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.spantime", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else
            return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            SpanStTime tm = list.get(row);
            switch (col) {
            case 0:
                return tm.getNum();
            case 1:
                return tm.getTmoveO();
            case 2:
                return tm.getTmoveE();
            default:
                break;
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return Integer.class;
        case 1:
        case 2:
            return Double.class;
        default:
            return Object.class;
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (list != null && aValue != null && row < getRowCount() && col < getColumnCount()) {
            SpanStTime tm = list.get(row);
            switch (col) {
            case 1:
                tm.setTmoveO((Double)aValue);
                break;
            case 2:
                tm.setTmoveE((Double)aValue);
                break;
            default:
                break;
            }
        }
    }

    public LocomotiveType getLoc() {
        return loc;
    }

    public void setLoc(LocomotiveType loc) {
        this.loc = loc;
        fillList();
    }

    public List<SpanStTime> getAllList() {
        return allList;
    }

    public void setAllList(List<SpanStTime> allList) {
        this.allList = allList;
    }

    private void fillList() {
        if (allList != null && loc != null) {
            list = new ArrayList<>();
            for (int i = 0; i < allList.size(); i++) {
                SpanStTime tm = allList.get(i);
                if (tm.getIdLType() == loc.getIdLType()) {
                    list.add(tm);
                }
            }
            Collections.sort(list);
        }
    }
}
