package com.gmail.alaerof.javafx.control.editabletableview;

import java.util.function.Function;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;

public class EditableTableViewCreator<T> {

    public TableView<T> createEditableTableView() {
        TableView<T> table = new TableView<>();

        table.getSelectionModel().setCellSelectionEnabled(true);
        table.setEditable(true);

        table.setOnKeyPressed(event -> {
            TablePosition<T, ?> pos = table.getFocusModel().getFocusedCell() ;
            if (pos != null) {
                table.edit(pos.getRow(), pos.getTableColumn());
            }
        });

        return table;
    }

    public TableColumn<T, String> createStringColumn(String title, Function<T, StringProperty> property) {
        return createStringColumn(title, property, true);
    }

    public TableColumn<T, String> createStringColumn(String title, Function<T, StringProperty> property, boolean editable) {
        TableColumn<T, String> col = new TableColumn<>(title);
        col.setEditable(editable);
        col.setCellValueFactory(cellData -> property.apply(cellData.getValue()));
        col.setCellFactory(column -> new StringEditCell(property));
        return col;
    }

    public TableColumn<T, Number> createIntegerColumn(String title, Function<T, IntegerProperty> property) {
        return createIntegerColumn(title, property, true);
    }

    public TableColumn<T, Number> createIntegerColumn(String title, Function<T, IntegerProperty> property, boolean editable) {
        TableColumn<T, Number> col = new TableColumn<>(title);
        col.setEditable(editable);
        col.setCellValueFactory(cellData -> property.apply(cellData.getValue()));
        col.setCellFactory(column -> new IntegerEditCell(property));
        return col;
    }

    public TableColumn<T, Number> createDoubleColumn(String title, Function<T, DoubleProperty> property) {
        return createDoubleColumn(title, property, true);
    }

    public TableColumn<T, Number> createDoubleColumn(String title, Function<T, DoubleProperty> property, boolean editable) {
        TableColumn<T, Number> col = new TableColumn<>(title);
        col.setEditable(editable);
        col.setCellValueFactory(cellData -> property.apply(cellData.getValue()));
        col.setCellFactory(column -> new DoubleEditCell(property, editable));
        return col;
    }

    public TableColumn<T, Boolean> createBooleanColumn(String title, Function<T, BooleanProperty> property) {
        return createBooleanColumn(title, property, true);
    }

    public TableColumn<T, Boolean> createBooleanColumn(String title, Function<T, BooleanProperty> property, boolean editable) {
        TableColumn<T, Boolean> col = new TableColumn<>(title);
        col.setEditable(editable);
        col.setCellValueFactory(cellData -> property.apply(cellData.getValue()));
        col.setCellFactory(column -> new StringEditCell(property));
        return col;
    }

    public TableColumn<T, Number> createColorColumn(String title, Function<T, IntegerProperty> property) {
        TableColumn<T, Number> col = new TableColumn<>(title);
        col.setEditable(false);
        col.setCellValueFactory(cellData -> property.apply(cellData.getValue()));
        col.setCellFactory(column -> new ColorEditCell(property));
        return col;
    }
}
