package com.gmail.alaerof.dialog.config;

import java.awt.Color;
import java.io.Serializable;
/**
 * ��������� ��� ���������� (����)
 * @author Helen Yrofeeva
 *
 */
public class ApplicationConfig implements Serializable{
    private static final long serialVersionUID = 1L;
    private Color backgroundColor = Color.white;
    private Color selectTrainColor = Color.orange;

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Color getSelectTrainColor() {
        return selectTrainColor;
    }

    public void setSelectTrainColor(Color selectTrainColor) {
        this.selectTrainColor = selectTrainColor;
    }

}
