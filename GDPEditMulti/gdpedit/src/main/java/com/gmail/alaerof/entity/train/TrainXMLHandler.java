package com.gmail.alaerof.entity.train;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainShowParam;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.TrainCode;
import com.gmail.alaerof.dao.dto.gdp.TrainHideSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainLineStyleSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanColor;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanInscr;
import com.gmail.alaerof.dao.dto.gdp.TrainStop;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrPoint.TypePoint;

/**
 * @author Helen Yrofeeva
 */
public class TrainXMLHandler {
    private static Logger logger = LogManager.getLogger(TrainXMLHandler.class);

    public static Document getDocument() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document document = null;
        try {
            db = dbf.newDocumentBuilder();
            document = db.newDocument();
            Element root = document.createElement("CopyTrain");
            document.appendChild(root);
        } catch (ParserConfigurationException e) {
            logger.error(e.toString(), e);
        }
        return document;
    }

    /**
     * ������� xml ������� � ��������� ������� � ����� ������
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    private static Element saveMainToXML(TrainThread train, Document document) {
        Element e = document.createElement("TrainThread");
        try {
            // �������� ��������� ����� ������
            e.setAttribute("idTrain", String.valueOf(train.getIdTrain()));
            e.setAttribute("codeTR", train.getCode());
            e.setAttribute("nameTR", train.getNameTR());
            e.setAttribute("typeTR", train.getDistrTrain().getTypeTR().getString());
            e.setAttribute("oe", train.getDistrTrain().getOe().getString());
            e.setAttribute("idDistr", String.valueOf(train.getIdDistr()));
            e.setAttribute("ps", train.getDistrTrain().getPs());

            // �������� ��������� ����������� ����� ������
            DistrTrainShowParam shp = train.getDistrTrain().getShowParam();
            String hidden = "false";
            if (shp.isHidden())
                hidden = "true";
            e.setAttribute("hidden", hidden);
            e.setAttribute("colorTR", String.valueOf(shp.getColorTR()));
            e.setAttribute("ainscr", shp.getAinscr());
            e.setAttribute("lineStyle", String.valueOf(shp.getLineStyle()));
            e.setAttribute("widthTR", String.valueOf(shp.getWidthTR()));
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return e;
    }

    /**
     * ���������� �� ���������� �������
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    private static Element saveLineStTrainToXML(TrainThread train, Document document) {
        Element listSt = document.createElement("listTrainSt");
        try {
            List<TimeStation> listTimeSt = train.getListTimeSt();
            for (TimeStation timeSt : listTimeSt) {
                Element st = document.createElement("TrainSt");
                listSt.appendChild(st);
                LineStTrain lst = timeSt.lineStTrain;
                st.setAttribute("idSt", String.valueOf(lst.getIdSt()));
                st.setAttribute("tOn", lst.getOccupyOn());
                st.setAttribute("tOff", lst.getOccupyOff());
                st.setAttribute("sk_ps", lst.getSk_ps());
                st.setAttribute("lineSt", timeSt.getLineName(lst.getIdLineSt()));
                st.setAttribute("texStop", String.valueOf(lst.getTexStop()));
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return listSt;
    }

    /**
     * ���������� �� �.�.
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    private static Element saveTrainTownToXML(TrainThread train, Document document) {
        Element listTw = document.createElement("listTrainTown");
        try {
            Collection<TrainStop> listTS = train.getDistrTrain().getTrainStops();
            if (listTS != null) {
                for (TrainStop ts : listTS) {
                    Element st = document.createElement("TrainTown");
                    listTw.appendChild(st);
                    st.setAttribute("idSpanst", String.valueOf(ts.getIdSpanst()));
                    st.setAttribute("tOn", ts.getOccupyOn());
                    st.setAttribute("tOff", ts.getOccupyOff());
                    st.setAttribute("sk_ps", ts.getSk_ps());
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return listTw;
    }

    /**
     * ����������� ������� ������ ����� �������
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    private static Element saveTrainCodeToXML(TrainThread train, Document document) {
        Element listE = document.createElement("listTrainCode");
        try {
            List<TrainCode> list = train.getDistrTrain().getListTrainCode();
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    Element el = document.createElement("TrainCode");
                    listE.appendChild(el);
                    TrainCode le = list.get(i);
                    el.setAttribute("idTrain", "" + le.getIdTrain());
                    el.setAttribute("idDistr", "" + le.getIdDistr());
                    el.setAttribute("idStB", "" + le.getIdStB());
                    el.setAttribute("viewCode", "" + le.getViewCode());
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return listE;
    }

    /**
     * ������� ����� ������ ����� �������
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    private static Element saveTrainHideSpanToXML(TrainThread train, Document document) {
        Element listE = document.createElement("listTrainHideSpan");
        try {
            List<TrainHideSpan> list = train.getDistrTrain().getListTrainHideSpan();
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    Element el = document.createElement("TrainHideSpan");
                    listE.appendChild(el);
                    TrainHideSpan le = list.get(i);
                    el.setAttribute("idTrain", "" + le.getIdTrain());
                    el.setAttribute("idDistr", "" + le.getIdDistr());
                    el.setAttribute("idStB", "" + le.getIdStB());
                    el.setAttribute("shape", "" + le.getShape());
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return listE;
    }

    /**
     * ����� ����� ������ ����� �������
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    private static Element saveTrainLineStyleSpanToXML(TrainThread train, Document document) {
        Element listE = document.createElement("listTrainLineStyleSpan");
        try {
            List<TrainLineStyleSpan> list = train.getDistrTrain().getListTrainLineStyleSpan();
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    Element el = document.createElement("TrainLineStyleSpan");
                    listE.appendChild(el);
                    TrainLineStyleSpan le = list.get(i);
                    el.setAttribute("idTrain", "" + le.getIdTrain());
                    el.setAttribute("idDistr", "" + le.getIdDistr());
                    el.setAttribute("idStB", "" + le.getIdStB());
                    el.setAttribute("shape", "" + le.getShape());
                    el.setAttribute("lineStyle", "" + le.getLineStyle());
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return listE;
    }

    /**
     * ���� ����� ������ ����� �������
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    private static Element saveTrainSpanColorToXML(TrainThread train, Document document) {
        Element listE = document.createElement("listTrainSpanColor");
        try {
            List<TrainSpanColor> list = train.getDistrTrain().getListTrainSpanColor();
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    Element el = document.createElement("TrainSpanColor");
                    listE.appendChild(el);
                    TrainSpanColor le = list.get(i);
                    el.setAttribute("idTrain", "" + le.getIdTrain());
                    el.setAttribute("idDistr", "" + le.getIdDistr());
                    el.setAttribute("idStB", "" + le.getIdStB());
                    el.setAttribute("colorTR", "" + le.getColorTR());
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return listE;
    }

    /**
     * �������� ������� ����� ������ ����� �������
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    private static Element saveTrainSpanInscrToXML(TrainThread train, Document document) {
        Element listE = document.createElement("listTrainSpanInscr");
        try {
            List<TrainSpanInscr> list = train.getDistrTrain().getListTrainSpanInscr();
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    Element el = document.createElement("TrainSpanInscr");
                    listE.appendChild(el);
                    TrainSpanInscr le = list.get(i);
                    el.setAttribute("idTrain", "" + le.getIdTrain());
                    el.setAttribute("idDistr", "" + le.getIdDistr());
                    el.setAttribute("idStB", "" + le.getIdStB());
                    el.setAttribute("AInscr", "" + le.getAInscr());
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        return listE;
    }

    /**
     * ������� xml ������� � ������� � ������
     * @param train ����� ������
     * @param document ��� �������� ��������
     * @return xml ������� c �������
     */
    public static Element saveToXML(TrainThread train, Document document) {
        Element e = saveMainToXML(train, document);

        {
            // ���������� �� ���������� �������
            Element listE = saveLineStTrainToXML(train, document);
            e.appendChild(listE);
        }
        {
            // ���������� �� ������������ �������
            Element listE = saveTrainTownToXML(train, document);
            e.appendChild(listE);
        }

        // ����� ������� ��������� �������, ���� IDspan
        // private HashMap<Integer, SpTOcc> mapSpTOcc;

        {
            // ** ����� �������� ����������� �������, ���� IDst */
            Element listE = saveTrainCodeToXML(train, document);
            e.appendChild(listE);
        }

        {
            // ** ����� ������� �����, ���� IDst */
            Element listE = saveTrainHideSpanToXML(train, document);
            e.appendChild(listE);
        }
        {
            // ** ����� ����� �����, ���� IDst */
            Element listE = saveTrainLineStyleSpanToXML(train, document);
            e.appendChild(listE);
        }
        {
            // ** ����� ����� �����, ���� IDst */
            Element listE = saveTrainSpanColorToXML(train, document);
            e.appendChild(listE);
        }
        {
            // ** ����� �������� �����, ���� IDst */
            Element listE = saveTrainSpanInscrToXML(train, document);
            e.appendChild(listE);
        }

        return e;
    }

    /**
     * ��������� ����� ��������� ������� � ����������� ����������� �� xml
     * @param train ����� ������ ��� ����������
     * @param xmlElement ������ �������
     */
    public static void loadMainFromXML(TrainThread train, Element xmlElement, boolean loadID) {
        try {
            NodeList nlist = xmlElement.getElementsByTagName("TrainThread");
            DistrTrain dt = train.getDistrTrain();
            DistrTrainShowParam shp = train.getDistrTrain().getShowParam();
            if (nlist.getLength() > 0) {
                Element el = (Element) nlist.item(0);

                // �������� ��������� ����� ������
                if (loadID) {
                    dt.setIdTrain(Long.parseLong(el.getAttribute("idTrain")));
                    dt.setIdDistr(Integer.parseInt(el.getAttribute("idDistr")));
                }
                dt.setCodeTR(el.getAttribute("codeTR"));
                dt.setNameTR(el.getAttribute("nameTR"));
                dt.setTypeTR(el.getAttribute("typeTR"));
                dt.setOe();
                dt.setPs(el.getAttribute("ps"));

                // �������� ��������� ����������� ����� ������
                shp.setHidden(Boolean.parseBoolean(el.getAttribute("hidden")));
                shp.setColorTR(Integer.parseInt(el.getAttribute("colorTR")));
                shp.setAinscr(el.getAttribute("ainscr"));
                shp.setLineStyle(Integer.parseInt(el.getAttribute("lineStyle")));
                shp.setWidthTR(Integer.parseInt(el.getAttribute("widthTR")));
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ��������� ����� ����������� �� �������
     * @param train ����� ������ ��� ����������
     * @param xmlElement ������ �������
     */
    public static void loadTimeFromXML(TrainThread train, Element xmlElement) {
        try {
            ArrayList<TrainStTableData> listData = new ArrayList<>();
            TrainStTableData std = null;
            DistrPoint dp = null;
            // ���������� �� �.�.
            NodeList nlist = xmlElement.getElementsByTagName("TrainSt");
            for (int i = 0; i < nlist.getLength(); i++) {
                Element el = (Element) nlist.item(i);
                int idSt = Integer.parseInt(el.getAttribute("idSt"));
                dp = train.getDistrEntity().getDistrPoint(idSt, TypePoint.SeparatePoint);
                std = new TrainStTableData();
                std.distrpoint = dp;
                std.tOn = el.getAttribute("tOn");
                std.tOff = el.getAttribute("tOff");
                std.sk_ps = el.getAttribute("sk_ps");
                std.lineSt = el.getAttribute("lineSt");
                std.texStop = Integer.parseInt(el.getAttribute("texStop"));
                listData.add(std);
            }
            // ���������� �� �.�.
            nlist = xmlElement.getElementsByTagName("TrainTown");
            for (int i = 0; i < nlist.getLength(); i++) {
                Element el = (Element) nlist.item(i);
                int idSt = Integer.parseInt(el.getAttribute("idSpanst"));
                dp = train.getDistrEntity().getDistrPoint(idSt, TypePoint.StopPoint);
                std = new TrainStTableData();
                std.distrpoint = dp;
                std.tOn = el.getAttribute("tOn");
                std.tOff = el.getAttribute("tOff");
                std.sk_ps = el.getAttribute("sk_ps");
                listData.add(std);
            }
            train.applyThreadByTable(listData, true);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    // ** ����� �������� ����������� �������, ���� IDst */
    public static void loadTrainCodeFromXML(TrainThread train, Element xmlElement) {
        try {
            NodeList nlist = xmlElement.getElementsByTagName("TrainCode");
            DistrTrain ds = train.getDistrTrain();
            HashMap<Integer, TrainCode> map = new HashMap<>();
            for (int i = 0; i < nlist.getLength(); i++) {
                Element el = (Element) nlist.item(i);
                TrainCode tc = new TrainCode();
                tc.setIdDistr(train.getIdDistr());
                tc.setIdTrain(train.getIdTrain());
                tc.setIdStB(Integer.parseInt(el.getAttribute("idStB")));
                tc.setViewCode(Integer.parseInt(el.getAttribute("viewCode")));
                map.put(tc.getIdStB(), tc);
            }
            ds.setMapTrainCode(map);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    // ** ����� ������� �����, ���� IDst */
    public static void loadTrainHideSpanFromXML(TrainThread train, Element xmlElement) {
        try {
            NodeList nlist = xmlElement.getElementsByTagName("TrainHideSpan");
            DistrTrain ds = train.getDistrTrain();
            HashMap<Integer, TrainHideSpan> map = new HashMap<>();
            for (int i = 0; i < nlist.getLength(); i++) {
                Element el = (Element) nlist.item(i);
                TrainHideSpan tc = new TrainHideSpan();
                tc.setIdDistr(train.getIdDistr());
                tc.setIdTrain(train.getIdTrain());
                tc.setIdStB(Integer.parseInt(el.getAttribute("idStB")));
                tc.setShape(el.getAttribute("shape"));
                map.put(tc.getIdStB(), tc);
            }
            ds.setMapTrainHideSpan(map);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    // ** ����� ����� �����, ���� IDst */
    public static void loadTrainLineStyleSpanFromXML(TrainThread train, Element xmlElement) {
        try {
            NodeList nlist = xmlElement.getElementsByTagName("TrainLineStyleSpan");
            DistrTrain ds = train.getDistrTrain();
            HashMap<Integer, TrainLineStyleSpan> map = new HashMap<>();
            for (int i = 0; i < nlist.getLength(); i++) {
                Element el = (Element) nlist.item(i);
                TrainLineStyleSpan tc = new TrainLineStyleSpan();
                tc.setIdDistr(train.getIdDistr());
                tc.setIdTrain(train.getIdTrain());
                tc.setIdStB(Integer.parseInt(el.getAttribute("idStB")));
                tc.setShape(el.getAttribute("shape"));
                tc.setLineStyle(Integer.parseInt(el.getAttribute("lineStyle")));
                map.put(tc.getIdStB(), tc);
            }
            ds.setMapTrainLineStyleSpan(map);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    // ** ����� ����� �����, ���� IDst */
    public static void loadTrainSpanColorFromXML(TrainThread train, Element xmlElement) {
        try {
            NodeList nlist = xmlElement.getElementsByTagName("TrainSpanColor");
            DistrTrain ds = train.getDistrTrain();
            HashMap<Integer, TrainSpanColor> map = new HashMap<>();
            for (int i = 0; i < nlist.getLength(); i++) {
                Element el = (Element) nlist.item(i);
                TrainSpanColor tc = new TrainSpanColor();
                tc.setIdDistr(train.getIdDistr());
                tc.setIdTrain(train.getIdTrain());
                tc.setIdStB(Integer.parseInt(el.getAttribute("idStB")));
                tc.setColorTR(Integer.parseInt(el.getAttribute("colorTR")));
                map.put(tc.getIdStB(), tc);
            }
            ds.setMapTrainSpanColor(map);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    // ** ����� �������� �����, ���� IDst */
    public static void loadTrainSpanInscrFromXML(TrainThread train, Element xmlElement) {
        try {
            NodeList nlist = xmlElement.getElementsByTagName("TrainSpanInscr");
            DistrTrain ds = train.getDistrTrain();
            HashMap<Integer, TrainSpanInscr> map = new HashMap<>();
            for (int i = 0; i < nlist.getLength(); i++) {
                Element el = (Element) nlist.item(i);
                TrainSpanInscr tc = new TrainSpanInscr();
                tc.setIdDistr(train.getIdDistr());
                tc.setIdTrain(train.getIdTrain());
                tc.setIdStB(Integer.parseInt(el.getAttribute("idStB")));
                tc.setAInscr(el.getAttribute("AInscr"));
                map.put(tc.getIdStB(), tc);
            }
            ds.setMapTrainSpanInscr(map);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * 
     * @param train
     * @param xmlElement
     * @param loadID
     */
    public static void loadAllFromXML(TrainThread train, Element xmlElement, boolean loadID) {
        loadMainFromXML(train, xmlElement, loadID);
        loadTimeFromXML(train, xmlElement);
        loadTrainCodeFromXML(train, xmlElement);
        loadTrainHideSpanFromXML(train, xmlElement);
        loadTrainLineStyleSpanFromXML(train, xmlElement);
        loadTrainSpanColorFromXML(train, xmlElement);
        loadTrainSpanInscrFromXML(train, xmlElement);
    }
}
