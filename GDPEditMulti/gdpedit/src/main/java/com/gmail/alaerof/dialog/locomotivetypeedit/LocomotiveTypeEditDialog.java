package com.gmail.alaerof.dialog.locomotivetypeedit;

import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.util.ColorConverter;
import com.gmail.alaerof.dialog.ColorDialog;
import com.gmail.alaerof.util.TableUtils;
import com.gmail.alaerof.manager.LocaleManager;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;

public class LocomotiveTypeEditDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(LocomotiveTypeEditDialog.class);
    private final JPanel contentPanel = new JPanel();
    private LocTypeTableModel tableModel = new LocTypeTableModel();
    private JTable table = new JTable(tableModel);
    private JTextField textField;
    private List<LocomotiveType> deleted = new ArrayList<>();
    private static ResourceBundle bundle;

    public LocomotiveTypeEditDialog() {
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.locomotivetypeedit", LocaleManager.getLocale());
    }


    public LocomotiveTypeEditDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private void initContent() {
        setBounds(0, 0, 450, 300);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);

        JScrollPane scrollPane = new JScrollPane();
        contentPanel.add(scrollPane, BorderLayout.CENTER);

        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSize[] = { 100, 35 };
        TableUtils.setTableColumnSize(table, colSize);
        scrollPane.setViewportView(table);
        table.setToolTipText(bundle.getString("locomotivetypeedit.nameandcolorcommontoall"));
        table.setDefaultRenderer(Object.class, new LocTypeCellRenderer());

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent ev) {
                try {
                    if (ev.getClickCount() == 2) {
                        int row = table.getSelectedRow();
                        int col = table.getSelectedColumn();
                        LocomotiveType loc = tableModel.getList().get(row);
                        if (col == 1 && loc.isEditable()) {
                            ColorDialog cd = GDPEdit.gdpEdit.getDialogManager().getColorDialog();
                            cd.setVisible(true);
                            if (cd.getResult() > 0) {
                                loc.setColorLoc(ColorConverter.convert(cd.getColor()));
                                table.repaint();
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                }
            }
        });

        {
            JPanel actionPanel = new JPanel();
            actionPanel.setPreferredSize(new Dimension(120, 0));
            contentPanel.add(actionPanel, BorderLayout.EAST);
            GridBagLayout gbl_actionPanel = new GridBagLayout();
            gbl_actionPanel.columnWidths = new int[] { 0, 0 };
            gbl_actionPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
            gbl_actionPanel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
            gbl_actionPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
            actionPanel.setLayout(gbl_actionPanel);
            {
                JLabel lblNewLabel = new JLabel(bundle.getString("locomotivetypeedit.name"));
                GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
                gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
                gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
                gbc_lblNewLabel.gridx = 0;
                gbc_lblNewLabel.gridy = 0;
                actionPanel.add(lblNewLabel, gbc_lblNewLabel);
            }
            {
                textField = new JTextField();
                GridBagConstraints gbc_textField = new GridBagConstraints();
                gbc_textField.insets = new Insets(0, 0, 5, 0);
                gbc_textField.fill = GridBagConstraints.HORIZONTAL;
                gbc_textField.gridx = 0;
                gbc_textField.gridy = 1;
                actionPanel.add(textField, gbc_textField);
                textField.setColumns(10);
            }
            {
                JButton button = new JButton(bundle.getString("locomotivetypeedit.addarrow"));
                GridBagConstraints gbc_button = new GridBagConstraints();
                gbc_button.anchor = GridBagConstraints.WEST;
                gbc_button.insets = new Insets(0, 0, 5, 0);
                gbc_button.gridx = 0;
                gbc_button.gridy = 2;
                button.setToolTipText(bundle.getString("locomotivetypeedit.add"));
                actionPanel.add(button, gbc_button);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            if (textField.getText() != null && textField.getText().trim().length() > 0 && isNewType(textField.getText().trim())) {
                                LocomotiveType loc = new LocomotiveType();
                                loc.setColorLoc(0);
                                loc.setLtype(textField.getText().trim());
                                tableModel.getList().add(loc);
                                table.repaint();
                            }
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
            {
                JButton button = new JButton(bundle.getString("locomotivetypeedit.removearrow"));
                GridBagConstraints gbc_button = new GridBagConstraints();
                gbc_button.anchor = GridBagConstraints.WEST;
                gbc_button.gridx = 0;
                gbc_button.gridy = 3;
                button.setToolTipText(bundle.getString("locomotivetypeedit.remove"));
                actionPanel.add(button, gbc_button);
                // ��� ������ ������ ���� ���������� ��� ������ "������"
//                if (GDPDAOFactoryCreater.getGDPDAOFactory().isServer()) {
//                    button.setEnabled(false);
//                }
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            int row = table.getSelectedRow();
                            LocomotiveType loc = tableModel.getList().get(row);
                            if (loc.isEditable()) {
                                tableModel.getList().remove(loc);
                                table.repaint();
                                deleted.add(loc);
                            }
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }
                    }
                });
            }
        }

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton(bundle.getString("locomotivetypeedit.save"));
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        try {
                            // ��� ����� ���� ������ ���������, �.�. �� ��� ��� ID ����������� �� �����
                            GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO().saveLocomotiveTypes(tableModel.getList());
                            // ��� ���� ����� ���� ������ ���������, �.�. �� ��� � ID ����������� �� �����
                            GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO().addLocomotiveTypes(tableModel.getList());
                            GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO().deleteLocomotiveTypes(deleted);
                            // ����� ������ ��������� ������ �� ��, ����� ���� ��� ����� ID
                            List<LocomotiveType> list = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO().getListLocomotiveType();
                            tableModel.setList(list);
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                        }

                    }
                });
            }
            {
                JButton cancelButton = new JButton(bundle.getString("locomotivetypeedit.cancel"));
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LocomotiveTypeEditDialog.this.setVisible(false);
                    }
                });
            }
        }
    }

    /**
     * ��������� ������� ����� �������� � ����� �� ��������� � ������
     * @param locType �������� ��� ��������
     * @return true ���� � ����� ��������� � ������ ���
     */
    protected boolean isNewType(String locType) {
        for (LocomotiveType loc : tableModel.getList()) {
            if (loc.getLtype().equals(locType)) {
                return false;
            }
        }
        return true;
    }

    /** ���� ����� ������ ���������� ������ ��� ��� �������� ������� */
    // ������ �� ���� ������ ��� �� �������
    public void load() {
        List<LocomotiveType> list;
        try {
            list = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrDAO().getListLocomotiveType();
        } catch (DataManageException e) {
            list = new ArrayList<>();
            logger.error(e.toString(), e);
        }
        tableModel.setList(list);
        table.repaint();
        textField.setText("");
        deleted.clear();
    }
}
