package com.gmail.alaerof.components.menu;

import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dialog.spantimetown.SpanTimeTownDialog;
import com.gmail.alaerof.dialog.timeimport.ImportSPTimeFrame;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.javafx.dialog.energyfromtr.ImportEnergyFromTRFXDialog;
import com.gmail.alaerof.javafx.dialog.distrcomb.DistrCombFXDialog;
import com.gmail.alaerof.exportspantimes.ExportSpanTimes;
import com.gmail.alaerof.javafx.dialog.scalechange.ScaleChangeFXDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

public class MenuDistr extends LoggerMenu {
    public MenuDistr(){
        super();
        String title = bundle.getString("components.menu.Distr");
        this.setText(title);
        {
            title = bundle.getString("components.menu.distr.SpanTimeTown");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    showSpanTimeTownDialog();
                }
            });
        }

        {   // ������� ���� �� ������� ��������
            title = bundle.getString("components.menu.distr.ImportSPTime");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            if (!GDPDAOFactoryCreater.getGDPDAOFactory().isServer()) {
                menuItem.setEnabled(false);
            }
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    showImportSPTimeDialog();
                }
            });
        }
        {   // ������� ���������� ������ ����
            title = bundle.getString("components.menu.distr.ExportSpanTimes");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            if (!GDPDAOFactoryCreater.getGDPDAOFactory().isServer()) {
                menuItem.setEnabled(false);
            }
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    processExportSpanTimes();
                }
            });
        }
        {   // ������ �������������� ������ �� ������� ��������
            title = bundle.getString("components.menu.distr.ImportEnergySpending");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);

            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    importEnergy();
                }
            });
        }
        {   // ������������ �������
            title = bundle.getString("components.menu.distr.DistrComb");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);

            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    distrComb();
                }
            });
        }
    }

    private void distrComb() {
        try {
            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
            if (dee == null) {
                dee = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            if (dee != null) {
                DistrCombFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getDistrCombFXDialog(
                        dee.getDistrEntity().getDistrName());
                dialog.setModal(true);
                dialog.setDialogContent(dee);
                dialog.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void importEnergy() {
        try {
            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
            if (dee == null) {
                dee = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            if (dee != null) {
                ImportEnergyFromTRFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getImportEnergyFromTRFXDialog(
                        dee.getDistrEntity().getDistrName());
                dialog.setModal(true);
                dialog.setDialogContent(dee);
                dialog.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void processExportSpanTimes() {
        try {
            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
            if (dee == null) {
                dee = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            if (dee != null) {
                GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.waitCursor);
                ExportSpanTimes.processExport(dee.getDistrEntity());
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        } finally {
            GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.defCursor);
        }
    }

    private void showImportSPTimeDialog() {
        try {
            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
            if (dee == null) {
                dee = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            if (dee != null) {
                int idDistr = dee.getIdDistr();
                if (idDistr > 0) {
                    ImportSPTimeFrame dialog = GDPEdit.gdpEdit.getDialogManager().getImportSPTimeDialog(
                            dee.getDistrEntity().getDistrName());
                    dialog.setDistrID(idDistr);
                    dialog.setVisible(true);
                }
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void showSpanTimeTownDialog() {
        try {
            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
            if (dee == null) {
                dee = GDPEdit.gdpEdit.getDistrTree().getDistrEditEntity();
            }
            if (dee != null) {
                DistrEntity de = dee.getDistrEntity();
                SpanTimeTownDialog dialog =  GDPEdit.gdpEdit.getDialogManager().getSpanTimeTownDialog(
                        dee.getDistrEntity().getDistrName());
                dialog.setDistrEntity(de);
                dialog.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
