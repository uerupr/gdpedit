package com.gmail.alaerof.components.mainframe.gdp.action.listspan;

import java.util.LinkedList;

import java.util.List;
import java.util.ResourceBundle;
import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ListSpanTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("listspan.number"),
            bundle.getString("listspan.stage"),
            bundle.getString("listspan.nonbrw") };
    private List<DistrSpan> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.listspan", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null)
            return list.size();
        else
            return 0;
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            Scale sp = list.get(row).getSpanScale();
            switch (col) {
            case 0:
                return sp.getNum();
            case 1:
                return sp.getName();
            case 2:
                String ss = "";
                if(!sp.isBr()) ss = "*";
                return ss;
            default:
                break;
            }
        }
        return null;
    }

    public List<DistrSpan> getList() {
        return list;
    }

    public void setList(List<DistrSpan> list) {
        this.list = list;
    }

}
