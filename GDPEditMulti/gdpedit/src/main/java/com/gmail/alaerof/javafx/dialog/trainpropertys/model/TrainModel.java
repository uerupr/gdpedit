package com.gmail.alaerof.javafx.dialog.trainpropertys.model;

import com.gmail.alaerof.dao.dto.Category;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TrainModel {
    // IDtrain
    private long idTrain;
    // IDDistr
    private int idDistr;
    // IDcat
    private int idCat;
    // Code
    private StringProperty codeTrain;
    // Name
    private StringProperty nameTrain;
    // DistrName
    private StringProperty nameDistr;
    // Mass
    private DoubleProperty massTrain;
    // Length
    private DoubleProperty lengthTrain;
    // Priority
    private IntegerProperty priorityTrain;
    // OldPriority
    private int oldPriorityTrain;
    // TrainType
    private StringProperty typeTrain;
    // Category Name
    private StringProperty categoryName;
    // Category Priority
    private IntegerProperty categoryPriority;


    public TrainModel(DistrTrain distrTrain, String nameDistr, Category category) {
        this.idTrain = distrTrain.getIdTrain();
        this.idCat = distrTrain.getIdCat();
        this.codeTrain = new SimpleStringProperty(distrTrain.getCodeTR());
        this.nameTrain = new SimpleStringProperty(distrTrain.getNameTR());
        this.nameDistr = new SimpleStringProperty(nameDistr);
        this.idDistr = distrTrain.getIdDistr();
        this.oldPriorityTrain = distrTrain.getPriority();
        this.priorityTrain = new SimpleIntegerProperty(distrTrain.getPriority());
        this.typeTrain = new SimpleStringProperty(distrTrain.getTypeTR().getString());
        this.categoryName = new SimpleStringProperty(category.getName());
        this.categoryPriority = new SimpleIntegerProperty(category.getPriority());
    }

    public long getIDTrain() {return idTrain;}

    public int getIDDistr() {return idDistr;}

    public int getIDCat() {return idCat;}

    public StringProperty codeTrainProperty() {
        return codeTrain;
    }

    public StringProperty nameTrainProperty() {
        return nameTrain;
    }

    public StringProperty nameDistrProperty() {
        return nameDistr;
    }

    public IntegerProperty priorityTrainProperty() {return priorityTrain;}

    public int getOldPriorityTrain() {return oldPriorityTrain;}

    public StringProperty typeTrainProperty() {return typeTrain;}

    public StringProperty categoryNameProperty() {return categoryName;}

    public IntegerProperty categoryPriorityProperty() {return categoryPriority;}

}
