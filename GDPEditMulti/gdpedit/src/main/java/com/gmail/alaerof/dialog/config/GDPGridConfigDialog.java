package com.gmail.alaerof.dialog.config;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.dialog.ColorDialog;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JCheckBox;
import javax.swing.border.TitledBorder;

import com.gmail.alaerof.manager.LocaleManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class GDPGridConfigDialog extends JDialog {
    private static Logger logger = LogManager.getLogger(GDPGridConfigDialog.class);

    private static final long serialVersionUID = 1L;
    private final JPanel centerPanel = new JPanel();
    private final JPanel rightPanel = new JPanel();
    private int result;
    /** ������ ���� ����������� */
    private List<LocomotiveType> listLoc;
    /** ������ �������� �����������, ��������� ��� ����������� */
    private List<String> listSelectedLoc;
    /** ������ ���� ������������ ������� ������� */
    private List<DistrStation> listSt;
    /** ������ �������� �������, ��������� ��� ���������� ����� */
    private List<String> listSelectedSt;

    /**
     * Create the dialog.
     */
    public GDPGridConfigDialog() {
        initContent();
    }

    private JSpinner spinnerPicTimeVert;
    private JSpinner spinnerPicTimeHor;
    private JSpinner spinnerCodeStepO;
    private JSpinner spinnerCodeStepE;
    private JButton buttonGDPGridColor;
    private JButton buttonGDPStHourColor;
    private JSpinner spinnerGDPHourFont;
    private JSpinner spinnerHeadFont;
    private JSpinner spinnerUpdownFont;
    private JSpinner spinnerMoveFont;
    private JSpinner spinnerStationFont;
    private JSpinner spinnerStationLineFont;
    private JSpinner spinnerTrainTimeFont;
    private JSpinner spinnerTrainCaptionSpFont;
    private JSpinner spinnerTrainCaptionStFont;
    private JCheckBox checkBoxEvenDown;
    private JCheckBox checkBoxZeroOnGrid;

    private List<JCheckBox> listButtonLoc = new ArrayList<>();
    private JPanel panelLoc;

    private List<JCheckBox> listButtonSt = new ArrayList<>();
    private JPanel panelSt;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.config", LocaleManager.getLocale());
    }


    private void initContent() {
        // setAlwaysOnTop(true);
        setBounds(100, 100, 1000, 550);
        getContentPane().setLayout(new BorderLayout());
        centerPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        //FlowLayout fl_contentPanel = new FlowLayout(FlowLayout.LEADING, 5, 5);
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));
        JScrollPane senterScroll = new JScrollPane(centerPanel);
        getContentPane().add(senterScroll, BorderLayout.CENTER);
        getContentPane().add(rightPanel, BorderLayout.EAST);
        rightPanel.setLayout(new BorderLayout());
        
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                result = 0;
            }
        });

        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEADING);
            panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true),
                    bundle.getString("config.pixelsperminute"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
            centerPanel.add(panel);
            // panel.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 5));

            JLabel lpicTimeVert = new JLabel(bundle.getString("config.vertically"));
            lpicTimeVert.setHorizontalAlignment(SwingConstants.TRAILING);
            panel.add(lpicTimeVert);

            spinnerPicTimeVert = new JSpinner();
            spinnerPicTimeVert.setModel(new SpinnerNumberModel(5, 1, 10, 1));
            panel.add(spinnerPicTimeVert);

            JLabel lpicTimeHor = new JLabel(bundle.getString("config.horizontally"));
            lpicTimeHor.setHorizontalAlignment(SwingConstants.TRAILING);
            panel.add(lpicTimeHor);

            spinnerPicTimeHor = new JSpinner();
            spinnerPicTimeHor.setModel(new SpinnerNumberModel(3, 1, 10, 1));
            panel.add(spinnerPicTimeHor);

        }

        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEADING);
            panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true),
                    bundle.getString("config.indentinpixels"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
            centerPanel.add(panel);
            {
                JLabel label = new JLabel(bundle.getString("config.numberstrainpartofthestation"));
                int size = label.getFont().getSize();
                label.setFont(new Font("Sans Seif", Font.BOLD, size));
                panel.add(label);
            }
            {
                JLabel labelOdd = new JLabel(bundle.getString("config.odd"));
                panel.add(labelOdd);
            }
            {
                spinnerCodeStepO = new JSpinner();
                spinnerCodeStepO.setModel(new SpinnerNumberModel(14, 5, 50, 1));
                panel.add(spinnerCodeStepO);
            }
            {
                JLabel labelEven = new JLabel(bundle.getString("config.even"));
                panel.add(labelEven);
            }
            {
                spinnerCodeStepE = new JSpinner();
                spinnerCodeStepE.setModel(new SpinnerNumberModel(24, 5, 50, 1));
                panel.add(spinnerCodeStepE);
            }
        }

        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEADING);
            panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true),
                    bundle.getString("config.gdpgrid"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
            centerPanel.add(panel);
            {
                JLabel label = new JLabel(bundle.getString("config.gridcolor"));
                panel.add(label);
            }
            buttonGDPGridColor = new JButton("");
            buttonGDPGridColor.setBackground(new Color(75, 75, 0));
            buttonGDPGridColor.setPreferredSize(new Dimension(40, 30));
            panel.add(buttonGDPGridColor);
            buttonGDPGridColor.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    /*ColorDialog cd = GDPEdit.gdpEdit.getDialogManager().getColorDialog();
                    cd.setVisible(true);

                    if (cd.getResult() > 0) {
                        buttonGDPGridColor.setBackground(cd.getColor());
                    }*/
                    JColorChooser cc = new JColorChooser(buttonGDPGridColor.getBackground());
                    Color newColor = JColorChooser.showDialog(cc, bundle.getString("config.choiceofcolor"),
                            buttonGDPGridColor.getBackground());
                    if (newColor != null) {
                        buttonGDPGridColor.setBackground(newColor);
                    }
                }
            });
            {
                JLabel label = new JLabel(bundle.getString("config.sthourcolor"));
                panel.add(label);
            }
            buttonGDPStHourColor = new JButton("");
            buttonGDPStHourColor.setBackground(new Color(0, 0, 0));
            buttonGDPStHourColor.setPreferredSize(new Dimension(40, 30));
            panel.add(buttonGDPStHourColor);
            buttonGDPStHourColor.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    JColorChooser cc = new JColorChooser(buttonGDPStHourColor.getBackground());
                    Color newColor = JColorChooser.showDialog(cc, bundle.getString("config.choiceofcolor"),
                            buttonGDPStHourColor.getBackground());
                    if (newColor != null) {
                        buttonGDPStHourColor.setBackground(newColor);
                    }
                }
            });
            {
                JLabel label = new JLabel(bundle.getString("config.fontsizehours"));
                panel.add(label);
            }
            {
                spinnerGDPHourFont = new JSpinner();
                spinnerGDPHourFont.setModel(new SpinnerNumberModel(18, 8, 32, 1));
                panel.add(spinnerGDPHourFont);
            }
        }

        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEADING);
            panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true),
                    bundle.getString("config.fontsizesleftside"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
            centerPanel.add(panel);
            {
                JLabel label = new JLabel(bundle.getString("config.hatsignaling"));
                panel.add(label);
            }
            {
                spinnerHeadFont = new JSpinner();
                spinnerHeadFont.setModel(new SpinnerNumberModel(12, 8, 14, 1));
                panel.add(spinnerHeadFont);
            }
            {
                JLabel label = new JLabel(bundle.getString("config.accelerationdeceleration"));
                panel.add(label);
            }
            {
                spinnerUpdownFont = new JSpinner();
                spinnerUpdownFont.setModel(new SpinnerNumberModel(10, 8, 16, 1));
                panel.add(spinnerUpdownFont);
            }
            {
                JLabel label = new JLabel(bundle.getString("config.runningtime"));
                panel.add(label);
            }
            {
                spinnerMoveFont = new JSpinner();
                spinnerMoveFont.setModel(new SpinnerNumberModel(12, 8, 18, 1));
                panel.add(spinnerMoveFont);
            }
            {
                JLabel label = new JLabel(bundle.getString("config.namerp"));
                panel.add(label);
            }
            {
                spinnerStationFont = new JSpinner();
                spinnerStationFont.setModel(new SpinnerNumberModel(14, 8, 18, 1));
                panel.add(spinnerStationFont);
            }
            {
                JLabel label = new JLabel(bundle.getString("config.namepath"));
                panel.add(label);
            }
            {
                spinnerStationLineFont = new JSpinner();
                spinnerStationLineFont.setModel(new SpinnerNumberModel(10, 8, 16, 1));
                panel.add(spinnerStationLineFont);
            }
        }

        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEADING);
            panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true),
                    bundle.getString("config.trainthreadfontsizes"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
            centerPanel.add(panel);
            {
                JLabel label = new JLabel(bundle.getString("config.arrivaldeparture"));
                panel.add(label);
            }
            {
                spinnerTrainTimeFont = new JSpinner();
                spinnerTrainTimeFont.setModel(new SpinnerNumberModel(12, 8, 24, 1));
                panel.add(spinnerTrainTimeFont);
            }
            {
                JLabel label = new JLabel(bundle.getString("config.inscriptiontrainstage"));
                panel.add(label);
            }
            {
                spinnerTrainCaptionSpFont = new JSpinner();
                spinnerTrainCaptionSpFont.setModel(new SpinnerNumberModel(14, 8, 32, 1));
                panel.add(spinnerTrainCaptionSpFont);
            }
            {
                JLabel label = new JLabel(bundle.getString("config.inscriptiontrainstation"));
                panel.add(label);
            }
            {
                spinnerTrainCaptionStFont = new JSpinner();
                spinnerTrainCaptionStFont.setModel(new SpinnerNumberModel(10, 8, 32, 1));
                panel.add(spinnerTrainCaptionStFont);
            }
        }

        {
            JPanel panel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel.getLayout();
            flowLayout.setAlignment(FlowLayout.LEADING);
            panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true), null,
                    TitledBorder.LEADING, TitledBorder.TOP, null, null));
            centerPanel.add(panel);
            {
                checkBoxEvenDown = new JCheckBox(bundle.getString("config.evenfromtoptobottom"));
                panel.add(checkBoxEvenDown);
            }
            {
                checkBoxZeroOnGrid = new JCheckBox(bundle.getString("config.zeroongrid"));
                panel.add(checkBoxZeroOnGrid);
            }
        }

        {
            panelLoc = new JPanel();
            panelLoc.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true),
                    null, TitledBorder.LEFT, TitledBorder.TOP, null, null));
            panelLoc.setLayout(new BoxLayout(panelLoc, BoxLayout.PAGE_AXIS));
            panelLoc.setMinimumSize(new Dimension(200, 0));
            centerPanel.add(panelLoc);
            {
                JLabel label = new JLabel(bundle.getString("config.displaytraveltime"));
                label.setFont(new Font("Tahoma", Font.BOLD, 11));
                panelLoc.add(label);
            }
            {
                JLabel label = new JLabel(bundle.getString("config.movementtype"));
                panelLoc.add(label);
            }
        }

        {
            panelSt = new JPanel();
            panelSt.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true),
                    null, TitledBorder.LEFT, TitledBorder.TOP, null, null));
            panelSt.setLayout(new BoxLayout(panelSt, BoxLayout.PAGE_AXIS));
            panelSt.setMinimumSize(new Dimension(200, 0));
            JScrollPane scroll = new JScrollPane(panelSt);
            rightPanel.add(scroll, BorderLayout.CENTER);
            {
                JLabel label = new JLabel(bundle.getString("config.displayinggridstation"));
                label.setFont(new Font("Tahoma", Font.BOLD, 11));
                panelSt.add(label);
            }
            {
                JLabel label = new JLabel(bundle.getString("config.station"));
                panelSt.add(label);
            }
        }

        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog dialog = this;
            {
                JButton okButton = new JButton(bundle.getString("config.apply"));
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        result = 1;
                        fillListSelection(listButtonLoc, listSelectedLoc);
                        fillListSelection(listButtonSt, listSelectedSt);
                        dialog.setVisible(false);
                    }
                });
            }
            {
                JButton cancelButton = new JButton(bundle.getString("config.cancel"));
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        result = 0;
                        dialog.setVisible(false);
                    }
                });
            }
        }
    }

    public GDPGridConfigDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    public void fillFrameByConfig(GDPGridConfig conf) {
        spinnerPicTimeVert.setValue(conf.picTimeVert);
        spinnerPicTimeHor.setValue(conf.picTimeHor);
        spinnerCodeStepO.setValue(conf.codeStepO);
        spinnerCodeStepE.setValue(conf.codeStepE);
        buttonGDPGridColor.setBackground(conf.gdpGridColor);
        buttonGDPStHourColor.setBackground(conf.gdpStHourColor);
        spinnerGDPHourFont.setValue(conf.gdpHourFont.getSize());
        spinnerHeadFont.setValue(conf.headFont.getSize());
        spinnerUpdownFont.setValue(conf.updownFont.getSize());
        spinnerMoveFont.setValue(conf.moveFont.getSize());
        spinnerStationFont.setValue(conf.stationFont.getSize());
        spinnerStationLineFont.setValue(conf.stationLineFont.getSize());
        spinnerTrainTimeFont.setValue(conf.trainTimeFont.getSize());
        spinnerTrainCaptionSpFont.setValue(conf.trainCaptionSpFont.getSize());
        spinnerTrainCaptionStFont.setValue(conf.trainCaptionStFont.getSize());
        checkBoxEvenDown.setSelected(conf.evenDown);
        checkBoxZeroOnGrid.setSelected(conf.zeroOnGrid);
        listSelectedLoc = conf.listSelectedLoc;
        listSelectedSt = conf.listSelectedSt;
    }

    public void fillConfigByFrame(GDPGridConfig conf) {
        try {
            conf.picTimeVert = (Integer) spinnerPicTimeVert.getValue();
            conf.picTimeHor = (Integer) spinnerPicTimeHor.getValue();
            conf.codeStepO = (Integer) spinnerCodeStepO.getValue();
            conf.codeStepE = (Integer) spinnerCodeStepE.getValue();
            conf.gdpGridColor = buttonGDPGridColor.getBackground();
            conf.gdpStHourColor = buttonGDPStHourColor.getBackground();
            conf.gdpHourFont = new Font(Font.SANS_SERIF, Font.BOLD, (int) spinnerGDPHourFont.getValue());
            conf.headFont = new Font(Font.SANS_SERIF, 0, (int) spinnerHeadFont.getValue());
            conf.updownFont = new Font(Font.SANS_SERIF, 0, (int) spinnerUpdownFont.getValue());
            conf.moveFont = new Font(Font.SANS_SERIF, 0, (int) spinnerMoveFont.getValue());
            conf.stationFont = new Font(Font.SANS_SERIF, 0, (int) spinnerStationFont.getValue());
            conf.stationLineFont = new Font(Font.SANS_SERIF, 0, (int) spinnerStationLineFont.getValue());
            conf.trainTimeFont = new Font(Font.SANS_SERIF, 0, (int) spinnerTrainTimeFont.getValue());
            conf.trainCaptionSpFont = new Font("Times New Roman", Font.BOLD,
                    (int) spinnerTrainCaptionSpFont.getValue());
            conf.trainCaptionStFont = new Font("Times New Roman", 0,
                    (int) spinnerTrainCaptionStFont.getValue());
            conf.evenDown = checkBoxEvenDown.isSelected();
            conf.zeroOnGrid = checkBoxZeroOnGrid.isSelected();
            conf.listSelectedLoc = listSelectedLoc;
            conf.listSelectedSt = listSelectedSt;
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    public int getResult() {
        return result;
    }

    public List<LocomotiveType> getListLoc() {
        return listLoc;
    }

    public void setListLoc(List<LocomotiveType> listLoc) {
        this.listLoc = listLoc;
        // ������� ������ �����������
        clearButtonGroup(listButtonLoc, panelLoc);
        // ��������� ������ �����������
        fillButtonGroupLoc();
        // �������� ������������
        fillButtonGroupSelection(listButtonLoc, listSelectedLoc);
    }

    private void clearButtonGroup(List<JCheckBox> list, JPanel panel) {
        for (int i = 0; i < list.size(); i++) {
            JCheckBox but = list.get(i);
            panel.remove(but);
        }
        list.clear();
    }

    private void fillButtonGroupLoc() {
        for (int i = 0; i < listLoc.size(); i++) {
            LocomotiveType loc = listLoc.get(i);
            JCheckBox cbLoc = new JCheckBox(loc.getLtype().trim());
            panelLoc.add(cbLoc);
            listButtonLoc.add(cbLoc);
        }
    }

    private void fillButtonGroupSelection(List<JCheckBox> listButton, List<String> listSelected) {
        for (int i = 0; i < listButton.size(); i++) {
            JCheckBox but = listButton.get(i);
            String name = but.getText().trim();
            boolean selected = listSelected.contains(name);
            but.setSelected(selected);
        }
    }

    private void fillListSelection(List<JCheckBox> listButton, List<String> listSelected) {
        listSelected.clear();
        for (int i = 0; i < listButton.size(); i++) {
            JCheckBox but = listButton.get(i);
            String name = but.getText().trim();
            if (but.isSelected()) {
                listSelected.add(name);
            }
        }
    }

    public List<DistrStation> getListSt() {
        return listSt;
    }

    public void setListSt(List<DistrStation> listSt) {
        this.listSt = listSt;
        // ������� ������ �����������
        clearButtonGroup(listButtonSt, panelSt);
        // ��������� ������ �����������
        fillButtonGroupSt();
        // �������� ������������
        fillButtonGroupSelection(listButtonSt, listSelectedSt);
    }
    
    private void fillButtonGroupSt() {
        for (int i = 0; i < listSt.size(); i++) {
            DistrStation st = listSt.get(i);
            JCheckBox cbSt = new JCheckBox(st.getNamePoint().trim());
            panelSt.add(cbSt);
            listButtonSt.add(cbSt);
        }
    }
}
