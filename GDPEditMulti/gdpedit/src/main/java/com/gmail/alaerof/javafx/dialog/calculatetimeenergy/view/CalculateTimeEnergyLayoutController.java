package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.view;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.dao.dto.Dir;
import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.energy.EnergyType;
import com.gmail.alaerof.dao.dto.energy.ExpenseRate;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateEnergy;
import com.gmail.alaerof.dao.dto.energy.ExpenseRateNormative;
import com.gmail.alaerof.dao.dto.gdp.DirTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainExpense;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.dao.dto.groupp.GroupColor;
import com.gmail.alaerof.dao.dto.groupp.GroupP;
import com.gmail.alaerof.dao.dto.groupp.GroupRange;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDirDAO;
import com.gmail.alaerof.dao.interfacedao.IDistrTrainDAO;
import com.gmail.alaerof.dao.interfacedao.IGroupPDAO;
import com.gmail.alaerof.dao.interfacedao.ISpanEnergyDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dialog.ColorDialog;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.CalculateTimeEnergyHandler;
import com.gmail.alaerof.entity.train.TrainComparatorByCode;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.ExportExcelHandler;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.TaskWarning;
import com.gmail.alaerof.javafx.dialog.TaskWithCursorExecutor;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.CalculateTimeEnergyXML;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.CalcModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.DirModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.DirTrainModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.CalcGroup;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.ExpenseRateModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.ExpenseRateEn;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.ExpenseRateTransfer;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.GroupColorModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.GroupPModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.GroupRangeModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.LocTypeModel;
import com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model.TrainThreadModel;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.ColorConverter;
import com.gmail.alaerof.util.FileUtil;
import com.gmail.alaerof.util.IconImageUtil;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.swing.filechooser.FileNameExtensionFilter;

import javafx.util.Callback;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CalculateTimeEnergyLayoutController implements Initializable {
    private static ResourceBundle bundle_static = ResourceBundle.getBundle("bundles.CalculateTimeEnergyFXDialog", LocaleManager.getLocale());
    public static final String KOFF_EFFECT_GDP = bundle_static.getString("calc.energyefficiencyratio");
    /** logger */
    protected Logger logger;
    private boolean statusEffectGDP;

    {
        logger = LogManager.getLogger(this.getClass());
    }
    private double expectedValueEffect;
    private double standardDeviation;
    private Stage stage;
    /** Internationalization */
    protected ResourceBundle bundle;
    /** ������ ��� ������� � ����������� */
    private DistrEditEntity distrEditEntity;
    /** ��������� ������ */
    private ExpenseRateModel expenseRate;
    /** ������� ������� */
    private TreeTableView<TrainThreadModel> trainThreadTable;
    /** ������� ������� ��� */
    private TreeTableView<TrainThreadModel> trainThreadTableGDP;
    /***/
    private Scene scene;
    // � ������� ����� ���� ������ ��. ���
    private String errCalculateTimeEnergy;
    /** ������� ����� ��� ����������� */
    private TableView<GroupPModel> groupPTable;
    /** ������� ���������� ��� ������  */
    private TableView<GroupRangeModel> groupRangeTable;
    /** ������� ������ ��� ������ */
    private TableView<GroupColorModel> groupColorTable;
    /** ������� ����������� ������� */
    private TreeTableView<CalcModel> distrCalcTreeTable;
    /** ������� ��������� ������ */
    private List<ExpenseRateModel> expenseRateModelList;
    /** ������� ����������� ��������*/
    private TableView<ExpenseRateModel> expenseRateModelTable;
    /** ����������� ������ ����������� (����� ��������) */
    private List<LocTypeModel> locTypeModelList;
    /** ������� ����������� �������� ����������� */
    private TableView<LocTypeModel> locTypeModelTable;
    /** ��������� �������/�� */
    private Map<EnergyType, ExpenseRateEn> expenseRateEnMap;
    /** ��������� ������ */
    private GroupPModel selectedGroupP;
    /** ������ ��������� ����� */
    private List<GroupP> deletedGroupP = new ArrayList<>();
    /** ������ ��������� ���������� */
    private List<GroupRange> deletedRange = new ArrayList<>();
    /** ������ ��������� ������ */
    private List<GroupColor> deletedColor = new ArrayList<>();

    /** ������� ����������� ������� �� ��� */
    private TableView<DirTrainModel> dirTrainTable;
    /** ������ ��� */
    private List<Dir> dirList = new ArrayList<>();
    /** ���������� ����� �� ��� */
    private Map<Dir, List<CalcGroup>>  dirCalcMap = new HashMap<>();
    /** ������� ��� */
    private TableView<DirModel> dirTable;
    /** ������� ����������� ��� */
    private TreeTableView<CalcModel> dirCalcTreeTable;

    /** ������ ��������� ������ */
    @FXML
    private BorderPane expenseRatePane;
    /** ������ ���������� �������� ����������� */
    @FXML
    private BorderPane expenseRateLocPane;
    /** ������ ������ ������� */
    @FXML
    private BorderPane trainPane;
    /** ������ ������ ������� ��� */
    @FXML
    private BorderPane trainPaneGDP;
    /** ������ ����� ����������� */
    @FXML
    private BorderPane groupPane;
    /** ������ GroupRange */
    @FXML
    private BorderPane rangePane;
    /** ������ GroupColor */
    @FXML
    private BorderPane colorPane;
    /** ������ ����������� ������� */
    @FXML
    private BorderPane distrCalcPane;
    /** ������ ����������� ������� �� ��� */
    @FXML
    private BorderPane dirTrainPane;
    /** ������ ����������� ����� �� ��� */
    @FXML
    private BorderPane dirGroupPane;

    /** ����� ���� �������� �� ������ ��� ������� �������������� ������ */
    @FXML
    private ComboBox<LocTypeModel> locTypeComboBox;
    @FXML
    private Button addGroupButton;
    @FXML
    private Button delGroupButton;
    @FXML
    private Button openDBGroupButton;
    @FXML
    private Button saveDBGroupButton;
    @FXML
    private Button addGroupToCalcButton;
    @FXML
    private Button delGroupFromCalcButton;
    @FXML
    private Button addRangeButton;
    @FXML
    private Button delRangeButton;
    @FXML
    private Button addColorButton;
    @FXML
    private Button delColorButton;
    @FXML
    private Button calcButton;
    @FXML
    private Button calcSaveFileButton;
    @FXML
    private Button calcEnergySaveFileButton;
    @FXML
    private Button calcSaveDBButton;
    @FXML
    private Button calcButtonGDP;
    @FXML
    private Button saveExpenseXMLButton;
    @FXML
    private Button loadExpenseXMLButton;
    @FXML
    private Button saveExpenseDBButton;
    @FXML
    private TextField expenseRateEnTeplo;
    @FXML
    private TextField expenseRateEnElectro;
    @FXML
    private CheckBox onlyRBCheckBox;
    @FXML
    private CheckBox noTechStCheckBox;
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab tabReting;

    /**
     * ������������� ���������� ��� �������� �� FXML ����������� (FXMLLoader)
     * @param location {@link URL}
     * @param resources {@ResourceBundle} �������������� ��������
     */
    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " + location + " resources = " + resources);
        bundle = resources;
        errCalculateTimeEnergy = bundle.getString("calculate.time.energy.err");
        initTrainTable();
        initTrainTableGDP();
        initGroupPTable();
        initGroupRangeTable();
        initGroupColorTable();
        initGropPButtons();
        initGropRangeButtons();
        initGropColorButtons();
        initCalcButtons();
        initExpenseRatesButtons();

        initExpenseRateTable();
        initLocTypeModelTable();
        initDistrCalcTreeTable();

        this.expenseRateModelList = buildExpenseRateModelList();
        this.expenseRateEnMap = buildExpenseRateEnMap();

        initExpenseRateEnPane();

        initDirTrainTable();
        initDirGroupPane();
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    private void initExpenseRateEnPane() {
        expenseRateEnTeplo.setTextFormatter(getDoubleFormatter());
        expenseRateEnElectro.setTextFormatter(getDoubleFormatter());
    }

    private TextFormatter getDoubleFormatter() {
        Pattern pattern = Pattern.compile("\\d*|\\d+\\.\\d*");
        return new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
            return pattern.matcher(change.getControlNewText()).matches() ? change : null;
        });
    }

    public static Map<EnergyType,ExpenseRateEn> buildExpenseRateEnMap() {
        Map<EnergyType,ExpenseRateEn> map = new HashMap<>();
        map.put(EnergyType.teplo, new ExpenseRateEn(EnergyType.teplo));
        map.put(EnergyType.electro, new ExpenseRateEn(EnergyType.electro));
        return map;
    }

    private void initLocTypeModelTable() {
        // todo ������ �������� �� ���������� ���� ����� ���-�� �������
        EditableTableViewCreator<LocTypeModel> builder = new EditableTableViewCreator<>();
        locTypeModelTable = builder.createEditableTableView();
        locTypeModelTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        expenseRateLocPane.setCenter(locTypeModelTable);
        //--- columns ---
        String title;
        title = bundle.getString("locTypeModelTable.name");
        TableColumn col = builder.createStringColumn(title, LocTypeModel::locNameProperty, false);
        col.setPrefWidth(260);
        locTypeModelTable.getColumns().add(col);
        title = bundle.getString("locTypeModelTable.downupLocExpRate");
        locTypeModelTable.getColumns().add(builder.createDoubleColumn(title, LocTypeModel::downupExpProperty, true));
        //---
    }

    private void initDirTrainTable() {
        EditableTableViewCreator<DirTrainModel> builder = new EditableTableViewCreator<>();
        dirTrainTable = builder.createEditableTableView();
        dirTrainTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        dirTrainPane.setCenter(dirTrainTable);

        String title;
        TableColumn col;
        //--- columns ---
        title = bundle.getString("dirTrainTable.nod");
        col = builder.createStringColumn(title, DirTrainModel::dirNameProperty, false);
        dirTrainTable.getColumns().add(col);

        title = bundle.getString("trainThreadTable.codeTR");// "CodeTR";
        col = builder.createStringColumn(title, DirTrainModel::codeTRProperty, false);
        dirTrainTable.getColumns().add(col);
        title = bundle.getString("trainThreadTable.nameTR");// "NameTR";
        col = builder.createStringColumn(title, DirTrainModel::nameTRProperty, false);
        dirTrainTable.getColumns().add(col);
        title = bundle.getString("trainThreadTable.len"); // "Len, m";
        col = builder.createDoubleColumn(title, DirTrainModel::lenProperty, false);
        dirTrainTable.getColumns().add(col);
        title = bundle.getString("trainThreadTable.timeWithoutStop"); // "Tt, m";
        col = builder.createDoubleColumn(title, DirTrainModel::ttProperty, false);
        dirTrainTable.getColumns().add(col);
        title = bundle.getString("trainThreadTable.timeWithStop"); //  "Tu, m";
        col = builder.createDoubleColumn(title, DirTrainModel::tuProperty, false);
        dirTrainTable.getColumns().add(col);
        title = bundle.getString("calc.speedWithoutStop"); //  "Vt";
        col = builder.createDoubleColumn(title, DirTrainModel::vtProperty, false);
        dirTrainTable.getColumns().add(col);
        title = bundle.getString("calc.speedWithStop"); //  "Vu";
        col = builder.createDoubleColumn(title, DirTrainModel::vuProperty, false);
        dirTrainTable.getColumns().add(col);
    }

    private void initDirGroupPane() {
        EditableTableViewCreator<DirModel> builder = new EditableTableViewCreator<>();
        dirTable = builder.createEditableTableView();
        dirTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        dirGroupPane.setLeft(dirTable);

        String title;
        TableColumn col;
        //--- columns ---
        title = bundle.getString("dirTrainTable.nod");
        col = builder.createStringColumn(title, DirModel::dirNameProperty, false);
        dirTable.getColumns().add(col);

        dirTable.getSelectionModel().selectedItemProperty().addListener(getDirModelChangeListener());

        Image image = IconImageUtil.getFXImage(IconImageUtil.OPEN);
        ImageView imageView = new ImageView(image);

        GroupP rootGroup = new GroupP();
        TreeItem<CalcModel> root = new TreeItem<>(
                new CalcModel(rootGroup, "X", CalcModel.LAYER_ROOT), imageView);
        dirCalcTreeTable = new TreeTableView<>(root);
        dirGroupPane.setCenter(dirCalcTreeTable);

        double prefWidth;
        TreeTableColumn<CalcModel, String> strColumn;
        TreeTableColumn<CalcModel, Integer> intColumn;
        TreeTableColumn<CalcModel, Double> dblColumn;
        //--- columns ---
        title = bundle.getString("calc.Group");
        prefWidth = 200;
        strColumn = new TreeTableColumn<>(title);
        strColumn.setPrefWidth(prefWidth);
        strColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, String>("groupPName"));
        dirCalcTreeTable.getColumns().add(strColumn);

//        title = bundle.getString("calc.locTypeName");
//        prefWidth = 180;
//        strColumn = new TreeTableColumn<>(title);
//        strColumn.setPrefWidth(prefWidth);
//        strColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, String>("locTypeName"));
//        dirCalcTreeTable.getColumns().add(strColumn);

        title = bundle.getString("calc.trainCount");
        intColumn = new TreeTableColumn<>(title);
        intColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Integer>("trainCount"));
        dirCalcTreeTable.getColumns().add(intColumn);

        title = bundle.getString("calc.lenKM");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("lenKM"));
        dirCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.timeWithStopH");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("timeWithStopH"));
        dirCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.timeWithoutStopH");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("timeWithoutStopH"));
        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.timeNormMoveH");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("timeNormMoveH"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedWithStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("speedWithStop"));
        dirCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedWithoutStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("speedWithoutStop"));
        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.speedNorm");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("speedNorm"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.energyMove");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("energyMove"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.stopCount");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("stopCount"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.moveExpense");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("moveExpense"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.downUpExpense");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("downUpExpense"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.stopExpense");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("stopExpense"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.normExpense");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("normExpense"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

//        title = bundle.getString("calc.effect");
//        dblColumn = new TreeTableColumn<>(title);
//        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("effect"));
//        dirCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.vu_divide_vtech");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("vv"));
        dirCalcTreeTable.getColumns().add(dblColumn);

    }

    private ChangeListener<? super DirModel> getDirModelChangeListener() {
        return (obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                DirModel selected = newSelection;
                // TODO update dir group calc
                updateDirCalcTreeTable(selected.getDir());
                logger.debug("DIR selected: " + selected.getDir().getIdDir() + " " + selected.getDirName());
            } else {
            }
        };
    }

    private void initExpenseRateTable() {
        // todo ������� ���-����
        EditableTableViewCreator<ExpenseRateModel> builder = new EditableTableViewCreator<>();
        expenseRateModelTable = builder.createEditableTableView();
        expenseRateModelTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        expenseRatePane.setCenter(expenseRateModelTable);

        String title;
        TableColumn col;
        //--- columns ---
        title = "";
        col = builder.createStringColumn(title, ExpenseRateModel::trainTypeNameProperty, false);
        col.setPrefWidth(120);
        expenseRateModelTable.getColumns().add(col);
        title = "";
        col = builder.createStringColumn(title, ExpenseRateModel::energyTypeNameProperty, false);
        col.setPrefWidth(120);
        expenseRateModelTable.getColumns().add(col);

        title = bundle.getString("tab.moveExpRate");
        expenseRateModelTable.getColumns().add(builder.createDoubleColumn(title, ExpenseRateModel::moveExpRateProperty, true));
        title = bundle.getString("tab.stopExpRate");
        expenseRateModelTable.getColumns().add(builder.createDoubleColumn(title, ExpenseRateModel::stopExpRateProperty, true));
        //---

    }

    private void initDistrCalcTreeTable() {
        Image image = IconImageUtil.getFXImage(IconImageUtil.OPEN);
        ImageView imageView = new ImageView(image);

        GroupP rootGroup = new GroupP();
        TreeItem<CalcModel> root = new TreeItem<>(
                new CalcModel(rootGroup, "X", CalcModel.LAYER_ROOT), imageView);
        distrCalcTreeTable = new TreeTableView<>(root);
        distrCalcPane.setCenter(distrCalcTreeTable);

        String title;
        double prefWidth;
        TreeTableColumn<CalcModel, String> strColumn;
        TreeTableColumn<CalcModel, Integer> intColumn;
        TreeTableColumn<CalcModel, Double> dblColumn;
        //--- columns ---
        title = bundle.getString("calc.Group");
        prefWidth = 200;
        strColumn = new TreeTableColumn<>(title);
        strColumn.setPrefWidth(prefWidth);
        strColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, String>("groupPName"));
        distrCalcTreeTable.getColumns().add(strColumn);

        title = bundle.getString("calc.locTypeName");
        prefWidth = 180;
        strColumn = new TreeTableColumn<>(title);
        strColumn.setPrefWidth(prefWidth);
        strColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, String>("locTypeName"));
        distrCalcTreeTable.getColumns().add(strColumn);

        title = bundle.getString("calc.trainCount");
        intColumn = new TreeTableColumn<>(title);
        intColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Integer>("trainCount"));
        distrCalcTreeTable.getColumns().add(intColumn);

        title = bundle.getString("calc.lenKM");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("lenKM"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.timeWithStopH");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("timeWithStopH"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.timeWithoutStopH");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("timeWithoutStopH"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.timeNormMoveH");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("timeNormMoveH"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedWithStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("speedWithStop"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedWithoutStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("speedWithoutStop"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedNorm");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("speedNorm"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.energyMove");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("energyMove"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.stopCount");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("stopCount"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.moveExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("moveExpense"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.downUpExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("downUpExpense"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.stopExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("stopExpense"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.normExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("normExpense"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.effect");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("effect"));
        distrCalcTreeTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.vu_divide_vtech");;
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<CalcModel, Double>("vv"));
        distrCalcTreeTable.getColumns().add(dblColumn);
    }

    private void initGroupColorTable() {
        EditableTableViewCreator<GroupColorModel> builder = new EditableTableViewCreator<>();
        groupColorTable = builder.createEditableTableView();
        groupColorTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        colorPane.setCenter(groupColorTable);
        //--- columns ---
        String title;
        title = bundle.getString("groupColorTable.color");
        groupColorTable.getColumns().add(builder.createColorColumn(title, GroupColorModel::colorProperty));
        //---
    }

    private void initGroupRangeTable() {
        EditableTableViewCreator<GroupRangeModel> builder = new EditableTableViewCreator<>();
        groupRangeTable = builder.createEditableTableView();
        groupRangeTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        rangePane.setCenter(groupRangeTable);
        String title;
        //--- columns ---
        title = bundle.getString("groupRangeTable.min");
        groupRangeTable.getColumns().add(builder.createIntegerColumn(title, GroupRangeModel::minCodeProperty, true));
        title = bundle.getString("groupRangeTable.max");
        groupRangeTable.getColumns().add(builder.createIntegerColumn(title, GroupRangeModel::maxCodeProperty, true));
        //---
    }

    private void initGroupPTable() {
        EditableTableViewCreator<GroupPModel> builder = new EditableTableViewCreator<>();
        groupPTable = builder.createEditableTableView();
        groupPTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        groupPane.setCenter(groupPTable);
        String title;
        //--- columns ---
        title = bundle.getString("groupPTable.ord");
        groupPTable.getColumns().add(builder.createIntegerColumn(title, GroupPModel::ordProperty, true));
        title = bundle.getString("groupPTable.name");
        TableColumn<GroupPModel, String> column = builder.createStringColumn(title, GroupPModel::nameProperty, true);
        groupPTable.getColumns().add(column);
        column.setPrefWidth(260.0);
        title = bundle.getString("groupPTable.bycolor");
        column = builder.createStringColumn(title, GroupPModel::byColorProperty, false);
        groupPTable.getColumns().add(column);
        //---
        groupPTable.getSelectionModel().selectedItemProperty().addListener(getGroupPModelChangeListener());

        groupPTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    @SuppressWarnings("rawtypes")
                    TablePosition pos = groupPTable.getSelectionModel().getSelectedCells().get(0);
                    int row = pos.getRow();
                    int col = pos.getColumn();
                    if (row >= 0 && col == 2) {
                        GroupP groupP = groupPTable.getItems().get(row).getGroupP();
                        boolean byColor = !groupP.isByColor();
                        String byColorStr = byColor ? "*" : "";
                        groupPTable.getItems().get(row).setByColor(byColorStr);
                        disableGroupPButtons(groupP);
                    }
                }
            }
        });
    }

    /**
     * @return ChangeListener
     */
    private ChangeListener<GroupPModel> getGroupPModelChangeListener() {
        return (obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                updateCurrentValues(selectedGroupP);
                selectedGroupP = newSelection;
                selectGroupPModel(selectedGroupP);
                disableGroupPButtons(selectedGroupP.getGroupP());
            } else {
                selectedGroupP = null;
            }
        };
    }

    /**
     * ���������� �������� ���������� �������� GroupPModel � GroupRangeModel ����� �� �������� ��
     * @param selectedGroupP
     */
    private void updateCurrentValues(GroupPModel selectedGroupP) {
        if (selectedGroupP != null) {
            selectedGroupP.getGroupP().setName(selectedGroupP.getName());
            selectedGroupP.getGroupP().setOrd(selectedGroupP.getOrd());

            for(GroupRangeModel rangeModel: groupRangeTable.getItems()){
                rangeModel.getGroupRange().setMinCode(rangeModel.getMinCode());
                rangeModel.getGroupRange().setMaxCode(rangeModel.getMaxCode());
            }
        }
    }

    private void disableGroupPButtons(GroupP groupP) {
        if (groupP.isByColor()) {
            addRangeButton.setDisable(true);
            delRangeButton.setDisable(true);
            addColorButton.setDisable(false);
            delColorButton.setDisable(false);
        } else {
            addRangeButton.setDisable(false);
            delRangeButton.setDisable(false);
            addColorButton.setDisable(true);
            delColorButton.setDisable(true);
        }
    }

    private void initGropColorButtons() {
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.PLUS);
            ImageView imageView = new ImageView(image);
            addColorButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.MINUS);
            ImageView imageView = new ImageView(image);
            delColorButton.setGraphic(imageView);
        }
    }

    private void initGropRangeButtons() {
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.PLUS);
            ImageView imageView = new ImageView(image);
            addRangeButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.MINUS);
            ImageView imageView = new ImageView(image);
            delRangeButton.setGraphic(imageView);
        }
    }

    private void initCalcButtons() {
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.CALC);
            ImageView imageView = new ImageView(image);
            calcButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.SAVE_DB);
            ImageView imageView = new ImageView(image);
            calcSaveDBButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.SAVE_FILE);
            ImageView imageView = new ImageView(image);
            calcSaveFileButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.SAVE_FILE);
            ImageView imageView = new ImageView(image);
            calcEnergySaveFileButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.CALCGDP);
            ImageView imageView = new ImageView(image);
            calcButtonGDP.setGraphic(imageView);
        }
    }

    private void initExpenseRatesButtons() {
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.SAVE_DB);
            ImageView imageView = new ImageView(image);
            saveExpenseDBButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.SAVE_FILE);
            ImageView imageView = new ImageView(image);
            saveExpenseXMLButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.LOAD_FILE);
            ImageView imageView = new ImageView(image);
            loadExpenseXMLButton.setGraphic(imageView);
        }
    }

    private void initGropPButtons() {
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.PLUS);
            ImageView imageView = new ImageView(image);
            addGroupButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.MINUS);
            ImageView imageView = new ImageView(image);
            delGroupButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.ARR_DOWN);
            ImageView imageView = new ImageView(image);
            addGroupToCalcButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.ARR_UP);
            ImageView imageView = new ImageView(image);
            delGroupFromCalcButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.OPEN);
            ImageView imageView = new ImageView(image);
            openDBGroupButton.setGraphic(imageView);
        }
        {
            Image image = IconImageUtil.getFXImage(IconImageUtil.SAVE_DB);
            ImageView imageView = new ImageView(image);
            saveDBGroupButton.setGraphic(imageView);
        }
    }

    private void initTrainTable() {
        Image image = IconImageUtil.getFXImage(IconImageUtil.OPEN);
        ImageView imageView = new ImageView(image);

        TreeItem<TrainThreadModel> root = new TreeItem<>(
                new TrainThreadModel(), imageView);
        trainThreadTable = new TreeTableView<>(root);
        trainPane.setCenter(trainThreadTable);

        String title;
        double prefWidth;
        TreeTableColumn<TrainThreadModel, String> strColumn;
        TreeTableColumn<TrainThreadModel, Integer> intColumn;
        TreeTableColumn<TrainThreadModel, Double> dblColumn;
        //--- columns ---
        title = bundle.getString("trainThreadTable.codeTR");
        prefWidth = 100;
        intColumn = new TreeTableColumn<>(title);
        intColumn.setPrefWidth(prefWidth);
        intColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Integer>("codeTR"));
        trainThreadTable.getColumns().add(intColumn);

        title = bundle.getString("trainThreadTable.nameTR");
        prefWidth = 80;
        strColumn = new TreeTableColumn<>(title);
        strColumn.setPrefWidth(prefWidth);
        strColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, String>("nameTR"));
        trainThreadTable.getColumns().add(strColumn);

        title = bundle.getString("trainThreadTable.len");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("len"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("trainThreadTable.timeWithoutStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("timeWithoutStop"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("trainThreadTable.timeWithStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("timeWithStop"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedWithoutStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("speedWithoutStop"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedWithStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("speedWithStop"));
        trainThreadTable.getColumns().add(dblColumn);


        title = bundle.getString("calc.locTypeName");
        prefWidth = 180;
        strColumn = new TreeTableColumn<>(title);
        strColumn.setPrefWidth(prefWidth);
        strColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, String>("expenseName"));
        trainThreadTable.getColumns().add(strColumn);

        title = bundle.getString("trainThreadTable.timeNormMove");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("timeNormMove"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedNorm");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("speedNormMove"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.energyMove");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("energyWithoutStop"));
        trainThreadTable.getColumns().add(dblColumn);
        //  title = bundle.getString("javafx.dialog.calculatetimeenergy.table.energywithstop");

        title = bundle.getString("calc.stopCount");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("stopCount"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.moveExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("moveExpense"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.downUpExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("downUpExpense"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.stopExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("stopExpense"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.normExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("normExpense"));
        trainThreadTable.getColumns().add(dblColumn);

        title = bundle.getString("calc.effect");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("effect"));
        trainThreadTable.getColumns().add(dblColumn);

    }

    private void initTrainTableGDP() {
        Image image = IconImageUtil.getFXImage(IconImageUtil.OPEN);
        ImageView imageView = new ImageView(image);

        TreeItem<TrainThreadModel> root = new TreeItem<>(
                new TrainThreadModel(), imageView);
        trainThreadTableGDP = new TreeTableView<>(root);
        trainPaneGDP.setCenter(trainThreadTableGDP);

        String title;
        double prefWidth;
        TreeTableColumn<TrainThreadModel, String> strColumn;
        TreeTableColumn<TrainThreadModel, Integer> intColumn;
        TreeTableColumn<TrainThreadModel, Double> dblColumn;
        //--- columns ---
        title = bundle.getString("trainThreadTable.codeTR");
        prefWidth = 100;
        intColumn = new TreeTableColumn<>(title);
        Callback<TreeTableColumn<TrainThreadModel, Integer>, TreeTableCell<TrainThreadModel, Integer>> cellCallbackInteger = (TreeTableColumn<TrainThreadModel, Integer> param) -> new TreeTableCell<TrainThreadModel, Integer>() {
            @Override
            protected final void updateItem(Integer value, boolean empty) {

                super.updateItem(value, empty);
                if (empty) {
                    setGraphic(null);
                    setText(null);
                    setStyle("-fx-background-color: WHITE");
                } else {
                    TrainThreadModel element = getTreeTableRow().getItem();
                    if (element != null) {
                        double effect = (expectedValueEffect - element.getEffect());
                        if ((effect <= (1 * standardDeviation))) {
                            setStyle("-fx-background-color: #3bd867");
                        }
                        if ((effect > (1 * standardDeviation))&&(effect <= (2 * standardDeviation))) {
                            setStyle("-fx-background-color: #fffb64");
                        }
                        if (effect > (2 * standardDeviation)) {
                            setStyle("-fx-background-color: #ff504a");
                        }
                    }
                    setText(value != null ? value.toString() : "");
                }
            }
        };
        intColumn.setCellFactory(cellCallbackInteger);
        intColumn.setPrefWidth(prefWidth);
        intColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Integer>("codeTR"));
        trainThreadTableGDP.getColumns().add(intColumn);

        title = bundle.getString("trainThreadTable.nameTR");
        prefWidth = 80;
        strColumn = new TreeTableColumn<>(title);
        Callback<TreeTableColumn<TrainThreadModel, String>, TreeTableCell<TrainThreadModel, String>> cellCallbackString = (TreeTableColumn<TrainThreadModel, String> param) -> new TreeTableCell<TrainThreadModel, String>() {
            @Override
            protected final void updateItem(String value, boolean empty) {
                super.updateItem(value, empty);
                if (empty) {
                    setGraphic(null);
                    setText(null);
                    setStyle("-fx-background-color: WHITE");
                } else {
                    TrainThreadModel element = getTreeTableRow().getItem();
                    if (element != null) {
                        double effect = (expectedValueEffect - element.getEffect());
                        if (effect <= (1 * standardDeviation)) {
                            setStyle("-fx-background-color: #3bd867");
                        }
                        if ((effect > (1 * standardDeviation))&&(effect <= (2 * standardDeviation))) {
                            setStyle("-fx-background-color: #fffb64");
                        }
                        if (effect > (2 * standardDeviation)) {
                            setStyle("-fx-background-color: #ff504a");
                        }
                    }
                    setText(value != null ? value.toString() : "");
                }
            }
        };
        strColumn.setCellFactory(cellCallbackString);

        strColumn.setPrefWidth(prefWidth);
        strColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, String>("nameTR"));
        trainThreadTableGDP.getColumns().add(strColumn);

        title = bundle.getString("trainThreadTable.len");
        dblColumn = new TreeTableColumn<>(title);
        Callback<TreeTableColumn<TrainThreadModel, Double>, TreeTableCell<TrainThreadModel, Double>> cellCallbackDouble = (TreeTableColumn<TrainThreadModel, Double> param) -> new TreeTableCell<TrainThreadModel, Double>() {
            @Override
            protected final void updateItem(Double value, boolean empty) {
                super.updateItem(value, empty);
                if (empty) {
                    setGraphic(null);
                    setText(null);
                    setStyle("-fx-background-color: WHITE");
                } else {
                    TrainThreadModel element = getTreeTableRow().getItem();
                    if (element != null) {
                        double effect = (expectedValueEffect - element.getEffect());
                        if (effect <= (1 * standardDeviation)) {
                            setStyle("-fx-background-color: #3bd867");
                        }
                        if ((effect > (1 * standardDeviation))&&(effect <= (2 * standardDeviation))) {
                            setStyle("-fx-background-color: #fffb64");
                        }
                        if (effect > (2 * standardDeviation)) {
                            setStyle("-fx-background-color: #ff504a");
                        }
                    }
                    setText(value != null ? value.toString() : "");
                }
            }
        };
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("len"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("trainThreadTable.timeWithoutStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("timeWithoutStop"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("trainThreadTable.timeWithStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("timeWithStop"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedWithoutStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("speedWithoutStop"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedWithStop");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("speedWithStop"));
        trainThreadTableGDP.getColumns().add(dblColumn);


        title = bundle.getString("calc.locTypeName");
        prefWidth = 180;
        strColumn = new TreeTableColumn<>(title);
        strColumn.setPrefWidth(prefWidth);
        strColumn.setCellFactory(cellCallbackString);
        strColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, String>("expenseName"));
        trainThreadTableGDP.getColumns().add(strColumn);

        title = bundle.getString("trainThreadTable.timeNormMove");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("timeNormMove"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("calc.speedNorm");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("speedNormMove"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("calc.energyMove");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("energyWithoutStop"));
        trainThreadTableGDP.getColumns().add(dblColumn);
        //  title = bundle.getString("javafx.dialog.calculatetimeenergy.table.energywithstop");

        title = bundle.getString("calc.stopCount");
        intColumn = new TreeTableColumn<>(title);
        //dblColumn.setCellFactory(cellCallbackDouble);
        intColumn.setCellFactory(cellCallbackInteger);
        intColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Integer>("stopCount"));
        trainThreadTableGDP.getColumns().add(intColumn);

        title = bundle.getString("calc.moveExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("moveExpense"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("calc.downUpExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("downUpExpense"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("calc.stopExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("stopExpense"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("calc.normExpense");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("normExpense"));
        trainThreadTableGDP.getColumns().add(dblColumn);

        title = bundle.getString("calc.effect");
        dblColumn = new TreeTableColumn<>(title);
        dblColumn.setCellFactory(cellCallbackDouble);
        dblColumn.setCellValueFactory(new TreeItemPropertyValueFactory<TrainThreadModel, Double>("effect"));
        trainThreadTableGDP.getColumns().add(dblColumn);

    }

    /**
     * @param distrEditEntity {@link DistrEditEntity} ������ ��� ������� � �����������
     */
    public void setContent(DistrEditEntity distrEditEntity) {
        scene.setCursor(Cursor.WAIT);
        try {
            this.distrEditEntity = distrEditEntity;
            logger.debug("updateModelWithContent");

            // LocomotiveType
            List<LocomotiveType> locomotiveTypeList = distrEditEntity.getDistrEntity().getListDistrLoc();
            this.locTypeModelList = buildLocTypeModelList(locomotiveTypeList);
            locTypeComboBox.setItems(FXCollections.observableList(this.locTypeModelList));
            // �� ��������� ������ ���� ������ "��������"
            for (LocTypeModel type : this.locTypeModelList) {
                if (type.getLocomotiveType().getIdLType() == 1) {
                    locTypeComboBox.getSelectionModel().select(type);
                    break;
                }
            }
            locTypeModelTable.setItems(FXCollections.observableList(this.locTypeModelList));

            // ExpenseRate
            expenseRateModelTable.setItems(FXCollections.observableList(this.expenseRateModelList));

            // TrainThreadModel List
            updateTrainThreadTable();
            // DistrCalc
            distrCalcTreeTable.getRoot().getChildren().clear();
            // deleted
            deletedGroupP.clear();
            deletedRange.clear();
            deletedColor.clear();
            //load ExpenseRate from DB
            loadExpenseRateFromDB();
            tabPane.getTabs().remove(tabReting);

            dirCalcMap = new HashMap<>();
            dirCalcTreeTable.getRoot().getChildren().clear();
            dirTable.getItems().clear();
        } catch (DataManageException e) {
            e.printStackTrace();
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    public static List<ExpenseRateModel> buildExpenseRateModelList() {
        List<ExpenseRateModel> list = new ArrayList<>();
        ExpenseRateModel model;

        model = new ExpenseRateModel(TrainType.gr, EnergyType.teplo);
        list.add(model);
        model = new ExpenseRateModel(TrainType.gr, EnergyType.electro);
        list.add(model);
        model = new ExpenseRateModel(TrainType.pass, EnergyType.teplo);
        list.add(model);
        model = new ExpenseRateModel(TrainType.pass, EnergyType.electro);
        list.add(model);
        model = new ExpenseRateModel(TrainType.town, EnergyType.teplo);
        list.add(model);
        model = new ExpenseRateModel(TrainType.town, EnergyType.electro);
        list.add(model);

        return list;
    }

    /**
     *
     * @param locomotiveTypeList
     * @return
     */
    public List<LocTypeModel> buildLocTypeModelList(List<LocomotiveType> locomotiveTypeList) {
        List<LocTypeModel> list = new ArrayList<>();
        for (LocomotiveType type : locomotiveTypeList) {
            LocTypeModel typeModel;
            typeModel = new LocTypeModel(type, EnergyType.electro);
            list.add(typeModel);
            typeModel = new LocTypeModel(type, EnergyType.teplo);
            list.add(typeModel);
            /*
            ��� ���� ������ ������ ��� ���� ���������� ����� ��� �� ��� ����������
            LocomotiveType
            todo ����� ��� LocomotiveType �������� EnergyType
             */
        }
        Collections.sort(list);
        return list;
    }

    private List<GroupPModel> buildGroupPModelList() {
        List<GroupPModel> groupPModelList = new ArrayList<>();
        IGroupPDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getGroupPDAO();
        try {
            List<GroupP> list = dao.getGroupPList();
            Collections.sort(list);
            for(GroupP groupP: list){
                GroupPModel model = new GroupPModel(groupP);
                groupPModelList.add(model);
            }
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
            Alert alert = new Alert(Alert.AlertType.ERROR, e.toString());
            alert.showAndWait();
        }
        return groupPModelList;
    }

    @FXML
    private void handleOpenDBGroupButton(ActionEvent event){
        scene.setCursor(Cursor.WAIT);
        try {
            List<GroupPModel> groupPModelList = buildGroupPModelList();
            groupPTable.setItems(FXCollections.observableArrayList(groupPModelList));
            if (groupPModelList.size() > 0) {
                GroupPModel first = groupPModelList.get(0);
                selectGroupPModel(first);
            } else {
                groupRangeTable.setItems(FXCollections.observableArrayList(new ArrayList<>()));
                groupColorTable.setItems(FXCollections.observableArrayList(new ArrayList<>()));
            }
        } finally {
            addGroupButton.setDisable(false);
            delGroupButton.setDisable(false);
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    private void selectGroupPModel(GroupPModel groupPModel) {
        List<GroupRangeModel> groupRangeModelList = buildGroupRangeModelList(groupPModel);
        List<GroupColorModel> groupColorModelList = buildGroupColorModelList(groupPModel);
        groupRangeTable.setItems(FXCollections.observableArrayList(groupRangeModelList));
        groupColorTable.setItems(FXCollections.observableArrayList(groupColorModelList));
    }

    private List<GroupColorModel> buildGroupColorModelList(GroupPModel groupPModel) {
        List<GroupColorModel> list = new ArrayList<>();
        for (GroupColor groupColor: groupPModel.getGroupP().getColorList()){
            GroupColorModel colorModel = new GroupColorModel(groupColor);
            list.add(colorModel);
        }
        return list;
    }

    private List<GroupRangeModel> buildGroupRangeModelList(GroupPModel groupPModel) {
        List<GroupRangeModel> list = new ArrayList<>();
        for(GroupRange range: groupPModel.getGroupP().getRangeList()){
            GroupRangeModel rangeModel = new GroupRangeModel(range);
            list.add(rangeModel);
        }
        return list;
    }

    @FXML
    private void handleApplyLocomotiveType(ActionEvent event){
        LocTypeModel currentLoc = locTypeComboBox.getSelectionModel().getSelectedItem();
        if (currentLoc != null) {
            for (TreeItem<CalcModel> item : distrCalcTreeTable.getSelectionModel().getSelectedItems()) {
                CalcModel model = item.getValue();
                if (model.getLayer() == CalcModel.LAYER_GROUP) {
                    model.setLocType(currentLoc);
                }
            }
        }

    }

    @FXML
    private void handleCalculateTimeEnergyGDP(ActionEvent event) {
        logger.debug("handleCalculateTimeEnergyGDP");
        ////
        GroupRange groupRange = new GroupRange();
        groupRange.setIdRange(79);
        groupRange.setIdGroup(39);
        groupRange.setMinCode(1000);
        groupRange.setMaxCode(3399);

        List<GroupRange> rangeList = new ArrayList<>();
        rangeList.add(groupRange);

        GroupP groupP = new GroupP();
        groupP.setRangeList(rangeList);
        groupP.setName(bundle.getString("calc.freightlines"));
        groupP.setIdGroup(39);
        groupP.setByColor(false);
        groupP.setOrd(8);
        groupP.setIdLtype(0);

        LocomotiveType locomotiveType = new LocomotiveType();
        locomotiveType.setLtype(bundle.getString("calc.cargo"));
        locomotiveType.setColorLoc(0);
        locomotiveType.setShowInGDP(true);
        locomotiveType.setIdLtype(1);//1

        EnergyType energyType;
        int[] electroDistr = {248, 307, 218, 305, 224, 215, 225, 320, 321};//�������
        int idDistr = distrEditEntity.getIdDistr();
        boolean found = Arrays.stream(electroDistr).anyMatch(x -> x == idDistr);
        if (found) {energyType = EnergyType.electro;} else {energyType = EnergyType.teplo;}

        LocTypeModel locTypeModel = new LocTypeModel(locomotiveType, energyType);

        CalcModel model = new CalcModel(groupP, bundle.getString("calc.freightlines"), 1);
        model.setLocType(locTypeModel);
        TreeItem<CalcModel> root = distrCalcTreeTable.getRoot();
        root.getChildren().clear();
        root.getChildren().add(new TreeItem<>(model));
        /////
        try {
            List<String> gdpEditWarnings = new ArrayList<>();
            List<CalcGroup> distrCalcGroups = createTargetDistrCalcGroupList();
            Map<Dir, List<CalcGroup>>  dirCalcMap = new HashMap<>();
            Task task = new Task<Void>() {
                @Override
                protected Void call() {
                    // calculate
                    updateExpenseRateEn();
                    updateDirList();
                    CalculateTimeEnergyHandler.calculateTimeEnergy(distrEditEntity,
                            dirList, dirCalcMap,
                            distrCalcGroups,
                            expenseRateEnMap, expenseRateModelList,
                            onlyRBCheckBox.isSelected(), noTechStCheckBox.isSelected(),
                            gdpEditWarnings);
                    return null;
                }
            };
            TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, () -> {
                // showResult
                calcExpectedValue();
                updateTrainThreadTableGDP();
                String message = KOFF_EFFECT_GDP + String.format("%.3f", expectedValueEffect);
                tabPane.getTabs().add(tabReting);
                SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
                selectionModel.select(tabReting);
                Alert alert;
                if (statusEffectGDP) {
                    alert = new Alert(Alert.AlertType.INFORMATION, message);
                    alert.setTitle(bundle.getString("calc.energyefficiencygraphics"));
                    alert.setHeaderText(bundle.getString("calc.statusgraphicseffective"));
                } else {
                    alert = new Alert(Alert.AlertType.WARNING, message);
                    alert.setTitle(bundle.getString("calc.energyefficiencygraphics"));
                    alert.setHeaderText(bundle.getString("calc.statusgraphicsnoteffective"));
                }
                alert.showAndWait();
            });

        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }


    private void calcExpectedValue() {
        double minEffect = 1;
        double effectOnTime = 0;
        double sumTime = 0;
        List<Double> effects = new ArrayList<>();
        List<TrainThread> list = distrEditEntity.getListTrains();
        for (TrainThread trainThread : list) {
            if (!trainThread.isForeign()) {
                //TrainThreadModel trainThreadModel = new TrainThreadModel(trainThread);
                if (trainThread.getDistrTrain().getDistrTrainExpenseMap().size() > 0) {
                    List<DistrTrainExpense> expenses = new ArrayList<>(trainThread.getDistrTrain().getDistrTrainExpenseMap().values());
                    for (DistrTrainExpense expense : expenses) {
                        TrainThreadModel trainThreadModelE = new TrainThreadModel(trainThread);
                        trainThreadModelE.setExpense(expense);
                        double effect = trainThreadModelE.getEffect();
                        effects.add(effect);
                        double timeWithStop = trainThreadModelE.getTimeWithStop();
                        effectOnTime += effect * timeWithStop;
                        sumTime += timeWithStop;
                        if (minEffect > effect) minEffect = effect;
                    }
                }
            }
        }
        expectedValueEffect = effectOnTime / sumTime;
        standardDeviation = 0;
        for (double effect : effects) {
            standardDeviation += (effect - expectedValueEffect) * (effect - expectedValueEffect);
        }
        standardDeviation = Math.sqrt(standardDeviation / (effects.size() - 1));
        double effect = (expectedValueEffect - minEffect);
        if (effect > (2 * standardDeviation)) {
            statusEffectGDP = false;
        } else {statusEffectGDP = true;}
    }


    @FXML
    private void handleCalculateTimeEnergy(ActionEvent event){
        logger.debug("handleCalculateTimeEnergy");
        try {
            List<String> gdpEditWarnings = new ArrayList<>();
            List<CalcGroup> distrCalcGroups = createTargetDistrCalcGroupList();
            dirCalcMap = new HashMap<>();
            Task task = new Task<Void>() {
                @Override
                protected Void call() {
                    // calculate
                    updateExpenseRateEn();
                    updateDirList();
                    try {
                        CalculateTimeEnergyHandler.calculateTimeEnergy(distrEditEntity,
                                dirList, dirCalcMap, distrCalcGroups,
                                expenseRateEnMap, expenseRateModelList,
                                onlyRBCheckBox.isSelected(), noTechStCheckBox.isSelected(),
                                gdpEditWarnings);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                        throw e;
                    }
                    return null;
                }
            };
            TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, () -> {
                // showResult
                updateTrainThreadTable();
                updateDistrCalcTreeTable(distrCalcGroups);
                updateDirTrainTable();
                updateDirCalcTable();
                String message = "";
                if (gdpEditWarnings.size() > 0) {
                    message = message + "; " + errCalculateTimeEnergy + " ../logs/gdpedit.log";
                }

                Alert alert = new Alert(Alert.AlertType.INFORMATION, message);
                alert.showAndWait();
            });

        } catch (Exception e){
            logger.error(e.toString(), e);
        }
    }

    private void updateExpenseRateEn() {
        Double v;
        v = getDoubleFromTextField(expenseRateEnTeplo);
        expenseRateEnMap.get(EnergyType.teplo).setEnergyExpRate(v);
        v = getDoubleFromTextField(expenseRateEnElectro);
        expenseRateEnMap.get(EnergyType.electro).setEnergyExpRate(v);
    }

    private double getDoubleFromTextField(TextField field) {
        if (field.getText() != null && !field.getText().trim().isEmpty()) {
            try {
                return Double.parseDouble(field.getText());
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
        return 0;
    }

    private void updateTrainThreadTableGDP() {
        trainThreadTableGDP.getRoot().getChildren().clear();
        List<TrainThread> list = distrEditEntity.getListTrains();
        Collections.sort(list, new TrainComparatorByCode());
        for (TrainThread trainThread : list) {
            if (!trainThread.isForeign()) {
                TrainThreadModel trainThreadModel = new TrainThreadModel(trainThread);
                TreeItem<TrainThreadModel> item = new TreeItem<>(trainThreadModel);

                // trainThreadTableGDP.getRoot().getChildren().add(item);
                if (trainThread.getDistrTrain().getDistrTrainExpenseMap().size() > 0) {
                    List<DistrTrainExpense> expenses = new ArrayList<>(trainThread.getDistrTrain().getDistrTrainExpenseMap().values());
                    for (DistrTrainExpense expense : expenses) {
                        TrainThreadModel trainThreadModelE = new TrainThreadModel(trainThread);
                        trainThreadModelE.setExpense(expense);
                        TreeItem<TrainThreadModel> itemE = new TreeItem<>(trainThreadModelE);

                        //    item.getChildren().add(itemE);
                        trainThreadTableGDP.getRoot().getChildren().add(itemE);
                    }
                }
            }
        }
        trainThreadTableGDP.getRoot().setExpanded(true);
    }

    private void updateTrainThreadTable() {
        trainThreadTable.getRoot().getChildren().clear();
        List<TrainThread> list = distrEditEntity.getListTrains();
        Collections.sort(list, new TrainComparatorByCode());
        for (TrainThread trainThread : list) {
            if (!trainThread.isForeign()) {
                TrainThreadModel trainThreadModel = new TrainThreadModel(trainThread);
                TreeItem<TrainThreadModel> item = new TreeItem<>(trainThreadModel);
                trainThreadTable.getRoot().getChildren().add(item);
                if(trainThread.getDistrTrain().getDistrTrainExpenseMap().size()>0){
                    List<DistrTrainExpense> expenses = new ArrayList<>(trainThread.getDistrTrain().getDistrTrainExpenseMap().values());
                    for(DistrTrainExpense expense: expenses ){
                        TrainThreadModel trainThreadModelE = new TrainThreadModel(trainThread);
                        trainThreadModelE.setExpense(expense);
                        TreeItem<TrainThreadModel> itemE = new TreeItem<>(trainThreadModelE);
                        item.getChildren().add(itemE);
                    }
                }
            }
        }
    }

    private void updateDistrCalcTreeTable(List<CalcGroup> distrCalcGroups) {
        distrCalcTreeTable.getRoot().getChildren().clear();
        for (CalcGroup calcGroup : distrCalcGroups) {
            TreeItem<CalcModel> group = new TreeItem<>(calcGroup.getAllCalc());
            group.getValue().calcV();
            distrCalcTreeTable.getRoot().getChildren().add(group);
            TreeItem<CalcModel> odd = new TreeItem<>(calcGroup.getOddCalc());
            odd.getValue().calcV();
            TreeItem<CalcModel> even = new TreeItem<>(calcGroup.getEvenCalc());
            even.getValue().calcV();
            group.getChildren().add(odd);
            group.getChildren().add(even);
        }
    }

    private void updateDirTrainTable() {
        dirTrainTable.setItems(FXCollections.observableArrayList(new ArrayList<>()));
        List<TrainThread> list = distrEditEntity.getListTrains();
        Collections.sort(list, new TrainComparatorByCode());
        for (TrainThread trainThread : list) {
            if (!trainThread.isForeign()) {
                for (DirTrain dirTrain : trainThread.getDistrTrain().getDirTrainList()) {
                    String nameDir = getNameDir(dirList, dirTrain.getIdDir());
                    DirTrainModel dirTrainModel = new DirTrainModel(dirTrain, trainThread.getDistrTrain(), nameDir);
                    dirTrainTable.getItems().add(dirTrainModel);
                }
            }
        }
    }

    /**
     * �������� ������ ��� �� ��
     */
    private void updateDirList(){
        IDirDAO dirDAO = GDPDAOFactoryCreater.getGDPDAOFactory().getDirDAO();
        try {
            dirList = dirDAO.getListDir();
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
            dirList = new ArrayList<>();
            Alert alert = new Alert(Alert.AlertType.ERROR, e.toString());
            alert.showAndWait();
        }
    }

    private String getNameDir(List<Dir> dirList, int idDir){
        for(Dir dir: dirList){
            if(dir.getIdDir() == idDir){
                return dir.getName();
            }
        }
        return "";
    }

    private List<CalcGroup> createTargetDistrCalcGroupList() {
        List<CalcGroup> list = new ArrayList<>();
        for (TreeItem<CalcModel> item : distrCalcTreeTable.getRoot().getChildren()) {
            CalcModel model = item.getValue();
            model.clearCalc();
            CalcGroup distrCalcGroup = new CalcGroup(item.getValue());
            CalcModel oddModel = new CalcModel(model.getGroupP(), OE.odd.getString(), CalcModel.LAYER_VALUE);
            oddModel.setLocType(model.getLocType());
            distrCalcGroup.setOddCalc(oddModel);
            CalcModel evenModel = new CalcModel(model.getGroupP(), OE.even.getString(), CalcModel.LAYER_VALUE);
            evenModel.setLocType(model.getLocType());
            distrCalcGroup.setEvenCalc(evenModel);
            list.add(distrCalcGroup);
        }
        return list;
    }

    @FXML
    private void handleSaveGroupPToDB(ActionEvent event){
        scene.setCursor(Cursor.WAIT);
        Alert.AlertType  alertType = Alert.AlertType.INFORMATION;
        String message = bundle.getString("calc.ok");
        try {
            IGroupPDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getGroupPDAO();
            try {
                List<GroupP> groupPList = GroupPModel.extractGroupP(groupPTable.getItems());
                dao.saveToDB(groupPList, deletedGroupP, deletedRange, deletedColor);
            } catch (DataManageException e) {
                logger.error(e.toString(), e);
                alertType = Alert.AlertType.ERROR;
                message = e.toString();
            }
        } finally {
            scene.setCursor(Cursor.DEFAULT);
            handleOpenDBGroupButton(null);
        }
        Alert alert = new Alert(alertType, message);
        alert.showAndWait();
    }

    @FXML
    private void handleSaveTimeEnergyToDB(ActionEvent event){
        try {
            Task task = new Task<Void>() {
                @Override
                protected Void call() {
                    try {
                        IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
                        dao.saveAllTrainTimeEnergy(distrEditEntity.getDistrTrains());
                        dao.saveDirTrain(distrEditEntity.getDistrTrains());
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                        throw new RuntimeException(e);
                    }
                    return null;
                }
            };
            TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, () -> {
            });
        } catch (Exception e) {
            logger.error(e.toString(), e);
            Alert alert = new Alert(Alert.AlertType.ERROR, e.toString());
            alert.showAndWait();
        }
    }

    @FXML
    private void handleSaveTimeEnergyToFile(ActionEvent event) {
        String distrName = this.distrEditEntity.getDistrEntity().getDistrName();
        String fileName = distrName + ".csv";

        String title = bundle.getString("groupPane.calcSaveFileButton");
        File file = FileUtil.chooseFile(FileUtil.Mode.save, title, fileName, stage,
                new FileChooser.ExtensionFilter("Excel Files", "*.xlsx"));
        if (file != null) {
            final TaskWarning taskWarning = new TaskWarning();
            Task task = new Task<Void>() {
                @Override
                protected Void call() {
                    boolean res = ExportExcelHandler.saveToExcel(file, bundle,
                            distrCalcTreeTable,
                            trainThreadTable,
                            dirTrainTable,
                            dirCalcMap);
                    if(!res){
                        taskWarning.warnings.add("Err: see log");
                    }
                    return null;
                }
            };
            TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, ()->{
                if(taskWarning.warnings.size()>0) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION, taskWarning.warnings.get(0));
                    alert.showAndWait();
                }
            });
        }
    }

    @FXML
    private void handleSaveEnergyToFile(ActionEvent event) {
        String distrName = this.distrEditEntity.getDistrEntity().getDistrName();
        String fileName = distrName + ".csv";

        String title = bundle.getString("groupPane.calcSaveFileButton");
        File file = FileUtil.chooseFile(FileUtil.Mode.save, title, fileName, stage,
                new FileChooser.ExtensionFilter("Excel Files", "*.xlsx"));
        if (file != null) {
            final TaskWarning taskWarning = new TaskWarning();
            Task task = new Task<Void>() {
                @Override
                protected Void call() {
                    boolean res = ExportExcelHandler.saveEffectToExcel(file, bundle,
                            distrCalcTreeTable,
                            trainThreadTableGDP,
                            dirTrainTable,
                            dirCalcMap);
                    if(!res){
                        taskWarning.warnings.add("Err: see log");
                    }
                    return null;
                }
            };
            TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, ()->{
                if(taskWarning.warnings.size()>0) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION, taskWarning.warnings.get(0));
                    alert.showAndWait();
                }
            });
        }
    }

    @FXML
    private void handleAddGroupToCalcButton(ActionEvent event){
        if (groupPTable.getSelectionModel() != null) {
            TreeItem<CalcModel> root = distrCalcTreeTable.getRoot();
            for (GroupPModel groupPModel : groupPTable.getSelectionModel().getSelectedItems()) {
                GroupP groupP = groupPModel.getGroupP();
                CalcModel calcModel = new CalcModel(groupP, groupP.getName(), CalcModel.LAYER_GROUP);
                root.getChildren().add(new TreeItem<>(calcModel));
            }
        }
    }

    @FXML
    private void handleDelGroupFromCalcButton(ActionEvent event){
        if (distrCalcTreeTable.getSelectionModel() != null) {
            for (TreeItem<CalcModel> item: distrCalcTreeTable.getSelectionModel().getSelectedItems()) {
                distrCalcTreeTable.getRoot().getChildren().remove(item);
            }
        }
    }

    @FXML
    private void handleAddGroupP(ActionEvent event){
        GroupP groupP = new GroupP();
        groupPTable.getItems().add(new GroupPModel(groupP));
        logger.debug("add new GroupP: " + groupP);
    }

    @FXML
    private void handleDelGroupP(ActionEvent event){
        if (groupPTable.getSelectionModel() != null) {
            List<GroupPModel> selected = groupPTable.getSelectionModel().getSelectedItems();
            List<GroupP> deleted = GroupPModel.extractGroupP(selected);
            groupPTable.getItems().removeAll(selected);
            for (GroupP groupP : deleted) {
                if (groupP.getIdGroup() > 0) {
                    deletedGroupP.add(groupP);
                }
            }
            logger.debug("delete GroupP: " + deleted);
        }
    }

    @FXML
    private void handleAddRange(ActionEvent event) {
        if (selectedGroupP != null) {
            GroupRange range = new GroupRange();
            range.setIdGroup(selectedGroupP.getGroupP().getIdGroup());
            selectedGroupP.getGroupP().getRangeList().add(range);
            groupRangeTable.getItems().add(new GroupRangeModel(range));
            logger.debug("add GroupRang: " + range);
        }
    }

    @FXML
    private void handleDelRange(ActionEvent event){
        if (groupRangeTable.getSelectionModel() != null) {
            List<GroupRangeModel> selected = groupRangeTable.getSelectionModel().getSelectedItems();
            List<GroupRange> deleted = GroupRangeModel.extractGroupRange(selected);
            for (GroupRange range : deleted) {
                if (range.getIdRange() > 0) {
                    deletedRange.add(range);
                }
            }
            selectedGroupP.getGroupP().getRangeList().removeAll(deleted);
            groupRangeTable.getItems().removeAll(selected);
            logger.debug("delete GroupRang: " + selected);
        }
    }

    @FXML
    private void handleAddColor(ActionEvent event) {
        if (selectedGroupP != null) {
            ColorDialog cd = GDPEdit.gdpEdit.getDialogManager().getColorDialog();
            cd.setVisible(true);
            if (cd.getResult() > 0) {
                java.awt.Color clrAWT = cd.getColor();
                GroupColor groupColor = new GroupColor();
                groupColor.setIdGroup(selectedGroupP.getGroupP().getIdGroup());
                groupColor.setColor(ColorConverter.convert(clrAWT));
                groupColorTable.getItems().add(new GroupColorModel(groupColor));
                selectedGroupP.getGroupP().getColorList().add(groupColor);
                logger.debug("add GroupColor: " + groupColor);
            }
        }
    }

    @FXML
    private void handleDelColor(ActionEvent event){
        if (groupColorTable.getSelectionModel() != null) {
            List<GroupColorModel> selected = groupColorTable.getSelectionModel().getSelectedItems();
            List<GroupColor> deleted = GroupColorModel.extractGroupColor(selected);
            for (GroupColor color : deleted) {
                if (color.getIdColorGR() > 0) {
                    deletedColor.add(color);
                }
            }
            selectedGroupP.getGroupP().getColorList().removeAll(deleted);
            groupColorTable.getItems().removeAll(selected);
            logger.debug("delete GroupColor: " + selected);
        }
    }

    @FXML
    private void saveExpenseRatesDB(ActionEvent event) throws DataManageException {
        logger.debug("save Expense Rates to DB: ");
        String titleMain = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.attention");
        String titleMiddle = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.questionsave");
        String titleSmall;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        if (alert.showAndWait().get() == ButtonType.OK){
            titleMiddle = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.savedb");
            try {
                ISpanEnergyDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getSpanEnergyDAO();
                //ExpenseRateEnergy
                updateExpenseRateEn();
                List<ExpenseRateEnergy> expenseRateEnergyList = new ArrayList<>();
                expenseRateEnergyList.add(new ExpenseRateEnergy(EnergyType.teplo, (float) expenseRateEnMap.get(EnergyType.teplo).getEnergyExpRate()));
                expenseRateEnergyList.add(new ExpenseRateEnergy(EnergyType.electro, (float) expenseRateEnMap.get(EnergyType.electro).getEnergyExpRate()));
                dao.saveExpenseRateEnergy(expenseRateEnergyList);
                //ExpenseRate
                List<ExpenseRate> expenseRateList = new ArrayList<>();
                for (int i = 0; i < expenseRateModelList.size(); i++) {
                    float move = (float) expenseRateModelList.get(i).getMoveExpRate();
                    float stop = (float) expenseRateModelList.get(i).getStopExpRate();
                    String tr = expenseRateModelList.get(i).getTrainType().getString();
                    String en = expenseRateModelList.get(i).getEnergyType().getString();
                    ExpenseRate expenseRate = new ExpenseRate(move, stop, TrainType.getType(tr), EnergyType.getType(en));
                    expenseRateList.add(expenseRate);
                }
                dao.saveExpenseRate(expenseRateList);
                //ExpenseRateNormative
                List<ExpenseRateNormative> expenseRateNormativeList = new ArrayList<>();
                for (int i = 0; i < locTypeModelList.size(); i++) {
                    int id = locTypeModelList.get(i).getLocomotiveType().getIdLType();
                    String en = locTypeModelList.get(i).getEnergyType().getString();
                    float du = (float) locTypeModelList.get(i).getDownupExp();
                    ExpenseRateNormative expenseRateNormative = new ExpenseRateNormative(id, EnergyType.getType(en), du);
                    expenseRateNormativeList.add(expenseRateNormative);
                }
                dao.saveExpenseRateNormative(expenseRateNormativeList);
                titleSmall = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.good");
                showInformation(titleMain, titleMiddle, titleSmall);
            } catch (Exception e) {
                logger.error(e.toString(), e);
                titleSmall = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.bad");
                showInformation(titleMain, titleMiddle, titleSmall);
            }
        }
    }

    @FXML
    private void saveExpenseRatesXML(ActionEvent event){
        logger.debug("save Expense Rates to xml-file: ");
        String titleMain = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.attention");
        String titleMiddle = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.savexml");
        String titleSmall;
        try {
            ExpenseRateTransfer expenseRateTransfer = new ExpenseRateTransfer();
            updateExpenseRateEn();
            ExpenseRateEnergy teplo = new ExpenseRateEnergy(EnergyType.teplo, (float) expenseRateEnMap.get(EnergyType.teplo).getEnergyExpRate());
            expenseRateTransfer.setTeplo(teplo);
            ExpenseRateEnergy electro = new ExpenseRateEnergy(EnergyType.electro, (float) expenseRateEnMap.get(EnergyType.electro).getEnergyExpRate());
            expenseRateTransfer.setElectro(electro);
            List<ExpenseRate> expenseRateList = new ArrayList<>();
            for (ExpenseRateModel anExpenseRateModelList : expenseRateModelList) {
                float moveExpRate = (float) anExpenseRateModelList.getMoveExpRate();
                float stopExpRate = (float) anExpenseRateModelList.getStopExpRate();
                ExpenseRate expenseRate = new ExpenseRate(moveExpRate, stopExpRate, anExpenseRateModelList.getTrainType(), anExpenseRateModelList.getEnergyType());
                expenseRateList.add(expenseRate);
            }
            expenseRateTransfer.setExpenseRateList(expenseRateList);
            List<ExpenseRateNormative> expenseRateNormativeList = new ArrayList<>();
            for (LocTypeModel aLocTypeModelList : locTypeModelList) {
                int idLtype = aLocTypeModelList.getLocomotiveType().getIdLType();
                String lType = aLocTypeModelList.getLocomotiveType().getLtype();
                float downUpExp = (float) aLocTypeModelList.getDownupExp();
                ExpenseRateNormative expenseRateNormative = new ExpenseRateNormative(idLtype, lType, aLocTypeModelList.getEnergyType(), downUpExp);
                expenseRateNormativeList.add(expenseRateNormative);
            }
            expenseRateTransfer.setExpenseRateNormativeList(expenseRateNormativeList);
            String fileName = "ExpenseRates_Local.xml";
            expenseRateTransfer.setDbMode("localMode");
            if (GDPDAOFactoryCreater.getGDPDAOFactory().isServer()) {
                fileName = "ExpenseRates_Server.xml";
                expenseRateTransfer.setDbMode("serverMode");
            }
            File file = FileUtil.chooseFile(FileUtil.Mode.save,null, fileName, GDPEdit.gdpEdit.getMainFrame(),
                    new FileNameExtensionFilter("XML files", "xml"));
            CalculateTimeEnergyXML.saveExpRateXML(file, expenseRateTransfer);
            titleSmall = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.good");
            showInformation(titleMain, titleMiddle, titleSmall);
        } catch (Exception e) {
            logger.error(e.toString(), e);
            titleSmall = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.bad");
            showInformation(titleMain, titleMiddle, titleSmall);
        }
    }

    @FXML
    private void loadExpenseRatesXML(ActionEvent event){
        logger.debug("load Expense Rates from xml-file: ");
        String fileName = ".xml";
        File file = FileUtil.chooseFile(FileUtil.Mode.open,null, fileName, null,
                new FileChooser.ExtensionFilter("XML files", "*.xml"));
        // ����� ���� null, ��������, ���� ��� ������ ����� ������ cancel
        if (file != null) {
            boolean dopusk = true;
            ExpenseRateTransfer expenseRateTransfer = CalculateTimeEnergyXML.loadExpRateXML(file);
            String titleMain = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.attention");
            String titleMiddle = "";
            String titleSmall = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.wrangdbmode");
            if (expenseRateTransfer.getDbMode().equals("localMode") && GDPDAOFactoryCreater.getGDPDAOFactory().isServer()) {
                titleMiddle = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.wrangdbmodelocal");
                dopusk = false;
            }
            if (expenseRateTransfer.getDbMode().equals("serverMode") && !GDPDAOFactoryCreater.getGDPDAOFactory().isServer()) {
                titleMiddle = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.wrangdbmodeserver");
                dopusk = false;
            }
            if (dopusk) {
                useExpenseRateTransfer(expenseRateTransfer);
            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle(titleMain);
                alert.setHeaderText(titleMiddle);
                alert.setContentText(titleSmall);
                if (alert.showAndWait().get() == ButtonType.OK){
                    useExpenseRateTransfer(expenseRateTransfer);
                }
            }
        }
    }

    private void useExpenseRateTransfer(ExpenseRateTransfer expenseRateTransfer) {
        boolean dopusk = true;
        List<String> errorTypeList = new ArrayList<>();
        String errorLType = "";
        for (int i = 0; i < locTypeModelList.size(); i++) {
            String firstLType = locTypeModelList.get(i).getLocomotiveType().getLtype();
            boolean exist = false;
            for (int j = 0; j < expenseRateTransfer.getExpenseRateNormativeList().size(); j++) {
                String secondLType = expenseRateTransfer.getExpenseRateNormativeList().get(j).getLtype();
                if (firstLType.equals(secondLType)) {
                    exist = true;
                }
            }
            if (!exist) {
                for (String anErrorTypeList : errorTypeList) {
                    if (anErrorTypeList.equals(firstLType)) {
                        exist = true;
                    }
                }
            }
            if (!exist) {
                errorTypeList.add(firstLType);
                errorLType = errorLType + firstLType + "; ";
            }
        }
        if (!errorLType.equals("")) {
            dopusk = false;
            errorLType = errorLType.substring(0, errorLType.length() - 2);
        }
        if (dopusk) {
            updateExpenseRate(expenseRateTransfer);
        } else {
            String titleMain = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.attention");
            String titleMiddle = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.wrangloctype") + " " + errorLType;
            String titleSmall = bundle.getString("javafx.dialog.calculatetimeenergy.dialog.wrangdbmode");
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(titleMain);
            alert.setHeaderText(titleMiddle);
            alert.setContentText(titleSmall);
            if (alert.showAndWait().get() == ButtonType.OK){
                updateExpenseRate(expenseRateTransfer);
            }
        }
    }

    private void updateExpenseRate(ExpenseRateTransfer expenseRateTransfer) {
        ExpenseRateEnergy teplo = expenseRateTransfer.getTeplo();
        expenseRateEnMap.get(teplo.getEnergyType()).setEnergyExpRate(teplo.getExpRateEnValue());
        ExpenseRateEnergy electro = expenseRateTransfer.getElectro();
        expenseRateEnMap.get(electro.getEnergyType()).setEnergyExpRate(electro.getExpRateEnValue());
        for (int i = 0; i < expenseRateTransfer.getExpenseRateList().size(); i++) {
            ExpenseRate expenseRate = expenseRateTransfer.getExpenseRateList().get(i);
            for (int j = 0; j < expenseRateModelList.size(); j++) {
                ExpenseRateModel expenseRateModel = expenseRateModelList.get(j);
                if (expenseRateModel.getEnergyType().equals(expenseRate.getEnergyType()) && expenseRateModel.getTrainType().equals(expenseRate.getTrainType())) {
                    expenseRateModelList.get(j).setMoveExpRate(expenseRate.getMoveExpRate());
                    expenseRateModelList.get(j).setStopExpRate(expenseRate.getStopExpRate());
                }
            }
        }
        for (int i = 0; i < expenseRateTransfer.getExpenseRateNormativeList().size(); i++) {
            ExpenseRateNormative expenseRateNormative = expenseRateTransfer.getExpenseRateNormativeList().get(i);
            for (int j = 0; j < locTypeModelList.size(); j++) {
                LocTypeModel locTypeModel = locTypeModelList.get(j);
                // �������� ����������� ��� �� ���������� ID ��� �� ���������� ����
                if (locTypeModel.getEnergyType().equals(expenseRateNormative.getEnergyType()) && ((locTypeModel.getLocomotiveType().getIdLType() == expenseRateNormative.getIdLType()) || (locTypeModel.getLocomotiveType().getLtype().equals(expenseRateNormative.getLtype())))) {
                    locTypeModelList.get(j).setDownupExp(expenseRateNormative.getDownUpExp());
                }
            }
        }
        setExpenseRateEnergyFields();
        expenseRateModelTable.setItems(FXCollections.observableList(expenseRateModelList));
        locTypeModelTable.setItems(FXCollections.observableList(locTypeModelList));
    }

    private void setExpenseRateEnergyFields() {
        expenseRateEnTeplo.setText(String.valueOf(expenseRateEnMap.get(EnergyType.teplo).getEnergyExpRate()));
        expenseRateEnElectro.setText(String.valueOf(expenseRateEnMap.get(EnergyType.electro).getEnergyExpRate()));
    }

    private void loadExpenseRateFromDB() throws DataManageException {
        ISpanEnergyDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getSpanEnergyDAO();
        //ExpenseRateEnergy
        try {
            List<ExpenseRateEnergy> expenseRateEnergyList = dao.getExpenseRateEnergy();
            if (expenseRateEnergyList.size() > 0) {
                for (ExpenseRateEnergy anExpenseRateEnergyList : expenseRateEnergyList) {
                    expenseRateEnMap.get(anExpenseRateEnergyList.getEnergyType()).setEnergyExpRate(anExpenseRateEnergyList.getExpRateEnValue());
                }
            }
        } catch (Exception e) {
            logger.error("Not Exist ExpenseRateEnergy" + e.toString(), e);
        } finally {
            setExpenseRateEnergyFields();
        }
        //ExpenseRate
        try {
            List<ExpenseRate> expenseRateList = dao.getExpenseRate();
            if (expenseRateList.size() > 0) {
                expenseRateModelList.clear();
                for (ExpenseRate anExpenseRateList : expenseRateList) {
                    ExpenseRateModel expenseRateModel = new ExpenseRateModel(anExpenseRateList);
                    expenseRateModelList.add(expenseRateModel);
                }
            }
        } catch (Exception e) {
            logger.error("Not Exist ExpenseRate" + e.toString(), e);
        } finally {
            expenseRateModelTable.setItems(FXCollections.observableList(expenseRateModelList));
        }
        //ExpenseRateNormative
        try {
            List<ExpenseRateNormative> expenseRateNormativeList = dao.getExpenseRateNormative();
            if (expenseRateNormativeList.size() > 0) {
                locTypeModelList.clear();
                for (ExpenseRateNormative anExpenseRateNormativeList : expenseRateNormativeList) {
                    LocTypeModel locTypeModel = new LocTypeModel(anExpenseRateNormativeList);
                    locTypeModelList.add(locTypeModel);
                }
            }
        } catch (Exception e) {
            logger.error("Not Exist ExpenseRateNormative" + e.toString(), e);
        } finally {
            locTypeModelTable.setItems(FXCollections.observableList(locTypeModelList));
        }
    }

    private void showInformation(String titleMain, String titleMiddle, String titleSmall) {
        Alert alert;
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleMain);
        alert.setHeaderText(titleMiddle);
        alert.setContentText(titleSmall);
        alert.showAndWait();
    }

    private void updateDirCalcTable() {
        dirTable.setItems(FXCollections.observableArrayList(new ArrayList<>()));
        for(Dir dir: dirCalcMap.keySet()){
            DirModel dirModel = new DirModel(dir);
            dirTable.getItems().add(dirModel);
        }
        dirCalcTreeTable.getRoot().getChildren().clear();
    }

    private void updateDirCalcTreeTable(Dir dir) {
        dirCalcTreeTable.getRoot().getChildren().clear();
        List<CalcGroup> dirCalcGroups = dirCalcMap.get(dir);
        for (CalcGroup calcGroup : dirCalcGroups) {
            TreeItem<CalcModel> group = new TreeItem<>(calcGroup.getAllCalc());
            group.getValue().calcV();
            dirCalcTreeTable.getRoot().getChildren().add(group);
            TreeItem<CalcModel> odd = new TreeItem<>(calcGroup.getOddCalc());
            odd.getValue().calcV();
            TreeItem<CalcModel> even = new TreeItem<>(calcGroup.getEvenCalc());
            even.getValue().calcV();
            group.getChildren().add(odd);
            group.getChildren().add(even);
        }
    }
}
