package com.gmail.alaerof.dialog.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.gmail.alaerof.application.GDPEdit;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Helen Yrofeeva
 */
public class ApplicationConfigHandler {
    private static Logger logger = LogManager.getLogger(ApplicationConfigHandler.class);

    private static final String fileName = "appconfig" + File.separator + "appConfig.dat";
    
    public static void save(ApplicationConfig applicationConfig){

            ObjectOutputStream out;
            try {
                out = new ObjectOutputStream(new FileOutputStream(fileName));
                out.writeObject(applicationConfig);
                out.close();
            } catch (IOException e) {
                logger.error(e.toString(), e);
            }

    }
    
    public static ApplicationConfig load(){
        ApplicationConfig ac = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
            ac = (ApplicationConfig) in.readObject();
            in.close();
        } catch (IOException | ClassNotFoundException e) {
            logger.error(e.toString(), e);
            ApplicationConfigHandler.save(new ApplicationConfig());
        }
        return ac;
    }
}
