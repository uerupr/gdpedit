package com.gmail.alaerof.javafx.dialog.actualgdp;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class ClientSocketGDP {
    public static final String login = "admin";
    public static final String urlDefault = "10.20.47.73"; //"http://localhost/post2.php";
    public static final Charset charsetWin = Charset.forName("windows-1251"); // "CP1251" "US-ASCII"
    public static final Charset charsetUTF = Charset.forName("UTF-8");
    private static Logger logger = LogManager.getLogger(ClientSocketGDP.class);

    /**
     * основная функция установки соединения и передачи файла через сокет
     *
     * @param url - имя или IP сервера (пока задан urlDefault)
     * @param filePath   - путь и имя файла, который надо передать на сервер
     * @throws IOException
     * @throws ServerConnectionException
     * @throws GDPNotFoundException
     */
    public String connectAndSend(String url, String filePath) throws IOException, ServerConnectionException, GDPNotFoundException {
        File file = new File(filePath);
        String serverReply = "";
        if (file.exists()) {
        //    addLoginInXML(file);
            serverReply = sendXML(file, urlDefault);
        } else {
            throw new GDPNotFoundException(filePath);
        }
        return serverReply;
    }

    private String sendXML(File file, String url) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        final HttpEntity entity = MultipartEntityBuilder.create().addBinaryBody("userfile", file).build();
        httpPost.setEntity(entity);
        String respServer = "Нет ответа";
        try {
            HttpResponse response = httpclient.execute(httpPost);
            HttpEntity entityServer = response.getEntity();
            InputStream is = entityServer.getContent();
            BufferedReader bf = new BufferedReader(new InputStreamReader(is,
                    "UTF-8"));
            respServer = bf.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return respServer;
    }

    private void addLoginInXML(File file) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        Document document = null;
        try {
            document = documentBuilder.parse(file);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        NodeList fileInfo = document.getElementsByTagName("file_info");
        Element root = (Element) fileInfo.item(0);
        Element newServer = document.createElement("login");
        newServer.setAttribute("name", login);
        root.appendChild(newServer);

        DOMSource source = new DOMSource(document);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult(file);
            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
