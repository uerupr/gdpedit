package com.gmail.alaerof.dialog;

import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.ColorConverter;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.border.BevelBorder;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ColorDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private final JPanel panelColor = new JPanel();
    private int result;
    private static ResourceBundle bundle;

    /**
     * Create the dialog.
     */
    public ColorDialog() {
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.dialog", LocaleManager.getLocale());
    }

    private void initContent() {
        setBounds(0, 0, 600, 400);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new GridLayout(4, 6, 0, 0));
        this.addWindowListener(new WindowAdapter() {
            
            @Override
            public void windowClosing(WindowEvent e) {
                result = 0;
            }
        });
        
        ActionListener actionListener = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                JButton but = (JButton)ev.getSource();
                Color color = but.getBackground();
                panelColor.setBackground(color);
            }
        };

        {
            JButton btnNewButton = new JButton("");
            contentPanel.add(btnNewButton);
            btnNewButton.setBackground(ColorConverter.C7_RED);
            btnNewButton.addActionListener(actionListener);
            
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C5_FUCHSIA);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C10_YELLOW);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C19_MINT_GREEN);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C17_AQUA);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C25_WHITE);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C13_TOMATO);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C15_NEARLY_PALE_MAGENTA);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C9_NEARLY_ORANGE);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C11_GREEN);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C18_DODGER_BLUE);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C21_SILVER);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C14_DEEP_PINK);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C16_NEARLY_ORCHID);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C23_NEARLY_CARROT);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C12_NEARLY_DARK_GREEN);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C8_BLUE);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C3_GRAY);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C22_MAROON);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C24_PURPLE);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C1_BROWN);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C20_OLIVE);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C26_DARK_BLUE);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JButton btnNewButton = new JButton("");
            btnNewButton.setBackground(ColorConverter.C4_BLACK);
            contentPanel.add(btnNewButton);
            btnNewButton.addActionListener(actionListener);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog dialog = this;
            
            {
                panelColor.setBackground(Color.BLACK);
                panelColor.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
                panelColor.setPreferredSize(new Dimension(25,25));
                buttonPane.add(panelColor);
            }
            {
                JButton okButton = new JButton(bundle.getString("dialog.choose"));
                buttonPane.add(okButton);
                //okButton.setActionCommand("OK");
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        result = 1;
                        dialog.setVisible(false);
                    }
                });
            }
            {
                JButton cancelButton = new JButton(bundle.getString("dialog.cancel"));
                //cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        result = 0;
                        dialog.setVisible(false);
                    }
                });
            }
        }
    }
    
    public Color getColor(){
        return panelColor.getBackground();
    }
    
    public void setColor(Color color){
        panelColor.setBackground(color);
    }
    
    public int getResult(){
        return result;
    }

    public ColorDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

}
