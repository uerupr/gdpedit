package com.gmail.alaerof.printable;

import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.GDPPrintParam;
import com.gmail.alaerof.util.ConnectionDBFileHandlerUtil;
import javafx.print.Paper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.printing.PDFPageable;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.Destination;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;
import java.awt.Desktop;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * ���� ������ ��� ��� ������ javax.print.PrintService
 */
public class GDPPrintService {
    protected static Logger logger = LogManager.getLogger(GDPPrintService.class);

    /**
     * ��������������� ������ ��� � PDF
     * @param distrEditEntity
     * @param gdpPrintParam
     * @param printServiceName
     * @return
     * @throws PrinterException
     */
    private static boolean printPDF(DistrEditEntity distrEditEntity, GDPPrintParam gdpPrintParam, String printServiceName)
            throws PrinterException {
        // ��������������� ������ PDF
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setJobName(distrEditEntity.getDistrEntity().getDistrName());

        // ������ ��������� �� ����� �������� ������
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        PrintService printServicePDF = null;
        for (PrintService service : printServices) {
            logger.debug("PrintService: " + service.getName());
            if (printServiceName.equalsIgnoreCase(service.getName())) {
                printServicePDF = service;
            }
        }
        if (printServicePDF != null) {
            // ������� ��������������� ������ PDF
            job.setPrintService(printServicePDF);

            PrintRequestAttributeSet attrib = new HashPrintRequestAttributeSet();
            attrib.add(OrientationRequested.LANDSCAPE);
            MediaPrintableArea printableAreaManual = new MediaPrintableArea(0, 0,
                    (int) (Paper.A4.getWidth() / 72d), (int) (Paper.A4.getHeight() / 72d), MediaPrintableArea.INCH);

            attrib.remove(MediaPrintableArea.class);
            attrib.add(printableAreaManual);
            attrib.add(new Destination(new File("wtf.pdf").toURI()));

            job.setPrintable(new GDPPrintableImpl(distrEditEntity, gdpPrintParam));
            job.print(attrib);
            return true;
        } else {
            logger.error("'" + printServiceName + "' was not found");
        }
        return false;
    }

    /**
     * ������ ���
     * @param distrEditEntity
     * @param gdpPrintParam
     * @throws Exception
     */
    public static void print(DistrEditEntity distrEditEntity, GDPPrintParam gdpPrintParam) throws Exception {
        Properties gdpEditINI = ConnectionDBFileHandlerUtil.readGDPEditINI();
        boolean printPDF = true;
        boolean oldJob = false;
        boolean printAPI = false;
        String printServiceName = "Microsoft Print to PDF";
        try {
            printPDF = Boolean.valueOf(gdpEditINI.getProperty("printPDF"));
            oldJob = Boolean.valueOf(gdpEditINI.getProperty("oldJob"));
            printAPI = Boolean.valueOf(gdpEditINI.getProperty("printAPI"));
            printServiceName = gdpEditINI.getProperty("printPDFServiceName");
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        // ������ ��������� �� ����� �������� ������
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        // ��������������� ������ PDF +
        printPDF = printPDF && printPDF(distrEditEntity, gdpPrintParam, printServiceName);
        if (printPDF){
            // ������ ����� PDF �� ��������� �������
            PDDocument document = PDDocument.load(new File("wtf.pdf"));
            if (printAPI) {
                openPDF(distrEditEntity, gdpPrintParam, document);
            } else {
                PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
                PrintRequestAttributeSet attrib = new HashPrintRequestAttributeSet();
                PrintService selectedPrintService =
                        ServiceUI.printDialog(null, 150, 150, printServices, defaultPrintService, null, attrib);
                if (selectedPrintService != null) {
                    logger.debug("selected printer:" + selectedPrintService.getName());

                    PrinterJob job = PrinterJob.getPrinterJob();
                    job.setPageable(new PDFPageable(document));
                    job.setPrintService(selectedPrintService);
                    job.print();

                } else {
                    logger.debug("selection cancelled");
                }
            }
            document.close();
        } else {
            if (oldJob) {
                printJob(distrEditEntity, gdpPrintParam);
            } else {
                printServiceDo(distrEditEntity, gdpPrintParam);
            }
        }
    }

    private static void openPDF(DistrEditEntity distrEditEntity, GDPPrintParam gdpPrintParam, PDDocument document) throws IOException {
        GDPPrintSize printSize = new GDPPrintSize(distrEditEntity, gdpPrintParam);
        printSize.calcPrintSize(841, 595);
        Long pageHeight = printSize.getySize();
        PDPage page = document.getPage(0);
        float crop_width = pageHeight * 2.8346f;
        float crop_height = 842;
        float x_1 = 0;
        float y_1 = 50;
        PDRectangle area = new PDRectangle(x_1, y_1, crop_width, crop_height);
        page.setCropBox(area);
        document.save("print.pdf");

        try {
            File pdfFile = new File("print.pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                } else {
                    logger.error("Awt Desktop is not supported!");
                }
            } else {
                logger.error("File is not exists!");
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    /**
     * ������ ������ ������ ����� PrinterJob + GDPPrintableImpl
     * @param distrEditEntity
     * @param gdpPrintParam
     * @throws PrinterException
     */
    private static void printJob(DistrEditEntity distrEditEntity, GDPPrintParam gdpPrintParam) throws PrinterException {
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setJobName(distrEditEntity.getDistrEntity().getDistrName());

        boolean doPrint = job.printDialog();
        if (doPrint) {
            job.setPrintable(new GDPPrintableImpl(distrEditEntity, gdpPrintParam));
            job.print();
        }
    }

    /**
     * ������ ������ ������ ����� PrintService + GDPPrintableImpl
     * @param distrEditEntity ��� ������� (� ��� �������)
     * @param gdpPrintParam �������������� ��������� � ���� GDPPrintFXDialog ��������� ������ ��� �������
     */
    public static void printServiceDo(DistrEditEntity distrEditEntity, GDPPrintParam gdpPrintParam) throws PrintException, IOException {
        // ������ ��������� �� ����� �������� ������
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        // ������ ������ �� ��������� (��������� ������� �����)
        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
        // ����������, ��� ����� ��������� ��������� ������, ��������� ������������� � ���� ������ ��������
        PrintRequestAttributeSet attrib = new HashPrintRequestAttributeSet();
        attrib.add(OrientationRequested.LANDSCAPE);
        MediaPrintableArea printableAreaManual = new MediaPrintableArea(0.2f, 0.2f, 8.27f, 11.69f, MediaPrintableArea.INCH);
        attrib.remove(MediaPrintableArea.class);
        attrib.add(printableAreaManual);
        // ���� ������ ��������
        PrintService selectedPrintService =
                ServiceUI.printDialog(null, 150, 150, printServices, defaultPrintService, null, attrib);

        if (selectedPrintService != null) {
            logger.debug("selected printer:" + selectedPrintService.getName());

            DocPrintJob printJob = selectedPrintService.createPrintJob();
            printJob.addPrintJobListener(new PrintJobAdapter() {
                public void printDataTransferCompleted(PrintJobEvent event) {
                    logger.debug("data transfer complete");
                }

                public void printJobNoMoreEvents(PrintJobEvent event) {
                    logger.debug("received no more events");
                }
            });

            Doc doc = new SimpleDoc(new GDPPrintableImpl(distrEditEntity, gdpPrintParam),
                    DocFlavor.SERVICE_FORMATTED.PRINTABLE, null);

            attrib.add(new Copies(1));
            printJob.print(doc, attrib);

        } else {
            logger.debug("print selection cancelled");
        }
    }

}
