package com.gmail.alaerof.application;

import com.gmail.alaerof.components.mainframe.gdp.action.GDPActionPanel;
import com.gmail.alaerof.components.menu.MenuApp;
import com.gmail.alaerof.components.menu.MenuCatalog;
import com.gmail.alaerof.components.menu.MenuDistr;
import com.gmail.alaerof.components.menu.MenuGDP;
import com.gmail.alaerof.components.menu.MenuReports;
import com.gmail.alaerof.components.menu.MenuViewConfig;
import com.gmail.alaerof.components.menu.MenuView;
import com.gmail.alaerof.dialog.errormessage.ErrorMessageDialog;
import com.gmail.alaerof.manager.DialogManager;
import com.gmail.alaerof.manager.LocaleManager;

import java.awt.EventQueue;

import java.awt.Label;
import java.io.InputStream;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.Cursor;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;

import javax.swing.JToolBar;
import javax.swing.JButton;

import com.gmail.alaerof.util.ConnectionDBFileHandlerUtil;
import com.gmail.alaerof.util.IconImageUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.components.mainframe.DistrTree;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.components.mainframe.MainPanel;
import com.gmail.alaerof.components.mainframe.StatusBar;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditTabPane;
import com.gmail.alaerof.dialog.config.ApplicationConfig;
import com.gmail.alaerof.dialog.config.ApplicationConfigHandler;
import com.gmail.alaerof.dialog.spantimetown.SpanTimeTownDialog;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.history.ActionTrainEditTime;
import com.gmail.alaerof.history.History;
import com.gmail.alaerof.history.HistoryViewDialog;
import com.gmail.alaerof.history.Action.TypeValue;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.JToggleButton;
import java.awt.Font;
import java.util.Properties;

/**
 * ������� ���� ��� �������-��������
 *
 * @author Helen Yrofeeva
 */
public class GDPEdit {
    private static Logger logger = LogManager.getLogger(GDPEdit.class);
    private static ResourceBundle bundle;
    public static String authorUserName = "";
    public static Cursor waitCursor = new Cursor(Cursor.WAIT_CURSOR);
    public static Cursor defCursor = new Cursor(Cursor.DEFAULT_CURSOR);
    public static StatusBar statusBar = new StatusBar();
    public static GDPEdit gdpEdit;
    private JFrame mainFrame;
    final JSpinner spinnerScale = new JSpinner();
    private GDPActionPanel gdpActionPanel;
    private GDPEditTabPane gdpEditTabPane;
    private DistrTree distrTree;
    private ApplicationConfig applicationConfig;
    private DialogManager dialogManager;

    static {
        bundle = ResourceBundle.getBundle("bundles.GDPEdit", LocaleManager.getLocale());
    }

    {
        applicationConfig = ApplicationConfigHandler.load();
        if (applicationConfig == null) {
            applicationConfig = new ApplicationConfig();
        }
    }

    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }

    private static void setLookAndFeel() {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    UIManager.put("swing.boldMetal", Boolean.FALSE);
                    break;
                }
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    setLookAndFeel();
                    GDPDAOFactoryCreater.initGDPDAOFactory();
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                    // �������� � ������������ � ��, ��������� ���������
                    String message = bundle.getString("gdpedit.dbconnect.warn") + "\n\n" +
                            e.getMessage();
                    ErrorMessageDialog dialog = new ErrorMessageDialog(message);
                    dialog.setModal(true);
                    dialog.setVisible(true);
                    System.exit(0);
                }
                try {
                    gdpEdit = new GDPEdit();
                    gdpEdit.mainFrame.setTitle(getGDPEditTitle());
                    gdpEdit.mainFrame.setVisible(true);
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                    //GDPDAOFactoryCreater.getGDPDAOFactory().closeConnections();
                }
            }
        });
    }

    public static String getGDPEditTitle() {
        Properties gdpEditINI = ConnectionDBFileHandlerUtil.readGDPEditINI();
        // ��� �������� - ��������
        //String title = bundle.getString("gdpedit.title");
        String title = gdpEditINI.getProperty("title");
        String pathDB = "";
        // [����� '������']
        String modeDB = bundle.getString("gdpedit.modeDB.server");
        if (GDPDAOFactoryCreater.getGDPDAOFactory() != null) {
            pathDB = GDPDAOFactoryCreater.getGDPDAOFactory().getPath();
            if (!GDPDAOFactoryCreater.getGDPDAOFactory().isServer()) {
                // [����� '��������� ��']
                modeDB = bundle.getString("gdpedit.modeDB.local");
            }
        }
        // version
        title = title + " " + gdpEdit.getVersion() + " " + modeDB + ": " + pathDB;
        return title;
    }

    /**
     * Get version project from jar
     *
     * <artifactId>gdpedit</artifactId>
     * <version>1.0</version>
     */
    public String getVersion() {
        String version = "";
        try (InputStream is = getClass()
                .getResourceAsStream("/META-INF/maven/" + "by.belsut.uer.gdpedit" + "/"
                        + "gdpedit" + "/pom.properties")) {
            if (is != null) {
                Properties p = new Properties();
                p.load(is);
                version = p.getProperty("version", "").trim();
                if (!version.isEmpty()) {
                    return version;
                }
            }
        } catch (Exception e) {
            // Ignore
        }
        return "";
    }

    /**
     * Create the application
     */
    public GDPEdit() {
        mainFrame = new JFrame("");
        mainFrame.setIconImage(IconImageUtil.getImage(IconImageUtil.MAIN));
        mainFrame.setBounds(100, 100, 2100, 600);
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent ev) {
                mainFrameCloseOperation();
            }
        });

        dialogManager = new DialogManager(mainFrame);

        // ------- Menu ---------
        JMenuBar menuBar = new JMenuBar();
        mainFrame.setJMenuBar(menuBar);
        JMenu mnFile = new MenuApp();
        menuBar.add(mnFile);
        JMenu mnView = new MenuView();
        menuBar.add(mnView);
        JMenu mnSettings = new MenuViewConfig();
        menuBar.add(mnSettings);
        JMenu mnRi = new MenuCatalog();
        menuBar.add(mnRi);
        JMenu mnDistr = new MenuDistr();
        menuBar.add(mnDistr);
        JMenu mnGDP = new MenuGDP();
        menuBar.add(mnGDP);
        JMenu mnReports = new MenuReports();
        menuBar.add(mnReports);

        // ------- Tool bar ---------
        mainFrame.getContentPane().setLayout(new BorderLayout(0, 0));
        JToolBar toolBar = new JToolBar();
        mainFrame.getContentPane().add(toolBar, BorderLayout.NORTH);
        // ������� �� ��
        JButton loadGDP = new JButton(bundle.getString("btn.openFromDB"));
        toolBar.add(loadGDP);
        loadGDP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                loadGDPfromDB();
            }
        });

        Component horizontalStrut_2 = Box.createHorizontalStrut(10);
        toolBar.add(horizontalStrut_2);

        // ��������� � ��
        JButton butSaveGDPtoDB = new JButton(bundle.getString("btn.saveToDB"));
        toolBar.add(butSaveGDPtoDB);
        // ��������� ��� ������� � ��
        butSaveGDPtoDB.setToolTipText(bundle.getString("btn.saveToDB.tooltip"));
        butSaveGDPtoDB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                try {
                    saveGDPtoDB();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(),
                            e.getMessage(), "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        Component horizontalStrut = Box.createHorizontalStrut(20);
        toolBar.add(horizontalStrut);

        JButton buRestoreHistory = new JButton();
        buRestoreHistory.setIcon(IconImageUtil.getIcon(IconImageUtil.RESTORE));
        toolBar.add(buRestoreHistory);
        buRestoreHistory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                performRestoreHistory();
            }
        });

        JButton buHistory = new JButton("*");
        toolBar.add(buHistory);
        buHistory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                performViewHistory();
            }
        });

        JButton buApplyHistory = new JButton();
        buApplyHistory.setIcon(IconImageUtil.getIcon(IconImageUtil.APPLY));
        toolBar.add(buApplyHistory);
        buApplyHistory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performApplyHistory();
            }
        });

        Component horizontalStrut_1 = Box.createHorizontalStrut(20);
        toolBar.add(horizontalStrut_1);

        buildTrainMoveButtonGroup(toolBar);

        // ------- Status bar ---------
        statusBar.setFont(new Font("SansSerif", Font.PLAIN, 12));
        mainFrame.getContentPane().add(statusBar, BorderLayout.SOUTH);

        // ------- MainPanel ---------
        // �������� ���������� �������� ���� :
        // ����� - ������ �������� distrTree
        // ����� - �������� � ��� gdpEditTabPane
        // ������ - �������������� �������� gdpActionPanel
        MainPanel pane = new MainPanel();
        mainFrame.getContentPane().add(pane, BorderLayout.CENTER);
        gdpActionPanel = pane.getGDPActionPanel();
        gdpEditTabPane = pane.getGDPEditTabPane();
        distrTree = pane.getDistrTree();

        // ------- GDP EditTabPane ---------
        buildGDPEditPaneEventListeners();
    }

    /**
     * ���������� ��������� ������� ������ �������� ��� ������� �� ������ �������� ���
     * gdpEditTabPane
     */
    private void buildGDPEditPaneEventListeners() {
        // ��� ������ �������� - ����������� ������ �������, ������ �������, ������ ���������
        gdpEditTabPane.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int indx = gdpEditTabPane.getSelectedIndex();
                if (indx >= 0) {
                    try {
                        mainFrame.setCursor(waitCursor);
                        DistrEditEntity distrEditEntity = getCurrentDistrEditEntity();
                        setCurrentDistrEditEntity(distrEditEntity);
                    } finally {
                        mainFrame.setCursor(defCursor);
                    }
                }
            }
        });
    }

    /**
     * ������ ������� �������+��� ��� ������ �������������� GDPActionPanel � SpanTimeTownDialog
     *
     * @param distrEditEntity {@link DistrEditEntity}
     */
    public void setCurrentDistrEditEntity(DistrEditEntity distrEditEntity) {
        try {
            List<DistrPoint> listDistrPoints = distrEditEntity.getDistrEntity().getListDistrPoint();
            distrEditEntity.updateSpanTime();
            gdpActionPanel.getActionListTrain().setDistrEditEntity(distrEditEntity);
            gdpActionPanel.getActionListSt().setListDistrPoints(listDistrPoints);
            gdpActionPanel.getActionListSpan().setDistrEditEntity(distrEditEntity);
            SpanTimeTownDialog spanTimeTownDialog = dialogManager.getSpanTimeTownDialog(
                    distrEditEntity.getDistrEntity().getDistrName());
            spanTimeTownDialog.setDistrEntity(distrEditEntity.getDistrEntity());
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * ��������� � toolBar
     * ������ ������� ���������� �������
     * ���� ����� � �������� (spinnerMoveTime)
     * � ��������� ���� �����. �������
     *
     * @param toolBar
     */
    private void buildTrainMoveButtonGroup(JToolBar toolBar) {
        final ButtonGroup groupMode = new ButtonGroup();
        final JToggleButton moveMode1 = new JToggleButton();
        // ������������ ������� ���� �����
        moveMode1.setToolTipText(bundle.getString("gdp.movemode01"));
        moveMode1.setIcon(IconImageUtil.getIcon(IconImageUtil.MODE01));
        toolBar.add(moveMode1);
        groupMode.add(moveMode1);
        final JToggleButton moveMode2 = new JToggleButton();
        // ��������� ������� �������� �� �������� � ������������ ��������� �� ����������� ��������� (�� ����������� �������� ������)
        moveMode2.setToolTipText(bundle.getString("gdp.movemode02"));
        moveMode2.setIcon(IconImageUtil.getIcon(IconImageUtil.MODE02));
        toolBar.add(moveMode2);
        groupMode.add(moveMode2);
        final JToggleButton moveMode3 = new JToggleButton();
        // ��������� ������� �������� �� �������� � ������������ ��������� �� ���������� ��������� (�� ����������� �������� ������)
        moveMode3.setToolTipText(bundle.getString("gdp.movemode03"));
        moveMode3.setIcon(IconImageUtil.getIcon(IconImageUtil.MODE03));
        toolBar.add(moveMode3);
        groupMode.add(moveMode3);
        final JToggleButton moveMode4 = new JToggleButton();
        // ��������� ������� �������� �� ���� ����������� ��������� (��� ������������� ��������)
        moveMode4.setToolTipText(bundle.getString("gdp.movemode04"));
        moveMode4.setIcon(IconImageUtil.getIcon(IconImageUtil.MODE04));
        toolBar.add(moveMode4);
        groupMode.add(moveMode4);
        final JToggleButton moveMode5 = new JToggleButton();
        // �������� (��������, ���������) ��������� � ������������ ��������� ��������������� ����� ����� ������
        moveMode5.setToolTipText(bundle.getString("gdp.movemode05"));
        moveMode5.setIcon(IconImageUtil.getIcon(IconImageUtil.MODE05));
        toolBar.add(moveMode5);
        groupMode.add(moveMode5);
        moveMode1.setSelected(true);

        final JSpinner spinnerMoveTime = new JSpinner();
        spinnerMoveTime.setModel(new SpinnerNumberModel(1, -2399, 2399, 1));
        toolBar.add(spinnerMoveTime);
        JButton buttonApplyMoveTime = new JButton("Ok");
        toolBar.add(buttonApplyMoveTime);

        buttonApplyMoveTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int moveMode = 1;
                if (moveMode2.isSelected()) {
                    moveMode = 2;
                }
                if (moveMode3.isSelected()) {
                    moveMode = 3;
                }
                if (moveMode4.isSelected()) {
                    moveMode = 4;
                }
                if (moveMode5.isSelected()) {
                    moveMode = 5;
                }
                int moveTime = (int) spinnerMoveTime.getValue();
                GDPEditPanel ep = getCurrentGDPEditPanel();
                if (ep != null && ep.getCurrentTrain() != null) {
                    TrainThread train = ep.getCurrentTrain();
                    ActionTrainEditTime action = new ActionTrainEditTime(train);
                    // ���������� � ������� �������� ����������� ��������
                    History history = ep.getDistrEditEntity().getHistory();
                    history.addAction(action, TypeValue.oldValue);
                    // ��������� ��������
                    train.move(moveMode, moveTime);
                    // ���������� � ������� �������� ������ ��������
                    history.addAction(action, TypeValue.newValue);
                    gdpActionPanel.getActionListSt().tableRepaint();
                }
            }
        });

        //final JSpinner spinnerScale = new JSpinner();

        Label labelScale = new Label(bundle.getString("gdp.scale2"));
        toolBar.add(labelScale);
        double value = 1;
        double minimum = 0.5;
        double maximum = 1.5;
        double stepSize = 0.1;
        spinnerScale.setModel(new SpinnerNumberModel(value, minimum, maximum, stepSize));
        spinnerScale.setToolTipText(bundle.getString("gdp.scale"));
        toolBar.add(spinnerScale);
        JButton buttonApplyScale = new JButton("Ok");
        buttonApplyScale.setToolTipText(bundle.getString("gdp.applyscale"));
        toolBar.add(buttonApplyScale);

        buttonApplyScale.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.setCursor(waitCursor);
                GDPEditPanel ep = getCurrentGDPEditPanel();
                double mm = (Double) spinnerScale.getValue();
                float ss = (float) mm;
                ep.scaleGDP(ss);
                mainFrame.setCursor(defCursor);
            }
        });
    }

    private void performApplyHistory() {
        try {
            GDPEditPanel ep = getCurrentGDPEditPanel();
            if (ep != null) {
                ep.historyApply();
            }
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }

    private void performViewHistory() {
        try {
            GDPEditPanel ep = getCurrentGDPEditPanel();
            if (ep != null) {
                HistoryViewDialog historyViewDialog = dialogManager.getHistoryViewDialog();
                historyViewDialog.setHistory(ep.getDistrEditEntity().getHistory());
                historyViewDialog.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    private void performRestoreHistory() {
        try {
            GDPEditPanel ep = getCurrentGDPEditPanel();
            if (ep != null) {
                ep.historyRestore();
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * Load GDP from DB
     */
    private void loadGDPfromDB() {
        try {
            GDPEditPanel ep = getCurrentGDPEditPanel();
            if (ep != null) {
                boolean load = true;
                // ��������������
                String title = bundle.getString("warn.title");
                // ��� ��� ��������. ��������� ��� ���?
                String messageWarn = bundle.getString("warn.already.loaded");
                if (ep.listTrainLoaded()) {
                    int n = JOptionPane.showConfirmDialog(mainFrame,
                            messageWarn, title, JOptionPane.YES_NO_OPTION);
                    load = n == 0;
                }
                if (load) {
                    mainFrame.setCursor(waitCursor);
                    long dt1 = Calendar.getInstance().getTimeInMillis();
                    try {
                        ep.loadListDistrTrain();
                    } catch (Exception e) {
                        throw e;
                    } finally {
                        mainFrame.setCursor(defCursor);
                        long dt2 = Calendar.getInstance().getTimeInMillis();
                        double dt = ((double) dt2 - (double) dt1) / 1000.0;
                        // �������� ��� �������
                        String message = bundle.getString("status.loaddb") + " " + dt;
                        //statusBar.setMessage(message);
                        JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), message);
                        logger.debug(ep.getDistrEditEntity().getDistrEntity().getDistrName() + ": " + message);
                    }
                }
            } else {
                DistrEditEntity distrEditEntity = distrTree.getDistrEditEntity();
                if (distrEditEntity != null) {
                    distrEditEntity.loadListDistrTrain(0, 24, null, false);
                    setCurrentDistrEditEntity(distrEditEntity);
                }
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * Save GDP to DB
     */
    private void saveGDPtoDB() {
        try {
            GDPEditPanel ep = getCurrentGDPEditPanel();
            if (ep != null) {
                // ��������������
                String title = bundle.getString("warn.title");
                // ��������� ��� � ��?
                String messageWarn = bundle.getString("warn.save.gdp");
                int n = JOptionPane.showConfirmDialog(mainFrame,
                        messageWarn, title, JOptionPane.YES_NO_OPTION);
                if (n == 0) {
                    mainFrame.setCursor(waitCursor);
                    long dt1 = Calendar.getInstance().getTimeInMillis();
                    // ���������� ��� ������� � ��
                    String status = bundle.getString("status.savedb");
                    //statusBar.setMessage(status);
                    try {
                        ep.clearListDistrTrain(false);
                        ep.saveListDistrTrain(false);
                    } catch (Exception e) {
                        logger.error(e.toString(), e);
                        throw e;
                    } finally {
                        mainFrame.setCursor(defCursor);
                        long dt2 = Calendar.getInstance().getTimeInMillis();
                        double dt = ((double) dt2 - (double) dt1) / 1000.0;
                        String message = status + " " + dt;
                        JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), message);
                        logger.debug(ep.getDistrEditEntity().getDistrEntity().getDistrName() + ": " + message);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * Necessary operation when application close.
     */
    public void mainFrameCloseOperation() {
        try {
            // ������� ���������?
            String message = bundle.getString("gdpedit.exit.message");
            // ������������� ������ �� ���������
            String title = bundle.getString("gdpedit.exit.title");
            int confirm = JOptionPane.showOptionDialog(mainFrame,
                    message, title, JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, null, null);
            if (confirm == JOptionPane.YES_OPTION) {
                for (int i = 0; i < gdpEditTabPane.getTabCount(); i++) {
                    GDPEditPanel ep = gdpEditTabPane.getGDPEditPanelByTabIndex(i);
                    ep.clearHistory();
                }
                GDPDAOFactoryCreater.getGDPDAOFactory().closeConnections();
                // daoFactory.closeOperations();
                logger.debug("GDPEdit.mainFrame is Closed");
                System.exit(1);
            } else {
                logger.debug("GDPEdit.mainFram is Not Closed");
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    /**
     * Returns DistrEditEntity from active GDPEditPanel (tab pane) or null
     *
     * @return {@link DistrEditEntity} or null
     */
    public DistrEditEntity getCurrentDistrEditEntity() {
        DistrEditEntity distrEditEntity = null;
        GDPEditPanel ep = getCurrentGDPEditPanel();
        if (ep != null) {
            distrEditEntity = ep.getDistrEditEntity();
        }
        return distrEditEntity;
    }

    /**
     * Returns current active GDPEditPanel (tab pane) or null
     *
     * @return {@link GDPEditPanel} or null
     */
    public GDPEditPanel getCurrentGDPEditPanel() {
        GDPEditPanel ep = null;
        int indx = gdpEditTabPane.getSelectedIndex();
        if (indx >= 0) {
            ep = gdpEditTabPane.getGDPEditPanelByTabIndex(indx);
        }
        return ep;
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public static void setGDPEdit(GDPEdit gdpEdit) {
        GDPEdit.gdpEdit = gdpEdit;
    }

    public GDPActionPanel getGDPActionPanel() {
        return gdpActionPanel;
    }

    public DistrTree getDistrTree() {
        return distrTree;
    }

    public ApplicationConfig getApplicationConfig() {
        return applicationConfig;
    }

    public DialogManager getDialogManager() {
        return dialogManager;
    }

    public GDPEditTabPane getGDPEditTabPane() {
        return gdpEditTabPane;
    }

    public void setWaitCursor() {
        mainFrame.setCursor(waitCursor);
    }

    public void setDefaultCursor() {
        mainFrame.setCursor(defCursor);
    }

    public void setScale(float scale) {
        this.spinnerScale.setValue((double) scale);
    }
}
