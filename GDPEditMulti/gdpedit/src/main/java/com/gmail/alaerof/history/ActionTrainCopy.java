package com.gmail.alaerof.history;

import com.gmail.alaerof.manager.LocaleManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.entity.train.TrainXMLHandler;

import java.util.ResourceBundle;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ActionTrainCopy extends Action{
    private TrainThread train;
    private DistrEditEntity distrEditEntity;
    private String oldCodeTR;
    private String newCodeTR;
    private static ResourceBundle bundle;

    public ActionTrainCopy(TrainThread train, DistrEditEntity distrEditEntity, String oldCodeTR, String newCodeTR){
        actionName = "CopyTrain";
        this.train = train;
        this.distrEditEntity = distrEditEntity;
        this.oldCodeTR = oldCodeTR;
        this.newCodeTR = newCodeTR;
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.history", LocaleManager.getLocale());
    }

    @Override
    protected void buildElement(Document doc, Element action) {
        Element tr = TrainXMLHandler.saveToXML(train, doc);
        action.appendChild(tr);
    }

    @Override
    protected boolean executeAction(int typeAction, Element oldValue, Element newValue) {
        // ��� oldValue ������ ���� ������ Null 
        // ������ ����������� �������� �������� ������������ ������ �� ������ �������
        if (typeAction == sRestore) {
            distrEditEntity.removeTrain(train);
            GDPEdit.gdpEdit.getGDPActionPanel().getActionListTrain().updateListTrain();
            actionState = sRestore;
            return true;
        }

        // ���������� ����������� �������� ����������� ������������ ������ � ������ �������
        if (typeAction == sApply && newValue != null) {
            distrEditEntity.addTrain(train);
            GDPEdit.gdpEdit.getGDPActionPanel().getActionListTrain().updateListTrain();
            actionState = sApply;
            return true;
        }
       
        return false;
    }

    @Override
    public String getActionDescription() {
        return oldCodeTR + bundle.getString("history.copyto") + newCodeTR;
    }

}
