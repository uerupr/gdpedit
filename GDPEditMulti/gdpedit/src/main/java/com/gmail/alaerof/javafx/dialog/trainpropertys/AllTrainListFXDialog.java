package com.gmail.alaerof.javafx.dialog.trainpropertys;

import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.trainpropertys.view.AllTrainListLayoutController;
import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

public class AllTrainListFXDialog extends SwingFXDialogBase {
    private AllTrainListLayoutController controller;
    public AllTrainListFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/trainpropertys/view/AllTrainListLayout.fxml";
        resourceBundleBaseName = "bundles.AllTrainListFXDialog";
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 740, 500};
        return bounds;
    }

    public void setDialogContent() {

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent();
            }
        });
    }
}
