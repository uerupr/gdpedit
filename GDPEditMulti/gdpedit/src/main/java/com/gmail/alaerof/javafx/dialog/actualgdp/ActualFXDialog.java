package com.gmail.alaerof.javafx.dialog.actualgdp;

import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.actualgdp.view.ActualLayoutController;

import java.awt.Frame;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;


/**
 * FX Actual Graph
 */
public class ActualFXDialog extends SwingFXDialogBase {

    private DistrEditEntity distrEntity;
    private ActualLayoutController controller;

    public ActualFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/actualgdp.view/ActualGDPLayout.fxml";
        resourceBundleBaseName = "bundles.ActualFXDialog";
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    public void setDialogContent(final DistrEditEntity distrEditEntity) {
        this.distrEntity = distrEditEntity;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(distrEntity);
            }
        });
    }

}
