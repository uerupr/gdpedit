package com.gmail.alaerof.javafx.dialog.gdpprint.view;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.javafx.dialog.TaskWithCursorExecutor;
import com.gmail.alaerof.printable.GDPPrintSize;
import com.gmail.alaerof.util.ListTextPrintParamComparator;
import com.gmail.alaerof.dao.dto.TextPrintParam;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.GDPPrintParam;
import com.gmail.alaerof.javafx.dialog.gdpprint.GDPPrintTextParamFXDialog;
import com.gmail.alaerof.util.FileUtil;
import java.awt.Frame;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.Paper;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.ComboBox;
import javafx.stage.FileChooser;

import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.SwingUtilities;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by Paul M. Bui
 */
public class GDPPrintLayoutController extends Frame implements Initializable {
    protected Logger logger = LogManager.getLogger(GDPPrintLayoutController.class);
    protected ResourceBundle bundle;
    private GDPEditPanel editPanel;
    private DistrEditEntity distrEntity;
    private Scene scene;

    @FXML
    private RadioButton rbFirst;
    @FXML
    private RadioButton rbSecond;
    @FXML
    private RadioButton rbTherd;
    @FXML
    private CheckBox cbLeftPanel;
    @FXML
    private CheckBox cbRightPanel;
    @FXML
    private CheckBox cbTopPanel;
    @FXML
    private CheckBox cbButtomPanel;
    @FXML
    private CheckBox cbSaveProportial;
    @FXML
    private Label stPrinterName;
    @FXML
    private Label stPrinterOrientation;
    @FXML
    private Label stPrinterWidth;
    @FXML
    private Label stPrinterHeight;
    @FXML
    private Spinner<Integer> spBeginTime;
    @FXML
    private Spinner<Integer> spEndTime;
    @FXML
    private Spinner<Integer> spSkail;
    @FXML
    private TextField tfHeight;
    @FXML
    private TextField tfWidth;
    @FXML
    private ComboBox<String> cbFormat;
    final static private Integer initialBeginValue = 0;
    final static private Integer initialEndValue = 24;
    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize: location = " +location + " resources = " + resources);
        bundle = resources;
        // ���� ������ � �����
        SpinnerValueFactory<Integer> valueBeginFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,24,initialBeginValue);
        SpinnerValueFactory<Integer> valueEndFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,24,initialEndValue);
        spBeginTime.setValueFactory(valueBeginFactory);
        spEndTime.setValueFactory(valueEndFactory);
        // �������
        SpinnerValueFactory<Integer> valueSkailFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(50,600,100,10);
        spSkail.setValueFactory(valueSkailFactory);
        // �������������� �������
        tfHeight.setText("205");
        tfWidth.setText("288");
        // ����� �������
        ObservableList<String> formats = FXCollections.observableArrayList(
                bundle.getString("javafx.dialog.gdpprint.titledpane2.auto"),
                Paper.A4.getName(),
                Paper.A3.getName(),
                Paper.A2.getName(),
                Paper.A1.getName(),
                Paper.A0.getName()
                );
        cbFormat.setItems(formats);
        cbFormat.getSelectionModel().select(0);
    }

    @FXML
    private void printToPrinter(ActionEvent event){
        logger.debug("printToPrinter");
        GDPPrintParam gdpPrintParam = getGDPPrintParam();
        // �������� �� ��������
        try {
            if (editPanel != null) {
                Task task = new Task<Void>() {
                    @Override
                    protected Void call() {

                        try {
                            editPanel.printGDP(gdpPrintParam);
                        } catch (Exception e) {
                            logger.error(e.toString(), e);
                            throw  new RuntimeException(e);
                        } finally {
                            distrEntity.setNoJoint();
                        }
                        return null;
                    }
                };
//                TaskWithCursorExecutor.executeTaskWithCursorAndAlert(task, scene, () -> {});
                ExecutorService service = Executors.newFixedThreadPool(1);
                service.execute(task);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    @FXML
    private void printToFile(ActionEvent event){
        logger.debug("printToFile");
        GDPPrintParam gdpPrintParam = getGDPPrintParam();
        scene.setCursor(Cursor.WAIT);
        // �������� � ����
        try {
            if (editPanel != null) {
                String fileName = distrEntity.getDistrEntity().getDistrName() + ".png";
                File file = FileUtil.chooseFile(FileUtil.Mode.save,null, fileName, null,
                        new FileChooser.ExtensionFilter("PNG files", "*.png"));
                if (file != null) {
                    editPanel.printGDP(file, gdpPrintParam);
                    distrEntity.setNoJoint();
                }
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }
    @FXML
    private void textParam(ActionEvent event){
        logger.debug("textParam");
        scene.setCursor(Cursor.WAIT);
        // ����� � �������
        try {
            if (editPanel != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        GDPPrintTextParamFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getGDPPrintTextParamFXDialog();
                        dialog.setModal(true);
                        dialog.setDialogContent(editPanel);
                        dialog.setVisible(true);
                    }
                });
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        } finally {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    @FXML
    private void printerParam(ActionEvent event){
        logger.debug("printerParam");
        // �������
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        if (printerJob.printDialog()) {
            PrintService service = printerJob.getPrintService();
            //printerJob.pageDialog(pras);
            // �������� ��������
            stPrinterName.setText(service.toString());
            // ������ ���������
            PageFormat pageFormat = printerJob.getPageFormat(pras);

            GDPPrintParam gdpPrintParam = getGDPPrintParam();
            GDPPrintSize printSize = new GDPPrintSize(distrEntity, gdpPrintParam);
            printSize.calcPrintSize(pageFormat.getWidth(), pageFormat.getHeight());
            Long pageWidth = printSize.getxSize();
            Long pageHeight = printSize.getySize();

            stPrinterHeight.setText(pageHeight.toString());
            stPrinterWidth.setText(pageWidth.toString());
            if (pageFormat.getHeight() > pageFormat.getWidth()) {
                stPrinterOrientation.setText(bundle.getString("javafx.dialog.gdpprint.orientation.book"));
            }
            else {
                stPrinterOrientation.setText(bundle.getString("javafx.dialog.gdpprint.orientation.albom"));
            }

            /*if (pageFormat.getOrientation() == PORTRAIT) {
                stPrinterOrientation.setText(bundle.getString("javafx.dialog.gdpprint.orientation.book"));
            }
            if (pageFormat.getOrientation() == LANDSCAPE) {
                stPrinterOrientation.setText(bundle.getString("javafx.dialog.gdpprint.orientation.albom"));
            }*/
        };
    }

    /**
     * ��������� gdpPrintParam ���������� � �����
     * @return GDPPrintParam
     */
    private GDPPrintParam getGDPPrintParam(){
        GDPPrintParam gdpPrintParam = new GDPPrintParam();
        gdpPrintParam.setHourBeg(spBeginTime.getValue());
        gdpPrintParam.setHourEnd(spEndTime.getValue());
        gdpPrintParam.setLeftPanel(cbLeftPanel.isSelected());
        gdpPrintParam.setRightPanel(cbRightPanel.isSelected());
        gdpPrintParam.setTopPanel(cbTopPanel.isSelected());
        gdpPrintParam.setButtomPanel(cbButtomPanel.isSelected());
        switch (cbFormat.getSelectionModel().getSelectedIndex()) {
            case 0: gdpPrintParam.setFormat(null); break;
            case 1: gdpPrintParam.setFormat(Paper.A4); break;
            case 2: gdpPrintParam.setFormat(Paper.A3); break;
            case 3: gdpPrintParam.setFormat(Paper.A2); break;
            case 4: gdpPrintParam.setFormat(Paper.A1); break;
            case 5: gdpPrintParam.setFormat(Paper.A0); break;
        }
        if (rbTherd.isSelected()){
            gdpPrintParam.setScallingType(3);
        }
        if (rbSecond.isSelected()){
            gdpPrintParam.setScallingType(2);
        }
        if (rbFirst.isSelected()){
            gdpPrintParam.setScallingType(1);
        }
        gdpPrintParam.setSaveProportionl(cbSaveProportial.isSelected());
        gdpPrintParam.setScale(spSkail.getValue());
        gdpPrintParam.setHeight(Integer.parseInt(tfHeight.getText()));
        gdpPrintParam.setWidth(Integer.parseInt(tfWidth.getText()));
        gdpPrintParam.clearHeadTextFields();
        gdpPrintParam.clearBossTextFields();
        gdpPrintParam.clearWorkerTextFields();

        // ��������� �� ��������� �� ��������� ��������� ����� ����� � ��������
        try {
            List<TextPrintParam> listTextPrintParam = GDPDAOFactoryCreater.getGDPDAOFactory().getTextPrintParamDAO().getListTextPrintParam(distrEntity.getIdDistr());
            // ���������� listTextPrintParam �� tpType � �� num
            Collections.sort(listTextPrintParam, new ListTextPrintParamComparator());
            // ���������� �����
            if (listTextPrintParam.size() > 0){
                for (int i = 0; i< 4; i++){
                    gdpPrintParam.addHeadTextFields(listTextPrintParam.get(i));
                }
                for (int i = 4; i< 8; i++){
                    gdpPrintParam.addBossTextFields(listTextPrintParam.get(i));
                }
                for (int i = 8; i< 10; i++){
                    gdpPrintParam.addWorkerTextFields(listTextPrintParam.get(i));
                }
            }
        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
        return gdpPrintParam;
    }

    public void setContent(DistrEditEntity distrEntity, GDPEditPanel editPanel) {
        logger.debug("setContent");
        this.distrEntity = distrEntity;
        this.editPanel = editPanel;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
