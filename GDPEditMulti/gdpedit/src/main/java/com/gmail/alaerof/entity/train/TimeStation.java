package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.Color;

import com.gmail.alaerof.dao.dto.TrainCombSt;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.entity.DistrLineSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.util.TimeConverter;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class TimeStation {
    public enum DirectionVert {
        onFromTop, onFromBottom, offToTop, offToBottom, noDirection
    }
    // 0 - before, 1 - after
    // ������� ������� � ����� ��� � ������ ��������, �� ���� ���� ��� �� ��� ����� �������
    private int clickPos = 0;

    /** ������ �� ������� ������� */
    // idSt, y0, BR, ������ �����
    DistrStation station;
    /** ������ �� ���������� �� ������� */
    // idSt, tOn, tOff, tStop, tPL, sk_ps, texStop, idLine, overTime
    LineStTrain lineStTrain;

    /** �������� � ������� */
    int mOn;
    /** ����������� � ������� */
    int mOff;
    /** ����� �� � �������� �� ������� (��� �����) */
    int xOn;
    /** ����� �� � ����������� �� ������� (��� �����) */
    int xOff;
    /** �������� � ������� */
    int mOnTexStop;
    /** ����������� � ������� */
    int mOffTexStop;
    /** ����� �� � �������� �� ������� (��� �����) */
    int xOnTexStop;
    /** ����� �� � ����������� �� ������� (��� �����) */
    int xOffTexStop;
    /** ��� �������� �� �� �������� �� ������� �� ���� */
    boolean nullOn;
    /** ��� �������� �� �� ����������� �� ������� �� ���� */
    boolean nullOff;
    /** ���� ������ ����� ������� (���� null, �� ����� ���� �����) */
    Color colorTR;
    /** ������� ��������� ����� ����� ������� */
    int showCode = -1;
    // -1 - ��� �������;
    /** �������� ������ ������� */
    String ainscr;
    // 2 - AI_sp (������ �������);
    /** ���������� ��������� ����� � ������� � ���� ������� */
    boolean hideSpan;
    // 1 - AI_all (����� �������);
    /** ���������� �������������� ����� � ������� � ���� ������� */
    boolean hideHor;
    // 0 - Code (�����);
    /** ���������� ����� ����� �.�. ��� ������ �� ���� ��������, ���������� � 0 */
    int order;
    boolean last;
    /** ������ �� ������� */
    private TrainCombSt trainCombSt; // ������ �������� �����������

    // ��������� � �������������� ����� ����� ����� ������
    // ����� ����� �����
    /** ��� ��������� ����� � ������� � ���� ������� */
    LineStyle lineStyleSpan;
    /** ��� �������������� ����� � ������� � ���� ������� */
    LineStyle lineStyleHor;

    public DistrStation getDistrStation() {
        return station;
    }

    public LineStTrain getLineStTrain() {
        return lineStTrain;
    }

    public int getXOn() {
        return xOn;
    }

    public int getMOn() {
        return mOn;
    }

    public int getMOff() {
        return mOff;
    }

    public boolean isNullOn() {
        return nullOn;
    }

    public boolean isNullOff() {
        return nullOff;
    }

    public boolean isLast() {
        return last;
    }

    public boolean isFirst() {
        return order == 0;
    }

    public static DirectionVert getOnDirection(TimeStation timeSt, TimeStation timeStBefore) {
        if (timeStBefore != null) {
            if (timeSt.station.getY1() > timeStBefore.station.getY1()) {
                return DirectionVert.onFromTop;
            } else {
                return DirectionVert.onFromBottom;
            }
        } else {
            return DirectionVert.noDirection;
        }
    }

    public static DirectionVert getOffDirection(TimeStation timeSt, TimeStation timeStAfter) {
        if (timeStAfter != null) {
            if (timeSt.station.getY1() < timeStAfter.station.getY1()) {
                return DirectionVert.offToBottom;
            } else {
                return DirectionVert.offToTop;
            }
        } else {
            return DirectionVert.noDirection;
        }
    }

    /**
     * TexOn - ����� ������ � �����
     * @return ������� ����, ��� ���� ����������� ������� �� ��������� ������� �� ��������
     */
    public boolean isTexOn() {
        // ���� ���� ����� �� ��� �������, ���� ��� ��������� ������� � ����� ����������� ���� null
        if ((lineStTrain.getTexStop() > 0) && isLast() && nullOff) {
            return true;
        }
        return false;
    }

    /**
     * TexOff - ����� ����� � ������������
     * @return ������� ����, ��� ���� ����������� ������� �� ������ ������� �� �����������
     */
    public boolean isTexOff(){
        // ���� ���� ����� �� ��� �������, ���� ��� ������ ������� � ����� �������� ���� null
        if ((lineStTrain.getTexStop() > 0) && isFirst() && nullOn) {
            return true;
        }
        return false;
    }

    public TimeStation(DistrStation station, LineStTrain lineStTrain) {
        this.station = station;
        this.lineStTrain = lineStTrain;
    }

    public TimeStation(DistrStation station, TrainCombSt trainCombSt) {
        this.station = station;
        this.trainCombSt = trainCombSt;
    }

    private int getHour(String tm) {
        int h = -1;
        if (tm != null) {
            String ss[] = tm.split(":");
            h = Integer.parseInt(ss[0]);
        }
        return h;
    }

    public int getHourOn() {
        if (trainCombSt != null) {
            return getHour(trainCombSt.occupyOn);
        } else {
            return getHour(lineStTrain.getOccupyOn());
        }
    }

    public int getHourOff() {
        if (trainCombSt != null) {
            return getHour(trainCombSt.occupyOff);
        } else {
            return getHour(lineStTrain.getOccupyOff());
        }
    }

    public String getLineName(int idLineSt) {
        return station.getLineName(idLineSt);
    }

    public DistrLineSt getLine(int idLineSt) {
        return station.getLine(idLineSt);
    }

    public int getLineCount() {
        return station.getListLines().size();
    }

    @Override
    public String toString() {
        if (lineStTrain != null) {
            return "TimeStation [tOn = " + lineStTrain.getOccupyOn() + "; tOff = " + lineStTrain.getOccupyOff()
                    + "; station=" + station.toString() + "]";
        } else {
            if (trainCombSt != null) {
                return "TimeStation [tOn = " + trainCombSt.occupyOn + "; tOff = " + trainCombSt.occupyOff
                        + "; station=" + station.toString() + "]";
            } else {
                return "TimeStation [station=" + station.toString() + "]";
            }
        }
    }

    public TrainCombSt getTrainCombSt() {
        return trainCombSt;
    }

    public void setTrainCombSt(TrainCombSt trainCombSt) {
        this.trainCombSt = trainCombSt;
    }

    /**
     * �������� ����� �������� �/��� ����������� �� �������� ���������� �����
     * 
     * @param moveTime   �������� ���������� �����
     * @param generalParams
     * @param conf
     * @param oneTime   0 - ���, 1 - ������ ��������, 2 - ������ �����������
     */
    public void moveTime(int moveTime, TrainThreadGeneralParams generalParams, GDPGridConfig conf, int oneTime) {
        if (oneTime != 2) {
            mOn = TimeConverter.addMinutes(moveTime, mOn);
        }
        if (oneTime != 1) {
            mOff = TimeConverter.addMinutes(moveTime, mOff);
        }
        String tOn = TimeConverter.minutesToTime(mOn);
        String tOff = TimeConverter.minutesToTime(mOff);
        lineStTrain.setOccupyOn(tOn);
        lineStTrain.setOccupyOff(tOff);
        xOn = TimeConverter.minutesToX(mOn, generalParams, conf);
        xOff = TimeConverter.minutesToX(mOff, generalParams, conf);
        if(mOn!=mOff){
            int tStop = TimeConverter.minutesBetween(mOn, mOff);
            lineStTrain.setTstop(tStop);
        }
        updatePositionTexStop(generalParams, conf);
    }
    
    public void updatePositionTexStop(TrainThreadGeneralParams generalParams, GDPGridConfig conf){
        if (lineStTrain.getTexStop() > 0) {
            if (nullOn) {
                mOffTexStop = mOff;
                mOnTexStop = TimeConverter.addMinutes(mOffTexStop, -lineStTrain.getTexStop());
                xOnTexStop = TimeConverter.minutesToX(mOnTexStop, generalParams, conf);
                xOffTexStop = TimeConverter.minutesToX(mOffTexStop, generalParams, conf);
            }
            if (nullOff) {
                mOnTexStop = mOn;
                mOffTexStop = TimeConverter.addMinutes(mOnTexStop, lineStTrain.getTexStop());
                xOnTexStop = TimeConverter.minutesToX(mOnTexStop, generalParams, conf);
                xOffTexStop = TimeConverter.minutesToX(mOffTexStop, generalParams, conf);
            }
        }
    }

    public int getClickPos() {
        return clickPos;
    }

    public void setClickPos(int clickPos) {
        this.clickPos = clickPos;
    }

}
