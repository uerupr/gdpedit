package com.gmail.alaerof.components.mainframe.gdp.edit;

import com.gmail.alaerof.components.mainframe.gdp.action.GDPActionPanel;
import com.gmail.alaerof.components.tab.ButtonTabPane;
import com.gmail.alaerof.components.tab.TitledTab;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrEditEntity;

/**
 * ������ ��� ��� �������� GDPEditTab
 * @author Helen Yrofeeva
 * 
 */
public class GDPEditTabPane extends ButtonTabPane {
    private static final long serialVersionUID = 1L;
    private GDPActionPanel gdpActionPanel;

    public GDPEditTabPane(GDPActionPanel gdpActionPanel) {
        List<TitledTab> listC = new ArrayList<>();
        super.initTabs(listC);
        this.gdpActionPanel = gdpActionPanel;
        final GDPEditTabPane etp = this;
        this.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent arg0) {
                int index = etp.getSelectedIndex();
                if (index > -1) {
                    GDPEditPanel ep = getGDPEditPanelByTabIndex(index);
                    GDPEdit.statusBar.setMessage(ep.getDistrEditEntity().getSoureGDP());
                    float scale = ep.getDistrEditEntity().getDistrEntity().getConfigGDP().scale;
                    GDPEdit.gdpEdit.setScale(scale);
                }
            }
        });

    }

    public void addGDPEditTab(DistrEditEntity distr) {
        boolean ins = true;
        for (int i = 0; i < this.getTabCount(); i++) {
            GDPEditPanel ep = getGDPEditPanelByTabIndex(i);
            if (ep.getIdDistr() == distr.getIdDistr()) {
                ins = false;
                setSelectedIndex(i);
            }
        }
        if (ins) {
            GDPEditPanel newEditPanel = new GDPEditPanel(distr, gdpActionPanel);
            add(distr.toString(), newEditPanel.getContent());
            initTabComponent(this.getTabCount() - 1, newEditPanel.getActionOnClose());
            setSelectedIndex(this.getTabCount() - 1);
            float scale = distr.getDistrEntity().getConfigGDP().scale;
            GDPEdit.gdpEdit.setScale(scale);

            if(distr.listTrainLoaded()){
                setCursor(GDPEdit.waitCursor);
                try {
                    distr.makeTrainsVisible(newEditPanel.getLineListener(), newEditPanel.getContentGDPpanel());
                }finally {
                    setCursor(GDPEdit.defCursor);
                }
            }
        }
    }

    public GDPEditPanel getGDPEditPanelByTabIndex(int index) {
        GDPEditPanel ep = null;
        Component item = this.getComponentAt(index);
        JSplitPane sp = (JSplitPane) item;
        //sp = (JSplitPane) sp.getBottomComponent();
        item = sp.getBottomComponent();
        //ep = (GDPEditPanel) ((JScrollPane) item).getViewport().getComponent(0);

        //+++++++++++++++++++++++++
        JScrollPane scp = (JScrollPane)((JPanel) item).getComponent(1);
        ep = (GDPEditPanel) scp.getViewport().getComponent(0);

        //++++++++++++++++++++++++++
        return ep;
    }
}
