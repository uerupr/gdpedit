package com.gmail.alaerof.components.mainframe.gdp.action.listst;

import java.util.regex.Pattern;

/**
 * ����� ������������ ��� �������� ������������ ����� ������� � ����������
 * @author Helen Yrofeeva
 *
 */
public class TimeSt {
    private String time;

    public TimeSt() {
    }

    public TimeSt(String time) throws TimeStException {
        setTime(time);
    }

    public String getTime() {
        return time;
    }

    public static int checkTime(String time){
        //Result: 1 - good time; 2 - empty (null); 3 - bad time (error)
        String hh = null;
        String nn = null;

        if (time != null) {
            if (time.equals("")){
                return 2;
            }
            if (Pattern.matches("\\d\\d:\\d\\d", time)) {
                String s[] = time.split(":");
                s[0] = s[0].trim();
                s[1] = s[1].trim();
                if (s[0].length() == 0 || s[1].length() == 0) {
                    return 3;
                }
                hh = s[0];
                nn = s[1];
            } else {
                if (Pattern.matches("\\d{4}", time)) {
                    if (time.length() != 4) {
                        return 3;
                    }
                    hh = time.substring(0, 2);
                    nn = time.substring(2, 4);
                } else
                    return 3;
            }
            int h = 0;
            int n = 0;
            try {
                h = Integer.parseInt(hh);
                n = Integer.parseInt(nn);
            } catch (Exception e) {
                return 3;
            }
            if (h > 23 || n > 59) {
                return 3;
            }
            hh = "" + h;
            nn = "" + n;
            if (h < 10) {
                hh = "0" + hh;
            }
            if (n < 10) {
                nn = "0" + nn;
            }
            time = hh + ":" + nn;
        }
        return 1;
    }
    /**
     * ��������� �� ������ � ��������� ���������� �������������� ����� ���
     * �����������
     * 
     * @param time
     * @throws TimeStException
     */
    public void setTime(String time) throws TimeStException {
        String res = null;
        String hh = null;
        String nn = null;

        if (time != null) {
            res = time.trim();
            if (Pattern.matches("\\d\\d:\\d\\d", res)) {
                String s[] = res.split(":");
                s[0] = s[0].trim();
                s[1] = s[1].trim();
                if (s[0].length() == 0 || s[1].length() == 0) {
                    throw new TimeStException(time);
                }
                hh = s[0];
                nn = s[1];
            } else {
                if (Pattern.matches("\\d{4}", res)) {
                    if (time.length() != 4) {
                        throw new TimeStException(time);
                    }
                    hh = time.substring(0, 2);
                    nn = time.substring(2, 4);
                } else
                    throw new TimeStException(time);
            }
            int h = 0;
            int n = 0;
            try {
                h = Integer.parseInt(hh);
                n = Integer.parseInt(nn);
            } catch (Exception e) {
                throw new TimeStException(time, e);
            }
            if (h > 23 || n > 59) {
                throw new TimeStException(time);
            }
            hh = "" + h;
            nn = "" + n;
            if (h < 10) {
                hh = "0" + hh;
            }
            if (n < 10) {
                nn = "0" + nn;
            }
            res = hh + ":" + nn;

        }

        this.time = res;
    }

    @Override
    public String toString() {
        return time;
    }
    
}
