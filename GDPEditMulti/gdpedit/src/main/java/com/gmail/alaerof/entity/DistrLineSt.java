package com.gmail.alaerof.entity;

import com.gmail.alaerof.dao.dto.LineSt;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class DistrLineSt {
    private LineSt lineSt;
    private int y0;// ���������� ���������� ����� ���� ������� � �������� ��� �����
    // ������� ������ (��� ���������������)
    private int y1;// ���������� ���������� ����� ���� ������� � �������� c ������
    // ������� ������ �� ��� (� ����������������)
    
    public DistrLineSt(LineSt lineSt){
        this.lineSt = lineSt;
    }
    
    public int getY0() {
        return y0;
    }
    public void setY0(int y0) {
        this.y0 = y0;
    }
    public int getY1() {
        return y1;
    }
    public void setY1(int y1) {
        this.y1 = y1;
    }

    public LineSt getLineSt() {
        return lineSt;
    }

    @Override
    public String toString() {
        return lineSt.getPs();
    }

}
