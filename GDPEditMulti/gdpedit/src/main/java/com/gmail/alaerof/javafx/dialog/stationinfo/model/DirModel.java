package com.gmail.alaerof.javafx.dialog.stationinfo.model;

import com.gmail.alaerof.dao.dto.Dir;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DirModel {
    // Dir
    private Dir dir;
    // N
    private StringProperty nameDir;

    public DirModel(Dir dir) {
        this.dir = dir;
        this.nameDir = new SimpleStringProperty(dir.getName());
    }

    public Dir getDir() {
        return dir;
    }

    public String getNameDir() {
        return nameDir.get();
    }

    public StringProperty nameDirProperty() {
        return nameDir;
    }

    public void setDir(Dir dir) {
        this.dir = dir;
    }

    public void setNameDir(String nameDir) {
        this.nameDir.set(nameDir);
    }

    @Override
    public String toString() {
        return nameDirProperty().get();
    }
}
