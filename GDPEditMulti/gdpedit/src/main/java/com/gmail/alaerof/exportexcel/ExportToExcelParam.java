package com.gmail.alaerof.exportexcel;


import java.awt.Color;

public class ExportToExcelParam {
    private String trainType;        // ��� �������
    private int trainTCount;         // ���������� ������� � ����� �������
    private boolean ESR;             // ����� ���� ���
    private boolean Express;         // ����� ���� ��������
    private boolean uslDl;           // ����� �������� ����� �������
    private boolean roundKm;         // ���������� ��
    private boolean sortTimeMuve;    // ���������� �� ������� ����� �� �������
    private boolean dopParam;        // �������������� ���������
    private boolean rp;              // ��������� �.�.
    private boolean pohode;          // �� ���� ��������
    private boolean obgon;           // ������
    private boolean autoobgon;       // ��������� ��� ������� ����� 3 �����
    private String textPt;           // ����� ��� �� = * (������)
    private String textPtW;          // ����� ��� �� = w
    private boolean edinNomer;       // ���� ������ � ����������� �������� (��������)
    private Color holidayTrainColor; // ���� ������� ��������� ���

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public void setTrainTCount(int trainTCount) {
        this.trainTCount = trainTCount;
    }

    public void setESR(boolean ESR) {
        this.ESR = ESR;
    }

    public void setExpress(boolean express) {
        Express = express;
    }

    public void setUslDl(boolean uslDl) {
        this.uslDl = uslDl;
    }

    public void setRoundKm(boolean roundKm) {
        this.roundKm = roundKm;
    }

    public void setSortTimeMuve(boolean sortTimeMuve) {
        this.sortTimeMuve = sortTimeMuve;
    }

    public void setDopParam(boolean dopParam) {
        this.dopParam = dopParam;
    }

    public void setRp(boolean rp) {
        this.rp = rp;
    }

    public void setPohode(boolean pohode) {
        this.pohode = pohode;
    }

    public void setObgon(boolean obgon) {
        this.obgon = obgon;
    }

    public void setEdinNomer(boolean edinNomer) {
        this.edinNomer = edinNomer;
    }

    public void setAutoobgon(boolean autoobgon) {
        this.autoobgon = autoobgon;
    }

    public void setTextPt(String textPt) {
        this.textPt = textPt;
    }

    public void setTextPtW(String textPtW) {
        this.textPtW = textPtW;
    }

    public void setHolidayTrainColor(Color holidayTrainColor) {
        this.holidayTrainColor = holidayTrainColor;
    }

    public String getTrainType() {

        return trainType;
    }

    public int getTrainTCount() {
        return trainTCount;
    }

    public boolean isESR() {
        return ESR;
    }

    public boolean isExpress() {
        return Express;
    }

    public boolean isUslDl() {
        return uslDl;
    }

    public boolean isRoundKm() {
        return roundKm;
    }

    public boolean isSortTimeMuve() {
        return sortTimeMuve;
    }

    public boolean isDopParam() {
        return dopParam;
    }

    public boolean isRp() {
        return rp;
    }

    public boolean isPohode() {
        return pohode;
    }

    public boolean isObgon() {
        return obgon;
    }

    public boolean isEdinNomer() {
        return edinNomer;
    }

    public boolean isAutoobgon() {
        return autoobgon;
    }

    public String getTextPt() {
        return textPt;
    }

    public String getTextPtW() {
        return textPtW;
    }

    public Color getHolidayTrainColor() {
        return holidayTrainColor;
    }
}
