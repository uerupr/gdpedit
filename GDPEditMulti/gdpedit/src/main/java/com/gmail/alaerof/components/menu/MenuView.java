package com.gmail.alaerof.components.menu;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.action.GDPActionPanel;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrPoint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JMenuItem;

public class MenuView extends LoggerMenu {
    public MenuView() {
        super();
        this.setText(bundle.getString("components.menu.view"));
        {
            JMenuItem menuItem = new JMenuItem(bundle.getString("components.menu.view.schedule"));
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    showActionListStTab();
                }
            });
        }

        {
            JMenuItem menuItem = new JMenuItem(bundle.getString("components.menu.view.stages"));
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    showActionListSpanTab();
                }
            });
        }
    }

    private void showActionListSpanTab() {
        try {
            GDPActionPanel gdpActionPanel = GDPEdit.gdpEdit.getGDPActionPanel();
            gdpActionPanel.addActionListSpanTab();
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                gdpActionPanel.getActionListSpan().setDistrEditEntity(editPanel.getDistrEditEntity());
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }


    private void showActionListStTab() {
        try {
            GDPActionPanel actionTabPane = GDPEdit.gdpEdit.getGDPActionPanel();
            actionTabPane.addActionListStTab();
            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
            if (dee != null) {
                List<DistrPoint> listDistrPoints = dee.getDistrEntity().getListDistrPoint();
                actionTabPane.getActionListSt().setListDistrPoints(listDistrPoints);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
