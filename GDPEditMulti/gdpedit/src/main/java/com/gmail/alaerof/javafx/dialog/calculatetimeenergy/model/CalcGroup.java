package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

/**
 * ���������� ������� �� ������ �������: �����, ���, �����
 */
public class CalcGroup {
    private CalcModel allCalc;
    private CalcModel oddCalc;
    private CalcModel evenCalc;
    private ExpenseRateEn expenseRateEn;
    private ExpenseRateModel expenseRate;

    public CalcGroup(CalcModel allCalc) {
        this.allCalc = allCalc;
    }

    public CalcModel getAllCalc() {
        return allCalc;
    }

    public CalcModel getOddCalc() {
        return oddCalc;
    }

    public void setOddCalc(CalcModel oddCalc) {
        this.oddCalc = oddCalc;
    }

    public CalcModel getEvenCalc() {
        return evenCalc;
    }

    public void setEvenCalc(CalcModel evenCalc) {
        this.evenCalc = evenCalc;
    }

    public ExpenseRateEn getExpenseRateEn() {
        return expenseRateEn;
    }

    public void setExpenseRateEn(ExpenseRateEn expenseRateEn) {
        this.expenseRateEn = expenseRateEn;
    }

    public ExpenseRateModel getExpenseRate() {
        return expenseRate;
    }

    public void setExpenseRate(ExpenseRateModel expenseRate) {
        this.expenseRate = expenseRate;
    }
}
