package com.gmail.alaerof.entity.train;

import java.awt.Color;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class NewLineSelfParams {
    /** ���� ����� */
    public Color color = Color.blue;
    /** ����� ����� */
    public LineStyle lineStyle = LineStyle.Solid;
    public int lineWidth = 1;
    /** ������� ����� (������� - �����) */
    private String captionValue = "----";
    /** ������� - ����� �������� */
    public String timeA = "";
    /** ������� - ����� ����������� */
    public String timeL = "";
    //public String trainCode = "";
 

    public String getCaptionValue() {
        return captionValue;
    }

    public void setCaptionValue(String captionValue) {
        if (captionValue != null)
            this.captionValue = captionValue;
    }

}
