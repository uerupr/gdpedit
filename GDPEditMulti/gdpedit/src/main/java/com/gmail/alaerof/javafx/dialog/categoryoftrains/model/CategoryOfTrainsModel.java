package com.gmail.alaerof.javafx.dialog.categoryoftrains.model;

import com.gmail.alaerof.dao.dto.Category;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.awt.Color;

import static com.gmail.alaerof.dao.dto.gdp.TrainType.gr;

public class CategoryOfTrainsModel {
    //Category
    private Category category;
    //�������������
    private IntegerProperty idCat;
    // ������������
    private StringProperty name;
    // ���������, ��� ������ ��������, ��� ������
    private IntegerProperty priority;
    // c �
    private IntegerProperty trMin;
    // �� �
    private IntegerProperty trMax;
    // ��������� (��������� ��������: ����, ��, ����)
    private StringProperty type;
    // ����
    private ObjectProperty color;

    public CategoryOfTrainsModel(Category category) {
        this.category = category;
        if (category.getType() == null) {
            category.setType(gr);
        }
        this.idCat = new SimpleIntegerProperty(category.getIdCat());
        this.name = new SimpleStringProperty(category.getName());
        this.priority = new SimpleIntegerProperty(category.getPriority());
        this.trMin = new SimpleIntegerProperty(category.getTrMin());
        this.trMax = new SimpleIntegerProperty(category.getTrMax());
        this.type = new SimpleStringProperty(category.getType().getString());
//        this.type = new SimpleStringProperty((category.getType() == null) ? "" : category.getType().getString());
        this.color = new SimpleObjectProperty(category.getColor());
    }

    public Category getCategory() {
        return category;
    }

    public int getIdCat() {
        return idCat.get();
    }

    public IntegerProperty idCatProperty() {
        return idCat;
    }

    public void setIdCat(int idCat) {
        this.idCat.set(idCat);
        this.category.setIdCat(idCat);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
        this.category.setName(name);
    }

    public int getPriority() {
        return priority.get();
    }

    public IntegerProperty priorityProperty() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority.set(priority);
        this.category.setPriority(priority);
    }

    public int getTrMin() {
        return trMin.get();
    }

    public IntegerProperty trMinProperty() {
        return trMin;
    }

    public void setTrMin(int trMin) {
        this.trMin.set(trMin);
        this.category.setTrMin(trMin);
    }

    public int getTrMax() {
        return trMax.get();
    }

    public IntegerProperty trMaxProperty() {
        return trMax;
    }

    public void setTrMax(int trMax) {
        this.trMax.set(trMax);
        this.category.setTrMax(trMax);
    }

    public String getType() {
        return type.get();
    }

    public StringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
        this.category.setType(type);
    }

    public Object getColor() {
        return color.get();
    }

    public Object colorProperty() {
        return color;
    }

    public void setColor(Color color) {
        this.color.set(color);
        this.category.setColor(color);
    }

    @Override
    public String toString() {
        return "CategoryOfTrainsModel{" +
                "idCat=" + idCat.getValue() +
                ", name='" + name.getValue() + '\'' +
                ", priority=" + priority.getValue() +
                ", trMin=" + trMin.getValue() +
                ", trMax=" + trMax.getValue() +
                ", type=" + type.getValue() +
                ", color=" + color.getValue() +
                '}';
    }
}
