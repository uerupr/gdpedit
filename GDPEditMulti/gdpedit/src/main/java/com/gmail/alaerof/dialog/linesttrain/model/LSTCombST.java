package com.gmail.alaerof.dialog.linesttrain.model;

import java.util.ArrayList;
import java.util.List;

public class LSTCombST {

    private List<LST> listLST = new ArrayList<>();

    private List<TRCombST> listTRCombST = new ArrayList<>();

    public List<LST> getListLST() {
        return listLST;
    }

    public List<TRCombST> getListTRCombST() {
        return listTRCombST;
    }

    public void setListLST(List<LST> listLST) {
        this.listLST = listLST;
    }

    public void setListTRCombST(List<TRCombST> listTRCombST) {
        this.listTRCombST = listTRCombST;
    }
}
