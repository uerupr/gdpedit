package com.gmail.alaerof.javafx.dialog.calculatetimeenergy.model;

import com.gmail.alaerof.dao.dto.groupp.GroupColor;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class GroupColorModel {
    private GroupColor groupColor;
    private IntegerProperty color;

    public GroupColorModel(GroupColor groupColor) {
        this.groupColor = groupColor;
        this.color = new SimpleIntegerProperty(groupColor.getColor());
    }

    public GroupColor getGroupColor() {
        return groupColor;
    }

    public int getColor() {
        return color.get();
    }

    public IntegerProperty colorProperty() {
        return color;
    }

    public void setColor(int color) {
        this.color.set(color);
        this.groupColor.setColor(color);
    }

    public static List<GroupColor> extractGroupColor(List<GroupColorModel> selected) {
        List<GroupColor> list = new ArrayList<>();
        for(GroupColorModel colorModel: selected){
            list.add(colorModel.groupColor);
        }
        return list;
    }

    @Override
    public String toString() {
        return "GroupColorModel{" +
                "groupColor=" + groupColor +
                '}';
    }
}
