package com.gmail.alaerof.javafx.dialog.calculategdp.model;

import com.gmail.alaerof.dao.dto.routed.RouteDTrain;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class RouteDTrainModel {
    private RouteDTrain routeDTrain;
    private IntegerProperty min;
    private IntegerProperty max;

    public RouteDTrainModel(RouteDTrain routeDTrain) {
        this.routeDTrain = routeDTrain;
        min = new SimpleIntegerProperty(routeDTrain.getTrMin());
        max = new SimpleIntegerProperty(routeDTrain.getTrMax());
    }

    public RouteDTrain getRouteDTrain() {
        return routeDTrain;
    }

    public int getMin() {
        return min.get();
    }

    public IntegerProperty minProperty() {
        return min;
    }

    public void setMin(int min) {
        this.min.set(min);
    }

    public int getMax() {
        return max.get();
    }

    public IntegerProperty maxProperty() {
        return max;
    }

    public void setMax(int max) {
        this.max.set(max);
    }

    public static List<RouteDTrain> extractRouteDTrain(List<RouteDTrainModel> selected) {
        List<RouteDTrain> list = new ArrayList<>();
        for (RouteDTrainModel trainModel : selected) {
            list.add(trainModel.getRouteDTrain());
        }
        return list;
    }
}
