package com.gmail.alaerof.entity.train;

import java.util.Comparator;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class TrainComparatorByCode implements Comparator<TrainThread>{

    @Override
    public int compare(TrainThread t1, TrainThread t2) {
        Integer code1 = t1.getCodeInt();
        Integer code2 = t2.getCodeInt();
        return code1.compareTo(code2);
    }

}
