package com.gmail.alaerof.dialog.linesttrain;

import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IStationDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.dialog.linesttrain.model.LST;
import com.gmail.alaerof.dialog.linesttrain.model.LSTCombST;
import com.gmail.alaerof.dialog.linesttrain.model.TRCombST;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.FileUtil;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Frame;

import java.util.ArrayList;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrLineSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.entity.train.TimeStation;
import com.gmail.alaerof.entity.train.TrainStDouble;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.util.IconImageUtil;
import com.gmail.alaerof.util.ListTrainHandler;
import com.gmail.alaerof.util.TableUtils;
import com.gmail.alaerof.xml.LineTRXMLHandler;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.SwingConstants;
import javax.swing.JTextField;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class LineStTRDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    protected static Logger logger = LogManager.getLogger(LineStTRDialog.class);
    private JPanel contentPane;
    private DistrStation distrStation;
    private ListTrainCombSt panelListTrainCombSt;

    private ListTrainStTableModel tableModel;
    private JTable table;
    private JScrollPane tablePane;

    private TrainThread trainOn;
    private TrainThread trainOff;
    private JLabel lblCodeOn;
    private JLabel lblCodeOff;
    private JLabel lblNameOn;
    private JLabel lblNameOff;
    private JLabel lbTrainCombSource;
    private LineStCellEditor lineStSellEditor = new LineStCellEditor(null);
    private static ResourceBundle bundle;

    public LineStTRDialog() {
        initContent();
    }

    public LineStTRDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.linesttrain", LocaleManager.getLocale());
    }

    private void initContent() {
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 800, 600);
        // setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        {
            JPanel panel = new JPanel();
            panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
            contentPane.add(panel, BorderLayout.CENTER);
            panel.setLayout(new BorderLayout(0, 0));
            JPanel top = new JPanel();
            panel.add(top, BorderLayout.NORTH);
            JLabel label = new JLabel(bundle.getString("linesttrain.listalltrainsatthestation"));
            label.setFont(new Font("Tahoma", Font.BOLD, 12));
            top.add(label);

            Font tableFont = new Font(Font.SANS_SERIF, 0, 11);
            tableModel = new ListTrainStTableModel();
            table = new JTable(tableModel);
            tableModel.setTable(table);
            tablePane = new JScrollPane(table);
            table.setFont(tableFont);
            table.getTableHeader().setFont(tableFont);
            table.setFillsViewportHeight(true);
            table.setDefaultRenderer(Object.class, new ListTrainStCellRenderer());
            table.setDefaultEditor(DistrLineSt.class, lineStSellEditor);

            ListSelectionModel cellSelectionModel = table.getSelectionModel();
            cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            int colSize[] = { 40, 30, 40, 40, 30, 60, 20 };
            TableUtils.setTableColumnSize(table, colSize);

            //table.setToolTipText("��������� ������ � ������ ������ �������������� ������� ������ �� ������ ������");
            
            table.addMouseListener(new MouseAdapter() {

                @Override
                public void mousePressed(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        int row = table.getSelectedRow();
                        int col = table.getSelectedColumn();
                        if (col == 0) {
                            TrainThread train = tableModel.getListTrainSt().get(row);
                            panelListTrainCombSt.selectTrain(train);
                        }
                    }
                }
            });

            panel.add(tablePane, BorderLayout.CENTER);

            {
                JPanel panelS = new JPanel();
                FlowLayout flowLayout = (FlowLayout) panelS.getLayout();
                flowLayout.setAlignment(FlowLayout.LEADING);
                panel.add(panelS, BorderLayout.SOUTH);

                final JTextField fieldSearch = new JTextField();
                panelS.add(fieldSearch);
                fieldSearch.setColumns(10);

                JButton btnSearch = new JButton("");
                btnSearch.setIcon(IconImageUtil.getIcon(IconImageUtil.SEARCH));
                panelS.add(btnSearch);
                btnSearch.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        String codeTR = fieldSearch.getText();
                        List<TrainThread> listTR = tableModel.getListTrainSt();
                        TrainThread res = ListTrainHandler.searchTrain(codeTR, listTR);
                        int row = listTR.indexOf(res);
                        table.changeSelection(row, 0, false, false);
                    }
                });
                
                JButton saveToFile = new JButton("");
                saveToFile.setToolTipText(bundle.getString("linesttrain.savethejunction"));
                saveToFile.setIcon(IconImageUtil.getIcon(IconImageUtil.SAVE_FILE));
                panelS.add(saveToFile);
                saveToFile.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        bbSaveLSTClick();
                    }
                });

                JButton loadFromFile = new JButton("");
                loadFromFile.setToolTipText(bundle.getString("linesttrain.openthejunction"));
                loadFromFile.setIcon(IconImageUtil.getIcon(IconImageUtil.LOAD_FILE));
                panelS.add(loadFromFile);
                loadFromFile.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        bbOpenLSTClick();
                    }
                });
            }
        }

        JPanel panelTrainCombSource = new JPanel();
        FlowLayout flowLayout2 = (FlowLayout) panelTrainCombSource.getLayout();
        flowLayout2.setAlignment(FlowLayout.LEADING);
        contentPane.add(panelTrainCombSource, BorderLayout.SOUTH);
        lbTrainCombSource = new JLabel(bundle.getString("linesttrain.pathtothefilejunction"));
        lbTrainCombSource.setFont(new Font("Tahoma", Font.BOLD, 10));
        panelTrainCombSource.add(lbTrainCombSource);

        {
            JPanel panel = new JPanel();
            panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
            contentPane.add(panel, BorderLayout.EAST);
            panel.setLayout(new BorderLayout(0, 0));

            JPanel panelAddTRainCombSt = new JPanel();
            panelAddTRainCombSt.setBorder(new TitledBorder(null,
                    bundle.getString("linesttrain.creatinglink"),
                    TitledBorder.CENTER, TitledBorder.TOP, null, null));
            panel.add(panelAddTRainCombSt, BorderLayout.NORTH);
            panelAddTRainCombSt.setLayout(new BoxLayout(panelAddTRainCombSt, BoxLayout.X_AXIS));

            {
                JPanel panelOn = new JPanel();
                FlowLayout flowLayout = (FlowLayout) panelOn.getLayout();
                flowLayout.setAlignment(FlowLayout.LEFT);
                panelOn.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null),
                        bundle.getString("linesttrain.arrival"), TitledBorder.LEADING,
                        TitledBorder.TOP, null, null));
                panelAddTRainCombSt.add(panelOn);

                JButton buAdd = new JButton(bundle.getString("linesttrain.rightarrow"));
                panelOn.add(buAdd);

                buAdd.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int row = table.getSelectedRow();
                        if (row > -1) {
                            trainOn = tableModel.getListTrainSt().get(row);
                            lblCodeOn.setText(trainOn.getCode());
                            lblNameOn.setText(trainOn.getNameTR());
                        }
                    }
                });

                lblCodeOn = new JLabel(bundle.getString("linesttrain.x4"));
                lblCodeOn.setFont(new Font("Tahoma", Font.BOLD, 11));
                lblCodeOn.setHorizontalAlignment(SwingConstants.CENTER);
                panelOn.add(lblCodeOn);

                lblNameOn = new JLabel(bundle.getString("linesttrain.y2"));
                panelOn.add(lblNameOn);
            }
            {
                JPanel panelOff = new JPanel();
                panelOff.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null),
                        bundle.getString("linesttrain.departure"),
                        TitledBorder.LEADING, TitledBorder.TOP, null, null));
                panelAddTRainCombSt.add(panelOff);

                JButton buAdd = new JButton(bundle.getString("linesttrain.rightarrow"));
                panelOff.add(buAdd);

                buAdd.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int row = table.getSelectedRow();
                        if (row > -1) {
                            trainOff = tableModel.getListTrainSt().get(row);
                            lblCodeOff.setText(trainOff.getCode());
                            lblNameOff.setText(trainOff.getNameTR());
                        }
                    }
                });

                lblCodeOff = new JLabel(bundle.getString("linesttrain.x4"));
                lblCodeOff.setFont(new Font("Tahoma", Font.BOLD, 11));
                panelOff.add(lblCodeOff);

                lblNameOff = new JLabel(bundle.getString("linesttrain.y2"));
                panelOff.add(lblNameOff);
            }
            {
                JPanel panelButt = new JPanel();
                panelAddTRainCombSt.add(panelButt);
                panelButt.setLayout(new BoxLayout(panelButt, BoxLayout.Y_AXIS));

                JButton buCreate = new JButton(bundle.getString("linesttrain.create"));
                panelButt.add(buCreate);
                buCreate.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
                        if (dee != null) {
                            List<String> warnList = new ArrayList<>();
                            dee.addTrainStDouble(trainOn, trainOff, distrStation, warnList);
                            panelListTrainCombSt.repaintTable();
//                JOptionPane
//                        .showConfirmDialog(
//                                GDPEdit.gdpEdit.getMainFrame(),
//                                "������ ����� ������ �������� � ������ ����������� �� ���������. ����� ����������� ����� ����������� �� ���� ������ ��������",
//                                "��������������", JOptionPane.YES_OPTION);

                        }
                    }
                });

                JButton buCreateOnes = new JButton(bundle.getString("linesttrain.thesame"));
                panelButt.add(buCreateOnes);
                buCreateOnes.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        List<TrainThread> list = tableModel.getListTrainSt();
                        TrainThread tr1 = null;
                        TrainThread tr2 = null;
                        List<String> warnList = new ArrayList<>();
//                JOptionPane
//                        .showConfirmDialog(
//                                GDPEdit.gdpEdit.getMainFrame(),
//                                "������ ����� ������ �������� � ������ ����������� �� ���������. ����� ����������� ����� ����������� �� ���� ������ ��������",
//                                "��������������", JOptionPane.YES_OPTION);

                        if (list != null) {
                            DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
                            int idSt = distrStation.getIdPoint();
                            for (int i = 0; i < list.size(); i++) {
                                tr2 = list.get(i);
                                if (tr1 != null) {
                                    int cd1 = tr1.getCodeInt();
                                    int cd2 = tr2.getCodeInt();
                                    if (cd1 == cd2) {
                                        TrainThread trOn = null;
                                        TrainThread trOff = null;
                                        TimeStation timeSt1 = tr1.getTimeSt(distrStation.getIdPoint());
                                        TimeStation timeSt2 = tr2.getTimeSt(distrStation.getIdPoint());
                                            if (timeSt1.isNullOn() && !timeSt1.isNullOff()) {
                                            trOff = tr1;
                                        }
                                        if (!timeSt1.isNullOn() && timeSt1.isNullOff()) {
                                            trOn = tr1;
                                        }
                                        if (timeSt2.isNullOn() && !timeSt2.isNullOff()) {
                                            trOff = tr2;
                                        }
                                        if (!timeSt2.isNullOn() && timeSt2.isNullOff()) {
                                            trOn = tr2;
                                        }
                                        if (trOn != null && trOff != null) {
                                            long idTrainOn = trOn.getIdTrain();
                                            long idTrainOff = trOff.getIdTrain();

                                            TrainStDouble trStD = panelListTrainCombSt.getTrainStDouble(
                                                    idTrainOn, idTrainOff);
                                            if (trStD == null) {
                                                // ���������� �����
                                                dee.addTrainStDouble(trOn, trOff, distrStation, warnList);
                                            }
                                        }
                                    }
                                }
                                tr1 = tr2;
                            }
                            panelListTrainCombSt.setListTrainStDouble(dee.getListTrainStDouble(idSt));
                            panelListTrainCombSt.repaintTable();
                        }
                    }
                });
            }
            panelListTrainCombSt = new ListTrainCombSt();
            panelListTrainCombSt.setLineStTR(this);
            panel.add(panelListTrainCombSt, BorderLayout.CENTER);
        }
    }

    protected void bbOpenLSTClick() {
        // �������� �� ����� ������ ������ �� �������
        logger.debug("loadXML");
        int idSt = distrStation.getIdPoint();
        String nameSt = distrStation.getNamePoint().trim();
        String fileName = ".xml";
        File file = FileUtil.chooseFile(FileUtil.Mode.open,null, fileName, GDPEdit.gdpEdit.getMainFrame(),
                new FileNameExtensionFilter("XML files", "xml"));
        if (file != null) {
            String mess = bundle.getString("linesttrain.ok");
            GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.waitCursor);
            try {
                LSTCombST lSTCombST = new LineTRXMLHandler().loadLineTRxml(file, idSt, nameSt);
                List<LST> listLST = lSTCombST.getListLST();
                List<TrainThread> listTr = tableModel.getListTrainSt();
                for (LST aListLST : listLST) {
                    int idLineSt = findIDLineSt(aListLST);
                    TrainThread train = findTrainThread(listTr, aListLST);
                    if (train != null) {
                        TimeStation st = train.getTimeSt(idSt);
                        LineStTrain lstTr = st.getLineStTrain();
                        if (lstTr != null) {
                            lstTr.setTexStop(aListLST.gettTex());
                            lstTr.setIdLineSt(idLineSt);
                        }
                    }
                }
                table.repaint();

                List<TRCombST> listTRCombST = lSTCombST.getListTRCombST();
                List<TrainStDouble> listTrainStDouble = new ArrayList<>();
                for (TRCombST aListTRCombST : listTRCombST) {
                    boolean findTrainOn = false;
                    boolean findTrainOff = false;
                    TrainThread trainThreadOn = null;
                    TrainThread trainThreadOff = null;
                    for (TrainThread aListTr : listTr) {
                        if (aListTRCombST.getTrNameOn().equals(aListTr.getNameTR()) && aListTRCombST.getTrCodeOn().equals(aListTr.getCode())) {
                            findTrainOn = true;
                            trainThreadOn = aListTr;
                        }
                        if (aListTRCombST.getTrNameOff().equals(aListTr.getNameTR()) && aListTRCombST.getTrCodeOff().equals(aListTr.getCode())) {
                            findTrainOff = true;
                            trainThreadOff = aListTr;
                        }
                    }
                    if (findTrainOn && findTrainOff) {
                        List<String> warnList = new ArrayList<>();
                        DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
                        TrainStDouble trainStDouble = dee.createTrainStDouble(trainThreadOn, trainThreadOff, distrStation, warnList);
                        listTrainStDouble.add(trainStDouble);
                    } else {
                        if (!findTrainOn) {
                            logger.error(bundle.getString("linesttrain.arrivaltrainnotfound") + aListTRCombST.getTrCodeOn() + " (" + aListTRCombST.getTrNameOn() + ")");
                        }
                        if (!findTrainOff) {
                            logger.error(bundle.getString("linesttrain.departuretrainnotfound") + aListTRCombST.getTrCodeOff() + " (" + aListTRCombST.getTrNameOff() + ")");
                        }
                    }
                }
                panelListTrainCombSt.getListTrainStDouble().clear();
                panelListTrainCombSt.getListTrainStDouble().addAll(listTrainStDouble);
                panelListTrainCombSt.repaintTable();
                lbTrainCombSource.setText(file.getPath());
                panelListTrainCombSt.setTrainCombSource(lbTrainCombSource.getText());
            } finally {
                GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.defCursor);
            }
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
        }
    }

    private TrainThread findTrainThread(List<TrainThread> listTr, LST aListLST) {
        for (TrainThread aListTr : listTr) {
            if (aListLST.getNameTR().equals(aListTr.getNameTR()) && aListLST.getCodeTR().equals(aListTr.getCode())) {
                return  aListTr;
            }
        }
        return null;
    }

    private int findIDLineSt(LST aListLST) {
        List<DistrLineSt> listLines = distrStation.getListLines();
        for (DistrLineSt listLine : listLines) {
            if (listLine.getLineSt().getN() == aListLST.getN() && listLine.getLineSt().getPs().equals(aListLST.getTRA())) {
                return listLine.getLineSt().getIdLineSt();
            }
        }
        return 0;
    }

    protected void bbSaveLSTClick() {
        int idSt = distrStation.getIdPoint();
        String nameSt = distrStation.getNamePoint().trim();
        String fileName = "��������_" + nameSt + ".xml";
        File file = FileUtil.chooseFile(FileUtil.Mode.save,null, fileName, GDPEdit.gdpEdit.getMainFrame(),
                new FileNameExtensionFilter("XML files", "xml"));
        if (file != null) {
            String mess = bundle.getString("linesttrain.ok");
            GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.waitCursor);
            try {
                List<TrainThread> listTr = tableModel.getListTrainSt();
                List<TrainStDouble> listDouble = panelListTrainCombSt.getListTrainStDouble();
                boolean x = new LineTRXMLHandler().saveLineTRxml(file, listTr, listDouble, idSt, nameSt);
                if(!x){
                    mess = bundle.getString("linesttrain.saveerrorsseelog");
                }
            } finally {
                GDPEdit.gdpEdit.getMainFrame().setCursor(GDPEdit.defCursor);
            }
            JOptionPane.showMessageDialog(GDPEdit.gdpEdit.getMainFrame(), mess);
        }
    }

    public DistrStation getDistrStation() {
        return distrStation;
    }

    public void setDistrStation(DistrStation distrStation) {
        this.distrStation = distrStation;
        this.setTitle(distrStation.getNamePoint());
        DistrEditEntity dee = GDPEdit.gdpEdit.getCurrentDistrEditEntity();
        int idSt = distrStation.getIdPoint();
        // �������� ���� �� ��
        try {
            IStationDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getStationDAO();
            String trainCombSource = dao.getTrainCombSource(idSt);
            lbTrainCombSource.setText(trainCombSource);
        } catch (DataManageException e){
            logger.error(e.toString(), e);
            lbTrainCombSource.setText("");
        }
        panelListTrainCombSt.setIdSt(idSt);
        panelListTrainCombSt.setTrainCombSource(lbTrainCombSource.getText());
        panelListTrainCombSt.setListTrainStDouble(dee.getListTrainStDouble(idSt));
        tableModel.setListTrainSt(dee.getListTrainSt(idSt));
        tableModel.setIdSt(idSt);
        lineStSellEditor.setListLineSt(distrStation.getListLines());
    }

    public void clearLabel() {
        trainOn = null;
        trainOff = null;
        lblCodeOn.setText(bundle.getString("linesttrain.x4"));
        lblCodeOff.setText(bundle.getString("linesttrain.x4"));
        lblNameOn.setText(bundle.getString("linesttrain.y2"));
        lblNameOff.setText(bundle.getString("linesttrain.y2"));

        panelListTrainCombSt.clearLabel();
    }

    public void selectTrain(TrainThread train) {
        try {
            List<TrainThread> list = tableModel.getListTrainSt();
            int row = -1;
            int i = 0;
            while (row == -1 && i < list.size()) {
                if (train.equalsByCodeName(list.get(i))) {
                    row = i;
                }
                i++;
            }
            if (row > -1) {
                table.changeSelection(row, 0, false, false);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

}
