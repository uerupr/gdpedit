package com.gmail.alaerof.dialog.timeimport;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.gmail.alaerof.dao.dto.VariantCalc;
import com.gmail.alaerof.manager.LocaleManager;

/**
 * 
 * @author Helen Yrofeeva
 *
 */

public class VarCalcTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private String[] columnNames = { bundle.getString("timeimport.direction"),
            bundle.getString("timeimport.savedate"),
            bundle.getString("timeimport.calculationoption"),
            bundle.getString("timeimport.description"),
            bundle.getString("timeimport.additionaldescription") };
    private List<VariantCalc> list;
    private static ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("bundles.timeimport", LocaleManager.getLocale());
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public int getRowCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (list != null) {
            VariantCalc el = list.get(row);
            switch (col) {
            case 0:
                return el.getStrOE();
            case 1:
                return el.getDate();
            case 2:
                return el.getName();
            case 3:
                return el.getDescription();
            case 4:
                return el.getDescriptionAdd();
            }
        }
        return null;
    }

    public List<VariantCalc> getList() {
        return list;
    }

    public void setList(List<VariantCalc> list) {
        this.list = list;
    }

}
