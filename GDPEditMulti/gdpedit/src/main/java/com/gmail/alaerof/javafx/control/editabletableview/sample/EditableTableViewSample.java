package com.gmail.alaerof.javafx.control.editabletableview.sample;

import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class EditableTableViewSample extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        EditableTableViewCreator<Person> builder = new EditableTableViewCreator<>();
        TableView<Person> table = builder.createEditableTableView();

        table.getColumns().add(builder.createStringColumn("First Name", Person::firstNameProperty, false));
        table.getColumns().add(builder.createStringColumn("Last Name", Person::lastNameProperty, false));
        table.getColumns().add(builder.createStringColumn("Email", Person::emailProperty));
        table.getColumns().add(builder.createIntegerColumn("Year", Person::yearProperty));
        table.getColumns().add(builder.createDoubleColumn("Salary", Person::salaryProperty));

        table.getItems().addAll(
                new Person("Jacob", "Smith", "jacob.smith@example.com", 123,0.0),
                new Person("Isabella", "Johnson", "isabella.johnson@example.com", 1644, 0.0),
                new Person("Ethan", "Williams", "ethan.williams@example.com", 2034, 0.0),
                new Person("Emma", "Jones", "emma.jones@example.com", 954, 0.0),
                new Person("Michael", "Brown", "michael.brown@example.com", 875, 0.0)
        );

        Scene scene = new Scene(new BorderPane(table), 880, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

