package com.gmail.alaerof.javafx.dialog.gdpprint.model;

import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.dao.dto.TextPrintParam;
import com.gmail.alaerof.javafx.dialog.gdpprint.model.GDPPrintParam;
import com.gmail.alaerof.util.CalcString;
import java.util.HashMap;
import java.util.Map;


import static jdk.nashorn.internal.objects.NativeString.trim;

public class TextPrintPointsOut {
    // ������ �� ���� �����, �� ���������
    public final static int upFreeSpace = 50;
    // ������ �� ���� ������� �� ������ ������� �����������
    public final static int downFreeSpace = 20;
    // ������ �� ��������� ������� ����������� �� ���� �����
    public final static int lastDownFreeSpace = 50;
    // ������ ����� ��������� �����
    public final static int beetwinHeadFreeSpace = 10;
    // ������ ����� ��������� �������������
    public final static int beetwinBossFreeSpace = 5;
    // ������ ����� ��������� ������������
    public final static int beetwinWorkerFreeSpace = 5;
    // ������ ����� �� ���� ������� �� ������������
    public final static int leftFreeSpace = 100;
    // ������ ������ �� ���� ������� �� ����� �������������
    public final static int rightFreeSpace = 100;
    private int headGap;
    private int bottomGap;
    private GDPPrintParam gdpPrintParam;
    private Map<Integer, Integer> mapXPoints = new HashMap<>();
    private Map<Integer, Integer> mapYPoints = new HashMap<>();

    public TextPrintPointsOut(GDPPrintParam gdpPrintParam){
        this.gdpPrintParam = gdpPrintParam;
        CalcAllGaps();
    }

    private void CalcAllGaps(){
        int gap = upFreeSpace;
        for (int i = 0; i < gdpPrintParam.getHeadTextFields().size(); i++){
            TextPrintParam tpp = gdpPrintParam.getHeadTextFields().get(i);
            if (tpp.getBChoise()){
                mapYPoints.put(i, gap);
                gap = CalcGap(gap, tpp, beetwinHeadFreeSpace);
            }
            else {
                mapYPoints.put(i, 0);
            }
        }
        gap = gap - beetwinHeadFreeSpace;
        if (headGap < gap){
            headGap = gap;
        }
        gap = upFreeSpace;
        for (int i = 0; i < gdpPrintParam.getBossTextFields().size(); i++){
            TextPrintParam tpp = gdpPrintParam.getBossTextFields().get(i);
            if (tpp.getBChoise()){
                mapYPoints.put(i + 4, gap);
                gap = CalcGap(gap, tpp, beetwinBossFreeSpace);
            }
            else {
                mapYPoints.put(i + 4, 0);
            }
        }
        gap = gap - beetwinBossFreeSpace;
        if (headGap < gap){
            headGap = gap;
        }
        gap = downFreeSpace + lastDownFreeSpace;
        for (int i = 0; i < gdpPrintParam.getWorkerTextFields().size(); i++){
            TextPrintParam tpp = gdpPrintParam.getWorkerTextFields().get(i);
            if (tpp.getBChoise()){
                gap = CalcGap(gap, tpp, beetwinWorkerFreeSpace);
            }
        }
        gap = gap - beetwinWorkerFreeSpace;
        if (bottomGap < gap){
            bottomGap = gap;
        }
    }

    private int CalcGap(int gap, TextPrintParam tpp, int beetwinFreeSpace){
        String fName = tpp.getFontName();
        int fStyle = tpp.getFontStyleAwt();
        int fSize = tpp.getFontSizeAwt();
        java.awt.Font strfont = new java.awt.Font(fName, fStyle, fSize);
        int textheight = CalcString.getStringH(strfont);
        gap = gap + textheight + beetwinFreeSpace;
        return gap;
    }

    public void CalcTextXPoints(int xLeftGDP, GDPGridConfig configGDP, int left){
        for (int i = 0; i < gdpPrintParam.getHeadTextFields().size(); i++) {
            TextPrintParam tpp = gdpPrintParam.getHeadTextFields().get(i);
            if (tpp.getBChoise()) {
                String strtext = tpp.getTextField();
                strtext = trim(strtext);
                String fName = tpp.getFontName();
                int fStyle = tpp.getFontStyleAwt();
                int fSize = tpp.getFontSizeAwt();
                java.awt.Font strfont = new java.awt.Font(fName, fStyle, fSize);
                int textwidth = CalcString.getStringW(strfont, strtext);
                mapXPoints.put(i, (xLeftGDP + configGDP.getScaledMainImageWidth() + left - textwidth) / 2);
            } else {
                mapXPoints.put(i, 0);
            }
        }
        int maxTextLength = 0;
        for (int i = 0; i < gdpPrintParam.getBossTextFields().size(); i++) {
            TextPrintParam tpp = gdpPrintParam.getBossTextFields().get(i);
            if (tpp.getBChoise()) {
                String strtext = tpp.getTextField();
                strtext = trim(strtext);
                String fName = tpp.getFontName();
                int fStyle = tpp.getFontStyleAwt();
                int fSize = tpp.getFontSizeAwt();
                java.awt.Font strfont = new java.awt.Font(fName, fStyle, fSize);
                int textwidth = CalcString.getStringW(strfont, strtext);
                if (maxTextLength < textwidth){
                    maxTextLength = textwidth;
                }

            }
        }
        for (int i = 0; i < gdpPrintParam.getBossTextFields().size(); i++) {
            TextPrintParam tpp = gdpPrintParam.getBossTextFields().get(i);
            if (tpp.getBChoise()) {
                mapXPoints.put(i + 4, xLeftGDP + configGDP.getScaledMainImageWidth() + left - maxTextLength - rightFreeSpace);
            } else {
                mapXPoints.put(i + 4, 0);
            }
        }
        int gap = headGap + configGDP.getScaledMainImageHeight() + downFreeSpace;
        for (int i = 0; i < gdpPrintParam.getWorkerTextFields().size(); i++) {
            TextPrintParam tpp = gdpPrintParam.getWorkerTextFields().get(i);
            if (tpp.getBChoise()) {
                String fName = tpp.getFontName();
                int fStyle = tpp.getFontStyleAwt();
                int fSize = tpp.getFontSizeAwt();
                java.awt.Font strfont = new java.awt.Font(fName, fStyle, fSize);
                int textheight = CalcString.getStringH(strfont);
                mapYPoints.put(i + 8, gap);
                gap = gap + textheight + beetwinWorkerFreeSpace;
                mapXPoints.put(i + 8, xLeftGDP + left + leftFreeSpace);
            } else {
                mapYPoints.put(i + 8, 0);
                mapXPoints.put(i + 8, 0);
            }
        }
    }

    public int getHeadGap() {
        return headGap;
    }

    public int getBottomGap() {
        return bottomGap;
    }

    public int getXPoint(int code) {
        return mapXPoints.get(code);
    }

    public int getYPoint(int code) {
        return mapYPoints.get(code);
    }
}
