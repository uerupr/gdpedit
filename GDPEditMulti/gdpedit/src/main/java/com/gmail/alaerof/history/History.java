package com.gmail.alaerof.history;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.gmail.alaerof.history.Action.TypeValue;
import com.gmail.alaerof.util.ConnectionDBFileHandlerUtil;
import com.gmail.alaerof.util.TimeConverter;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class History {
    public static Logger logger = LogManager.getLogger(History.class);
    private ArrayList<Action> actions = new ArrayList<>();
    private String historyFile;
    private int currentIntex = -1; // ������� �������

    public History(String id) {
        historyFile = "history" + File.separator + "h" + id + "_" + TimeConverter.getNowDateTime() + ".xml";
    }

    public int getActionsCount(){
        return actions.size();
    }
    
    private void saveHistory(Document document) {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            PrintStream out = new PrintStream(new FileOutputStream(historyFile));
            StreamResult result = new StreamResult(out);
            transformer.setOutputProperty(OutputKeys.ENCODING, ConnectionDBFileHandlerUtil.charsetWin.toString());
            transformer.transform(source, result);
        } catch (TransformerException e) {
            logger.error(e.toString(), e);
        } catch (FileNotFoundException e) {
            logger.error(e.toString(), e);
        }

    }

    private Document loadHistory() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document document = null;
        try {
            db = dbf.newDocumentBuilder();
            File file = new File(historyFile);
            if (file.exists()) {
                document = db.parse(historyFile);
            } else {
                document = db.newDocument();
                Element root = document.createElement("History");
                document.appendChild(root);
            }
        } catch (ParserConfigurationException e) {
            logger.error(e.toString(), e);
        } catch (SAXException e) {
            logger.error(e.toString(), e);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        return document;
    }

    /**
     * ���������� �������� � �������
     * @param action
     */
    public synchronized void addAction(Action action, TypeValue typeValue) {
        Document doc = loadHistory();
        action.saveAction(doc, typeValue);
        if (!actions.contains(action))
            actions.add(action);
        saveHistory(doc);
        currentIntex = actions.size() - 1;
    }

    /**
     * ������ �������� ��������
     */
    public synchronized boolean restoreAction() {
        if (currentIntex > -1 && currentIntex < actions.size()) {
            Document doc = loadHistory();
            Action action = actions.get(currentIntex);
            currentIntex--;
            return action.restoreAction(doc);
        } else
            return false;
    }

    /**
     * ������ ��������� ��������
     * @param action
     */
    public synchronized boolean restoreAction(Action action) {
        currentIntex = actions.indexOf(action);
        return restoreAction();
    }
    /**
     * ������ ��������� ��������
     */
    public synchronized boolean restoreAction(int index) {
        currentIntex = index;
        return restoreAction();
    }
    /**
     * ���������� �������� �������� ����� ��� ������
     */
    public boolean applyAction() {
        if (currentIntex >= -1 && currentIntex < actions.size()-1) {
            currentIntex++;
            Document doc = loadHistory();
            Action action = actions.get(currentIntex);
            return action.applyAction(doc);
        } else
            return false;
    }

    /**
     * ���������� ��������� �������� ����� ��� ������
     * @param action
     */
    public boolean applyAction(Action action) {
        currentIntex = actions.indexOf(action)-1;
        return applyAction();
    }

    /**
     * ���������� ��������� �������� ����� ��� ������
     * @param index
     */
    public boolean applyAction(int index) {
        currentIntex = index-1;
        return applyAction();
    }
    
    public void clear() {
        File file = new File(historyFile);
        if (file.exists()) {
            file.delete();
        }
        actions.clear();
        currentIntex = -1;
        historyFile = "history" + File.separator + "h_" + TimeConverter.getNowDateTime() + ".xml";
    }

    public ArrayList<Action> getActions(){
        return actions;
    }
    
    public int getCurrentIndex(){
        return currentIntex;
    }
}
