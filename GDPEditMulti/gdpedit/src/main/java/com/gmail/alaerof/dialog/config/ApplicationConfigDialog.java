package com.gmail.alaerof.dialog.config;

import com.gmail.alaerof.manager.LocaleManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class ApplicationConfigDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private int result = 0;
    private static ResourceBundle bundle;

    public ApplicationConfigDialog() {
        initContent();
    }

    public ApplicationConfigDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        initContent();
    }

    private JPanel paBGColor; 
    private JPanel paSTColor;

    static {
        bundle = ResourceBundle.getBundle("bundles.config", LocaleManager.getLocale());
    }

    private void initContent() {
        //setAlwaysOnTop(true);
        setBounds(100, 100, 400, 200);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new FlowLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);

        {
            JPanel panel = new JPanel();
            panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true), "",
                    TitledBorder.LEADING, TitledBorder.TOP, null, null));
            contentPanel.add(panel);
            {
                JLabel label = new JLabel(bundle.getString("config.backgroundcolor"));
                panel.add(label);
            }

            paBGColor = new JPanel();
            paBGColor.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
            paBGColor.setPreferredSize(new Dimension(50, 30));
            panel.add(paBGColor);
            paBGColor.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent arg0) {
                    JColorChooser cc = new JColorChooser(paBGColor.getBackground());
                    Color newColor = JColorChooser.showDialog(cc, bundle.getString("config.choiceofcolor"),
                            paBGColor.getBackground());
                    if (newColor != null) {
                        paBGColor.setBackground(newColor);
                    }
                }
            });
        }
        {
            JPanel panel = new JPanel();
            panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 1, true), "",
                    TitledBorder.LEADING, TitledBorder.TOP, null, null));
            contentPanel.add(panel);
            {
                JLabel label = new JLabel(bundle.getString("config.colorselectedtrain"));
                panel.add(label);
            }

            paSTColor = new JPanel();
            paSTColor.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
            paSTColor.setPreferredSize(new Dimension(50, 30));
            panel.add(paSTColor);
            paSTColor.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent arg0) {
                    JColorChooser cc = new JColorChooser(paBGColor.getBackground());
                    Color newColor = JColorChooser.showDialog(cc, bundle.getString("config.choiceofcolor"),
                            paSTColor.getBackground());
                    if (newColor != null) {
                        paSTColor.setBackground(newColor);
                    }
                }
            });
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            final JDialog dialog = this;
            {
                JButton okButton = new JButton(bundle.getString("config.apply"));
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        result = 1;
                        dialog.setVisible(false);
                    }
                });
            }
            {
                JButton cancelButton = new JButton(bundle.getString("config.cancel"));
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        result = 0;
                        dialog.setVisible(false);
                    }
                });
            }
        }
        // setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }

    public int getResult() {
        return result;
    }

    public void fillFrameByConfig(ApplicationConfig applicationConfig) {
        if (applicationConfig != null) {
            paBGColor.setBackground(applicationConfig.getBackgroundColor());
            paSTColor.setBackground(applicationConfig.getSelectTrainColor());
        }
    }

    public void fillConfigByFrame(ApplicationConfig applicationConfig) {
        if (applicationConfig != null) {
            applicationConfig.setBackgroundColor(paBGColor.getBackground());
            applicationConfig.setSelectTrainColor(paSTColor.getBackground());
        }
    }
}
