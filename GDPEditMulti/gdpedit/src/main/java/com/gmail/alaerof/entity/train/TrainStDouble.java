package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import com.gmail.alaerof.dao.dto.TrainCombSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.util.ColorConverter;
import com.gmail.alaerof.util.TimeConverter;
import com.gmail.alaerof.entity.train.NewLine.DrawCaption;

/**
 * @author Helen Yrofeeva
 */
public class TrainStDouble extends TrainThreadElement {
    // super.trainThread - ����� ��������
    /** ����� ����������� */
    private TrainThread trainThreadOff;
    /** ��� ����� ������� */
    private NewLine horLine1;
    /** ��� ����� ������� (������ ��������, ���� ���� ������� �� ����. �����) */
    private NewLine horLine2;
    /** ������� ������� */
    private DistrStation station;
    /** ������� + ����� */
    private TimeStation timeSt;
    /** ������ */
    private TrainCombSt trainCombSt;
    private boolean showLine1;
    private boolean showLine2;

    public TrainCombSt getTrainCombSt() {
        return trainCombSt;
    }

    private NewLineSelfParams selfParams;

    public TrainStDouble(JComponent owner, TrainThread trainThreadOn, TrainThread trainThreadOff,
            DistrStation station, TrainCombSt trainCombSt) {
        super(owner, trainThreadOn);
        this.trainThreadOff = trainThreadOff;
        this.trainCombSt = trainCombSt;
        this.station = station;

        // GeneralParams � ���� ����� ����������
        TrainThreadGeneralParams params = trainThread.getGeneralParams();
        GDPGridConfig conf = trainThread.getDistrEntity().getConfigGDP();
        timeSt = buildTimeSt(trainThreadOn, station, trainCombSt, params, conf);

        selfParams = createNewLineSelfParams();

        horLine1 = new LineHor(params, selfParams, this);
        horLine2 = new LineHor(params, selfParams, this);

        owner.add(horLine1, 0);
        owner.add(horLine2, 0);

        setLineBounds();
    }

    protected NewLineSelfParams createNewLineSelfParams() {
        NewLineSelfParams selfParams = new NewLineSelfParams();
        if (!trainThread.getCode().equals(trainThreadOff.getCode())) {
            selfParams.setCaptionValue(trainThread.getCode() + "-" + trainThreadOff.getCode());
        } else {
            selfParams.setCaptionValue(trainThread.getCode());
        }
        // ����� �����
        selfParams.lineStyle = LineStyle.Solid;
        return selfParams;
    }

    protected TimeStation buildTimeSt(TrainThread trainThreadOn,
                                      DistrStation station,
                                      TrainCombSt trainCombSt,
                                      TrainThreadGeneralParams params,
                                      GDPGridConfig conf) {
        TimeStation timeSt = new TimeStation(station, trainCombSt);
        try {
            timeSt.mOn = TimeConverter.timeToMinutes(trainCombSt.occupyOn);
            timeSt.mOff = TimeConverter.timeToMinutes(trainCombSt.occupyOff);
            timeSt.xOn = TimeConverter.minutesToX(timeSt.mOn, params, conf);
            timeSt.xOff = TimeConverter.minutesToX(timeSt.mOff, params, conf);
            int color = trainThreadOn.getColor();
            Color clr = ColorConverter.convert(color);
            timeSt.colorTR = clr;
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        return timeSt;
    }

    private void setLineBoundsNoExpanded() {
        horLine1.setCaption(DrawCaption.NoCaption);
        horLine2.setCaption(DrawCaption.NoCaption);

        showLine1 = false;
        showLine2 = false;

        horLine1.setVisible(showLine1);
        horLine2.setVisible(showLine2);
    }

    private void setLineBoundsExpanded() {
        // ������� ����������� ������� �� �����
        horLine1.setCaption(DrawCaption.NoCaption);
        if ((trainThread.isShowInStation() || trainThreadOff.isShowInStation()) && timeSt.getLineCount() > 0) {
            horLine1.setCaption(DrawCaption.On);
        }
        // �������� ������������� �������� (���������)
        showLine1 = false;
        showLine2 = false;

        if (timeSt.xOn != timeSt.xOff) {
            showLine1 = true; // ���� �������
        }
        if (timeSt.xOn > timeSt.xOff) {
            showLine2 = true; // ���� ������� �� ����� �����
        }
        // ���� ���������� �� �����
        if ((!trainThread.isShowInStation()) && (!trainThreadOff.isShowInStation())) {
            showLine1 = false;
            showLine2 = false;
        }

        if (trainThread.isHidden() || trainThreadOff.isHidden()) {
            showLine1 = false;
            showLine2 = false;
        }

        // ���� ���� ���� ���-��, �� ������� ����������
        if (showLine1 || showLine2) {
            // ������ ������������ ���������
            int yTop = 0;
            int yMiddle = 0;
            int h = timeSt.station.getNameHeight() + 2;

            int idLineSt = trainCombSt.idLineSt; // timeSt.lineStTrain.getIdLineSt();
            yMiddle = timeSt.station.getLineY(idLineSt);
            yTop = timeSt.station.getY1();

            if (yMiddle == -1) {
                yMiddle = yTop;
            }

            int w1 = 0;
            int w2 = 0;
            // ���� ��� �������� ����� �����
            if (!showLine2) {
                w1 = Math.max(1, timeSt.xOff - timeSt.xOn);

                Dimension dim = horLine1.getOriginalSize();
                dim.width = w1;
                dim.height = h;
                horLine1.setPosition(timeSt.xOn, yMiddle);
                horLine1.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
            } else {
                TrainThreadGeneralParams generalParams = horLine1.getGeneralParams();
                w1 = generalParams.xRight - timeSt.xOn;
                w2 = timeSt.xOff - generalParams.xLeft;

                Dimension dim = horLine1.getOriginalSize();
                dim.width = Math.max(1, w1);
                dim.height = h;

                dim = horLine2.getOriginalSize();
                dim.width = Math.max(1, w2);
                dim.height = h;

                horLine1.setPosition(timeSt.xOn, yMiddle);

                horLine2.setPosition(generalParams.xLeft, yMiddle);
                horLine1.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
                horLine2.selfParams.lineWidth = Math.max(2, trainThread.getWidthTrain());
            }
        }

        horLine1.setVisible(showLine1);
        horLine2.setVisible(showLine2);
    }

    @Override
    public void setLineBounds() {
        try {
            selfParams.color = timeSt.colorTR;
            if (timeSt.station.isExpanded()) {
                setLineBoundsExpanded();
            } else {
                setLineBoundsNoExpanded();
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    public int getIdLineSt() {
        return trainCombSt.idLineSt;
    }

    public void setIdLineSt(int idLineSt) {
        int id = getIdLineSt();
        if (id != idLineSt) {
            String nameLine = timeSt.station.getLineName(idLineSt);
            if ("".equals(nameLine)) {
                idLineSt = 0;
            }
            trainCombSt.idLineSt = idLineSt;
            trainThread.setLineStTr(timeSt.station.getIdPoint(), idLineSt);
            trainThreadOff.setLineStTr(timeSt.station.getIdPoint(), idLineSt);
            setLineBounds();
        }
    }

    public void setIdLineSt(int idLineSt, long idTrain) {
        int id = getIdLineSt();
        if (id != idLineSt) {
            String nameLine = timeSt.station.getLineName(idLineSt);
            if ("".equals(nameLine)) {
                idLineSt = 0;
            }
            trainCombSt.idLineSt = idLineSt;
            if (idTrain != trainThread.getIdTrain()) {
                trainThread.setLineStTr(timeSt.station.getIdPoint(), idLineSt);
            }
            if (idTrain != trainThreadOff.getIdTrain()) {
                trainThreadOff.setLineStTr(timeSt.station.getIdPoint(), idLineSt);
            }
            setLineBounds();
        }
    }

    public void removeFromTrains() {
        trainThread.removeTrainStDouble(this);
        trainThreadOff.removeTrainStDouble(this);
    }

    @Override
    public void updateLineListener() {

    }

    @Override
    public void repaint() {
        horLine1.repaint();
        horLine2.repaint();
    }

    @Override
    public void removeFromOwner() {
        if (horLine1 != null)
            owner.remove(horLine1);
        if (horLine2 != null)
            owner.remove(horLine2);
        owner.repaint();
    }

    @Override
    public void addToOwner() {
        if (horLine1 != null)
            owner.add(horLine1, 0);
        if (horLine2 != null)
            owner.add(horLine2, 0);
    }

    public TrainThread getTrainThreadOn() {
        return trainThread;
    }

    public TrainThread getTrainThreadOff() {
        return trainThreadOff;
    }

    @Override
    public TimeStation getClickedTimeSt() {
        return timeSt;
    }

    @Override
    public TimeStation getStationBegin() {
        return timeSt;
    }

    @Override
    public void drawInPicture(Graphics g) {
        if (horLine1 != null && showLine1)
            horLine1.drawInPicture(g);
        if (horLine2 != null && showLine2)
            horLine2.drawInPicture(g);
    }

    /**
     * ��������� �������� �� �����
     * @param g �����
     * @param config ��������� ���������
     */
    public void drawInPicture(Graphics g, GDPGridConfig config) {
        if (station.isExpanded()) {
            TrainThreadGeneralParams params = trainThread.getGeneralParams();
            TimeStation timeSt = buildTimeSt(this.trainThread, station, trainCombSt, params, config);
            NewLineSelfParams selfParams = createNewLineSelfParams();
            selfParams.color = timeSt.colorTR;
            DrawLineUtil.drawTrainStDouble(g,timeSt,trainThread, this.trainThreadOff, config,selfParams);
        }
    }
}
