package com.gmail.alaerof.components.mainframe.gdp.action.listspan;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.javafx.dialog.spantime.SpanTimeFXDialog;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.manager.LocaleManager;
import com.gmail.alaerof.util.TableUtils;
/**
 * @author Helen Yrofeeva
 */
public class GDPActionListSpan extends JPanel {
    private static final long serialVersionUID = 1L;
    private ListSpanTableModel tableModel;
    private JTable table;
    private DistrEditEntity distrEditEntity;
    private static ResourceBundle bundle;

    /**
     * Create the panel.
     */
    public GDPActionListSpan() {
        initPanel();
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.listspan", LocaleManager.getLocale());
    }

    private void initPanel() {
        setLayout(new BorderLayout(0, 0));

        {
            JPanel panel = new JPanel();
            add(panel, BorderLayout.NORTH);

            JLabel lblNewLabel = new JLabel(
                    bundle.getString("listspan.spans"));
            panel.add(lblNewLabel);
        }
        {
            JPanel panel = new JPanel();
            // add(panel, BorderLayout.SOUTH);

            JButton btnNewButton = new JButton(
                    bundle.getString("listspan.newbutton"));
            panel.add(btnNewButton);
        }

        Font tableFont = new Font(Font.SANS_SERIF, 0, 11);
        tableModel = new ListSpanTableModel();
        table = new JTable(tableModel);
        JScrollPane tablePane = new JScrollPane(table);
        table.setFont(tableFont);
        table.getTableHeader().setFont(tableFont);
        table.setFillsViewportHeight(true);

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int colSize[] = { 20, 100, 40 };
        TableUtils.setTableColumnSize(table, colSize);

        table.addMouseListener(selectSpanTableRow());
        this.add(tablePane, BorderLayout.CENTER);
    }

    /**
     *
     * @return {@link MouseAdapter}
     */
    private MouseAdapter selectSpanTableRow() {
        return new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int row = table.getSelectedRow();
                    if (row >= 0) {
                        DistrSpan distrSpan = tableModel.getList().get(row);
                        List<LocomotiveType> locomotiveTypeList = distrEditEntity.getDistrEntity().getListDistrLoc();
                        SpanTimeFXDialog spanTimeDialog = GDPEdit.gdpEdit.getDialogManager().getSpanTimeFXDialog();
                        spanTimeDialog.setModal(true);
                        spanTimeDialog.setDialogContent(distrSpan, locomotiveTypeList);
                        spanTimeDialog.setVisible(true);
                    }
                }
            }
        };
    }

    public void setDistrEditEntity(DistrEditEntity distrEditEntity) {
        this.distrEditEntity = distrEditEntity;
        updateSpans();
    }

    private void updateSpans() {
        if (distrEditEntity != null) {
            List<DistrSpan> list = new ArrayList<>(distrEditEntity.getDistrEntity().getListSp());
            Collections.sort(list, new ComparatorSpanByNum());
            tableModel.setList(list);
            table.repaint();
        }else{
            tableModel.setList(null);
            table.repaint();
        }
    }
}
