package com.gmail.alaerof.components.menu;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.components.mainframe.gdp.edit.GDPEditPanel;
import com.gmail.alaerof.javafx.dialog.scheduletoexcel.ScheduleToExcelFXDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 * Created by Paul M. Bui on 11.06.2018.
 */
public class MenuReports extends LoggerMenu {
    public MenuReports() {
        super();
        // �������� �����
        String title = bundle.getString("components.menu.reports");
        this.setText(title);
        {
            // ������� ���������� � Excel
            title = bundle.getString("components.menu.reports.ExportExcel");
            JMenuItem menuItem = new JMenuItem(title);
            this.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) { handleReports();}
            });
        }
    }

    private void handleReports() {
        try {
            GDPEditPanel editPanel = GDPEdit.gdpEdit.getCurrentGDPEditPanel();
            if (editPanel != null) {
                ScheduleToExcelFXDialog dialog = GDPEdit.gdpEdit.getDialogManager().getScheduleToExcelFXDialog();
                dialog.setModal(true);
                dialog.setDialogContent(editPanel);
                dialog.setVisible(true);
            }
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }
}
