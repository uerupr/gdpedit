package com.gmail.alaerof.javafx.dialog.energyfromtr.model;

import com.gmail.alaerof.dao.dto.Scale;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IScaleDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;
import com.gmail.alaerof.entity.DistrEditEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrSpanSt;
import com.gmail.alaerof.entity.DistrStation;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SpanStExModel {

    private DistrEditEntity distrEditEntity;
    private DistrSpanSt distrSpanSt;
    // Name
    private StringProperty nameSt;
    // CodeESR
    private StringProperty codeESR;
    // CodeESR2
    private StringProperty codeESR2;
    // IDspanSt
    private IntegerProperty idSpanSt;
    // spanName
    private StringProperty spanName;

    public SpanStExModel(DistrSpanSt distrSpanSt, DistrEditEntity distrEditEntity) throws DataManageException, ObjectNotFoundException {
        this.distrSpanSt = distrSpanSt;
        this.distrEditEntity = distrEditEntity;
        this.nameSt = new SimpleStringProperty(distrSpanSt.getSpanSt().getName());
        this.codeESR = new SimpleStringProperty(distrSpanSt.getSpanSt().getCodeESR());
        this.codeESR2 = new SimpleStringProperty(distrSpanSt.getSpanSt().getCodeESR2());
        this.idSpanSt = new SimpleIntegerProperty(distrSpanSt.getSpanSt().getIdSpan());
        int idSpan = distrSpanSt.getSpanSt().getIdSpan();
        IScaleDAO sd = GDPDAOFactoryCreater.getGDPDAOFactory().getScaleDAO();
        int idDistr = distrEditEntity.getIdDistr();
        Scale scale = sd.getScale(idDistr, idSpan);
        this.spanName = new SimpleStringProperty(scale.getName());
    }
    public StringProperty nameStProperty() {
        return nameSt;
    }
    public StringProperty codeESRProperty() {
        return codeESR;
    }
    public StringProperty codeESR2Property() {
        return codeESR2;
    }
    public IntegerProperty idSpanStProperty() {return idSpanSt;}
    public StringProperty spanNameProperty() {
        return spanName;
    }
}
