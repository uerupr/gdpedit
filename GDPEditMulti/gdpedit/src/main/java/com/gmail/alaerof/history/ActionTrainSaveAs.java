package com.gmail.alaerof.history;

import com.gmail.alaerof.components.mainframe.gdp.action.GDPActionPanel;
import com.gmail.alaerof.manager.LocaleManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gmail.alaerof.application.GDPEdit;
import com.gmail.alaerof.entity.train.TrainThread;
import com.gmail.alaerof.entity.train.TrainXMLHandler;

import java.util.ResourceBundle;

/**
 * @author Helen Yrofeeva
 */
public class ActionTrainSaveAs extends Action {
    private TrainThread train;
    private String oldCodeTR;
    private String newCodeTR;
    private static ResourceBundle bundle;

    public ActionTrainSaveAs(TrainThread train, String oldCodeTR, String newCodeTR) {
        actionName = "SaveAsTrain";
        this.train = train;
        this.oldCodeTR = oldCodeTR;
        this.newCodeTR = newCodeTR;
    }

    static {
        bundle = ResourceBundle.getBundle("bundles.history", LocaleManager.getLocale());
    }

    @Override
    protected void buildElement(Document doc, Element action) {
        Element tr = TrainXMLHandler.saveToXML(train, doc);
        action.appendChild(tr);
    }

    @Override
    protected boolean executeAction(int typeAction, Element oldValue, Element newValue) {
        boolean res = false;
        if (typeAction == sRestore && oldValue != null) {
            TrainXMLHandler.loadMainFromXML(train, oldValue, true);
            // �������� IDtrain �� ���� �����
            train.saveAs(train.getIdTrain(), train.getCode(), train.getNameTR());
            actionState = sRestore;
            res = true;
        }
        if (typeAction == sApply && newValue != null) {
            TrainXMLHandler.loadMainFromXML(train, newValue, true);
            // �������� IDtrain �� ���� �����
            train.saveAs(train.getIdTrain(), train.getCode(), train.getNameTR());
            actionState = sApply;
            res = true;
        }
        if(res){
            train.setTrainLocation();
            GDPActionPanel gdpActionPanel = GDPEdit.gdpEdit.getGDPActionPanel();
            gdpActionPanel.getActionListTrain().updateListTrain();
            gdpActionPanel.getActionListTrain().setCurrentTrainThread(train);
            gdpActionPanel.getActionListSt().setCurrentTrainThread(train);
            train.repaint();
        }
        return res;
    }

    @Override
    public String getActionDescription() {
        return oldCodeTR + bundle.getString("history.saveas") + newCodeTR;
    }

}
