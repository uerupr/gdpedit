package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import com.gmail.alaerof.dao.dto.gdp.DirTrain;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainExpense;
import com.gmail.alaerof.dao.dto.gdp.DistrTrainShowParam;
import com.gmail.alaerof.dao.dto.gdp.SpTOcc;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import java.util.Map;
import javax.swing.JComponent;

import com.gmail.alaerof.dao.dto.gdp.SpanTrain;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.gdp.DistrTrain;
import com.gmail.alaerof.dao.dto.gdp.OE;
import com.gmail.alaerof.dao.dto.gdp.TrainType;
import com.gmail.alaerof.dao.dto.gdp.LineStTrain;
import com.gmail.alaerof.dao.dto.gdp.TrainCode;
import com.gmail.alaerof.dao.dto.gdp.TrainHideSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainLineStyleSpan;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanColor;
import com.gmail.alaerof.dao.dto.gdp.TrainSpanInscr;
import com.gmail.alaerof.dao.dto.gdp.TrainStop;
import com.gmail.alaerof.dao.factory.GDPDAOFactoryCreater;
import com.gmail.alaerof.dao.interfacedao.IDistrTrainDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import com.gmail.alaerof.entity.DistrEntity;
import com.gmail.alaerof.entity.DistrPoint;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.entity.DistrSpanSt;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.history.History;
import com.gmail.alaerof.util.ColorConverter;
import com.gmail.alaerof.util.TimeConverter;

/**
 * ������������ ����� ������ �� �������
 * @author Helen Yrofeeva
 */

public class TrainThread {
    private static Logger logger = LogManager.getLogger(TrainThread.class);
    private boolean needCreateListElements = true;
    private JComponent owner;
    private boolean foreign = false;
    /** �������, ��� ����� ����� ���������� �� ������� */
    private boolean showInStation = false;

    /** ������ � ����� + ���������� */
    private DistrTrain distrTrain;
    /** �������� ������� (����� ��� ���� ����� �������) */
    private DistrEntity distrEntity;

    /** ������������ �������� ����� (�� �������� TrainSp � �� ������� TrainSt) */
    private List<TrainThreadElement> listElements;
    /** ������ ������� �����, ��������� ������ ���� */
    private TrainThreadElement currentElement;
    /** ���������� �������� ������ � ������� ������������� �� �.�. */
    private List<TimeStation> listTimeSt;
    /** ��������� �����������, ����� ��� ���� ����� �� ������� (������, �������) */
    private TrainThreadGeneralParams generalParams;
    /** ���� ������������� ����� ���� ����� */
    private ActionListener lineListener;
    /** ������� ����������� ������ */
    private boolean isSelected;
    /** ������� �������� � �������� �� ������� */
    private History history;
    /** ������ �� �������� */
    private List<TrainStDouble> listTrainStDouble = new ArrayList<>();

    public TrainThread(DistrTrain distrTrain, DistrEntity distrEntity, JComponent owner,
                       TrainThreadGeneralParams params, History history, boolean needCreateListElements) {
        this.distrTrain = distrTrain;
        this.distrEntity = distrEntity;
        this.generalParams = params;
        this.owner = owner;
        this.history = history;
        this.needCreateListElements = needCreateListElements;

        if (this.distrTrain.getIdDistr() != distrEntity.getIdDistr()) {
            foreign = true;
        }
        if (this.distrTrain.getTypeTR() != TrainType.gr) {
            showInStation = true;
        }

        GDPGridConfig conf = distrEntity.getConfigGDP();
        this.listTimeSt = createListTimeSt(conf, this.generalParams, distrTrain, distrEntity);
        this.listElements = createListElements(this.listTimeSt);
    }

    public History getHistory() {
        return history;
    }

    public DistrTrain getDistrTrain() {
        return distrTrain;
    }

    public boolean isHidden() {
        return distrTrain.getShowParam().isHidden();
    }

    public void setHidden(boolean hidden) {
        distrTrain.getShowParam().setHidden(hidden);
    }

    public void setHidden(boolean hidden, int idStB, int idStE) {
        // <= 0 ������ � ������, ����� ���� �����
        if (idStB <= 0 || idStE <= 0) {
            distrTrain.getShowParam().setHidden(hidden);
        } else {
            int index[] = getIndexBE(idStB, idStE);
            if (index[0] >= 0 && index[1] >= 0) {
                // ��� ���������� �������, ���������� ��� ������� ��� ��
                for (int i = index[0]; i < index[1]; i++) {
                    TimeStation st = listTimeSt.get(i);
                    if (hidden) {
                        distrTrain.setTrainHideSpan(distrEntity.getIdDistr(), st.station.getIdPoint(), ";hor;span");
                    } else {
                        distrTrain.removeTrainHideSpan(st.station.getIdPoint());
                    }
                }
            }
        }
        updateShowParamTimeSt();
    }

    public void setHidden(boolean hidden, int idStB, String shape) {
        TimeStation st = getTimeSt(idStB);
        if (hidden) {
            TrainHideSpan trh = distrTrain.getTrainHideSpan(idStB);
            String s = "";
            if (trh != null) {
                s = trh.getShape();
                if (!s.contains(shape)) {
                    s += ";" + shape;
                }
            } else {
                s += ";" + shape;
            }
            if (s.length() > 0) {
                distrTrain.setTrainHideSpan(distrEntity.getIdDistr(), st.station.getIdPoint(), s);
            }
        } else {
            distrTrain.removeTrainHideSpan(st.station.getIdPoint());
        }
        updateShowParamTimeSt();
    }

    public void setTrainLocation() {
        if (listElements != null) {
            for (TrainThreadElement trE : listElements) {
                trE.setLineBounds();
            }
        }
    }

    /**
     * ������� ����� ������ ��������� � ������� ��� �����
     */
    private List<TrainThreadElement> createListElements(List<TimeStation> listTimeSt) {
        List<TrainThreadElement>  listElements = new ArrayList<>();
        if (needCreateListElements) {
            GDPGridConfig config = distrEntity.getConfigGDP();
            int countSt = listTimeSt.size();
            if (countSt > 1) {
                for (int i = 0; i < (countSt - 1); i++) {
                    TimeStation st1 = listTimeSt.get(i);
                    TimeStation st2 = listTimeSt.get(i + 1);
                    TimeStation st3 = null;
                    if (i < (countSt - 2)) {
                        st3 = listTimeSt.get(i + 2);
                    }

                    if (i == 0) {
                        if (isStationInVisiblePartOfGDP(st1, st1.getDistrStation().isExpanded(), config)) {
                            TrainSt trSt = new TrainSt(owner, this, st1, null, st2);
                            listElements.add(trSt);
                        }
                    }

                    if (isStationInVisiblePartOfGDP(st1, false, config) ||
                            isStationInVisiblePartOfGDP(st2, false, config)) {
                        TrainSp trSp = new TrainSp(owner, this, st1, st2, i == (countSt - 2));
                        listElements.add(trSp);
                    }

                    if (isStationInVisiblePartOfGDP(st2, st1.getDistrStation().isExpanded(), config)) {
                        TrainSt trSt = new TrainSt(owner, this, st2, st1, st3);
                        listElements.add(trSt);
                    }
                }
            } else if (listTimeSt.size() > 0) {
                TimeStation st = listTimeSt.get(0);
                TrainSt trSt = new TrainSt(owner, this, st, null, null);
                listElements.add(trSt);
            }
        }
        return listElements;
    }

    /**
     * ��������� ��������� �� ������� � ������� ���������� ��� (� ������������ � hourB-hourE)
     * @param st {@link TimeStation}
     * @param checkForTexStop ����� �� ��������� ��� �������
     * @return boolean
     */
    private boolean isStationInVisiblePartOfGDP(TimeStation st, boolean checkForTexStop, GDPGridConfig config) {
        if (0 == config.hourB && 24 == config.hourE) {
            return true;
        }
        if (config.hourB == config.hourE) {
            return true;
        }
        int mGDPBegin = config.hourB * 60;
        int mGDPEnd = config.hourE * 60;
        int mStBegin = st.mOn;
        int mStEnd = st.mOff;
        // TexOn - ����� ������ � �����
        // TexOff - ����� � ����� ������������
        if (checkForTexStop && st.isTexOn() || st.isTexOff()) {
            mStBegin = st.mOnTexStop;
            mStEnd = st.mOffTexStop;
        }
        if (mGDPBegin < mGDPEnd) {
            if (mStBegin <= mStEnd && mStEnd < mGDPBegin) {
                return false;
            }
            if (mStBegin > mStEnd && mStBegin > mGDPBegin) {
                return false;
            }
        }
        if (mGDPBegin > mGDPEnd) {
            if (mStBegin <= mStEnd && mStEnd < mGDPBegin && mStBegin > mGDPEnd) {
                return false;
            }
        }
        return true;
    }

    /**
     * ������� ����� ������ ������� ��� �����
     */
    private List<TimeStation> createListTimeSt(GDPGridConfig conf, TrainThreadGeneralParams generalParams,
                                               DistrTrain distrTrain, DistrEntity distrEntity) {
        List<TimeStation> listTimeSt = new ArrayList<>();
        Collection<LineStTrain> listSt = distrTrain.getListLineStTrain();
        TimeStation timeSt;

        for (LineStTrain stTr : listSt) {
            try {
                int idSt = stTr.getIdSt();
                DistrStation st = distrEntity.getDistrStationFromVisible(idSt);
                if (st != null) {
                    timeSt = new TimeStation(st, stTr);
                    String tOn = stTr.getOccupyOn();
                    String tOff = stTr.getOccupyOff();
                    if (tOn != null || tOff != null) {
                        // ������� �����������
                        if (tOn == null || stTr.isNullOn()) {
                            timeSt.nullOn = true;
                            tOn = tOff;
                            stTr.setOccupyOn(tOn);
                            if (stTr.getTexStop() > 0) {
                                timeSt.mOffTexStop = TimeConverter.timeToMinutes(tOff);
                                timeSt.mOnTexStop = TimeConverter.addMinutes(timeSt.mOffTexStop, -stTr.getTexStop());
                                timeSt.xOnTexStop = TimeConverter.minutesToX(timeSt.mOnTexStop, generalParams, conf);
                                timeSt.xOffTexStop = TimeConverter.minutesToX(timeSt.mOffTexStop, generalParams, conf);
                            }
                        }
                        // ������� ��������
                        if (tOff == null || stTr.isNullOff()) {
                            tOff = tOn;
                            timeSt.nullOff = true;
                            stTr.setOccupyOff(tOff);
                            if (stTr.getTexStop() > 0) {
                                timeSt.mOnTexStop = TimeConverter.timeToMinutes(tOn);
                                timeSt.mOffTexStop = TimeConverter.addMinutes(timeSt.mOnTexStop, stTr.getTexStop());
                                timeSt.xOnTexStop = TimeConverter.minutesToX(timeSt.mOnTexStop, generalParams, conf);
                                timeSt.xOffTexStop = TimeConverter.minutesToX(timeSt.mOffTexStop, generalParams, conf);
                            }
                        }
                        timeSt.mOn = TimeConverter.timeToMinutes(tOn);
                        timeSt.mOff = TimeConverter.timeToMinutes(tOff);
                        timeSt.xOn = TimeConverter.minutesToX(timeSt.mOn, generalParams, conf);
                        timeSt.xOff = TimeConverter.minutesToX(timeSt.mOff, generalParams, conf);
                        // ������ ����������� ������ �� ���� �������� � ������� � ������
                        insertTimeSt(listTimeSt, timeSt);
                    }
                }
                // �������� �������� ���������� ������� ������� ��� ����������� �������������
                updateOrderTimeSt(listTimeSt);
                updateShowParamTimeSt(listTimeSt);
            } catch (Exception e) {
                logger.error(e.toString(), e);
            }
        }
        return listTimeSt;
    }

    public void updateShowParamTimeSt() {
        updateShowParamTimeSt(this.listTimeSt);
    }

    public void updateShowParamTimeSt(List<TimeStation> listTimeSt) {
        for (int i = 0; i < listTimeSt.size(); i++) {
            TimeStation timeSt = listTimeSt.get(i);
            int idSt = timeSt.station.getStation().getIdSt();
            // ����������� ������� ����� �������
            TrainCode tc = distrTrain.getTrainCode(idSt);
            if (tc != null) {
                timeSt.showCode = tc.getViewCode();
            } else {
                timeSt.showCode = -1;
            }
            // �������� ����� ����� �������
            TrainHideSpan th = distrTrain.getTrainHideSpan(idSt);
            if (th != null) {
                timeSt.hideHor = th.isHor();
                timeSt.hideSpan = th.isSpan();
            } else {
                timeSt.hideHor = false;
                timeSt.hideSpan = false;
            }
            // ����� ����������� ����� ����� �������
            timeSt.lineStyleSpan = LineStyle.getLineStyle(distrTrain.getShowParam().getLineStyle());
            timeSt.lineStyleHor = LineStyle.getLineStyle(distrTrain.getShowParam().getLineStyle());
            TrainLineStyleSpan ts = distrTrain.getTrainLineStyleSpan(idSt);
            if (ts != null) {
                // ���� ����� ��������� �� ������ ���� ����� ��
                // ������� ���� ����� �� �����
                if (timeSt.lineStyleSpan.equals(ts.getLineStyle())) {
                    distrTrain.removeTrainLineStyleSpan(idSt);
                } else {
                    if (ts.isHor()) {
                        timeSt.lineStyleHor = LineStyle.getLineStyle(ts.getLineStyle());
                    } else {
                        timeSt.lineStyleSpan = LineStyle.getLineStyle(ts.getLineStyle());
                    }
                }
            }
            // ���� ����� ����� �������
            // �� ��������� ����� ����� ���� �����
            TrainSpanColor clr = distrTrain.getTrainSpanColor(idSt);
            int trainClr = distrTrain.getShowParam().getColorTR();
            timeSt.colorTR = ColorConverter.convert(trainClr);
            if (clr != null) {
                // ���� ���� ������� ��������� � ������ ���� �����
                // �� ������� �� �����
                if (clr.getColorTR() == trainClr) {
                    distrTrain.removeTrainSpanColor(idSt);
                } else {
                    timeSt.colorTR = ColorConverter.convert(clr.getColorTR());
                }
            }
            // ������� ����� ����� �������
            TrainSpanInscr inscrLine = distrTrain.getTrainSpanInscr(idSt);
            String trainInscr = distrTrain.getShowParam().getAinscr();
            if (inscrLine != null) {
                // ���� ������� ������� ��������� � �������� ����
                // �����
                // ��� ��� ������, �� ������� �� �����
                if (inscrLine.getAInscr().equals(trainInscr) || "".equals(inscrLine.getAInscr())) {
                    distrTrain.removeTrainSpanInscr(idSt);
                } else {
                    timeSt.ainscr = inscrLine.getAInscr();
                }
            }
        }
    }

    /* ��������� �������� ���������� ������� ������� */
    private void updateOrderTimeSt(List<TimeStation> list) {
        for (TimeStation ts : list) {
            int index = list.indexOf(ts);
            ts.order = index;
            ts.last = false;
        }
        if (list.size() > 0) {
            list.get(list.size() - 1).last = true;
        }
    }

    private void insertTimeSt(List<TimeStation> listTimeSt, TimeStation timeSt) {
        int t1 = timeSt.getHourOff();
        int m1 = timeSt.mOff;
        if (t1 == -1) {
            t1 = timeSt.getHourOn();
        }
        int i = listTimeSt.size();
        int j = i - 1;
        while (j >= 0) {
            TimeStation ts = listTimeSt.get(j);
            int t2 = ts.getHourOn();
            int m2 = ts.mOn;
            if (t2 == -1) {
                t2 = ts.getHourOff();
            }
            if (m1 < m2 && (t2 - t1) < 12) {
                i = j;
            }
            if (m1 > m2 && (t1 - t2) >= 12) {
                i = j;
            }
            j--;
        }
        // timeSt.order = i;
        listTimeSt.add(i, timeSt);
    }

    public boolean isForeign() {
        return foreign;
    }

    public long getIdTrain() {
        return distrTrain.getIdTrain();
    }

    public int getIdDistr() {
        return distrTrain.getIdDistr();
    }

    public DistrEntity getDistrEntity() {
        return distrEntity;
    }

    public TrainThreadGeneralParams getGeneralParams() {
        return generalParams;
    }

    public void setGeneralParams(TrainThreadGeneralParams generalParams) {
        this.generalParams = generalParams;
    }

    public List<TimeStation> getListTimeSt() {
        return listTimeSt;
    }

    public int getColor() {
        return distrTrain.getShowParam().getColorTR();
    }

    public String getCode() {
        return distrTrain.getCodeTR();
    }

    public String getAInscr() {
        return distrTrain.getShowParam().getAinscr();
    }

    @Override
    public String toString() {
        return "TrainThread [foreign=" + foreign + ", codeTR=" + distrTrain.getCodeTR() + "]";
    }

    public ActionListener getLineListener() {
        return lineListener;
    }

    public void setLineListener(ActionListener lineListener) {
        this.lineListener = lineListener;
        if (listElements != null) {
            for (TrainThreadElement el : listElements) {
                el.updateLineListener();
            }
        }
    }

    public String getNameTR() {
        return distrTrain.getNameTR();
    }

    public TimeStation getTimeSt(int idSt) {
        TimeStation st = null;
        for (TimeStation ss : listTimeSt) {
            if (ss.station.getStation().getIdSt() == idSt) {
                st = ss;
                break;
            }
        }
        return st;
    }

    public TrainStop getTrainStopByID(int id) {
        Map<Integer, TrainStop> map = distrTrain.getMapTrainStop();
        if (map != null) {
            return map.get(id);
        }
        return null;
    }

    public OE getOe() {
        return distrTrain.getOe();
    }

    /**
     * ��������� ��������� ������, ���� � ��� ������� ������ �������� ���
     * ������� �/� ������
     * @param listStTableData
     */
    private void applyThreadByTableForeign(ArrayList<TrainStTableData> listStTableData) {
        // ������� ����� ��������, �� ������ �������� ��� ������� ������
        for (TrainStTableData stTableData : listStTableData) {
            if (stTableData.distrpoint.getTypePoint() == DistrPoint.TypePoint.SeparatePoint) {
                DistrStation ds = (DistrStation) stTableData.distrpoint;
                TimeStation ts = getTimeSt(ds.getIdPoint());
                if (ts != null) {
                    LineStTrain lstTR = ts.lineStTrain;
                    String tOn = stTableData.tOn;
                    String tOff = stTableData.tOff;
                    if (tOn == null) {
                        tOn = tOff;
                    }
                    if (tOff == null) {
                        tOff = tOn;
                    }
                    lstTR.setOccupyOn(tOn);
                    lstTR.setOccupyOff(tOff);
                    lstTR.setOccupyOn(tOn);
                    lstTR.setOccupyOff(tOff);
                    int tstop = 0;
                    if (tOn != null && tOff != null) {
                        tstop = TimeConverter.minutesBetween(tOn, tOff);
                    }
                    lstTR.setTstop(tstop);
                    lstTR.setSk_ps(stTableData.sk_ps);
                    lstTR.setTexStop(stTableData.texStop);
                    int idLineStO = lstTR.getIdLineSt();
                    int idLineStN = ds.getLineID(stTableData.lineSt);
                    lstTR.setIdLineSt(idLineStN);
                    if (idLineStN != idLineStO) {
                        updateLineForDouble(idLineStN, ds.getIdPoint());
                    }
                }
            } else {
                distrTrain.setTrainStop(stTableData, stTableData.distrpoint.getIdPoint());
            }
        }
    }

    /**
     * ��������� ���������� ������ � �����. �� ������� �������� �� ��������
     * @param listStTableData ������ ������� � ����� �����������
     * @param town ���� �� � ������ �.�. (���� �� ���������)
     */
    public void applyThreadByTable(ArrayList<TrainStTableData> listStTableData, boolean town) {
        if (foreign) {
            applyThreadByTableForeign(listStTableData);
            setTrainLocation();
        }
        // ������ ��� �����
        if (!foreign && listStTableData.size() > 0) {
            // ���������� ������ ������ TimeStation � ����� � ������� �� �������
            // � ������� ������������� �������
            ArrayList<TimeStation> newListTimeSt = new ArrayList<>();
            ArrayList<LineStTrain> listLineStTrains = new ArrayList<>();
            GDPGridConfig conf = distrEntity.getConfigGDP();
            for (TrainStTableData stTableData : listStTableData) {
                if (stTableData.distrpoint.getTypePoint() == DistrPoint.TypePoint.SeparatePoint) {
                    // ������ ��� ���������� �������
                    DistrStation ds = (DistrStation) stTableData.distrpoint;
                    LineStTrain lstTR = new LineStTrain();
                    listLineStTrains.add(lstTR);
                    TimeStation timeSt = new TimeStation(ds, lstTR);
                    String tOn = stTableData.tOn;
                    String tOff = stTableData.tOff;
                    if (tOn == null) {
                        tOn = tOff;
                    }
                    if (tOff == null) {
                        tOff = tOn;
                    }
                    lstTR.setOccupyOn(tOn);
                    lstTR.setOccupyOff(tOff);
                    int tstop = 0;
                    if (tOn != null && tOff != null) {
                        tstop = TimeConverter.minutesBetween(tOn, tOff);
                    }
                    lstTR.setTstop(tstop);

                    lstTR.setIdSt(ds.getIdPoint());
                    lstTR.setIdTrain(this.getIdTrain());
                    lstTR.setSk_ps(stTableData.sk_ps);
                    lstTR.setTexStop(stTableData.texStop);

                    int idLineStO = lstTR.getIdLineSt();
                    int idLineStN = ds.getLineID(stTableData.lineSt);
                    lstTR.setIdLineSt(idLineStN);
                    if (idLineStN != idLineStO) {
                        updateLineForDouble(idLineStN, ds.getIdPoint());
                    }

                    timeSt.mOn = TimeConverter.timeToMinutes(tOn);
                    timeSt.mOff = TimeConverter.timeToMinutes(tOff);
                    timeSt.xOn = TimeConverter.minutesToX(timeSt.mOn, generalParams, conf);
                    timeSt.xOff = TimeConverter.minutesToX(timeSt.mOff, generalParams, conf);

                    // ������ ����������� ������ �� ���� �������� � �������
                    // � ������
                    insertTimeSt(newListTimeSt, timeSt);
                }

            }
            // �������� �������� ���������� ������� ������� ��� �����������
            // �������������
            updateOrderTimeSt(newListTimeSt);

            // �������� ������������ null �������� � texStop ��� ������ � ��������� �������
            if (listStTableData.size() > 1) {
                TimeStation timeSt = newListTimeSt.get(0);
                if (timeSt.mOn == timeSt.mOff) {
                    timeSt.nullOn = true;
                    timeSt.getLineStTrain().setNullOn(true);
                }
                if (timeSt.lineStTrain.getTexStop() > 0) {
                    timeSt.nullOn = true;
                    timeSt.getLineStTrain().setNullOn(true);
                    timeSt.xOn = timeSt.xOff;
                    timeSt.lineStTrain.setOccupyOn(timeSt.lineStTrain.getOccupyOff());
                    timeSt.mOffTexStop = TimeConverter.timeToMinutes(timeSt.lineStTrain.getOccupyOff());
                    timeSt.mOnTexStop = timeSt.mOffTexStop - timeSt.lineStTrain.getTexStop();
                    timeSt.xOnTexStop = TimeConverter.minutesToX(timeSt.mOnTexStop, generalParams, conf);
                    timeSt.xOffTexStop = TimeConverter.minutesToX(timeSt.mOffTexStop, generalParams, conf);
                }
                timeSt = newListTimeSt.get(newListTimeSt.size() - 1);
                if (timeSt.mOn == timeSt.mOff) {
                    timeSt.nullOff = true;
                    timeSt.getLineStTrain().setNullOff(true);
                }
                if (timeSt.lineStTrain.getTexStop() > 0) {
                    timeSt.nullOff = true;
                    timeSt.getLineStTrain().setNullOff(true);
                    timeSt.xOff = timeSt.xOn;
                    timeSt.lineStTrain.setOccupyOff(timeSt.lineStTrain.getOccupyOn());
                    timeSt.mOnTexStop = TimeConverter.timeToMinutes(timeSt.lineStTrain.getOccupyOn());
                    timeSt.mOffTexStop = timeSt.mOnTexStop + timeSt.lineStTrain.getTexStop();
                    timeSt.xOnTexStop = TimeConverter.minutesToX(timeSt.mOnTexStop, generalParams, conf);
                    timeSt.xOffTexStop = TimeConverter.minutesToX(timeSt.mOffTexStop, generalParams, conf);
                }
            }

            // �������� ������ dto ���������� ������
            distrTrain.fillLineStTrMap(listLineStTrains);

            // ������� ������ ����� � ���� ���
            for (TrainThreadElement elem : listElements) {
                elem.removeFromOwner();
            }

            if (owner != null) {
                owner.repaint();
            }

            // ����� ��������� ���� �������� ������� � ������ Null �����
            if (newListTimeSt.size() > 1) {
                newListTimeSt.get(0).lineStTrain.setOccupyOn(null);
                newListTimeSt.get(newListTimeSt.size() - 1).lineStTrain.setOccupyOff(null);
            }
            // ������� ����� ������ ������� ��� �����
            this.listTimeSt = createListTimeSt(conf, this.generalParams, distrTrain, distrEntity);
            // ������� ����� ������ ��������� ��� �����
            this.listElements = createListElements(this.listTimeSt);
            // ������ ����� ������ ���������
            ActionListener listener = this.getLineListener();
            this.setLineListener(listener);
            this.repaint();

            // ��������� ������� �����/������ � ������� (��)
            setFirstLastRB();

            if (town) {
                // ���������� ���������� �� �.�.
                // ������� �������� ��������� � ��������� �����
                HashSet<Integer> setID = new HashSet<>();
                for (TrainStTableData stTableData : listStTableData) {
                    if (stTableData.distrpoint.getTypePoint() == DistrPoint.TypePoint.StopPoint) {
                        distrTrain.setTrainStop(stTableData, stTableData.distrpoint.getIdPoint());
                        DistrSpanSt spSt = (DistrSpanSt) stTableData.distrpoint;
                        Integer id = spSt.getIdPoint();
                        setID.add(id);
                    }
                }
                // ����� ������� ������
                Collection<TrainStop> list = distrTrain.getTrainStops();
                HashSet<Integer> setExID = new HashSet<>();
                if (list != null) {
                    Iterator<TrainStop> iterator = list.iterator();
                    while (iterator.hasNext()) {
                        TrainStop trStop = iterator.next();
                        Integer id = trStop.getIdSpanst();
                        if (!setID.contains(id)) {
                            setExID.add(id);
                        }
                    }
                }
                for (Integer id : setExID) {
                    distrTrain.removeTrainStop(id);
                }
            }
            // ���������� ��������� ��������� SpTOcc � ������������ � ��������� �� ��������
            updateSpTOcc(listTimeSt, distrTrain, distrEntity);
        }
    }

    public void setLineStTr(int idSt, int idLineSt) {
        TimeStation timeSt = this.getTimeSt(idSt);
        int idLine = timeSt.getLineStTrain().getIdLineSt();
        if (idLineSt != idLine) {
            timeSt.getLineStTrain().setIdLineSt(idLineSt);
            updateShowParamTimeSt();
            setTrainLocation();
        }
    }

    public void removeFromOwner() {
        for (TrainThreadElement elem : listElements) {
            elem.removeFromOwner();
        }
        if (owner != null) {
            owner.repaint();
        }
    }

    public void addToOwner() {
        for (TrainThreadElement elem : listElements) {
            elem.addToOwner();
        }
        if (owner != null) {
            owner.repaint();
        }
    }

    private void setFirstLastRB() {
        if (listTimeSt != null) {
            TimeStation firstSt = null;
            TimeStation lastSt = null;
            for (TimeStation st : listTimeSt) {
                if (st.station.getStation().isBr()) {
                    if (firstSt == null) {
                        firstSt = st;
                    }
                    lastSt = st;
                }
            }
            if (firstSt != null) {
                distrTrain.settOn(firstSt.getLineStTrain().getOccupyOn());
                distrTrain.settOff(firstSt.getLineStTrain().getOccupyOff());
            }
            if (lastSt != null) {
                distrTrain.settOn_e(lastSt.getLineStTrain().getOccupyOn());
                distrTrain.settOff_e(lastSt.getLineStTrain().getOccupyOff());
            }
            if (listTimeSt.size() > 0) {
                distrTrain.setOccupyOff(listTimeSt.get(0).lineStTrain.getOccupyOff());
            }
        }
    }

    public int getWidthTrain() {
        return distrTrain.getShowParam().getWidthTR();
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        this.repaint();
    }

    public void repaint() {
        for (TrainThreadElement el : listElements) {
            el.repaint();
        }
    }

    public boolean isShowInStation() {
        return showInStation;
    }

    public void setShowInStation(boolean showInStation) {
        this.showInStation = showInStation;
    }

    public void saveGDPtoDB(boolean onlyShowParam) {
        IDistrTrainDAO dao = GDPDAOFactoryCreater.getGDPDAOFactory().getDistrTrainDAO();
        try {
            updateSpTOcc();

            if (!onlyShowParam) {
                dao.clearDistrGDP(distrEntity.getIdDistr(), null, this.getIdTrain(), false, false);
                dao.saveTrainGDP(this.getDistrTrain(), distrEntity.getIdDistr());
            }

            if (!this.getDistrTrain().isOnlyStation()) {
                dao.clearDistrGDP_ShowParam(distrEntity.getIdDistr(), this.getIdTrain());
                dao.saveTrainGDP_ShowParam(this.getDistrTrain(), distrEntity.getIdDistr());
            }

        } catch (DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public TrainThreadElement getCurrentElement() {
        return currentElement;
    }

    public void setCurrentElement(TrainThreadElement currentElement) {
        this.currentElement = currentElement;
    }

    /**
     * ����� ����� ������ �� �������
     * @param moveTime ����� ������ � �������
     * @param beginIndex ������ ������� ������ ������
     * @param endIndex ������ ������� ����� ������
     * @param oneTime 0 - ���, 1 - ������ ��������, 2 - ������ �����������
     */
    private void moveInMode(int moveTime, int beginIndex, int endIndex, int oneTime) {
        Map<Integer, TrainStop> mapTrainStop = distrTrain.getMapTrainStop();
        for (int i = beginIndex; i < endIndex; i++) {
            int one = 0;
            TimeStation st1 = listTimeSt.get(i);
            if (i == beginIndex) {
                if (oneTime == 2) {
                    one = 2;
                } else {
                    one = 0;
                }
            }
            st1.moveTime(moveTime, generalParams, distrEntity.getConfigGDP(), one);
            TimeStation st2 = null;
            if (i < endIndex) {
                st2 = listTimeSt.get(i + 1);
            }
            if (st2 != null) {
                if ((i + 1) == endIndex) {
                    if (oneTime == 1) {
                        one = 1;
                    } else {
                        one = 0;
                    }
                    st2.moveTime(moveTime, generalParams, distrEntity.getConfigGDP(), one);
                }

                if (mapTrainStop != null) {
                    int idStB = st1.getLineStTrain().getIdSt();
                    int idStE = st2.getLineStTrain().getIdSt();
                    DistrSpan span = distrEntity.getDistrSpan(idStB, idStE);
                    if (span != null) {
                        List<SpanSt> list = span.getSpanScale().getSpanSt();
                        for (int j = 0; j < list.size(); j++) {
                            int idSpanSt = list.get(j).getIdSpanst();
                            TrainStop trainStop = mapTrainStop.get(idSpanSt);
                            if (trainStop != null) {
                                String tOn = trainStop.getOccupyOn();
                                String tOff = trainStop.getOccupyOff();
                                int mOn = TimeConverter.timeToMinutes(tOn);
                                int mOff = TimeConverter.timeToMinutes(tOff);
                                mOn = TimeConverter.addMinutes(moveTime, mOn);
                                mOff = TimeConverter.addMinutes(moveTime, mOff);
                                tOn = TimeConverter.minutesToTime(mOn);
                                tOff = TimeConverter.minutesToTime(mOff);
                                trainStop.setOccupyOn(tOn);
                                trainStop.setOccupyOff(tOff);
                            }
                        }
                    }
                }
            }
        }
        this.setTrainLocation();
    }

    /**
     * ��������� ����� ������
     * @param moveMode ����� ��������� ����� ������
     * @param moveTime ����� � ������� ��� ���������
     */
    public void move(int moveMode, int moveTime) {
        int beginIndex = 0;
        int endIndex = listTimeSt.size() - 1;
        if (moveMode == 1) {
            moveInMode(moveTime, beginIndex, endIndex, 0);
        } else {
            if (currentElement != null) {
                TimeStation timeSt = currentElement.getClickedTimeSt();
                if (timeSt != null) {
                    switch (moveMode) {
                    case 2:
                        beginIndex = listTimeSt.indexOf(timeSt);
                        if (timeSt.getClickPos() == 1) {
                            // ���� ����� �������
                            beginIndex++;
                        }
                        moveInMode(moveTime, beginIndex, endIndex, 0);
                        break;
                    case 3:
                        beginIndex = 0;
                        endIndex = listTimeSt.indexOf(timeSt);
                        if (timeSt.getClickPos() != 1) {
                            // ���� �� �������
                            endIndex--;
                        }
                        moveInMode(moveTime, beginIndex, endIndex, 0);
                        break;
                    case 4:
                        timeSt.moveTime(moveTime, generalParams, distrEntity.getConfigGDP(), 0);
                        this.setTrainLocation();
                        break;
                    case 5:
                        int time = moveTime;
                        int oneTime;
                        if (timeSt.getClickPos() == 1) {
                            // ���� ����� �������
                            oneTime = 2;
                            beginIndex = listTimeSt.indexOf(timeSt);
                            endIndex = listTimeSt.size() - 1;
                        } else {
                            // ���� �� �������
                            oneTime = 1;
                            beginIndex = 0;
                            endIndex = listTimeSt.indexOf(timeSt);
                        }
                        // oneTime 0 - ���, 1 - ������ ��������, 2 - ������ �����������
                        moveInMode(time, beginIndex, endIndex, oneTime);
                        break;
                    default:
                        break;
                    }
                }
            }
        }
        if(!foreign) {
            // ��������� ������� �����/������ � ������� (��)
            setFirstLastRB();
            // ���������� ��������� ��������� SpTOcc � ������������ � ��������� �� ��������
            updateSpTOcc(listTimeSt, distrTrain, distrEntity);
        }
    }

    public int getCodeInt() {
        return distrTrain.getCodeTRInt();
    }

    public boolean equalsByCodeName(TrainThread t) {
        return (this.getCodeInt() == t.getCodeInt()) && (this.getNameTR().equals(t.getNameTR()));
    }

    public boolean equalsByCodeName(int code, String name) {
        return (this.getCodeInt() == code) && (this.getNameTR().equals(name));
    }

    private int[] getIndexBE(int idStB, int idStE) {
        int[] index = new int[2];
        index[0] = -1;
        index[1] = -1;
        int i1 = -1;
        int i2 = -1;
        TimeStation st = this.getTimeSt(idStB);
        if (st != null) {
            i1 = listTimeSt.indexOf(st);
        }
        st = this.getTimeSt(idStE);
        if (st != null) {
            i2 = listTimeSt.indexOf(st);
        }
        index[0] = Math.min(i1, i2);
        index[1] = Math.max(i1, i2);
        return index;
    }

    public void setColor(Color color, int idStB, int idStE) {
        int colorTR = ColorConverter.convert(color);
        // <= 0 ������ � ������, ����� �� ���������� �������
        if (idStB <= 0 || idStE <= 0) {
            distrTrain.getShowParam().setColorTR(colorTR);
        } else {
            int index[] = getIndexBE(idStB, idStE);
            if (index[0] >= 0 && index[1] >= 0) {
                // ��� ���������� �����������, ���������� ��� ������� ��� ��
                for (int i = index[0]; i < index[1]; i++) {
                    TimeStation st = listTimeSt.get(i);
                    distrTrain.setTrainSpanColor(distrEntity.getIdDistr(), st.station.getIdPoint(), colorTR);
                }
            }
        }
        updateShowParamTimeSt();
    }

    public void setColor(Color color, int idStB) {
        int colorTR = ColorConverter.convert(color);
        TimeStation st = getTimeSt(idStB);
        if (st != null) {
            distrTrain.setTrainSpanColor(distrEntity.getIdDistr(), st.station.getIdPoint(), colorTR);
            updateShowParamTimeSt();
        }
    }

    public void setWidth(int width) {
        distrTrain.getShowParam().setWidthTR(width);
        updateShowParamTimeSt();
    }

    public void setViewCode(boolean view, int idStB) {
        if (idStB > 0) {
            TimeStation st = getTimeSt(idStB);
            if (st != null) {
                if (view) {
                    distrTrain.setTrainCode(st.station.getIdPoint(), 0, distrEntity.getIdDistr());
                } else {
                    distrTrain.removeTrainCode(st.station.getIdPoint());
                }
            }
        }
        updateShowParamTimeSt();
    }

    public void setViewCode(boolean view, int viewCode, int idStB) {
        TimeStation st = getTimeSt(idStB);
        if (view) {
            distrTrain.setTrainCode(st.station.getIdPoint(), viewCode, distrEntity.getIdDistr());
        } else {
            distrTrain.removeTrainCode(st.station.getIdPoint());
        }
        updateShowParamTimeSt();
    }

    public void setAInscr(String value) {
        distrTrain.getShowParam().setAinscr(value);
        updateShowParamTimeSt();
    }

    public void setAInscrSelf(String value, int idStB) {
        TimeStation st = getTimeSt(idStB);
        if (distrTrain.getShowParam().getAinscr().equals(value)) {
            distrTrain.removeTrainSpanInscr(st.station.getIdPoint());
        } else {
            distrTrain.setTrainSpanInscr(st.station.getIdPoint(), value, distrEntity.getIdDistr());
        }
        updateShowParamTimeSt();
    }

    public void setLineStyle(LineStyle style) {
        distrTrain.getShowParam().setLineStyle(style.getLineStyleCode());
        distrTrain.setMapTrainLineStyleSpan(new HashMap<Integer, TrainLineStyleSpan>());
        updateShowParamTimeSt();
    }

    public void setLineStyle(LineStyle style, String shape, int idStB) {
        TimeStation st = getTimeSt(idStB);
        if (st != null) {
            if (style == LineStyle.getLineStyle(distrTrain.getShowParam().getLineStyle())) {
                distrTrain.removeTrainLineStyleSpan(idStB);
            } else {
                TrainLineStyleSpan trh = distrTrain.getTrainLineStyleSpan(idStB);
                String s = "";
                if (trh != null) {
                    s = trh.getShape();
                    if (!s.contains(shape)) {
                        s += ";" + shape;
                    }
                } else {
                    s += ";" + shape;
                }
                if (s.length() > 0) {
                    distrTrain.setTrainLineStyleSpan(idStB, style.getLineStyleCode(), s, distrEntity.getIdDistr());
                }
            }
            updateShowParamTimeSt();
        }
    }

    /** ���������� ������ ������ �� �������� ������� */
    public ArrayList<TrainStDouble> getTrainStDouble(int idSt) {
        ArrayList<TrainStDouble> list = new ArrayList<>();
        for (int i = 0; i < listTrainStDouble.size(); i++) {
            TrainStDouble st = listTrainStDouble.get(i);
            if (st.getStationBegin().station.getIdPoint() == idSt) {
                list.add(st);
            }
        }
        return list;
    }

    /** ��������� ������ �� ������� � ����� ������ */
    public void addTrainStDouble(TrainStDouble trStD) {
        listTrainStDouble.add(trStD);
    }

    /** ������� ������ �� ������ */
    public void removeTrainStDouble(TrainStDouble trStD) {
        listTrainStDouble.remove(trStD);
    }

    /** ������������� ���� ��� ������ */
    public void updateLineForDouble(int idLineSt, int idSt) {
        ArrayList<TrainStDouble> list = getTrainStDouble(idSt);
        for (int i = 0; i < list.size(); i++) {
            TrainStDouble st = list.get(i);
            st.setIdLineSt(idLineSt, this.getIdTrain());
        }
    }

    /**
     * ��������� ����� ������ �� �����
     * ����� ��� �� ��� � �� ������ ��� (�.�. ���������)
     *
     * todo ������ �����
     *
     * @param grGDP �����
     */
    public void drawInPicture(Graphics grGDP) {
        for (int i = 0; i < listElements.size(); i++) {
            TrainThreadElement element = listElements.get(i);
            element.drawInPicture(grGDP);
        }
    }

    /**
     * ��������� ����� ������ �� ����� � ����������� ��������� ������������� �������
     * ��� ������� ��������� �������������� ��������� ���� ���������
     * @param grGDP �����
     * @param config �������� ������� �����!
     */
    public void drawInPicture(Graphics grGDP, GDPGridConfig config) {
        List<TimeStation> listTimeSt = createListTimeSt(config, this.generalParams, distrTrain, distrEntity);
        int countSt = listTimeSt.size();
        if (countSt > 1) {
            for (int i = 0; i < (countSt - 1); i++) {
                TimeStation st1 = listTimeSt.get(i);
                TimeStation st2 = listTimeSt.get(i + 1);
                TimeStation st3 = null;
                if (i < (countSt - 2)) {
                    st3 = listTimeSt.get(i + 2);
                }
                boolean st1Visible = isStationInVisiblePartOfGDP(st1, st1.getDistrStation().isExpanded(), config);
                boolean st2Visible = isStationInVisiblePartOfGDP(st2, st2.getDistrStation().isExpanded(), config);

                if (i == 0 && st1Visible) {
                    DrawLineUtil.drawTrainSt(grGDP, st1, null, st2, this, config);
                }

                if (st1Visible || st2Visible) {
                    DrawLineUtil.drawTrainSp(grGDP, st1, st2, i == (countSt - 2), this, config);
                }

                if (st2Visible) {
                    DrawLineUtil.drawTrainSt(grGDP, st2, st1, st3, this, config);
                }
            }
        } else if (listTimeSt.size() > 0) {
            TimeStation st = listTimeSt.get(0);
            DrawLineUtil.drawTrainSt(grGDP, st, null, null, this, config);
        }
    }

    /**
     * ���������� ��������� ��������� SpTOcc � ������������ � ��������� �� ��������
     */
    public void updateSpTOcc() {
        updateSpTOcc(listTimeSt, distrTrain, distrEntity);
    }

    /**
     * ���������� ��������� ��������� SpTOcc � ������������ � ��������� �� ��������
     * @param listTimeSt list TimeStation
     * @param distrTrain {@link DistrTrain} ��� ����� SpTOcc ��� ����������
     */
    private void updateSpTOcc(List<TimeStation> listTimeSt, DistrTrain distrTrain, DistrEntity distrEntity) {
        Map<Integer, SpTOcc> spTOccMap = distrTrain.getMapSpTOcc();
        spTOccMap.clear();
        for (int i = 0; i < listTimeSt.size() - 1; i++) {
            TimeStation stB = listTimeSt.get(i);
            TimeStation stE = listTimeSt.get(i + 1);
            int idStB = stB.station.getIdPoint();
            int idStE = stE.station.getIdPoint();
            DistrSpan distrSpan = distrEntity.getDistrSpan(idStB, idStE);
            if (distrSpan != null) {
                int idSpan = distrSpan.getSpanScale().getIdSpan();
                SpTOcc spTOcc = new SpTOcc();

                spTOcc.setIdSpan(idSpan);
                spTOcc.setIdTrain(distrTrain.getIdTrain());
                spTOcc.setIdDistr(distrTrain.getIdDistr());
                spTOcc.setOe(distrTrain.getOe().getString());
                spTOcc.setOccupyOn(stB.lineStTrain.getOccupyOff());
                spTOcc.setOccupyOff(stE.lineStTrain.getOccupyOn());
                // NextDay 0 ���� ��� �������� �� ����� ����� �� ���� ��������, 1 ���� ����
                int nextDay = stB.mOff < stE.mOn ? 0 : 1;
                spTOcc.setNextDay(nextDay);
                int tmove = TimeConverter.minutesBetween(stB.mOff, stE.mOn);
                spTOcc.setTmove(tmove);

                spTOccMap.put(idSpan, spTOcc);
            }
        }
    }

    public LineStyle getLineStyle() {
        LineStyle style = LineStyle.getLineStyle(getDistrTrain().getShowParam().getLineStyle());
        return style;
    }

    public void makeVisible() {
        needCreateListElements = true;
        this.listElements = createListElements(this.listTimeSt);
    }

    public void setOwner(JComponent owner) {
        this.owner = owner;
    }

    public void refreshLines() {
        removeFromOwner();
        GDPGridConfig conf = distrEntity.getConfigGDP();
        this.listTimeSt = createListTimeSt(conf, this.generalParams, distrTrain, distrEntity);
        this.listElements = createListElements(this.listTimeSt);
        ActionListener listener = this.getLineListener();
        this.setLineListener(listener);
    }

    public void saveAs(long newIdTrain, String newCode, String newNameTR) {
        distrTrain.setIdTrain(newIdTrain);
        distrTrain.setCodeTR(newCode);
        distrTrain.setNameTR(newNameTR);

        /** ���������� ������ �� ��� */
        // private List<DirTrain> dirTrainList = new ArrayList<>();
        distrTrain.getDirTrainList().forEach(item -> item.setIdTrain(newIdTrain));
        // Distr_Train_ShowParam
        // ������ �� �����
        /** ���������� �������������������� ������ �� �������, key = ExpenseKeyString = locType + EnergyType */
        // ������ �� �����
        /** ����� ���������� ������ �� �.�. �� �������, ���� IDst */
        // private Map<Integer, LineStTrain> mapLineStTr = new HashMap<>();
        distrTrain.getMapLineStTr().values().forEach(item -> item.setIdTrain(newIdTrain));
        /** ����� ���������� ������ �� o.�. �� �������, ���� IDspanst */
        // private Map<Integer, TrainStop> mapTrainStop = new HashMap<>();
        distrTrain.getMapTrainStop().values().forEach(item -> item.setIdTrain(newIdTrain));
        /** ����� ������� ��������� �������, ���� IDspan */
        // private Map<Integer, SpTOcc> mapSpTOcc = new HashMap<>();
        distrTrain.getMapSpTOcc().values().forEach(item -> item.setIdTrain(newIdTrain));
        /** ����� ����������� �������, ���� IDst */
        // private Map<Integer, TrainCode> mapTrainCode = new HashMap<>();
        distrTrain.getMapTrainCode().values().forEach(item -> item.setIdTrain(newIdTrain));
        /** ����� ������� �����, ���� IDst */
        // private Map<Integer, TrainHideSpan> mapTrainHideSpan = new HashMap<>();
        distrTrain.getMapTrainHideSpan().values().forEach(item -> item.setIdTrain(newIdTrain));
        /** ����� ����� �����, ���� IDst */
        // private Map<Integer, TrainLineStyleSpan> mapTrainLineStyleSpan = new HashMap<>();
        distrTrain.getMapTrainLineStyleSpan().values().forEach(item -> item.setIdTrain(newIdTrain));
        /** ����� ����� �����, ���� IDst */
        // private Map<Integer, TrainSpanColor> mapTrainSpanColor = new HashMap<>();
        distrTrain.getMapTrainSpanColor().values().forEach(item -> item.setIdTrain(newIdTrain));
        /** ����� �������� �����, ���� IDst */
        // private Map<Integer, TrainSpanInscr> mapTrainSpanInscr = new HashMap<>();
        distrTrain.getMapTrainSpanInscr().values().forEach(item -> item.setIdTrain(newIdTrain));
        /** ����� �������������� ������ ����, ���� IDspan */
        // private Map<Integer, SpanTrain> mapSpanTrain = new HashMap<>();
        distrTrain.getMapSpanTrain().values().forEach(item -> item.setIdTrain(newIdTrain));

        removeFromOwner();
        GDPGridConfig conf = distrEntity.getConfigGDP();
        this.listTimeSt = createListTimeSt(conf, this.generalParams, distrTrain, distrEntity);
        this.listElements = createListElements(this.listTimeSt);
        // ������ ����� ������ ���������
        ActionListener listener = this.getLineListener();
        this.setLineListener(listener);
    }

}
