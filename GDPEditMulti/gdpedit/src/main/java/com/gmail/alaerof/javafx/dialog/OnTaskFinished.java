package com.gmail.alaerof.javafx.dialog;

@FunctionalInterface
public interface OnTaskFinished {
    void doOnTaskFinished();
}
