package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import java.awt.Dimension;
import java.awt.Polygon;
/**
 * 
 * @author Helen Yrofeeva
 *
 */
public class LineLeft extends NewLine {
    private static final long serialVersionUID = 1L;

    public LineLeft(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams,
                    TrainThreadElement trainThreadElement) {
        super(generalParams, selfParams, trainThreadElement);
        direction = Direction.left;
    }

    @Override
    protected Polygon getPolygon() {
        int npoints = 10;
        int x[] = new int[npoints];
        int y[] = new int[npoints];
        int maxW = getSize().width; // ������ � ���������
        int maxH = getSize().height; // ������ � ���������
        int w = getOriginalSize().width; // ������ ��� ��������
        int h = getOriginalSize().height; // ������ ��� ��������
        double gg = Math.max(1, Math.sqrt(w * w + h * h));// ����������
        //����� ���������� ���� ������� �����������
        int timeShiftL = -5/*-13*/;
        //����� ���������� ���� ������� ��������
        int timeShiftA = 0/*6*/;
        int indentX = this.getIndent().width;
        x[0] = indentX; // ������ ���
        y[0] = 0; // ������ ���

        x[1] = x[0] + indentX;
        y[1] = y[0];

        int step = generalParams.codeStepO;
        if (step + getCaptionSize().width > gg) {
            step = 15;
        }
        double stepX = step * w / h;
        x[2] = (int) (x[1] + stepX);
        y[2] = y[1] + step;
        double chx = 0;
        double chy = 0;
        if (isCaption()) {
            chx = getCaptionSize().getHeight() * h / gg;
            chy = getCaptionSize().getHeight() * w / gg;
        }
        x[3] = (int) (x[2] + chx);
        y[3] = (int) (y[2] - chy);

        double cwx = getCaptionSize().getWidth() * w / gg;
        double cwy = getCaptionSize().getWidth() * h / gg;
        x[4] = (int) (x[3] + cwx);
        y[4] = (int) (y[3] + cwy);

        x[5] = (int) (x[4] - chx);
        y[5] = (int) (y[4] + chy);

        x[6] = maxW;
        y[6] = maxH;

        x[7] = x[6] - indentX; // ����� ���
        y[7] = y[6]; // ����� ���

        x[8] = x[7] - indentX;
        y[8] = y[6];

        x[9] = 0;
        y[9] = 0;

        alphaCaption = Math.asin(h / gg);

        captionPoint.x = (int) (x[0] + stepX) + 1;
        captionPoint.y = y[0] + step;
        begin.x = x[0];
        begin.y = y[0];
        end.x = x[7];
        end.y = y[7];

        pointTimeL.x = getTimeSize().width  + timeShiftL;
        pointTimeL.y = 3 + (int) (getTimeSize().height);
        pointTimeA.x = x[6] - (int) (getTimeSize().width) + timeShiftA;
        pointTimeA.y = y[6] - 3;

        captionPointGDP.x = (int) (x[0] + stepX) + 1 - indentX;
        captionPointGDP.y = y[0] + step;
        beginGDP.x = x[0] - indentX;
        beginGDP.y = y[0];
        endGDP.x = x[7] - indentX;
        endGDP.y = y[7];

        pointTimeLGDP.x = getTimeSize().width - indentX;
        pointTimeLGDP.y = 3 + (int) (getTimeSize().height);
        pointTimeAGDP.x = x[6] - (int) (getTimeSize().width) - indentX;
        pointTimeAGDP.y = y[6] - 3;
        return new Polygon(x, y, npoints);
    }

    @Override
    public Dimension getIndent() {
        //int w = Math.max(getOriginalSize().width, getCaptionSize().width);
        int w = getOriginalSize().width; // ������ ��� ��������
        int h = getOriginalSize().height; // ������ ��� ��������
        double gg = Math.max(1, Math.sqrt(h * h + w * w));
        int pw = (int) (ADD_WIDTH + selfParams.lineWidth);
        int indx = (int) (pw * gg / h);
        return new Dimension(indx + getTimeSize().width, 0);
    }

}
