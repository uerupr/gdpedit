package com.gmail.alaerof.javafx.dialog.spantime.view;

import com.gmail.alaerof.dao.dto.LocomotiveType;
import com.gmail.alaerof.dao.dto.SpanSt;
import com.gmail.alaerof.dao.dto.SpanStTime;
import com.gmail.alaerof.entity.DistrSpan;
import com.gmail.alaerof.javafx.control.editabletableview.EditableTableViewCreator;
import com.gmail.alaerof.javafx.dialog.spantime.model.SpanStModel;
import com.gmail.alaerof.javafx.dialog.spantime.model.SpanStTimeModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

public class SpanStLayoutController extends CommonDistrSpanController {
    /** ������ ��� �.�. */
    @FXML
    private BorderPane spanStTablePane;

    /** ������ ��� ������ ���� ����� �.�.*/
    @FXML
    private BorderPane spanStTimeTablePane;

    /** Span_St ������� �.�. */
    private TableView<SpanStModel> spanStTable;

    /** Span_StTime ������� ������ ���� ����� �.�. */
    private TableView<SpanStTimeModel> spanStTimeTable;

    /** ����� ���� �������� �� ������ ��� ������ ���� ����� �.�.*/
    @FXML
    private ComboBox<LocomotiveType> locomotiveTypeComboBox;

    @FXML
    private Label stBeginExpress;
    @FXML
    private Label stBeginESR;
    @FXML
    private Label stBeginESR2;
    @FXML
    private Label stBegin;
    @FXML
    private Label stBeginABS;
    @FXML
    private Label stEndExpress;
    @FXML
    private Label stEndESR;
    @FXML
    private Label stEndESR2;
    @FXML
    private Label stEnd;
    @FXML
    private Label stEndABS;

    private String okMessage;
    private String errMessage;

    /**
     * ������������� ������-�����������.
     * ���������� �� ������������� ������ initialize(URL location, ResourceBundle resources),
     * ������� � ���� ������� ���������� ����������� ��� �������� �� FXML
     */
    @FXML
    @Override
    protected void initialize() {
        logger.debug("@FXML initialize");
        okMessage = bundle.getString("javafx.dialog.spanst.ok");
        errMessage = bundle.getString("javafx.dialog.spanst.err");

        EditableTableViewCreator<SpanStModel> builderSt = new EditableTableViewCreator<>();

        spanStTable = builderSt.createEditableTableView();
        spanStTablePane.setCenter(spanStTable);
        String title = bundle.getString("javafx.dialog.spanst.table.n");
        spanStTable.getColumns().add(builderSt.createIntegerColumn(title, SpanStModel::numProperty, false));
        title = bundle.getString("javafx.dialog.spanst.table.express");
        spanStTable.getColumns().add(builderSt.createStringColumn(title, SpanStModel::expressProperty, false));
        title = bundle.getString("javafx.dialog.spanst.table.esr");
        spanStTable.getColumns().add(builderSt.createStringColumn(title, SpanStModel::esrProperty, false));
        title = bundle.getString("javafx.dialog.spanst.table.esr2");
        spanStTable.getColumns().add(builderSt.createStringColumn(title, SpanStModel::esr2Property, false));
        title = bundle.getString("javafx.dialog.spanst.table.name");
        spanStTable.getColumns().add(builderSt.createStringColumn(title, SpanStModel::nameProperty, false));
        title = bundle.getString("javafx.dialog.spanst.table.abs");
        spanStTable.getColumns().add(builderSt.createDoubleColumn(title, SpanStModel::absKMProperty, false));
        title = bundle.getString("javafx.dialog.spanst.table.stop");
        spanStTable.getColumns().add(builderSt.createIntegerColumn(title, SpanStModel::tStopProperty, true));

        EditableTableViewCreator<SpanStTimeModel> builderTm = new EditableTableViewCreator<>();
        spanStTimeTable = builderTm.createEditableTableView();
        spanStTimeTablePane.setCenter(spanStTimeTable);
        title = bundle.getString("javafx.dialog.spanst.table.n");
        spanStTimeTable.getColumns().add(builderTm.createIntegerColumn(title, SpanStTimeModel::numProperty, false));
        title = bundle.getString("javafx.dialog.spansttime.table.tmoveo");
        spanStTimeTable.getColumns().add(builderTm.createDoubleColumn(title, SpanStTimeModel::tMoveOProperty, true));
        title = bundle.getString("javafx.dialog.spansttime.table.tmovee");
        spanStTimeTable.getColumns().add(builderTm.createDoubleColumn(title, SpanStTimeModel::tMoveEProperty, true));

        // ��� ����� ���������� �� ��� �����.
        Callback<ListView<LocomotiveType>, ListCell<LocomotiveType>> factory = lv -> new ListCell<LocomotiveType>() {
            @Override
            protected void updateItem(LocomotiveType item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getLtype());
            }
        };
        locomotiveTypeComboBox.setCellFactory(factory);
    }

    /** {@inheritDoc} */
    @Override
    protected void updateModelWithContent() {
        logger.debug("updateModelWithContent");
        // Span_St
        List<SpanStModel> itemList = buildSpanStModelListFromDistrSpan(distrSpan);
        spanStTable.setItems(FXCollections.observableArrayList(itemList));
        // LocomotiveType
        List<LocomotiveType> locomotiveTypeList = distrSpan.getListLocomotiveType();
        //FXCollections.observableArrayList(locomotiveTypeList);
        locomotiveTypeComboBox.setItems(FXCollections.observableArrayList(locomotiveTypeList));

        // �� ��������� ������ ���� ������ "�����������"
        for (LocomotiveType type : locomotiveTypeList) {
            if (type.getIdLType() == 3) {
                locomotiveTypeComboBox.getSelectionModel().select(type);
                break;
            }
        }

        stBeginExpress.setText(distrSpan.getStB().getStation().getCodeExpress());
        stBeginESR.setText(distrSpan.getStB().getStation().getCodeESR());
        stBeginESR2.setText(distrSpan.getStB().getStation().getCodeESR2());
        stBegin.setText(distrSpan.getStB().getStation().getName());
        stBeginABS.setText(String.valueOf(distrSpan.getSpanScale().getAbsKM()));

        stEndExpress.setText(distrSpan.getStE().getStation().getCodeExpress());
        stEndESR.setText(distrSpan.getStE().getStation().getCodeESR());
        stEndESR2.setText(distrSpan.getStE().getStation().getCodeESR2());
        stEnd.setText(distrSpan.getStE().getStation().getName());
        stEndABS.setText(String.valueOf(distrSpan.getSpanScale().getAbsKM_E()));
    }

    /**
     * ������� ������ �.�. ��������, ��������� ��� ����������� � �������
     * @return list {@link SpanStModel}
     */
    private List<SpanStModel> buildSpanStModelListFromDistrSpan(DistrSpan distrSpan) {
        List<SpanStModel> itemList = new ArrayList<>();
        for (SpanSt spanSt : distrSpan.getSpanScale().getSpanSt()) {
            SpanStModel item = new SpanStModel(spanSt.getIdSpanst(),
                    spanSt.getIdSpan(), spanSt.getNum(), spanSt.getCodeExpress(),
                    spanSt.getCodeESR(), spanSt.getCodeESR2(), spanSt.getName(),
                    spanSt.getAbsKM(), spanSt.getTstop());
            itemList.add(item);
        }
        return itemList;
    }

    @FXML
    private void handleSelectLocomotiveType(ActionEvent event){
        logger.debug("handleSelectLocomotiveType");
        LocomotiveType type = locomotiveTypeComboBox.getSelectionModel().getSelectedItem();
        List<SpanStTimeModel> list = new ArrayList<>();
        if (type != null) {
            list = buildSpanStTimeModelList(distrSpan, type);
        }
        spanStTimeTable.setItems(FXCollections.observableArrayList(list));
    }

    /**
     * ������� ������ SpanStTimeModel ��� ��������� LocomotiveType �� ���� SpanStTime ��������
     * @param distrSpan {@link DistrSpan} ������� �������
     * @param type {@link LocomotiveType} ��� ��������
     * @return ������ {@link SpanStTimeModel}
     */
    private List<SpanStTimeModel> buildSpanStTimeModelList(DistrSpan distrSpan, LocomotiveType type) {
        List<SpanStTimeModel> list = new ArrayList<>();
        List<SpanStTime> spanStTimeList = distrSpan.getListSpanStTime(type);
        Collections.sort(spanStTimeList);
        for (SpanStTime stTime : spanStTimeList) {
            SpanStTimeModel item = new SpanStTimeModel(stTime, stTime.getIdScale(), stTime.getIdLType(),
                    stTime.getNum(), stTime.getTmoveO(), stTime.getTmoveE());
            list.add(item);
        }
        return list;
    }

    @FXML
    private void handleBuildAll(ActionEvent event){
        logger.debug("handleBuildAll");
        LocomotiveType currentLoc = locomotiveTypeComboBox.getSelectionModel().getSelectedItem();
        if (currentLoc != null) {
            distrSpan.buildNewSpanStTime(currentLoc);
            List<SpanStTimeModel> list = buildSpanStTimeModelList(distrSpan, currentLoc);
            spanStTimeTable.setItems(FXCollections.observableArrayList(list));
        }
    }

    @FXML
    private void handleSaveToDBSpanStTime(ActionEvent event){
        logger.debug("handleSaveToDBSpanStTime");
        try {
            //LocomotiveType currentLoc = locomotiveTypeComboBox.getSelectionModel().getSelectedItem();
            // ������� �������� ������ �� ������� � distrSpan
            updateDistrSpanFromSpanStTimeModelList(spanStTimeTable.getItems());
            // ����� ��������� � ��
            distrSpan.saveSpanStTime();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(distrSpan.getSpanScale().getName());
            alert.setHeaderText(okMessage);
            alert.show();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(distrSpan.getSpanScale().getName());
            alert.setHeaderText(errMessage);
            alert.show();

        }
    }

    /**
     * @param items
     */
    private void updateDistrSpanFromSpanStTimeModelList(ObservableList<SpanStTimeModel> items) {
        for(SpanStTimeModel timeModel: items){
            SpanStTime stTime = timeModel.getStTime();
            stTime.setTmoveO(timeModel.getTMoveO());
            stTime.setTmoveE(timeModel.getTMoveE());
        }
    }
}
