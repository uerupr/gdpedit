package com.gmail.alaerof.javafx.control.editabletableview;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.function.Function;
import java.util.regex.Pattern;
import javafx.beans.property.DoubleProperty;
import javafx.util.converter.DoubleStringConverter;

/**
 * {@link DoubleProperty} editable table cell
 * @param <T> Entity with property-fields to be edited in TableView
 */
public class DoubleEditCell<T> extends EditCell<T, Double, DoubleProperty> {
    /**
     * Format pattern
     */
    private static final String pattern = "####.##";
    /**
     * Double pattern
     */
    private final Pattern doublePattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    /**
     * {@inheritDoc}
     */
    public DoubleEditCell(Function<T, DoubleProperty> property) {
        super(property);
    }

    public DoubleEditCell(Function<T, DoubleProperty> property, boolean editable) {
        this(property);
        this.setEditable(editable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void bindItemProperty() {
        textProperty().bindBidirectional(itemProperty(), new FormattedDoubleStringConverter());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Double parseText(String text) {
        return Double.parseDouble(text);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Pattern getSimpleTypePattern() {
        return doublePattern;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateItem(Double item, boolean empty) {
        super.updateItem(item, empty);
        if (!this.isEditable()) {
            setText(empty ? null : getString());
            setGraphic(null);
        }
    }

    /**
     * @return formatted number as string
     */
    private String getString() {
        String ret = "";
        if (getItem() != null) {
            Double value = getItem();
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat myFormatter = new DecimalFormat(pattern, otherSymbols);
            String output = myFormatter.format(value);
            return output;
        } else {
            ret = "0.00";
        }
        return ret;
    }

    private class FormattedDoubleStringConverter extends  DoubleStringConverter{
        /** {@inheritDoc} */
        @Override public Double fromString(String value) {
            // If the specified value is null or zero-length, return null
            if (value == null) {
                return null;
            }

            value = value.trim();

            if (value.length() < 1) {
                return null;
            }

            return Double.valueOf(value);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString(Double value) {
            // If the specified value is null, return a zero-length String
            if (value == null) {
                return "";
            }
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat myFormatter = new DecimalFormat(pattern, otherSymbols);
            String output = myFormatter.format(value);
            return output;
        }
    }
}
