package com.gmail.alaerof.entity;

import com.gmail.alaerof.dao.common.GDPGridConfig;
import com.gmail.alaerof.dao.interfacedao.exceptions.DataManageException;
import java.util.ArrayList;

import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.gmail.alaerof.dao.dto.LineSt;
import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.dao.factory.IGDPDAOFactory;
import com.gmail.alaerof.dao.interfacedao.IStationDAO;
import com.gmail.alaerof.dao.interfacedao.exceptions.ObjectNotFoundException;

/**
 * 
 * @author Helen Yrofeeva
 * 
 */
public class DistrStation extends DistrPoint {
    private static Logger logger = LogManager.getLogger(DistrSpan.class);
    private IGDPDAOFactory dataSource;
    private Station station;
    private List<DistrLineSt> listLines;
    private int y0; // ���������� ���������� ����� ������� � �������� ��� �����
                    // ������� ������ (��� ���������������)
    private int y1; // ���������� ���������� ����� ������� � �������� c ������
                    // ������� ������ �� ��� (� ����������������)
    private int yE0; // ���������� ���������� ������ ����� ������� � ��������
                     // ��� �����
                     // ������� ������ (��� ���������������)
    private int yE1; // ���������� ���������� ������ ����� ������� � �������� c
                     // ������
                     // ������� ������ �� ��� (� ����������������)
    private int nameHeight; // ��������� ������ �������
    private int nameWidht; // ��������� ������ �������
    private boolean expanded; // ������� ����, ��� ���������� ���� �������

    private DistrSpan span; // �������, � �������� ����� �������
    private int be; // ������� 0-������ ��� 1-��������� ������� ��������

    public DistrStation(IGDPDAOFactory dataSource, int idSt) {
        this.dataSource = dataSource;
        IStationDAO sd = this.dataSource.getStationDAO();
        try {
            station = sd.getStation(idSt);
            listLines = new ArrayList<>();
            ArrayList<LineSt> list = (ArrayList<LineSt>) station.getListLine();
            for (LineSt slt : list) {
                listLines.add(new DistrLineSt(slt));
            }
        } catch (ObjectNotFoundException | DataManageException e) {
            logger.error(e.toString(), e);
        }
    }

    public Station getStation() {
        return station;
    }

    public List<DistrLineSt> getListLines() {
        return listLines;
    }

    public List<DistrLineSt> getVisibleLines() {
        ArrayList<DistrLineSt> list = new ArrayList<>();
        for (DistrLineSt ls : listLines) {
            if (!ls.getLineSt().isCloseInGDP()) {
                list.add(ls);
            }
        }
        return list;
    }

    /**
     * ������ �� ���������������� ������������ ��������� ����� �������
     * 
     * @param configGDP
     */
    public void calcLineStY0(GDPGridConfig configGDP) {
        int gap = configGDP.indentLine;
        if (!expanded) {
            // ���� ������� �� ��������, �� ��� ������������ ���������� �����
            // ���������
            gap = 0;
        }
        int y = y0;
        List<DistrLineSt> listL = this.getVisibleLines();
        for (DistrLineSt dlst : listL) {
            y += gap;
            dlst.setY0(y);
        }
        yE0 = y + gap;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + station.getIdSt();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DistrStation other = (DistrStation) obj;
        if (station.getIdSt() != other.station.getIdSt())
            return false;
        return true;
    }

    @Override
    public String toString() {
        //return "DistrStation [station=" + station + "]";
        return station.getName();
    }

    public int getBE() {
        return be;
    }

    public void setBE(int be) {
        this.be = be;
    }

    public DistrSpan getSpan() {
        return span;
    }

    public void setSpan(DistrSpan span) {
        this.span = span;
    }

    /**
     * 
     * @return ���������� ���������� ����� ������� � �������� ��� ����� �������
     *         ������ (��� ���������������)
     */
    public int getY0() {
        return y0;
    }

    public void setY0(int y0) {
        this.y0 = y0;
    }

    public int getNameHeight() {
        return nameHeight;
    }

    public void setNameHeight(int nameHeight) {
        this.nameHeight = nameHeight;
    }

    public int getNameWidht() {
        return nameWidht;
    }

    public void setNameWidht(int nameWidht) {
        this.nameWidht = nameWidht;
    }

    /**
     * 
     * @return ���������� ���������� ������� ����� ������� � �������� c ������
     *         ������� ������ �� ��� (� ����������������)
     */
    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    /**
     * ���������� ������������������ ������ � �������� �� ����� �������
     */
    public int getExpandGap() {
        return 0;
    }

    /**
     * ���������� ������������������ ������ ���� ����� �������
     */
    public int getExpandHeight() {
        return yE0 - y0;
    }

    /**
     * ���������� ���������������� ������ ���� ����� �������
     */
    public int getScaledExpandHeight() {
        return yE1 - y1;
    }

    public int getYE0() {
        return yE0;
    }

    public void setYE0(int yE0) {
        this.yE0 = yE0;
    }

    /**
     * 
     * @return ���������� ���������� ������ ����� ������� � �������� c ������
     *         ������� ������ �� ��� (� ����������������)
     */
    public int getYE1() {
        return yE1;
    }

    public void setYE1(int yE1) {
        this.yE1 = yE1;
    }

    /** ���������� ������� ����� ������� � �������� */
    public int getWidthLineSt() {
        int res = 1;
        if (be == 0) {
            res = span.getSpanScale().getWidthLine_fst();
        }
        if (be == 1) {
            res = span.getSpanScale().getWidthLine_lst();
        }
        return Math.max(1, res);
    }

    /** ������������� ������� ����� ������� � �������� */
    public void setWidthLineSt(int width) {
        int res =  Math.max(1, width);
        if (be == 0) {
            span.getSpanScale().setWidthLine_fst(res);
        }
        if (be == 1) {
            span.getSpanScale().setWidthLine_lst(res);
        }
    }
    
    /** ���������� ��������������� ����� ������� � % */
    public int getScaleSt() {
        int res = 100;
        if (be == 0) {
            res = span.getSpanScale().getScale();
        }
        if (be == 1) {
            res = span.getSpanScale().getScale_lst();
        }
        return Math.max(1, res);
    }
    /**
     * ���������� �������� ���� �� ��� ��
     * 
     * @param idLineSt
     *            �� ����
     * @return �������� ���� �� ID
     */
    public String getLineName(int idLineSt) {
        String name = "";
        for (DistrLineSt ln : listLines) {
            if (ln.getLineSt().getIdLineSt() == idLineSt) {
                name = ln.getLineSt().getPs();
            }
        }
        return name.trim();
    }
    /**
     * ���������� ������ ���� �� ����
     * @param idLineSt
     * @return
     */
    public DistrLineSt getLine(int idLineSt) {
        DistrLineSt line = null;
        for (DistrLineSt ln : listLines) {
            if (ln.getLineSt().getIdLineSt() == idLineSt) {
                line = ln;
            }
        }
        return line;
    }
    
    /**
     * ���������� �� ���� �� ��� ��������
     * 
     * @param name
     *            �������� ����
     * @return ID ����
     */
    public int getLineID(String name) {
        int idLine = 0;
        if (name != null) {
            for (DistrLineSt ln : listLines) {
                if (ln.getLineSt().getPs().equals(name)) {
                    idLine = ln.getLineSt().getIdLineSt(); 
                }
            }
        }
        return idLine;
    }
    
    /**
     * ���������� �������� ������������ ����� ���� �� ����� �� ��
     * 
     * @param idLineSt
     *            �� ����
     * @return �������� ���� �� ID
     */
    public int getLineY(int idLineSt) {
        int y = -1;
        for (DistrLineSt ln : listLines) {
            if (ln.getLineSt().getIdLineSt() == idLineSt) {
                y = ln.getY1();
            }
        }
        return y;
    }

    @Override
    public TypePoint getTypePoint() {
        return DistrPoint.TypePoint.SeparatePoint;
    }

    @Override
    public String getNamePoint() {
        if (station != null) {
            return station.getName();
        } else {
            return "";
        }
    }

    @Override
    public int getIdPoint() {
        if (station != null) {
            return station.getIdSt();
        } else {
            return 0;
        }
    }
}
