package com.gmail.alaerof.javafx.dialog.calculategdp.model;

import com.gmail.alaerof.dao.dto.groupp.GroupP;
import com.gmail.alaerof.dao.dto.routed.RouteD;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class RouteDModel {
    private RouteD routeD;
    private StringProperty nameR;
    private StringProperty typeR;

    public RouteDModel(RouteD routeD) {
        this.routeD = routeD;
        nameR = new SimpleStringProperty(routeD.getNameR());
        typeR = new SimpleStringProperty(routeD.getTypeRAsString());
    }

    public RouteD getRouteD() {
        return routeD;
    }

    public String getNameR() {
        return nameR.get();
    }

    public StringProperty nameRProperty() {
        return nameR;
    }

    public void setNameR(String nameR) {
        this.nameR.set(nameR);
    }

    public String getTypeR() {
        return typeR.get();
    }

    public StringProperty typeRProperty() {
        return typeR;
    }

    public void setTypeR(String typeR) {
        this.typeR.set(typeR);
    }

    public static List<RouteD> extractRouteD(List<RouteDModel> selected) {
        List<RouteD> list = new ArrayList<>();
        for(RouteDModel routeDModel: selected){
            list.add(routeDModel.getRouteD());
        }
        return list;
    }
}
