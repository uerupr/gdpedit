package com.gmail.alaerof.entity.train;

import com.gmail.alaerof.dao.common.TrainThreadGeneralParams;
import com.gmail.alaerof.util.CalcString;
import java.awt.Dimension;

public class DrawLineHor extends DrawLine {

    public DrawLineHor(TrainThreadGeneralParams generalParams, NewLineSelfParams selfParams) {
        super(generalParams, selfParams);
        direction = NewLine.Direction.hor;
    }

    @Override
    protected Dimension getCaptionSize() {
        int h = CalcString.getStringH(generalParams.captionSt);
        int w = CalcString.getStringW(generalParams.captionSt, selfParams.getCaptionValue()) + 2;
        return new Dimension(w, h);
    }

    @Override
    protected void calculateAllPoints() {
        int npoints = 10;
        int x[] = new int[npoints];
        int y[] = new int[npoints];
        int ph = 2; //this.getIndent().height;
        int pw = 0; //this.getIndent().width;
        int w = getSize().width;
        int h = getSize().height;
        x[0] = pw;
        y[0] = h - ph;

        x[1] = pw;
        y[1] = y[0] - ph;

        x[6] = x[0] + w;
        y[6] = y[1];

        x[7] = x[6];
        y[7] = y[0];

        x[8] = x[6];
        y[8] = h;

        x[9] = x[0];
        y[9] = y[8];

        x[2] = x[1];
        y[2] = y[1] - this.getCaptionSize().height;

        x[3] = x[1] + this.getCaptionSize().width;
        y[3] = y[1];

        x[4] = x[6] - this.getCaptionSize().width;
        y[4] = y[1];

        x[5] = x[6];
        y[5] = y[6] - this.getCaptionSize().height;

        captionPointGDP1.x = x[1] - pw;
        captionPointGDP1.y = y[1] - (h - ph);
        captionPointGDP.x = x[4] - pw;
        captionPointGDP.y = y[4] - (h - ph);
        beginGDP.x = x[0] - pw;
        beginGDP.y = y[0] - (h - ph);
        endGDP.x = x[7] - pw;
        endGDP.y = y[7] - (h - ph);
    }
}
