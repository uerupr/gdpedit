package com.gmail.alaerof.dialog.linesttrain;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.gmail.alaerof.entity.DistrLineSt;

class LineStCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
    private static final long serialVersionUID = 1L;
    private DistrLineSt lineSt;
    private List<DistrLineSt> listLineSt;

    public LineStCellEditor(List<DistrLineSt> listLineSt) {
        this.listLineSt = listLineSt;
    }

    @Override
    public Object getCellEditorValue() {
        return this.lineSt;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
            int column) {
        if (value instanceof DistrLineSt) {
            this.lineSt = (DistrLineSt) value;
        }

        JComboBox<DistrLineSt> comboLineSt = new JComboBox<>();

        for (DistrLineSt aLineSt : listLineSt) {
            comboLineSt.addItem(aLineSt);
        }

        comboLineSt.setSelectedItem(lineSt);
        comboLineSt.addActionListener(this);

        if (isSelected) {
            comboLineSt.setBackground(table.getSelectionBackground());
        } else {
            comboLineSt.setBackground(table.getSelectionForeground());
        }

        return comboLineSt;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        @SuppressWarnings("unchecked")
        JComboBox<DistrLineSt> comboLineSt = (JComboBox<DistrLineSt>) event.getSource();
        this.lineSt = (DistrLineSt) comboLineSt.getSelectedItem();
    }

    public void setListLineSt(List<DistrLineSt> listLineSt) {
        this.listLineSt = listLineSt;
    }

}