package com.gmail.alaerof.gdpcalculation.model;

import com.gmail.alaerof.util.TimeConverter;
import java.util.Comparator;

public class DistrTrainPriorityComparator implements Comparator<DistrTrainGDPCalc> {
    @Override
    public int compare(DistrTrainGDPCalc distrTrain1, DistrTrainGDPCalc distrTrain2) {
        int res = distrTrain1.getCategory().getPriority() - distrTrain2.getCategory().getPriority();
        if (res == 0) {
            int tm1 = TimeConverter.timeToMinutes(distrTrain1.getDistrTrain().getOccupyOff());
            int tm2 = TimeConverter.timeToMinutes(distrTrain2.getDistrTrain().getOccupyOff());
            res = tm1 - tm2;
        }
        return res;
    }
}
