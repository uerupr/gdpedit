package com.gmail.alaerof.javafx.dialog.stationinfo;

import com.gmail.alaerof.dao.dto.Station;
import com.gmail.alaerof.entity.DistrStation;
import com.gmail.alaerof.javafx.dialog.SwingFXDialogBase;
import com.gmail.alaerof.javafx.dialog.stationinfo.view.StationInfoLayoutController;
import java.awt.Frame;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

public class StationInfoFXDialog extends SwingFXDialogBase {
    private StationInfoLayoutController controller;
    private DistrStation station;
    public StationInfoFXDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        rootFXMLPath = "com/gmail/alaerof/javafx/dialog/stationinfo/view/StationInfoLayout.fxml";
        resourceBundleBaseName = "bundles.StationInfoFXDialog";
    }

    @Override
    protected void setController(FXMLLoader loader) {
        controller = loader.getController();
        controller.setScene(scene);
    }

    @Override
    protected int[] getInitialFrameBounds() {
        int bounds[] = {0, 0, 800, 650};
        return bounds;
    }

    public void setDialogContent(DistrStation station) {
        this.station = station;

        // This method is invoked on the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.setContent(station);
            }
        });
    }
}
