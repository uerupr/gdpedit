if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_rD8_sumOT]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_rD8_sumOT]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_rD8_sumOT AS  SELECT SUM(overTime) as sumOT, IDtrain, IDdir  FROM Station INNER JOIN xLineSt_Train ON Station.IDst = xLineSt_Train.IDst GROUP BY IDtrain, IDdir
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

