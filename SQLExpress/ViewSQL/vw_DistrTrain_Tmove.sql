if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DistrTrain_Tmove]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DistrTrain_Tmove]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_DistrTrain_Tmove
AS
SELECT IDtrain, SUM(Tmove) AS Tt
	FROM xSpTOcc
	WHERE (Tmove>0) AND (IDspan IN (SELECT IDspan FROM Span WHERE Span.BR <> 0))
	GROUP BY IDtrain 



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

