if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_rD8_dir]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_rD8_dir]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_rD8_dir AS  SELECT dbo.vw_rD8_sumOT_Tu.*, vw_GDParmDistr.name  FROM dbo.vw_rD8_sumOT_Tu INNER JOIN vw_GDParmDistr  ON dbo.vw_rD8_sumOT_Tu.GDParmID = vw_GDParmDistr.GDParmID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

