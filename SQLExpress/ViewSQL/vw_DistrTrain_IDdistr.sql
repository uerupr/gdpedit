if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DistrTrain_IDdistr]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DistrTrain_IDdistr]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_DistrTrain_IDdistr
AS
SELECT GDParm.IDdistr, xDistrTrain.*
	FROM GDParm INNER JOIN xDistrTrain ON GDParm.GDParmID = xDistrTrain.GDParmID
	


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

