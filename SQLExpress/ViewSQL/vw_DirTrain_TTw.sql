if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DirTrain_TTw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DirTrain_TTw]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_DirTrain_TTw
AS
SELECT xTrainStop.IDtrain, IDdir, SUM(xTrainStop.TStop) AS TwStop
	FROM xTrainStop INNER JOIN (Span INNER JOIN Span_St 
		ON Span.IDspan = Span_St.IDspan) 
		ON xTrainStop.IDspanst = Span_St.IDspanst
	WHERE (xTrainStop.TStop>0)
	GROUP BY IDdir, IDtrain

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

