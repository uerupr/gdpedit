if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DistrTrain_Len]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DistrTrain_Len]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_DistrTrain_Len
AS
SELECT Scale.IDdistr, xSpTOcc.IDtrain, SUM(Scale.L) AS Len
	FROM Scale, xSpTOcc
	WHERE (xSpTOcc.IDspan = Scale.IDspan) 
		AND (Scale.IDspan IN (SELECT IDspan FROM Span WHERE Span.BR <> 0))
	GROUP BY IDdistr, IDtrain


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

