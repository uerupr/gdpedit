if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_rD8_distr]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_rD8_distr]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_rD8_distr AS SELECT dbo.vw_GDParmDistr.name, dbo.xDistrTrain.GDParmID, dbo.xDistrTrain.IDtrain, dbo.xDistrTrain.code, dbo.xDistrTrain.Tt AS Tu, SUM(dbo.xLineSt_Train.overTime) AS sumOT FROM dbo.xDistrTrain INNER JOIN  dbo.xLineSt_Train ON dbo.xDistrTrain.IDtrain = dbo.xLineSt_Train.IDtrain INNER JOIN dbo.vw_GDParmDistr ON dbo.vw_GDParmDistr.GDParmID = dbo.xDistrTrain.GDParmID WHERE dbo.xLineSt_Train.IDst IN  (SELECT IDst FROM dbo.Station WHERE BR <> 0) GROUP BY dbo.vw_GDParmDistr.name, dbo.xDistrTrain.GDParmID, dbo.xDistrTrain.IDtrain, dbo.xDistrTrain.code, dbo.xDistrTrain.Tt 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

