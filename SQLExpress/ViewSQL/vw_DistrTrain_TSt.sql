if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DistrTrain_TSt]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DistrTrain_TSt]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_DistrTrain_TSt AS SELECT IDtrain, SUM(Tstop) AS ALLTStop  FROM dbo.xLineSt_Train  WHERE (Tstop >= 0) AND (IDst IN (SELECT IDst FROM Station WHERE (BR<>0)  AND (type <> 'тех.'))) GROUP BY IDtrain 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

