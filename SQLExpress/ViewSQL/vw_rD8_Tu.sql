if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_rD8_Tu]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_rD8_Tu]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_rD8_Tu AS SELECT xDirTrain.IDdir, xDirTrain.IDtrain, GDParmID, Direction.Name AS NOD, code, xDirTrain.Tt AS Tu   FROM (xDirTrain INNER JOIN Direction ON xDirTrain.IDdir = Direction.IDdir)  INNER JOIN xDistrTrain ON xDirTrain.IDtrain = xDistrTrain.IDtrain 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

