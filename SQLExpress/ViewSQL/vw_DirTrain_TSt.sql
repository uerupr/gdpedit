if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DirTrain_TSt]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DirTrain_TSt]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_DirTrain_TSt AS  SELECT IDtrain, IDdir, SUM(Tstop) AS ALLTStop 
FROM  Station INNER JOIN xLineSt_Train ON Station.IDst = xLineSt_Train.IDst  WHERE  (Tstop >= 0)  AND (type <> 'тех.') GROUP BY IDtrain, IDdir
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

