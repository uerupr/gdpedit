if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DistrTrain_TTw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DistrTrain_TTw]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_DistrTrain_TTw
AS
SELECT xTrainStop.IDtrain, SUM(TStop) AS TwStop
	FROM xTrainStop
	WHERE (TStop>0) AND IDspanst IN 
		(SELECT IDspanst FROM Span_St WHERE IDspan IN 
			(SELECT IDspan FROM Span WHERE Span.BR <> 0))
	GROUP BY IDtrain



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

