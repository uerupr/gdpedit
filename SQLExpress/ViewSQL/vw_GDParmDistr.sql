if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_GDParmDistr]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_GDParmDistr]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_GDParmDistr
AS 
SELECT GDParm.*, IDdir, INDX, Distr.name, main
FROM GDParm INNER JOIN Distr ON GDParm.IDdistr = Distr.IDdistr 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

