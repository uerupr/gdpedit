if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_rD1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_rD1]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_rD1
AS 
SELECT GDParm.IDdistr, rGDPall_list.GDPallID, xDistrTrain.typeTR, Count(xDistrTrain.code) AS CNT 
	FROM (xDistrTrain INNER JOIN GDParm ON xDistrTrain.GDParmID = GDParm.GDParmID) 
	  INNER JOIN rGDPall_list ON GDParm.GDParmID = rGDPall_list.GDParmID
	WHERE xDistrTrain.hidden = 0
	GROUP BY GDParm.IDdistr, rGDPall_list.GDPallID, xDistrTrain.typeTR


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

