if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DirTrain_Tmove]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DirTrain_Tmove]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_DirTrain_Tmove
AS
SELECT IDtrain, IDdir, SUM(Tmove) AS Tt
	FROM Span INNER JOIN xSpTOcc ON Span.IDspan = xSpTOcc.IDspan
	WHERE (Tmove>0) 
	GROUP BY IDtrain, IDdir

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

