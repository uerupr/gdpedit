if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_rD2allV]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_rD2allV]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_rD2allV
AS
SELECT     dbo.rPGRouteV.GDPallID, dbo.rPGRouteV.DCategoryID, dbo.rDCategory.Name, COUNT(dbo.rPGRouteV.PGRID) AS CountR, SUM(dbo.rPGRouteV.L) 
                      AS Sum_L, SUM(dbo.rPGRouteV.Tm) AS Sum_Tm, SUM(dbo.rPGRouteV.Tstop_uch) AS Sum_Tsu, SUM(dbo.rPGRouteV.Ttex) AS Sum_Tt
FROM         dbo.rPGRouteV INNER JOIN
                      dbo.rDCategory ON dbo.rPGRouteV.DCategoryID = dbo.rDCategory.DCategoryID
GROUP BY dbo.rPGRouteV.GDPallID, dbo.rPGRouteV.DCategoryID, dbo.rDCategory.Name

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

