if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_rD2N]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_rD2N]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_rD2N
AS
SELECT     dbo.rPGRouteV.GDPallID, COUNT(dbo.rPGRouteV.PGRID) AS nTR, dbo.rPGRouteV.DCategoryID, dbo.rPGRouteV.DTrafficID, 
                      dbo.rDCategory.Name AS cName, dbo.rDTraffic.Name AS tName
FROM         dbo.rPGRouteV INNER JOIN
                      dbo.rDCategory ON dbo.rPGRouteV.DCategoryID = dbo.rDCategory.DCategoryID INNER JOIN
                      dbo.rDTraffic ON dbo.rPGRouteV.DTrafficID = dbo.rDTraffic.DTrafficID
GROUP BY dbo.rPGRouteV.GDPallID, dbo.rPGRouteV.DCategoryID, dbo.rPGRouteV.DTrafficID, dbo.rDCategory.Name, dbo.rDTraffic.Name

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

