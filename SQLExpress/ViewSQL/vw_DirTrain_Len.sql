if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_DirTrain_Len]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_DirTrain_Len]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_DirTrain_Len
AS
SELECT Span.IDdir, Scale.IDdistr, xSpTOcc.IDtrain, SUM(Scale.L) AS Len
	FROM (Span INNER JOIN Scale ON Span.IDspan = Scale.IDspan) INNER JOIN xSpTOcc ON Span.IDspan = xSpTOcc.IDspan
	GROUP BY IDdir, IDdistr, IDtrain

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

