if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_rD8_sumOT_Tu]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_rD8_sumOT_Tu]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_rD8_sumOT_Tu AS SELECT GDParmID, dbo.vw_rD8_sumOT.*,  code, NOD, Tu FROM dbo.vw_rD8_sumOT, dbo.vw_rD8_Tu WHERE (dbo.vw_rD8_sumOT.IDdir = dbo.vw_rD8_Tu.IDdir) AND (dbo.vw_rD8_sumOT.IDtrain = dbo.vw_rD8_Tu.IDtrain) 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

